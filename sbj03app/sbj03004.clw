

   MEMBER('sbj03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ03004.INC'),ONCE        !Local module procedure declarations
                     END


TagLocationsToExclude PROCEDURE                       !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Site_Location                 LIKE(glo:Site_Location)
Shelf_Location                LIKE(glo:Shelf_Location)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tag                  STRING(1)
BRW1::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag                    LIKE(tag)                      !List box control field - type derived from local data
tag_Icon               LONG                           !Entry's icon ID
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Shelf Locations File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Shelf Location File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,112,148,214),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@80L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(584,324,12,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(584,340,12,13),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(448,164),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Shelf Location'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(los:Shelf_Location),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,156),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,186),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,215),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   Location_Queue.Site_Location = los:Site_Location
   Location_Queue.Shelf_Location = los:Shelf_Location
   GET(Location_Queue,Location_Queue.Site_Location,Location_Queue.Shelf_Location)
  IF ERRORCODE()
     Location_Queue.Site_Location = los:Site_Location
     Location_Queue.Shelf_Location = los:Shelf_Location
     ADD(Location_Queue,Location_Queue.Site_Location,Location_Queue.Shelf_Location)
    tag = '*'
  ELSE
    DELETE(Location_Queue)
    tag = ''
  END
    Queue:Browse:1.tag = tag
  IF (tag = '*')
    Queue:Browse:1.tag_Icon = 2
  ELSE
    Queue:Browse:1.tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(Location_Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     Location_Queue.Site_Location = los:Site_Location
     Location_Queue.Shelf_Location = los:Shelf_Location
     ADD(Location_Queue,Location_Queue.Site_Location,Location_Queue.Shelf_Location)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(Location_Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(Location_Queue)
    GET(Location_Queue,QR#)
    DASBRW::9:QUEUE = Location_Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(Location_Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Site_Location = los:Site_Location
     DASBRW::9:QUEUE.Shelf_Location = los:Shelf_Location
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Site_Location,DASBRW::9:QUEUE.Shelf_Location)
    IF ERRORCODE()
       Location_Queue.Site_Location = los:Site_Location
       Location_Queue.Shelf_Location = los:Shelf_Location
       ADD(Location_Queue,Location_Queue.Site_Location,Location_Queue.Shelf_Location)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020491'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TagLocationsToExclude')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCSHELF.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCSHELF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,los:Shelf_Location_Key)
  BRW1.AddRange(los:Site_Location,GLO:Select50)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,los:Shelf_Location,1,BRW1)
  BIND('tag',tag)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag,BRW1.Q.tag)
  BRW1.AddField(los:Shelf_Location,BRW1.Q.los:Shelf_Location)
  BRW1.AddField(los:Site_Location,BRW1.Q.los:Site_Location)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(Location_Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCSHELF.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020491'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020491'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020491'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      !Make the browse double click tag only - 4008 (DBH: 05-03-2004)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?Dastag)
      End !KeyCode() = MouseLeft2
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     Location_Queue.Site_Location = los:Site_Location
     Location_Queue.Shelf_Location = los:Shelf_Location
     GET(Location_Queue,Location_Queue.Site_Location,Location_Queue.Shelf_Location)
    IF ERRORCODE()
      tag = ''
    ELSE
      tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag = '*')
    SELF.Q.tag_Icon = 2
  ELSE
    SELF.Q.tag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     Location_Queue.Site_Location = los:Site_Location
     Location_Queue.Shelf_Location = los:Shelf_Location
     GET(Location_Queue,Location_Queue.Site_Location,Location_Queue.Shelf_Location)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Pick_Status PROCEDURE                                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Status           STRING(30)
Tmp:Single           BYTE
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Status
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
window               WINDOW('Pick Status'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Pick Status'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Report Type'),USE(?Tab1)
                           STRING('Single Status'),AT(248,196),USE(?String2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK,AT(304,196),USE(Tmp:Single),VALUE('1','0')
                           STRING('Status'),AT(248,212),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s20),AT(304,212,124,10),USE(tmp:Status),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('120L|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Status)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020499'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Status')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STATUS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Tmp:Single{Prop:Checked} = True
    ENABLE(?tmp:Status)
  END
  IF ?Tmp:Single{Prop:Checked} = False
    DISABLE(?tmp:Status)
  END
  FDCB6.Init(tmp:Status,?tmp:Status,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sts:Status_Key)
  FDCB6.AddField(sts:Status,FDCB6.Q.sts:Status)
  FDCB6.AddField(sts:Ref_Number,FDCB6.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020499'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020499'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020499'&'0')
      ***
    OF ?Tmp:Single
      IF ?Tmp:Single{Prop:Checked} = True
        ENABLE(?tmp:Status)
      END
      IF ?Tmp:Single{Prop:Checked} = False
        DISABLE(?tmp:Status)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
      tmp:status = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

EXhExcludedLocations PROCEDURE                        !Generated from procedure template - Browse

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Tag1             STRING(1)
tmp:Tag2             STRING(1)
tmp:Location         STRING(30)
Exch_Yes             STRING('YES')
BRW5::View:Browse    VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag1               LIKE(tmp:Tag1)                 !List box control field - type derived from local data
tmp:Tag1_Icon          LONG                           !Entry's icon ID
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(EXCEXCH)
                       PROJECT(eix1:Status)
                       PROJECT(eix1:WIPEXCID)
                       PROJECT(eix1:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:Tag2               LIKE(tmp:Tag2)                 !List box control field - type derived from local data
tmp:Tag2_Icon          LONG                           !Entry's icon ID
eix1:Status            LIKE(eix1:Status)              !List box control field - type derived from field
eix1:WIPEXCID          LIKE(eix1:WIPEXCID)            !Primary key field - type derived from field
eix1:Location          LIKE(eix1:Location)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Excluded Stock Types'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Excluded Stock Types'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,236,310),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Included Stock Types'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,78,148,270),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('10LJ@s1@120L(2)~Stock Type~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(224,162),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(224,194),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(224,226),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       PANEL,AT(304,54,72,310),USE(?Panel1),FILL(09A6A7CH)
                       SHEET,AT(380,54,236,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Excluded Stock Types'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(384,78,148,274),USE(?List:2),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('10LJ@s1@120L(2)~Stock Type~L(0)S(2)@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(544,162),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(544,198),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(544,230),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(308,134),USE(?btnAddStatus),TRN,FLAT,RIGHT,ICON('addstyp.jpg')
                       BUTTON,AT(308,302),USE(?btnRemoveStatus),TRN,FLAT,LEFT,ICON('remstyp.jpg')
                       BUTTON('&Rev tags'),AT(328,406,50,13),USE(?DASREVTAG:2),DISABLE,HIDE
                       BUTTON('sho&W tags'),AT(380,406,50,13),USE(?DASSHOWTAG:2),DISABLE,HIDE
                       BUTTON('&Rev tags'),AT(212,406,50,13),USE(?DASREVTAG),DISABLE,HIDE
                       BUTTON('sho&W tags'),AT(264,406,50,13),USE(?DASSHOWTAG),DISABLE,HIDE
                       BUTTON,AT(548,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
brw6                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = stp:Stock_Type
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = stp:Stock_Type
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag1 = 'Y'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag1 = ''
  END
    Queue:Browse.tmp:Tag1 = tmp:Tag1
  IF (tmp:Tag1 = 'Y')
    Queue:Browse.tmp:Tag1_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag1_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = stp:Stock_Type
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = stp:Stock_Type
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = stp:Stock_Type
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  brw6.UpdateBuffer
   glo:Queue2.Pointer2 = eix1:WIPEXCID
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = eix1:WIPEXCID
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:Tag2 = 'Y'
  ELSE
    DELETE(glo:Queue2)
    tmp:Tag2 = ''
  END
    Queue:Browse:1.tmp:Tag2 = tmp:Tag2
  IF (tmp:Tag2 = 'Y')
    Queue:Browse:1.tmp:Tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:Tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  brw6.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = eix1:WIPEXCID
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  brw6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  brw6.Reset
  SETCURSOR
  brw6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::8:QUEUE = glo:Queue2
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue2)
  brw6.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer2 = eix1:WIPEXCID
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = eix1:WIPEXCID
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  brw6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   brw6.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddTaggedStatus Routine

    Case Missive('This will exclude all the tagged stock types.'&|
      '<13,10>'&|
      '<13,10>Are you sure?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button

              Prog.ProgressSetup(Records(glo:Queue))

              loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  if error() then break.
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()

                  Access:StockTyp.Clearkey(stp:Use_Exchange_Key)
                  stp:Use_Exchange = 'YES'
                  stp:Stock_Type  = glo:Pointer
                  If Access:StockTyp.Tryfetch(stp:Use_Exchange_Key) = Level:Benign
                      !Found
                      Access:ExcExch.Clearkey(eix1:StatusTypeKey)
                      eix1:Location = tmp:Location
                      eix1:Status = stp:Stock_Type
                      If Access:ExcExch.Tryfetch(eix1:StatusTypeKey) = Level:Benign
                          !Found
                      Else
                          !Error
                          If Access:ExcExch.PrimeRecord() = Level:Benign
                              eix1:Location = tmp:Location
                              eix1:Status = stp:Stock_Type
                              If Access:ExcExch.TryUpdate() = Level:Benign
                                  !Insert Successful
                              Else !If Access:ACCSTAT.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:ACCSTAT.TryInsert() = Level:Benign
                          End !If Access:ACCSTAT.PrimeRecord() = Level:Benign
                      End !If Access:ACCSTAT.Tryfetch(acs:StatusKey) = Level:Benign
                  Else ! If Access:STATUS.Tryfetch(sta:Status_Key) = Level:Benign
                      !Error
                  End !If Access:STATUS.Tryfetch(sta:Status_Key) = Level:Benign
              End !Loop x# = 1 To Records(glo:Queue)

              Prog.ProgressFinish()
              Post(Event:Accepted,?DASUNTAGALL)

              BRW6.ResetSort(1)
              BRW5.ResetSort(1)
        Of 1 ! No Button
    End ! Case Missive
RemoveTaggedStatus Routine

    Case Missive('Are you sure you want to remove the tagged stock types?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
            Prog.ProgressSetup(Records(glo:queue2))

              Loop x# = 1 To Records(glo:Queue2)
                  Get(glo:Queue2,x#)

                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()

                  Access:ExcExch.Clearkey(eix1:WIPEXCKey)
                  eix1:WIPEXCID = glo:Pointer2
                  If Access:ExcExch.Fetch(eix1:WIPEXCKey) = Level:Benign
                      !Found
                      Relate:ExcExch.Delete(0)
                  Else ! If Access:ACCSTATS.Tryfetch(acs:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:ACCSTATS.Tryfetch(acs:RecordNumberKey) = Level:Benign
              End !Loop x# = 1 To Records(glo:Queue2)

              Prog.ProgressFinish()
              Post(Event:Accepted,?DASUNTAGALL:2)

              BRW6.ResetSort(1)
              BRW5.ResetSort(1)
        Of 1 ! No Button
    End ! Case Missive

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020466'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EXhExcludedLocations')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCEXCH.Open
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:STOCKTYP,SELF)
  brw6.Init(?List:2,Queue:Browse:1.ViewPosition,BRW7::View:Browse,Queue:Browse:1,Relate:EXCEXCH,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:Location = glo:Default_Site_Location
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,stp:Use_Exchange_Key)
  BRW5.AddRange(stp:Use_Exchange,Exch_Yes)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,stp:Stock_Type,1,BRW5)
  BIND('tmp:Tag1',tmp:Tag1)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:Tag1,BRW5.Q.tmp:Tag1)
  BRW5.AddField(stp:Stock_Type,BRW5.Q.stp:Stock_Type)
  BRW5.AddField(stp:Use_Exchange,BRW5.Q.stp:Use_Exchange)
  brw6.Q &= Queue:Browse:1
  brw6.AddSortOrder(,eix1:StatusTypeKey)
  brw6.AddRange(eix1:Location,tmp:Location)
  brw6.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,eix1:Status,1,brw6)
  BIND('tmp:Tag2',tmp:Tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  brw6.AddField(tmp:Tag2,brw6.Q.tmp:Tag2)
  brw6.AddField(eix1:Status,brw6.Q.eix1:Status)
  brw6.AddField(eix1:WIPEXCID,brw6.Q.eix1:WIPEXCID)
  brw6.AddField(eix1:Location,brw6.Q.eix1:Location)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF brw6.AskProcedure = 0
      CLEAR(brw6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  do DASBRW::6:DASUNTAGALL
  do DASBRW::8:DASUNTAGALL
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCEXCH.Close
    Relate:STOCKTYP.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnAddStatus
      do AddTaggedStatus
    OF ?btnRemoveStatus
      do RemoveTaggedStatus
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020466'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020466'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020466'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stp:Stock_Type
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag1 = ''
    ELSE
      tmp:Tag1 = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag1 = 'Y')
    SELF.Q.tmp:Tag1_Icon = 2
  ELSE
    SELF.Q.tmp:Tag1_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  Access:EXCEXCH.Clearkey(eix1:StatusTypeKey)
  eix1:Location    = tmp:Location
  eix1:Status    = stp:Stock_Type
  if (Access:EXCEXCH.TryFetch(eix1:StatusTypeKey) = Level:Benign)
      ! Found
      Return Record:Filtered
  else ! if (Access:EXCEXCH.TryFetch(eix1:StatusTypeKey) = Level:Benign)
      ! Error
  end ! if (Access:EXCEXCH.TryFetch(eix1:StatusTypeKey) = Level:Benign)
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stp:Stock_Type
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


brw6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = eix1:WIPEXCID
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:Tag2 = ''
    ELSE
      tmp:Tag2 = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag2 = 'Y')
    SELF.Q.tmp:Tag2_Icon = 2
  ELSE
    SELF.Q.tmp:Tag2_Icon = 1
  END


brw6.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


brw6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


brw6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = eix1:WIPEXCID
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Browse
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
InsertSerialNumber PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:IMEINumber       STRING(30)
window               WINDOW('Insert Serial Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert I.M.E.I. Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Insert I.M.E.I. Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(252,208,176,10),USE(tmp:IMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),REQ,UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:IMEINumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020474'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertSerialNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020474'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020474'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020474'&'0')
      ***
    OF ?tmp:IMEINumber
      IF LEN(CLIP(tmp:IMEINumber)) = 18
        !Ericsson IMEI!
        tmp:IMEINumber = SUB(tmp:IMEINumber,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
    OF ?OK
      ThisWindow.Update
      If tmp:IMEINUmber = ''
          Select(?tmp:IMEINumber)
      Else !tmp:IMEINUmber = ''
          Post(Event:CloseWindow)
      End !tmp:IMEINUmber = ''
    OF ?Cancel
      ThisWindow.Update
      tmp:IMEINumber = ''
      Post(event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
HandOverConfirmation PROCEDURE (Byte f:Type)          !Generated from procedure template - Window

FilesOpened          BYTE
Web_Temp_Job         STRING(20)
job_queue_temp       QUEUE,PRE(jobque)
Job_Number           STRING(20)
record_number        LONG
IMEINumber           STRING(30)
                     END
job_number_temp      REAL
wob_number_temp      LONG
engineer_temp        STRING(3)
engineer_name_temp   STRING(30)
Old_name_temp        STRING(30)
old_status_temp      STRING(30)
Location_temp        STRING(30)
update_text_temp     STRING(100)
tmp:status           STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SkillLevel       LONG
tmp:IMEINumber       STRING(30)
window               WINDOW('Hand Over Confirmation'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Hand Over Confirmation'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,212,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Defaults'),USE(?Tab1)
                           PROMPT('Hand Over Confirmation'),AT(168,113,200,8),USE(?Prompt:Title),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('User'),AT(168,176),USE(?tmp:SkillLevel:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(216,172),USE(?Lookup_Engineer),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(244,176,124,10),USE(engineer_name_temp),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Select Job Number'),AT(168,212),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s9),AT(244,212,48,10),USE(job_number_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('IMEI Number'),AT(168,228),USE(?tmp:IMEINumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(244,228,124,10),USE(tmp:IMEINumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('IMEI Number'),TIP('IMEI Number'),UPR
                         END
                       END
                       SHEET,AT(380,82,136,248),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(384,160,128,92),USE(?List1),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('32L(2)|M~Job Number~@s20@'),FROM(job_queue_temp)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       STRING(@s100),AT(168,340,268,12),USE(update_text_temp),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
! Inserting (DBH 06/03/2006) #6973 - Procedure to add to audit trail
local       CLASS
UpdateAuditTrail        Procedure(String func:Type)
            End ! local       CLASS
! End (DBH 06/03/2006) #6973
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

show_old_status     Routine
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        old_status_temp = job:current_status
    Else
        old_status_temp = ''
    end

show_engineer       Routine
    access:users.clearkey(use:user_code_key)
    use:user_code = engineer_temp
    if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    Else!if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = ''
    end!!if access:users.fetch(use:user_code_key) = Level:Benign
    Display(?engineer_name_temp)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020470'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HandOverConfirmation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  Relate:AUDIT.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:HANDOJOB.Open
  Relate:STAHEAD.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:LOCINTER.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  Access:LOCATLOG.UseFile
  Access:TRADEACC.UseFile
  Access:HANDOVER.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  
  ! Inserting (DBH 06/03/2006) #6973 - Change window title depending on type
  Case f:Type
  Of 0 !Job
      ?Prompt:Title{prop:Text} = Clip(?Prompt:Title{Prop:Text}) & ' - Job'
      ?WindowTitle{prop:Text} = Clip(?WindowTitle{Prop:Text}) & ' - Job'
  Of 1 !Exchange
      ?Prompt:Title{prop:Text} = Clip(?Prompt:Title{Prop:Text}) & ' - Exchange'
      ?WindowTitle{prop:Text} = Clip(?WindowTitle{Prop:Text}) & ' - Exchange'
  End ! Case f:Type
  ! End (DBH 06/03/2006) #6973
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:HANDOJOB.Close
    Relate:STAHEAD.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020470'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020470'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020470'&'0')
      ***
    OF ?Lookup_Engineer
      ThisWindow.Update
      GlobalRequest   = SelectRecord
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
      
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      glo:Select1 = use:Location
      PickLocationUsers
      glo:Select1 = ''
      
      case globalresponse
          of requestcompleted
              engineer_temp = use:user_code
              select(?+2)
          of requestcancelled
              engineer_temp = ''
              select(?-1)
      end!case globalreponse
      Do show_engineer
    OF ?tmp:IMEINumber
      ! Deleted (DBH 03/03/2006) #6973 - Superceeded by following code
      !     update_Text_temp = ''
      !     !------------------------------------------------------------------
      !     Web_Temp_Job = job_number_temp
      !     ! MESSAGE(glo:WebJob)
      !     if glo:WebJob then
      !         ! Change the tmp ref number if you
      !         ! can Look up from the wob file
      !         access:Webjob.clearkey(wob:RefNumberKey)
      !         wob:RefNumber = job_number_temp
      !         if access:Webjob.fetch(wob:refNumberKey) = level:benign then
      !             access:tradeacc.clearkey(tra:Account_Number_Key)
      !             tra:Account_Number = ClarioNET:Global.Param2
      !             access:tradeacc.fetch(tra:Account_Number_Key)
      !             Web_Temp_Job = job_number_temp & '-' & tra:BranchIdentification & wob:JobNumber
      !         end ! if
      !         ! Set up no preview on client
      !         ! ClarioNET:UseReportPreview(0)
      !     end ! if
      !     !------------------------------------------------------------------
      !     If Engineer_Temp = ''
      !         Case Missive('You must select an engineer.', 'ServiceBase 3g', |
      !                      'mstop.jpg', '/OK')
      !         Of 1 ! OK Button
      !         End ! Case Missive
      !     Else ! If Engineer_Temp = ''
      !
      !         access:jobs.clearkey(job:ref_number_key)
      !         job:ref_number = job_number_temp
      !         if access:jobs.fetch(job:ref_number_key) = Level:Benign
      !             If Access:AUDIT.PrimeRecord() = Level:Benign
      !
      !                 Access:USERS.Clearkey(use:User_Code_Key)
      !                 use:User_Code   = Engineer_Temp
      !                 If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !                     ! Found
      !                     aud:Notes         = 'USER: ' & Clip(use:Forename) & ' ' & Clip(use:Surname)
      !                 Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !                 ! Error
      !                 End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !
      !                 aud:Ref_Number = job:ref_number
      !                 aud:Date       = Today()
      !                 aud:Time       = Clock()
      !                 aud:Type       = 'JOB'
      !                 Access:USERS.ClearKey(use:Password_Key)
      !                 use:Password      = glo:Password
      !                 Access:USERS.Fetch(use:Password_Key)
      !                 aud:User   = use:User_Code
      !                 aud:Action = 'JOB HAND-OVER CONFIRMED'
      !                 Access:AUDIT.Insert()
      !             End! If Access:AUDIT.PrimeRecord() = Level:Benign
      !
      !             jobque:job_number     = Web_Temp_Job
      !             jobque:record_number += 1
      !             Add(job_queue_temp)
      !             Sort(job_queue_temp, -jobque:record_number)
      !
      !             update_text_temp                  = 'Job Number: ' & Clip(Web_Temp_Job) & ' Updated Successfully'
      !             ?update_text_temp{prop:fontcolor} = color:navy
      !             job_number_temp                   = ''
      !             old_status_temp                   = ''
      !             Display()
      !             beep(beep:systemasterisk)
      !             Select(?job_number_temp)
      !         Else! if access:jobs.fetch(job:ref_number_key) = Level:Benign
      !             Case Missive('Unable to find the selected job.', 'ServiceBase 3g', |
      !                          'mstop.jpg', '/OK')
      !             Of 1 ! OK Button
      !             End ! Case Missive
      !             old_status_temp = ''
      !             Select(?job_number_temp)
      !             update_text_temp                  = 'Job Number: ' & Clip(Web_Temp_Job) & ' Update Failed'
      !             ?update_text_temp{prop:fontcolor} = color:red
      !         end! if access:jobs.fetch(job:ref_number_key) = Level:Benign
      !     End ! If Engineer_Temp = ''
      !     Display()
      ! End (DBH 03/03/2006) #6973
      ! Inserting (DBH 03/03/2006) #6973 - Allow for Exchange or Job IMEI Number
      If Engineer_Temp = ''
      ! Changing (DBH 09/03/2006) #6973 - Change text
      !     Case Missive('You must select an engineer.','ServiceBase 3g',|
      !                    'mstop.jpg','/OK')
      !         Of 1 ! OK Button
      !     End ! Case Missive
      ! to (DBH 09/03/2006) #6973
          Case Missive('You must select a user.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          ! End (DBH 09/03/2006) #6973
          Select(?Engineer_Name_Temp)
          Cycle
      End ! If Engineer_Temp = ''
      
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = Job_Number_Temp
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Found
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber  = job:Ref_Number
          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              ! Found
      
          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              ! Error
          End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          If glo:WebJob
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  ! Found
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number  = wob:HeadAccountNumber
                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      ! Found
      
                  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      ! Error
                  End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  ! Error
              End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Found
      
              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Error
              End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
              If use:Location <> tra:SiteLocation
                  Case Missive('Error! '&|
                    '|The selected job is not in your control.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End ! If use:Location <> tra:SiteLocation
          End ! If glo:WebJob
          Case f:Type
          Of 0 !Job
              If tmp:IMEINumber <> job:ESN
                  Case Missive('The entered IMEI Number does not match the Job IMEI Number on the selected job.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Old_Status_Temp = ''
                  Update_Text_Temp    = 'Job Number: ' & job:Ref_Number & ' Update Failed'
                  Select(?tmp:IMEINumber)
                  Cycle
              End ! If tmp:IMEINumber <> job:ESN
              ! Inserting (DBH 06/06/2006) #7793 - Check the job is at the user's location
              If glo:WebJob
                  If jobe:HubRepair = True
                      Case Missive('Error!'&|
                        '|The selected job is in ARC Control.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  End ! If jobe:HubRepair = True
              Else ! If glo:WebJob
                  If jobe:HubRepair = False
                      Case Missive('Error!'&|
                        '|The selected job is in RRC Control.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  End ! If jobe:HubRepair = False
              End ! If glo:WebJob
              ! End (DBH 06/06/2006) #7793
              local.UpdateAuditTrail('JOB')
          Of 1 !Exchange
              Error# = False
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number  = job:Exchange_Unit_Number
              If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                  ! Found
                  If xch:ESN = tmp:IMEINumber
                      ! Changing (DBH 06/06/2006) #7793 - Check the correct site it working on the unit
                      ! local.UpdateAuditTrail('EXC')
                      ! to (DBH 06/06/2006) #7793
                      If glo:WebJob
      ! Deleting (DBH 25/07/2006) # 8020 - The RRC can HO an ARC exchange unit
      !                    If jobe:ExchangedATRRC = False
      !                        Case Missive('Error!'&|
      !                          '|The selected unit is not in your control.','ServiceBase 3g',|
      !                                       'mstop.jpg','/OK')
      !                            Of 1 ! OK Button
      !                        End ! Case Missive
      !                        Cycle
      !                    End ! If jobe:ExchangedATRRC = False
      ! End (DBH 25/07/2006) #8020
                      Else ! If glo:WebJob
                          If jobe:ExchangedATRRC = True
                              Case Missive('Error!'&|
                                '|The selected unit is not in your control.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Cycle
                          End ! If jobe:ExchangedATRRC = True
                      End ! If glo:WebJob
                      local.UpdateAuditTrail('EXC')
                      ! End (DBH 06/06/2006) #7793
                  Else ! If xch:ESN = tmp:IMEINumber
                      Error# = True
                  End ! If xch:ESN = tmp:IMEINumber
              Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                  ! Error
                  Error# = True
              End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
              If Error# = True
                  ! Didn't find 1st exchange. Try second (DBH: 06-03-2006)
                  Error# = False
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number  = jobe:SecondExchangeNumber
                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          ! Found
                          If xch:ESN = tmp:IMEINumber
                              ! Changing (DBH 6/06/2006) #7793 - Only allow the RRC to act on a 2nd exchange unit
                              ! local.UpdateAuditTrail('2NE')
                              ! to (DBH 0�06/2006) #7793
                              If glo:WebJob
                                  Case Missive('Error! '&|
                                    '|The selected unit is not in RRC control.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Cycle
                              End ! If glo:WebJob
                              local.UpdateAuditTrail('2NE')
                              ! End (DBH 06/06/2006) #7793
                          Else ! If xch:ESN = tmp:IMEINumber
                              Error# = True
                          End ! If xch:ESN = tmp:IMEINumber
                      Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          ! Error
                          Error# = True
                      End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                      If Error# = True
                          Case Missive('The entered IMEI Number does not match the Exchange IMEI Number on the selected job.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Old_Status_Temp = ''
                          Update_Text_Temp    = 'Job Number: ' & job:Ref_Number & ' Update Failed'
                          Select(?tmp:IMEINumber)
                          Cycle
                      End ! If Error# = True
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                      Case Missive('Unable to retrieve the selected job''s full details.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Old_Status_Temp = ''
                      Update_Text_Temp    = 'Job Number: ' & job:Ref_Number & ' Update Failed'
                      Select(?tmp:IMEINumber)
                      Cycle
                  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              End ! If Error# = True
          End ! Case f:Type
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Error
          Case Missive('Unable to find the selected job number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Old_Status_Temp = ''
          Update_Text_Temp    = 'Job Number: ' & job:Ref_Number & ' Update Failed'
          Select(?Job_Number_Temp)
          Cycle
      End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! End (DBH 03/03/2006) #6973
    OF ?Finish
      ThisWindow.Update
      If Records(Job_Queue_Temp)
          Print# = 0
          Case Missive('Do you wish to print a report of all jobs updated in this batch?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  Print# = 1
              Of 1 ! No Button
          End ! Case Missive
      
          If Print# = 1
      ! Changing (DBH 03/03/2006) #6973 - Pass a queue to the report
      !         Free(glo:Queue)
      !         Clear(glo:Queue)
      !         Loop x# = 1 To Records(job_queue_temp)
      !             Get(job_queue_temp,x#)
      !             glo:pointer  = jobque:Job_Number
      !             Add(glo:Queue)
      !         End!Loop x# = 1 To Records(job_queue_temp)
      !
      !         RapidStatusAllocationsReport(engineer_temp,GETINI('RAPIDSTATUS','Copies',,CLIP(PATH())&'\SB2KDEF.INI'),'HO')
      !
      !         Free(glo:Queue)
      ! to (DBH 03/03/2006) #6973
      
      ! End (DBH 03/03/2006) #6973
              If Access:HANDOVER.PrimeRecord() = Level:Benign
                  han:UserCode    = Engineer_Temp
                  If glo:WebJob
                      han:RRCAccountNumber = Clarionet:Global.Param2
                  End ! If glo:WebJob
                  ! Inserting (DBH 06/?6/2006) #7793 - Record the type of handover conf.
                  Case f:Type
                  Of 0
                      han:HandoverType = 'JOB'
                  Of 1
                      han:HandoverType = 'EXC'
                  End ! Case f:Type
                  ! End (DBH 06/06/2006) #7793
                  If Access:HANDOVER.TryInsert() = Level:Benign
                      ! Insert Successful
                      Loop x# = 1 To Records(Job_Queue_Temp)
                          Get(Job_Queue_Temp,x#)
                          If Access:HANDOJOB.PrimeRecord() = Level:Benign
                              haj:RefNumber   = han:RecordNumber
                              haj:JobNumber   = Job_Queue_Temp.Job_Number
                              haj:IMEINumber  = Job_Queue_Temp.IMEINumber
                              Access:JOBS.Clearkey(job:Ref_Number_Key)
                              job:Ref_Number  = Job_Queue_Temp.Job_Number
                              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  ! Found
                                  ! Changing (DBH 30/06/2006) # 7945 - Save the exchange model if for exchange
                                  ! haj:ModelNumber = job:Model_Number
                                  ! ! to (DBH 30/06/2006) # 7945
                                  Case f:Type
                                  Of 0
                                      haj:ModelNumber = job:Model_Number
                                  Of 1
                                      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                                      xch:Ref_Number = job:Exchange_UNit_Number
                                      If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                          !Found
                                          haj:ModelNumber = xch:Model_Number
                                      Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                          !Error
                                      End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                  End ! Case f:Type
                                  ! End (DBH 30/06/2006) #7945
      
                                  Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                                  jac:Ref_Number = job:Ref_Number
                                  Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                                  Loop
                                      If Access:JOBACC.NEXT()
                                         Break
                                      End !If
                                      If jac:Ref_Number <> job:Ref_Number      |
                                          Then Break.  ! End If
                                      haj:Accessories  = Clip(haj:Accessories) & ', ' & Clip(jac:Accessory)
                                  End !Loop
                              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  ! Error
                              End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              haj:Accessories = Sub(haj:Accessories,3,255)
                              If Access:HANDOJOB.TryInsert() = Level:Benign
                                  ! Insert Successful
                              Else ! If Access:HANDOJOB.TryInsert() = Level:Benign
                                  ! Insert Failed
                                  Access:HANDOJOB.CancelAutoInc()
                              End ! If Access:HANDOJOB.TryInsert() = Level:Benign
                          End !If Access:HANDOJOB.PrimeRecord() = Level:Benign
                      End ! Loop x# = 1 To Records(Job_Queue_Temp)
                      HandOverConfirmationReport(han:RecordNumber)
                  Else ! If Access:HANDOVER.TryInsert() = Level:Benign
                      ! Insert Failed
                      Access:HANDOVER.CancelAutoInc()
                  End ! If Access:HANDOVER.TryInsert() = Level:Benign
              End !If Access:HANDOVER.PrimeRecord() = Level:Benign
          End !If Print# = 1
      
      End !Records(Job_Queue_Temp)
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.UpdateAuditTrail        Procedure(String func:Type)
locAction STRING(80)
Code

    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = Engineer_Temp
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        ! Found
    Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        ! Error
    End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    If func:Type = 'JOB'
        locAction  = 'JOB HAND-OVER CONFIRMED'
    Else ! If func:Type = 'JOB'
        locAction  = 'EXCHANGE HAND-OVER CONFIRMED'
    End ! If func:Type = 'JOB'

    IF (AddToAudit(job:Ref_Number,func:Type,locAction,'USER: ' & Clip(use:Forename) & ' ' & Clip(use:Surname)))
    END ! IF


    Job_Queue_Temp.Job_Number   = job:Ref_Number
    Job_Queue_Temp.Record_Number += 1
    Job_Queue_Temp.IMEINumber = tmp:IMEINumber
    Add(Job_Queue_Temp)
    Sort(Job_Queue_Temp,-Job_Queue_Temp.Record_Number)

    Update_Text_Temp    = 'Job Number: ' & job:Ref_Number & ' Updated Successfully'
    Job_Number_Temp     = ''
    Old_Status_Temp     = ''
    tmp:IMEINumber      = ''
    Display()
    Beep(Beep:SystemAsterisk)
    Select(?Job_Number_Temp)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RapidExchangeAllocation PROCEDURE                     !Generated from procedure template - Window

save_ex4_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_audit_id        USHORT,AUTO
tmp:Location         STRING(30)
tmp:Engineer         STRING(60)
tmp:Status           STRING(30)
tmp:OnOrder          STRING(30)
tmp:48Hour           BYTE(0)
tmp:CurrentLocation  STRING(30)
ExchangeQueue        QUEUE,PRE(excque)
JobNumber            LONG
Manufacturer         STRING(30)
ModelNumber          STRING(30)
EngineerName         STRING(60)
Tick48Hour           BYTE(0)
Tick48Hour_Icon      LONG
TickOnOrder          BYTE(0)
TickOnOrder_Icon     LONG
Unit48Hour           BYTE(0)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:EngineerName     STRING(60)
BRW10::View:Browse   VIEW(RAPENGLS)
                       PROJECT(rapl:JobNumber)
                       PROJECT(rapl:SkillLevel)
                       PROJECT(rapl:JobStatus)
                       PROJECT(rapl:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rapl:JobNumber         LIKE(rapl:JobNumber)           !List box control field - type derived from field
job:Manufacturer       LIKE(job:Manufacturer)         !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
tmp:EngineerName       LIKE(tmp:EngineerName)         !List box control field - type derived from local data
rapl:SkillLevel        LIKE(rapl:SkillLevel)          !List box control field - type derived from field
rapl:SkillLevel_Icon   LONG                           !Entry's icon ID
rapl:JobStatus         LIKE(rapl:JobStatus)           !List box control field - type derived from field
rapl:JobStatus_Icon    LONG                           !Entry's icon ID
rapl:RecordNumber      LIKE(rapl:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Rapid Exchange Allocation'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(116,112,448,214),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('39L(2)|M~Job No~@s8@120L(2)|M~Manufacturer~@s30@120L(2)|M~Model Number~@s30@142L' &|
   '(2)|M~Engineer Name~@s60@11C(2)|I~48~@s1@11C(2)|I~O~@s1@'),FROM(Queue:Browse)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Exchange Allocation'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,38),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location:'),AT(68,56),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s30),AT(128,56),USE(tmp:Location),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       PANEL,AT(348,56,8,8),USE(?Panel3),FILL(COLOR:Lime)
                       PROMPT('- Order exists for selected Model'),AT(360,56),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(348,68,8,8),USE(?Panel3:2),FILL(COLOR:Blue)
                       PROMPT('- 48 Hour Exchange Order Requested'),AT(360,68),USE(?Prompt2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('- 48 Hour Exchange Order Processed'),AT(360,80),USE(?Prompt2:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(348,80,8,8),USE(?Panel3:3),FILL(COLOR:Red)
                       SHEET,AT(64,96,552,268),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Exchange Units Order Required'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,330),USE(?AllocateExchange),TRN,FLAT,LEFT,ICON('allexcp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Waybill:Check Routine
    ! Check if this job requires inserting into the 'waybill generation - awaiting processing' table
    If glo:WebJob
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        if jobe:HubRepair
            if job:Incoming_Consignment_Number <> ''                    ! Job must have an empty consignment number
                do Waybill:CheckRemoveFromAwaitProcess
                exit
            end

!            if job:Location <> tmp:RRCLocation
                !do Waybill:CheckRemoveFromAwaitProcess
!                exit
!            end

            Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
            wob:RefNumber = job:Ref_Number
            if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.

            Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
            wya:AccountNumber = wob:HeadAccountNumber
            wya:JobNumber   = job:Ref_Number
            if Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
                if not Access:WAYBAWT.PrimeRecord()                     ! Insert new waybill 'awaiting processing' record
                    wya:JobNumber     = job:Ref_Number
                    wya:AccountNumber = wob:HeadAccountNumber
                    wya:Manufacturer  = job:Manufacturer
                    wya:ModelNumber   = job:Model_Number
                    wya:IMEINumber    = job:ESN
                    if Access:WAYBAWT.Update()
                        Access:WAYBAWT.CancelAutoInc()
                    end
                end
            else

            end

        else
            do Waybill:CheckRemoveFromAwaitProcess
        end
    End !If glo:WebJob
Waybill:CheckRemoveFromAwaitProcess Routine
    ! Remove waybill 'awaiting processing' record (not actual waybill record(!))

    Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
    wob:RefNumber = job:Ref_Number
    if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.

    Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
    wya:AccountNumber = wob:HeadAccountNumber
    wya:JobNumber = job:Ref_Number
    if not Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
        Relate:WAYBAWT.Delete(0)                                ! Delete record if it exists
    end
!FillQueue       Routine

!Data
!local:WrongSite     Byte(0)
!Code
!    Free(ExchangeQueue)
!    Clear(ExchangeQueue)
!    SetCursor(Cursor:Wait)
!    If glo:WebJob
!        tmp:CurrentLocation = Clarionet:Global.Param2
!    Else !glo:WebJob
!        tmp:CurrentLocation = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!    End !glo:WebJob
!
!    Access:USERS.Clearkey(use:Password_Key)
!    use:Password    = glo:Password
!    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!        !Found
!        tmp:Location    = use:Location
!    Else ! If Access:USERS.Tryfetch(use:Password_Key) = LevelBenign
!        !Error
!    End !If AccessUSERS.Tryfetch(usePassword_Key) = LevelBenign
!
!    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
!    sts:Ref_Number = 350
!    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!        !Found
!        tmp:Status = sts:Status
!    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!
!    Save_job_ID = Access:JOBS.SaveFile()
!    Access:JOBS.ClearKey(job:ExcStatusKey)
!    job:Exchange_Status = tmp:Status
!    Set(job:ExcStatusKey,job:ExcStatusKey)
!    Loop
!        If Access:JOBS.NEXT()
!           Break
!        End !If
!        If job:Exchange_Status <> tmp:Status      |
!            Then Break.  ! End If
!
!        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
!        local:WrongSite = False
!        Save_aus_ID = Access:AUDSTATS.SaveFile()
!        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
!        aus:RefNumber   = job:Ref_Number
!        aus:Type        = 'EXC'
!        aus:DateChanged = Today()
!        Set(aus:DateChangedKey,aus:DateChangedKey)
!        Loop
!            If Access:AUDSTATS.PREVIOUS()
!               Break
!            End !If
!            If aus:RefNumber   <> job:Ref_Number      |
!            Or aus:Type        <> 'EXC'      |
!                Then Break.  ! End If
!            If aus:NewStatus = tmp:Status
!                Access:USERS.Clearkey(use:User_Code_Key)
!                use:User_Code   = aus:UserCode
!                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                    !Found
!                    If use:Location <> tmp:Location
!                        local:WrongSite = True
!                    End !If use:Location <> tmp:Location
!                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                    !Error
!                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                Break
!            End !If aus:NewStatus = tmp:Status
!        End !Loop
!        Access:AUDSTATS.RestoreFile(Save_aus_ID)
!
!        If local:WrongSite = True
!            Cycle
!        End !If WrongSite# = True
!
!        excque:JobNumber = job:Ref_Number
!        excque:Manufacturer = job:Manufacturer
!        excque:ModelNumber = job:Model_Number
!
!        Access:USERS.Clearkey(use:User_Code_Key)
!        use:User_code   = job:Engineer
!        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Found
!            excque:EngineerName    = Clip(use:Forename) & ' '& Clip(use:Surname)
!        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Error
!            excque:EngineerName    = ''
!        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!        excque:Tick48Hour_Icon = 3
!
!        Access:EXCHORDR.ClearKey(exo:By_Location_Key)
!        exo:Location     = tmp:Location
!        exo:Manufacturer = job:Manufacturer
!        exo:Model_Number = job:Model_Number
!        If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
!            !Found
!            excque:TickOnOrder_Icon = 2
!        Else!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
!            !Error
!            excque:TickOnOrder_Icon = 3
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
!
!        excque:Unit48Hour = 0
!        Add(ExchangeQueue)
!    End !Loop
!    Access:JOBS.RestoreFile(Save_job_ID)
!
!    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
!    sts:Ref_Number = 360
!    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!        !Found
!        tmp:Status = sts:Status
!    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
!
!    Save_job_ID = Access:JOBS.SaveFile()
!    Access:JOBS.ClearKey(job:ExcStatusKey)
!    job:Exchange_Status = tmp:Status
!    Set(job:ExcStatusKey,job:ExcStatusKey)
!    Loop
!        If Access:JOBS.NEXT()
!           Break
!        End !If
!        If job:Exchange_Status <> tmp:Status      |
!            Then Break.  ! End If
!
!        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
!        local:WrongSite = False
!        Save_aus_ID = Access:AUDSTATS.SaveFile()
!        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
!        aus:RefNumber   = job:Ref_Number
!        aus:Type        = 'EXC'
!        aus:DateChanged = Today()
!        Set(aus:DateChangedKey,aus:DateChangedKey)
!        Loop
!            If Access:AUDSTATS.PREVIOUS()
!               Break
!            End !If
!            If aus:RefNumber   <> job:Ref_Number      |
!            Or aus:Type        <> 'EXC'      |
!                Then Break.  ! End If
!            If aus:NewStatus = tmp:Status
!                Access:USERS.Clearkey(use:User_Code_Key)
!                use:User_Code   = aus:UserCode
!                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                    !Found
!                    If use:Location <> tmp:Location
!                        local:WrongSite = True
!                    End !If use:Location <> tmp:Location
!                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                    !Error
!                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                Break
!            End !If aus:NewStatus = tmp:Status
!        End !Loop
!        Access:AUDSTATS.RestoreFile(Save_aus_ID)
!
!        If local:WrongSite = True
!            Cycle
!        End !If WrongSite# = True
!
!        excque:JobNumber = job:Ref_Number
!        excque:Manufacturer = job:Manufacturer
!        excque:ModelNumber = job:Model_Number
!        Access:USERS.Clearkey(use:User_Code_Key)
!        use:User_code   = job:Engineer
!        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Found
!            excque:EngineerName    = Clip(use:Forename) & ' '& Clip(use:Surname)
!        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Error
!            excque:EngineerName    = ''
!        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!        !Has the exchange order been processed? -  (DBH: 30-10-2003)
!        Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
!        ex4:Received  = 1
!        ex4:Returning = 0
!        ex4:Location  = tmp:Location
!        ex4:JobNumber = excque:JobNumber
!        If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
!            excque:Tick48Hour_Icon = 4
!        Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
!            excque:Tick48Hour_Icon = 1
!        End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
!        excque:TickOnOrder = 3
!        excque:TickOnOrder_Icon = 3
!        excque:Unit48Hour = 1
!        Add(ExchangeQueue)
!    End !Loop
!    Access:JOBS.RestoreFile(Save_job_ID)
!
!    Sort(ExchangeQueue,excque:JobNumber)
!    SetCursor()
FillQueue       Routine
Data
local:WrongSite     Byte(0)
local:FoundHistory        Byte(0)
Code
    ! Clear data file - TrkBs: 6841 (DBH: 09-12-2005)
    Access:RAPENGLS.ClearKey(rapl:RecordNumberKey)
    rapl:RecordNumber = 1
    Set(rapl:RecordNumberKey,rapl:RecordNumberKey)
    Loop
        If Access:RAPENGLS.NEXT()
           Break
        End !If
        Relate:RAPENGLS.Delete(0)
    End !Loop

    SetCursor(Cursor:Wait)
    If glo:WebJob
        tmp:CurrentLocation = Clarionet:Global.Param2
    Else !glo:WebJob
        tmp:CurrentLocation = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    End !glo:WebJob

    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !Found
        tmp:Location    = use:Location
    Else ! If Access:USERS.Tryfetch(use:Password_Key) = LevelBenign
        !Error
    End !If AccessUSERS.Tryfetch(usePassword_Key) = LevelBenign

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 350
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found
        tmp:Status = sts:Status
    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

    Save_job_ID = Access:JOBS.SaveFile()
    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = tmp:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:Exchange_Status <> tmp:Status      |
            Then Break.  ! End If

        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:WrongSite = False
        local:FoundHistory = False
        Save_aus_ID = Access:AUDSTATS.SaveFile()
        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
               Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
            Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = tmp:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> tmp:Location
                        local:WrongSite = True
                    End !If use:Location <> tmp:Location
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Inserting (DBH 13/04/2007) # 8912 - Found correct status history
                local:FoundHistory = True
                ! End (DBH 13/04/2007) #8912
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop
        Access:AUDSTATS.RestoreFile(Save_aus_ID)

        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False
            Save_AUDIT_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> tmp:Location
                        local:WrongSite = True
                    End ! If use:Location <> tmp:Location
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop
            Access:AUDIT.RestoreFile(Save_AUDIT_ID)

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = tmp:CurrentLocation
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> tmp:Location
                    local:WrongSite = True
                End ! If tra:SiteLocation <> tmp:Location
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912

        If Access:RAPENGLS.PrimeRecord() = Level:Benign
            rapl:JobNumber = job:Ref_Number
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            rapl:ReportedFault = 0
            Access:EXCHORDR.ClearKey(exo:By_Location_Key)
            exo:Location     = tmp:Location
            exo:Manufacturer = job:Manufacturer
            exo:Model_Number = job:Model_Number
            If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Found
                ! Use this field to indicate the "On Order" - TrkBs: 6841 (DBH: 09-12-2005)
                rapl:JobStatus = 1
            Else!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Error
                rapl:JobStatus = 0
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
            ! Use this field to indicate the "48 Hour" - TrkBs: 6841 (DBH: 09-12-2005)
            rapl:SkillLevel = 0

            If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:RAPENGLS.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign
    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 360
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found
        tmp:Status = sts:Status
    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

    Save_job_ID = Access:JOBS.SaveFile()
    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = tmp:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:Exchange_Status <> tmp:Status      |
            Then Break.  ! End If

        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:FoundHistory = False
        local:WrongSite = False
        Save_aus_ID = Access:AUDSTATS.SaveFile()
        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
               Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
            Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = tmp:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> tmp:Location
                        local:WrongSite = True
                    End !If use:Location <> tmp:Location
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop
        Access:AUDSTATS.RestoreFile(Save_aus_ID)

        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False
            Save_AUDIT_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> tmp:Location
                        local:WrongSite = True
                    End ! If use:Location <> tmp:Location
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop
            Access:AUDIT.RestoreFile(Save_AUDIT_ID)

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = tmp:CurrentLocation
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> tmp:Location
                    local:WrongSite = True
                End ! If tra:SiteLocation <> tmp:Location
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912

        If Access:RAPENGLS.PrimeRecord() = Level:Benign
            rapl:JobNumber = job:Ref_Number
            rapl:JobStatus = 0
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            rapl:ReportedFault = 1
            !Has the exchange order been processed? -  (DBH: 30-10-2003)
            Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
            ex4:Received  = 1
            ex4:Returning = 0
            ex4:Location  = tmp:Location
            ex4:JobNumber = rapl:JobNumber
            If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                rapl:SkillLevel = 2
            Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                rapl:SkillLevel = 1
            End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
            If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:RAPENGLS.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign

    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

    Brw1.ResetSort(1)

    SetCursor()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020485'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  glo:FileName = 'RAPEXCHANGE' & Clock() & '.TMP'
  If GetTempPathA(255,TempFilePath)
      If Sub(TempFilePath,-1,1) = '\'
          glo:FileName = Clip(TempFilePath) & Clip(glo:FileName)
      Else !If Sub(TempFilePath,-1,1) = '\'
          glo:FileName = Clip(TempFilePath) & '\' & Clip(glo:FileName)
      End !If Sub(TempFilePath,-1,1) = '\'
  End
  GlobalErrors.SetProcedureName('RapidExchangeAllocation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:EXCHOR48.Open
  Relate:EXCHORDR.Open
  Relate:RAPENGLS.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Relate:WAYBPRO.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  Access:JOBSE.UseFile
  Access:WAYBAWT.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:RAPENGLS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Deleted (DBH 09/12/2005) #6841 - Not needed
  ! ?List{PROP:IconList,1} = '~block_blue.ico'
  ! ?List{PROP:IconList,2} = '~block_green.ico'
  ! ?List{Prop:IconList,3} = '~notick1.ico'
  ! ?List{Prop:IconList,4} = '~block_red.ico'
  !
  ! End (DBH 09/12/2005) #6841
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,rapl:JobNumberKey)
  BRW1.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,rapl:JobNumber,1,BRW1)
  BIND('tmp:EngineerName',tmp:EngineerName)
  ?List{PROP:IconList,1} = '~bblue.ico'
  ?List{PROP:IconList,2} = '~bgreen.ico'
  ?List{PROP:IconList,3} = '~bred.ico'
  ?List{PROP:IconList,4} = '~notick1.ico'
  BRW1.AddField(rapl:JobNumber,BRW1.Q.rapl:JobNumber)
  BRW1.AddField(job:Manufacturer,BRW1.Q.job:Manufacturer)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(tmp:EngineerName,BRW1.Q.tmp:EngineerName)
  BRW1.AddField(rapl:SkillLevel,BRW1.Q.rapl:SkillLevel)
  BRW1.AddField(rapl:JobStatus,BRW1.Q.rapl:JobStatus)
  BRW1.AddField(rapl:RecordNumber,BRW1.Q.rapl:RecordNumber)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:EXCHOR48.Close
    Relate:EXCHORDR.Close
    Relate:RAPENGLS.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
    Relate:WAYBPRO.Close
    Relate:WEBJOB.Close
  END
  Remove(Glo:FileName)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020485'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020485'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020485'&'0')
      ***
    OF ?AllocateExchange
      ThisWindow.Update
      Access:RAPENGLS.Clearkey(rapl:JobNumberKey)
      rapl:JobNumber  = brw1.q.rapl:JobNumber
      If Access:RAPENGLS.Tryfetch(rapl:JobNumberKey) = Level:Benign
          ! Found
          Found# = 0
          If rapl:ReportedFault = 1
              If Is48hourOrderProcessed(tmp:Location,rapl:JobNumber) = True
                  Found# = 1
              Else !Is47hourUnitProcessed(tmp:Location,excque:JobNumber) = True
                  Case Missive('The Exchange Unit request for this job has not yet been processed.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              End !Is47hourUnitProcessed(tmp:Location,excque:JobNumber) = True
          Else !Unit48Hour = 1
              Found# = 1
          End !Unit48Hour = 1
          If Found# = 1
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = rapl:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  ViewExchangeUnit()
                  If job:Exchange_Unit_Number <> 0
                      Do Waybill:Check
                      Access:JOBS.TryUpdate()
                  End !If job:Exchange_Unit_Number <> 0
      
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          End !Found# = 1
          Do FillQueue
      
      Else ! If Access:RAPENGLS.Tryfetch(rapl:JobNumberKey) = Level:Benign
          ! Error
      End ! If Access:RAPENGLS.Tryfetch(rapl:JobNumberKey) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Do FillQueue
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (rapl:SkillLevel = 1)
    SELF.Q.rapl:SkillLevel_Icon = 1
  ELSIF (rapl:SkillLevel = 2)
    SELF.Q.rapl:SkillLevel_Icon = 3
  ELSE
    SELF.Q.rapl:SkillLevel_Icon = 4
  END
  IF (rapl:JobStatus = 1)
    SELF.Q.rapl:JobStatus_Icon = 2
  ELSE
    SELF.Q.rapl:JobStatus_Icon = 4
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  Access:JOBS.Clearkey(job:Ref_Number_Key)
  job:Ref_Number  = rapl:JobNumber
  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! Found
  
  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! Error
  End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Engineer
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:EngineerName = CLip(use:Forename) & ' ' & Clip(use:Surname)
  Else !If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      tmp:EngineerName = ''
  End !If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Access:EXCHORDR.ClearKey(exo:By_Location_Key)
  exo:Location     = tmp:Location
  exo:Manufacturer = job:Manufacturer
  exo:Model_Number = job:Model_Number
  If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
      !Found
  
      excque:TickOnOrder_Icon = 2
  Else!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
      !Error
      excque:TickOnOrder_Icon = 3
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
  BRW10::RecordStatus=ReturnValue
  RETURN ReturnValue

HandOverReportHistory PROCEDURE                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:UserName         STRING(60)
tmp:RRCAccountNumber STRING(30)
BRW9::View:Browse    VIEW(HANDOVER)
                       PROJECT(han:RecordNumber)
                       PROJECT(han:DateCreated)
                       PROJECT(han:TimeCreated)
                       PROJECT(han:RRCAccountNumber)
                       JOIN(USERS,'Upper(use:User_Code) = Upper(han:UserCode)')
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
han:RecordNumber       LIKE(han:RecordNumber)         !List box control field - type derived from field
han:DateCreated        LIKE(han:DateCreated)          !List box control field - type derived from field
han:TimeCreated        LIKE(han:TimeCreated)          !List box control field - type derived from field
tmp:UserName           LIKE(tmp:UserName)             !List box control field - type derived from local data
han:RRCAccountNumber   LIKE(han:RRCAccountNumber)     !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Handover Confirmation Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Report Number'),USE(?Tab1)
                           LIST,AT(168,100,344,194),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('59L(2)|M~Report Number~@s8@51L(2)|M~Date Created~@d6@53L(2)|M~Time Created~@t1b@' &|
   '240L(2)|M~User Name~@s60@'),FROM(Queue:Browse)
                           BUTTON,AT(444,298),USE(?Print),TRN,FLAT,ICON('printp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Handover Confirmation Report History'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9::Sort1:Locator  StepLocatorClass                 !Conditional Locator - glo:WebJob = 1
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020641'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HandOverReportHistory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:HANDOVER.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
   tmp:RRCAccountNumber = Clarionet:Global.Param2
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:HANDOVER,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,han:RRCRecordNumberKey)
  BRW1.AddRange(han:RRCAccountNumber,tmp:RRCAccountNumber)
  BRW1.AddLocator(BRW9::Sort1:Locator)
  BRW9::Sort1:Locator.Init(,han:RecordNumber,1,BRW1)
  BRW1.AddSortOrder(,han:RecordNumberKey)
  BRW1.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,han:RecordNumber,1,BRW1)
  BIND('tmp:UserName',tmp:UserName)
  BRW1.AddField(han:RecordNumber,BRW1.Q.han:RecordNumber)
  BRW1.AddField(han:DateCreated,BRW1.Q.han:DateCreated)
  BRW1.AddField(han:TimeCreated,BRW1.Q.han:TimeCreated)
  BRW1.AddField(tmp:UserName,BRW1.Q.tmp:UserName)
  BRW1.AddField(han:RRCAccountNumber,BRW1.Q.han:RRCAccountNumber)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:HANDOVER.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Print
      ThisWindow.Update
      HandOverConfirmationReport(brw1.q.han:RecordNumber)
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020641'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020641'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020641'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF glo:WebJob = 1
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  tmp:UserName = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  PARENT.SetQueueRecord
  SELF.Q.tmp:UserName = tmp:UserName                  !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

OBFProcessing PROCEDURE                               !Generated from procedure template - Window

save_jobseng_id      USHORT,AUTO
save_users_id        USHORT,AUTO
save_joe_id          USHORT,AUTO
tmp:AwaitingProcessing BYTE(1)
tmp:Accepted         BYTE(2)
tmp:Rejected         BYTE(3)
tmp:Engineer         STRING(60)
BRW8::View:Browse    VIEW(JOBSOBF)
                       PROJECT(jof:RefNumber)
                       PROJECT(jof:IMEINumber)
                       PROJECT(jof:RecordNumber)
                       PROJECT(jof:Status)
                       JOIN(job:Ref_Number_Key,jof:RefNumber)
                         PROJECT(job:Model_Number)
                         PROJECT(job:Engineer)
                         PROJECT(job:Ref_Number)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jof:RefNumber          LIKE(jof:RefNumber)            !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
jof:IMEINumber         LIKE(jof:IMEINumber)           !List box control field - type derived from field
tmp:Engineer           LIKE(tmp:Engineer)             !List box control field - type derived from local data
job:Engineer           LIKE(job:Engineer)             !List box control field - type derived from field
jof:RecordNumber       LIKE(jof:RecordNumber)         !Primary key field - type derived from field
jof:Status             LIKE(jof:Status)               !Browse key field - type derived from field
job:Ref_Number         LIKE(job:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(JOBSOBF_ALIAS)
                       PROJECT(jofali:RefNumber)
                       PROJECT(jofali:IMEINumber)
                       PROJECT(jofali:RNumber)
                       PROJECT(jofali:RejectionReason)
                       PROJECT(jofali:Status)
                       JOIN(job_ali:Ref_Number_Key,jofali:RefNumber)
                         PROJECT(job_ali:Model_Number)
                         PROJECT(job_ali:Completed)
                         PROJECT(job_ali:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
jofali:RefNumber       LIKE(jofali:RefNumber)         !List box control field - type derived from field
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !List box control field - type derived from field
jofali:IMEINumber      LIKE(jofali:IMEINumber)        !List box control field - type derived from field
jofali:RNumber         LIKE(jofali:RNumber)           !List box control field - type derived from field
jofali:RejectionReason LIKE(jofali:RejectionReason)   !List box control field - type derived from field
job_ali:Completed      LIKE(job_ali:Completed)        !List box control field - type derived from field
jof:RejectionReason    LIKE(jof:RejectionReason)      !Browse hot field - type derived from field
jofali:Status          LIKE(jofali:Status)            !Browse key field - type derived from field
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK14::jof:Status          LIKE(jof:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,310),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Awaiting Processing'),AT(68,80),USE(?Prompt3),FONT(,12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Processed'),AT(344,80),USE(?Prompt3:2),FONT(,12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(68,94,264,242),USE(?Sheet1),SPREAD
                         TAB('By Job Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(72,110,64,10),USE(jof:RefNumber),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Like to JOBS RefNumber')
                         END
                         TAB('By I.M.E.I. Number'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(72,110,124,10),USE(jof:IMEINumber),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('I.M.E.I. Number')
                         END
                       END
                       LIST,AT(72,124,256,176),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('40R(2)|M~Job No~@s8@70L(2)|M~Model Number~@s30@70L(2)|M~I.M.E.I. Number~@s30@240' &|
   'L(2)|M~Engineer~@s60@0L(2)|M~Engineer~@s3@'),FROM(Queue:Browse)
                       LIST,AT(348,124,260,150),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('40R(2)|M~Job No~@s8@80L(2)|M~Model Number~@s30@80L(2)|M~I.M.E.I. Number~@s30@120' &|
   'L(2)|M~R Number~@s30@0L(2)|M~Rejection Reason~@s255@12L(2)|M~QA~@s3@'),FROM(Queue:Browse:1)
                       SHEET,AT(344,94,268,242),USE(?Sheet2),SPREAD
                         TAB('Accepted'),USE(?Tab3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Rejected'),USE(?Tab4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(348,280,184,52),USE(jofali:RejectionReason),SKIP,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('Rejection Reason')
                           BUTTON,AT(540,306),USE(?Button:QAUnit),TRN,FLAT,ICON('qaunitp.jpg')
                         END
                       END
                       ENTRY(@s8),AT(348,110,64,10),USE(jofali:RefNumber),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Like to JOBS RefNumber')
                       BUTTON,AT(240,304),USE(?Button:RejectOBF),TRN,FLAT,ICON('rejobfp.jpg')
                       BUTTON,AT(156,304),USE(?Button:Accept),TRN,FLAT,ICON('accobfp.jpg')
                       BUTTON,AT(72,304),USE(?Button:AllocateJob),TRN,FLAT,ICON('alljobp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('OBF Processing'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(544,368),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW8::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 2
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator EntryLocatorClass                !Default Locator
BRW10::Sort1:Locator EntryLocatorClass                !Conditional Locator - Choice(?Sheet2) = 2
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

OBFRejection        Routine
Data
local:RejectionReason       String(255)
Code
    If InsertOBFRejectionReason(local:RejectionReason) = 0
        Exit
    End ! If InsertOBFRejectionReason(local:RejectionReason) = 0

    If Clip(local:RejectionReason) = ''
        Exit
    End ! If Clip(local:RejectionReason) = ''

    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = jof:RefNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Found
        If JobInUse(job:Ref_Number,1)
            Exit
        End ! If JobInUse(job:Ref_Number,1)
    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Error
        Exit
    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
        Exit
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
        Exit
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

    ! Add Free Text Out Fault (DBH: 14/03/2007)
    If Access:JOBOUTFL.PrimeRecord() = Level:Benign
        joo:JobNumber = job:Ref_Number
        joo:FaultCode = 0
        joo:Description = Clip(local:RejectionReason)
        joo:Level = 0
        If Access:JOBOUTFL.TryInsert() = Level:Benign
            !Insert
        Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
            Access:JOBOUTFL.CancelAutoInc()
        End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
    End ! If Access.JOBOUTFL.PrimeRecord() = Level:Benign

    ! Add "OBF Rejected" Out Fault (DBH: 14/03/2007)
    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:MainFault = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULO.ClearKey(mfo:HideDescriptionKey)
        mfo:NotAvailable = 0
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        mfo:Description = 'OBF REJECTED'
        If Access:MANFAULO.TryFetch(mfo:HideDescriptionKey) = Level:Benign
            !Found
        Else ! If Access:MANFAULO.TryFetch(mfo:HideDescriptionKey) = Level:Benign
            !Error
        End ! If Access:MANFAULO.TryFetch(mfo:HideDescriptionKey) = Level:Benign

        If Access:JOBOUTFL.PrimeRecord() = Level:Benign
            joo:JobNumber = job:Ref_Number
            joo:FaultCode = mfo:Field
            joo:Description = mfo:Description
            joo:Level = mfo:ImportanceLevel
            If Access:JOBOUTFL.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                Access:JOBOUTFL.CancelAutoInc()
            End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
        End ! If Access.JOBOUTFL.PrimeRecord() = Level:Benign
    Else ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
    End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    ! Set the repair type (DBH: 14/03/2007)
    job:Repair_Type_Warranty = 'OBF REJECTED'

    ! Set the charge type (DBH: 14/03/2007)
    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'

    ! Reset "OBF Validated" flag (DBH: 14/03/2007)
    jobe:OBFValidated = 0
    jobe:OBFValidateDate = 0
    jobe:OBFValidateTime = 0

    GetStatus(605,1,'JOB') ! QA Check Required

    If Access:JOBS.Update()
        Exit
    End ! If Access:JOBS.Update()
    If Access:JOBSE.Update()
        Exit
    End ! If Access:JOBSE.Update()
    If Access:WEBJOB.Update()
        Exit
    End ! If Access:WEBJOB.Update()

    If AddToAudit(job:Ref_Number,'JOB','OBF REJECTED','REASON: ' & Clip(local:RejectionReason))

    End ! If AddToAudit(job:Ref_Number,'JOB','OBF REJECTED','REASON: ' & Clip(local:RejectionReason))
    jof:Status = 3
    jof:RNumber = ''
    jof:RejectionReason = Clip(local:RejectionReason)

    Save_USERS_ID = Access:USERS.SaveFile()
    Access:USERS.ClearKey(use:Password_Key)
    use:Password = glo:Password
    If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        !Found
        jof:UserCode = use:User_Code
    Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        !Error
    End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
    Access:USERS.RestoreFile(Save_USERS_ID)

    jof:HeadAccountNumber = wob:HeadAccountNumber
    jof:DateProcessed = Today()
    jof:TimeProcessed = Clock()
    Access:JOBSOBF.Update()
    Brw8.ResetSort(1)
    Brw10.ResetSort(1)


AcceptOBF       Routine
Data
local:RNumber       String(30)
Code
    If jof:RefNumber > 0
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = jof:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
            If JobInUse(job:Ref_Number,1)
                Exit
            End ! If JobInUse(job:Ref_Number,1)
        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Exit
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Exit
        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
            Exit
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign



        If InsertRNumber(local:RNumber) = 0
            Exit
        End ! If InsertRNumber(local:RNumber) = 0

        ! Add Free Text Out Fault (DBH: 13/03/2007)
        If Access:JOBOUTFL.PrimeRecord() = Level:Benign
            joo:JobNumber = job:Ref_Number
            joo:FaultCode = 0
            joo:Description = 'RNUMBER: ' & Clip(local:RNumber)
            joo:Level = 0
            If Access:JOBOUTFL.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                Access:JOBOUTFL.CancelAutoInc()
            End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
        End ! If Access.JOBOUTFL.PrimeRecord() = Level:Benign

        ! Add "OBF Credited" Out Fault (DBH: 13/03/2007)
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Found
            Access:MANFAULO.ClearKey(mfo:HideDescriptionKey)
            mfo:NotAvailable = 0
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Description = 'OBF CREDITED'
            If Access:MANFAULO.TryFetch(mfo:HideDescriptionKey) = Level:Benign
                !Found
            Else ! If Access:MANFAULO.TryFetch(mfo:HideDescriptionKey) = Level:Benign
                !Error
            End ! If Access:MANFAULO.TryFetch(mfo:HideDescriptionKey) = Level:Benign

            If Access:JOBOUTFL.PrimeRecord() = Level:Benign
                joo:JobNumber = job:Ref_Number
                joo:FaultCode = mfo:Field
                joo:Description = mfo:Description
                joo:Level = mfo:ImportanceLevel
                If Access:JOBOUTFL.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                    Access:JOBOUTFL.CancelAutoInc()
                End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
            End ! If Access.JOBOUTFL.PrimeRecord() = Level:Benign

        Else ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

        ! Set the repjair type (DBH: 13/03/o2007)
        job:Repair_Type_Warranty = 'OBF ACCEPTED'

        ! QA Job (DBH: 13/03/2007)
        job:QA_Passed = 'YES'
        job:Date_QA_Passed = Today()
        job:Time_QA_Passed = Clock()

        JobPricingRoutine(0)
        Access:JOBSE.Update()

        GetStatus(705,1,'JOB') ! Job Completed
        ForceDespatch
        GetStatus(710,1,'JOB') ! Job Competed (OBF)

        If job:Date_Completed = ''
            job:Date_Completed = Today()
            job:Time_Completed = Clock()
            wob:DateCompleted = job:Date_Completed
            wob:TimeCompleted = job:Time_Completed
            wob:Completed = 'YES'
            job:Completed = 'YES'
        End ! If job:Date_Completed = ''

        !Lookup current engineer and mark as escalated
        Save_joe_ID = Access:JOBSENG.SaveFile()
        Access:JOBSENG.ClearKey(joe:UserCodeKey)
        joe:JobNumber     = job:Ref_Number
        joe:UserCode      = job:Engineer
        joe:DateAllocated = Today()
        Set(joe:UserCodeKey,joe:UserCodeKey)
        Loop
            If Access:JOBSENG.PREVIOUS()
               Break
            End !If
            If joe:JobNumber     <> job:Ref_Number       |
            Or joe:UserCode      <> job:Engineer      |
            Or joe:DateAllocated > Today()       |
                Then Break.  ! End If
            joe:Status = 'COMPLETED'
            joe:StatusDate = Today()
            joe:StatusTime = Clock()
            Access:JOBSENG.Update()
            Break
        End !Loop
        Access:JOBSENG.RestoreFile(Save_joe_ID)

        !Check if this job is an EDI job
        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
        mod:Model_Number    = job:Model_Number
        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Found
            job:edi = PendingJob(mod:Manufacturer)
            job:Manufacturer    = mod:Manufacturer
        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

        !Mark the job's exchange unit as being available
        CompleteDoExchange(job:Exchange_Unit_Number)

        LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))

        If job:Warranty_Job = 'YES'
            If jobe:WarrantyClaimStatus = ''
                jobe:WarrantyClaimStatus = 'PENDING'
                Access:JOBSE.Update()
            End !If jobe:WarrantyClaimStatus = ''
        End !If job:Warranty_Job = 'YES'

        Access:JOBS.Update()
        Access:JOBSE.Update()
        Access:WEBJOB.Update()


        If AddToAudit(job:Ref_Number,'JOB','OBF ACCEPTED','R NUMBER: ' & Clip(local:RNumber))

        End ! If AddToAudit(job:Ref_Number,'JOB','OBF ACCEPTED','R NUMBER: ' & Clip(local:RNumber))
        jof:RNumber = local:RNumber
        jof:Status = 2
        jof:DateProcessed = Today()
        jof:TimeProcessed = Clock()
        Save_USERS_ID = Access:USERS.SaveFile()
        Access:USERS.ClearKey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            !Found
            jof:UserCode = use:User_Code
        Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            !Error
        End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign

        Access:USERS.RestoreFile(Save_USERS_ID)


        jof:HeadAccountNumber = wob:HeadAccountNumber
        jof:DateCompleted = job:Date_Completed
        jof:TimeCompleted = job:Time_Completed
        Access:JOBSOBF.Update()
        Brw8.ResetSort(1)
        Brw10.ResetSort(1)
    End ! If jof:RefNumber > 0
AllocateJob     Routine
Data
local:CurrentEngineer       String(3)
Code
    If jof:RefNumber > 0
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = jof:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
            If JobInUse(job:Ref_Number,1)
                Exit
            End ! If JobInUse(job:Ref_Number,1)

            local:CurrentEngineer = job:Engineer

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
                Exit
            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

            Access:USERS_ALIAS.ClearKey(use_ali:Password_Key)
            use_ali:Password = glo:Password
            If Access:USERS_ALIAS.TryFetch(use_ali:Password_Key) = Level:Benign
                !Found
            Else ! If Access:USERS_ALIAS.TryFetch(use_ali:Password_Key) = Level:Benign
                !Error
            End ! If Access:USERS_ALIAS.TryFetch(use_ali:Password_Key) = Level:Benign


            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = InsertEngineerPassword()
            If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Found
                If use:User_Code <> job:Engineer
                    If use:User_Type <> 'ENGINEER'
                        Case Missive('You are attempting to allocate an engineer that is not available for this site.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else ! If use:User_Type <> 'ENGINEER'
                        If jobe:HubRepair = False
                            Case Missive('Error! Cannot allocate engineer.'&|
                              '|The selected job is not at the ARC.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            Exit
                        End ! If jobe:HubRepair = False
                        Set(DEFAULT2)
                        Access:DEFAULT2.Next()
                        If de2:UserSkillLEvel
                            If use:SkillLevel < jobe:SkillLevel
                                Case Missive('The selected Engineer''s Skill level is NOT high enough to be allocated to this job.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Exit
                            End ! If use:SkillLevel < jobe:SkillLevel
                        End ! If de2:UserSkillLEvel


                        If job:Engineer = ''
                            If jobe:WebJob = 0
                                ! Do not send if ARC engineer being assign to RRC job (DBH: 11-05-2006)
                                Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                                jobe2:RefNumber = job:Ref_Number
                                If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                    ! Found
                                    If jobe2:SMSNotification
                                        !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','SMS',jobe2:SMSAlertNumber,'',0,'')
                                        SendSMSText('J','Y','N')                
                                        if glo:ErrorText[1:5] = 'ERROR' then

                                            miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )

                                        ELSE
                                            !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
                                            miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
                                        END 

                                    End ! If jobe2:SMSNotification

                                    If jobe2:EmailNotification
                                        AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','EMAIL','',jobe2:EmailAlertAddress,0,'')
                                    End ! If jobe2:EmailNotification
                                Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                    ! Error
                                End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level
                            End ! If jobe:WebJob = 0
                            If AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(use:User_Code),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))

                            End ! If AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
                            If Access:JOBSENG.PrimeRecord() = Level:Benign
                                joe:JobNumber     = job:Ref_Number
                                joe:UserCode      = use:User_Code
                                joe:DateAllocated = Today()
                                joe:TimeAllocated = Clock()
                                joe:AllocatedBy   = use_ali:User_Code
                                joe:Status        = 'ALLOCATED'
                                joe:StatusDate    = Today()
                                joe:StatusTime    = Clock()
                                joe:EngSkillLevel = use:SkillLevel
                                joe:JobSkillLevel = jobe:SkillLevel

                                If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:JOBSENG.TryInsert() = Level:Benign
                            End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                        Else ! If job:Engineer = ''
                            If AddToAudit(job:Ref_Number,'JOB','ENGINEER CHANGEDTO ' & Clip(use:User_Code), 'NEW ENGINEER: ' & CLip(use:User_Code) & |
                                                                                                            '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                                                                            '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(job:Engineer))
                            End ! If AddToAudit..
                            ! Escalate the current engineer (DBH: 13/03/2007)
                            Save_JOBSENG_ID = Access:JOBSENG.SaveFile()
                            Access:JOBSENG.Clearkey(joe:UserCodeKey)
                            joe:JobNumber = job:Ref_Number
                            joe:UserCode = job:Engineer
                            joe:DateAllocated = Today()
                            Set(joe:UserCodeKey,joe:UserCodeKey)
                            Loop ! Begin Loop
                                If Access:JOBSENG.Previous()
                                    Break
                                End ! If Access:JOBSENG.Next()
                                If joe:JobNumber <> job:Ref_Number
                                    Break
                                End ! If joe:JobNumber <> job:Ref_Number
                                If joe:UserCode <> job:Engineer
                                    Break
                                End ! If joe:UserCode <> job:Engineer
                                If joe:DateAllocated > Today()
                                    Break
                                End ! If joe:DateAllocated > Today()
                                If joe:Status = 'ALLOCATED'
                                    joe:Status = 'ESCALATED'
                                    joe:StatusDate = Today()
                                    joe:StatusTime = Clock()
                                    Access:JOBSENG.Update()
                                    Break
                                End ! If joe:Status = 'ALLOCATED'
                            End ! Loop
                            Access:JOBSENG.RestoreFile(Save_JOBSENG_ID)

                            If Access:JOBSENG.PrimeRecord() = Level:Benign
                                joe:JobNumber     = job:Ref_Number
                                joe:UserCode      = use:User_Code
                                joe:DateAllocated = Today()
                                joe:TimeAllocated = Clock()
                                joe:AllocatedBy   = use_ali:User_Code
                                joe:Status        = 'ALLOCATED'
                                joe:StatusDate    = Today()
                                joe:StatusTime    = Clock()
                                joe:EngSkillLevel = use:SkillLevel
                                joe:JobSkillLevel = jobe:SkillLevel

                                If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:JOBSENG.TryInsert() = Level:Benign
                            End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                        End ! If job:Engineer = ''
                        job:Engineer = use:User_Code
                        If Access:JOBS.Update() = Level:Benign
                            GetStatus(310,0,'JOB') ! Allocated To Engineer
                            ! Insert --- Was not saving the job after setting the status (DBH: 12/02/2009) #10667
                            If Access:JOBS.Update() = Level:Benign

                            End ! If Access:JOBS.Update() = Level:Benign
                            ! End --- (DBH: 12/02/2009) #10667
                        End ! If Access:JOBS.Update() = Level:Benign

                    End ! If use:User_Type <> 'ENGINEER'
                Else ! If use:User_Code <>job:Engineer
                End ! If use:User_Code <>job:Engineer
            Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                !Error
            End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign

        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        Brw8.ResetSort(1)
    End ! If jof:RefNumber > 0

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020676'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OBFProcessing')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:STAHEAD.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  Access:JOBSE2.UseFile
  Access:JOBSENG.UseFile
  Access:USERS.UseFile
  Access:JOBOUTFL.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:MANUFACT.UseFile
  Access:STATUS.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSTAGE.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:JOBSOBF,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:JOBSOBF_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,jof:StatusIMEINumberKey)
  BRW8.AddRange(jof:Status)
  BRW8.AddLocator(BRW8::Sort1:Locator)
  BRW8::Sort1:Locator.Init(?jof:IMEINumber,jof:IMEINumber,1,BRW8)
  BRW8.AddSortOrder(,jof:StatusRefNumberKey)
  BRW8.AddRange(jof:Status)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?jof:RefNumber,jof:RefNumber,1,BRW8)
  BIND('tmp:Engineer',tmp:Engineer)
  BRW8.AddField(jof:RefNumber,BRW8.Q.jof:RefNumber)
  BRW8.AddField(job:Model_Number,BRW8.Q.job:Model_Number)
  BRW8.AddField(jof:IMEINumber,BRW8.Q.jof:IMEINumber)
  BRW8.AddField(tmp:Engineer,BRW8.Q.tmp:Engineer)
  BRW8.AddField(job:Engineer,BRW8.Q.job:Engineer)
  BRW8.AddField(jof:RecordNumber,BRW8.Q.jof:RecordNumber)
  BRW8.AddField(jof:Status,BRW8.Q.jof:Status)
  BRW8.AddField(job:Ref_Number,BRW8.Q.job:Ref_Number)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,jofali:StatusRefNumberKey)
  BRW10.AddRange(jofali:Status,tmp:Rejected)
  BRW10.AddLocator(BRW10::Sort1:Locator)
  BRW10::Sort1:Locator.Init(?jofali:RefNumber,jofali:RefNumber,1,BRW10)
  BRW10.AddSortOrder(,jofali:StatusRefNumberKey)
  BRW10.AddRange(jofali:Status,tmp:Accepted)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(?jofali:RefNumber,jofali:RefNumber,1,BRW10)
  BRW10.AddField(jofali:RefNumber,BRW10.Q.jofali:RefNumber)
  BRW10.AddField(job_ali:Model_Number,BRW10.Q.job_ali:Model_Number)
  BRW10.AddField(jofali:IMEINumber,BRW10.Q.jofali:IMEINumber)
  BRW10.AddField(jofali:RNumber,BRW10.Q.jofali:RNumber)
  BRW10.AddField(jofali:RejectionReason,BRW10.Q.jofali:RejectionReason)
  BRW10.AddField(job_ali:Completed,BRW10.Q.job_ali:Completed)
  BRW10.AddField(jof:RejectionReason,BRW10.Q.jof:RejectionReason)
  BRW10.AddField(jofali:Status,BRW10.Q.jofali:Status)
  BRW10.AddField(job_ali:Ref_Number,BRW10.Q.job_ali:Ref_Number)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'By Job Number'
    ?Tab2{PROP:TEXT} = 'By I.M.E.I. Number'
    ?List{PROP:FORMAT} ='40R(2)|M~Job No~@s8@#1#70L(2)|M~Model Number~@s30@#2#70L(2)|M~I.M.E.I. Number~@s30@#3#240L(2)|M~Engineer~@s60@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab3{PROP:TEXT} = 'Accepted'
    ?Tab4{PROP:TEXT} = 'Rejected'
    ?List:2{PROP:FORMAT} ='40R(2)|M~Job No~@s8@#1#80L(2)|M~Model Number~@s30@#2#80L(2)|M~I.M.E.I. Number~@s30@#3#120L(2)|M~R Number~@s30@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:STAHEAD.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:QAUnit
      ThisWindow.Update
      Update_QA('PRI')
      ThisWindow.Reset
      BRW10.ResetSort(1)
    OF ?Button:RejectOBF
      ThisWindow.Update
      Brw8.UpdateViewRecord()
      If jof:RefNumber = 0
          Cycle
      End ! If jof:RefNumber = 0
      Do OBFRejection
    OF ?Button:Accept
      ThisWindow.Update
      Brw8.UpdateViewRecord()
      Do AcceptOBF
    OF ?Button:AllocateJob
      ThisWindow.Update
      Brw8.UpdateViewRecord()
      Do AllocateJob
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020676'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020676'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020676'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet1)
        OF 1
          ?List{PROP:FORMAT} ='40R(2)|M~Job No~@s8@#1#70L(2)|M~Model Number~@s30@#2#70L(2)|M~I.M.E.I. Number~@s30@#3#240L(2)|M~Engineer~@s60@#4#'
          ?Tab1{PROP:TEXT} = 'By Job Number'
        OF 2
          ?List{PROP:FORMAT} ='70L(2)|M~I.M.E.I. Number~@s30@#3#40R(2)|M~Job No~@s8@#1#70L(2)|M~Model Number~@s30@#2#'
          ?Tab2{PROP:TEXT} = 'By I.M.E.I. Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Sheet2
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet2)
        OF 1
          ?List:2{PROP:FORMAT} ='40R(2)|M~Job No~@s8@#1#80L(2)|M~Model Number~@s30@#2#80L(2)|M~I.M.E.I. Number~@s30@#3#120L(2)|M~R Number~@s30@#4#'
          ?Tab3{PROP:TEXT} = 'Accepted'
        OF 2
          ?List:2{PROP:FORMAT} ='40R(2)|M~Job No~@s8@#1#80L(2)|M~Model Number~@s30@#2#80L(2)|M~I.M.E.I. Number~@s30@#3#12L(2)|M~QA~@s3@#6#'
          ?Tab4{PROP:TEXT} = 'Rejected'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:AwaitingProcessing
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:AwaitingProcessing
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW8.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Engineer
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:Engineer = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      tmp:Engineer = '* NOT ALLOCATED *'
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  PARENT.SetQueueRecord


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

InsertRNumber PROCEDURE (f:RNumber)                   !Generated from procedure template - Window

tmp:RNumber          STRING(30)
tmp:Return           BYTE(0)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(296,258),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Please Enter The R Number'),AT(276,196),USE(?tmp:RNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(275,208,124,10),USE(tmp:RNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Please Enter The R Number'),TIP('Please Enter The R Number'),UPR
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('OBF Credited'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020677'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertRNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      tmp:Return = 0
    OF ?OK
      If CLip(tmp:RNumber) = ''
          Select(?tmp:RNumber)
          Cycle
      End ! If CLip(tmp:RNumber) = ''
      
      If Sub(tmp:RNumber,1,1) <> 'R'
          Case Missive('The R Number must begin with the letter "R".','ServiceBase 3g',|
                     'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:RNumber)
          Cycle
      End ! If Sub(tmp:RNumber,1,1) <> 'R'
      f:RNumber = tmp:RNumber
      tmp:Return = 1
      Post(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020677'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020677'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020677'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseOBFRejectionReasons PROCEDURE                   !Generated from procedure template - Window

tmp:BrowseType       BYTE(9)
BRW8::View:Browse    VIEW(OBFREASN)
                       PROJECT(obf:Description)
                       PROJECT(obf:RejectionText)
                       PROJECT(obf:Active)
                       PROJECT(obf:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
obf:Description        LIKE(obf:Description)          !List box control field - type derived from field
obf:RejectionText      LIKE(obf:RejectionText)        !List box control field - type derived from field
obf:Active             LIKE(obf:Active)               !List box control field - type derived from field
obf:RecordNumber       LIKE(obf:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Browse Type'),AT(168,86,152,24),USE(tmp:BrowseType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(173,95),USE(?tmp:BrowseType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('9')
                             RADIO('Active'),AT(212,95),USE(?tmp:BrowseType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Inactive'),AT(268,95),USE(?tmp:BrowseType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           ENTRY(@s60),AT(168,111,152,10),USE(obf:Description),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description')
                           LIST,AT(168,124,152,202),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('240L(2)|M~Description~@s60@0L|M~Rejection Text~L(2)@s255@0L|M~Active~L(2)@n1@'),FROM(Queue:Browse)
                           TEXT,AT(324,122,188,82),USE(obf:RejectionText),SKIP,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('Rejection Text')
                           BUTTON,AT(324,240),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(324,270),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                           BUTTON,AT(324,300),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse OBF Rejection Reason File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Upper(tmp:BrowseType) = 9
BRW8::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Upper(tmp:BrowseType) <> 9
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020679'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseOBFRejectionReasons')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:OBFREASN.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:OBFREASN,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,obf:DescriptionKey)
  BRW8.AddLocator(BRW8::Sort1:Locator)
  BRW8::Sort1:Locator.Init(?obf:Description,obf:Description,1,BRW8)
  BRW8.AddSortOrder(,obf:ActiveDescriptionKey)
  BRW8.AddRange(obf:Active,tmp:BrowseType)
  BRW8.AddLocator(BRW8::Sort2:Locator)
  BRW8::Sort2:Locator.Init(?obf:Description,obf:Description,1,BRW8)
  BRW8.AddSortOrder(,obf:DescriptionKey)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,obf:Description,1,BRW8)
  BRW8.AddField(obf:Description,BRW8.Q.obf:Description)
  BRW8.AddField(obf:RejectionText,BRW8.Q.obf:RejectionText)
  BRW8.AddField(obf:Active,BRW8.Q.obf:Active)
  BRW8.AddField(obf:RecordNumber,BRW8.Q.obf:RecordNumber)
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:OBFREASN.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateOBFRejectionReasons
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:BrowseType
      BRW8.ResetSort(1)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020679'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020679'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020679'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Upper(tmp:BrowseType) = 9
    RETURN SELF.SetSort(1,Force)
  ELSIF Upper(tmp:BrowseType) <> 9
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

