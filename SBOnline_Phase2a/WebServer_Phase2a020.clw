

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A020.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A019.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A021.INC'),ONCE        !Req'd for module callout resolution
                     END


TotalPrice           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
paid_chargeable_temp REAL                                  !
paid_warranty_temp   REAL                                  !
labour_rate_temp     REAL                                  !
parts_rate_temp      REAL                                  !
FilesOpened     BYTE(0)
  CODE
!How much has been paid?
! Parameters
! TotalPrice:VAT
! TotalPrice:Total
! TotalPrice:Balance
! TotalPrice:Type

    do openFiles
    p_web.SSV('TotalPrice:VAT',0)
    p_web.SSV('TotalPrice:Total',0)
    p_web.SSV('TotalPrice:Balance',0)


    Paid_Warranty_Temp = 0
    Paid_Chargeable_Temp = 0
    Access:JOBPAYMT_ALIAS.Clearkey(jpt_ali:All_Date_Key)
    jpt_ali:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(jpt_ali:All_Date_Key,jpt_ali:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT_ALIAS.Next()
            Break
        End ! If Access:JOBPAYMT_ALIAS.Next()
        If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number

        Paid_Chargeable_Temp += jpt_ali:Amount
    End ! Loop

    !Look up VAT Rates
    Labour_Rate_Temp = 0
    Parts_Rate_Temp = 0

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Case p_web.GSV('TotalPrice:Type')
    Of 'C' !Chargeable
        If p_web.GSV('BookingSite') = 'RRC'
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCCLabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCCPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCCLabourCost') + |
                        p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:courier_cost') + p_web.GSV('TotalPrice:VAT'))

            p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)

        Else !If glo:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost') + |
                    p_web.GSV('job:parts_cost') + p_web.GSV('job:courier_cost') + p_web.GSV('TotalPrice:VAT'))

            p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
        End !If glo:WebJob
    Of 'W' ! Warranty
        If p_web.GSV('BookingSite') = 'RRC'
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCWLabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCWPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCWLabourCost') + |
                        p_web.GSV('jobe:RRCWPartsCost') + p_web.GSV('job:courier_cost_warranty') + p_web.GSV('TotalPrice:VAT'))

        Else !If glo:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost_warranty') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost_warranty') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost_warranty') + |
                        p_web.GSV('job:parts_cost_warranty') + p_web.GSV('job:courier_cost_warranty') + p_web.GSV('TotalPrice:VAT'))

        End !If glo:WebJob
    Of 'E' !Estimate
        If p_web.GSV('jobe:WebJob') = 1
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCELabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCEPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCELabourCost') + |
                        p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:courier_cost_estimate') + p_web.GSV('TotalPrice:VAT'))

        Else !If p_web.GSV('jobe:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost_estimate') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost_estimate') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost_estimate') + |
                        p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate') + p_web.GSV('TotalPrice:VAT'))

        End !If p_web.GSV('jobe:WebJob
    Of 'I' ! Chargealbe Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:InvRRCCLabourCost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100))

                p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:InvRRCCLabourCost') + |
                        p_web.GSV('jobe:InvRRCCPartsCost') + p_web.GSV('job:invoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            Else !If glo:webJOb
                p_web.SSV('TotalPrice:VAT',p_web.GSV('job:invoice_labour_cost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('job:invoice_parts_cost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100))

                p_web.SSV('TotalPrice:Total',p_web.GSV('job:invoice_labour_cost') + |
                            p_web.GSV('job:invoice_parts_cost') + p_web.GSV('job:invoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            End !If glo:webJOb

        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    Of 'V' ! Warranty Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number_Warranty')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:InvRRCWLabourCost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('jobe:InvRRCWPartsCost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100))
                p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:InvRRCWLabourCost') + p_web.GSV('jobe:InvRRCWPartsCost') + |
                            p_web.GSV('job:Winvoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            Else !If glo:WebJob
                p_web.SSV('TotalPrice:VAT',p_web.GSV('job:Winvoice_labour_cost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('job:Winvoice_parts_cost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100))
                p_web.SSV('TotalPrice:Total',p_web.GSV('job:Winvoice_labour_cost') + p_web.GSV('job:Winvoice_parts_cost') + |
                            p_web.GSV('job:Winvoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            End !If glo:WebJob
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! Case f:Type

    p_web.SSV('TotalPrice:VAT',Round(p_web.GSV('TotalPrice:VAT'),.01))
    p_web.SSV('TotalPrice:Total',Round(p_web.GSV('TotalPrice:Total'),.01))
    p_web.SSV('TotalPrice:Balance',Round(p_web.GSV('TotalPrice:Balance'),.01))
    p_web.SSV('TotalPrice:Type','')

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.Open                               ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.UseFile                            ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     Access:JOBPAYMT_ALIAS.Close
     FilesOpened = False
  END
JobEstimateQuery     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEstimateReadyOption BYTE                                !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobEstimateQuery')
  loc:formname = 'JobEstimateQuery_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobEstimateQuery','')
    p_web._DivHeader('JobEstimateQuery',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobEstimateQuery',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobEstimateQuery_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locEstimateReadyOption',locEstimateReadyOption)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locEstimateReadyOption')
    locEstimateReadyOption = p_web.GetValue('locEstimateReadyOption')
    p_web.SetSessionValue('locEstimateReadyOption',locEstimateReadyOption)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobEstimateQuery_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locEstimateReadyOption = p_web.RestoreValue('locEstimateReadyOption')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobEstimateQuery_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobEstimateQuery_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobEstimateQuery_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobEstimateQuery" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobEstimateQuery" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobEstimateQuery" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Estimate Query') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Estimate Query',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobEstimateQuery">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobEstimateQuery" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobEstimateQuery')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobEstimateQuery')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobEstimateQuery'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobEstimateQuery')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimateQuery_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textQuery
      do Comment::textQuery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&100&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstimateReadyOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstimateReadyOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstimateReadyOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::textQuery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textQuery',p_web.GetValue('NewValue'))
    do Value::textQuery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textQuery  Routine
  p_web._DivHeader('JobEstimateQuery_' & p_web._nocolon('textQuery') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('The total value of this Estimate is less than the specified Estimate If Over value.<br><br>Do you want to continue and COMPLETE the repair, or create an ESTIMATE anyway?<br><br>',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textQuery  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimateQuery_' & p_web._nocolon('textQuery') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstimateReadyOption  Routine
  p_web._DivHeader('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Option')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEstimateReadyOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstimateReadyOption',p_web.GetValue('NewValue'))
    locEstimateReadyOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstimateReadyOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstimateReadyOption',p_web.GetValue('Value'))
    locEstimateReadyOption = p_web.GetValue('Value')
  End
  do Value::locEstimateReadyOption
  do SendAlert

Value::locEstimateReadyOption  Routine
  p_web._DivHeader('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locEstimateReadyOption
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEstimateReadyOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEstimateReadyOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEstimateReadyOption'',''jobestimatequery_locestimatereadyoption_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEstimateReadyOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEstimateReadyOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Create Repair') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEstimateReadyOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEstimateReadyOption'',''jobestimatequery_locestimatereadyoption_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEstimateReadyOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEstimateReadyOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Create Estimate') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEstimateReadyOption') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEstimateReadyOption'',''jobestimatequery_locestimatereadyoption_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEstimateReadyOption',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEstimateReadyOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Do Nothing') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_value')

Comment::locEstimateReadyOption  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobEstimateQuery_locEstimateReadyOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstimateReadyOption
      else
        do Value::locEstimateReadyOption
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobEstimateQuery',0)

PreCopy  Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobEstimateQuery:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobEstimateQuery:Primed',0)
  p_web.setsessionvalue('showtab_JobEstimateQuery',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobEstimateQuery_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('locEstimateReadyOption') = 0)
          loc:alert = 'Please select an option'
          loc:invalid = 'locEstimateReadyOption'
          exit
      end ! if (p_web.GSV('locEstimateReadyOption') = 0)
  p_web.DeleteSessionValue('JobEstimateQuery_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobEstimateQuery:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locEstimateReadyOption')
!!! <summary>
!!! Generated from procedure template - Report
!!! Estimate
!!! </summary>
Estimate PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
RejectRecord         LONG,AUTO                             !
tmp:Ref_Number       STRING(20)                            !
tmp:PrintedBy        STRING(255)                           !
save_epr_id          USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Company_Name_Temp STRING(30)                      !Delivery Company Name
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Number_Temp STRING(30)                  !Delivery Telephone Number
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(40)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(24)                            !
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
customer_name_temp   STRING(60)                            !
sub_total_temp       REAL                                  !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:InvoiceText      STRING(255)                           !Invoice Text
DefaultAddress       GROUP,PRE(address)                    !
Location             STRING(40)                            !
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(30)                            !
Name                 STRING(40)                            !Name
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
locRefNumber         LONG                                  !
locAccountNumber     STRING(30)                            !
locOrderNumber       STRING(30)                            !
locAuthorityNumber   STRING(20)                            !
locChargeType        STRING(20)                            !
locRepairType        STRING(20)                            !
locModelNumber       STRING(30)                            !
locManufacturer      STRING(30)                            !
locUnitType          STRING(30)                            !
locESN               STRING(20)                            !
locMSN               STRING(20)                            !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,6917,7521,1927),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,6313),USE(?unnamed)
                         STRING('Job No:'),AT(4896,365),USE(?String25),FONT(,12),TRN
                         STRING(@s16),AT(5729,365),USE(tmp:Ref_Number),FONT(,12,,FONT:bold),LEFT,TRN
                         STRING('Estimate Date:'),AT(4896,573),USE(?String58),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(5729,573),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),FONT(,8),TRN
                         STRING(@s60),AT(4063,1563),USE(customer_name_temp,,?customer_name_temp:2),FONT(,8),TRN
                         STRING(@s20),AT(1604,3240),USE(locOrderNumber),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4531,3240),USE(locChargeType),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3156,3240),USE(locAuthorityNumber),FONT(,8),TRN
                         STRING(@s20),AT(6042,3229),USE(locRepairType),FONT(,8),TRN
                         STRING(@s30),AT(156,3917,1000,156),USE(locModelNumber),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1604,3917,1323,156),USE(locManufacturer),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3156,3917,1396,156),USE(locUnitType),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4604,3917,1396,156),USE(locESN),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6083,3917),USE(locMSN),FONT(,8),TRN
                         STRING('Reported Fault: '),AT(156,4323),USE(?String64),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(1615,4323,5625,417),USE(jbn:Fault_Description),FONT(,8,,,CHARSET:ANSI),TRN
                         TEXT,AT(1615,5052,5625,885),USE(tmp:InvoiceText),FONT(,8,,,CHARSET:ANSI),TRN
                         STRING('Engineers Report:'),AT(156,5000),USE(?String88),FONT(,8,,FONT:bold),TRN
                         GROUP,AT(104,5938,7500,208),USE(?PartsHeadingGroup),FONT('Tahoma')
                           STRING('PARTS REQUIRED'),AT(156,6042),USE(?String79),FONT(,9,,FONT:bold),TRN
                           STRING('Qty'),AT(1510,6042),USE(?String80),FONT(,8,,FONT:bold),TRN
                           STRING('Part Number'),AT(1917,6042),USE(?String81),FONT(,8,,FONT:bold),TRN
                           STRING('Description'),AT(3677,6042),USE(?String82),FONT(,8,,FONT:bold),TRN
                           STRING('Unit Cost'),AT(5719,6042),USE(?UnitCost),FONT(,8,,FONT:bold),TRN
                           STRING('Line Cost'),AT(6677,6042),USE(?LineCost),FONT(,8,,FONT:bold),TRN
                           LINE,AT(1406,6197,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4063,2500),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2500),USE(Delivery_Telephone_Number_Temp),FONT(,8)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2240,2500),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(156,3240),USE(locAccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(4063,1719),USE(Delivery_Company_Name_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,1875,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2031),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2188),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2344),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),FONT(,8),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,198),USE(?DetailBand)
                         STRING(@n8b),AT(1156,0),USE(Quantity_temp),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,8),TRN
                         STRING(@s25),AT(3677,0),USE(Description_temp),FONT(,8),TRN
                         STRING(@n14.2b),AT(5208,0),USE(Cost_Temp),FONT(,8),RIGHT,TRN
                         STRING(@n14.2b),AT(6198,0),USE(line_cost_temp),FONT(,8),RIGHT,TRN
                       END
                       FOOTER,AT(385,9177,7521,2146),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,52),USE(?labour_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,52),USE(labour_temp),FONT(,8),RIGHT,TRN
                         STRING(@s20),AT(2865,313,1771,156),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING(@s20),AT(2875,83,1760,198),USE(Bar_Code_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING('Carriage:'),AT(4844,365),USE(?carriage_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,208),USE(parts_temp),FONT(,8),RIGHT,TRN
                         STRING('V.A.T.'),AT(4844,677),USE(?vat_String),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,365),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                         STRING('Total:'),AT(4844,833),USE(?total_string),FONT(,8,,FONT:bold),TRN
                         STRING('<128>'),AT(6146,833,156,208),USE(?Euro),FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         LINE,AT(6354,833,1000,0),USE(?line),COLOR(COLOR:Black)
                         LINE,AT(6354,521,1000,0),USE(?line:2),COLOR(COLOR:Black)
                         STRING(@n14.2b),AT(6198,833),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Sub Total:'),AT(4844,521),USE(?String92),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,521),USE(sub_total_temp),FONT(,8),RIGHT,TRN
                         STRING(@s20),AT(2865,521,1760,198),USE(Bar_Code2_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING(@n14.2b),AT(6250,677),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING('Parts:'),AT(4844,208),USE(?parts_string),FONT(,8),TRN
                         TEXT,AT(104,1094,7365,958),USE(stt:Text),FONT(,8)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,11198),USE(?Image1)
                         STRING('ESTIMATE'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),FONT(,16,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),FONT(,9,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1500),USE(?String24),FONT(,9,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),FONT(,9,,FONT:bold),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),FONT(,9,,FONT:bold),TRN
                         STRING('AUTHORISATION DETAILS'),AT(156,8479),USE(?String73),FONT(,9,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),FONT(,9,,FONT:bold),TRN
                         STRING('Estimate Accepted'),AT(208,8750),USE(?String94),FONT(,8,,FONT:bold),TRN
                         BOX,AT(1406,8750,156,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Estimate Refused'),AT(208,8958),USE(?String95),FONT(,8,,FONT:bold),TRN
                         BOX,AT(1406,8958,156,156),USE(?Box2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name (Capitals)'),AT(208,9219),USE(?String96),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1198,9323,1354,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Signature'),AT(208,9479),USE(?String97),FONT(,8,,FONT:bold),TRN
                         LINE,AT(833,9583,1719,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Model'),AT(156,3844),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1635,3844),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4563,3844),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),FONT(,9,,FONT:bold),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Estimate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:VATCODE.Open                                      ! File VATCODE used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAUPA.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:CHARTYPE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locRefNumber = p_web.GSV('job:Ref_Number')
  
  set(DEFAULTS)
  access:DEFAULTS.next()
  
  set(DEFAULT2)
  access:DEFAULT2.next()
  
  
  locAccountNumber = p_web.GSV('job:Account_Number')
  locOrderNumber = p_web.GSV('job:Order_Number')
  locAuthorityNumber = p_web.GSV('job:Authority_Number')
  locChargeType = p_web.GSV('job:Charge_Type')
  locRepairType = p_web.GSV('job:Repair_Type')
  locModelNumber = p_web.GSV('job:Model_Number')
  locManufacturer = p_web.GSV('job:Manufacturer')
  locUnitType = p_web.GSV('job:Unit_Type')
  locESN = p_web.GSV('job:ESN')
  locMSN = p_web.GSV('job:MSN')
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('Estimate',ProgressWindow)                  ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locRefNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Estimate',ProgressWindow)               ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Delivery_Company_Name_Temp = p_web.GSV('job:Company_Name_Delivery')
      delivery_address1_temp     = p_web.GSV('job:address_line1_delivery')
      delivery_address2_temp     = p_web.GSV('job:address_line2_delivery')
      If p_web.GSV('job:address_line3_delivery')   = ''
          delivery_address3_temp = p_web.GSV('job:postcode_delivery')
          delivery_address4_temp = ''
      Else
          delivery_address3_temp  = p_web.GSV('job:address_line3_delivery')
          delivery_address4_temp  = p_web.GSV('job:postcode_delivery')
      End
      Delivery_Telephone_Number_Temp = p_web.GSV('job:Telephone_Delivery')
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = p_web.GSV('job:account_number')
      access:subtracc.fetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
          Else!If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = sub:address_line1
              invoice_address2_temp     = sub:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
              invoice_company_name_temp   = sub:company_name
              invoice_telephone_number_temp   = sub:telephone_number
              invoice_fax_number_temp = sub:fax_number
  
  
          End!If sub:invoice_customer_address = 'YES'
  
      else!if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
  
          Else!If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = tra:address_line1
              invoice_address2_temp     = tra:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = tra:address_line3
                  invoice_address4_temp  = tra:postcode
              End
              invoice_company_name_temp   = tra:company_name
              invoice_telephone_number_temp   = tra:telephone_number
              invoice_fax_number_temp = tra:fax_number
  
          End!If tra:invoice_customer_address = 'YES'
      End!!if tra:use_sub_accounts = 'YES'
  
  
      if p_web.GSV('job:title') = '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') = '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      else
          customer_name_temp = ''
      end
  
      access:stantext.clearkey(stt:description_key)
      stt:description = 'ESTIMATE'
      access:stantext.fetch(stt:description_key)
  
  
      courier_cost_temp = p_web.GSV('job:courier_cost_estimate')
  
      !If booked at RRC, use RRC costs for estimate
      If p_web.GSV('jobe:WebJob') = 1
          Labour_Temp = p_web.GSV('jobe:RRCELabourCost')
          Parts_Temp  = p_web.GSV('jobe:RRCEPartsCost')
          Sub_Total_Temp = p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate')
      Else !p_web.GSV('jobe:WebJob')
          labour_temp = p_web.GSV('job:labour_cost_estimate')
          parts_temp  = p_web.GSV('job:parts_cost_estimate')
          sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate')
      End !p_web.GSV('jobe:WebJob')
  
  
        ! Inserting (DBH 03/12/2007) # 8218 - Use the ARC details, if it's an RRC-ARC estimate job
        sentToHub(p_web)
        If p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob') = 1
            ! ARC Estimate, use ARC Details
            If p_web.GSV('BookingSite') <> 'RRC'
                ! Use ARC Details
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        Customer_Name_Temp = ''
                        Invoice_Company_Name_Temp = tra:Company_Name
                        Invoice_Address1_Temp = tra:Address_Line1
                        Invoice_Address2_Temp = tra:Address_Line2
                        Invoice_Address3_Temp = tra:Address_Line3
                        Invoice_Address4_Temp = tra:Postcode
                        Invoice_Telephone_Number_Temp = tra:Telephone_Number
                        Invoice_Fax_Number_Temp = tra:Fax_Number
  
                        Delivery_Company_Name_Temp = tra:Company_Name
                        Delivery_Address1_Temp = tra:Address_Line1
                        Delivery_Address2_Temp = tra:Address_Line2
                        Delivery_Address3_Temp = tra:Address_Line3
                        Delivery_Address4_Temp = tra:Postcode
                        Delivery_Telephone_Number_Temp = tra:Telephone_Number
  
                        labour_temp = p_web.GSV('job:labour_cost_estimate')
                        parts_temp  = p_web.GSV('job:parts_cost_estimate')
                        sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate')+ p_web.GSV('job:courier_cost_estimate')
  
                    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            End ! If glo:WebJob
        End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
        ! End (DBH 03/12/2007) #8218
  
      p_web.SSV('TotalPrice:Type','E')
      totalPrice(p_web)
      vat_temp = p_web.GSV('TotalPrice:VAT')
      total_temp = p_web.GSV('TotalPrice:Total')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If ~man:UseInvTextForFaults
              tmp:InvoiceText = jbn:Invoice_Text
          Else !If ~man:UseInvTextForFaults
              tmp:InvoiceText = ''
              Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
              joo:JobNumber = p_web.GSV('job:Ref_Number')
              Set(joo:JobNumberKey,joo:JobNumberKey)
              Loop
                  If Access:JOBOUTFL.NEXT()
                     Break
                  End !If
                  If joo:JobNumber <> p_web.GSV('job:Ref_Number')      |
                      Then Break.  ! End If
                  ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
                  If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                      Cycle
                  End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                  ! End (DBH 16/10/2006) #8059
                  If tmp:InvoiceText = ''
                      tmp:InvoiceText = Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  Else !If tmp:Invoice_Text = ''
                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  End !If tmp:Invoice_Text = ''
              End !Loop
  
              Access:MANFAUPA.ClearKey(map:MainFaultKey)
              map:Manufacturer = p_web.GSV('job:Manufacturer')
              map:MainFault    = 1
              If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Found
                  Access:MANFAULT.ClearKey(maf:MainFaultKey)
                  maf:Manufacturer = p_web.GSV('job:Manufacturer')
                  maf:MainFault    = 1
                  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Found
                      Save_epr_ID = Access:ESTPARTS.SaveFile()
                      Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                      epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                      Set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop
                          If Access:ESTPARTS.NEXT()
                             Break
                          End !If
                          If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                              Then Break.  ! End If
                          Access:MANFAULO.ClearKey(mfo:Field_Key)
                          mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfo:Field_Number = maf:Field_Number
  
                          Case map:Field_Number
                              Of 1
                                  mfo:Field   = epr:Fault_Code1
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code1) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 2
                                  mfo:Field   = epr:Fault_Code2
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code2) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 3
                                  mfo:Field   = epr:Fault_Code3
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code3) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 4
                                  mfo:Field   = epr:Fault_Code4
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code4) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 5
                                  mfo:Field   = epr:Fault_Code5
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code5) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 6
                                  mfo:Field   = epr:Fault_Code6
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code6) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 7
                                  mfo:Field   = epr:Fault_Code7
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code7) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 8
                                  mfo:Field   = epr:Fault_Code8
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code8) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 9
                                  mfo:Field   = epr:Fault_Code9
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code9) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 10
                                  mfo:Field   = epr:Fault_Code10
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code10) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 11
                                  mfo:Field   = epr:Fault_Code11
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code11) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 12
                                  mfo:Field   = epr:Fault_Code12
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code12) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          End !Case map:Field_Number
                      End !Loop
  
                  Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
              Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
  
          End !If ~man:UseInvTextForFaults
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
      End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      job_Number_Temp = locRefNumber
      esn_Temp = locESN
      Bar_Code_Temp = '*' & clip(job_Number_Temp) & '*'
      Bar_Code2_Temp = '*' & clip(esn_Temp) & '*'
  
      job_Number_temp = 'Job No: ' & clip(job_Number_Temp)
      esn_Temp = 'IMEI No: ' & clip(esn_Temp)
  !*********CHANGE LICENSE ADDRESS*********
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  tmp:Ref_Number = p_web.GSV('Job:Ref_Number') & '-' & |
      tra:BranchIdentification & p_web.GSV('wob:JobNumber')
      
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
      jbn:Fault_Description = p_web.GSV('jbn:Fault_Description')
  
  
      count# = 0
      FixedCharge# = 0
      Access:CHARTYPE.ClearKey(cha:Warranty_Key)
      cha:Warranty    = 'NO'
      cha:Charge_Type = p_web.GSV('job:Charge_Type')
      If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Found
          If glo:WebJob
              If cha:Zero_Parts
                  FixedCharge# = 1
              End !If cha:Fixed_Charge
          Else !If glo:WebJob
              If cha:Zero_Parts_ARC
                  FixedCharge# = 1
              End !If cha:Fixed_Charge_ARC
          End !If glo:WebJob
      Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
  
      If FixedCharge#
          SetTarget(Report)
          ?Cost_Temp{prop:Hide} = 1
          ?Line_Cost_Temp{prop:Hide} = 1
          ?UnitCost{prop:Hide} = 1
          ?LineCost{prop:Hide} = 1
          SetTarget()
      End !FixedCharge#
  
      access:estparts.clearkey(epr:part_number_key)
      epr:ref_number  = p_web.GSV('job:ref_number')
      set(epr:part_number_key,epr:part_number_key)
      loop
          if access:estparts.next()
             break
          end !if
          if epr:ref_number  <> p_web.GSV('job:ref_number')      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          count# += 1
          part_number_temp = epr:part_number
          description_temp = epr:description
          quantity_temp = epr:quantity
          cost_temp = epr:sale_cost
          line_cost_temp = epr:quantity * epr:sale_cost
          Print(rpt:detail)
  
      end !loop
  
      If count# = 0
          SetTarget(Report)
          ?PartsHeadingGroup{Prop:Hide} = 1
          Part_Number_Temp = ''
          !Print Blank Line
          Print(Rpt:Detail)
          SetTarget()
      End!If count# = 0
  
  
      If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:estimate_accepted') <> 'YES' and p_web.GSV('job:estimate_rejected') <> 'YES'
  !        p_web.SSV('GetStatus:Type','JOB')
  !        p_web.SSV('GetStatus:StatusNumber',520)
  !        getStatus(p_web)
  !
  !        Access:JOBS.Clearkey(job:ref_Number_Key)
  !        job:ref_Number    = p_web.GSV('job:Ref_Number')
  !        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(JOBS)
  !            access:JOBS.tryUpdate()
  !        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Error
  !        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !
  !
  !        Access:WEBJOB.Clearkey(wob:refNumberKey)
  !        wob:refNumber    = p_web.GSV('job:Ref_Number')
  !        if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(WEBJOB)
  !            access:WEBJOB.tryUpdate()
  !        else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Error
  !        end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !
  !        p_web.SSV('AddToAudit:Type','JOB')
  !        p_web.SSV('AddToAudit:Notes','ESTIMATE VALUE: ' & Format(Total_Temp,@n14.2))
  !        p_web.SSV('AddToAudit:Action','ESTIMATE SENT')
  !        addToAudit(p_web)
  
      End!If p_web.GSV('job:estimate = 'YES' And p_web.GSV('job:estimate_accepted <> 'YES' and p_web.GSV('job:estimate_refused <> 'YES'
  
  
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:DETAIL)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','Estimate','Estimate','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

ViewEstimateParts    PROCEDURE  (NetWebServerWorker p_web)
tmp:partStatus       STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(ESTPARTS)
                      Project(epr:Record_Number)
                      Project(epr:Part_Number)
                      Project(epr:Description)
                      Project(epr:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('ViewEstimateParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('ViewEstimateParts:NoForm')
      loc:NoForm = p_web.GetValue('ViewEstimateParts:NoForm')
      loc:FormName = p_web.GetValue('ViewEstimateParts:FormName')
    else
      loc:FormName = 'ViewEstimateParts_frm'
    End
    p_web.SSV('ViewEstimateParts:NoForm',loc:NoForm)
    p_web.SSV('ViewEstimateParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('ViewEstimateParts:NoForm')
    loc:FormName = p_web.GSV('ViewEstimateParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('ViewEstimateParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('ViewEstimateParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ESTPARTS,epr:record_number_key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'EPR:PART_NUMBER') then p_web.SetValue('ViewEstimateParts_sort','1')
    ElsIf (loc:vorder = 'EPR:DESCRIPTION') then p_web.SetValue('ViewEstimateParts_sort','2')
    ElsIf (loc:vorder = 'EPR:QUANTITY') then p_web.SetValue('ViewEstimateParts_sort','3')
    ElsIf (loc:vorder = 'TMP:PARTSTATUS') then p_web.SetValue('ViewEstimateParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('ViewEstimateParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('ViewEstimateParts:LookupFrom','LookupFrom')
    p_web.StoreValue('ViewEstimateParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('ViewEstimateParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('ViewEstimateParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  !Override browse buttons
  p_web.site.insertButton.class = 'BrowseButtonsSmall'
  p_web.site.changeButton.class = 'BrowseButtonsSmall'
  p_web.site.deletebButton.class = 'BrowseButtonsSmall'
  p_web.site.insertButton.image = ''
  p_web.site.changeButton.image = ''
  p_web.site.deletebButton.image = ''
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('ViewEstimateParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('ViewEstimateParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(epr:Part_Number)','-UPPER(epr:Part_Number)')
    Loc:LocateField = 'epr:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(epr:Description)','-UPPER(epr:Description)')
    Loc:LocateField = 'epr:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'epr:Quantity','-epr:Quantity')
    Loc:LocateField = 'epr:Quantity'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:partStatus','-tmp:partStatus')
    Loc:LocateField = 'tmp:partStatus'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('epr:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('ViewEstimateParts_LocatorPic','@s30')
  Of upper('epr:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('ViewEstimateParts_LocatorPic','@s30')
  Of upper('epr:Quantity')
    loc:SortHeader = p_web.Translate('Qty')
    p_web.SetSessionValue('ViewEstimateParts_LocatorPic','@n8')
  Of upper('tmp:partStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('ViewEstimateParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('ViewEstimateParts:LookupFrom')
  End!Else
  loc:formaction = 'ViewEstimateParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="ViewEstimateParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="ViewEstimateParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('ViewEstimateParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ESTPARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="epr:record_number_key"></input><13,10>'
  end
  If p_web.Translate('Estimate Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Estimate Parts',0)&'</span>'&CRLF
  End
  If clip('Estimate Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewEstimateParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2ViewEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="ViewEstimateParts.locate(''Locator2ViewEstimateParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'ViewEstimateParts.cl(''ViewEstimateParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="ViewEstimateParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="ViewEstimateParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','ViewEstimateParts','Part Number',,,,'20%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('20%')&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','ViewEstimateParts','Description',,,,'40%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('40%')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','ViewEstimateParts','Qty',,,,'10%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('10%')&'">'&p_web.Translate('Qty')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','ViewEstimateParts','Status',,,,'20%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('20%')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('epr:record_number',lower(Thisview{prop:order}),1,1) = 0 !and ESTPARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'epr:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('epr:Record_Number'),p_web.GetValue('epr:Record_Number'),p_web.GetSessionValue('epr:Record_Number'))
      loc:FilterWas = 'epr:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewEstimateParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('ViewEstimateParts_Filter')
    p_web.SetSessionValue('ViewEstimateParts_FirstValue','')
    p_web.SetSessionValue('ViewEstimateParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ESTPARTS,epr:record_number_key,loc:PageRows,'ViewEstimateParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ESTPARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ESTPARTS,loc:firstvalue)
              Reset(ThisView,ESTPARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ESTPARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ESTPARTS,loc:lastvalue)
            Reset(ThisView,ESTPARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:PartStatus = getPartStatus('E')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(epr:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Estimate Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'ViewEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'ViewEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'ViewEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'ViewEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewEstimateParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('ViewEstimateParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('ViewEstimateParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1ViewEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="ViewEstimateParts.locate(''Locator1ViewEstimateParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'ViewEstimateParts.cl(''ViewEstimateParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('ViewEstimateParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('ViewEstimateParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'ViewEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'ViewEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'ViewEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'ViewEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = epr:Record_Number
    p_web._thisrow = p_web._nocolon('epr:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('ViewEstimateParts:LookupField')) = epr:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((epr:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="ViewEstimateParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ESTPARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ESTPARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ESTPARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ESTPARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','epr:Record_Number',clip(loc:field),,loc:checked,,,'onclick="ViewEstimateParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','epr:Record_Number',clip(loc:field),,'checked',,,'onclick="ViewEstimateParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::epr:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('40%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::epr:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('10%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::epr:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:partStatus = 'Requested'
              packet = clip(packet) & '<td class="'&clip('GreenRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Picked'
              packet = clip(packet) & '<td class="'&clip('BlueRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'On Order'
              packet = clip(packet) & '<td class="'&clip('PurpleRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&clip('PinkRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&clip('OrangeRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:partStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="ViewEstimateParts.omv(this);" onMouseOut="ViewEstimateParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var ViewEstimateParts=new browseTable(''ViewEstimateParts'','''&clip(loc:formname)&''','''&p_web._jsok('epr:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('epr:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'ViewEstimateParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'ViewEstimateParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2ViewEstimateParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1ViewEstimateParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1ViewEstimateParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2ViewEstimateParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ESTPARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ESTPARTS)
  Bind(epr:Record)
  Clear(epr:Record)
  NetWebSetSessionPics(p_web,ESTPARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('epr:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::epr:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('epr:Part_Number_'&epr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(epr:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::epr:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('epr:Description_'&epr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(epr:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::epr:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('epr:Quantity_'&epr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(epr:Quantity,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:partStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:partStatus = 'Requested'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&epr:Record_Number,'GreenRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Picked'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&epr:Record_Number,'BlueRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'On Order'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&epr:Record_Number,'PurpleRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Picking'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&epr:Record_Number,'PinkRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Return'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&epr:Record_Number,'OrangeRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&epr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:partStatus,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = epr:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('epr:Record_Number',epr:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('epr:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('epr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('epr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormEstimateParts    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:Location         STRING(30)                            !
tmp:ShelfLocation    STRING(30)                            !
tmp:SecondLocation   STRING(30)                            !
tmp:PurchaseCost     REAL                                  !
tmp:OutWarrantyCost  REAL                                  !
tmp:OutWarrantyMarkup REAL                                 !
tmp:ARCPart          BYTE                                  !
tmp:FaultCodesChecked BYTE                                 !
tmp:FaultCodes2      STRING(30)                            !
tmp:FaultCodes4      STRING(30)                            !
tmp:FaultCodes5      STRING(30)                            !
tmp:FaultCodes6      STRING(30)                            !
tmp:FaultCodes7      STRING(30)                            !
tmp:FaultCodes8      STRING(30)                            !
tmp:FaultCodes9      STRING(30)                            !
tmp:FaultCodes10     STRING(30)                            !
tmp:FaultCodes11     STRING(30)                            !
tmp:FaultCodes12     STRING(30)                            !
tmp:FaultCodes3      STRING(30)                            !
tmp:FaultCodes1      STRING(30)                            !
tmp:CreateOrder      BYTE                                  !
tmp:UnallocatePart   BYTE                                  !
locUserCode          STRING(3)                             !
locPartUsedOnRepair  BYTE                                  !
FilesOpened     Long
ESTPARTS::State  USHORT
STOMODEL::State  USHORT
STOCK::State  USHORT
SUPPLIER::State  USHORT
LOCATION::State  USHORT
CHARTYPE::State  USHORT
MANFAUPA::State  USHORT
MANFAULT::State  USHORT
MANFPALO::State  USHORT
DEFAULTS::State  USHORT
PARTS_ALIAS::State  USHORT
STOHIST::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       Class
AfterFaultCodeLookup Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEstimateParts')
  loc:formname = 'FormEstimateParts_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEstimateParts','')
    p_web._DivHeader('FormEstimateParts',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
buildFaultCodes    Routine
data
locFoundFault      Byte(0)
code
    ! Clear Variables
    if (p_web.GSV('FormEstimateParts:FirstTime') = 0)
        loop x# = 1 To 12
            p_web.SSV('Hide:PartFaultCode' & x#,1)
            p_web.SSV('Req:PartFaultCode' & x#,0)
            p_web.SSV('ReadOnly:PartFaultCode' & x#,0)
            p_web.SSV('Prompt:PartFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:PartFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:PartFaultCode' & x#,0)
            p_web.SSV('Lookup:PartFaultCode' & x#,0)
            p_web.SSV('Comment:PartFaultCode' & x#,'')
        end ! loop x# = 1 To 12
        p_web.SSV('Hide:FaultCodesChecked',1)
    end ! if (p_web.GSV('FormEstimateParts:FirstTime') = 0)

    locMainFaultOnly# = 0
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
            !Main Fault Only
            locMainFaultOnly# = 1
        end ! if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:ScreenOrder    = 0
    set(map:ScreenOrderKey,map:ScreenOrderKey)
    loop
        if (Access:MANFAUPA.Next())
            Break
        end ! if (Access:MANFAUPA.Next())
        if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))

        if (map:ScreenOrder = 0)
            cycle
        end ! if (map:ScreenOrder = 0)

        if (locMainFaultOnly# = 1)
            if (map:MainFault = 0)
                cycle
            end ! if (map:MainFault = 0)
        end ! if (locMainFaultOnly# = 1)

        p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,0)
        p_web.SSV('Prompt:PartFaultCode' & map:ScreenOrder,map:Field_Name)

        ! #11659 Don't force estimate fault codes (Bryan: 23/08/2010)
!        if (map:Compulsory = 'YES')
!            if (p_web.GSV('epr:Adjustment') = 'YES')
!                if (map:CompulsoryForAdjustment)
!                    p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
!                    p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
!                end ! if (map:CompulsoryForAdjustment)
!            else !if (p_web.GSV('epr:Adjustment') = 'YES')
!                p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
!                p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
!            end ! if (p_web.GSV('epr:Adjustment') = 'YES')
!        else ! if (map:Compulsory = 'YES')
!            p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,0)
!        end ! if (map:Compulsory = 'YES')

        if (map:MainFault)
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
                case maf:Field_Type
                of 'DATE'
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(maf:DateType))
                    ! Date Lookup Required
                of 'STRING'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & maf:LengthTo)
                    end !~ if (maf:RestrictLength)

                    ! Lookup Required
                of 'NUMBER'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & maf:LengthTo)
                    else !end !~ if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                    end !~ if (maf:RestrictLength)
                end ! case maf:Field_Type

                if (maf:Lookup = 'YES')
                    p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)
                end ! if (map:Lookup = 'YES')
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
        else ! if (map:MainFault)
            case map:Field_Type
            of 'DATE'
                p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(map:DateType))
                p_web.SSV('ShowDate:PartFaultCode' & map:ScreenOrder,1)
                ! Date Lookup Required
            of 'STRING'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & map:LengthTo)
                end !~ if (maf:RestrictLength)

                ! Lookup Required
            of 'NUMBER'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & map:LengthTo)
                else !end !~ if (maf:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                end !~ if (maf:RestrictLength)
            end ! case maf:Field_Type
        end !if (map:MainFault)

        if (map:Lookup = 'YES')
            p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)

        end ! if (map:Lookup = 'YES')

        if (map:NotAvailable = 1)
            if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,1)
            else ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('ReadOnly:PartFaultCode' & map:ScreenOrder,1)
            end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
        end ! if (map:NotAvailable = 1)

        if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')
            if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('job:Fault_Code' & map:CopyJobFaultCode))
            else ! if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('wob:FaultCode' & map:CopyJobFaultCode))
            end ! if (map:ScreenOrder < 13)
        end ! if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')

        if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Manufacturer    = p_web.GSV('job:Manufacturer')
            stm:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Found
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode1)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode2)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode3)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode4)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode5)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode6)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode7)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode8)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode9)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode10)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode11)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode12)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            else ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
        locFoundFault = 1
    end ! loop

    !Check if the part main fault is set to assign value to anouther fault code

    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
        mfp:Field_Number    = map:Field_Number
        mfp:Field    = p_web.GSV('tmp:FaultCode' & map:ScreenOrder)
        if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Found
            if (mfp:SetPartFaultCode)
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer    = p_web.GSV('job:Manufacturer')
                map:Field_Number    = mfp:SelectPartFaultCode
                if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                       p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                       p_web.SSV('tmp:FaultCode' & map:ScreenOrder,mfp:PartFaultCodeValue)
                    end ! if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                else ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
            end ! if (mfp:SetPartFaultCode)
        else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Error
        end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    if (locFoundFault)
        p_web.SSV('Hide:FaultCodesChecked',0)
    end ! if (locFoundFault)
deleteSessionValues     Routine
    p_web.deleteSessionValue('adjustment')
    p_web.deleteSessionValue('FormEstimateParts:FirstTime')
    p_web.deleteSessionValue('locOrderRequired')
    p_web.deleteSessionValue('locOutFaultCode')

    p_web.deleteSessionValue('ReadOnly:OutWarrantyMarkup')
    p_web.deleteSessionValue('ReadOnly:PurchaseCost')
    p_web.deleteSessionValue('ReadOnly:OutWarrantyCost')
    p_web.deleteSessionValue('ReadOnly:Quantity')

    p_web.deleteSessionValue('Parts:ViewOnly')

    p_web.deleteSessionValue('Show:UnallocatePart')
enableDisableCosts    Routine
    p_web.SSV('tmp:FixedPrice','')
    if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',0)
    else ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',1)
    end ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)

    if (p_web.GSV('job:Invoice_Number') = 0)
        if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
            p_web.SSV('tmp:InWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:InWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:InWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
        if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
            p_web.SSV('tmp:OutWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:OutWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:OutWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
    end ! if (p_web.GSV('job:Invoice_Number') = 0)

    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type    = p_web.GSV('job:Charge_Type')
    if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Found
        if (cha:Zero_Parts_ARC)
           if (p_web.GSV('tmp:ARCPart') = 1)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts_ARC)

        if (cha:Zero_Parts = 'YES')
           if (p_web.GSV('tmp:ARCPart') = 0)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts = 'YES')
    else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Error
    end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
lookupLocation    Routine
    if (p_web.GSV('epr:Part_Ref_Number') <> '')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Found
            p_web.SSV('tmp:Location',sto:Location)
            p_web.SSV('tmp:ShelfLocation',sto:Shelf_Location)
            p_web.SSV('tmp:SecondLocation',sto:Second_Location)
        else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Error
            p_web.SSV('tmp:Location','')
            p_web.SSV('tmp:ShelfLocation','')
            p_web.SSV('tmp:SecondLocation','')
        end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    else ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
        p_web.SSV('tmp:Location','')
        p_web.SSV('tmp:ShelfLocation','')
        p_web.SSV('tmp:SecondLocation','')
    end ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
lookupMainFault  Routine
    p_web.SSV('locOutFaultCode','')
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFAULT.Clearkey(maf:MainFaultKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:MainFault    = 1
        if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Found

            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCodes' & map:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Found
                p_web.SSV('locOutFaultCode','Out Fault Code: ' & mfo:Description)
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

        else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)

    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
showCosts      Routine
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
        if (sto:Location <> p_web.GSV('ARC:SiteLocation'))
            p_web.SSV('tmp:ARCPart',0)
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND RRC PART'))
                    p_web.SSV('Parts:ViewOnly',1)
                end ! if (SecurityCheckFailed(p_web.GSV('BookingUser'),'AMEND RRC PART'))
            end ! if (p_web.GSV('BookingSite') <> 'RRC')
        else ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
            p_web.SSV('tmp:ARCPart',1)
            if (p_web.GSV('BookingSite') <> 'ARC')
                p_web.SSV('Parts:ViewOnly',1)
            end ! if (p_web.GSV('BookingSite') <> 'ARC')
        end ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    if (p_web.GSV('tmp:ARCPart') = 1)
        p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:AveragePurchaseCost'))
        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('epr:Purchase_Cost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('epr:Sale_Cost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('epr:InWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('epr:OutWarrantyMarkup'))
    else ! if (tmp:ARCPart)
        if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:RRCAveragePurchaseCost'))
        else ! if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:RRCPurchaseCost'))
        end ! if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)

        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('epr:RRCPurchaseCost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('epr:RRCSaleCost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('epr:RRCInWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('epr:RRCOutWarrantyMarkup'))
    end !if (tmp:ARCPart)
UpdateComments    Routine
    loop x# = 1 to 12
        if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
            cycle
        end ! if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        map:ScreenOrder    = x#
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
            if (map:MainFault)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf:MainFault    = 1
                if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Found
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:PartFaultCode' & x#,mfo:Description)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:PartFaultCode' & x#,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            else ! if (map:MainFault)
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
                mfp:Field_Number    = map:Field_Number
                mfp:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Found
                    p_web.SSV('Comment:PartFaultCode' & x#,mfp:Description)
                else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Error
                    p_web.SSV('Comment:PartFaultCode' & x#,'')
                end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            end ! if (map:MainFault)
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
    end ! loop x# = 1 to 12


updatePartDetails      routine
! Update Part Details
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = stm:Ref_Number
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)

    Access:LOCATION.Clearkey(loc:Location_Key)
    loc:Location    = sto:Location
    if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    p_web.SSV('epr:Description',stm:Description)
    p_web.SSV('epr:Part_Ref_Number',stm:Ref_Number)
    p_web.SSV('epr:Supplier',sto:Supplier)
    p_web.SSV('epr:Purchase_Cost',sto:Purchase_Cost)
    p_web.SSV('epr:Sale_Cost',sto:Sale_Cost)
    p_web.SSV('epr:Retail_Cost',sto:Retail_Cost)
    p_web.SSV('epr:InWarrantyMarkup',sto:PurchaseMarkup)
    p_web.SSV('epr:OutWarrantyMarkup',sto:Percentage_Mark_Up)

    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('epr:RRCAveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('epr:PurchaseCost',sto:Purchase_Cost)
        p_web.SSV('epr:RRCSaleCost',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts = 'YES')
                p_web.SSV('epr:RRCSaleCost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('epr:RRCInWarrantyMarkup',sto:PurchaseMarkUp)
        p_web.SSV('epr:RRCOutWarrantyMarkup',sto:Percentage_Mark_Up)
        p_web.SSV('epr:Purchase_Cost',epr:RRCPurchaseCost)
        p_web.SSV('epr:Sale_Cost',epr:RRCSaleCost)
        p_web.SSV('epr:AveragePurchaseCost',epr:RRCAveragePurchaseCost)
    end ! if (p_web.GSV('BookingSite') = 'RRC')

    if (p_web.GSV('BookingSite') = 'ARC')
        p_web.SSV('epr:AveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('epr:Purchase_Cost',sto:Purchase_Cost)
        p_web.SSV('epr:Sale_Code',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts_ARC)
                p_web.SSV('epr:Sale_Cost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('epr:RRCPurchaseCost',0)
        p_web.SSV('epr:RRCSaleCost',0)

        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('epr:RRCAveragePurchaseCost',epr:Sale_Cost)
            p_web.SSV('epr:RRCPurchaseCost',VodacomClass.Markup(p_web.GSV('epr:RRCPurchaseCost'),|
                                                p_web.GSV('epr:RRCAveragePurchaseCost'),|
                                                InWarrantyMarkup(p_web.GSV('job:Manufacturer'),|
                                                sto:Location)))
            p_web.SSV('epr:RRCSaleCost',VodacomClass.Markup(p_web.GSV('epr:RRCSaleCost'),|
                                            p_web.GSV('epr:RRCAveragePurchaseCost'),|
                                            loc:OutWarrantyMarkup))
            p_web.SSV('epr:RRCInWarrantyMarkup',p_web.GSV('epr:InWarrantyMarkup'))
            p_web.SSV('epr:RRCOutWarrantyMarkup',p_web.GSV('epr:OutWarrantyMarkup'))
        end ! if (p_web.GSV('jobe:WebJob') = 1)
    end ! if (p_web.GSV('BookingSite') = 'ARC')

    if (sto:Assign_Fault_Codes = 'YES')
        p_web.SSV('tmp:FaultCode1',stm:FaultCode1)
        p_web.SSV('tmp:FaultCode2',stm:FaultCode2)
        p_web.SSV('tmp:FaultCode3',stm:FaultCode3)
        p_web.SSV('tmp:FaultCode4',stm:FaultCode4)
        p_web.SSV('tmp:FaultCode5',stm:FaultCode5)
        p_web.SSV('tmp:FaultCode6',stm:FaultCode6)
        p_web.SSV('tmp:FaultCode7',stm:FaultCode7)
        p_web.SSV('tmp:FaultCode8',stm:FaultCode8)
        p_web.SSV('tmp:FaultCode9',stm:FaultCode9)
        p_web.SSV('tmp:FaultCode10',stm:FaultCode10)
        p_web.SSV('tmp:FaultCode11',stm:FaultCode11)
        p_web.SSV('tmp:FaultCode12',stm:FaultCode12)
    end ! if (sto:Assign_Fault_Codes = 'YES')

    p_web.SSV('epr:Part_Ref_Number',sto:Ref_Number)
    p_web.SSV('locOrderRequired',0)

    do ShowCosts
    do lookupLocation
    do enableDisableCosts
OpenFiles  ROUTINE
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(PARTS_ALIAS)
  p_web._OpenFile(STOHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(PARTS_ALIAS)
  p_Web._CloseFile(STOHIST)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !Initialize
      do buildFaultCodes
  
      p_web.SSV('Comment:OutWarrantyMarkup','')
      p_web.SSV('Comment:PartNumber','Required')
  p_web.SetValue('FormEstimateParts_form:inited_',1)
  p_web.SetValue('UpdateFile','ESTPARTS')
  p_web.SetValue('UpdateKey','epr:record_number_key')
  p_web.SetValue('IDField','epr:Record_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormEstimateParts:Primed') = 1
    p_web._deleteFile(ESTPARTS)
    p_web.SetSessionValue('FormEstimateParts:Primed',0)
  End
      do deleteSessionValues
  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','ESTPARTS')
  p_web.SetValue('UpdateKey','epr:record_number_key')
  If p_web.IfExistsValue('epr:Order_Number')
    p_web.SetPicture('epr:Order_Number','@n08b')
  End
  p_web.SetSessionPicture('epr:Order_Number','@n08b')
  If p_web.IfExistsValue('epr:Date_Ordered')
    p_web.SetPicture('epr:Date_Ordered','@d6b')
  End
  p_web.SetSessionPicture('epr:Date_Ordered','@d6b')
  If p_web.IfExistsValue('epr:Date_Received')
    p_web.SetPicture('epr:Date_Received','@d6b')
  End
  p_web.SetSessionPicture('epr:Date_Received','@d6b')
  If p_web.IfExistsValue('epr:Part_Number')
    p_web.SetPicture('epr:Part_Number','@s30')
  End
  p_web.SetSessionPicture('epr:Part_Number','@s30')
  If p_web.IfExistsValue('epr:Description')
    p_web.SetPicture('epr:Description','@s30')
  End
  p_web.SetSessionPicture('epr:Description','@s30')
  If p_web.IfExistsValue('epr:Despatch_Note_Number')
    p_web.SetPicture('epr:Despatch_Note_Number','@s30')
  End
  p_web.SetSessionPicture('epr:Despatch_Note_Number','@s30')
  If p_web.IfExistsValue('epr:Quantity')
    p_web.SetPicture('epr:Quantity','@n4')
  End
  p_web.SetSessionPicture('epr:Quantity','@n4')
  If p_web.IfExistsValue('tmp:PurchaseCost')
    p_web.SetPicture('tmp:PurchaseCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:PurchaseCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyCost')
    p_web.SetPicture('tmp:OutWarrantyCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    p_web.SetPicture('tmp:OutWarrantyMarkup','@n3')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyMarkup','@n3')
  If p_web.IfExistsValue('epr:Supplier')
    p_web.SetPicture('epr:Supplier','@s30')
  End
  p_web.SetSessionPicture('epr:Supplier','@s30')
  If p_web.IfExistsValue('tmp:FaultCodes1')
    p_web.SetPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCodes2')
    p_web.SetPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCodes3')
    p_web.SetPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCodes4')
    p_web.SetPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCodes5')
    p_web.SetPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCodes6')
    p_web.SetPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCodes7')
    p_web.SetPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCodes8')
    p_web.SetPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCodes9')
    p_web.SetPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCodes10')
    p_web.SetPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCodes11')
    p_web.SetPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCodes12')
    p_web.SetPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
AfterLookup Routine
  loc:TabNumber = -1
  If loc:act = ChangeRecord
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'epr:Part_Number'
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOMODEL)
      ! After Lookup
      ! After Lookup Assignments
      do updatePartDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Description')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  Of 'epr:Supplier'
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUPPLIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Exclude_From_Order')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  End
  If p_web.GSV('locOrderRequired') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:Location',tmp:Location)
  p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tmp:Location')
    tmp:Location = p_web.GetValue('tmp:Location')
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  End
  if p_web.IfExistsValue('tmp:SecondLocation')
    tmp:SecondLocation = p_web.GetValue('tmp:SecondLocation')
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  End
  if p_web.IfExistsValue('tmp:ShelfLocation')
    tmp:ShelfLocation = p_web.GetValue('tmp:ShelfLocation')
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  End
  if p_web.IfExistsValue('tmp:PurchaseCost')
    tmp:PurchaseCost = p_web.GetValue('tmp:PurchaseCost')
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost')
    tmp:OutWarrantyCost = p_web.GetValue('tmp:OutWarrantyCost')
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    tmp:OutWarrantyMarkup = p_web.GetValue('tmp:OutWarrantyMarkup')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  End
  if p_web.IfExistsValue('tmp:UnallocatePart')
    tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  End
  if p_web.IfExistsValue('tmp:CreateOrder')
    tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked')
    tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  End
  if p_web.IfExistsValue('tmp:FaultCodes1')
    tmp:FaultCodes1 = p_web.GetValue('tmp:FaultCodes1')
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  End
  if p_web.IfExistsValue('tmp:FaultCodes2')
    tmp:FaultCodes2 = p_web.GetValue('tmp:FaultCodes2')
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  End
  if p_web.IfExistsValue('tmp:FaultCodes3')
    tmp:FaultCodes3 = p_web.GetValue('tmp:FaultCodes3')
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  End
  if p_web.IfExistsValue('tmp:FaultCodes4')
    tmp:FaultCodes4 = p_web.GetValue('tmp:FaultCodes4')
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  End
  if p_web.IfExistsValue('tmp:FaultCodes5')
    tmp:FaultCodes5 = p_web.GetValue('tmp:FaultCodes5')
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  End
  if p_web.IfExistsValue('tmp:FaultCodes6')
    tmp:FaultCodes6 = p_web.GetValue('tmp:FaultCodes6')
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  End
  if p_web.IfExistsValue('tmp:FaultCodes7')
    tmp:FaultCodes7 = p_web.GetValue('tmp:FaultCodes7')
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  End
  if p_web.IfExistsValue('tmp:FaultCodes8')
    tmp:FaultCodes8 = p_web.GetValue('tmp:FaultCodes8')
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  End
  if p_web.IfExistsValue('tmp:FaultCodes9')
    tmp:FaultCodes9 = p_web.GetValue('tmp:FaultCodes9')
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  End
  if p_web.IfExistsValue('tmp:FaultCodes10')
    tmp:FaultCodes10 = p_web.GetValue('tmp:FaultCodes10')
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  End
  if p_web.IfExistsValue('tmp:FaultCodes11')
    tmp:FaultCodes11 = p_web.GetValue('tmp:FaultCodes11')
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  End
  if p_web.IfExistsValue('tmp:FaultCodes12')
    tmp:FaultCodes12 = p_web.GetValue('tmp:FaultCodes12')
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  End
  if p_web.IfExistsValue('locPartUsedOnRepair')
    locPartUsedOnRepair = p_web.GetValue('locPartUsedOnRepair')
    p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormEstimateParts_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('FormEstimateParts:FirstTime') = 0)
          !Write Fault Codes
          p_web.SSV('epr:Fault_Code1',epr:Fault_Code1)
          p_web.SSV('epr:Fault_Code2',epr:Fault_Code2)
          p_web.SSV('epr:Fault_Code3',epr:Fault_Code3)
          p_web.SSV('epr:Fault_Code4',epr:Fault_Code4)
          p_web.SSV('epr:Fault_Code5',epr:Fault_Code5)
          p_web.SSV('epr:Fault_Code6',epr:Fault_Code6)
          p_web.SSV('epr:Fault_Code7',epr:Fault_Code7)
          p_web.SSV('epr:Fault_Code8',epr:Fault_Code8)
          p_web.SSV('epr:Fault_Code9',epr:Fault_Code9)
          p_web.SSV('epr:Fault_Code10',epr:Fault_Code10)
          p_web.SSV('epr:Fault_Code11',epr:Fault_Code11)
          p_web.SSV('epr:Fault_Code12',epr:Fault_Code12)
  
  
          Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
          map:Manufacturer    = p_web.GSV('job:Manufacturer')
          map:ScreenOrder    = 0
          set(map:ScreenOrderKey,map:ScreenOrderKey)
          loop
              if (Access:MANFAUPA.Next())
                  Break
              end ! if (Access:MANFAUPA.Next())
              if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (map:ScreenOrder    = 0)
                  cycle
              end ! if (map:ScreenOrder    <> 0)
  
              p_web.SSV('tmp:FaultCodes' & map:ScreenOrder,p_web.GSV('epr:Fault_Code' & map:Field_Number))
          end ! loop
  
  !        loop x# = 1 To 12
  !            p_web.SSV('tmp:FaultCodes' & x#,p_web.GSV('epr:Fault_Code' & x#))
  !            linePrint('tmp:FaultCode' & x# & ' - ' & p_web.GSV('tmp:FaultCode' & x#),'c:\log.log')
  !        end ! loop x# = 1 To 12
          p_web.SSV('FormEstimateParts:FirstTime',1)
          !p_web.SSV('locOrderRequired',0)
  
          ! Save For Later
          p_web.SSV('Save:Quantity',epr:Quantity)
      end ! if (p_web.GSV('FormEstimateParts:FirstTime',0))
      do updateComments
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:Location = p_web.RestoreValue('tmp:Location')
 tmp:SecondLocation = p_web.RestoreValue('tmp:SecondLocation')
 tmp:ShelfLocation = p_web.RestoreValue('tmp:ShelfLocation')
 tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
 tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
 tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
 tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
 tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
 tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
 tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
 tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
 tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
 tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
 tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
 tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
 tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
 tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
 tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
 tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
 tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
 tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
 locPartUsedOnRepair = p_web.RestoreValue('locPartUsedOnRepair')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEstimateParts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEstimateParts_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEstimateParts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Parts:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="ESTPARTS__FileAction" value="'&p_web.getSessionValue('ESTPARTS:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="ESTPARTS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="ESTPARTS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="epr:record_number_key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormEstimateParts" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormEstimateParts" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormEstimateParts" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','epr:Record_Number',p_web._jsok(p_web.getSessionValue('epr:Record_Number'))) & '<13,10>'
  If p_web.Translate('Insert / Amend Estimate Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Insert / Amend Estimate Parts',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormEstimateParts">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormEstimateParts" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormEstimateParts')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If loc:act = ChangeRecord
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Status') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        If p_web.GSV('locOrderRequired') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Order Required') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select When Part Will Be Used') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormEstimateParts')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormEstimateParts'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STOMODEL'
            p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Description')
    End
    If upper(p_web.getvalue('LookupFile'))='SUPPLIER'
        If p_web.GSV('Hide:PartFaultCode1') <> 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes1')
        End
    End
  Else
    If False
    ElsIf loc:act = ChangeRecord
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Part_Number')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If loc:act = ChangeRecord
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('locOrderRequired') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormEstimateParts')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if loc:act = ChangeRecord
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('locOrderRequired') = 1
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If loc:act = ChangeRecord
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Status') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Order_Number
      do Value::epr:Order_Number
      do Comment::epr:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Date_Ordered
      do Value::epr:Date_Ordered
      do Comment::epr:Date_Ordered
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Date_Received
      do Value::epr:Date_Received
      do Comment::epr:Date_Received
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locOutFaultCode
      do Comment::locOutFaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Despatch_Note_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Despatch_Note_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Despatch_Note_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Quantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Quantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Quantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      !Set Width
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:SecondLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:SecondLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:SecondLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShelfLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:PurchaseCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:PurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:PurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OutWarrantyCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OutWarrantyCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OutWarrantyCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OutWarrantyMarkup
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OutWarrantyMarkup
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OutWarrantyMarkup
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FixedPrice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FixedPrice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FixedPrice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Supplier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Exclude_From_Order
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Exclude_From_Order
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Exclude_From_Order
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:PartAllocated
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:PartAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:PartAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Show:UnallocatePart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:UnallocatePart
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:UnallocatePart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:UnallocatePart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('locOrderRequired') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Order Required') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3" class="'&clip('RedBold')&'">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3" class="'&clip('RedBold')&'">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:OrderRequired
      do Comment::text:OrderRequired
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:OrderRequired2
      do Comment::text:OrderRequired2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CreateOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCodesChecked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCodesChecked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCodesChecked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode1') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCodes1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCodes1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCodes1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode2') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode3') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode4') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode5') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode6') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode7') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode8') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode9') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode10') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode11') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode12') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select When Part Will Be Used') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:UsedOnRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:UsedOnRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::epr:Order_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Order_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Order_Number',p_web.GetValue('NewValue'))
    epr:Order_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = epr:Order_Number
    do Value::epr:Order_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@n08b'))
    epr:Order_Number = p_web.Dformat(p_web.GetValue('Value'),'@n08b') !
  End

Value::epr:Order_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- epr:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('epr:Order_Number'),'@n08b')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::epr:Order_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Date_Ordered  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Ordered')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Date_Ordered  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Date_Ordered',p_web.GetValue('NewValue'))
    epr:Date_Ordered = p_web.GetValue('NewValue') !FieldType= DATE Field = epr:Date_Ordered
    do Value::epr:Date_Ordered
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Date_Ordered',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    epr:Date_Ordered = p_web.GetValue('Value')
  End

Value::epr:Date_Ordered  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- epr:Date_Ordered
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('epr:Date_Ordered'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::epr:Date_Ordered  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Date_Received  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Received')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Date_Received  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Date_Received',p_web.GetValue('NewValue'))
    epr:Date_Received = p_web.GetValue('NewValue') !FieldType= DATE Field = epr:Date_Received
    do Value::epr:Date_Received
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Date_Received',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    epr:Date_Received = p_web.GetValue('Value')
  End

Value::epr:Date_Received  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- epr:Date_Received
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('epr:Date_Received'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::epr:Date_Received  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locOutFaultCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOutFaultCode',p_web.GetValue('NewValue'))
    do Value::locOutFaultCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locOutFaultCode  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('locOutFaultCode') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locOutFaultCode'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locOutFaultCode  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('locOutFaultCode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Part_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Part_Number',p_web.GetValue('NewValue'))
    epr:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Part_Number
    do Value::epr:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Part_Number = p_web.GetValue('Value')
  End
  If epr:Part_Number = ''
    loc:Invalid = 'epr:Part_Number'
    loc:alert = p_web.translate('Part Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    epr:Part_Number = Upper(epr:Part_Number)
    p_web.SetSessionValue('epr:Part_Number',epr:Part_Number)
      if (p_web.GSV('job:Engineer') <> '')
          locUserCode = p_web.GSV('job:Engineer')
      else !
          locUserCode = p_web.GSV('BookingUSerCOde')
      end !if (p_web.GSV('job:Engineer') <> '')
  
      case validFreeTextPart('C',locUserCode,p_web.GSV('job:Manufacturer'),|
                                  p_web.GSV('job:Model_Number'),p_web.GSV('epr:part_Number'))
      of 0
          p_web.SSV('Comment:PartNumber','')
  
          Access:STOMODEL.Clearkey(stm:Location_Part_Number_Key)
          stm:Model_Number    = p_web.GSV('job:Model_Number')
          stm:Location    = p_web.GSV('BookingSiteLocation')
          stm:Part_Number    = p_web.GSV('epr:Part_Number')
          if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Found
          else ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
  
          do updatePartDetails
      of 1
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Invalid User')
      of 2
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Not In Stock Location')
      of 3
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Suspended')
      of 4
          p_web.SSV('Comment:PartNumber','Warning! Cannot Find The Selected Part In Stock')
      of 5
          p_web.SSV('epr:part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Access Level Is Not High Enough For Part')
  
      end ! case
  
  p_Web.SetValue('lookupfield','epr:Part_Number')
  do AfterLookup
  do Value::epr:Part_Number
  do SendAlert
  do Comment::epr:Part_Number
  do Comment::epr:Part_Number
  do Value::epr:Description  !1
  do Value::epr:Supplier  !1
  do Value::tmp:PurchaseCost  !1
  do Value::tmp:OutWarrantyCost  !1
  do Value::tmp:OutWarrantyMarkup  !1
  do Prompt::tmp:SecondLocation
  do Value::tmp:SecondLocation  !1
  do Prompt::tmp:ShelfLocation
  do Value::tmp:ShelfLocation  !1
  do Prompt::tmp:Location
  do Value::tmp:Location  !1

Value::epr:Part_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Part_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('epr:Part_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If epr:Part_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Part_Number'',''formestimateparts_epr:part_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('epr:Part_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','epr:Part_Number',p_web.GetSessionValue('epr:Part_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseModelStock')&'?LookupField=epr:Part_Number&Tab=4&ForeignField=stm:Part_Number&_sort=stm:Description&Refresh=sort&LookupFrom=FormEstimateParts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_value')

Comment::epr:Part_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartNumber'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_comment')

Prompt::epr:Description  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Description',p_web.GetValue('NewValue'))
    epr:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Description
    do Value::epr:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Description = p_web.GetValue('Value')
  End
  If epr:Description = ''
    loc:Invalid = 'epr:Description'
    loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    epr:Description = Upper(epr:Description)
    p_web.SetSessionValue('epr:Description',epr:Description)
  do Value::epr:Description
  do SendAlert

Value::epr:Description  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Description
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('epr:Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If epr:Description = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Description'',''formestimateparts_epr:description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Description',p_web.GetSessionValueFormat('epr:Description'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_value')

Comment::epr:Description  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Despatch_Note_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch Note Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Despatch_Note_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Despatch_Note_Number',p_web.GetValue('NewValue'))
    epr:Despatch_Note_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Despatch_Note_Number
    do Value::epr:Despatch_Note_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Despatch_Note_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Despatch_Note_Number = p_web.GetValue('Value')
  End
    epr:Despatch_Note_Number = Upper(epr:Despatch_Note_Number)
    p_web.SetSessionValue('epr:Despatch_Note_Number',epr:Despatch_Note_Number)
  do Value::epr:Despatch_Note_Number
  do SendAlert

Value::epr:Despatch_Note_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Despatch_Note_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('epr:Despatch_Note_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Despatch_Note_Number'',''formestimateparts_epr:despatch_note_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Despatch_Note_Number',p_web.GetSessionValueFormat('epr:Despatch_Note_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_value')

Comment::epr:Despatch_Note_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Quantity  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Quantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Quantity',p_web.GetValue('NewValue'))
    epr:Quantity = p_web.GetValue('NewValue') !FieldType= LONG Field = epr:Quantity
    do Value::epr:Quantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Quantity',p_web.dFormat(p_web.GetValue('Value'),'@n4'))
    epr:Quantity = p_web.Dformat(p_web.GetValue('Value'),'@n4') !
  End
  If epr:Quantity = ''
    loc:Invalid = 'epr:Quantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    epr:Quantity = Upper(epr:Quantity)
    p_web.SetSessionValue('epr:Quantity',epr:Quantity)
  do Value::epr:Quantity
  do SendAlert

Value::epr:Quantity  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Quantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('epr:Quantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If epr:Quantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Quantity'',''formestimateparts_epr:quantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Quantity',p_web.GetSessionValue('epr:Quantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n4',loc:javascript,,'Quantity') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_value')

Comment::epr:Quantity  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Location  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_prompt',Choose(p_web.GSV('tmp:Location') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Location')
  If p_web.GSV('tmp:Location') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_prompt')

Validate::tmp:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Location',p_web.GetValue('NewValue'))
    tmp:Location = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Location',p_web.GetValue('Value'))
    tmp:Location = p_web.GetValue('Value')
  End

Value::tmp:Location  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_value',Choose(p_web.GSV('tmp:Location') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:Location') = '')
  ! --- DISPLAY --- tmp:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_value')

Comment::tmp:Location  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_comment',Choose(p_web.GSV('tmp:Location') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:Location') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:SecondLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Second Location')
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt')

Validate::tmp:SecondLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:SecondLocation',p_web.GetValue('NewValue'))
    tmp:SecondLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:SecondLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:SecondLocation',p_web.GetValue('Value'))
    tmp:SecondLocation = p_web.GetValue('Value')
  End

Value::tmp:SecondLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_value',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:SecondLocation') = '')
  ! --- DISPLAY --- tmp:SecondLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:SecondLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_value')

Comment::tmp:SecondLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_comment',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ShelfLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Shelf Location')
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt')

Validate::tmp:ShelfLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShelfLocation',p_web.GetValue('NewValue'))
    tmp:ShelfLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShelfLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShelfLocation',p_web.GetValue('Value'))
    tmp:ShelfLocation = p_web.GetValue('Value')
  End

Value::tmp:ShelfLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ShelfLocation') = '')
  ! --- DISPLAY --- tmp:ShelfLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:ShelfLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value')

Comment::tmp:ShelfLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_comment',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:PurchaseCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Purchase Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:PurchaseCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:PurchaseCost',p_web.GetValue('NewValue'))
    tmp:PurchaseCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:PurchaseCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:PurchaseCost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    tmp:PurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End
  do Value::tmp:PurchaseCost
  do SendAlert

Value::tmp:PurchaseCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:PurchaseCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:PurchaseCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:PurchaseCost'',''formestimateparts_tmp:purchasecost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:PurchaseCost',p_web.GetSessionValue('tmp:PurchaseCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value')

Comment::tmp:PurchaseCost  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OutWarrantyCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Out Of Warranty Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OutWarrantyCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OutWarrantyCost',p_web.GetValue('NewValue'))
    tmp:OutWarrantyCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OutWarrantyCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OutWarrantyCost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    tmp:OutWarrantyCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End
  do enableDisableCosts
  do Value::tmp:OutWarrantyCost
  do SendAlert
  do Value::tmp:OutWarrantyMarkup  !1

Value::tmp:OutWarrantyCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:OutWarrantyCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:OutWarrantyCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyCost'',''formestimateparts_tmp:outwarrantycost_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyCost')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyCost',p_web.GetSessionValue('tmp:OutWarrantyCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value')

Comment::tmp:OutWarrantyCost  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OutWarrantyMarkup  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Markup')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OutWarrantyMarkup  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',p_web.GetValue('NewValue'))
    tmp:OutWarrantyMarkup = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OutWarrantyMarkup
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',p_web.dFormat(p_web.GetValue('Value'),'@n3'))
    tmp:OutWarrantyMarkup = p_web.Dformat(p_web.GetValue('Value'),'@n3') !
  End
  !Out Warranty Markup
  markup# = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  if (p_web.GSV('tmp:OutWarrantyMarkup') > markup#)
      p_web.SSV('Comment:OutWarrantyMarkup','You cannot markup more than ' & markup# & '%')
      p_web.SSV('tmp:OutWarrantyMarkup',markup#)
  else! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
      p_web.SSV('Comment:OutWarrantyMarkup','')
  end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
      p_web.SSV('tmp:OutWarrantyCost',format(p_web.GSV('tmp:PurchaseCost') + (p_web.GSV('tmp:PurchaseCost') * (p_web.GSV('tmp:OutWarrantyMarkup')/100)),@n_14.2))
  end !if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
  do Value::tmp:OutWarrantyMarkup
  do SendAlert
  do Value::tmp:OutWarrantyCost  !1
  do Comment::tmp:OutWarrantyMarkup

Value::tmp:OutWarrantyMarkup  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:OutWarrantyMarkup
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:OutWarrantyMarkup')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyMarkup'',''formestimateparts_tmp:outwarrantymarkup_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyMarkup')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyMarkup',p_web.GetSessionValue('tmp:OutWarrantyMarkup'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n3',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value')

Comment::tmp:OutWarrantyMarkup  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:OutWarrantyMarkup'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment')

Prompt::tmp:FixedPrice  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FixedPrice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FixedPrice',p_web.GetValue('NewValue'))
    do Value::tmp:FixedPrice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::tmp:FixedPrice  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::tmp:FixedPrice  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Supplier  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Supplier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Supplier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Supplier',p_web.GetValue('NewValue'))
    epr:Supplier = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Supplier
    do Value::epr:Supplier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Supplier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Supplier = p_web.GetValue('Value')
  End
    epr:Supplier = Upper(epr:Supplier)
    p_web.SetSessionValue('epr:Supplier',epr:Supplier)
  p_Web.SetValue('lookupfield','epr:Supplier')
  do AfterLookup
  do Value::epr:Supplier
  do SendAlert
  do Comment::epr:Supplier

Value::epr:Supplier  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Supplier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('epr:Supplier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Supplier'',''formestimateparts_epr:supplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(epr:Supplier)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','epr:Supplier',p_web.GetSessionValue('epr:Supplier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectSuppliers')&'?LookupField=epr:Supplier&Tab=4&ForeignField=sup:Company_Name&_sort=sup:Company_Name&Refresh=sort&LookupFrom=FormEstimateParts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_value')

Comment::epr:Supplier  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_comment')

Prompt::epr:Exclude_From_Order  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exclude From Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Exclude_From_Order  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Exclude_From_Order',p_web.GetValue('NewValue'))
    epr:Exclude_From_Order = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Exclude_From_Order
    do Value::epr:Exclude_From_Order
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Exclude_From_Order',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    epr:Exclude_From_Order = p_web.GetValue('Value')
  End
  do Value::epr:Exclude_From_Order
  do SendAlert

Value::epr:Exclude_From_Order  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- epr:Exclude_From_Order
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('epr:Exclude_From_Order')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('epr:Exclude_From_Order') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:Exclude_From_Order'',''formestimateparts_epr:exclude_from_order_value'',1,'''&clip('YES')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:Exclude_From_Order',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'epr:Exclude_From_Order_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('epr:Exclude_From_Order') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:Exclude_From_Order'',''formestimateparts_epr:exclude_from_order_value'',1,'''&clip('NO')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:Exclude_From_Order',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'epr:Exclude_From_Order_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_value')

Comment::epr:Exclude_From_Order  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:PartAllocated  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Allocated')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:PartAllocated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:PartAllocated',p_web.GetValue('NewValue'))
    epr:PartAllocated = p_web.GetValue('NewValue') !FieldType= BYTE Field = epr:PartAllocated
    do Value::epr:PartAllocated
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:PartAllocated',p_web.dFormat(p_web.GetValue('Value'),'@n1'))
    epr:PartAllocated = p_web.GetValue('Value')
  End
  do Value::epr:PartAllocated
  do SendAlert

Value::epr:PartAllocated  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- epr:PartAllocated
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('epr:PartAllocated')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('epr:PartAllocated') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:PartAllocated'',''formestimateparts_epr:partallocated_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:PartAllocated',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','epr:PartAllocated_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('epr:PartAllocated') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:PartAllocated'',''formestimateparts_epr:partallocated_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:PartAllocated',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','epr:PartAllocated_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_value')

Comment::epr:PartAllocated  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:UnallocatePart  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unallocate Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:UnallocatePart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:UnallocatePart',p_web.GetValue('NewValue'))
    tmp:UnallocatePart = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:UnallocatePart
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:UnallocatePart',p_web.GetValue('Value'))
    tmp:UnallocatePart = p_web.GetValue('Value')
  End
  do Value::tmp:UnallocatePart
  do SendAlert

Value::tmp:UnallocatePart  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:UnallocatePart
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UnallocatePart'',''formestimateparts_tmp:unallocatepart_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:UnallocatePart') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:UnallocatePart',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value')

Comment::tmp:UnallocatePart  Routine
    loc:comment = p_web.Translate('Return part to stock')
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::text:OrderRequired  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OrderRequired',p_web.GetValue('NewValue'))
    do Value::text:OrderRequired
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OrderRequired  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate(p_web.GSV('text:OrderRequired'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OrderRequired  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::text:OrderRequired2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OrderRequired2',p_web.GetValue('NewValue'))
    do Value::text:OrderRequired2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OrderRequired2  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('Either reduce the quanitity required, or select the check box to create an order for the excess items',) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OrderRequired2  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CreateOrder  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Create Order For Selected Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CreateOrder',p_web.GetValue('NewValue'))
    tmp:CreateOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CreateOrder
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:CreateOrder',p_web.GetValue('Value'))
    tmp:CreateOrder = p_web.GetValue('Value')
  End
  do Value::tmp:CreateOrder
  do SendAlert

Value::tmp:CreateOrder  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:CreateOrder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CreateOrder'',''formestimateparts_tmp:createorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:CreateOrder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CreateOrder',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_value')

Comment::tmp:CreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCodesChecked  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_prompt',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Codes Checked')
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCodesChecked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCodesChecked',p_web.GetValue('NewValue'))
    tmp:FaultCodesChecked = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCodesChecked
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:FaultCodesChecked',p_web.GetValue('Value'))
    tmp:FaultCodesChecked = p_web.GetValue('Value')
  End
  do Value::tmp:FaultCodesChecked
  do SendAlert

Value::tmp:FaultCodesChecked  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:FaultCodesChecked') = 1)
  ! --- CHECKBOX --- tmp:FaultCodesChecked
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:FaultCodesChecked'',''formestimateparts_tmp:faultcodeschecked_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:FaultCodesChecked') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:FaultCodesChecked',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value')

Comment::tmp:FaultCodesChecked  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_comment',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCodes1  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode1'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCodes1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCodes1',p_web.GetValue('NewValue'))
    tmp:FaultCodes1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCodes1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes1',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1')))
    tmp:FaultCodes1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1')) !
  End
  If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
    loc:Invalid = 'tmp:FaultCodes1'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  do Value::tmp:FaultCodes1
  do SendAlert

Value::tmp:FaultCodes1  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFautlCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes1 = '' and (p_web.GSV('Req:PartFautlCode1') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCodes1'',''formestimateparts_tmp:faultcodes1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes1',p_web.GetSessionValue('tmp:FaultCodes1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode1'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(1)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value')

Comment::tmp:FaultCodes1  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode1'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode2  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode2'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.GetValue('NewValue'))
    tmp:FaultCodes2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes2',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2')))
    tmp:FaultCodes2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2')) !
  End
  If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCodes2'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  do Value::tmp:FaultCode2
  do SendAlert

Value::tmp:FaultCode2  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes2 = '' and (p_web.GSV('Req:PartFaultCode2') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''formestimateparts_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes2',p_web.GetSessionValue('tmp:FaultCodes2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode2'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(2)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_value')

Comment::tmp:FaultCode2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode2'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode3  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode3'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.GetValue('NewValue'))
    tmp:FaultCodes3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes3',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3')))
    tmp:FaultCodes3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3')) !
  End
  If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCodes3'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  do Value::tmp:FaultCode3
  do SendAlert

Value::tmp:FaultCode3  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes3 = '' and (p_web.GSV('Req:PartFaultCode3') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''formestimateparts_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes3',p_web.GetSessionValue('tmp:FaultCodes3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode3'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(3)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_value')

Comment::tmp:FaultCode3  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode3'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode4  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode4'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.GetValue('NewValue'))
    tmp:FaultCodes4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes4',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4')))
    tmp:FaultCodes4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4')) !
  End
  If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCodes4'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  do Value::tmp:FaultCode4
  do SendAlert

Value::tmp:FaultCode4  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode4') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode4') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes4 = '' and (p_web.GSV('Req:PartFaultCode4') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''formestimateparts_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes4',p_web.GetSessionValue('tmp:FaultCodes4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode4'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(4)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_value')

Comment::tmp:FaultCode4  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode4'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode5  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode5'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.GetValue('NewValue'))
    tmp:FaultCodes5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes5',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5')))
    tmp:FaultCodes5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5')) !
  End
  If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCodes5'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  do Value::tmp:FaultCode5
  do SendAlert

Value::tmp:FaultCode5  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode5') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode5') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes5 = '' and (p_web.GSV('Req:PartFaultCode5') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''formestimateparts_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes5',p_web.GetSessionValue('tmp:FaultCodes5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode5'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(5)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_value')

Comment::tmp:FaultCode5  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode5'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode6  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode6'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.GetValue('NewValue'))
    tmp:FaultCodes6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes6',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6')))
    tmp:FaultCodes6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6')) !
  End
  If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCodes6'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  do Value::tmp:FaultCode6
  do SendAlert

Value::tmp:FaultCode6  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode6') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode6') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes6 = '' and (p_web.GSV('Req:PartFaultCode6') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''formestimateparts_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes6',p_web.GetSessionValue('tmp:FaultCodes6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode6'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(6)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_value')

Comment::tmp:FaultCode6  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode6'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode7  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode7'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.GetValue('NewValue'))
    tmp:FaultCodes7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes7',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7')))
    tmp:FaultCodes7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7')) !
  End
  If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCodes7'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  do Value::tmp:FaultCode7
  do SendAlert

Value::tmp:FaultCode7  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode7') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode7') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes7 = '' and (p_web.GSV('Req:PartFaultCode7') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''formestimateparts_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes7',p_web.GetSessionValue('tmp:FaultCodes7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode7'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(7)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_value')

Comment::tmp:FaultCode7  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode7'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode8  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode8'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.GetValue('NewValue'))
    tmp:FaultCodes8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes8',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8')))
    tmp:FaultCodes8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8')) !
  End
  If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCodes8'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  do Value::tmp:FaultCode8
  do SendAlert

Value::tmp:FaultCode8  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode8') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode8') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes8 = '' and (p_web.GSV('Req:PartFaultCode8') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''formestimateparts_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes8',p_web.GetSessionValue('tmp:FaultCodes8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode8'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(8)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_value')

Comment::tmp:FaultCode8  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode8'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode9  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode9'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.GetValue('NewValue'))
    tmp:FaultCodes9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes9',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9')))
    tmp:FaultCodes9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9')) !
  End
  If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCodes9'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  do Value::tmp:FaultCode9
  do SendAlert

Value::tmp:FaultCode9  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode9') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode9') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes9')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes9 = '' and (p_web.GSV('Req:PartFaultCode9') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''formestimateparts_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes9',p_web.GetSessionValue('tmp:FaultCodes9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode9'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(9)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_value')

Comment::tmp:FaultCode9  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode9'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode10  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode10'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.GetValue('NewValue'))
    tmp:FaultCodes10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes10',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10')))
    tmp:FaultCodes10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10')) !
  End
  If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCodes10'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  do Value::tmp:FaultCode10
  do SendAlert

Value::tmp:FaultCode10  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode10') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode10') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes10')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes10 = '' and (p_web.GSV('Req:PartFaultCode10') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''formestimateparts_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes10',p_web.GetSessionValue('tmp:FaultCodes10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode10'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(10)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_value')

Comment::tmp:FaultCode10  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode10'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode11  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode11'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.GetValue('NewValue'))
    tmp:FaultCodes11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes11',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11')))
    tmp:FaultCodes11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11')) !
  End
  If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCodes11'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  do Value::tmp:FaultCode11
  do SendAlert

Value::tmp:FaultCode11  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode11') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode11') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes11')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes11 = '' and (p_web.GSV('Req:PartFaultCode11') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''formestimateparts_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes11',p_web.GetSessionValue('tmp:FaultCodes11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode11'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(11)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_value')

Comment::tmp:FaultCode11  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode11'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode12  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode12'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.GetValue('NewValue'))
    tmp:FaultCodes12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes12',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12')))
    tmp:FaultCodes12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12')) !
  End
  If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCodes12'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  do Value::tmp:FaultCode12
  do SendAlert

Value::tmp:FaultCode12  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode12') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode12') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes12')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes12 = '' and (p_web.GSV('Req:PartFaultCode12') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''formestimateparts_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes12',p_web.GetSessionValue('tmp:FaultCodes12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode12'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(12)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_value')

Comment::tmp:FaultCode12  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode12'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::epr:UsedOnRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:UsedOnRepair',p_web.GetValue('NewValue'))
    locPartUsedOnRepair = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::epr:UsedOnRepair
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartUsedOnRepair',p_web.GetValue('Value'))
    locPartUsedOnRepair = p_web.GetValue('Value')
  End
  do Value::epr:UsedOnRepair
  do SendAlert

Value::epr:UsedOnRepair  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locPartUsedOnRepair
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locPartUsedOnRepair')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:act = ChangeRecord,'disabled','')
    if p_web.GetSessionValue('locPartUsedOnRepair') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:UsedOnRepair'',''formestimateparts_epr:usedonrepair_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPartUsedOnRepair',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Used On Repair','locPartUsedOnRepair_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Use Part NOW') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:act = ChangeRecord,'disabled','')
    if p_web.GetSessionValue('locPartUsedOnRepair') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:UsedOnRepair'',''formestimateparts_epr:usedonrepair_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPartUsedOnRepair',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Used On Repair','locPartUsedOnRepair_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Use Part When Estimate ACCEPTED') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_value')

Comment::epr:UsedOnRepair  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormEstimateParts_epr:Part_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Part_Number
      else
        do Value::epr:Part_Number
      end
  of lower('FormEstimateParts_epr:Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Description
      else
        do Value::epr:Description
      end
  of lower('FormEstimateParts_epr:Despatch_Note_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Despatch_Note_Number
      else
        do Value::epr:Despatch_Note_Number
      end
  of lower('FormEstimateParts_epr:Quantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Quantity
      else
        do Value::epr:Quantity
      end
  of lower('FormEstimateParts_tmp:PurchaseCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PurchaseCost
      else
        do Value::tmp:PurchaseCost
      end
  of lower('FormEstimateParts_tmp:OutWarrantyCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyCost
      else
        do Value::tmp:OutWarrantyCost
      end
  of lower('FormEstimateParts_tmp:OutWarrantyMarkup_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyMarkup
      else
        do Value::tmp:OutWarrantyMarkup
      end
  of lower('FormEstimateParts_epr:Supplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Supplier
      else
        do Value::epr:Supplier
      end
  of lower('FormEstimateParts_epr:Exclude_From_Order_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Exclude_From_Order
      else
        do Value::epr:Exclude_From_Order
      end
  of lower('FormEstimateParts_epr:PartAllocated_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:PartAllocated
      else
        do Value::epr:PartAllocated
      end
  of lower('FormEstimateParts_tmp:UnallocatePart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UnallocatePart
      else
        do Value::tmp:UnallocatePart
      end
  of lower('FormEstimateParts_tmp:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CreateOrder
      else
        do Value::tmp:CreateOrder
      end
  of lower('FormEstimateParts_tmp:FaultCodesChecked_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodesChecked
      else
        do Value::tmp:FaultCodesChecked
      end
  of lower('FormEstimateParts_tmp:FaultCodes1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodes1
      else
        do Value::tmp:FaultCodes1
      end
  of lower('FormEstimateParts_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('FormEstimateParts_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('FormEstimateParts_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('FormEstimateParts_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('FormEstimateParts_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('FormEstimateParts_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('FormEstimateParts_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('FormEstimateParts_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('FormEstimateParts_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('FormEstimateParts_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('FormEstimateParts_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  of lower('FormEstimateParts_epr:UsedOnRepair_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:UsedOnRepair
      else
        do Value::epr:UsedOnRepair
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  epr:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('epr:Ref_Number',epr:Ref_Number)
  epr:Quantity = 1
  p_web.SetSessionValue('epr:Quantity',epr:Quantity)
  epr:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('epr:Exclude_From_Order',epr:Exclude_From_Order)

PreCopy  Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  p_web._PreCopyRecord(ESTPARTS,epr:record_number_key)
  ! here we need to copy the non-unique fields across
  epr:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('epr:Ref_Number',p_web.GSV('wob:RefNumber'))
  epr:Quantity = 1
  p_web.SetSessionValue('epr:Quantity',1)
  epr:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('epr:Exclude_From_Order','NO')

PreUpdate       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormEstimateParts:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)

LoadRelatedRecords  Routine
    if (p_web.ifExistsValue('adjustment'))
        p_web.storeValue('adjustment')
        !        p_web.SSV('adjustment',p_web.getValue('adjustment'))
        if (p_web.getValue('adjustment') = 1)
            epr:Part_Number = 'ADJUSTMENT'
            epr:Description = 'ADJUSTMENT'
            epr:Adjustment = 'YES'
            epr:Quantity = 1
            epr:Warranty_Part = 'NO'
            epr:Exclude_From_Order = 'YES'
      
            p_web.SSV('epr:Part_Number',epr:Part_Number)
            p_web.SSV('epr:Description',epr:Description)
            p_web.SSV('epr:Adjustment',epr:Adjustment)
            p_web.SSV('epr:Quantity',epr:Quantity)
            p_web.SSV('epr:Warranty_Part',epr:Warranty_Part)
            p_web.SSV('epr:Exclude_From_Order',epr:Exclude_From_Order)
      
        end ! if (p_web.getValue('adjustment') = 1)
    end !if (p_web.ifExistsValue('adjustment'))
      
    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
        p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
        p_web.SSV('ReadOnly:PurchaseCost',1)
        p_web.SSV('ReadOnly:OutWarrantyCOst',1)
    else !if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
        p_web.SSV('ReadOnly:OutWarrantyMarkup',0)
        p_web.SSV('ReadOnly:PurchaseCost',0)
        p_web.SSV('ReadOnly:OutWarrantyCOst',0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
      
    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PART ALLOCATED'))
        p_web.SSV('ReadOnly:PartAllocated',1)
    else ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
        p_web.SSV('ReadOnly:PartAllocated',0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
      
    !      if (epr:WebOrder = 1)
    !          p_web.SSV('ReadOnly:Quantity',1)
    !      end ! if (epr:WebOrder = 1)
      
    p_web.SSV('ReadOnly:ExcludeFromOrder',0)
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PARTS - EXCLUDE FROM ORDER'))
        p_web.SSV('ReadOnly:ExcludeFromOrder',1)
    END
    
    if (loc:Act = ChangeRecord)
        if (p_web.GSV('job:Date_Completed') > 0)
            if (~SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS COSTS - EDIT POST COMPLETE'))
                p_web.SSV('ReadOnly:OutWarrantyCost',1)
                p_web.SSV('ReadOnly:Quantity',1)
            end !if (SecurityCheck('JOBS COSTS - EDIT POST COMPLETE'))
        end ! if (p_web.GSV('job:Date_Completed') > 0)
      
        if (p_web.GSV('job:Estimate') = 'YES')
            Access:ESTPARTS.Clearkey(epr:part_Ref_Number_Key)
            epr:Ref_Number    = p_web.GSV('epr:Ref_Number')
            epr:Part_Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
            if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
                ! Found
                ! This is same part as on the estimate
                p_web.SSV('ReadOnly:OutWarrantyCost',1)
                p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
            else ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('job:Estimate') = 'YES')
        p_web.SSV('ReadOnly:ExcludeFromOrder',1)
    end ! if (loc:Act = ChangeRecord)
      
    do enableDisableCosts
    do showCosts
    do lookupLocation
    do lookupMainFault
      
      
    if (p_web.GSV('Part:ViewOnly') <> 1)
        ! Show unallodate part tick box
      
        if (loc:Act = ChangeRecord)
            if (p_web.GSV('epr:Part_Ref_Number') <> '' And |
                p_web.GSV('epr:PartAllocated') = 1 And |
                p_web.GSV('epr:WebOrder') = 0)
                p_web.SSV('Show:UnallocatePart',1)
            end ! if (p_web.GSV('epr:Part_Ref_Number') <> '' And |
        end ! if (loc:Act = ChangeRecord)
    end ! if (p_web.GSV('Part:ViewOnly') <> 1)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If loc:act = ChangeRecord
  End
    If (p_web.GSV('Show:UnallocatePart') = 1)
          If p_web.IfExistsValue('tmp:UnallocatePart') = 0
            p_web.SetValue('tmp:UnallocatePart',0)
            tmp:UnallocatePart = 0
          End
    End
  If p_web.GSV('locOrderRequired') = 1
          If p_web.IfExistsValue('tmp:CreateOrder') = 0
            p_web.SetValue('tmp:CreateOrder',0)
            tmp:CreateOrder = 0
          End
  End
    If (0)
      If(p_web.GSV('Hide:FaultCodesChecked') = 1)
          If p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
            p_web.SetValue('tmp:FaultCodesChecked',0)
            tmp:FaultCodesChecked = 0
          End
      End
    End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
      if (p_web.GSV('locPartUsedOnRepair') = 0)
          loc:alert = 'Select When The Part Will Be Used'
          loc:invalid = 'locPartUsedOnRepair'
      END
  ! Insert Record Validation
  p_web.SSV('AddToStockAllocation:Type','')
  epr:PartAllocated = 1
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKey)
  def:Record_Number    = 1
  set(def:RecordNumberKey,def:RecordNumberKey)
  loop
      if (Access:DEFAULTS.Next())
          Break
      end ! if (Access:DEFAULTS.Next())
      break
  end ! loop
  
  stockPart# = 0
  if (epr:part_Ref_Number <> '')
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = p_web.GSV('epr:part_Ref_Number')
      if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Found
          ! Check for duplicate?
          stockPart# = 1
          found# = 0
          if (sto:AllowDuplicate = 0)
  
              Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
              par_ali:Ref_Number    = p_web.GSV('job:Ref_Number')
              par_Ali:Part_Number = p_web.GSV('epr:part_Number')
              set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
              loop
                  if (Access:PARTS_ALIAS.Next())
                      Break
                  end ! if (Access:PARTS.Next())
                  if (par_ali:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (epr:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (par_ali:part_Number <> p_web.GSV('epr:Part_Number'))
                      Break
                  end ! if (epr:Part_Number    <> p_web.GSV('epr:Part_Number'))
                  if (par_ali:Date_Received = '')
                      found# = 1
                      break
                  end ! if (epr:Date_Received = '')
              end ! loop
          end ! if (sto:AllowDuplicate = 0)
  
          if (found# = 1)
              loc:Invalid = 'epr:Part_Number'
              loc:Alert = 'This part is already attached to this job.'
              exit
          end ! if (epr:Date_Received = '')
      else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  else ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
  end ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
  
  ! update stock details
  p_web.SSV('locOrderRequired',0)
  if (p_web.GSV('epr:part_Number') <> 'ADJUSTMENT')
      if (p_web.GSV('epr:exclude_From_Order') <> 'YES')
          if (stockPart# = 1)
                  
              IF (p_web.GSV('locPartUsedOnRepair') = 1)
                  
                  p_web.SSV('epr:UsedOnRepair',1)
                  if (sto:Sundry_Item <> 'YES')
                      if (p_web.GSV('epr:Quantity') > sto:Quantity_Stock)
                          if (p_web.GSV('locOrderRequired') <> 1 and p_web.GSV('tmp:CreateOrder') = 0)
                              p_web.SSV('locOrderRequired',1)
                              loc:Invalid = 'tmp:OrderPart'
                              loc:Alert = 'Insufficient Items In Stock'
                              p_web.SSV('text:OrderRequired','Qty Required: ' & p_web.GSV('epr:Quantity') & ', Qty In Stock: ' & sto:Quantity_Stock)
                          else ! if (p_web.GSV('locOrderRequired') <> 1)
                                  
                          end ! if (p_web.GSV('locOrderRequired') <> 1)
                      else ! if (epr:Quantity > sto:Quantity_Stock)
                          p_web.SSV('epr:date_Ordered',Today())
                          if rapidLocation(sto:Location)
                              epr:PartAllocated = 0
                              p_web.SSV('AddToStockAllocation:Type','EST')
                              p_web.SSV('AddToStockAllocation:Status','')
                              p_web.SSV('AddToStockAllocation:Qty',p_web.GSV('epr:Quantity'))
                          end ! if rapidLocation(sto:Location)
  
                          sto:quantity_Stock -= epr:Quantity
                          if (sto:quantity_Stock < 0)
                              sto:quantity_Stock = 0
                          end ! if rapidLocation(sto:Location)
                          if (access:STOCK.tryUpdate() = level:Benign)
                              rtn# = AddToStockHistory(sto:Ref_Number, |
                                  'DEC', |
                                  epr:Despatch_Note_Number, |
                                  p_web.GSV('job:Ref_Number'), |
                                  0, |
                                  sto:Quantity_Stock, |
                                  p_web.GSV('tmp:InWarrantyCost'), |
                                  p_web.GSV('tmp:OutWarrantyCost'), |
                                  epr:Retail_Cost, |
                                  'STOCK DECREMENTED', |
                                  '', |
                                  p_web.GSV('BookingUserCode'), |
                                  sto:Quantity_Stock)
                          end ! if (access:STOCK.tryUpdate() = level:Benign)
                      end ! if (epr:Quantity > sto:Quantity_Stock)
                  else ! if (sto:Sundry_Item <> 'YES')
                      p_web.SSV('epr:date_Ordered',Today())
                  end ! if (sto:Sundry_Item <> 'YES')
              ELSE
                      
              end
          else ! if (stockPart# = 1)
  
          end ! if (stockPart# = 1)
      else ! if (epr:exclude_From_Order <> 'YES')
          p_web.SSV('epr:date_Ordered',Today())
      end ! if (epr:exclude_From_Order <> 'YES')
  else ! if (epr:part_Number <> 'ADJUSTMENT')
      p_web.SSV('epr:date_Ordered',Today())
  end !if (epr:part_Number <> 'ADJUSTMENT')
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  ! Change Record Validation

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEstimateParts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Write Fields
      do deleteSessionValues
  
      !Write Fault Codes
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer    = p_web.GSV('job:Manufacturer')
      map:ScreenOrder    = 0
      set(map:ScreenOrderKey,map:ScreenOrderKey)
      loop
          if (Access:MANFAUPA.Next())
              Break
          end ! if (Access:MANFAUPA.Next())
          if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              Break
          end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
          if (map:ScreenOrder    = 0)
              cycle
          end ! if (map:ScreenOrder    <> 0)
  
          p_web.SSV('epr:Fault_Code' & map:Field_Number,p_web.GSV('tmp:FaultCodes' & map:ScreenOrder))
      end ! loop
  
  !    loop x# = 1 To 12
  !        p_web.SSV('epr:Fault_Code' & x#,p_web.GSV('tmp:FaultCodes' & x#))
  !        linePrint('epr:Fault_Code' & x# & ' - ' & p_web.GSV('tmp:FaultCodes' & x#),'c:\log.log')
  !    end ! loop x# = 1 To 12
  
      epr:Fault_Code1 = p_web.GSV('epr:Fault_Code1')
      epr:Fault_Code2 = p_web.GSV('epr:Fault_Code2')
      epr:Fault_Code3 = p_web.GSV('epr:Fault_Code3')
      epr:Fault_Code4 = p_web.GSV('epr:Fault_Code4')
      epr:Fault_Code5 = p_web.GSV('epr:Fault_Code5')
      epr:Fault_Code6 = p_web.GSV('epr:Fault_Code6')
      epr:Fault_Code7 = p_web.GSV('epr:Fault_Code7')
      epr:Fault_Code8 = p_web.GSV('epr:Fault_Code8')
      epr:Fault_Code9 = p_web.GSV('epr:Fault_Code9')
      epr:Fault_Code10 = p_web.GSV('epr:Fault_Code10')
      epr:Fault_Code11 = p_web.GSV('epr:Fault_Code11')
      epr:Fault_Code12 = p_web.GSV('epr:Fault_Code12')
  
      If p_web.GSV('tmp:ARCPart') = 1
          epr:AveragePurchaseCost = p_web.GSV('tmp:PurchaseCost')
          epr:Purchase_Cost       = p_web.GSV('tmp:InWarrantyCost')
          epr:Sale_Cost           = p_web.GSV('tmp:OutWarrantyCost')
          epr:InWarrantyMarkup    = p_web.GSV('tmp:InWarrantyMarkup')
          epr:OutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
      Else !If tmp:ARCPart
          epr:RRCAveragePurchaseCost  = p_web.GSV('tmp:PurchaseCost')
          epr:RRCPurchaseCost     = p_web.GSV('tmp:InWarrantyCost')
          epr:RRCSaleCost         = p_web.GSV('tmp:OutWarrantyCost')
          epr:RRCInWarrantyMarkup = p_web.GSV('tmp:InWarrantyMarkup')
          epr:RRCOutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
          epr:Purchase_Cost       = epr:RRCPurchaseCost
          epr:Sale_Cost           = epr:RRCSaleCost
      End !If tmp:ARCPart
  
  
  
  p_web.DeleteSessionValue('FormEstimateParts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
  If loc:act = ChangeRecord
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If epr:Part_Number = ''
          loc:Invalid = 'epr:Part_Number'
          loc:alert = p_web.translate('Part Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          epr:Part_Number = Upper(epr:Part_Number)
          p_web.SetSessionValue('epr:Part_Number',epr:Part_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If epr:Description = ''
          loc:Invalid = 'epr:Description'
          loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          epr:Description = Upper(epr:Description)
          p_web.SetSessionValue('epr:Description',epr:Description)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          epr:Despatch_Note_Number = Upper(epr:Despatch_Note_Number)
          p_web.SetSessionValue('epr:Despatch_Note_Number',epr:Despatch_Note_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If epr:Quantity = ''
          loc:Invalid = 'epr:Quantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          epr:Quantity = Upper(epr:Quantity)
          p_web.SetSessionValue('epr:Quantity',epr:Quantity)
        If loc:Invalid <> '' then exit.
      if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
          loc:Invalid = 'tmp:OutWarrantyMarkup'
          loc:Alert = 'You cannot mark-up parts more than ' & GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI') & '%.'
      end !if p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          epr:Supplier = Upper(epr:Supplier)
          p_web.SetSessionValue('epr:Supplier',epr:Supplier)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('locOrderRequired') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
    loc:InvalidTab += 1
    If p_web.GSV('Hide:PartFaultCode1') <> 1
        If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
          loc:Invalid = 'tmp:FaultCodes1'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
          p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode2') <> 1
        If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
          loc:Invalid = 'tmp:FaultCodes2'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
          p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode3') <> 1
        If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
          loc:Invalid = 'tmp:FaultCodes3'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
          p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode4') <> 1
        If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
          loc:Invalid = 'tmp:FaultCodes4'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
          p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode5') <> 1
        If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
          loc:Invalid = 'tmp:FaultCodes5'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
          p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode6') <> 1
        If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
          loc:Invalid = 'tmp:FaultCodes6'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
          p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode7') <> 1
        If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
          loc:Invalid = 'tmp:FaultCodes7'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
          p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode8') <> 1
        If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
          loc:Invalid = 'tmp:FaultCodes8'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
          p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode9') <> 1
        If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
          loc:Invalid = 'tmp:FaultCodes9'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
          p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode10') <> 1
        If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
          loc:Invalid = 'tmp:FaultCodes10'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
          p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode11') <> 1
        If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
          loc:Invalid = 'tmp:FaultCodes11'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
          p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode12') <> 1
        If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
          loc:Invalid = 'tmp:FaultCodes12'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
          p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
        If loc:Invalid <> '' then exit.
    End
  ! tab = 6
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
  IF (p_web.GSV('AddToStockAllocation:Type') <> '')
      AddToStockAllocation(p_web)
  END

PostCopy        Routine
  p_web.SetSessionValue('FormEstimateParts:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Location')
  p_web.StoreValue('tmp:SecondLocation')
  p_web.StoreValue('tmp:ShelfLocation')
  p_web.StoreValue('tmp:PurchaseCost')
  p_web.StoreValue('tmp:OutWarrantyCost')
  p_web.StoreValue('tmp:OutWarrantyMarkup')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:UnallocatePart')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:CreateOrder')
  p_web.StoreValue('tmp:FaultCodesChecked')
  p_web.StoreValue('tmp:FaultCodes1')
  p_web.StoreValue('tmp:FaultCodes2')
  p_web.StoreValue('tmp:FaultCodes3')
  p_web.StoreValue('tmp:FaultCodes4')
  p_web.StoreValue('tmp:FaultCodes5')
  p_web.StoreValue('tmp:FaultCodes6')
  p_web.StoreValue('tmp:FaultCodes7')
  p_web.StoreValue('tmp:FaultCodes8')
  p_web.StoreValue('tmp:FaultCodes9')
  p_web.StoreValue('tmp:FaultCodes10')
  p_web.StoreValue('tmp:FaultCodes11')
  p_web.StoreValue('tmp:FaultCodes12')
  p_web.StoreValue('locPartUsedOnRepair')

PostDelete      Routine
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone

!        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
!        map:ScreenOrder    = fNumber
!        map:Manufacturer   = p_web.GSV('job:Manufacturer')
!        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Found
!        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Error
!        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!
!        if (map:MainFault)
!            p_web.FileToSessionQueue(MANFAULO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfo:Description)
!
!        else ! if (map:MainFault)
!            p_web.FileToSessionQueue(MANFPALO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfp:Description)
!        end ! if (map:MainFault)
        do buildFaultCodes
        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes' & fNumber)
local.SetLookupButton      Procedure(Long fNumber)
locUseRelatedPart           String(30)
Code
    if (p_web.GSV('ShowDate:PartFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''FormEstimateParts_pickdate_value'',1,FieldValue(this,1));nextFocus(FormEstimateParts_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:PartFaultCode' & fNumber) = 1)

        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:ScreenOrder    = fNumber
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
        if (map:UseRelatedJobCode <> 0)
            locUseRelatedPart = 'relatedPartCode=' & map:Field_Number
        else
            locUseRelatedPart = ''
        end ! if (map:UseRelatedJobCode <> 0)

        if (map:MainFault)

            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                        'sort&LookupFrom=FormEstimateParts&' & |
                        'fieldNumber=' & maf:Field_Number & '&partType=C&partMainFault=1&' & clip(locUseRelatedPart)),) !lookupextra

        else ! if (map:MainFault)
            found# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
            if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Found
                ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
                If sto:Assign_Fault_Codes = 'YES'
                    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                    stm:Ref_Number = sto:Ref_Number
                    stm:Manufacturer = p_web.GSV('job:Manufacturer')
                    stm:Model_Number = p_web.GSV('job:Model_Number')
                    If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign

                        Case map:Field_Number
                        Of 1
                            If stm:FaultCode1 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 2
                            If stm:FaultCode2 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 3
                            If stm:FaultCode3 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 4
                            If stm:FaultCode4 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 5
                            If stm:FaultCode5 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 6
                            If stm:FaultCode6 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 7
                            If stm:FaultCode7 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 8
                            If stm:FaultCode8 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 9
                            If stm:FaultCode9 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 10
                            If stm:FaultCode10 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 11
                            If stm:FaultCode11 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 12
                            If stm:FaultCode12 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        End ! Case map:Field_Number
                        If Found# = 1
                            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseStockPartFaultCodeLookup')&|
                                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=stu:Field&_sort=stu:Field&Refresh=' & |
                                        'sort&LookupFrom=FormEstimateParts&' & |
                                        'fieldNumber=' & map:Field_Number & '&stockRefNumber=' & stm:RecordNumber),) !lookupextra

                        End ! If Found# = 1
                    End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                End ! If sto:Assign_Fault_Codes = 'YES'
            else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)

            if (found# = 0)
                packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowsePartFaultCodeLookup')&|
                            '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfp:Field&_sort=mfp:Field&Refresh=' & |
                            'sort&LookupFrom=FormEstimateParts&' & |
                            'fieldNumber=' & map:Field_Number & '&partType=C&'),) !lookupextra
            end ! if (found# = 0)
        end ! if (map:MainFault)
        do sendPacket
    end !if (p_web.GSV('Lookup:PartFaultCode1') = 1)
ReceiptFromPUP       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locIMEINumber        STRING(30)                            !
locMSN               STRING(30)                            !
locNetwork           STRING(30)                            !
locInFault           STRING(30)                            !
locIMEIMessage       STRING(100)                           !
locPassword          STRING(30)                            !
locValidationMessage STRING(100)                           !
FilesOpened     Long
WEBJOB::State  USHORT
NETWORKS::State  USHORT
MANFAULT::State  USHORT
TagFile::State  USHORT
JOBACC::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ReceiptFromPUP')
  loc:formname = 'ReceiptFromPUP_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ReceiptFromPUP','')
    p_web._DivHeader('ReceiptFromPUP',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
deleteVariables         routine
    p_web.deleteSessionValue('locIMEINumber')
    p_web.deleteSessionValue('locIMEIMessage')
    p_web.deleteSessionValue('locIMEIAccepted')
    p_web.deleteSessionValue('locMSN')
    p_web.deleteSessionValue('locNetwork')
    p_web.deleteSessionValue('locInFault')
    p_web.deleteSessionValue('locPassword')
    p_web.deleteSessionValue('PasswordRequired')
    p_web.deleteSessionValue('Comment:IMEINumber')
    p_web.deleteSessionValue('Comment:Password')
    p_web.deleteSessionValue('tmp:LoanModelNumber')
    p_web.deleteSessionValue('locValidationMessage')
saveChanges         routine
    data
locNotes        String(255)
    code
        if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
            locNotes = '<13,10>OLD I.M.E.I.: ' & p_web.GSV('job:ESN') & |
                '<13,10>NEW I.M.E.I.: ' & p_web.GSV('locIMEINumber')
            p_web.SSV('job:ESN',p_web.GSV('locIMEINumber'))
        end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))

        if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD M.S.N.: ' & p_web.GSV('job:MSN') & |
                '<13,10>NEW M.S.N.: ' & p_web.GSV('locMSN')
            p_web.SSV('job:MSN',p_web.GSV('locMSN'))
        end ! if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))

        if (p_web.GSV('locNetwork') <> p_web.GSV('jobe:Network'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD NETWORK: ' & p_web.GSV('jobe:Network') & |
                '<13,10>NEW NETWORK: ' & p_web.GSV('locNetwork')
            p_web.SSV('jobe:Network',p_web.GSV('locNetwork'))
        end ! if (p_web.GSV('locNetwork') <> p_web.GSV('job:Network'))

        Access:MANFAULT.Clearkey(maf:inFaultKey)
        maf:manufacturer    = p_web.GSV('job:manufacturer')
        maf:inFault    = 1
        if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)

        if (maf:Field_Number < 13)
            if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('job:Fault_Code' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        else ! if (maf:Field_Number < 13)
            if (p_web.GSV('wob:FaultCode' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('wob:FaultCode' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        end ! if (maf:Field_Number < 13)

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Notes','DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','PUP VALIDATION')
            addToAudit(p_web)
        end ! if (clip(locNotes) <> '')

        locNotes = ''
        if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1))
            locNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'
            Access:JOBACC.Clearkey(jac:ref_Number_Key)
            jac:ref_Number    = p_web.GSV('job:Ref_Number')
            set(jac:ref_Number_Key,jac:ref_Number_Key)
            loop
                if (Access:JOBACC.Next())
                    Break
                end ! if (Access:JOBACC.Next())
                if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                    Break
                end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(jac:accessory)
            end ! loop

            locNotes = clip(locNotes) & |
                '<13,10,13,0>ACCESSORIES RECEIVED:-'

            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(tag:taggedValue)
            end ! loop

        end ! if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1)

        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Notes',clip(locNotes))
        p_web.SSV('AddToAudit:Action','UNIT RECEIVED AT ' & p_web.GSV('BookingSite') & ' FROM PUP')
        addToAudit(p_web)

        p_web.SSV('LocationChange:Location',p_web.GSV('Default:' & p_web.GSV('BookingSite') & 'Location'))
        locationChange(p_web)

        p_web.SSV('job:Workshop','YES')

        p_web.SSV('GetStatus:Type','JOB')
        p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('Default:StatusReceivedFromPUP'),1,3))
        getStatus(p_web)

        Access:JOBS.Clearkey(job:ref_Number_Key)
        job:ref_Number    = p_web.GSV('job:Ref_Number')
        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBS)
            access:JOBS.tryUpdate()
            updateDateTimeStamp(job:Ref_Number)
        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = p_web.GSV('job:Ref_Number')
        if (Access:WEBJOB.TryFetch(wob:RefNumberKey)= Level:Benign)
            p_web.SessionQueueToFile(WEBJOB)
            access:WEBJOB.TryUpdate()
        END
        
        Access:JOBSE.Clearkey(jobe:refNumberKey)
        jobe:refNumber    = p_web.GSV('job:Ref_Number')
        if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBSE)
            access:JOBSE.tryUpdate()
        else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ReceiptFromPUP_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do DeleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locNetwork'
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  p_web.SetSessionValue('locPassword',locPassword)
  p_web.SetSessionValue('locMSN',locMSN)
  p_web.SetSessionValue('locNetwork',locNetwork)
  p_web.SetSessionValue('locInFault',locInFault)
  p_web.SetSessionValue('locValidationMessage',locValidationMessage)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locIMEIMessage')
    locIMEIMessage = p_web.GetValue('locIMEIMessage')
    p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  End
  if p_web.IfExistsValue('locPassword')
    locPassword = p_web.GetValue('locPassword')
    p_web.SetSessionValue('locPassword',locPassword)
  End
  if p_web.IfExistsValue('locMSN')
    locMSN = p_web.GetValue('locMSN')
    p_web.SetSessionValue('locMSN',locMSN)
  End
  if p_web.IfExistsValue('locNetwork')
    locNetwork = p_web.GetValue('locNetwork')
    p_web.SetSessionValue('locNetwork',locNetwork)
  End
  if p_web.IfExistsValue('locInFault')
    locInFault = p_web.GetValue('locInFault')
    p_web.SetSessionValue('locInFault',locInFault)
  End
  if p_web.IfExistsValue('locValidationMessage')
    locValidationMessage = p_web.GetValue('locValidationMessage')
    p_web.SetSessionValue('locValidationMessage',locValidationMessage)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ReceiptFromPUP_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !INIT
      if (MSNRequired(p_web.GSV('job:Manufacturer')))
          p_web.SSV('Hide:MSN',0)
      else
          p_web.SSV('Hide:MSN',1)
      end ! if (MSNRequired(p_web.GSV('job:Manufacturer')))
  
      p_web.SSV('Comment:IMEINumber','Enter I.M.E.I. and press [TAB] to accept')
  
      ! Used for the tag browse
      p_web.SSV('tmp:LoanModelNumber',p_web.GSV('job:Model_Number'))
  
      clearTagFile(p_web)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locIMEIMessage = p_web.RestoreValue('locIMEIMessage')
 locPassword = p_web.RestoreValue('locPassword')
 locMSN = p_web.RestoreValue('locMSN')
 locNetwork = p_web.RestoreValue('locNetwork')
 locInFault = p_web.RestoreValue('locInFault')
 locValidationMessage = p_web.RestoreValue('locValidationMessage')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ReceiptFromPUP_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ReceiptFromPUP_ChainTo')
    loc:formaction = p_web.GetSessionValue('ReceiptFromPUP_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ReceiptFromPUP" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ReceiptFromPUP" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ReceiptFromPUP" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Receipt From PUP') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Receipt From PUP',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ReceiptFromPUP">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ReceiptFromPUP" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ReceiptFromPUP')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('PUP Unit Validation') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ReceiptFromPUP')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ReceiptFromPUP'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ReceiptFromPUP')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('PUP Unit Validation') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEIMessage
      do Value::locIMEIMessage
      do Comment::locIMEIMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:MSN') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNetwork
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInFault
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonValidateAccessories
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locValidationMessage
      do Comment::locValidationMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locIMEINumber  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
      if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
          p_web.SSV('locIMEIMessage','IMEI Number does not match IMEI from booking. Enter password to accept change and continue.')
          p_web.SSV('PasswordRequired',1)
  !        p_web.SSV('locIMEIAccepted',0)
      else!
  !        p_web.SSV('locIMEIAccepted',1)
          p_web.SSV('PasswordRequired',0)
          p_web.SSV('locIMEIMessage','')
      end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Value::locPassword  !1
  do Comment::locPassword
  do Prompt::locInFault
  do Value::locInFault  !1
  do Prompt::locMSN
  do Value::locMSN  !1
  do Prompt::locNetwork
  do Value::locNetwork  !1
  do Value::locIMEIMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''receiptfrompup_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:IMEINumber'))
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment')

Prompt::locIMEIMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_prompt',Choose(p_web.GSV('locIMEIMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locIMEIMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEIMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEIMessage',p_web.GetValue('NewValue'))
    locIMEIMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEIMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEIMessage',p_web.GetValue('Value'))
    locIMEIMessage = p_web.GetValue('Value')
  End

Value::locIMEIMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value',Choose(p_web.GSV('locIMEIMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locIMEIMessage') = '')
  ! --- DISPLAY --- locIMEIMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIMEIMessage'),0) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value')

Comment::locIMEIMessage  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_comment',Choose(p_web.GSV('locIMEIMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locIMEIMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPassword  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('PasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt')

Validate::locPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPassword',p_web.GetValue('NewValue'))
    locPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPassword',p_web.GetValue('Value'))
    locPassword = p_web.GetValue('Value')
  End
  If locPassword = ''
    loc:Invalid = 'locPassword'
    loc:alert = p_web.translate('Enter Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locPassword = Upper(locPassword)
    p_web.SetSessionValue('locPassword',locPassword)
  Access:users.Clearkey(use:Password_Key)
  use:Password    = p_web.GSV('locPassword')
  if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Found
      Access:ACCAREAS.Clearkey(acc:access_Level_Key)
      acc:user_Level    = use:user_Level
      acc:access_Area    = 'PUP RECEIPT - CHANGE IMEI'
      if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Found
          p_web.SSV('Comment:Password','Password Accepted')
      else ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Error
          p_web.SSV('Comment:Password','You do not have access to change the IMEI')
          p_web.SSV('locPassword','')
      end ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
  else ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Error
      p_web.SSV('Comment:Password','Invalid Password')
      p_web.SSV('locPassword','')
  
  end ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locPassword
  do SendAlert
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Comment::locPassword

Value::locPassword  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('PasswordRequired') <> 1)
  ! --- STRING --- locPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPassword'',''receiptfrompup_locpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPassword',p_web.GetSessionValueFormat('locPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value')

Comment::locPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:Password'))
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('PasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment')

Prompt::locMSN  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt')

Validate::locMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMSN',p_web.GetValue('NewValue'))
    locMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMSN',p_web.GetValue('Value'))
    locMSN = p_web.GetValue('Value')
  End
  If locMSN = ''
    loc:Invalid = 'locMSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locMSN = Upper(locMSN)
    p_web.SetSessionValue('locMSN',locMSN)
  do Value::locMSN
  do SendAlert

Value::locMSN  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMSN'',''receiptfrompup_locmsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMSN',p_web.GetSessionValueFormat('locMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value')

Comment::locMSN  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNetwork  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt')

Validate::locNetwork  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('NewValue'))
    locNetwork = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNetwork
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('Value'))
    locNetwork = p_web.GetValue('Value')
  End
  If locNetwork = ''
    loc:Invalid = 'locNetwork'
    loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNetwork = Upper(locNetwork)
    p_web.SetSessionValue('locNetwork',locNetwork)
  p_Web.SetValue('lookupfield','locNetwork')
  do AfterLookup
  do Value::locNetwork
  do SendAlert
  do Comment::locNetwork

Value::locNetwork  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNetwork
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNetwork')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNetwork = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNetwork'',''receiptfrompup_locnetwork_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNetwork')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locNetwork',p_web.GetSessionValueFormat('locNetwork'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=locNetwork&Tab=2&ForeignField=net:Network&_sort=net:Network&Refresh=sort&LookupFrom=ReceiptFromPUP&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value')

Comment::locNetwork  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment')

Prompt::locInFault  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('In Fault')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt')

Validate::locInFault  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInFault',p_web.GetValue('NewValue'))
    locInFault = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInFault
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInFault',p_web.GetValue('Value'))
    locInFault = p_web.GetValue('Value')
  End
  If locInFault = ''
    loc:Invalid = 'locInFault'
    loc:alert = p_web.translate('In Fault') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locInFault = Upper(locInFault)
    p_web.SetSessionValue('locInFault',locInFault)
  do Value::locInFault
  do SendAlert

Value::locInFault  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locInFault
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locInFault')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locInFault = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInFault'',''receiptfrompup_locinfault_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInFault',p_web.GetSessionValueFormat('locInFault'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
          Access:MANFAULT.Clearkey(maf:InFaultKey)
          maf:manufacturer    = p_web.GSV('job:manufacturer')
          maf:inFault    = 1
          if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Found
              packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                          '?LookupField=locInFault&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                          'sort&LookupFrom=ReceiptFromPUP&' & |
                          'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
  
          else ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
  
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value')

Comment::locInFault  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','ReceiptFromPUP')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !Validate Accessories
      error# = 0
  
      Access:JOBACC.Clearkey(jac:ref_Number_Key)
      jac:ref_Number    = p_web.GSV('job:Ref_Number')
      set(jac:ref_Number_Key,jac:ref_Number_Key)
      loop
          if (Access:JOBACC.Next())
              Break
          end ! if (Access:JOBACC.Next())
          if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
          Access:TAGFILE.Clearkey(tag:keyTagged)
          tag:sessionID    = p_web.sessionID
          tag:taggedValue  = jac:accessory
          if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
              ! Found
              if (tag:tagged <> 1)
                  error# = 1
                  break
              end ! if (tag:tagged <> 1)
          else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
              ! Error
              error# = 1
              break
          end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  
      end ! loop
  
      if (error# = 0)
          Access:TAGFILE.Clearkey(tag:keyTagged)
          tag:sessionID    = p_web.sessionID
          set(tag:keyTagged,tag:keyTagged)
          loop
              if (Access:TAGFILE.Next())
                  Break
              end ! if (Access:TAGFILE.Next())
              if (tag:sessionID    <> p_web.sessionID)
                  Break
              end ! if (tag:sessionID    <> p_web.sessionID)
              if (tag:Tagged = 0)
                  cycle
              end ! if (tag:Tagged = 0)
  
              Access:JOBACC.Clearkey(jac:ref_Number_Key)
              jac:ref_Number    = p_web.GSV('job:Ref_Number')
              jac:accessory    = tag:taggedValue
              if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Found
              else ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Error
                  error# = 1
                  break
              end ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
  
          end ! loop
      end ! if (error# = 0)
  
      case error#
      of 1
          p_web.SSV('locValidationMessage','<span class="RedBold">Accessories Validated. There is a mismatch.</span>')
      else
          p_web.SSV('locValidationMessage','<span class="GreenBold">Accessories Validated</span>')
      end
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::locValidationMessage  !1
  do Value::locValidationMessage  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''receiptfrompup_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locValidationMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locValidationMessage',p_web.GetValue('NewValue'))
    locValidationMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locValidationMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locValidationMessage',p_web.GetValue('Value'))
    locValidationMessage = p_web.GetValue('Value')
  End

Value::locValidationMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value',Choose(p_web.GSV('locValidationMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locValidationMessage') = '')
  ! --- DISPLAY --- locValidationMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locValidationMessage'),1) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value')

Comment::locValidationMessage  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_comment',Choose(p_web.GSV('locValidationMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locValidationMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ReceiptFromPUP_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('ReceiptFromPUP_locPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPassword
      else
        do Value::locPassword
      end
  of lower('ReceiptFromPUP_locMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMSN
      else
        do Value::locMSN
      end
  of lower('ReceiptFromPUP_locNetwork_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNetwork
      else
        do Value::locNetwork
      end
  of lower('ReceiptFromPUP_locInFault_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInFault
      else
        do Value::locInFault
      end
  of lower('ReceiptFromPUP_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)

PreCopy  Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)

PreDelete       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validation
      if (p_web.GSV('locValidationMessage') = '')
          loc:Alert = 'You must validate the accessories'
          loc:invalid = 'buttonValidateAccessories'
          exit
      end ! if (p_web.GSV('locValidationMessage') = '')
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('PasswordRequired') <> 1)
        If locPassword = ''
          loc:Invalid = 'locPassword'
          loc:alert = p_web.translate('Enter Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locPassword = Upper(locPassword)
          p_web.SetSessionValue('locPassword',locPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 1
    loc:InvalidTab += 1
    If p_web.GSV('Hide:MSN') <> 1
        If locMSN = ''
          loc:Invalid = 'locMSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locMSN = Upper(locMSN)
          p_web.SetSessionValue('locMSN',locMSN)
        If loc:Invalid <> '' then exit.
    End
        If locNetwork = ''
          loc:Invalid = 'locNetwork'
          loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNetwork = Upper(locNetwork)
          p_web.SetSessionValue('locNetwork',locNetwork)
        If loc:Invalid <> '' then exit.
        If locInFault = ''
          loc:Invalid = 'locInFault'
          loc:alert = p_web.translate('In Fault') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locInFault = Upper(locInFault)
          p_web.SetSessionValue('locInFault',locInFault)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locIMEIMessage')
  p_web.StoreValue('locPassword')
  p_web.StoreValue('locMSN')
  p_web.StoreValue('locNetwork')
  p_web.StoreValue('locInFault')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidationMessage')
  !Post Update
  do saveChanges


  do deleteVariables
QAProcess PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'QAProcess') ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
MenuTools            PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MenuTools -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('MenuTools')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
  do WebMenus
  do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end

WebMenus  Routine
  packet = clip(packet) & |
    p_web.br &|
    '<div id="'&clip('ChromeMenu')&'" class="'&clip('chromemenu')&'"><13,10><ul><13,10>'
        packet = clip(packet) & '<li><a 0 onMouseover="cssdropdown.dropit(this,event,''dropmenu1'')">'&p_web.Translate('QA Process')&'</a></li><13,10>'
  packet = clip(packet) & '</ul><13,10></div><13,10>'
        do Menu:1

Menu:1  Routine
  packet = clip(packet) & '<div id="dropmenu1" class="'&clip('DropMenu')&'"><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('QAProcess?QAType=Electronic'))&'">'&p_web.Translate('Electronic QA')&'</a><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('QAProcess?QAType=Manual'))&'">'&p_web.Translate('Manual QA')&'</a><13,10>'
  packet = clip(packet) & '</div><13,10>'
  do SendPacket

useStockAllocation   PROCEDURE  (fLocation)                ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    retValue# = 0

    Access:LOCATION.Clearkey(loc:location_Key)
    loc:location    = fLocation
    if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Found
        if (loc:useRapidStock)
            retValue# = 1
        end !if (loc:useRapidStock)
    else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)

    do closeFiles

    return retValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
saveJob              PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    Access:WEBJOB.Clearkey(wob:refNumberKey)
    wob:refNumber    = p_web.GSV('wob:ref_Number')
    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(WEBJOB)
        access:WEBJOB.tryUpdate()
    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

    Access:JOBS.Clearkey(job:ref_Number_Key)
    job:ref_Number    = p_web.GSV('wob:RefNumber')
    if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBS)
        access:JOBS.tryupdate()
    else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE)
        access:JOBSE.tryupdate()
    else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)


    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE2)
        access:JOBSE2.tryupdate()
    else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)


    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBNOTES)
        if access:JOBNOTES.tryupdate()
        end
    else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS.Close
     Access:JOBSE.Close
     Access:JOBNOTES.Close
     Access:JOBSE2.Close
     Access:WEBJOB.Close
     FilesOpened = False
  END
