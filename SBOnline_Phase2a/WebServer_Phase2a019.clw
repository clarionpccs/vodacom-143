

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A019.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A024.INC'),ONCE        !Req'd for module callout resolution
                     END


CompulsoryFieldCheck PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_taf_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
tmp:ForceFaultCodes  BYTE(0)                               !Force Fault Codes
tmp:FoundRequestedParts BYTE(0)                            !Found Requested Parts
tmp:FoundOrderedParts BYTE(0)                              !Found Ordered Parts
tmp:FoundCommonFault BYTE(0)                               !Found Common Fault
save_aud_id          USHORT,AUTO                           !
save_map_id          USHORT                                !
Skip_non_Main        BYTE(0)                               !
tmp:KeyRepair        BYTE(0)                               !Key Repair
tmp:FaultCode        STRING(255),DIM(20)                   !Fault Codes
locErrorMessage      STRING(10000)                         !
func:Type            STRING('C')                           !
local                      CLASS
ValidateFaultCodes             Procedure(),Byte
OutFaultPart                   Procedure(Long func:FieldNumber, String func:JobType),Byte
                           END ! local   CLASS
FilesOpened     BYTE(0)
  CODE
!Main Process
    !return

    do openFiles
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    func:Type = p_web.GSV('CompulsoryFieldCheck:Type')
    error# = 0

    ! Inserting (DBH 09/11/2007) # 9278 - Save the fault codes in a "easy to use" form
    tmp:FaultCode[1]  = p_web.GSV('job:Fault_Code1')
    tmp:FaultCode[2]  = p_web.GSV('job:Fault_Code2')
    tmp:FaultCode[3]  = p_web.GSV('job:Fault_Code3')
    tmp:FaultCode[4]  = p_web.GSV('job:Fault_Code4')
    tmp:FaultCode[5]  = p_web.GSV('job:Fault_Code5')
    tmp:FaultCode[6]  = p_web.GSV('job:Fault_Code6')
    tmp:FaultCode[7]  = p_web.GSV('job:Fault_Code7')
    tmp:FaultCode[8]  = p_web.GSV('job:Fault_Code8')
    tmp:FaultCode[9]  = p_web.GSV('job:Fault_Code9')
    tmp:FaultCode[10] = p_web.GSV('job:Fault_Code10')
    tmp:FaultCode[11] = p_web.GSV('job:Fault_Code11')
    tmp:FaultCode[12] = p_web.GSV('job:Fault_Code12')
    tmp:FaultCode[13] = p_web.GSV('wob:FaultCode13')
    tmp:FaultCode[14] = p_web.GSV('wob:FaultCode14')
    tmp:FaultCode[15] = p_web.GSV('wob:FaultCode15')
    tmp:FaultCode[16] = p_web.GSV('wob:FaultCode16')
    tmp:FaultCode[17] = p_web.GSV('wob:FaultCode17')
    tmp:FaultCode[18] = p_web.GSV('wob:FaultCode18')
    tmp:FaultCode[19] = p_web.GSV('wob:FaultCode19')
    tmp:FaultCode[20] = p_web.GSV('wob:FaultCode20')
    ! End (DBH 09/11/2007) #9278

    If ForceTransitType(func:Type)
        If p_web.GSV('job:Transit_Type') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Transit Type Missing.'
        End!If p_web.GSV('job:Transit_Type = ''
    End !If ForceTransitType(func:Type)
    
    If p_web.GSV('jobe2:CourierWaybillNumber') = ''
        ! #11880 The compulsory-ness of the waybill is determined by the Transit Type setup. (Bryan: 05/05/2011)
        Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
        trt:Transit_Type = p_web.GSV('job:Transit_Type')
        IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
            ! #11880 If comp at booking, always compulsory, otherwise check at completion (Bryan: 05/05/2011)
            IF (trt:WaybillComBook = 1 OR |
                (func:Type = 'C' And trt:WaybillComComp = 1))
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Courier Waybill Number Missing.'
            END
        END ! IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
    END ! If jobe2:CourierWaybillNumber = ''

    If p_web.GSV('job:POP') = 'REJ' And p_web.GSV('job:Warranty_Job') = 'YES'
        error# = 1
        locErrorMessage = Clip(locErrorMessage) & '<13,10>Cannot be a warranty job, it has been rejected (no POP).'
    End !If p_web.GSV('job:POP = 'REJ' And p_web.GSV('job:Warranty_Job = 'YES'

    If ForceMobileNumber(func:Type)
        If p_web.GSV('job:Mobile_Number') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Mobile Number Missing.'
        End!If p_web.GSV('job:Mobile_Number = ''
    End !If ForceMobileNumber(func:Type)

    ! Inserting (DBH 07/12/2007) # 9491 - Check if an SMS alert number has been filled in
    If (p_web.GSV('jobe2:SMSNotification') = 1 And p_web.GSV('jobe2:SMSAlertNumber') = '')
        If p_web.GSV('job:Mobile_Number') <> ''
            ! May as well use the mobile number if it exists (DBH: 07/12/2007)
            p_web.SSV('jobe2:SMSAlertNumber',p_web.GSV('job:Mobile_Number'))
            Access:JOBSE2.TryUpdate()
        Else ! If p_web.GSV('job:Mobile_Number <> ''
            Error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>SMS Alert Number Missing.'
        End ! If p_web.GSV('job:Mobile_Number <> ''
    End ! If p_web.GSV('jobe:2:SMSNotification And p_web.GSV('jobe:2:SMSAlertNumber = ''
    
    !Added by Paul 05/10/2010 - Log no 11691
    !Do this only for chargeable jobs
    If p_web.GSV('job:Chargeable_Job') = 'YES' then
        If func:Type = 'C' then
            !we need to check of the default labour cost is now lower than the labour cost set on the job
            DefaultLabourCost(p_web)
            !now we have the default labour cost - is it less than the labour cost entered
            ! #11773 Only check if ignoring costs. (Bryan: 28/10/2010)
            If p_web.GSV('jobe:RRCCLabourCost') > p_web.GSV('DefaultLabourCost') AND p_web.GSV('jobe:IgnoreRRCChaCosts') = 1 then

                !labour costs need to be changed
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Override Labour Cost is Higher than Default Cost.'
            End
        End
    End

    ! Inserting (DBH 24/01/2008) # 9612 - Check for waybill courier number
    If p_web.GSV('job:Transit_Type') = 'ARRIVED FRANCHISE'
        If p_web.GSV('jobe2:CourierWaybillNumber') = ''
            If securityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND COURIER WAYBILL NUMBER')
                ! Only force field if the user has access to change it
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Courier Waybill Number Missing.'
            End ! If func:Type = 'B' Or SecurityCheck('AMEND COURIER WAYBILL NUMBER') = 0
        End ! If p_web.GSV('jobe:2:CourierWaybillNumber = ''
    End ! If p_web.GSV('job:Transit_Type = 'ARRIVED FRANCHISE'
    ! End (DBH 24/01/2008) #9612

    If ForceModelNumber(func:Type)
        If p_web.GSV('job:Model_Number') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Model Number Missing.'
        End!If p_web.GSV('job:Model_Number = ''
    End !If ForceModelNumber(func:Type)

    If ForceUnitType(func:Type)
        If p_web.GSV('job:Unit_Type') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Unit Type Type Missing.'
        End!If p_web.GSV('job:Unit_Type = ''
    End !If ForceUnitType(func:Type)

    If ForceColour(func:Type)
        If p_web.GSV('job:Colour') = ''
            error# = 1
            If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Missing.'
            Else !If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Colour Missing'
            End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
        End!If p_web.GSV('job:Unit_Type = ''
    End !If ForceColour(func:Type)

    If ForceFaultDescription(func:Type)
        If p_web.GSV('jbn:Fault_Description') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Description Missing.'
        End!If jbn:Fault_Description = ''
    End !If ForceFaultDescription(func:Type)

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:UseInvTextForFaults = FALSE
            If ~man:QAAtCompletion And p_web.GSV('job:QA_Passed') <> 'YES'
                If ForceInvoiceText(func:Type)
                    IF jbn:Invoice_Text = ''
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Invoice Text Missing.'
                    End!IF jbn:Invoice_Text = ''
                End !If ForceInvoiceText(func:Type)
            END
        ELSE
           !Check to see if any havebeen added to the file?!
            If ForceInvoiceText(func:Type)
                fail# = 0
                Access:ReptyDef.ClearKey(rtd:ManRepairTypeKey)
                rtd:Manufacturer = p_web.GSV('job:manufacturer')
                rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
                IF Access:ReptyDef.Fetch(rtd:ManRepairTypeKey)
                  !Error!
                ELSE
                    IF rtd:Warranty = 'YES'
                        IF rtd:ExcludeFromEDI = TRUE
                      !Fail Outfault!
                            fail# = 1
                        END
                    END
                END

                JOFL# = 0
                Access:JobOutFL.ClearKey(joo:JobNumberKey)
                joo:JobNumber = p_web.GSV('job:ref_number')
                SET(joo:JobNumberKey,joo:JobNumberKey)
                LOOP
                    IF Access:JobOutFL.Next()
                        BREAK
                    END
                    IF joo:JobNumber <> p_web.GSV('job:ref_number')
                        BREAK
                    END
                 !Do not include free text Out Faults
                    If p_web.GSV('job:Warranty_Job') = 'YES' AND fail# = 0
                        If joo:FaultCode = '0'
                      !JOFL# = 0
                            CYCLE
                        End !If joo:FaultCode <> 0

                        IF SUB(joo:FaultCode,1,1) = '*'
                      !JOFL# = 0
                            CYCLE
                        End !If joo:FaultCode <> 0
                    END !If p_web.GSV('job:Warranty_Job = 'YES' AND fail# = 0
                    jofl# = 1
                END
                IF JOFL# = 0
                    Access:MANFAUPA.ClearKey(map:MainFaultKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:MainFault    = 1
                        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                        !Found
                            If p_web.GSV('job:Chargeable_Job') = 'YES'
                                If Local.OutFaultPart(map:Field_Number,'C')
                                    JOFL# = 1
                                End !If Local.OutFaultPart(map:Field_Number,'C')
                            End !If p_web.GSV('job:Chargeable_Job = 'YES'

                            If p_web.GSV('job:Warranty_Job') = 'YES' And JOFL# = 0
                                If Local.OutFaultPart(map:Field_Number,'W')
                                    JOFL# = 1
                                End !If Local.OutFaultPart(map:Field_Number,'W')
                            End !If p_web.GSV('job:Warranty_Job = 'YES'

                        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                    End !If JOFL#
                        If JOFL# = 0
                            error# = 1
                            locErrorMessage = Clip(locErrorMessage) & '<13,10>Out Faults Missing.'
                        End!IF JOFL# = 0
                    End !If ForceInvoiceText(func:Type)
                End
            END

            If ForceIMEI(func:Type)
                If p_web.GSV('job:ESN') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>I.M.E.I. Number Missing.'
                End!If p_web.GSV('job:ESN = ''
            End !If ForceIMEI(func:Type)

            If ForceMSN(p_web.GSV('job:Manufacturer'),func:Type)
                If p_web.GSV('job:MSN') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>M.S.N. Missing.'
                End!If p_web.GSV('job:MSN = ''
            End !If ForceMSN(func:Type)

    !Check I.M.E.I. Number Length
            If CheckLength(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),'IMEI')
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>I.M.E.I. Number Incorrect Length.'
            End !If CheckLength(p_web.GSV('job:ESN,p_web.GSV('job:Model_Number,'IMEI')

            If MSNRequired(p_web.GSV('job:Manufacturer')) = Level:Benign
                If CheckLength(p_web.GSV('job:MSN'),p_web.GSV('job:Model_Number'),'MSN')
                    Error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>M.S.N. Incorrect Length.'
                End !If CheckLength(p_web.GSV('job:MSN,p_web.GSV('job:Model_Number,'MSN')
                If man:ApplyMSNFormat
                    If CheckFaultFormat(p_web.GSV('job:MSN'),man:MSNFormat)
                        Error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>M.S.N. Incorrect Format.'
                    End !If CheckFaultFormat(p_web.GSV('job:MSN,man:MSNFormat)
                End !If man:ApplyMSNFormat
            End !If MSNRequired(p_web.GSV('job:Manufacturer) = Level:Benign

            If (def:Force_Job_Type = 'B' And func:Type = 'B') Or |
                (def:Force_Job_Type <> 'I' And func:Type = 'C')
                If p_web.GSV('job:Chargeable_Job') <> 'YES' And p_web.GSV('job:Warranty_Job') <> 'YES'
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Job Type(s) Missing.'
                End!If p_web.GSV('job:Chargeable_Job <> 'YES' And p_web.GSV('job:Warranty_Job <> 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Charge_Type') = ''
                    Error# = 1
                    locErrorMessage   = Clip(locErrorMessage) & '<13,10>Chargeable Charge Type Missing.'
                End !If p_web.GSV('job:Chargeable_Job = 'YES' And p_web.GSV('job:Charge_Type = ''
                If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Warranty_Charge_Type') = ''
                    Error# = 1
                    locErrorMessage   = Clip(locErrorMessage) & '<13,10>Warranty Charge Type Missing.'
                End !If p_web.GSV('job:Warranty_Job = 'YES' and p_web.GSV('job:Warranty_Charge_Type = ''
            End!If def:Force_Job_Type = 'B'

            If (def:Force_Engineer = 'B' And func:Type = 'B') Or |
                (def:Force_Engineer <> 'I' And func:Type = 'C')
                If p_web.GSV('job:Engineer') = '' And p_web.GSV('job:Workshop') = 'YES'
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Engineer Missing.'
                End!If p_web.GSV('job:Engineer = ''
            End!If def:Force_Engineer = 'B'

            If ForceRepairType(func:Type)
                If (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Repair_Type') = '') Or |
                    (p_web.GSV('job:Warranty_Job') = 'YES' And p_web.GSV('job:Repair_Type_Warranty') = '')
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Repair Type(s) Missing.'
                Else
            !Check to see if the repair type is an "Exchange Fee" or "Handling Fee" one.
                    If p_web.GSV('job:Chargeable_Job') = 'YES'
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = p_web.GSV('job:Repair_Type')
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Found
                            If rtd:BER = 10
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Chargeable Repair Type is for "Exchange Fee".'
                            End !If rtd:BER = 10 or rtd:BER = 11
                            If rtd:BER = 11
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Chargeable Repair Type is for "Handling Fee".'
                            End !If rtd:BER = 11
                        Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    End !If p_web.GSV('job:Chargeable_Job = 'YES'
                    If p_web.GSV('job:Warranty_Job') = 'YES'
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                        rtd:Warranty   = 'YES'
                        rtd:Repair_Type  = p_web.GSV('job:Repair_Type_Warranty')
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                    !Found
                            If rtd:BER = 10
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Warranty Repair Type is for "Exchange Fee".'
                            End !If rtd:BER = 10 or rtd:BER = 11
                            If rtd:BER = 11
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Warranty Repair Type is for "Handling Fee".'
                            End !If rtd:BER = 11
                        Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    End !If p_web.GSV('job:Chargeable_Job = 'YES'
                End
            End!If def:Force_Repair_Type = 'B'

            If ForceAuthorityNumber(func:Type)
                If p_web.GSV('job:Authority_Number') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Authority Number Missing.'
                End
            Else
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    Access:CHARTYPE.Clearkey(cha:Warranty_Key)
                    cha:Charge_Type = p_web.GSV('job:Charge_Type')
                    cha:Warranty    = 'NO'
                    If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                !Found
                        If cha:ForceAuthorisation
                            If p_web.GSV('job:Authority_Number') = ''
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Authority Number Missing.'
                            End !If p_web.GSV('job:Authority_Number = ''
                        End !If cha:ForceAuthorisation
                    Else ! If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                !Error
                    End !If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                End !If p_web.GSV('job:Chargeable_Job = 'YES'
            End !If ForceAuthorityNumber(func:Type)


            If ForceIncomingCourier(func:Type)
                If p_web.GSV('job:Incoming_Courier') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Incoming Courier Missing.'
                End!If p_web.GSV('job:Incoming_Courier = ''
            End !If ForceIncomingCourier(func:Type)

            If ForceCourier(func:Type)
                If p_web.GSV('job:Courier') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Courier Missing.'
                End!If p_web.GSV('job:Courier = ''
            End !If ForceCourier(func:Type)

            If ForceDOP(p_web.GSV('job:Transit_Type'),p_web.GSV('job:Manufacturer'),p_web.GSV('job:Warranty_Job'),func:Type)
                If p_web.GSV('job:DOP') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>DOP Missing, required by Transit Type.'
                End !If p_web.GSV('job:DOP = ''
            End !If ForceDOP(p_web.GSV('job:Transit_Type,p_web.GSV('job:Manufacturer,p_web.GSV('job:Warranty_Job,func:Type)

            If ForceLocation(p_web.GSV('job:Transit_Type'),p_web.GSV('job:Workshop'),func:Type)
                If p_web.GSV('job:Location') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Location Missing, required by Transit Type.'
                End !If p_web.GSV('job:Location = ''
            End !If ForceLocation(p_web.GSV('job:Transit_Type,p_web.GSV('job:Workshop,func:Type)

            If ForceCustomerName(p_web.GSV('job:Account_Number'),func:Type)
                If p_web.GSV('job:Initial') = '' And p_web.GSV('job:Title') = '' And p_web.GSV('job:Surname') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Customer Name Missing.'
                End!If p_web.GSV('job:Initial = '' And p_web.GSV('job:Title = '' And p_web.GSV('job:Surname = ''
            End !If ForceCustomerName(p_web.GSV('job:Account_Number,func:Type)

            If ForcePostcode(func:type)
                If p_web.GSV('job:Postcode') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Postcode Missing.'
                End!If p_web.GSV('job:Postcode = ''
            End !If ForcePostcode(func:type)

            If ForceDeliveryPostcode(func:Type)
                If p_web.GSV('job:Postcode_Delivery') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Delivery Postcode Missing.'
                End!If p_web.GSV('job:Postcode_Delivery = ''
            End !If ForceDeliveryPostcode(func:Type)

            Error# = local.ValidateFaultCodes()

    !The Trade Fault codes, should be independent of Charge Types,
    !therefore, they are compulsory if the boxes are ticked.

            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = p_web.GSV('job:account_number')
            if access:subtracc.fetch(sub:account_number_key) = level:benign
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number = sub:main_account_number
                If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
                End!If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
            End!if access:subtracc.fetch(sub:account_number_key) = level:benign
            If ForceNetwork(func:Type)
                If p_web.GSV('jobe:Network') = ''
                    Error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Network Missing.'
                End !If p_web.GSV('jobe::Network = ''
            End !If ForceNetwork(func:Type)


    !Check For Adjustments only at completion

            LookForParts# = 0
            If p_web.GSV('job:warranty_job') = 'YES' And func:Type = 'C' !and tmp:ForceFaultCodes = 1
                access:manufact.clearkey(man:manufacturer_key)
                man:manufacturer  = p_web.GSV('job:manufacturer')
                If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Found
                    If man:forceparts
                        LookForParts# = 1

                    End!If man:forceparts


                Else! If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Error
                End! If access:.tryfetch(man:manufacturer_key) = Level:Benign

                Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
                rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                rtd:Repair_Type  = p_web.GSV('job:Repair_Type_Warranty')
                If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
            !Found
                    If rtd:ForceAdjustment
                        LookForParts# = 1
                    End !If rtd:ForceAdjustment
                Else!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign


                If LookForParts#
                    setcursor(cursor:wait)
                    parts# = 0
                    access:warparts.clearkey(wpr:part_number_key)
                    wpr:ref_number  = p_web.GSV('job:ref_number')
                    set(wpr:part_number_key,wpr:part_number_key)
                    loop
                        if access:warparts.next()
                            break
                        end !if
                        if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                            then break.  ! end if
                        parts# = 1
                        Break
                    end !loop
                    setcursor()
                    If parts# = 0
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Warranty Spares Missing'
                    End !If found# = 0
                End !If LookForParts#
            End!If p_web.GSV('job:warranty_job = 'YES'

            If (def:Force_Spares = 'B' And func:Type = 'B') Or |
                (def:Force_Spares <> 'I' And func:Type = 'C')
                If p_web.GSV('job:chargeable_job')    = 'YES'
                    setcursor(cursor:wait)
                    parts# = 0
                    access:parts.clearkey(par:part_number_key)
                    par:ref_number  = p_web.GSV('job:ref_number')
                    set(par:part_number_key,par:part_number_key)
                    loop
                        if access:parts.next()
                            break
                        end !if
                        if par:ref_number  <> p_web.GSV('job:ref_number')      |
                            then break.  ! end if
                        parts# = 1
                        Break
                    end !loop
                    setcursor()
                    If parts# = 0
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Chargeable Spares Missing'
                    End !If found# = 0
                End !If p_web.GSV('job:job_Type = 'CHARGEABLE'
            End!If def:Force_Spares = 'B'

    !Qa before Completion

    ! Set Key Repair to "1". Turn it off, if Key Repair Is Required.
            tmp:KeyRepair = 1
            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = p_web.GSV('job:Manufacturer')
            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
                If man:UseQA And func:Type = 'C'
                    If ~man:QAAtCompletion And p_web.GSV('job:QA_Passed') <> 'YES'
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>QA Has Not Been Passed'

                    End !If man:QAAtCompletion And p_web.GSV('job:QA_Passed <> 'YES'
                End !If man:UseQA
                If man:KeyRepairRequired And func:Type = 'C'
                    tmp:KeyRepair = 0
                End ! If man:KeyRepairRequired
            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    !Parts Remaining
            tmp:FoundRequestedParts = 0
            tmp:FoundOrderedParts = 0

    !Also check if parts have not been removed,
    !if rapid stock is being used
            RestockPart# = 0

            CountParts# = 0
            setcursor(cursor:wait)
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = p_web.GSV('job:ref_number')
            Set(wpr:part_number_key,wpr:part_number_key)
            Loop
                if access:warparts.next()
                    break
                end !if
                if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                    then break.  ! end if
        !Do not validate a Exchange unit
                If wpr:Part_Number = 'EXCH'
                    Cycle
                End !If wpr:Part_Number = 'EXCH'

        ! Inserting (DBH 23/11/2007) # 9568 - Only check key repair if there ARE parts attached
                CountParts# += 1
        ! End (DBH 23/11/2007) #9568

        !Bodge to make Fault Code 3 compulsory for warranty adjustments for Siemens
                If p_web.GSV('job:Manufacturer') = 'SIEMENS' and func:Type = 'C'
                    If wpr:Fault_Code3 = '' and wpr:Adjustment = 'YES'
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code 3 Missing On Warranty Adjustment'
                    End !If wpr:Fault_Code3 = '' and wpr:Adjustment = 'YES'
                End !If p_web.GSV('job:Manufacturer = 'SIEMENS'
                
                If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
                    p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'),|
                    p_web.GSV('job:Repair_Type'),p_web.GSV('job:Repair_Type_Warranty'),'W', |
                    p_web.GSV('job:Manufacturer'))
                    Do WarrantyPartFaultCodes
                Else
            ! This may be wrong, but if Fault Codes aren't required, don't check for Key Repair (DBH: 06/11/2007)
                    tmp:KeyRepair = 1
                End!If tmp:ForceFaultCodes

                If wpr:Status = 'RET'
                    RestockPart# += 1
                End !If wpr:Status = 'RET'

                If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                    tmp:FoundRequestedParts = 1
                    Break
                End
                If wpr:order_number <> '' And wpr:date_received = ''
                    tmp:FoundOrderedParts = 1
                    Break
                End
            End !loop
            access:warparts.restorefile(save_wpr_id)
            setcursor()

! Changing (DBH 23/11/2007) # 9568 - Only check for Key Repair if there are parts attached
!    If tmp:KeyRepair = 0
! to (DBH 23/11/2007) # 9568
            If tmp:KeyRepair = 0 And CountParts# > 0
! End (DBH 23/11/2007) #9568
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>None of the warranty parts have been marked as "Key Repair".'
            End ! If tmp:KeyRepair = 0

            If func:Type = 'C'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = p_web.GSV('job:ref_number')
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                        break
                    end !if
                    if par:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if

                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                    If par:Part_Number <> 'EXCH'
                        If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
                            p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'),|
                            p_web.GSV('job:Repair_Type'),p_web.GSV('job:Repair_Type_Warranty'),'X', |
                            p_web.GSV('job:Manufacturer'))
                            Do ChargeablePartFaultCodes
                        End !If ForceFaultCodes(p_web.GSV('job:Chargeable_Job,p_web.GSV('job:Warranty_Job,|
                    End!If tmp:ForceFaultCodes
                    If (useStockAllocation(sto:Location) & par:Status = 'RET')
                        RestockPart# += 1
                    End !If wpr:Status = 'RET'

                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        tmp:FoundRequestedParts = 1
                        Break
                    End
                    If par:order_number <> '' And par:date_received = ''
                        tmp:FoundOrderedParts = 1
                        Break
                    End

                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()

                If tmp:FoundRequestedParts = 1
                    error# = 1
                    If def:SummaryOrders
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>There are Unreceived spares attached'
                    Else !If def:SummaryOrders
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>There are Unordered spares attached'
                    End !If def:SummaryOrders

                Elsif tmp:FoundOrderedParts = 1
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>There are Unreceived spares attached'
                End

                If RestockPart#
                    Error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>There are spares that have NOT been returned to stock by Allocations'
                End !If RestockPart#
            End!If func:Type = 'C'


            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = p_web.GSV('job:Account_Number')
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
                    If tra:Invoice_Sub_Accounts = 'YES'
                        If sub:ForceOrderNumber and p_web.GSV('job:Order_Number') = ''
                            error# = 1
                            locErrorMessage   = Clip(locErrorMessage) & '<13,10>Order Number Missing.'
                        End !If tra:ForceOrderNumber
                    Else !If tra:Invoice_Sub_Accounts
                        If tra:ForceOrderNumber and p_web.GSV('job:Order_Number') = ''
                            error# = 1
                            locErrorMessage   = Clip(locErrorMessage) & '<13,10>Order Number Missing.'
                        End !If tra:ForceOrderNumber
                    End !If tra:Invoice_Sub_Accounts
                Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

            If ForceAccessories(func:Type)
                If NoAccessories(p_web.GSV('job:Ref_Number'))
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Job Accessories Missing.'
                End !If AccError# = 1
            End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B'


            If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                If p_web.GSV('job:Chargeable_Job') = 'YES' and func:Type = 'C'
                    If p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' And p_web.GSV('job:Estimate_Rejected') <> 'YES'
                        Error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Estimate has not been Accepted/Rejected.'
                    End !If p_web.GSV('job:Estimate = 'YES' and (p_web.GSV('job:Estiamte_Accepted <> 'YES' Or p_web.GSV('job:Estimate_Rejected <> 'YES')
                End !If p_web.GSV('job:Chargeable_Job = 'YES'
            End !If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1

            p_web.SSV('locCompleteRepair',clip(p_web.GSV('locCompleteRepair')) & clip(locErrorMessage))

            do closeFiles
ChargeablePartFaultCodes      Routine
    !Warranty Parts Fault Codes
    !Before we start Check if PART is an accessory

    Save_map_ID = Access:MANFAUPA.SaveFile()
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:Manufacturer')
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.NEXT()
           Break
        End !If
        If map:Manufacturer <> p_web.GSV('job:Manufacturer')      |
            Then Break.  ! End If
        !Check ok!
        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
          !Error!
        ELSE
          IF sto:Accessory <> 'YES' AND map:Manufacturer = 'MOTOROLA' !ALERT WORKAROUND!
            Skip_non_main = TRUE
          END
        END
        IF Skip_non_Main = TRUE
          IF map:MainFault = FALSE
            CYCLE
          END
        END
        Case map:Field_Number
            Of 1
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code1 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'
                If map:restrictlength
                    If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength

                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 2
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code2 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code2)) < map:lengthfrom or Len(Clip(par:Fault_Code2)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code2,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

            Of 3
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code3 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code3)) < map:lengthfrom or Len(Clip(par:Fault_Code3)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code3,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 4
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code4 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code4)) < map:lengthfrom or Len(Clip(par:Fault_Code4)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code4,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 5
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code5 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code5)) < map:lengthfrom or Len(Clip(par:Fault_Code5)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code5,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 6
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code6 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code6)) < map:lengthfrom or Len(Clip(par:Fault_Code6)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code6,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 7
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code7 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code7)) < map:lengthfrom or Len(Clip(par:Fault_Code7)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code7,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 8
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code8 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code8)) < map:lengthfrom or Len(Clip(par:Fault_Code8)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code8,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 9
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code9 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code9)) < map:lengthfrom or Len(Clip(par:Fault_Code9)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code9,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 10
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code10 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code10)) < map:lengthfrom or Len(Clip(par:Fault_Code10)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code10,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat



            Of 11
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code11 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code11)) < map:lengthfrom or Len(Clip(par:Fault_Code11)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code11,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 12
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code12 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code12)) < map:lengthfrom or Len(Clip(par:Fault_Code12)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code12,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

        End !Case map:Field_Number
    End !Loop
    Access:MANFAUPA.RestoreFile(Save_map_ID)
WarrantyPartFaultCodes      Routine
Data
local:FaultCode     String(30),Dim(12)
Code
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number = wpr:Part_Ref_Number
    If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    Clear(local:FaultCode)

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:Field_Number = 0
                        Set(map:Field_Number_Key,map:Field_Number_Key)
                        Loop
                            If Access:MANFAUPA.Next()
                                Break
                            End ! If Access:MANFAUPA.Next()
                            If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                                Break
                            End ! If map:Manufacturer <> job:Manufacturer
                            If map:NotAvailable
                                Cycle
                            End ! If map:NotAvailable

                            Case map:Field_Number
                            Of 1
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code1
                            Of 2
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code2
                            Of 3
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code3
                            Of 4
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code4
                            Of 5
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code5
                            Of 6
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code6
                            Of 7
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code7
                            Of 8
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code8
                            Of 9
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code9
                            Of 10
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code10
                            Of 11
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code11
                            Of 12
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code12
                            End ! Case map:Field_Number
                        End ! Loop

                        
                        Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:Field_Number = 0
                        Set(map:Field_Number_Key,map:Field_Number_Key)
                        Loop
                            If Access:MANFAUPA.Next()
                                Break
                            End ! If Access:MANFAUPA.Next()
                            If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                                Break
                            End ! If map:Manufacturer <> job:Manufacturer

                            If map:MainFault = 0
                                If sto:Accessory <> 'YES' And man:ForceAccessoryCode
                                    Cycle
                                End ! If sto:Accessory <> 'YES' And man:ForceAcessoryCode
                            End ! If map:MainFault = 0

                            If (map:Compulsory = 'YES' And wpr:Adjustment = 'NO') Or |
                                (map:Compulsory = 'YES' And wpr:Adjustment = 'YES' And map:CompulsoryForAdjustment)

! Compulsory if not Adjustment, or if Adjustment and fault marked as compulsory FOR adjustment
                                If local:FaultCode[map:Field_Number] = ''
                                    Error# = 1
                                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                                End ! If local:FaultCode[map:Field_Number] = ''
                            End ! If map:Compulsory = 'YES'

                            If map:RestrictLength
                                If Len(Clip(local:FaultCode[map:Field_Number])) < map:LengthFrom Or Len(Clip(local:FaultCode[map:Field_Number])) > map:LengthTo
                                    Error# = 1
                                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                                End ! If Len(Clip(local:FaultCode[map:Field_Number])) < map:LengthFrom Or Len(Clip(local:FaultCode[map:Field_Number])) > map:LengthTo
                            End ! If map:RestrictLength

                            If map:ForceFormat = 'YES'
                                If CheckFaultFormat(local:FaultCode[map:Field_Number],map:FieldFormat)
                                    Error# = 1
                                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                                End ! If CheckFaultFormat(local:FaultCode[map:Field_Number],map:FieldFormat)
                            End ! If map:ForceFormat = 'YES'

                            If map:KeyRepair
                                If local:FaultCode[map:Field_Number] = 1
! The Key Repair Fault Code should only by 1 or 0.
                                    tmp:KeyRepair = 1
                                End ! If local:FaultCode[map:Field_Number] <> ''
                            End ! If map:KeyRepair
                        End ! Loop
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRANTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRANTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRANTYPE.Close
     Access:MANFAUPA.Close
     Access:STOCK.Close
     Access:WARPARTS.Close
     Access:SUBTRACC.Close
     Access:PARTS.Close
     Access:TRADEACC.Close
     Access:CHARTYPE.Close
     Access:JOBOUTFL.Close
     Access:REPTYDEF.Close
     Access:MANUFACT.Close
     Access:DEFAULTS.Close
     FilesOpened = False
  END
Local.OutFaultPart      Procedure(Long  func:FieldNumber,String func:JobType)
Code
    Case func:JobType
        Of 'C'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If
                Case func:FieldNumber
                    Of 1
                        If par:Fault_Code1 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                    Of 2
                        If par:Fault_Code2 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 3
                        If par:Fault_Code3 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 4
                        If par:Fault_Code4 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 5
                        If par:Fault_Code5 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 6
                        If par:Fault_Code6 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 7
                        If par:Fault_Code7 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 8
                        If par:Fault_Code8 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 9
                        If par:Fault_Code9 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 10
                        If par:Fault_Code10 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 11
                        If par:Fault_Code11 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 12
                        If par:Fault_Code12 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                End !Case map:Field_Number
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            Return Found#
        Of 'W'
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If
                Case func:FieldNumber
                    Of 1
                        If wpr:Fault_Code1 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                    Of 2
                        If wpr:Fault_Code2 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 3
                        If wpr:Fault_Code3 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 4
                        If wpr:Fault_Code4 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 5
                        If wpr:Fault_Code5 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 6
                        If wpr:Fault_Code6 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 7
                        If wpr:Fault_Code7 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 8
                        If wpr:Fault_Code8 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 9
                        If wpr:Fault_Code9 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 10
                        If wpr:Fault_Code10 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 11
                        If wpr:Fault_Code11 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 12
                        If wpr:Fault_Code12 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                End !Case map:Field_Number
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)

            Return Found#

    End !Case func:Type
local.ValidateFaultCodes        Procedure()
local:PartFaultCode     String(30),Dim(12)
local:CExcludeFromEDI           Byte(0)
local:WExcludeFromEDI           Byte(0)
Code
    Error# = 0
    ChaComp# = False
    WarComp# = False
    If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
            p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'), |
        p_web.GSV('job:Repair_type'),p_web.GSV('job:Repair_Type_Warranty'),'C', |
        p_web.GSV('job:Manufacturer'))
        ChaComp# = True
        ! Inserting (DBH 01/05/2008) # 9723 - Fault codes should be forced. Check exclude from edi flag
        Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
        rtd:Chargeable = 'YES'
        rtd:Repair_Type = p_web.GSV('job:Repair_Type')
        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
            ! Found
            If rtd:ExcludeFromEDI = 1
                local:CExcludeFromEDI = 1
            End ! If rtd:ExcludeFromEDI = 1
        Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        ! End (DBH 01/05/2008) #9723


    End ! p_web.GSV('job:Warranty_Charge_Type,p_web.GSV('job:Repair_type,p_web.GSV('job:Repair_Type_Warranty,'C')

    If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
                p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'),|
        p_web.GSV('job:Repair_type'),p_web.GSV('job:Repair_Type_Warranty'),'W', |
        p_web.GSV('job:Manufacturer'))
        WarComp# = True
        ! Inserting (DBH 01/05/2008) # 9723 - Fault codes should be forced. Check exclude from edi flag
        Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
        rtd:Warranty = 'YES'
        rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
            ! Found
            If rtd:ExcludeFromEDI = 1
                local:WExcludeFromEDI = 1
            End ! If rtd:ExcludeFromEDI = 1
        Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        ! End (DBH 01/05/2008) #9723
    End ! p_web.GSV('job:Warranty_Charge_Type,p_web.GSV('job:Repair_type,p_web.GSV('job:Repair_Type_Warranty,'C')

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = p_web.GSV('wob:HeadACcountNumber')
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location = tra:SiteLocation
        If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
        End ! If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign


    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = p_web.GSV('job:manufacturer')
    maf:Field_Number = 0
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> p_web.GSV('job:manufacturer')      |
            then break.  ! end if

! Changing (DBH 09/11/2007) # 9278 - Logic improved (i hope)
!        If (maf:Compulsory_At_Booking = 'YES' And ~maf:CompulsoryIfExchange And WarComp# = True) Or |  !Maybe long winded but sometimes logic fails me
!            (maf:Compulsory_At_Booking = 'YES' And maf:CompulsoryIfExchange And p_web.GSV('job:Exchange_Unit_Number <> 0 And WarComp# = True) Or |
!            (maf:Compulsory = 'YES' And func:Type = 'C' And ~maf:CompulsoryIfExchange And WarComp# = True) Or |
!            (maf:Compulsory = 'YES' And func:Type = 'C' And maf:CompulsoryIfExchange And p_web.GSV('job:Exchange_Unit_Number <> 0 And WarComp# = True) Or |
!            (maf:CharCompulsoryBooking = True And ChaComp# = True) Or |
!            (maf:CharCompulsory = True And func:Type = 'C' And ChaComp# = True)
! to (DBH 09/11/2007) # 9278
        Continue# = 0
        If WarComp# = 1
            ! Warranty p_web.GSV('job: Force Fault Codes (DBH: 09/11/2007)
            If maf:CompulsoryIfExchange = 0
                ! Doesn't matter if exchange is attached (DBH: 09/11/2007)
                If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                    ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                    Continue# = 1
                End ! If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
            Else ! If maf:CompulsoryIfExchange = 0
                ! Only forced if exchange attached (DBH: 09/11/2007)
                If p_web.GSV('job:Exchange_Unit_Number') <> 0
                    If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                        ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                        Continue# = 1
                    End ! If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                End ! If p_web.GSV('job:Exchange_Unit_Number <> 0
            End ! If maf:CompulsoryIfExchange = 0
            If Continue# = 1
                ! Inserting (DBH 06/05/2008) # 9723 - Only force fault code if the warranty repair type matches
                If maf:CompulsoryForRepairType = 1
                    If maf:CompulsoryRepairType <> p_web.GSV('job:Repair_Type_Warranty')
                        ! Fault Code is only compulsory when the repair type matches (DBH: 06/05/2008)
                        Continue# = 0
                    End ! If maf:CompulsoryRepairType <> p_web.GSV('job:Repair_Type_Warranty
                End ! If maf:CompulsoryForRepairType = 1
                ! End (DBH 06/05/2008) #9723
            End ! If Continue# = 1
        End ! If WarComp# = 1

        If ChaComp# = 1
            ! Chargeable p_web.GSV('job: Force Fault Codes (DBH: 09/11/2007)
            If maf:CharCompulsoryBooking = 1 Or (maf:CharCompulsory = 1 and func:Type = 'C')
                ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                Continue# = 1
            End ! If maf:Compulsory_At_Booking = 'YES' or (maf:Compulsory = 'YES' and func:Type = 'C')
        End ! If ChaComp# = 1

        If Continue# = 0
            ! Check all the part fault codes to see if any of the value mean this fault code is forced (DBH: 09/11/2007)
            Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
            wpr:Ref_Number = p_web.GSV('job:Ref_Number')
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.Next()
                    Break
                End ! If Access:WARPARTS.Next()
                If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                    Break
                End ! If wpr:Ref_Number <> p_web.GSV('job:Ref_Number

                ! Save the fault codes in an easy format (DBH: 09/11/2007)
                local:PartFaultCode[1] = wpr:Fault_Code1
                local:PartFaultCode[2] = wpr:Fault_Code2
                local:PartFaultCode[3] = wpr:Fault_Code3
                local:PartFaultCode[4] = wpr:Fault_Code4
                local:PartFaultCode[5] = wpr:Fault_Code5
                local:PartFaultCode[6] = wpr:Fault_Code6
                local:PartFaultCode[7] = wpr:Fault_Code7
                local:PartFaultCode[8] = wpr:Fault_Code8
                local:PartFaultCode[9] = wpr:Fault_Code9
                local:PartFaultCode[10] = wpr:Fault_Code10
                local:PartFaultCode[11] = wpr:Fault_Code11
                local:PartFaultCode[12] = wpr:Fault_Code12

                Loop x# = 1 To 12
                    If local:PartFaultCode[x#] <> ''
                        Access:MANFPALO.Clearkey(mfp:Field_Key)
                        mfp:Manufacturer = p_web.GSV('job:Manufacturer')
                        mfp:Field_Number = x#
                        mfp:Field = local:PartFaultCode[x#]
                        If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                            If mfp:ForceJobFaultCode
                                If mfp:ForceFaultCodeNumber = maf:Field_Number
                                    Continue# = 1
                                    Break
                                End ! If mfp:ForceFaultCodeNumber = 1
                            End ! If mfp:ForceJobFaultCode
                        End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                    End ! If local:PartFaultCode[x#] <> ''
                End ! Loop x# = 1 To 12
                If Continue# = 1
                    Break
                End ! If Continue# = 1
            End ! Loop
        End ! If Continue# = 0

        If Continue# = 0
            Cycle
        End ! If Continue# = 0
! End (DBH 09/11/2007) #9278

!            Stop('maf:Compulsory_At_Booking: ' & maf:Compulsory_At_Booking & |
!                '<13,10>maf:CompulsoryIfExchange: ' & maf:CompulsoryIfExchange & |
!                '<13,10>p_web.GSV('job:Exchange_Unit_Number: ' & p_web.GSV('job:Exchange_Unit_Number & |
!                '<13,10>maf:Compulsory: ' & maf:Compulsory & |
!                '<13,10>maf:CharCompulsory: ' & maf:CharCompulsory & |
!                '<13,10>maf:CharCompulsoryBooking:' & maf:CharCompulsoryBooking & |
!                '<13,10>ChaComp#: ' & ChaComp# & |
!                '<13,10>WarComp#: ' & WarComp# & |
!                '<13,10,13,10>maf:Field_Name: ' & maf:Field_Name)

        ! Inserting (DBH 14/03/2007) # 8718 - Do not force fault codes (apart from in and out faults) for obf jobs,
        If ~man:UseFaultCodesForOBF And p_web.GSV('jobe:OBFValidated') = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <> 1
        End ! If ~man:UseFaultCodeForOBF
        ! End (DBH 14/03/2007) #8718

        ! Inserting (DBH 01/05/2008) # 9723 - If exclude from edi and fault code forced, only force the in/out fault
        If WarComp# = 1 And local:WExcludeFromEDI = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <>1
        End ! If WarComp# = 1 And local:WExcludeFromEDI = 1
        If ChaComp# = 1 And local:CExcludeFromEDI = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <> 1
        End ! If ChaComp# = 1 And local:CExcludeFromEDI = 1
        ! End (DBH 01/05/2008) #9723

        ! An old override. Don't know why this exists, but thought it safer not to delete (DBH: 09/11/2007)
        If p_web.GSV('job:Manufacturer') = 'ERICSSON' and p_web.GSV('job:DOP') = ''
            Cycle
        End ! If p_web.GSV('job:Manufacturer = 'ERICSSON' and p_web.GSV('job:DOP = ''

        ! If fault code is hidden due to another code being blank, then don't check (DBH: 09/11/2007)
        If maf:HideRelatedCodeIfBlank
            If tmp:FaultCode[maf:BlankRelatedCode] = ''
                Cycle
            End ! If tmp:FaultCode[maf:BlankRelatedCode} = ''
        End ! If maf:HideRelatedCodeIfBlank

        ! Is the fault code restricted by site location (DBH: 09/11/2007)
        If maf:RestrictAvailability
            If (maf:RestrictServiceCentre = 1 And ~loc:Level1) Or |
                (maf:RestrictServiceCentre = 2 And ~loc:Level2) Or |
                (maf:RestrictServiceCentre = 3 And ~loc:Level3)
                Cycle
            End ! (maf:RestrictServiceCentre = 3 And ~loc:Level3)
        End ! If maf:RestrictAvailability

        If maf:RestrictLength
            If Len(Clip(tmp:FaultCode[maf:Field_Number])) < maf:LengthFrom Or Len(Clip(tmp:FaultCode[maf:Field_Number])) > maf:LengthTo
                error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
            End ! If Len(Clip(tmp:FaultCode[maf:Field_Number])) < maf:LengthFrom Or Len(Clip(tmp:FaultCode[maf:Field_Number])) > maf:LengthTo
        End ! If maf:RetrictLength

        If maf:ForceFormat
            If CheckFaultFormat(tmp:FaultCode[maf:Field_Number],maf:FieldFormat)
                error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
            End ! If CheckFaultFormat(tmp:FaultCode[maf:Field_Number],maf:FieldFormat)
        End ! If maf:ForceFormat

        If tmp:FaultCode[maf:Field_Number] = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
        End ! If tmp:FaultCode[maf:Field_Number] = ''

    End !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()

    Return Error#
NoAccessories        PROCEDURE  (func:refNumber)           ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    retValue# = 0

    do openFiles
    found# = 0
    Access:JOBACC.Clearkey(jac:ref_Number_Key)
    jac:ref_Number    = func:refNumber
    set(jac:ref_Number_Key,jac:ref_Number_Key)
    loop
        if (Access:JOBACC.Next())
            Break
        end ! if (Access:JOBACC.Next())
        if (jac:ref_Number    <> func:refNumber)
            Break
        end ! if (jac:ref_Number    <> func:refNumber)
        found# = 1
        break
    end ! loop

    do closeFiles

    if (found# = 0)
        retValue# = 1
    end ! if (found# = 0)

    return retValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBACC.Close
     FilesOpened = False
  END
CheckParts           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locReturnValue       BYTE                                  !
FilesOpened     BYTE(0)
  CODE
    p_web.SSV('CheckParts:ReturnValue',0)

    do openFiles

    do CheckParts

    do closeFiles

    p_web.SSV('CheckParts:ReturnValue',locReturnValue)
checkParts    routine
    Data
tmp:FoundReceived   Byte()
    Code
!Return (1) = Pending Order
!Return (2) = On Order
!Return (3) = Back Order Spares
!Return (4) = Parts Recevied
!Return (0) = Received Order Or No Parts With Anything To Do WIth Orders
    tmp:FoundReceived = False
    locReturnValue = 0
    Case p_web.GSV('CheckParts:Type')
        Of 'C'
            If p_web.GSV('job:Chargeable_Job') = 'YES'
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = p_web.GSV('job:ref_number')
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = par:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = par:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If par:order_number <> ''
                        If par:date_received = ''
                            locReturnValue = 2
                            exit

                        Else
                            tmp:FoundReceived = True
                        End!If par:date_received = ''
                    End!If par:order_number <> ''
                end !loop
            End ! If p_web.GSV('job:Chargeable_Job = 'YES'

        Of 'W'
            If p_web.GSV('job:Warranty_Job') = 'YES'
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = p_web.GSV('job:ref_number')
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If wpr:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = wpr:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = wpr:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)

                    If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If wpr:order_number <> ''
                        If wpr:date_received = ''
                            locReturnValue = 2
                            exit
                        Else!If wpr:date_recieved = ''
                            tmp:FoundReceived = True
                        End!If wpr:date_recieved = ''
                    End!If wpr:order_number <> ''
                end !loop

            End ! If p_web.GSV('job:Warranty_Job = 'YES'

    End!Case f_type

    If tmp:FoundReceived = True
        !All parts received - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 4
        exit
    Else ! If tmp:FoundReceived = True
        !No parts attached - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 0
    End ! If tmp:FoundReceived = True


!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:STOCK.Close
     FilesOpened = False
  END
UpdateDateTimeStamp  PROCEDURE  (f:RefNumber)              ! Declare Procedure
JOBSTAMP::State  USHORT
FilesOpened     BYTE(0)
  CODE
    ! Inserting (DBH 05/08/2008) # 10253 - Update date/time stamp everytime a job changes
    do openfiles
    do savefiles
    Access:JOBSTAMP.ClearKey(jos:JOBSRefNumberKey)
    jos:JOBSRefNumber = f:RefNumber
    If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Found
        jos:DateStamp = Today()
        jos:TimeStamp = Clock()
        Access:JOBSTAMP.TryUpdate()
    Else ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Error
        If Access:JOBSTAMP.PrimeRecord() = Level:Benign
            jos:JOBSRefNumber = f:RefNumber
            jos:DateStamp = Today()
            jos:TimeStamp = Clock()
            If Access:JOBSTAMP.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:JOBSTAMP.TryInsert() = Level:Benign
                Access:JOBSTAMP.CancelAutoInc()
            End ! If Access:JOBSTAMP.TryInsert() = Level:Benign
        End ! If Access.JOBSTAMP.PrimeRecord() = Level:Benign
    End ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign

    do restorefiles
    do closefiles
    ! End (DBH 05/08/2008) #10253
SaveFiles  ROUTINE
  JOBSTAMP::State = Access:JOBSTAMP.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBSTAMP::State <> 0
    Access:JOBSTAMP.RestoreFile(JOBSTAMP::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSTAMP.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSTAMP.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSTAMP.Close
     FilesOpened = False
  END
JobAccessories       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FoundAccessory   STRING(1000)                          !
tmp:TheAccessory     STRING(1000)                          !
tmp:ShowAccessory    STRING(1000)                          !
tmp:TheJobAccessory  STRING(1000)                          !
FilesOpened     Long
ACCESSOR::State  USHORT
JOBSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobAccessories')
  loc:formname = 'JobAccessories_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobAccessories','')
    p_web._DivHeader('JobAccessories',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobAccessories',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCESSOR)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCESSOR)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobAccessories_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ShowAccessory')
    tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  End
  if p_web.IfExistsValue('tmp:TheAccessory')
    tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  End
  if p_web.IfExistsValue('jobe:AccessoryNotes')
    jobe:AccessoryNotes = p_web.GetValue('jobe:AccessoryNotes')
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobAccessories_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
 tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobAccessories_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobAccessories_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobAccessories_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobAccessories" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobAccessories" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobAccessories" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Accessories') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Accessories',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobAccessories">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobAccessories" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobAccessories')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Accessory Numbers') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobAccessories')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobAccessories'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ShowAccessory')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobAccessories')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobAccessories_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShowAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShowAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TheAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TheAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonAddAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAddAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonRemoveAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonRemoveAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:AccessoryNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:AccessoryNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Accessory Numbers') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobAccessories_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseAccessoryNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::textAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textAccessoryMessage',p_web.GetValue('NewValue'))
    do Value::textAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textAccessoryMessage  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('textAccessoryMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one accessory',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::tmp:ShowAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ShowAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('NewValue'))
    tmp:ShowAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShowAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('Value'))
    tmp:ShowAccessory = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::tmp:TheAccessory  !1

Value::tmp:ShowAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ShowAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ShowAccessory'',''jobaccessories_tmp:showaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ShowAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ShowAccessory',loc:fieldclass,loc:readonly,20,180,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Available Accessories ---','',choose('' = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GSV('job:Model_Number')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      If acr:Model_Number <> p_web.GSV('job:Model_Number')
          Break
      End ! If acr:Model_Number <> p_web.GSV('tmp:ModelNumber')
      If Instring(';' & Clip(acr:Accessory),p_web.GSV('tmp:TheJobAccessory'),1,1)
          Cycle
      End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('tmp:TheJobAccessory'),1,1)
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.GSV('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_value')


Prompt::tmp:TheAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TheAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('NewValue'))
    tmp:TheAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TheAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('Value'))
    tmp:TheAccessory = p_web.GetValue('Value')
  End
  do SendAlert

Value::tmp:TheAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:TheAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TheAccessory'',''jobaccessories_tmp:theaccessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:TheAccessory',loc:fieldclass,loc:readonly,20,180,0,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Accessories On Job ---','',choose('' = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('tmp:TheJobAccessory') <> ''
          Loop x# = 1 To 1000
              If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
  
              If Start# > 0
                 If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                     tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                     If tmp:FoundAccessory <> ''
                         loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                         packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                         loc:even = Choose(loc:even=1,2,1)
                     End ! If tmp:FoundAccessory <> ''
                     Start# = 0
                 End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
             tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
             If tmp:FoundAccessory <> ''
                 loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                 packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                 loc:even = Choose(loc:even=1,2,1)
             End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('tmp:TheJobAccessory') <> ''
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_value')


Prompt::buttonAddAccessories  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Job:ViewOnly') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonAddAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAddAccessories',p_web.GetValue('NewValue'))
    do Value::buttonAddAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & p_web.GSV('tmp:ShowAccessory'))
  p_web.SSV('tmp:ShowAccessory','')
  do Value::buttonAddAccessories
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::buttonAddAccessories  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_value',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAddAccessories'',''jobaccessories_buttonaddaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddAccessories','Add Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_value')


Prompt::buttonRemoveAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Job:ViewOnly') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonRemoveAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRemoveAccessory',p_web.GetValue('NewValue'))
    do Value::buttonRemoveAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  tmp:TheJobAccessory = p_web.GSV('tmp:TheJobAccessory') & ';'
  tmp:TheAccessory = '|;' & Clip(p_web.GSV('tmp:TheAccessory')) & ';'
  Loop x# = 1 To 1000
      pos# = Instring(Clip(tmp:TheAccessory),tmp:TheJobAccessory,1,1)
      If pos# > 0
          tmp:TheJobAccessory = Sub(tmp:TheJobAccessory,1,pos# - 1) & Sub(tmp:TheJobAccessory,pos# + Len(Clip(tmp:TheAccessory)),1000)
          Break
      End ! If pos# > 0#
  End ! Loop x# = 1 To 1000
  p_web.SetSessionValue('tmp:TheJobAccessory',Sub(tmp:TheJobAccessory,1,Len(Clip(tmp:TheJobAccessory)) - 1))
  p_web.SetSessionValue('tmp:TheAccessory','')
  
  do Value::buttonRemoveAccessory
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::buttonRemoveAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_value',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAccessory'',''jobaccessories_buttonremoveaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAccessory','Remove Accessory','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_value')


Prompt::jobe:AccessoryNotes  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessory Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:AccessoryNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:AccessoryNotes',p_web.GetValue('NewValue'))
    jobe:AccessoryNotes = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:AccessoryNotes
    do Value::jobe:AccessoryNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:AccessoryNotes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe:AccessoryNotes = p_web.GetValue('Value')
  End
    jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  do Value::jobe:AccessoryNotes
  do SendAlert

Value::jobe:AccessoryNotes  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jobe:AccessoryNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Job:ViewOnly') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('jobe:AccessoryNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:AccessoryNotes'',''jobaccessories_jobe:accessorynotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jobe:AccessoryNotes',p_web.GetSessionValue('jobe:AccessoryNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jobe:AccessoryNotes),'Accessory Notes',Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_value')


Validate::BrowseAccessoryNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseAccessoryNumber',p_web.GetValue('NewValue'))
    do Value::BrowseAccessoryNumber
  Else
    p_web.StoreValue('joa:RecordNumber')
  End

Value::BrowseAccessoryNumber  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAccessoryNumber --
  p_web.SetValue('BrowseAccessoryNumber:NoForm',1)
  p_web.SetValue('BrowseAccessoryNumber:FormName',loc:formname)
  p_web.SetValue('BrowseAccessoryNumber:parentIs','Form')
  p_web.SetValue('_parentProc','JobAccessories')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&'"><!-- Net:BrowseAccessoryNumber --></div><13,10>'
    p_web._DivHeader('JobAccessories_' & lower('BrowseAccessoryNumber') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('JobAccessories_' & lower('BrowseAccessoryNumber') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAccessoryNumber --><13,10>'
  end
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobAccessories_tmp:ShowAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ShowAccessory
      else
        do Value::tmp:ShowAccessory
      end
  of lower('JobAccessories_tmp:TheAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TheAccessory
      else
        do Value::tmp:TheAccessory
      end
  of lower('JobAccessories_buttonAddAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAddAccessories
      else
        do Value::buttonAddAccessories
      end
  of lower('JobAccessories_buttonRemoveAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAccessory
      else
        do Value::buttonRemoveAccessory
      end
  of lower('JobAccessories_jobe:AccessoryNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:AccessoryNotes
      else
        do Value::jobe:AccessoryNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)

PreCopy  Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.setsessionvalue('showtab_JobAccessories',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
          p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ShowAccessory')
  p_web.StoreValue('tmp:TheAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:AccessoryNotes')
  p_web.StoreValue('')
FormAccessoryNumbers PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBACCNO::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormAccessoryNumbers')
  loc:formname = 'FormAccessoryNumbers_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormAccessoryNumbers','')
    p_web._DivHeader('FormAccessoryNumbers',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBACCNO)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBACCNO)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormAccessoryNumbers_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBACCNO')
  p_web.SetValue('UpdateKey','joa:RecordNumberKey')
  p_web.SetValue('IDField','joa:RecordNumber')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormAccessoryNumbers:Primed') = 1
    p_web._deleteFile(JOBACCNO)
    p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBACCNO')
  p_web.SetValue('UpdateKey','joa:RecordNumberKey')
  If p_web.IfExistsValue('joa:AccessoryNumber')
    p_web.SetPicture('joa:AccessoryNumber','@s30')
  End
  p_web.SetSessionPicture('joa:AccessoryNumber','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormAccessoryNumbers_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormAccessoryNumbers')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAccessoryNumbers_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAccessoryNumbers_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAccessoryNumbers_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBACCNO__FileAction" value="'&p_web.getSessionValue('JOBACCNO:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBACCNO" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBACCNO" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="joa:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormAccessoryNumbers" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormAccessoryNumbers" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormAccessoryNumbers" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','joa:RecordNumber',p_web._jsok(p_web.getSessionValue('joa:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Accessory Numbers') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Accessory Numbers',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormAccessoryNumbers">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormAccessoryNumbers" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormAccessoryNumbers')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormAccessoryNumbers')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormAccessoryNumbers'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.joa:AccessoryNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormAccessoryNumbers')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAccessoryNumbers_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::joa:AccessoryNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::joa:AccessoryNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::joa:AccessoryNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::joa:AccessoryNumber  Routine
  p_web._DivHeader('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessory Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::joa:AccessoryNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('joa:AccessoryNumber',p_web.GetValue('NewValue'))
    joa:AccessoryNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = joa:AccessoryNumber
    do Value::joa:AccessoryNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('joa:AccessoryNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    joa:AccessoryNumber = p_web.GetValue('Value')
  End
  If joa:AccessoryNumber = ''
    loc:Invalid = 'joa:AccessoryNumber'
    loc:alert = p_web.translate('Accessory Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    joa:AccessoryNumber = Upper(joa:AccessoryNumber)
    p_web.SetSessionValue('joa:AccessoryNumber',joa:AccessoryNumber)
  do Value::joa:AccessoryNumber
  do SendAlert

Value::joa:AccessoryNumber  Routine
  p_web._DivHeader('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- joa:AccessoryNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('joa:AccessoryNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If joa:AccessoryNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''joa:AccessoryNumber'',''formaccessorynumbers_joa:accessorynumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','joa:AccessoryNumber',p_web.GetSessionValueFormat('joa:AccessoryNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Accessory Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_value')

Comment::joa:AccessoryNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormAccessoryNumbers_joa:AccessoryNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::joa:AccessoryNumber
      else
        do Value::joa:AccessoryNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  joa:RefNumber = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('joa:RefNumber',joa:RefNumber)

PreCopy  Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  p_web._PreCopyRecord(JOBACCNO,joa:RecordNumberKey)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)
  p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAccessoryNumbers_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormAccessoryNumbers_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If joa:AccessoryNumber = ''
          loc:Invalid = 'joa:AccessoryNumber'
          loc:alert = p_web.translate('Accessory Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          joa:AccessoryNumber = Upper(joa:AccessoryNumber)
          p_web.SetSessionValue('joa:AccessoryNumber',joa:AccessoryNumber)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)

PostDelete      Routine
BrowseAccessoryNumber PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBACCNO)
                      Project(joa:RecordNumber)
                      Project(joa:AccessoryNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BrowseAccessoryNumber')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAccessoryNumber:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAccessoryNumber:NoForm')
      loc:FormName = p_web.GetValue('BrowseAccessoryNumber:FormName')
    else
      loc:FormName = 'BrowseAccessoryNumber_frm'
    End
    p_web.SSV('BrowseAccessoryNumber:NoForm',loc:NoForm)
    p_web.SSV('BrowseAccessoryNumber:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAccessoryNumber:NoForm')
    loc:FormName = p_web.GSV('BrowseAccessoryNumber:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAccessoryNumber') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAccessoryNumber')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBACCNO,joa:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOA:ACCESSORYNUMBER') then p_web.SetValue('BrowseAccessoryNumber_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAccessoryNumber:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAccessoryNumber:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseAccessoryNumber:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAccessoryNumber:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAccessoryNumber:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAccessoryNumber_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAccessoryNumber_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joa:AccessoryNumber)','-UPPER(joa:AccessoryNumber)')
    Loc:LocateField = 'joa:AccessoryNumber'
  of 2
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+joa:RecordNumber'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joa:AccessoryNumber')
    loc:SortHeader = p_web.Translate('Accessory Number')
    p_web.SetSessionValue('BrowseAccessoryNumber_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseAccessoryNumber:LookupFrom')
  End!Else
    loc:formaction = 'FormAccessoryNumbers'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAccessoryNumber:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAccessoryNumber:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAccessoryNumber:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBACCNO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joa:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseAccessoryNumber',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAccessoryNumber.locate(''Locator2BrowseAccessoryNumber'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAccessoryNumber.cl(''BrowseAccessoryNumber'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseAccessoryNumber_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseAccessoryNumber_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseAccessoryNumber','Accessory Number','Click here to sort by Accessory Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Accessory Number')&'">'&p_web.Translate('Accessory Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('Job:ViewOnly') <> 1) AND  true
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Delete
        do AddPacket
        loc:columns += 1
    End ! Selecting
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('joa:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBACCNO{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'joa:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joa:RecordNumber'),p_web.GetValue('joa:RecordNumber'),p_web.GetSessionValue('joa:RecordNumber'))
      loc:FilterWas = 'joa:RefNumber = ' & p_web.GSV('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAccessoryNumber_Filter')
    p_web.SetSessionValue('BrowseAccessoryNumber_FirstValue','')
    p_web.SetSessionValue('BrowseAccessoryNumber_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBACCNO,joa:RecordNumberKey,loc:PageRows,'BrowseAccessoryNumber',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBACCNO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBACCNO,loc:firstvalue)
              Reset(ThisView,JOBACCNO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBACCNO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBACCNO,loc:lastvalue)
            Reset(ThisView,JOBACCNO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joa:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAccessoryNumber.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAccessoryNumber.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAccessoryNumber.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAccessoryNumber.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 0
      if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseAccessoryNumber')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAccessoryNumber_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAccessoryNumber_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseAccessoryNumber',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAccessoryNumber.locate(''Locator1BrowseAccessoryNumber'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAccessoryNumber.cl(''BrowseAccessoryNumber'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseAccessoryNumber_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAccessoryNumber_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAccessoryNumber.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAccessoryNumber.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAccessoryNumber.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAccessoryNumber.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 0
    if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseAccessoryNumber')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = joa:RecordNumber
    p_web._thisrow = p_web._nocolon('joa:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAccessoryNumber:LookupField')) = joa:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((joa:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseAccessoryNumber.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBACCNO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBACCNO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBACCNO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBACCNO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joa:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseAccessoryNumber.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joa:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseAccessoryNumber.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joa:AccessoryNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Job:ViewOnly') <> 1) AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseAccessoryNumber.omv(this);" onMouseOut="BrowseAccessoryNumber.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseAccessoryNumber=new browseTable(''BrowseAccessoryNumber'','''&clip(loc:formname)&''','''&p_web._jsok('joa:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('joa:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormAccessoryNumbers'');<13,10>'&|
      'BrowseAccessoryNumber.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseAccessoryNumber.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAccessoryNumber')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAccessoryNumber')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAccessoryNumber')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAccessoryNumber')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBACCNO)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBACCNO)
  Bind(joa:Record)
  Clear(joa:Record)
  NetWebSetSessionPics(p_web,JOBACCNO)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('joa:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::joa:AccessoryNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joa:AccessoryNumber_'&joa:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joa:AccessoryNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Job:ViewOnly') <> 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Delete_'&joa:RecordNumber,,net:crc)
          If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseAccessoryNumber',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = joa:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('joa:RecordNumber',joa:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('joa:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joa:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joa:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
JobEstimate          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEstAccBy          STRING(30)                            !
locEstAccCommunicationMethod STRING(30)                    !
locEstRejBy          STRING(30)                            !
locEstRejCommunicationMethod STRING(30)                    !
locEstRejReason      STRING(30)                            !
FilesOpened     Long
AUDSTATS::State  USHORT
JOBS::State  USHORT
ESREJRES::State  USHORT
PARTS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locEstRejReason_OptionView   View(ESREJRES)
                          Project(esr:RejectionReason)
                        End
  CODE
  GlobalErrors.SetProcedureName('JobEstimate')
  loc:formname = 'JobEstimate_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobEstimate','')
    p_web._DivHeader('JobEstimate',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimate',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobEstimate',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(ESREJRES)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(ESREJRES)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Initi
      p_web.SSV('Hide:EstimateAccepted',1)
      p_web.SSV('Hide:EstimateRejected',1)
      p_web.SSV('Comment:EstimateReady','')
      p_web.SSV('Comment:EstimateAccepted','')
      p_web.SSV('Comment:EstimateRejected','')
  p_web.SetValue('JobEstimate_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Estimate_If_Over')
    p_web.SetPicture('job:Estimate_If_Over','@n14.2')
  End
  p_web.SetSessionPicture('job:Estimate_If_Over','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Estimate',job:Estimate)
  p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over)
  p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready)
  p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted)
  p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected)
  p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  p_web.SetSessionValue('locEstRejReason',locEstRejReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Estimate')
    job:Estimate = p_web.GetValue('job:Estimate')
    p_web.SetSessionValue('job:Estimate',job:Estimate)
  End
  if p_web.IfExistsValue('job:Estimate_If_Over')
    job:Estimate_If_Over = p_web.dformat(clip(p_web.GetValue('job:Estimate_If_Over')),'@n14.2')
    p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over)
  End
  if p_web.IfExistsValue('job:Estimate_Ready')
    job:Estimate_Ready = p_web.GetValue('job:Estimate_Ready')
    p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready)
  End
  if p_web.IfExistsValue('job:Estimate_Accepted')
    job:Estimate_Accepted = p_web.GetValue('job:Estimate_Accepted')
    p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted)
  End
  if p_web.IfExistsValue('job:Estimate_Rejected')
    job:Estimate_Rejected = p_web.GetValue('job:Estimate_Rejected')
    p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected)
  End
  if p_web.IfExistsValue('locEstAccBy')
    locEstAccBy = p_web.GetValue('locEstAccBy')
    p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  End
  if p_web.IfExistsValue('locEstAccCommunicationMethod')
    locEstAccCommunicationMethod = p_web.GetValue('locEstAccCommunicationMethod')
    p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  End
  if p_web.IfExistsValue('locEstRejBy')
    locEstRejBy = p_web.GetValue('locEstRejBy')
    p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  End
  if p_web.IfExistsValue('locEstRejCommunicationMethod')
    locEstRejCommunicationMethod = p_web.GetValue('locEstRejCommunicationMethod')
    p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  End
  if p_web.IfExistsValue('locEstRejReason')
    locEstRejReason = p_web.GetValue('locEstRejReason')
    p_web.SetSessionValue('locEstRejReason',locEstRejReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobEstimate_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locEstAccBy = p_web.RestoreValue('locEstAccBy')
 locEstAccCommunicationMethod = p_web.RestoreValue('locEstAccCommunicationMethod')
 locEstRejBy = p_web.RestoreValue('locEstRejBy')
 locEstRejCommunicationMethod = p_web.RestoreValue('locEstRejCommunicationMethod')
 locEstRejReason = p_web.RestoreValue('locEstRejReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobEstimate_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobEstimate_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobEstimate_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobEstimate" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobEstimate" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobEstimate" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Estimate Details') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Estimate Details',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobEstimate">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobEstimate" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobEstimate')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobEstimate')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobEstimate'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Estimate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobEstimate')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimate_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_If_Over
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_If_Over
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_If_Over
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_Ready
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_Ready
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_Ready
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_Accepted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_Accepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_Accepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_Rejected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_Rejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_Rejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimate_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEstimateAccepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textEstimateAccepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstAccBy
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstAccBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstAccBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstAccCommunicationMethod
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstAccCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstAccCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimate_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEstimateRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textEstimateRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstRejBy
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstRejBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstRejBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstRejCommunicationMethod
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstRejCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstRejCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstRejReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstRejReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstRejReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:Estimate  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Estimate Required')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Estimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate',p_web.GetValue('NewValue'))
    job:Estimate = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate
    do Value::job:Estimate
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate',p_web.GetValue('Value'))
    job:Estimate = p_web.GetValue('Value')
  End
  if (p_web.GSV('job:Estimate') = 'YES')
      ! #11656 Set status when estimate = yes / no (Bryan: 23/08/2010)
      p_web.SSV('GetStatus:Type','JOB')
      p_web.SSV('GetStatus:StatusNumber',505)
      getStatus(p_web)
  ELSE
      ! Get the previous Status
      if (p_web.GSV('job:Current_Status') = '505 ESTIMATE REQUIRED')
          Access:AUDSTATS.CLearkey(aus:StatusTypeRecordKey)
          aus:RefNumber = p_web.GSV('job:Ref_Number')
          aus:Type = 'JOB'
          Set(aus:StatusTypeRecordKey,aus:StatusTypeRecordKey)
          Loop Until Access:AUDSTATS.Next()
              if (aus:RefNumber <> p_web.GSV('job:Ref_Number'))
                  break
              end
              if (aus:Type <> 'JOB')
                  cycle
              end
              if (aus:NewStatus = '505 ESTIMATE REQUIRED')
                  p_web.SSV('GetStatus:Type','JOB')
                  p_web.SSV('GetStatus:StatusNumber',sub(aus:OldStatus,1,3))
                  getStatus(p_web)
                  break
              end
          end
      end
  END
  do Value::job:Estimate
  do SendAlert
  do Prompt::job:Estimate_Accepted
  do Value::job:Estimate_Accepted  !1
  do Prompt::job:Estimate_If_Over
  do Value::job:Estimate_If_Over  !1
  do Prompt::job:Estimate_Ready
  do Value::job:Estimate_Ready  !1
  do Prompt::job:Estimate_Rejected
  do Value::job:Estimate_Rejected  !1

Value::job:Estimate  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- job:Estimate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate'',''jobestimate_job:estimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate') & '_value')

Comment::job:Estimate  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Estimate_If_Over  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate If Over')
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_prompt')

Validate::job:Estimate_If_Over  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_If_Over',p_web.GetValue('NewValue'))
    job:Estimate_If_Over = p_web.GetValue('NewValue') !FieldType= REAL Field = job:Estimate_If_Over
    do Value::job:Estimate_If_Over
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Estimate_If_Over',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    job:Estimate_If_Over = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::job:Estimate_If_Over
  do SendAlert

Value::job:Estimate_If_Over  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- STRING --- job:Estimate_If_Over
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:Estimate_If_Over')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Estimate_If_Over'',''jobestimate_job:estimate_if_over_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Estimate_If_Over',p_web.GetSessionValue('job:Estimate_If_Over'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_value')

Comment::job:Estimate_If_Over  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Estimate_Ready  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Ready')
  If p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_prompt')

Validate::job:Estimate_Ready  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_Ready',p_web.GetValue('NewValue'))
    job:Estimate_Ready = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate_Ready
    do Value::job:Estimate_Ready
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate_Ready',p_web.GetValue('Value'))
    job:Estimate_Ready = p_web.GetValue('Value')
  End
  do Value::job:Estimate_Ready
  do SendAlert
  do Prompt::job:Estimate_Ready

Value::job:Estimate_Ready  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
  ! --- CHECKBOX --- job:Estimate_Ready
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Ready'',''jobestimate_job:estimate_ready_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Ready')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Ready') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Ready',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_value')

Comment::job:Estimate_Ready  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateReady'))
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Estimate_Accepted  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Accepted')
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_prompt')

Validate::job:Estimate_Accepted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_Accepted',p_web.GetValue('NewValue'))
    job:Estimate_Accepted = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate_Accepted
    do Value::job:Estimate_Accepted
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate_Accepted',p_web.GetValue('Value'))
    job:Estimate_Accepted = p_web.GetValue('Value')
  End
  if (p_web.GSV('job:Estimate_Accepted') = 'YES')
      if (p_web.GSV('job:Estimate_Ready') <> 'YES')
          p_web.SSV('Comment:EstimateAccepted','Estimate has not been marked as ready')
          p_web.SSV('job:Estimate_Accepted','NO')
      else ! if (p_web.GSV('job:Estimate_Ready') <> 'YES')
          p_web.SSV('Comment:EstimateAccepted','')
          p_web.SSV('Hide:EstimateAccepted',0)
          p_web.SSV('job:Estimate_Rejected','NO')
      end !if (p_web.GSV('job:Estimate_Ready') <> 'YES')
  else ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
      ! Check for char parts
      found# = 0
      Access:PARTS.Clearkey(par:part_Number_Key)
      par:ref_Number    = p_web.GSV('job:Ref_Number')
      set(par:part_Number_Key,par:part_Number_Key)
      loop
          if (Access:PARTS.Next())
              Break
          end ! if (Access:PARTS.Next())
          if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
          found# = 1
          break
      end ! loop
  
      if (found# = 1)
          p_web.SSV('Comment:EstimateAccepted','Error! There are Chargeable Parts on this job.')
          p_web.SSV('job:Estimate_Accepted','YES')
      else ! if (found# = 1)
          p_web.SSV('Comment:EstimateAccepted','')
          p_web.SSV('Hide:EstimateAccepted',1)
      end ! if (found# = 1)
      
  end ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
  do Value::job:Estimate_Accepted
  do SendAlert
  do Prompt::locEstAccBy
  do Value::locEstAccBy  !1
  do Prompt::locEstAccCommunicationMethod
  do Value::locEstAccCommunicationMethod  !1
  do Value::textEstimateAccepted  !1
  do Comment::job:Estimate_Accepted
  do Value::job:Estimate_Rejected  !1
  do Comment::job:Estimate_Rejected
  do Prompt::job:Estimate_Ready
  do Value::job:Estimate_Ready  !1

Value::job:Estimate_Accepted  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- CHECKBOX --- job:Estimate_Accepted
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Accepted'',''jobestimate_job:estimate_accepted_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Accepted')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Accepted') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Accepted',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_value')

Comment::job:Estimate_Accepted  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateAccepted'))
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_comment')

Prompt::job:Estimate_Rejected  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Rejected')
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_prompt')

Validate::job:Estimate_Rejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_Rejected',p_web.GetValue('NewValue'))
    job:Estimate_Rejected = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate_Rejected
    do Value::job:Estimate_Rejected
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate_Rejected',p_web.GetValue('Value'))
    job:Estimate_Rejected = p_web.GetValue('Value')
  End
  if (p_web.GSV('job:Estimate_Rejected') = 'YES')
      if (p_web.GSV('job:Estimate_Accepted') = 'YES')
          p_web.SSV('Comment:EstimateRejected','You must untick Estimate Accepted')
      else ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
          p_web.SSV('Hide:EstimateRejected',0)
          p_web.SSV('Comment:EstimateRejected','')
      end ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
  else ! if (p_web.GSV('job:Estimate_Rejected') = 'YES')
      p_web.SSV('Hide:EstimateRejected',1)
      p_web.SSV('Comment:EstimateRejected','')
  end ! if (p_web.GSV('job:Estimate_Rejected') = 'YES')
  do Value::job:Estimate_Rejected
  do SendAlert
  do Prompt::locEstRejBy
  do Value::locEstRejBy  !1
  do Prompt::locEstRejCommunicationMethod
  do Value::locEstRejCommunicationMethod  !1
  do Prompt::locEstRejReason
  do Value::locEstRejReason  !1
  do Comment::job:Estimate_Rejected

Value::job:Estimate_Rejected  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- CHECKBOX --- job:Estimate_Rejected
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Rejected'',''jobestimate_job:estimate_rejected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Rejected')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Rejected') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Rejected',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_value')

Comment::job:Estimate_Rejected  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateRejected'))
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_comment')

Validate::textEstimateAccepted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEstimateAccepted',p_web.GetValue('NewValue'))
    do Value::textEstimateAccepted
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEstimateAccepted  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_value',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Estimate Accepted',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_value')

Comment::textEstimateAccepted  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_comment',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstAccBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_prompt',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Accepted By')
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_prompt')

Validate::locEstAccBy  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstAccBy',p_web.GetValue('NewValue'))
    locEstAccBy = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstAccBy
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstAccBy',p_web.GetValue('Value'))
    locEstAccBy = p_web.GetValue('Value')
  End
    locEstAccBy = Upper(locEstAccBy)
    p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  do Value::locEstAccBy
  do SendAlert

Value::locEstAccBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_value',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- STRING --- locEstAccBy
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstAccBy')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstAccBy'',''jobestimate_locestaccby_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstAccBy',p_web.GetSessionValueFormat('locEstAccBy'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_value')

Comment::locEstAccBy  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_comment',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstAccCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_prompt',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Communication Method')
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_prompt')

Validate::locEstAccCommunicationMethod  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstAccCommunicationMethod',p_web.GetValue('NewValue'))
    locEstAccCommunicationMethod = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstAccCommunicationMethod
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstAccCommunicationMethod',p_web.GetValue('Value'))
    locEstAccCommunicationMethod = p_web.GetValue('Value')
  End
    locEstAccCommunicationMethod = Upper(locEstAccCommunicationMethod)
    p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  do Value::locEstAccCommunicationMethod
  do SendAlert

Value::locEstAccCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_value',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- STRING --- locEstAccCommunicationMethod
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstAccCommunicationMethod')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstAccCommunicationMethod'',''jobestimate_locestacccommunicationmethod_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstAccCommunicationMethod',p_web.GetSessionValueFormat('locEstAccCommunicationMethod'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_value')

Comment::locEstAccCommunicationMethod  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_comment',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::textEstimateRejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEstimateRejected',p_web.GetValue('NewValue'))
    do Value::textEstimateRejected
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEstimateRejected  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateRejected') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('Estimate Rejected',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::textEstimateRejected  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateRejected') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstRejBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Rejected By')
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_prompt')

Validate::locEstRejBy  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstRejBy',p_web.GetValue('NewValue'))
    locEstRejBy = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstRejBy
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstRejBy',p_web.GetValue('Value'))
    locEstRejBy = p_web.GetValue('Value')
  End
    locEstRejBy = Upper(locEstRejBy)
    p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  do Value::locEstRejBy
  do SendAlert

Value::locEstRejBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- STRING --- locEstRejBy
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstRejBy')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejBy'',''jobestimate_locestrejby_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstRejBy',p_web.GetSessionValueFormat('locEstRejBy'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_value')

Comment::locEstRejBy  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstRejCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Communication Method')
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_prompt')

Validate::locEstRejCommunicationMethod  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstRejCommunicationMethod',p_web.GetValue('NewValue'))
    locEstRejCommunicationMethod = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstRejCommunicationMethod
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstRejCommunicationMethod',p_web.GetValue('Value'))
    locEstRejCommunicationMethod = p_web.GetValue('Value')
  End
    locEstRejCommunicationMethod = Upper(locEstRejCommunicationMethod)
    p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  do Value::locEstRejCommunicationMethod
  do SendAlert

Value::locEstRejCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- STRING --- locEstRejCommunicationMethod
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstRejCommunicationMethod')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejCommunicationMethod'',''jobestimate_locestrejcommunicationmethod_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstRejCommunicationMethod',p_web.GetSessionValueFormat('locEstRejCommunicationMethod'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_value')

Comment::locEstRejCommunicationMethod  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstRejReason  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Rejection Reason')
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_prompt')

Validate::locEstRejReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstRejReason',p_web.GetValue('NewValue'))
    locEstRejReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstRejReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstRejReason',p_web.GetValue('Value'))
    locEstRejReason = p_web.GetValue('Value')
  End
  do Value::locEstRejReason
  do SendAlert

Value::locEstRejReason  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEstRejReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejReason'',''jobestimate_locestrejreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locEstRejReason',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locEstRejReason') = 0
    p_web.SetSessionValue('locEstRejReason','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locEstRejReason')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(AUDSTATS)
  bind(aus:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(ESREJRES)
  bind(esr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locEstRejReason_OptionView)
  locEstRejReason_OptionView{prop:order} = 'UPPER(esr:RejectionReason)'
  Set(locEstRejReason_OptionView)
  Loop
    Next(locEstRejReason_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locEstRejReason') = 0
      p_web.SetSessionValue('locEstRejReason',esr:RejectionReason)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,esr:RejectionReason,choose(esr:RejectionReason = p_web.getsessionvalue('locEstRejReason')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locEstRejReason_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(ESREJRES)
  p_Web._CloseFile(PARTS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_value')

Comment::locEstRejReason  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobEstimate_job:Estimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate
      else
        do Value::job:Estimate
      end
  of lower('JobEstimate_job:Estimate_If_Over_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_If_Over
      else
        do Value::job:Estimate_If_Over
      end
  of lower('JobEstimate_job:Estimate_Ready_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Ready
      else
        do Value::job:Estimate_Ready
      end
  of lower('JobEstimate_job:Estimate_Accepted_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Accepted
      else
        do Value::job:Estimate_Accepted
      end
  of lower('JobEstimate_job:Estimate_Rejected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Rejected
      else
        do Value::job:Estimate_Rejected
      end
  of lower('JobEstimate_locEstAccBy_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstAccBy
      else
        do Value::locEstAccBy
      end
  of lower('JobEstimate_locEstAccCommunicationMethod_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstAccCommunicationMethod
      else
        do Value::locEstAccCommunicationMethod
      end
  of lower('JobEstimate_locEstRejBy_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejBy
      else
        do Value::locEstRejBy
      end
  of lower('JobEstimate_locEstRejCommunicationMethod_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejCommunicationMethod
      else
        do Value::locEstRejCommunicationMethod
      end
  of lower('JobEstimate_locEstRejReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejReason
      else
        do Value::locEstRejReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobEstimate',0)

PreCopy  Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobEstimate',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobEstimate:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobEstimate:Primed',0)
  p_web.setsessionvalue('showtab_JobEstimate',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('job:Estimate') = 0
            p_web.SetValue('job:Estimate','NO')
            job:Estimate = 'NO'
          End
      If(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
          If p_web.IfExistsValue('job:Estimate_Ready') = 0
            p_web.SetValue('job:Estimate_Ready','NO')
            job:Estimate_Ready = 'NO'
          End
      End
      If(p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_Accepted') = 0
            p_web.SetValue('job:Estimate_Accepted','NO')
            job:Estimate_Accepted = 'NO'
          End
      End
      If(p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_Rejected') = 0
            p_web.SetValue('job:Estimate_Rejected','NO')
            job:Estimate_Rejected = 'NO'
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobEstimate_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobEstimate_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:EstimateAccepted') = 1)
          locEstAccBy = Upper(locEstAccBy)
          p_web.SetSessionValue('locEstAccBy',locEstAccBy)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EstimateAccepted') = 1)
          locEstAccCommunicationMethod = Upper(locEstAccCommunicationMethod)
          p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          locEstRejBy = Upper(locEstRejBy)
          p_web.SetSessionValue('locEstRejBy',locEstRejBy)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          locEstRejCommunicationMethod = Upper(locEstRejCommunicationMethod)
          p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
      if (p_web.GSV('job:estimate_rejected') = 'YES')
          if (p_web.GSV('Hide:EstimateRejected') = 0)
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber',540)
              getStatus(p_web)
  
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ESTIMATE REJECTED FROM: ' & p_web.GSV('locEstRejBy'))
              p_web.SSV('AddToAudit:Notes','COMMUNICATION METHOD: ' & p_web.GSV('locEstRejCommunicationMethod') & |
                           '<13,10>REASON: ' & p_web.GSV('locEstRejReason'))
              addToAudit(p_web)
          end ! if (p_web.GSV('Hide:EstimateRejected') = 0)
      end ! if (p_web.GSV('job:estimate_rejected') = 'YES')
  
      if (p_web.GSV('job:estimate_Accepted') = 'YES')
          if (p_web.GSV('Hide:EstimateAccepted') = 0)
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber',535)
              getStatus(p_web)
  
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ESTIMATE ACCEPTED FROM: ' & p_web.GSV('locEstAccBy'))
              p_web.SSV('AddToAudit:Notes','COMMUNICATION METHOD: ' & p_web.GSV('locEstAccCommunicationMethod'))
              addToAudit(p_web)
  
              p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
  
              p_web.SSV('job:Labour_Cost',p_web.GSV('job:Labour_Cost_Estimate'))
              p_web.SSV('job:Parts_Cost',p_web.GSV('job:Parts_Cost_Estimate'))
              p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
              p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('jobe:RRCELabourCost'))
              p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
              p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))
              convertEstimateParts(p_web)
          end ! if (p_web.GSV('Hide:EstimateAccepted') = 0)
      end ! if (p_web.GSV('job:estimate_Accepted') = 'YES')
  
  
  
  
  
  
  
  ! Delete Variables
      p_web.deleteSessionValue('locEstAccBy')
      p_web.deleteSessionValue('locEstAccCommunicationMethod')
      p_web.deleteSessionValue('locEstRejBy')
      p_web.deleteSessionValue('locEstRejCommunicationMethod')
      p_web.deleteSessionValue('locEstRejReason')
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobEstimate:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locEstAccBy')
  p_web.StoreValue('locEstAccCommunicationMethod')
  p_web.StoreValue('')
  p_web.StoreValue('locEstRejBy')
  p_web.StoreValue('locEstRejCommunicationMethod')
  p_web.StoreValue('locEstRejReason')
ConvertEstimateParts PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locCreateWebOrder    BYTE                                  !
locPartNumber        LONG                                  !
locQuantity          LONG                                  !
local       class
AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
            end
tmp:AddToStockAllocation Byte(0)
FilesOpened     BYTE(0)
  CODE
    do openFiles

    tmp:AddToStockAllocation = 0

    access:estparts.clearkey(epr:part_number_key)
    epr:ref_number  = p_web.GSV('job:ref_number')
    set(epr:part_number_key,epr:part_number_key)
    loop
        if access:estparts.next()
           break
        end !if
        if epr:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Access:STOCK.ClearKey(sto:ref_number_key)
        sto:ref_number = epr:Part_Ref_Number
        if not Access:STOCK.Fetch(sto:ref_number_key)
            if sto:ExchangeUnit = 'YES'

                !Check for existing unit - then exchange
                !chargeable parts and partnumber = EXCH?
                access:parts.clearkey(par:Part_Number_Key)
                par:Ref_Number = p_web.GSV('job:ref_number')
                par:Part_Number = 'EXCH'
                if access:parts.fetch(par:Part_NUmber_Key) = level:benign
                    !found an existing Exchange unit
                ELSE

                    Access:USERS.Clearkey(use:User_Code_Key)
                    use:User_Code   = p_web.GSV('job:Engineer')
                    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = 'EXCH'
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            access:location.clearkey(loc:Location_Key)
                            loc:Location = use:location
                            access:location.fetch(loc:Location_Key)
                            !changed 19/11 alway allocate at once
                            !change back 20/11
!                            if loc:UseRapidStock then
                                !Found
                                Local.AllocateExchangePart('CHA',0)
!                            ELSE
!                                ExchangeUnitNumber# = p_web.GSV('job:Exchange_Unit_Number')
!                                ViewExchangeUnit()
!                                Access:JOBS.TryUpdate()
!                                ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
!                                UpdateDateTimeStamp(p_web.GSV('job:Ref_Number'))
!                                ! End (DBH 16/09/2008) #10253
!                                If p_web.GSV('job:Exchange_Unit_Number') <> ExchangeUnitNumber#
!                                    Local.AllocateExchangePart('CHA',1)
!                                End !If p_web.GSV('job:Exchange_Unit_Number <> ExchangeUnitNumber#
!                            END !if loc:useRapidStock
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    Else ! If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign

                    p_web.SSV('GetStatus:Type','EXC')
                    p_web.SSV('GetStatus:StatusNumber',108)
                    getStatus(p_web)
                End !If exchang unit already existed

                !Remove the tick so that if the job is made "Unaccpeted"
                !then you won't have to remove these parts to stock twice
                If epr:UsedOnRepair
                    epr:UsedOnRepair = 0
                    Access:ESTPARTS.Update()
                End !If epr:UsedOnRepair
                cycle
            end
        end

        get(parts,0)
        glo:select1 = ''
        if access:parts.primerecord() = level:benign
            par:ref_number           = p_web.GSV('job:ref_number')
            par:adjustment           = epr:adjustment
            par:part_number     = epr:part_number
            par:description     = epr:description
            par:supplier        = epr:supplier
            par:purchase_cost   = epr:purchase_cost
            par:sale_cost       = epr:sale_cost
            par:retail_cost     = epr:retail_cost
            par:quantity             = epr:quantity
            par:exclude_from_order   = epr:exclude_from_order
            par:part_ref_number      = epr:part_ref_number
            !If not used, then mark to be allocated
            If epr:UsedOnRepair
                par:PartAllocated       = epr:PartAllocated
            Else !If epr:UsedOnRepair
                par:PartAllocated       = 0
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    IF (sto:Sundry_Item = 'YES') ! ! #11837 Don't decrement a sundry item. (Bryan: 08/12/2010)
                        par:PartAllocated = 1
                        par:Date_Ordered = TODAY()
                    ELSE
                        If par:Quantity <= sto:Quantity_Stock
                            !If its not a Rapid site, turn part allocated off
!                        If RapidLocation(sto:Location)
                            par:PartAllocated = 0
!                        End !If RapidLocation(sto:Location) = Level:Benign
                            sto:Quantity_Stock  -= par:Quantity
                            If sto:Quantity_Stock < 0
                                sto:Quantity_Stock = 0
                            End !If sto:Quantity_Stock < 0

                            tmp:AddToStockAllocation = 1

                            If Access:STOCK.Update() = Level:Benign
                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                    'DEC', | ! Transaction_Type
                                    par:Despatch_Note_Number, | ! Depatch_Note_Number
                                    p_web.GSV('job:Ref_Number'), | ! Job_Number
                                    0, | ! Sales_Number
                                    par:Quantity, | ! Quantity
                                    sto:Purchase_Cost, | ! Purchase_Cost
                                    sto:Sale_Cost, | ! Sale_Cost
                                    sto:Retail_Cost, | ! Retail_Cost
                                    'STOCK DECREMENTED', | ! Notes
                                    '', | !Information
                                    p_web.GSV('BookingUserCode'), |
                                    sto:Quantity_Stock) ! Information
                                    ! Added OK
                                Else ! AddToStockHistory
                                    ! Error
                                End ! AddToStockHistory
                            End! If Access:STOCK.Update() = Level:Benign
                        Else !If par:Quantity < sto:Quantity

                            CreateWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,par:Description,par:Quantity - sto:Quantity_Stock,par:Retail_Cost)
                            If sto:Quantity_Stock > 0
                                locCreateWebOrder = 1
                                locPartNumber = par:Part_Ref_Number
                                locQuantity = par:Quantity - sto:Quantity_Stock
                                !glo:Select1 = 'NEW PENDING WEB'
                                !glo:Select2 = par:Part_Ref_Number
                                !glo:Select3 = par:Quantity - sto:Quantity_Stock
                                !glo:Select4 =
                                par:Quantity    = sto:Quantity_Stock
                                par:Date_Ordered    = Today()
                                tmp:AddToStockAllocation = 0
                            Else !If sto:Quantity_Stock > 0
                                par:WebOrder    = 1
                                tmp:AddToStockAllocation = 2
                            End !If sto:Quantity_Stock > 0
                        End !If par:Quantity > sto:Quantity
                    END
                    
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            End !If epr:UsedOnRepair

            par:Fault_Code1         = epr:Fault_Code1
            par:Fault_Code2         = epr:Fault_Code2
            par:Fault_Code3         = epr:Fault_Code3
            par:Fault_Code4         = epr:Fault_Code4
            par:Fault_Code5         = epr:Fault_Code5
            par:Fault_Code6         = epr:Fault_Code6
            par:Fault_Code7         = epr:Fault_Code7
            par:Fault_Code8         = epr:Fault_Code8
            par:Fault_Code9         = epr:Fault_Code9
            par:Fault_Code10        = epr:Fault_Code10
            par:Fault_Code11        = epr:Fault_Code11
            par:Fault_Code12        = epr:Fault_Code12
            par:Fault_Codes_Checked = epr:Fault_Codes_Checked
            !Neil
            par:RRCPurchaseCost        = epr:RRCPurchaseCost
            par:RRCSaleCost            = epr:RRCSaleCost
            par:InWarrantyMarkUp       = epr:InWarrantyMarkUp
            par:OutWarrantyMarkUp      = epr:OutWarrantyMarkUp
            par:RRCAveragePurchaseCost = epr:RRCAveragePurchaseCost
            par:AveragePurchaseCost    = epr:AveragePurchaseCost

            access:parts.insert()

            !Add part to stock allocation list - L873 (DBH: 11-08-2003)
            Case tmp:AddToStockAllocation
            Of 1
                p_web.SSV('AddToStockAllocation:Type','CHA')
                p_web.SSV('AddToStockAllocation:Status','')
                p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                addToStockAllocation(p_web)  
            Of 2
                p_web.SSV('AddToStockAllocation:Type','CHA')
                p_web.SSV('AddToStockAllocation:Status','WEB')
                p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                addToStockAllocation(p_web)  
            End !If tmp:AddToStockAllocation

            If (locCreateWebOrder = 1)
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = locPartNumber
                If access:stock.fetch(sto:ref_number_key)
                Else!If access:stock.fetch(sto:ref_number_key)
                    sto:quantity_stock     = 0
                    access:stock.update()

                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                         p_web.GSV('job:Ref_Number'), | ! Job_Number
                                         0, | ! Sales_Number
                                         sto:Quantity_stock, | ! Quantity
                                         par:Purchase_Cost, | ! Purchase_Cost
                                         par:Sale_Cost, | ! Sale_Cost
                                         par:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '', |
                                         p_web.GSV('BookingUserCode'), |
                                         sto:Quantity_Stock) ! Information
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory

                    access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                    par_ali:ref_number      = p_web.GSV('job:ref_number')
                    par_ali:part_ref_number = par:part_Ref_Number
                    If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                        par:ref_number           = par_ali:ref_number
                        par:adjustment           = par_ali:adjustment
                        par:part_ref_number      = par_ali:part_ref_number

                        par:part_number     = par_ali:part_number
                        par:description     = par_ali:description
                        par:supplier        = par_ali:supplier
                        par:purchase_cost   = par_ali:purchase_cost
                        par:sale_cost       = par_ali:sale_cost
                        par:retail_cost     = par_ali:retail_cost
                        par:quantity             = locQuantity
                        par:warranty_part        = 'NO'
                        par:exclude_from_order   = 'NO'
                        par:despatch_note_number = ''
                        par:date_ordered         = ''
                        par:date_received        = ''
                        par:pending_ref_number   = ''
                        par:order_number         = ''
                        par:fault_code1    = par_ali:fault_code1
                        par:fault_code2    = par_ali:fault_code2
                        par:fault_code3    = par_ali:fault_code3
                        par:fault_code4    = par_ali:fault_code4
                        par:fault_code5    = par_ali:fault_code5
                        par:fault_code6    = par_ali:fault_code6
                        par:fault_code7    = par_ali:fault_code7
                        par:fault_code8    = par_ali:fault_code8
                        par:fault_code9    = par_ali:fault_code9
                        par:fault_code10   = par_ali:fault_code10
                        par:fault_code11   = par_ali:fault_code11
                        par:fault_code12   = par_ali:fault_code12
                        par:WebOrder = 1
                        access:parts.insert()
                        p_web.SSV('AddToStockAllocation:Type','CHA')
                        p_web.SSV('AddToStockAllocation:Status','WEB')
                        p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                        addToStockAllocation(p_web)  
                    end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign

                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
            End !If glo:Select1 = 'NEW PENDING WEB'
            !End
        end!if access:parts.primerecord() = level:benign

        !Remove the tick so that if the job is made "Unaccpeted"
        !then you won't have to remove these parts to stock twice
        If epr:UsedOnRepair
            epr:UsedOnRepair = 0
            Access:ESTPARTS.Update()
        End !If epr:UsedOnRepair
    end !loop

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.Open                                  ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.UseFile                               ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:PARTS_ALIAS.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:USERS.Close
     FilesOpened = False
  END
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
Code
        Case func:Type
            Of 'WAR'
                get(warparts,0)
                if access:warparts.primerecord() = level:benign
                    wpr:PArt_Ref_Number      = sto:Ref_Number
                    wpr:ref_number            = p_web.GSV('job:ref_number')
                    wpr:adjustment            = 'YES'
                    wpr:part_number           = 'EXCH'
                    wpr:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                    wpr:quantity              = 1
                    wpr:warranty_part         = 'NO'
                    wpr:exclude_from_order    = 'YES'
                    wpr:PartAllocated         = func:Allocated !was func:Allocated but see VP116 - must always be allocated straight off!
                                                  !changed back from '1' 20/11 - original system reinstalled
                    if func:Allocated = 0 then
                        wpr:Status            = 'REQ'
                    ELSE
                        WPR:Status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                        !Try and get the fault codes. This key should get the only record
                        Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                        stm:Ref_Number  = sto:Ref_Number
                        stm:Part_Number = sto:Part_Number
                        stm:Location    = sto:Location
                        stm:Description = sto:Description
                        If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                            !Found
                            wpr:Fault_Code1  = stm:FaultCode1
                            wpr:Fault_Code2  = stm:FaultCode2
                            wpr:Fault_Code3  = stm:FaultCode3
                            wpr:Fault_Code4  = stm:FaultCode4
                            wpr:Fault_Code5  = stm:FaultCode5
                            wpr:Fault_Code6  = stm:FaultCode6
                            wpr:Fault_Code7  = stm:FaultCode7
                            wpr:Fault_Code8  = stm:FaultCode8
                            wpr:Fault_Code9  = stm:FaultCode9
                            wpr:Fault_Code10 = stm:FaultCode10
                            wpr:Fault_Code11 = stm:FaultCode11
                            wpr:Fault_Code12 = stm:FaultCode12
                        Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    end !If sto:Assign_Fault_Codes = 'YES'
                    if access:warparts.insert()
                        access:warparts.cancelautoinc()
                    end
                End

            Of 'CHA'
                get(parts,0)
                if access:parts.primerecord() = level:benign
                    par:PArt_Ref_Number      = sto:Ref_Number
                    par:ref_number            = p_web.GSV('job:ref_number')
                    par:adjustment            = 'YES'
                    par:part_number           = 'EXCH'
                    par:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                    par:quantity              = 1
                    par:warranty_part         = 'NO'
                    par:exclude_from_order    = 'YES'
                    par:PartAllocated         = func:Allocated
                    !see above for confusion
                    if func:allocated = 0 then
                        par:Status            = 'REQ'
                    ELSE
                        par:status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           par:Fault_Code1  = stm:FaultCode1
                           par:Fault_Code2  = stm:FaultCode2
                           par:Fault_Code3  = stm:FaultCode3
                           par:Fault_Code4  = stm:FaultCode4
                           par:Fault_Code5  = stm:FaultCode5
                           par:Fault_Code6  = stm:FaultCode6
                           par:Fault_Code7  = stm:FaultCode7
                           par:Fault_Code8  = stm:FaultCode8
                           par:Fault_Code9  = stm:FaultCode9
                           par:Fault_Code10 = stm:FaultCode10
                           par:Fault_Code11 = stm:FaultCode11
                           par:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Error
                           !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    End !If sto:Assign_Fault_Codes = 'YES'
                    if access:parts.insert()
                        access:parts.cancelautoinc()
                    end
                End !If access:Prime
        End !Case func:Type
AddToStockHistory    PROCEDURE  (Long f:RefNo,String f:Trans,String f:Desp,Long f:JobNo,Long f:SaleNo,Long f:Qty,Real f:Purch,Real f:Sale,Real f:Retail,String f:Notes,String f:Info,String f:UserCode,Long f:QtyStock,<Real f:AvPC>,<Real f:PC>,<Real f:SC>,<Real f:RC>) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    rtnValue# = 0
    do openFiles
    If Access:STOHIST.PrimeRecord() = Level:Benign
        shi:Ref_Number           = f:RefNo
        shi:User                 = f:UserCode
        shi:Transaction_Type     = f:Trans
        shi:Despatch_Note_Number = f:Desp
        shi:Job_Number           = f:JobNo
        shi:Sales_Number         = f:SaleNo
        shi:Quantity             = f:Qty
        shi:Date                 = Today()
        shi:Purchase_Cost        = f:Purch
        shi:Sale_Cost            = f:Sale
        shi:Retail_Cost          = f:Retail
        shi:Notes                = f:Notes
        shi:Information          = f:Info
        shi:StockOnHand          = f:QtyStock
        If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Successful
            rtnValue# = 1
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber = shi:Record_Number
                IF (f:AvPC = 0)
                    f:AvPC = f:Purch
                END
                IF (f:PC = 0)
                    f:PC = f:Purch
                END
                IF (f:SC = 0)
                    f:SC = f:Sale
                END
                IF (f:RC = 0)
                    f:RC = f:Retail
                END
                
                stoe:PreviousAveragePurchaseCost = f:AvPC
                stoe:PurchaseCost = f:PC
                stoe:SaleCost = f:SC
                stoe:RetailCost = f:RC
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END
            
        Else ! If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Failed
            Access:STOHIST.CancelAutoInc()
        End ! If Access:STOHIST.TryInsert() = Level:Benign
    End ! If Access:STOHIST.PrimeRecord() = Level:Benign

    do closeFiles

    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOHISTE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHISTE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOHISTE.Close
     Access:STOHIST.Close
     FilesOpened = False
  END
