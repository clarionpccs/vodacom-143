

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A023.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A019.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A024.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A025.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A026.INC'),ONCE        !Req'd for module callout resolution
                     END


CustCollectionValidated PROCEDURE  (LONG jobNumber)        ! Declare Procedure
  CODE
    RETURN vod.CustomerCollectionValidated(jobNumber)
CustClassificationFields PROCEDURE  (LONG refNumber,*STRING lifeTimeValue,*STRING averageSpend,*STRING loyaltyStatus,*DATE upgradeDate,*STRING IDNumber) ! Declare Procedure
  CODE
    vod.CustomerClassificationFields(refNumber,|
        lifeTimeValue,|
        averageSpend,|
        loyaltyStatus,|
        upgradeDate,|
        IDNumber)
GetTheMainOutFault   PROCEDURE  (LONG jobNumber,*STRING outFault,*STRING description) ! Declare Procedure
  CODE
    vod.GetMainOutFault(jobNumber,outFault,description)
IsOneYearWarranty    PROCEDURE  (STRING manufacturer,STRING modelNumber) ! Declare Procedure
  CODE
    RETURN vod.OneYearWarranty(manufacturer,modelNumber)
LookupGenericAccounts PROCEDURE  (NetWebServerWorker p_web)
BookingAccount       STRING(30)                            !BookingAccount
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(TRAHUBAC)
                      Project(TRA1:RecordNo)
                      Project(TRA1:SubAcc)
                      Project(TRA1:SubAccName)
                      Project(TRA1:SubAccBranch)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('LookupGenericAccounts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupGenericAccounts:NoForm')
      loc:NoForm = p_web.GetValue('LookupGenericAccounts:NoForm')
      loc:FormName = p_web.GetValue('LookupGenericAccounts:FormName')
    else
      loc:FormName = 'LookupGenericAccounts_frm'
    End
    p_web.SSV('LookupGenericAccounts:NoForm',loc:NoForm)
    p_web.SSV('LookupGenericAccounts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupGenericAccounts:NoForm')
    loc:FormName = p_web.GSV('LookupGenericAccounts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupGenericAccounts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupGenericAccounts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(TRAHUBAC,TRA1:RecordNoKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TRA1:SUBACC') then p_web.SetValue('LookupGenericAccounts_sort','1')
    ElsIf (loc:vorder = 'TRA1:SUBACC') then p_web.SetValue('LookupGenericAccounts_sort','1')
    ElsIf (loc:vorder = 'TRA1:SUBACCNAME') then p_web.SetValue('LookupGenericAccounts_sort','2')
    ElsIf (loc:vorder = 'TRA1:SUBACCNAME') then p_web.SetValue('LookupGenericAccounts_sort','2')
    ElsIf (loc:vorder = 'TRA1:SUBACCBRANCH') then p_web.SetValue('LookupGenericAccounts_sort','4')
    ElsIf (loc:vorder = 'TRA1:SUBACCBRANCH') then p_web.SetValue('LookupGenericAccounts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupGenericAccounts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupGenericAccounts:LookupFrom','LookupFrom')
    p_web.StoreValue('LookupGenericAccounts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupGenericAccounts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupGenericAccounts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupGenericAccounts_sort',net:DontEvaluate)
  p_web.SetSessionValue('LookupGenericAccounts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 5
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'TRA1:SubAcc','-TRA1:SubAcc')
    Loc:LocateField = 'TRA1:SubAcc'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'TRA1:SubAccName','-TRA1:SubAccName')
    Loc:LocateField = 'TRA1:SubAccName'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'TRA1:SubAccBranch','-TRA1:SubAccBranch')
    Loc:LocateField = 'TRA1:SubAccBranch'
  end
  if loc:vorder = ''
    Loc:LocateField = 'TRA1:SubAcc'
    loc:sortheader = 'Account Number'
    loc:vorder = '+UPPER(TRA1:SubAcc)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('TRA1:SubAcc')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('LookupGenericAccounts_LocatorPic','@s15')
  Of upper('TRA1:SubAccName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('LookupGenericAccounts_LocatorPic','@s30')
  Of upper('TRA1:SubAccBranch')
    loc:SortHeader = p_web.Translate('Branch')
    p_web.SetSessionValue('LookupGenericAccounts_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('LookupGenericAccounts:LookupFrom')
  End!Else
  loc:formaction = 'LookupGenericAccounts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupGenericAccounts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupGenericAccounts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupGenericAccounts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="TRAHUBAC"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="TRA1:RecordNoKey"></input><13,10>'
  end
  If p_web.Translate('Select Account') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Account',0)&'</span>'&CRLF
  End
  If clip('Select Account') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupGenericAccounts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2LookupGenericAccounts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupGenericAccounts.locate(''Locator2LookupGenericAccounts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupGenericAccounts.cl(''LookupGenericAccounts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="LookupGenericAccounts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="LookupGenericAccounts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','LookupGenericAccounts','Account Number','Click here to sort by Account Number',,,200,1)
        Else
          packet = clip(packet) & '<th width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','LookupGenericAccounts','Company Name','Click here to sort by Company Name',,,300,1)
        Else
          packet = clip(packet) & '<th width="'&clip(300)&'" Title="'&p_web.Translate('Click here to sort by Company Name')&'">'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','LookupGenericAccounts','Branch','Click here to sort by Branch',,,280,1)
        Else
          packet = clip(packet) & '<th width="'&clip(280)&'" Title="'&p_web.Translate('Click here to sort by Branch')&'">'&p_web.Translate('Branch')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('tra1:recordno',lower(Thisview{prop:order}),1,1) = 0 !and TRAHUBAC{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'TRA1:RecordNo'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('TRA1:RecordNo'),p_web.GetValue('TRA1:RecordNo'),p_web.GetSessionValue('TRA1:RecordNo'))
      loc:FilterWas = 'Upper(tra1:HeadAcc) = Upper(''' & p_web.GSV('BookingAccount') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupGenericAccounts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupGenericAccounts_Filter')
    p_web.SetSessionValue('LookupGenericAccounts_FirstValue','')
    p_web.SetSessionValue('LookupGenericAccounts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,TRAHUBAC,TRA1:RecordNoKey,loc:PageRows,'LookupGenericAccounts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If TRAHUBAC{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(TRAHUBAC,loc:firstvalue)
              Reset(ThisView,TRAHUBAC)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If TRAHUBAC{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(TRAHUBAC,loc:lastvalue)
            Reset(ThisView,TRAHUBAC)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(TRA1:RecordNo)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupGenericAccounts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupGenericAccounts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupGenericAccounts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupGenericAccounts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupGenericAccounts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupGenericAccounts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupGenericAccounts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1LookupGenericAccounts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupGenericAccounts.locate(''Locator1LookupGenericAccounts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupGenericAccounts.cl(''LookupGenericAccounts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('LookupGenericAccounts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupGenericAccounts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupGenericAccounts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupGenericAccounts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupGenericAccounts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupGenericAccounts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = TRA1:RecordNo
    p_web._thisrow = p_web._nocolon('TRA1:RecordNo')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupGenericAccounts:LookupField')) = TRA1:RecordNo and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((TRA1:RecordNo = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="LookupGenericAccounts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If TRAHUBAC{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(TRAHUBAC)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If TRAHUBAC{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(TRAHUBAC)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','TRA1:RecordNo',clip(loc:field),,loc:checked,,,'onclick="LookupGenericAccounts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','TRA1:RecordNo',clip(loc:field),,'checked',,,'onclick="LookupGenericAccounts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::TRA1:SubAcc
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(300)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::TRA1:SubAccName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(280)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::TRA1:SubAccBranch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="LookupGenericAccounts.omv(this);" onMouseOut="LookupGenericAccounts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var LookupGenericAccounts=new browseTable(''LookupGenericAccounts'','''&clip(loc:formname)&''','''&p_web._jsok('TRA1:RecordNo',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('TRA1:RecordNo')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'LookupGenericAccounts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'LookupGenericAccounts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupGenericAccounts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupGenericAccounts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupGenericAccounts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupGenericAccounts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(TRAHUBAC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(TRAHUBAC)
  Bind(TRA1:Record)
  Clear(TRA1:Record)
  NetWebSetSessionPics(p_web,TRAHUBAC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('TRA1:RecordNo',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&TRA1:RecordNo,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupGenericAccounts',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupGenericAccounts',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupGenericAccounts',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::TRA1:SubAcc   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('TRA1:SubAcc_'&TRA1:RecordNo,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(TRA1:SubAcc,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::TRA1:SubAccName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('TRA1:SubAccName_'&TRA1:RecordNo,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(TRA1:SubAccName,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::TRA1:SubAccBranch   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('TRA1:SubAccBranch_'&TRA1:RecordNo,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(TRA1:SubAccBranch,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = TRA1:RecordNo

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('TRA1:RecordNo',TRA1:RecordNo)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('TRA1:RecordNo',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('TRA1:RecordNo'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('TRA1:RecordNo'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormDeletePart       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locAlertMessage      STRING(255)                           !
locErrorMessage      STRING(255)                           !
locScrapRestock      BYTE                                  !
tmp:delete           BYTE                                  !
tmp:Scrap            BYTE                                  !
FilesOpened     Long
ESTPARTS::State  USHORT
STOFAULT::State  USHORT
STOCK::State  USHORT
PARTS::State  USHORT
WARPARTS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
                    Map
AddSolder               Procedure(String fType)
ShowAlert               Procedure(String fAlert)
RemoveWarrantyPartStockHistory      Procedure(String fInformation,STRING fType)
                    end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormDeletePart')
  loc:formname = 'FormDeletePart_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormDeletePart','')
    p_web._DivHeader('FormDeletePart',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeletePart',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormDeletePart',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(WARPARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(WARPARTS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormDeletePart_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locPartNumber',locPartNumber)
  p_web.SetSessionValue('locDescription',locDescription)
  p_web.SetSessionValue('locAlertMessage',locAlertMessage)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locScrapRestock',locScrapRestock)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPartNumber')
    locPartNumber = p_web.GetValue('locPartNumber')
    p_web.SetSessionValue('locPartNumber',locPartNumber)
  End
  if p_web.IfExistsValue('locDescription')
    locDescription = p_web.GetValue('locDescription')
    p_web.SetSessionValue('locDescription',locDescription)
  End
  if p_web.IfExistsValue('locAlertMessage')
    locAlertMessage = p_web.GetValue('locAlertMessage')
    p_web.SetSessionValue('locAlertMessage',locAlertMessage)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locScrapRestock')
    locScrapRestock = p_web.GetValue('locScrapRestock')
    p_web.SetSessionValue('locScrapRestock',locScrapRestock)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormDeletePart_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Delete'
  p_web.site.SaveButton.Image = 'images/pdelete.png'
  
  p_web.SSV('DeletePart:ViewOnly',0)
  p_web.SSV('Hide:ScrapRestock',0)
  p_web.SSV('locAlertMessage','Click "Delete" to confirm deletion.')
  p_web.SSV('locErrorMessage','')
  p_web.SSV('Hide:ScrapRestock',1)
  p_web.SSV('locScrapRestock',0)
  
  If p_web.IfExistsValue('wpr:Record_Number')
      p_web.StoreValue('wpr:Record_Number')
  End ! If p_web.IfExistsValue('a')
  
  If p_web.IfExistsValue('par:Record_Number')
      p_web.StoreValue('par:Record_Number')
  End ! If p_web.IfExistsValue('a')
  
  If p_web.IfExistsValue('DelType')
      p_web.StoreValue('DelType')
  End ! If p_web.IfExistsValue('a')
  
  Case p_web.GSV('DelType')
  OF 'WAR'
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number = p_web.GSV('wpr:Record_Number')
      If (Access:WARPARTS.TryFetch(wpr:RecordNumberKey) = Level:Benign)
          
      End ! End
      p_web.SSV('locPartNumber',wpr:Part_Number)
      p_web.SSV('locDescription',wpr:Description)
      
      IF (wpr:Part_Number = 'EXCH')
          IF (p_web.GSV('job:Exchange_Unit_Number') > 0) ! #11937 Allow to delete an "EXCH" entry if there is no exchange unit attached. (Bryan: 19/01/2011)
              p_web.SSV('locErrorMessage','Click the "Exchange Unit" button on the Amend Job to remove an exchange unit')
          END !IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
      ELSE
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = wpr:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
          END
          
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (sto:Location = p_web.GSV('ARC:SiteLocation'))
                  p_web.SSV('locErrorMessage','Cannot delete! This part was added at the ARC')
              ELSE
                  IF (p_web.GSV('job:Invoice_Number_Warranty') <> 0)
                      p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  END
              END
          ELSE
              IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                  p_web.SSV('locErrorMessage','Cannot Delete! This part was added at the RRC')
              ELSE
                  IF (p_web.GSV('job:Invoice_Number_Warranty') <> 0)
                      p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  END
              END
          END
      
          IF (wpr:WebOrder)
              p_web.SSV('locAlertMessage','Warning! A Retail Order has been raised for this part.')
          ELSE
              IF (wpr:Adjustment = 'YES')
              ELSE
                  If (sto:Ref_Number > 0)
                      IF (sto:Sundry_Item = 'YES')
                      ELSE
                          IF (RapidLocation(sto:Location))
                              IF (sto:AttachBySolder)
                                  p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                              ELSE ! IF (sto:AttachBySolder) 
                                  IF (wpr:PartAllocated = 0)
                                      p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                  ELSE
                                      p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                  END
                              END ! IF (sto:AttachBySolder)
                          ELSE ! IF (RapidLocation(sto:Location))
                              p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                              p_web.SSV('Hide:ScrapRestock',0)
                          END ! IF (RapidLocation(sto:Location))
                          
                      END
                      
                  END
                  
                  
              END
          END
      END
      
      
  OF 'CHA'
      error# = 0
      Access:PARTS.ClearKey(par:recordnumberkey)
      par:Record_Number = p_web.GSV('par:Record_Number')
      IF (Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign)
      END
      p_web.SSV('locPartNumber',par:Part_Number)
      p_web.SSV('locDescription',par:Description)
      
      IF (par:Part_Number = 'EXCH')
          IF (p_web.GSV('job:Exchange_Unit_Number') > 0) ! #11937 Allow to delete an "EXCH" entry if there is no exchange unit attached. (Bryan: 19/01/2011)
              p_web.SSV('locErrorMessage','Click the "Exchange Unit" button on the Amend Job screen to remove an exchange unit')
              error# = 1
          END
          
      ELSE
          if (par:Status = 'RTS')        
              if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
                  p_web.SSV('locErrorMessage','You do not have access to delete this part')
                  error# = 1
              end ! if
          end ! if
          if (error# = 0)
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = par:Part_Ref_Number
              if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
              end ! if
              
              IsJobInvoiced(p_web)
              IF (p_web.GSV('IsJobInvoiced') = 1)
                  p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  ERROR# = 1
              ELSE
                  IF (p_web.GSV('BookingSite') = 'RRC')
                      IF (sto:Location = p_web.GSV('ARC:SiteLocation'))
                          p_web.SSV('locErrorMessage','Cannot delete! This part was added at the ARC')
                          ERROR# = 1
                      END
                  ELSE
                      IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                          p_web.SSV('locErrorMessage','Cannot Delete! This part was added at the RRC')
                          ERROR#= 1
                      END
                  END                
              END
          end ! if
          IF (ERROR# = 0)
              IF (par:WebOrder)
                  p_web.SSV('locAlertMessage','Warning! A Retail Order has been raised for this part.')
              ELSE
                  IF (par:Adjustment = 'YES')
                  ELSE
                      If (sto:Ref_Number > 0)
                          IF (sto:Sundry_Item = 'YES')
                          ELSE
                              IF (RapidLocation(sto:Location))
                                  IF (sto:AttachBySolder)
                                      p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                                  ELSE ! IF (sto:AttachBySolder) 
                                      IF (par:PartAllocated = 0)
                                          p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                      ELSE
                                          p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                      END
                                  END ! IF (sto:AttachBySolder)
                              ELSE ! IF (RapidLocation(sto:Location))
                                  p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                                  p_web.SSV('Hide:ScrapRestock',0)
                              END ! IF (RapidLocation(sto:Location))
                          
                          END
                      
                      END
                  
                  
                  END
              END
          END ! if
          
          
      END ! IF
  OF 'EST'
      error# = 0
      Access:ESTPARTS.ClearKey(epr:record_number_key)
      epr:Record_Number = p_web.GSV('epr:Record_Number')
      IF (Access:ESTPARTS.TryFetch(epr:record_number_key) = Level:Benign)
      END
      p_web.SSV('locPartNumber',epr:Part_Number)
      p_web.SSV('locDescription',epr:Description)
  
      if (epr:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('locErrorMessage','You do not have access to delete this part')
              error# = 1
          end ! if
      end ! if
      if (error# = 0)
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number = epr:Part_Ref_Number
          if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          end ! if
  
          if (p_web.GSV('job:Estimate_Accepted') = 'YES')
              p_web.SSV('locErrorMessage','Cannot delete! The estimate has been accepted.')
              error# = 1
          else
              if (p_web.GSV('job:Estimate_Rejected') = 'YES')
                  p_web.SSV('locErrorMessage','Cannot delete! The estimate has been rejected.')
                  error# = 1
              end
          end
      end ! if
      IF (ERROR# = 0)
              IF (epr:Adjustment = 'YES' or epr:UsedOnRepair = 0)
                  IF (sto:AttachBySolder)
                      p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                  END
              ELSE
                  If (sto:Ref_Number > 0)
                      IF (sto:Sundry_Item = 'YES')
                      ELSE
                          IF (RapidLocation(sto:Location))
                              IF (sto:AttachBySolder)
                                  p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                              ELSE ! IF (sto:AttachBySolder)
                                  IF (epr:PartAllocated = 0)
                                      p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                  ELSE
                                      p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                  END
                              END ! IF (sto:AttachBySolder)
                          ELSE ! IF (RapidLocation(sto:Location))
                              p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                              p_web.SSV('Hide:ScrapRestock',0)
                          END ! IF (RapidLocation(sto:Location))
                      END
                  END
              END
      END ! if
  
  ELSE
      
  END
  
  IF (p_web.GSV('locErrorMessage') <> '')
      p_web.SSV('locAlertMessage','')
      ShowALert(p_web.GSV('locErrorMessage'))
      p_web.SSV('DeletePart:ViewOnly',1)
  END
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locPartNumber = p_web.RestoreValue('locPartNumber')
 locDescription = p_web.RestoreValue('locDescription')
 locAlertMessage = p_web.RestoreValue('locAlertMessage')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locScrapRestock = p_web.RestoreValue('locScrapRestock')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormDeletePart_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormDeletePart_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormDeletePart_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('DeletePart:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormDeletePart" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormDeletePart" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormDeletePart" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Delete Part') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Delete Part',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormDeletePart">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormDeletePart" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormDeletePart')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormDeletePart')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormDeletePart'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormDeletePart')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormDeletePart_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDescription
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormDeletePart_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locAlertMessage
      do Comment::locAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorMessage
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locScrapRestock
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locScrapRestock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locScrapRestock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locPartNumber  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPartNumber',p_web.GetValue('NewValue'))
    locPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPartNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartNumber',p_web.GetValue('Value'))
    locPartNumber = p_web.GetValue('Value')
  End
  do Value::locPartNumber
  do SendAlert

Value::locPartNumber  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPartNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locPartNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPartNumber'',''formdeletepart_locpartnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartNumber',p_web.GetSessionValueFormat('locPartNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_value')

Comment::locPartNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locDescription  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDescription  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDescription',p_web.GetValue('NewValue'))
    locDescription = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDescription
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDescription',p_web.GetValue('Value'))
    locDescription = p_web.GetValue('Value')
  End
  do Value::locDescription
  do SendAlert

Value::locDescription  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locDescription
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locDescription')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locDescription'',''formdeletepart_locdescription_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locDescription',p_web.GetSessionValueFormat('locDescription'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDeletePart_' & p_web._nocolon('locDescription') & '_value')

Comment::locDescription  Routine
      loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAlertMessage',p_web.GetValue('NewValue'))
    locAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAlertMessage',p_web.GetValue('Value'))
    locAlertMessage = p_web.GetValue('Value')
  End

Value::locAlertMessage  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locAlertMessage') & '_value',Choose(p_web.GSV('locAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAlertMessage') = '')
  ! --- DISPLAY --- locAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAlertMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locAlertMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locAlertMessage') & '_comment',Choose(p_web.GSV('locAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locScrapRestock  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_prompt',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Delete Option')
  If p_web.GSV('Hide:ScrapRestock') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locScrapRestock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locScrapRestock',p_web.GetValue('NewValue'))
    locScrapRestock = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locScrapRestock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locScrapRestock',p_web.GetValue('Value'))
    locScrapRestock = p_web.GetValue('Value')
  End
  do Value::locScrapRestock
  do SendAlert

Value::locScrapRestock  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_value',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ScrapRestock') = 1)
  ! --- RADIO --- locScrapRestock
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locScrapRestock')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locScrapRestock') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locScrapRestock'',''formdeletepart_locscraprestock_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locScrapRestock',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locScrapRestock_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Scrap') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locScrapRestock') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locScrapRestock'',''formdeletepart_locscraprestock_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locScrapRestock',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locScrapRestock_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_value')

Comment::locScrapRestock  Routine
    loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_comment',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ScrapRestock') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormDeletePart_locPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPartNumber
      else
        do Value::locPartNumber
      end
  of lower('FormDeletePart_locDescription_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDescription
      else
        do Value::locDescription
      end
  of lower('FormDeletePart_locScrapRestock_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locScrapRestock
      else
        do Value::locScrapRestock
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormDeletePart',0)

PreCopy  Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormDeletePart',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormDeletePart:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  p_web.setsessionvalue('showtab_FormDeletePart',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormDeletePart_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('Hide:ScrapRestock') = 0)
      if (p_web.GSV('locScrapRestock') = '')
          loc:Invalid = 'locScrapRestock'
          loc:Alert = 'You must select "Scrap" or "Restock"'
          exit
      ENd
  End
  p_web.DeleteSessionValue('FormDeletePart_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  ! Delete Part
  tmp:Delete = 0
  tmp:Scrap = 0
  Case p_web.GSV('DelType')
  of 'WAR'
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number = p_web.GSV('wpr:Record_Number')
      If (Access:WARPARTS.TryFetch(wpr:RecordNumberKey))
          
      End
      
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = wpr:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
  
      if (wpr:WebOrder)
          RemoveFromStockAllocation(wpr:Record_Number,'WAR')   
      else
          IF (wpr:Adjustment = 'YES')
              tmp:delete = 1
          ELSE
              IF (sto:Sundry_Item = 'YES')
                  tmp:delete = 1
              ELSE
                  
                  If (RapidLocation(sto:Location))
                      if (sto:AttachBySolder)
                          tmp:Delete = 1
                          tmp:Scrap = 1
                          AddSolder('WAR')
                      else ! if (sto:AttachBySolder)
                          if (wpr:PartAllocated = 0)
                      
                              sto:Quantity_STock += wpr:Quantity
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  tmp:Delete = 1
                                  RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','WAR')
                                  if (sto:Suspend)
                                      ! Not has stock. Unsuspend part
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          if (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              wpr:Purchase_Cost, |
                                              wpr:Sale_Cost, |
                                              wpr:Retail_Cost, |
                                              'PART UNSUSPENDED', |
                                              '', |
                                              p_web.GSV('BookingUserCode'),|
                                              sto:Quantity_Stock))                                       
                                          end
                                       
                                      end
                                  end   
                              End
                          else ! if (wpr:PartAllocated = 0)
                              wpr:PartAllocated = 0
                              wpr:Status = 'RET'
                              Access:WARPARTS.TryUpdate()
                              p_web.FileToSessionQueue(WARPARTS)
                              p_web.SSV('AddToStockAllocation:Type','WAR')
                              p_web.SSV('AddToStockAllocation:Status','RET')
                              p_web.SSV('AddToStockAllocation:Qty',wpr:Quantity)
                              addToStockAllocation(p_web)  
                          end
                      end
                  else ! If (RapidLocation(sto:Location))
                      Case p_web.GSV('locScrapRestock')
                      of 1 ! Scrap
                          tmp:Delete = 1
                          tmp:Scrap = 1
                      of 2 ! Restock
                          sto:Quantity_Stock += wpr:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('','WAR')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          wpr:Purchase_Cost, |
                                          wpr:Sale_Cost, |
                                          wpr:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
                                       
                                  end
                              end   
                      
                          End
                  
                      end
              
                  end ! If (RapidLocation(sto:Location))
              END
          END
          
      end
      if (tmp:Scrap)
          RemoveFromSTockAllocation(wpr:Record_Number,'WAR')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP WARRANTY PART: ' & Clip(wpr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(wpr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')    
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & wpr:Quantity)    
          AddToAudit(p_web)
      else
          RemoveFromSTockAllocation(wpr:Record_Number,'WAR')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','WARRANTY PART DELETED: ' & Clip(wpr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(wpr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')    
          End
          AddToAudit(p_web)        
      end 
      Relate:WARPARTS.Delete(0)
  OF 'CHA'
      Access:PARTS.Clearkey(par:RecordNumberKey)
      par:Record_Number = p_web.GSV('par:Record_Number')
      If (Access:PARTS.TryFetch(par:RecordNumberKey))
          
      End
      
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End        
      if (par:WebOrder)
          RemoveFromStockAllocation(par:Record_Number,'CHA')   
      else
          if (par:Adjustment = 'YES')
              tmp:delete = 1
          ELSE
              IF (sto:Sundry_Item = 'YES')
                  tmp:delete = 1
              ELSE
                  If (RapidLocation(sto:Location))
                      if (sto:AttachBySolder)
                          tmp:Delete = 1
                          tmp:Scrap = 1
                          AddSolder('CHA')
                      else ! if (sto:AttachBySolder)
                          if (par:PartAllocated = 0)
                              sto:Quantity_STock += par:Quantity
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  tmp:Delete = 1
                                  RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','CHA')
                                  if (sto:Suspend)
                                      ! Not has stock. Unsuspend part
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          if (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              par:Purchase_Cost, |
                                              par:Sale_Cost, |
                                              par:Retail_Cost, |
                                              'PART UNSUSPENDED', |
                                              '', |
                                              p_web.GSV('BookingUserCode'),|
                                              sto:Quantity_Stock))                                       
                                          end
                                       
                                      end
                                  end   
                              End
                          else ! if (wpr:PartAllocated = 0)
                              par:PartAllocated = 0
                              par:Status = 'RET'
                              Access:PARTS.TryUpdate()
                              p_web.FileToSessionQueue(PARTS)
                              p_web.SSV('AddToStockAllocation:Type','CHA')
                              p_web.SSV('AddToStockAllocation:Status','RET')
                              p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                              addToStockAllocation(p_web)  
                          end
                      end
                  else ! If (RapidLocation(sto:Location))
                      Case p_web.GSV('locScrapRestock')
                      of 1 ! Scrap
                          tmp:Delete = 1
                          tmp:Scrap = 1
                      of 2 ! Restock
                          sto:Quantity_Stock += par:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('','CHA')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          par:Purchase_Cost, |
                                          par:Sale_Cost, |
                                          par:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
                                       
                                  end
                              end   
                          End
                      end
                  end ! If (RapidLocation(sto:Location))
              END
          END
      end
      if (tmp:Scrap)
          RemoveFromSTockAllocation(par:Record_Number,'CHA')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(par:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')    
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & par:Quantity)    
          AddToAudit(p_web)
      else
          RemoveFromSTockAllocation(par:Record_Number,'CHA')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','CHARGEABLE PART DELETED: ' & Clip(par:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(par:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')    
          End
          AddToAudit(p_web)        
      end 
      Relate:PARTS.Delete(0)
  OF 'EST'
      Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
      epr:Record_Number = p_web.GSV('epr:Record_Number')
      If (Access:ESTPARTS.TryFetch(epr:Record_Number_Key))
          
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = epr:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
  
      if (epr:Adjustment = 'YES')
          tmp:delete = 1
          if (sto:AttachBySolder)
              tmp:Delete = 1
              tmp:Scrap = 1
              AddSolder('EST')
          end ! if (sto:AttachBySolder)
  
      ELSE
          IF (sto:Sundry_Item = 'YES')
              tmp:delete = 1
          ELSE
              If (RapidLocation(sto:Location))
                  if (sto:AttachBySolder)
                      tmp:Delete = 1
                      tmp:Scrap = 1
                      AddSolder('EST')
                  else ! if (sto:AttachBySolder)
                      if (epr:PartAllocated = 0)
                          sto:Quantity_STock += epr:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','EST')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          epr:Purchase_Cost, |
                                          epr:Sale_Cost, |
                                          epr:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
                          End
                      else ! if (wpr:PartAllocated = 0)
                          epr:PartAllocated = 0
                          epr:Status = 'RET'
                          Access:ESTPARTS.TryUpdate()
                          p_web.FileToSessionQueue(ESTPARTS)
                          p_web.SSV('AddToStockAllocation:Type','EST')
                          p_web.SSV('AddToStockAllocation:Status','RET')
                          p_web.SSV('AddToStockAllocation:Qty',epr:Quantity)
                          addToStockAllocation(p_web)  
                      end
                  end
              else ! If (RapidLocation(sto:Location))
                  Case p_web.GSV('locScrapRestock')
                  of 1 ! Scrap
                      tmp:Delete = 1
                      tmp:Scrap = 1
                  of 2 ! Restock
                      sto:Quantity_Stock += epr:Quantity
                      If (Access:STOCK.TryUpdate() = Level:Benign)
                          tmp:Delete = 1
                          RemoveWarrantyPartStockHistory('','EST')
                          if (sto:Suspend)
                              ! Not has stock. Unsuspend part
                              sto:Suspend = 0
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  if (AddToStockHistory(sto:Ref_Number, |
                                      'ADD', |
                                      '', |
                                      0, |
                                      0, |
                                      0, |
                                      epr:Purchase_Cost, |
                                      epr:Sale_Cost, |
                                      epr:Retail_Cost, |
                                      'PART UNSUSPENDED', |
                                      '', |
                                      p_web.GSV('BookingUserCode'),|
                                      sto:Quantity_Stock))
                                  end
  
                              end
                          end
  
                      End
  
                  end
  
              end ! If (RapidLocation(sto:Location))
          END
  
      END
  
      if (tmp:Scrap)
          if (epr:UsedOnRepair)
              RemoveFromSTockAllocation(epr:Record_Number,'EST')
          end
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP ESTIMATE PART: ' & Clip(epr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(epr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & epr:Quantity)
          AddToAudit(p_web)
      else
          RemoveFromSTockAllocation(epr:Record_Number,'EST')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','ESTIMATE PART DELETED: ' & Clip(epr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(epr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web)
      end
      Relate:ESTPARTS.Delete(0)
  
  end
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  p_web.StoreValue('locPartNumber')
  p_web.StoreValue('locDescription')
  p_web.StoreValue('locAlertMessage')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('locScrapRestock')
AddSolder           Procedure(String fType)
    Code
  
        Case fType
        of 'WAR'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = wpr:Part_Number
                stf:Description = wpr:Description
                stf:Quantity    = wpr:Quantity
                stf:PurchaseCost    = wpr:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        of 'CHA'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = par:Part_Number
                stf:Description = par:Description
                stf:Quantity    = par:Quantity
                stf:PurchaseCost    = par:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        of 'EST'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = epr:Part_Number
                stf:Description = epr:Description
                stf:Quantity    = epr:Quantity
                stf:PurchaseCost    = epr:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        end
        
RemoveWarrantyPartStockHistory      Procedure(String fInformation,STRING fType)
Code
   
    CASE fType
    OF 'WAR'
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
            'ADD', | ! Transaction_Type
            wpr:Despatch_Note_Number, | ! Depatch_Note_Number
            p_web.GSV('job:Ref_Number'), | ! Job_Number
            0, | ! Sales_Number
            wpr:Quantity, | ! Quantity
            wpr:Purchase_Cost, | ! Purchase_Cost
            wpr:Sale_Cost, | ! Sale_Cost
            wpr:Retail_Cost, | ! Retail_Cost
            fInformation, | ! Notes
            'WARRANTY PART REMOVED FROM JOB', |
            p_web.GSV('BookingUserCode'),|
            sto:Quantity_Stock) ! Information
        
        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    OF 'CHA'
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
            'ADD', | ! Transaction_Type
            par:Despatch_Note_Number, | ! Depatch_Note_Number
            p_web.GSV('job:Ref_Number'), | ! Job_Number
            0, | ! Sales_Number
            par:Quantity, | ! Quantity
            par:Purchase_Cost, | ! Purchase_Cost
            par:Sale_Cost, | ! Sale_Cost
            par:Retail_Cost, | ! Retail_Cost
            fInformation, | ! Notes
            'WARRANTY PART REMOVED FROM JOB', |
            p_web.GSV('BookingUserCode'),|
            sto:Quantity_Stock) ! Information
        
        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory        
    END ! Case
    
ShowAlert     Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
IndividualDespatch   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
tmp:LoanModelNumber  STRING(30)                            !
Ans                  LONG                                  !
locAccessoryErrorMessage STRING(255)                       !
locIMEINumber        STRING(30)                            !
locJobNumber         LONG                                  !
locErrorMessage      STRING(255)                           !
locMessage           STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locSecurityPackID    STRING(30)                            !
locAccessoryPassword STRING(30)                            !
locAccessoryPasswordMessage STRING(255)                    !
locAuditTrail        STRING(255)                           !
locConsignmentNumber STRING(30)                            !
FilesOpened     Long
LOANACC::State  USHORT
JOBACC::State  USHORT
WEBJOB::State  USHORT
WAYBILLJ::State  USHORT
WAYBILLS::State  USHORT
TagFile::State  USHORT
COURIER::State  USHORT
MULDESPJ::State  USHORT
SUBTRACC::State  USHORT
LOAN::State  USHORT
TRADEACC::State  USHORT
EXCHANGE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
cou:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('IndividualDespatch')
  loc:formname = 'IndividualDespatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('IndividualDespatch','')
    p_web._DivHeader('IndividualDespatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      Routine
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')        
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('Show:DespatchInvoiceDetails',0) ! Show invoice details on despatch screen
    p_web.SSV('CreateDespatchInvoice',0) ! Auto create invoice
    p_web.SSV('Show:ConsignmentNumber',0)

ValidateUnitDetails Routine
    ClearTagFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locErrorMessage','')
    p_web.SSV('locMessage','')
    p_web.SSV('DespatchType','')
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('locSecurityPackID','')
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')    
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)

    if (p_web.GSV('locJobNumber')= '')
        exit
    end
    if (p_web.GSV('locIMEINumber') = '')
        exit
    end

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    If (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage','Cannot find selected Job Number!')
        Exit
    end

        
    if (JobInUse(job:Ref_Number))
        p_web.SSV('locErrorMessage','Cannot despatch the selected job is in use.')
        exit
    end
    
    p_web.FileToSessionQueue(JOBS)
    
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
    End
    p_web.FileToSessionQueue(JOBSE)
    
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber  = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END
    p_web.FileToSessionQueue(WEBJOB)
    
    if (p_web.GSV('BookingSite') = 'RRC')
        ! RRC Despatch
        
        IF (wob:ReadyToDespatch <> 1)
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            EXIT
        END
        
        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            p_web.SSV('locErrorMessage','The selected job is not from your RRC.')
            EXIT
        END
        
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                if (sub:UseCustDespAdd = 'YES')
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubCustomer) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                else
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubDelivery) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                end
            end
        end

        found# = 0
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        set(mulj:JobNumberOnlyKey,mulj:JobNumberOnlyKey)
        Loop Until Access:MULDESPJ.Next()
            if (mulj:JobNumber <> job:Ref_Number)
                break
            end
            Access:MULDESP.Clearkey(muld:RecordNumberKey)
            muld:RecordNumber = mulj:RefNumber
            if (Access:MULDESP.TryFetch(muld:RecordNumberKey)= Level:Benign)
                if (muld:HeadAccountNumber = p_web.GSV('BookingAccount'))
                    found# = 1
                    break
                end
            end
        end
        if (found# = 1)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end
        
        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And jobe:Despatchtype = 'JOB')
            if (job:Loan_Unit_Number <> 0)
                p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                exit
            end
        end
        
        Access:COURIER.ClearKey(cou:Courier_Key)
        CASE jobe:DespatchType
        OF 'JOB'
            cou:Courier = job:Courier
        OF 'EXC'
            cou:Courier = job:Exchange_Courier
        of 'LOA'
            cou:Courier = job:Loan_Courier
        end
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit            
        end
        
        
        p_web.SSV('DespatchType',jobe:DespatchType)
        
       
        
    else ! if (p_web.GSV('BookingSite') = 'RRC')
        ! ARC Despatch
        
        ! Can job be despatched?
        if (job:Chargeable_Job = 'YES')
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('Default:ARCLocation')
            if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) )
                
            End 
            if (tra:Despatch_Paid_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
                p_web.SSV('locErrorMessage','Unable to despatch. The selected job has not been paid.')
                exit
            end
            if (tra:Despatch_Invoiced_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
                p_web.SSV('locErrorMessage','Unble to despatch. The selected job has not been invoiced.')
                exit
            end
        end
        
        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And job:Despatch_type = 'JOB')
            if (job:Loan_Unit_Number <> 0)
                p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                exit
            end
        end
        
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        if (Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:benign)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end
        
        if (job:Despatched <> 'REA')
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            exit
        end

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_NUmber_Key) = Level:Benign)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            if (access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                if (tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES')
                    if (sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                        
                else
                    if (tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                end
            end
        end
        
        Access:COURIER.Clearkey(cou:Courier_Key)
        cou:Courier = job:Current_Courier
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit
        end
        
        p_web.SSV('DespatchType',job:Despatch_Type)
        
    end
    
    p_web.FileToSessionQueue(COURIER)
    
    case p_web.GSV('DespatchType')
    of 'JOB'
        if (job:ESN <> p_web.GSV('locIMEINumber'))
            p_web.SSV('locErrorMessage','The selected I.M.E.I. Number does not match the selected job.')
            exit
        end
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    of 'EXC'
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Exchange Despatch! Unable to find Exchange Unit on selected job.')
            exit
        else
            if (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Exchange Despatch! The selected I.M.E.I. Number does not match the Exchange Unit attached to the selected job.')
                exit
            end
        end
        p_web.SSV('tmp:LoanModelNumber',xch:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('ValidateAccessories',0)
        p_web.SSV('Hide:ValidateAccessoriesButton',1)
        p_web.SSV('AccessoriesValidated',1)
    of 'LOA'
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Loan Despatch! Unable to find Loan Unit on selected job.')
            exit
        else
            if (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Loan Despatch! The selected I.M.E.I. Number does not match the Loan Unit attached to the selected job.')
                exit    
            end
        end
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Loan_Unit_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    end

    
    p_web.SSV('UnitValidated',1)
    
    p_web.SSV('ValidateButtonText','Despatch Another Unit')
    
    ! Can the job be invoiced?
    IF (job:Bouncer <> 'X' AND job:Chargeable_Job = 'YES')
        p_web.SSV('Show:DespatchInvoiceDetails',1)
        IF (InvoiceSubAccounts(job:Account_Number))
            IF (sub:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE sub:InvoiceType
                OF 0 ! Manual invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 !Auto Invoice
                    IF (job:Invoice_Number = '')
                        ! Create Invoice
                        p_web.SSV('CreateDespatchInvoice',1)
                    END
                    
                END
                
                
            END
            
        ELSE
            IF (tra:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE tra:InvoiceType
                OF 0 ! Manual Invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 ! Auto Invoice
                    p_web.SSV('CreateDespatchInvoice',1)
                END
                 
            END
            
        END
        
    END
    

    
ShowConsignmentNumber       ROUTINE
    Access:COURIER.ClearKey(cou:Courier_Key)
    cou:Courier = p_web.GSV('cou:Courier')
    IF (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
        p_web.FileToSessionQueue(COURIER)
    
        IF (p_web.GSV('cou:PrintWaybill') <> 1 AND p_web.GSV('cou:AutoConsignmentNo') <> 1)
            p_web.SSV('Show:ConsignmentNumber',1)
        ELSE
            p_web.SSV('Show:ConsignmentNumber',0)
        END
    END
    
    
        
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('tmp:LoanModelNumber')
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('locMessage')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locSecurityPackID')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('locAccessoryPasswordMessage')
    p_web.DeleteSessionValue('locAuditTrail')
    p_web.DeleteSessionValue('locConsignmentNumber')

    ! Other Variables
    p_web.DeleteSessionValue('UnitValidated')
    p_web.DeleteSessionValue('ValidateButtonText')
    p_web.DeleteSessionValue('AccessoryRefNumber')
    p_web.DeleteSessionValue('DespatchType')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('ValidateAccessories')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('AccessoryPasswordRequired')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('Show:DespatchInvoiceDetails')
    p_web.DeleteSessionValue('CreateDespatchInvoice')

OpenFiles  ROUTINE
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SSV('ValidateButtonText','Validate Unit Details')
  Do ClearVariables
  p_web.SetValue('IndividualDespatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do deletesessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'cou:Courier'
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locConsignmentNumber')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  p_web.SetSessionValue('cou:Courier',cou:Courier)
  p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  End
  if p_web.IfExistsValue('locAccessoryPasswordMessage')
    locAccessoryPasswordMessage = p_web.GetValue('locAccessoryPasswordMessage')
    p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  End
  if p_web.IfExistsValue('cou:Courier')
    cou:Courier = p_web.GetValue('cou:Courier')
    p_web.SetSessionValue('cou:Courier',cou:Courier)
  End
  if p_web.IfExistsValue('locConsignmentNumber')
    locConsignmentNumber = p_web.GetValue('locConsignmentNumber')
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  End
  if p_web.IfExistsValue('locSecurityPackID')
    locSecurityPackID = p_web.GetValue('locSecurityPackID')
    p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('IndividualDespatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
 locAccessoryPasswordMessage = p_web.RestoreValue('locAccessoryPasswordMessage')
 locConsignmentNumber = p_web.RestoreValue('locConsignmentNumber')
 locSecurityPackID = p_web.RestoreValue('locSecurityPackID')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('IndividualDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('IndividualDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('IndividualDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="IndividualDespatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="IndividualDespatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="IndividualDespatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Individual Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Individual Despatch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_IndividualDespatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_IndividualDespatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_IndividualDespatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_IndividualDespatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_IndividualDespatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_IndividualDespatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateJobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateJobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locErrorMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TagValidateLoanAccessories
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryErrorMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPasswordMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::cou:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::cou:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::cou:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locConsignmentNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locConsignmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locConsignmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSecurityPackID
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSecurityPackID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSecurityPackID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locJobNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locJobNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('UnitValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''individualdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment')

Prompt::locIMEINumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('UnitValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''individualdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment')

Validate::buttonValidateJobDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateJobDetails',p_web.GetValue('NewValue'))
    do Value::buttonValidateJobDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do ValidateUnitDetails
  do Value::buttonValidateJobDetails
  do SendAlert
  do Value::locErrorMessage  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonValidateJobDetails  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateJobDetails'',''individualdespatch_buttonvalidatejobdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateJob',p_web.GSV('ValidateButtonText'),'button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value')

Comment::buttonValidateJobDetails  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_prompt',Choose(p_web.GSV('locErrorMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locErrorMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TagValidateLoanAccessories  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Validate Accessories')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','IndividualDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  
  ! Validate
  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  AccessoryCheck(p_web)
  Case p_web.GSV('AccessoryCheck:Return')
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end            
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')    
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end                
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonFailAccessory  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''individualdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_prompt',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('NewValue'))
    locAccessoryMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('Value'))
    locAccessoryMessage = p_web.GetValue('Value')
  End

Value::locAccessoryMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryMessage') = '')
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value')

Comment::locAccessoryMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_prompt',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('NewValue'))
    locAccessoryErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('Value'))
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End

Value::locAccessoryErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryErrorMessage') = '')
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value')

Comment::locAccessoryErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPassword  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Password Required For Confirmation')
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt')

Validate::locAccessoryPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('NewValue'))
    locAccessoryPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('Value'))
    locAccessoryPassword = p_web.GetValue('Value')
  End
  If locAccessoryPassword = ''
    loc:Invalid = 'locAccessoryPassword'
    loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locAccessoryPassword = Upper(locAccessoryPassword)
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  do Value::locAccessoryPassword
  do SendAlert

Value::locAccessoryPassword  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('AccessoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locAccessoryPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locAccessoryPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''individualdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value')

Comment::locAccessoryPassword  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmMismatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryPasswordMessage','Password Required')    
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryPasswordMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do SendAlert
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonValidateAccessories  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonConfirmMismatch  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''individualdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmMismatch',p_web.GSV('ConfirmMismatchText'),'button-entryfield',loc:formname,,,,loc:javascript,0,'images\tick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value')

Comment::buttonConfirmMismatch  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFailAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFailAccessory',p_web.GetValue('NewValue'))
    do Value::buttonFailAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Fail Accessory Check
  p_web.SSV('GetStatus:StatusNumber',850)
  p_web.SSV('GetStatus:Type',p_web.GSV('DespatchType'))
  GetStatus(p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
      IF (tag:tagged = 1)
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tag:TaggedValue)
      END
  END
  
  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web)
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonValidateJobDetails  !1
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Value::locJobNumber  !1
  do Comment::locJobNumber
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryErrorMessage  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonFailAccessory  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''individualdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FailAccessory','Fail Accessory Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'\images\cross.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value')

Comment::buttonFailAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPasswordMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_prompt',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryPasswordMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPasswordMessage',p_web.GetValue('NewValue'))
    locAccessoryPasswordMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPasswordMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPasswordMessage',p_web.GetValue('Value'))
    locAccessoryPasswordMessage = p_web.GetValue('Value')
  End

Value::locAccessoryPasswordMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryPasswordMessage') = '')
  ! --- DISPLAY --- locAccessoryPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryPasswordMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value')

Comment::locAccessoryPasswordMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_comment',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::cou:Courier  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Courier')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt')

Validate::cou:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('cou:Courier',p_web.GetValue('NewValue'))
    cou:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = cou:Courier
    do Value::cou:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('cou:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    cou:Courier = p_web.GetValue('Value')
  End
  DO ShowConsignmentNumber
  do Value::cou:Courier
  do SendAlert
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::cou:Courier  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('cou:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cou:Courier'',''individualdespatch_cou:courier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('cou:Courier')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('cou:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('cou:Courier') = 0
    p_web.SetSessionValue('cou:Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  p_web._OpenFile(JOBACC)
  bind(jac:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(WAYBILLJ)
  bind(waj:Record)
  p_web._OpenFile(WAYBILLS)
  bind(way:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(MULDESPJ)
  bind(mulj:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(cou:Courier_OptionView)
  cou:Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(cou:Courier_OptionView)
  Loop
    Next(cou:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('cou:Courier') = 0
      p_web.SetSessionValue('cou:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(cou:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value')

Comment::cou:Courier  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locConsignmentNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Consignment Number')
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt')

Validate::locConsignmentNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConsignmentNumber',p_web.GetValue('NewValue'))
    locConsignmentNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConsignmentNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConsignmentNumber',p_web.GetValue('Value'))
    locConsignmentNumber = p_web.GetValue('Value')
  End
    locConsignmentNumber = Upper(locConsignmentNumber)
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  do Value::locConsignmentNumber
  do SendAlert

Value::locConsignmentNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Show:ConsignmentNumber') <> 1)
  ! --- STRING --- locConsignmentNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locConsignmentNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConsignmentNumber'',''individualdespatch_locconsignmentnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locConsignmentNumber',p_web.GetSessionValueFormat('locConsignmentNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value')

Comment::locConsignmentNumber  Routine
      loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_comment',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSecurityPackID  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Security Pack ID')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt')

Validate::locSecurityPackID  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSecurityPackID',p_web.GetValue('NewValue'))
    locSecurityPackID = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSecurityPackID
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSecurityPackID',p_web.GetValue('Value'))
    locSecurityPackID = p_web.GetValue('Value')
  End
  do Value::locSecurityPackID
  do SendAlert

Value::locSecurityPackID  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- STRING --- locSecurityPackID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSecurityPackID')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackID'',''individualdespatch_locsecuritypackid_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackID',p_web.GetSessionValueFormat('locSecurityPackID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value')

Comment::locSecurityPackID  Routine
      loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmDespatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonConfirmDespatch
  do SendAlert

Value::buttonConfirmDespatch  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmDespatch'',''individualdespatch_buttonconfirmdespatch_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmDespatch','Confirm Despatch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchConfirmation')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value')

Comment::buttonConfirmDespatch  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('IndividualDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('IndividualDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('IndividualDespatch_buttonValidateJobDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateJobDetails
      else
        do Value::buttonValidateJobDetails
      end
  of lower('IndividualDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('IndividualDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  of lower('IndividualDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('IndividualDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('IndividualDespatch_cou:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cou:Courier
      else
        do Value::cou:Courier
      end
  of lower('IndividualDespatch_locConsignmentNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConsignmentNumber
      else
        do Value::locConsignmentNumber
      end
  of lower('IndividualDespatch_locSecurityPackID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackID
      else
        do Value::locSecurityPackID
      end
  of lower('IndividualDespatch_buttonConfirmDespatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmDespatch
      else
        do Value::buttonConfirmDespatch
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)

PreCopy  Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 4
    loc:InvalidTab += 1
      If not (p_web.GSV('AccessoryPasswordRequired') <> 1)
        If locAccessoryPassword = ''
          loc:Invalid = 'locAccessoryPassword'
          loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locAccessoryPassword = Upper(locAccessoryPassword)
          p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 5
    loc:InvalidTab += 1
      If not (p_web.GSV('Show:ConsignmentNumber') <> 1)
          locConsignmentNumber = Upper(locConsignmentNumber)
          p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPasswordMessage')
  p_web.StoreValue('cou:Courier')
  p_web.StoreValue('locConsignmentNumber')
  p_web.StoreValue('locSecurityPackID')
  p_web.StoreValue('')
JobPaid              PROCEDURE  (fJobNumber)               ! Declare Procedure
tmp:VAT              REAL                                  !
tmp:Total            REAL                                  !
tmp:AmountPaid       REAL                                  !
tmp:Paid             BYTE                                  !
tmp:LabourVatRate    REAL                                  !
tmp:PartsVatRate     REAL                                  !
tmp:Balance          REAL                                  !
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBS_ALIAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openfiles
    do savefiles
    
    
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = fJobNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
    End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
    
    tmp:Paid = 0
    tmp:AmountPaid = 0
    Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
    jpt:Ref_Number = job_ali:Ref_Number
    Set(jpt:All_Date_Key,jpt:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT.Next()
            Break
        End ! If Access:JOBPAYMT.Next()
        If jpt:Ref_Number <> job_ali:Ref_Number
            Break
        End ! If jpt:RefNumber <> f:RefNumber
        tmp:AmountPaid += jpt:Amount
    End ! Loop
    
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job_ali:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
    
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
    
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job_ali:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
    
    If job_ali:Invoice_Number > 0
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! If job_ali:Invoice_Number > 0
    
    If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:InvRRCCLabourCost *(inv:Vat_Rate_Labour / 100) + |
                jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts / 100) + |
                job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)
    
            tmp:Total = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job_ali:Invoice_Courier_Cost + tmp:VAT
    
        Else ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:RRCCLabourCost *(tmp:LabourVatRate / 100) + |
                jobe:RRCCPartsCost * (tmp:PartsVatRate / 100) + |
                job_ali:Courier_Cost * (tmp:LabourVatRate / 100)
    
            tmp:Total = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job_ali:Courier_Cost + tmp:VAT
    
        End ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    Else ! If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100) + |
                job_ali:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) + |
                job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)
    
            tmp:Total = job_ali:Invoice_Labour_Cost + job_ali:Invoice_Parts_Cost + job_ali:Invoice_Courier_Cost + tmp:VAT
    
        Else ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Labour_Cost * (tmp:LabourVatRate / 100) + |
                job_ali:Parts_Cost * (tmp:PartsVatRate / 100) + |
                job_ali:Courier_Cost * (tmp:LabourVatRate / 100)
    
            tmp:Total = job_ali:Labour_Cost + job_ali:Parts_Cost + job_ali:Courier_Cost + tmp:VAT
    
        End ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    End ! If glo:WebJob
    
    If tmp:Balance < 0.01
        tmp:Paid = 1
    End ! If tmp:Balance < 0
    
    do restorefiles
    do closefiles
    
    return tmp:Paid
SaveFiles  ROUTINE
  JOBPAYMT::State = Access:JOBPAYMT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBSE::State = Access:JOBSE.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  VATCODE::State = Access:VATCODE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
  INVOICE::State = Access:INVOICE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS_ALIAS::State = Access:JOBS_ALIAS.SaveFile()         ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBPAYMT::State <> 0
    Access:JOBPAYMT.RestoreFile(JOBPAYMT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBSE::State <> 0
    Access:JOBSE.RestoreFile(JOBSE::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF VATCODE::State <> 0
    Access:VATCODE.RestoreFile(VATCODE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF INVOICE::State <> 0
    Access:INVOICE.RestoreFile(INVOICE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS_ALIAS::State <> 0
    Access:JOBS_ALIAS.RestoreFile(JOBS_ALIAS::State)       ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBPAYMT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.Open                                   ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.UseFile                                ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBPAYMT.Close
     Access:JOBSE.Close
     Access:VATCODE.Close
     Access:INVOICE.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:JOBS_ALIAS.Close
     FilesOpened = False
  END
AccessoryCheck       PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    !Validate Accessories
    ! Variables Passed:
    ! AccessoryCheck:Type
    ! AccessoryCheck:RefNumber
    
    ! Returned Value
! AccessoryCheck:Return

    do OpenFiles
    Case p_web.GSV('AccessoryCheck:Type')
    of 'LOA'
        error# = 0
        Access:LOANACC.Clearkey(lac:Ref_Number_Key)
        lac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        loop
            if (Access:LOANACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (lac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
    
    
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = lac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop
    
        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:LOANACC.Clearkey(lac:ref_number_Key)
                lac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                lac:accessory    = tag:taggedValue
                if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    Else
        error# = 0
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        loop
            if (Access:JOBACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (jac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
    
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (NOT jac:Attached)
                    Cycle
                End
            End
    
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = jac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                else
                    if (p_web.GSV('BookingSite') <> 'RRC' AND NOT jac:Attached)
                        error# = 1
                        break
                    End
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop
    
        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:JOBACC.Clearkey(jac:ref_number_Key)
                jac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                jac:accessory    = tag:taggedValue
                if (Access:JOBACC.TryFetch(jac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    End
    
    do CloseFiles

    p_web.SSV('AccessoryCheck:Return',error#)
    ! 1 = Missing
    ! 2 = Mismatch
    ! Else = All Fine
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     Access:JOBACC.Close
     Access:LOANACC.Close
     FilesOpened = False
  END
DespatchConfirmation PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyName       STRING(30)                            !
locCourierCost       REAL                                  !
locLabourCost        REAL                                  !
locPartsCost         REAL                                  !
locSubTotal          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locInvoiceNumber     REAL                                  !
locInvoiceDate       DATE                                  !
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locCourier           STRING(30)                            !
FilesOpened     Long
EXCHHIST::State  USHORT
LOANHIST::State  USHORT
EXCHANGE::State  USHORT
LOAN::State  USHORT
JOBSCONS::State  USHORT
COURIER::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
job:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DespatchConfirmation')
  loc:formname = 'DespatchConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DespatchConfirmation','')
    p_web._DivHeader('DespatchConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DespatchConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locCompanyName')
    p_web.DeleteSessionValue('locCourierCost')
    p_web.DeleteSessionValue('locLabourCost')
    p_web.DeleteSessionValue('locPartsCost')
    p_web.DeleteSessionValue('locSubTotal')
    p_web.DeleteSessionValue('locVAT')
    p_web.DeleteSessionValue('locTotal')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locInvoiceDate')
    p_web.DeleteSessionValue('locLabourVatRate')
    p_web.DeleteSessionValue('locPartsVatRate')
    p_web.DeleteSessionValue('locCourier')

    ! Other Variables
    p_web.DeleteSessionValue('DespatchConfirmation:FirstTime')

DespatchProcess     Routine
DATA
locWaybillNumber    STRING(30)
CODE
    p_web.SSV('Hide:PrintDespatchNote',1)
    p_web.SSV('Hide:PrintWaybill',1)
    
    IF (p_web.GSV('cou:PrintWaybill') = 1)
            
        p_web.SSV('Hide:PrintWaybill',0)
            
        locWaybillNumber = NextWaybillNumber()
        IF (locWaybillNumber = 0)
            EXIT
        END
        
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locWaybillNumber
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
            EXIT
        END
        
        IF (p_web.GSV('BookingSite') = 'RRC')

            way:AccountNumber = p_web.GSV('BookingAccount')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                way:WaybillID = 2
                p_web.SSV('wob:JobWaybillNumber',locWaybillNumber)
            OF 'EXC'
                way:WaybillID = 3
                p_web.SSV('wob:ExcWaybillNumber',way:WayBillNumber)
            OF 'LOA'
                way:WaybillID = 4
                p_web.SSV('wob:LoaWaybillNumber',way:WayBillNumber)
            END
            
            IF (p_web.GSV('job:Who_Booked') = 'WEB')
                ! PUP Job
                way:WayBillType = 9
                way:WaybillID = 21
            ELSE
                way:WayBillType = 2
            END
            
            way:FromAccount = p_web.GSV('BookingAccount')
            way:ToAccount = p_web.GSV('job:Account_Number')
            
        ELSE
            way:WayBillType = 1 ! Created From RRC
            way:AccountNumber = p_web.GSV('wob:HeadAccountNumber')
            way:FromAccount = p_web.GSV('ARC:AccountNumber')
            IF (p_web.GSV('jobe:WebJob') = 1)
                way:ToAccount = p_web.GSV('wob:HeadAccountNumber')
                Case p_web.GSV('DespatchType')
                of 'JOB'
                    way:WaybillID = 5 ! ARC to RRC (JOB)
                of 'EXC'
                    way:WaybillID = 6 ! ARC to RRC (EXC)
                END
                
            ELSE
                way:ToAccount = job:Account_Number
                Case p_web.GSV('DespatchType')
                OF 'JOB'
                    way:WaybillID = 7 ! ARC To Customer (JOB)
                of 'EXC'
                    way:WaybillID = 8 ! ARC To Customer (EXC)
                of 'LOA'
                    way:WaybillID = 9 ! ARC To Customer (LOA)
                END
                
            END
            
        END
        
            
        IF (Access:WAYBILLS.TryUpdate())
            EXIT
        END
            
        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
            waj:WayBillNumber = way:WayBillNumber
            waj:JobNumber = p_web.GSV('job:Ref_Number')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                waj:IMEINumber = p_web.GSV('job:ESN')
            OF 'EXC'
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Available_Key) = Level:Benign)
                    waj:IMEINumber = xch:ESN
                END
                    
            OF 'LOA'
                Access:LOAN.ClearKey(loa:Ref_Number_Key)
                loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                    waj:IMEINumber = loa:ESN
                END
                    
            END
            waj:OrderNumber = p_web.GSV('job:Order_Number')
            waj:SecurityPackNumber = p_web.GSV('locSecurityPackID')
            waj:JobType = p_web.GSV('DespatchType')
            IF (Access:WAYBILLJ.TryUpdate() = Level:Benign)
            ELSE
                Access:WAYBILLJ.CancelAutoInc()
            END
        END
            
    ELSE ! IF (p_web.GSV('cou:PrintWaybill') = 1)
        if (p_web.GSV('cou:AutoConsignmentNo') = 1)
            Access:COURIER.Clearkey(cou:Courier_Key)
            cou:Courier = p_web.GSV('cou:Courier')
            if (Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign)
                cou:LastConsignmentNo += 1
                Access:COURIER.TryUpdate()
                locWaybillNumber = cou:LastConsignmentNo
            end
        ELSE
            locWaybillNumber = p_web.GSV('locConsignmentNumber')
        end
        p_web.SSV('Hide:PrintDespatchNote',0)
            
    END ! IF (p_web.GSV('cou:PrintWaybill') = 1)
            
    p_web.SSV('locWaybillNumber',locWaybillNumber)
    
    CASE p_web.GSV('DespatchType')
    OF 'JOB'
        !Despatch:Job(locWayBillNumber)
        Despatch:Job(p_web)
        
    OF 'EXC'
        Despatch:Exc(p_web)                
                
    OF 'LOA'
        Despatch:Loa(p_web)
        
    END

    ! Save Files
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(WEBJOB)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                    
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        IF (Access:JOBSE.TryUpdate() = Level:Benign)
                            
                        END
                    END
                END
            END
        END
    END
    
    
    
    
ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString    CSTRING(30)
CODE
    
    paymentFail# = 0
        
    IF (p_web.GSV('BookingSite') = 'RRC')
        locPaymentString = locPaymentStringRRC
    ELSE
        locPaymentString = locPaymentStringARC
    END
    
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
        IF (p_web.GSV('job:Loan_Unit_Number') = 0)
            IF (p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Chargeable_Job') <> 'YES')
                paymentFail# = 1
            END
        END
    ELSE
        paymentFail# = 1
    END
    IF (paymentFail# = 1)
        p_web.SSV('Hide:PaymentDetails',1)
    ELSE
        p_web.SSV('Hide:PaymentDetails',0)
    END
    
OpenFiles  ROUTINE
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBSCONS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(VATCODE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('DespatchConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO deletesessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('locCourierCost')
    p_web.SetPicture('locCourierCost','@n14.2')
  End
  p_web.SetSessionPicture('locCourierCost','@n14.2')
  If p_web.IfExistsValue('locLabourCost')
    p_web.SetPicture('locLabourCost','@n14.2')
  End
  p_web.SetSessionPicture('locLabourCost','@n14.2')
  If p_web.IfExistsValue('locPartsCost')
    p_web.SetPicture('locPartsCost','@n14.2')
  End
  p_web.SetSessionPicture('locPartsCost','@n14.2')
  If p_web.IfExistsValue('locSubTotal')
    p_web.SetPicture('locSubTotal','@n14.2')
  End
  p_web.SetSessionPicture('locSubTotal','@n14.2')
  If p_web.IfExistsValue('locInvoiceDate')
    p_web.SetPicture('locInvoiceDate','@d06')
  End
  p_web.SetSessionPicture('locInvoiceDate','@d06')
  If p_web.IfExistsValue('locVAT')
    p_web.SetPicture('locVAT','@n14.2')
  End
  p_web.SetSessionPicture('locVAT','@n14.2')
  If p_web.IfExistsValue('locTotal')
    p_web.SetPicture('locTotal','@n14.2')
  End
  p_web.SetSessionPicture('locTotal','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('locCompanyName',locCompanyName)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  p_web.SetSessionValue('locCourierCost',locCourierCost)
  p_web.SetSessionValue('locLabourCost',locLabourCost)
  p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  p_web.SetSessionValue('locPartsCost',locPartsCost)
  p_web.SetSessionValue('locSubTotal',locSubTotal)
  p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  p_web.SetSessionValue('locVAT',locVAT)
  p_web.SetSessionValue('locTotal',locTotal)
  p_web.SetSessionValue('job:Courier',job:Courier)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  End
  if p_web.IfExistsValue('locCourierCost')
    locCourierCost = p_web.dformat(clip(p_web.GetValue('locCourierCost')),'@n14.2')
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  End
  if p_web.IfExistsValue('locLabourCost')
    locLabourCost = p_web.dformat(clip(p_web.GetValue('locLabourCost')),'@n14.2')
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  End
  if p_web.IfExistsValue('locPartsCost')
    locPartsCost = p_web.dformat(clip(p_web.GetValue('locPartsCost')),'@n14.2')
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  End
  if p_web.IfExistsValue('locSubTotal')
    locSubTotal = p_web.dformat(clip(p_web.GetValue('locSubTotal')),'@n14.2')
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  End
  if p_web.IfExistsValue('locInvoiceDate')
    locInvoiceDate = p_web.dformat(clip(p_web.GetValue('locInvoiceDate')),'@d06')
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  End
  if p_web.IfExistsValue('locVAT')
    locVAT = p_web.dformat(clip(p_web.GetValue('locVAT')),'@n14.2')
    p_web.SetSessionValue('locVAT',locVAT)
  End
  if p_web.IfExistsValue('locTotal')
    locTotal = p_web.dformat(clip(p_web.GetValue('locTotal')),'@n14.2')
    p_web.SetSessionValue('locTotal',locTotal)
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('DespatchConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.site.SaveButton.TextValue = 'OK'
  
  IF (p_web.GSV('DespatchConfirmation:FirstTime') = 0)
      p_web.SSV('DespatchConfirmation:FirstTime',1)
      ! Call this code once
      IF (p_web.GSV('Show:DespatchInvoiceDetails') = 1)
         
          IF (p_web.GSV('CreateDespatchInvoice') = 1 )
              CreateTheInvoice(p_web)
          END
      END
      DO DespatchProcess
  
      
  END
  ! Hideous Calculations to work out invoice/costs
  p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DespatchConfirmation')
  p_web.SSV('URL:CreateInvoiceTarget','_self')
  p_web.SSV('URL:CreateInvoiceText','Create Invoice')
  
  isJobInvoiced(p_web)
  IF (p_web.GSV('IsJobInvoiced') = 1)
      p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
      p_web.SSV('URL:CreateInvoiceTarget','_blank')
      p_web.SSV('URL:CreateInvoiceText','Print Invoice')
              
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCLabourCost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCPartsCost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('BookingAccount')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
                  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:RRCInvoiceDate)
      else 
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Invoice_Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
  
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('Default:AccountNumber')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
                  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:ARCInvoiceDate)        
      end
  
  ELSE
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
  
                  
      else 
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Parts_Cost'))
  
      end
      IF (InvoiceSubAccounts(p_web.GSV('job:Account_Number')))
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
                  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
      ELSE
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
                  
                  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
                  
      END
              
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCLabourCost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCPartsCost') * locPartsVatRate/100))                
      ELSE
                  
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Labour_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Parts_Cost') * locPartsVatRate/100))
      END
              
              
  END
  p_web.SSV('locSubTotal',p_web.GSV('locCourierCost') + | 
      p_web.GSV('locLabourCost') + | 
      p_web.GSV('locPartsCost'))        
  p_web.SSV('locTotal',p_web.GSV('locSubTotal') + | 
      p_web.GSV('locVAT'))        
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
  END    
  p_web.SSV('locCompanyName',sub:Company_Name)
  
  
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PAYMENT TYPES'))
      p_web.SSV('Hide:PaymentDetails',1)
  ELSE
      p_web.SSV('Hide:PaymentDetails',0)
  END
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locCourierCost = p_web.RestoreValue('locCourierCost')
 locLabourCost = p_web.RestoreValue('locLabourCost')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locPartsCost = p_web.RestoreValue('locPartsCost')
 locSubTotal = p_web.RestoreValue('locSubTotal')
 locInvoiceDate = p_web.RestoreValue('locInvoiceDate')
 locVAT = p_web.RestoreValue('locVAT')
 locTotal = p_web.RestoreValue('locTotal')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndividualDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DespatchConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DespatchConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('DespatchConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndividualDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="DespatchConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="DespatchConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="DespatchConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Despatch Confirmation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Despatch Confirmation',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_DespatchConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_DespatchConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_DespatchConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Show:DespatchInvoiceDetails') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Invoice Details') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_DespatchConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_DespatchConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Show:DespatchInvoiceDetails') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Courier')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Show:DespatchInvoiceDetails') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_DespatchConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Show:DespatchInvoiceDetails') = 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Invoice Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DespatchConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCompanyName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::aLine
      do Comment::aLine
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textBillingDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textBillingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textBillingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textInvoiceDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textInvoiceDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textInvoiceDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCourierCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCourierCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCourierCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLabourCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLabourCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLabourCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPartsCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPartsCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPartsCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSubTotal
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSubTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSubTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locVAT
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locVAT
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locVAT
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTotal
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonCreateInvoice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateInvoice
      do Comment::buttonCreateInvoice
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPaymentDetails
      do Comment::buttonPaymentDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DespatchConfirmation_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:Account_Number  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End

Value::job:Account_Number  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Account_Number  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCompanyName  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCompanyName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('NewValue'))
    locCompanyName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('Value'))
    locCompanyName = p_web.GetValue('Value')
  End

Value::locCompanyName  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCompanyName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCompanyName  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Repair_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type',p_web.GetValue('NewValue'))
    job:Repair_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type
    do Value::job:Repair_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type = p_web.GetValue('Value')
  End

Value::job:Repair_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Repair_Type  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::aLine  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('aLine',p_web.GetValue('NewValue'))
    do Value::aLine
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::aLine  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::aLine  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textBillingDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Billing Details:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textBillingDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBillingDetails',p_web.GetValue('NewValue'))
    do Value::textBillingDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBillingDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textBillingDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textInvoiceDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice Details:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textInvoiceDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textInvoiceDetails',p_web.GetValue('NewValue'))
    do Value::textInvoiceDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textInvoiceDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textInvoiceDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCourierCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCourierCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCourierCost',p_web.GetValue('NewValue'))
    locCourierCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCourierCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCourierCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locCourierCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locCourierCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locCourierCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locCourierCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCourierCost',p_web.GetSessionValue('locCourierCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCourierCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locLabourCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Labour Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locLabourCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLabourCost',p_web.GetValue('NewValue'))
    locLabourCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLabourCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLabourCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locLabourCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locLabourCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locLabourCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locLabourCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locLabourCost',p_web.GetSessionValue('locLabourCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locLabourCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceNumber  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('NewValue'))
    locInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('Value'))
    locInvoiceNumber = p_web.GetValue('Value')
  End

Value::locInvoiceNumber  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_value',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locInvoiceNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_comment',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPartsCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Parts Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPartsCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPartsCost',p_web.GetValue('NewValue'))
    locPartsCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPartsCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartsCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locPartsCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locPartsCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPartsCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locPartsCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartsCost',p_web.GetSessionValue('locPartsCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locPartsCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSubTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Sub Total')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSubTotal  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSubTotal',p_web.GetValue('NewValue'))
    locSubTotal = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSubTotal
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSubTotal',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locSubTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locSubTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSubTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locSubTotal')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSubTotal',p_web.GetSessionValue('locSubTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locSubTotal  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceDate  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Date')
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceDate',p_web.GetValue('NewValue'))
    locInvoiceDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locInvoiceDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End

Value::locInvoiceDate  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_value',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locInvoiceDate'),'@d06')) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locInvoiceDate  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_comment',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locVAT  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('VAT')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locVAT  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locVAT',p_web.GetValue('NewValue'))
    locVAT = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locVAT
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locVAT',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locVAT = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locVAT  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locVAT
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locVAT')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locVAT',p_web.GetSessionValue('locVAT'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locVAT  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Total')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTotal  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTotal',p_web.GetValue('NewValue'))
    locTotal = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTotal
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTotal',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locTotal')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locTotal',p_web.GetSessionValue('locTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locTotal  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Courier  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Courier',p_web.GetValue('NewValue'))
    job:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Courier
    do Value::job:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Courier = p_web.GetValue('Value')
  End
    job:Courier = Upper(job:Courier)
    p_web.SetSessionValue('job:Courier',job:Courier)
  do Value::job:Courier
  do SendAlert

Value::job:Courier  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''despatchconfirmation_job:courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBSCONS)
  bind(joc:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(VATCODE)
  bind(vat:Record)
  p_web._OpenFile(INVOICE)
  bind(inv:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Courier_OptionView)
  job:Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Courier_OptionView)
  Loop
    Next(job:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Courier') = 0
      p_web.SetSessionValue('job:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value')

Comment::job:Courier  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonCreateInvoice  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonCreateInvoice
  do SendAlert

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateInvoice'',''despatchconfirmation_buttoncreateinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),'DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value')

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPaymentDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPaymentDetails',p_web.GetValue('NewValue'))
    do Value::buttonPaymentDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPaymentDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_value',Choose(p_web.GSV('Hide:PaymentDetails') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PaymentDetails','PaymentDetails','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=DespatchConfirmation')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonPaymentDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_comment',Choose(p_web.GSV('Hide:PaymentDetails') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintWaybill
  do SendAlert

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''despatchconfirmation_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Print Waybill','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value')

Comment::buttonPrintWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_comment',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintDespatchNote
  do SendAlert

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''despatchconfirmation_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintDespatchNote','Print Despatch Note','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchNote?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')

Comment::buttonPrintDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('DespatchConfirmation_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      else
        do Value::job:Courier
      end
  of lower('DespatchConfirmation_buttonCreateInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateInvoice
      else
        do Value::buttonCreateInvoice
      end
  of lower('DespatchConfirmation_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  of lower('DespatchConfirmation_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)

PreCopy  Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO deletesessionvalues
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:InvalidTab += 1
          job:Courier = Upper(job:Courier)
          p_web.SetSessionValue('job:Courier',job:Courier)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Repair_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCourierCost')
  p_web.StoreValue('locLabourCost')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locPartsCost')
  p_web.StoreValue('locSubTotal')
  p_web.StoreValue('locInvoiceDate')
  p_web.StoreValue('locVAT')
  p_web.StoreValue('locTotal')
  p_web.StoreValue('job:Courier')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
