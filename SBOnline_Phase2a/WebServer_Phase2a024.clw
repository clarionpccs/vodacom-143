

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A024.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A018.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A023.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A025.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A026.INC'),ONCE        !Req'd for module callout resolution
                     END


ReleasedForDespatch  PROCEDURE  (f:JobNumber)              ! Declare Procedure
AUDIT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles
    Return# = 0
    Access:AUDIT.Clearkey(aud:Action_Key)
    aud:Ref_Number = f:JobNumber
    aud:Action = 'OUT OF REGION: RELEASED FOR DESPATCH'
    Set(aud:Action_Key,aud:Action_Key)
    Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> f:JobNumber
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
            Break
        End ! If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
        Return# = 1
        Break
    End ! Loop
    do restoreFiles
    do closeFiles
    
    Return Return#
SaveFiles  ROUTINE
  AUDIT::State = Access:AUDIT.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF AUDIT::State <> 0
    Access:AUDIT.RestoreFile(AUDIT::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT.Close
     FilesOpened = False
  END
DefaultLabourCost    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
SUBCHRGE::State  USHORT
TRACHRGE::State  USHORT
STDCHRGE::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do OpenFiles
    Do saveFiles
    p_web.SSV('DefaultLabourCost',0)
    If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
        access:subchrge.clearkey(suc:model_repair_type_key)
        suc:account_number = p_web.GSV('job:account_number')
        suc:model_number   = p_web.GSV('job:model_number')
        suc:charge_type    = p_web.GSV('job:charge_type')
        suc:unit_type      = p_web.GSV('job:unit_type')
        suc:repair_type    = p_web.GSV('job:repair_type')
        if access:subchrge.fetch(suc:model_repair_type_key)
            access:trachrge.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = p_web.GSV('job:model_number')
            trc:charge_type    = p_web.GSV('job:charge_type')
            trc:unit_type      = p_web.GSV('job:unit_type')
            trc:repair_type    = p_web.GSV('job:repair_type')
            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                p_web.SSV('DefaultLabourCost',trc:RRCRate)
            End!if access:trachrge.fetch(trc:account_charge_key)
        Else
            p_web.SSV('DefaultLabourCost',suc:RRCRate)
        End!if access:subchrge.fetch(suc:model_repair_type_key)
    Else !If InvoiceSubAccounts(job:Account_Number)
        access:trachrge.clearkey(trc:account_charge_key)
        trc:account_number = sub:main_account_number
        trc:model_number   = p_web.GSV('job:model_number')
        trc:charge_type    = p_web.GSV('job:charge_type')
        trc:unit_type      = p_web.GSV('job:unit_type')
        trc:repair_type    = p_web.GSV('job:repair_type')
        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
            p_web.SSV('DefaultLabourCost',trc:RRCRate)
        End!if access:trachrge.fetch(trc:account_charge_key)
    
    End !If InvoiceSubAccounts(job:Account_Number)
    
    If p_web.GSV('DefaultLabourCost') = 0 then
        access:stdchrge.clearkey(sta:model_number_charge_key)
        sta:model_number = p_web.GSV('job:model_number')
        sta:charge_type  = p_web.GSV('job:charge_type')
        sta:unit_type    = p_web.GSV('job:unit_type')
        sta:repair_type  = p_web.GSV('job:repair_type')
        if access:stdchrge.fetch(sta:model_number_charge_key)
        Else !if access:stdchrge.fetch(sta:model_number_charge_key)
            p_web.SSV('DefaultLabourCost',sta:RRCRate)
        end !if access:stdchrge.fetch(sta:model_number_charge_key)
    End !LocalDefaultLabour = 0 then
    do RestoreFiles
    do CloseFiles
SaveFiles  ROUTINE
  SUBCHRGE::State = Access:SUBCHRGE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRACHRGE::State = Access:TRACHRGE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  STDCHRGE::State = Access:STDCHRGE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF SUBCHRGE::State <> 0
    Access:SUBCHRGE.RestoreFile(SUBCHRGE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRACHRGE::State <> 0
    Access:TRACHRGE.RestoreFile(TRACHRGE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STDCHRGE::State <> 0
    Access:STDCHRGE.RestoreFile(STDCHRGE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBCHRGE.Close
     Access:TRACHRGE.Close
     Access:STDCHRGE.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
Waybill PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locWaybillNumber     LONG                                  !
FromAddressGroup     GROUP,PRE(from)                       !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(39)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(60)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
ToAddressGroup       GROUP,PRE(to)                         !
AccountNumber        STRING(30)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
locDateDespatched    DATE                                  !
locTimeDespatched    TIME                                  !
locCourier           STRING(30)                            !
locConsignmentNumber STRING(30)                            !
locBarCode           STRING(60)                            !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locOrderNumber       STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locAccessories       STRING(255)                           !
locExchanged         STRING(1)                             !
Process:View         VIEW(WAYBILLJ)
                       PROJECT(waj:JobNumber)
                       PROJECT(waj:WayBillNumber)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('JOBS Report'),AT(250,3875,7750,6594),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,COLOR:Black, |
  FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(250,250,7750,3250),USE(?Header),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT)
                         STRING('Courier:'),AT(4896,781),USE(?String23),FONT(,8),TRN
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),FONT(,8),TRN
                         STRING(@d6),AT(6042,365),USE(locDateDespatched),FONT(,8,,FONT:bold),TRN
                         STRING(@t1b),AT(6042,573),USE(locTimeDespatched),FONT(,8,,FONT:bold),TRN
                         STRING('From Sender:'),AT(83,83),USE(?String16),FONT(,8),TRN
                         STRING('WAYBILL REJECTION'),AT(3750,31,3781),USE(?WaybillRejection),FONT(,16,,FONT:bold),RIGHT, |
  HIDE,TRN
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),FONT(,8),TRN
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),FONT(,8),TRN
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),FONT(,8),TRN
                         STRING(@s60),AT(938,1198),USE(from:ContactName),FONT(,8,,FONT:bold),TRN
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Email:'),AT(104,1354),USE(?String16:4),FONT(,8),TRN
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),FONT(,12,,FONT:bold),CENTER, |
  TRN
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),FONT(,8),TRN
                         STRING(@s30),AT(83,250,4687,365),USE(from:CompanyName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(3490,1979,3906,260),USE(locBarCode),FONT('C39 High 12pt LJ3',12,COLOR:Black, |
  ,CHARSET:ANSI),CENTER,COLOR(COLOR:White)
                         STRING(@s30),AT(83,542),USE(from:AddressLine1),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(83,708),USE(from:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),FONT(,8,,FONT:bold),TRN
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),FONT(,8),TRN
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),FONT(,8),TRN
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),FONT(,8),TRN
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),FONT(,8,,FONT:bold),TRN
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),FONT(,8),TRN
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),FONT(,8),TRN
                         STRING(@s30),AT(83,875),USE(from:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(3490,2292,3906,260),USE(locConsignmentNumber),FONT('Arial',12,,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING(@s255),AT(4323,2969,3385,208),USE(glo:ErrorText),FONT(,8,,FONT:bold),RIGHT,TRN
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@s30),AT(6042,781),USE(locCourier),FONT(,,,FONT:bold)
                         STRING(@N3),AT(6042,990),USE(ReportPageNumber),FONT(,,,FONT:bold)
                       END
Detail                 DETAIL,AT(0,0,7750,229),USE(?Detail)
                         STRING(@s30),AT(104,0,1094),USE(locJobNumber)
                         STRING(@s30),AT(1260,0,1302),USE(locIMEINumber)
                         STRING(@s30),AT(2625,-10),USE(locOrderNumber),FONT(,7)
                         STRING(@s30),AT(4354,-10,958),USE(locSecurityPackNumber),FONT(,7)
                         TEXT,AT(5375,-10,2021,146),USE(locAccessories),RESIZE
                         STRING(@s1),AT(7458,-10),USE(locExchanged)
                       END
                       FOOTER,AT(250,10531,7750,906),USE(?Footer)
                         TEXT,AT(125,0,7500,833),USE(stt:Text),TRN
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT)
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),FONT(,7),TRN
                         STRING('I.M.E.I. No'),AT(1313,3333),USE(?IMEINo),FONT(,7),TRN
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),FONT(,7),TRN
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),FONT(,7),TRN
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),FONT(,7),TRN
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Waybill Variables
  ! p_web.SSV('locWaybillNumber',)
  ! p_web.SSV('Waybill:Courier,)
  ! p_web.SSV('Waybill:FromType,)
  ! p_web.SSV('Waybill:FromAccount,)
  ! p_web.SSV('Waybill:ToType,)
  ! p_web.SSV('Waybill:ToAccount,)
  GlobalErrors.SetProcedureName('Waybill')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBACC.SetOpenRelated()
  Relate:JOBACC.Open                                       ! File JOBACC used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WAYBILLJ.SetOpenRelated()
  Relate:WAYBILLJ.Open                                     ! File WAYBILLJ used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WAYBILLS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locWaybillNumber = p_web.GSV('locWaybillNumber')
  Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
  way:WayBillNumber = locWaybillNumber
  IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
  
  END
  SET(DEFAULTS,0)
  Access:DEFAULTS.Next()
  
  locCourier = p_web.GSV('Waybill:Courier')
  
  ! Set consignment number
  IF (way:FromAccount = p_web.GSV('ARC:AccountNumber'))
      IF (GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1)
          locConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(way:WaybillNumber,@n07)
      ELSE
          locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
      END
  ELSE
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          If clip(tra:RRCWaybillPrefix) <> ''
              locConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(way:WaybillNumber,@n07)
          ELSE
              locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
          END
      END
  END
  
  
  
  locDateDespatched = way:TheDate
  locTimeDespatched = way:TheTime
  
  ! Set addresses
  CASE p_web.GSV('Waybill:FromType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName        = tra:Company_Name
          from:AddressLine1       = tra:Address_Line1
          from:AddressLine2       = tra:Address_Line2
          from:AddressLine3       = tra:Address_Line3
          from:TelephoneNumber    = tra:Telephone_Number
          from:ContactName        = tra:Contact_Name
          from:EmailAddress       = tra:EmailAddress        
      END
      
  OF 'SUB'
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          from:CompanyName        = sub:Company_Name
          from:AddressLine1       = sub:Address_Line1
          from:AddressLine2       = sub:Address_Line2
          from:AddressLine3       = sub:Address_Line3
          from:TelephoneNumber    = sub:Telephone_Number
          from:ContactName        = sub:Contact_Name
          from:EmailAddress       = sub:EmailAddress
      END
      
  OF 'DEF'
      from:CompanyName        = def:User_Name
      from:AddressLine1       = def:Address_Line1
      from:AddressLine2       = def:Address_Line2
      from:AddressLine3       = def:Address_Line3
      from:TelephoneNumber    = def:Telephone_Number
      from:ContactName        = ''
      from:EmailAddress       = def:EmailAddress
  END
  
  CASE p_web.GSV('Waybill:ToType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = tra:Account_Number
          to:CompanyName        = tra:Company_Name
          to:AddressLine1       = tra:Address_Line1
          to:AddressLine2       = tra:Address_Line2
          to:AddressLine3       = tra:Address_Line3
          to:TelephoneNumber    = tra:Telephone_Number
          to:ContactName        = tra:Contact_Name
          to:EmailAddress       = tra:EmailAddress
      END
      
  OF 'SUB'
      Access:SUBTRACC.clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = sub:Account_Number
          to:CompanyName        = sub:Company_Name
          to:AddressLine1       = sub:Address_Line1
          to:AddressLine2       = sub:Address_Line2
          to:AddressLine3       = sub:Address_Line3
          to:TelephoneNumber    = sub:Telephone_Number
          to:ContactName        = sub:Contact_Name
          to:EmailAddress       = sub:EmailAddress
      END
      
  END
  
  
  IF (way:WaybillID <> 0)
      IF (way:WaybillID = 13)
          ! This is a rejection
          SETTARGET(Report)
          ?WaybillRejection{PROP:Hide} = FALSE
          SETTARGET()
      END
      
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName    = tra:Company_Name
          from:AddressLine1   = tra:Address_Line1
          from:AddressLine2   = tra:Address_Line2
          from:AddressLine3   = tra:Address_Line3
          from:TelephoneNumber= tra:Telephone_Number
          from:ContactName    = tra:Contact_Name
          from:EmailAddress   = tra:EmailAddress
      END
      
      IF (way:WaybillID = 20) ! PUP to RRC. Get address from job
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:FromAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END
          
          IF (sub:VCPWaybillPrefix <> '')
              locConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  IF (tra:VCPWaybillPrefix <> '')
                      locConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
                  END
                  
              END
          END
      END
      
      CASE way:WaybillID
      OF 1 OROF 5 OROF 6 OROF 11 OROF 12 OROF 13 OROF 20
          IF (way:ToAccount = 'CUSTOMER') ! PUP to Customer
              to:ContactName = ''
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = way:ToAccount
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:benign)
                  IF (sub:Use_Delivery_Address = 'YES')
                      to:ContactName = sub:Contact_Name
                  END
              END
              to:AccountNumber   = job:Account_Number
              to:CompanyName     = job:Company_Name
              to:AddressLine1    = job:Address_Line1
              to:AddressLine2    = job:Address_Line2
              to:AddressLine3    = job:Address_Line3
              to:TelephoneNumber = job:Telephone_Number
  
              to:EmailAddress    = jobe:EndUserEmailAddress
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = way:ToAccount
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  to:AccountNumber  = tra:Account_Number
                  to:CompanyName    = tra:Company_Name
                  to:AddressLine1   = tra:Address_Line1
                  to:AddressLine2   = tra:Address_Line2
                  to:AddressLine3   = tra:Address_Line3
                  to:TelephoneNumber= tra:Telephone_Number
                  to:ContactName    = tra:Contact_Name
                  to:EmailAddress   = tra:EmailAddress
              END
              
          END
      OF 21 OROF 22 ! RRC to PUP
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:ToAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END   
      OF 300 ! Sundry Waybill
          ! Do this later.
      ELSE
          ! Get details from the job on the waybill
          Access:WAYBILLJ.ClearKey(waj:DescWaybillNoKey)
          waj:WayBillNumber = way:WayBillNumber
          IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey) = Level:Benign)
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = waj:JobNumber
              IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                  
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  IF (Access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
                  END
                  
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = way:ToAccount
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:UseCustDespAdd = 'YES')
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress                    
                      ELSE
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress                        
                      END
                  end
              END
          END
      END
  END
  
  locBarcode = '*' & CLIP(locConsignmentNumber) & '*'
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'WAYBILL'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('Waybill',ProgressWindow)                   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:WAYBILLJ, ?Progress:PctText, Progress:Thermometer, ProgressMgr, waj:JobNumber)
  ThisReport.AddSortOrder(waj:JobNumberKey)
  ThisReport.AddRange(waj:WayBillNumber,locWaybillNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:WAYBILLJ.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Waybill',ProgressWindow)                ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = waj:JobNumber
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
      RETURN Level:User
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
      RETURN Level:User
  END
  
  locExchanged = ''
  locJobNumber = CLIP(job:Ref_Number) & '-' & p_web.GSV('BookingBranchID') & CLIP(wob:JobNumber)
  IF (waj:WayBillNumber = job:Exchange_Consignment_Number)
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
          locIMEINumber = waj:IMEINumber
      ELSE
          locIMEINumber = xch:ESN
      END
  ELSE
      locIMEINumber = waj:IMEINumber
      IF (job:Exchange_Unit_Number <> 0)
          locExchanged = 'E'
      END
  END
  locSecurityPackNumber = waj:SecurityPackNumber
  locAccessories = ''
        
  If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
      way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
  
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
          If way:WaybillType = 0 Or way:WaybillType = 1
              ! End (DBH 08/03/2007) #8703
              !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
              If jac:Attached <> True
                  Cycle
              End !If jac:Attached <> True
          End ! If way:WaybillType <> 0 And way:WaybillType <> 1
          If locAccessories = ''
              locAccessories = Clip(jac:Accessory)
          Else !If locAccessories = ''
              locAccessories = Clip(locAccessories) & ', ' & Clip(jac:Accessory)
          End !If tmp:Accessories = ''
      End !Loop
  End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
      
  locOrderNumber = job:Order_Number
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','Waybill','Waybill','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
DespatchNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locFullJobNumber     STRING(30)                            !
locChargeType        STRING(30)                            !
locRepairType        STRING(30)                            !
locOriginalIMEI      STRING(30)                            !
locFinalIMEI         STRING(30)                            !
locEngineerReport    STRING(255)                           !
locAccessories       STRING(255)                           !
locLoanExchangeUnit  STRING(100)                           !
locIDNumber          STRING(20)                            !
locQuantity          LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locUnitCost          REAL                                  !
locLineCost          REAL                                  !
locLoanReplacementValue REAL                               !
locLabour            REAL                                  !
locParts             REAL                                  !
locCarriage          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locTerms             STRING(1000)                          !
locBarCodeJobNumber  STRING(30)                            !
locBarCodeIMEINumber STRING(30)                            !
locDisplayJobNumber  STRING(30)                            !
locDisplayIMEINumber STRING(30)                            !
locCustomerName      STRING(60)                            !
locEndUserTelNo      STRING(60)                            !
locDespatchUser      STRING(60)                            !
locClientName        STRING(60)                            !
qOutFaults           QUEUE,PRE()                           !
Description          STRING(255)                           !
                     END                                   !
DefaultAddress       GROUP,PRE(address)                    !
Name                 STRING(40)                            !Name
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
InvoiceAddress       GROUP,PRE(invoice)                    !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
MobileNumber         STRING(30)                            !
                     END                                   !
DeliveryAddress      GROUP,PRE(delivery)                   !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
                     END                                   !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Courier)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Despatch_Number)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(250,6792,7760,1417),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(250,292,7750,10000),USE(?Header),FONT('Tahoma',8,,FONT:regular)
                         STRING('Job Number:'),AT(5125,375),USE(?STRING1)
                         STRING('Despach Batch No:'),AT(5125,625,1042,167),USE(?STRING1:2)
                         STRING('Date Booked:'),AT(5125,875),USE(?STRING2)
                         STRING('Date Completed:'),AT(5125,1125),USE(?STRING3)
                         STRING(@s8),AT(6250,625),USE(job:Despatch_Number),LEFT(1)
                         STRING(@d6b),AT(6250,875),USE(job:date_booked),LEFT
                         STRING(@D6b),AT(6250,1125),USE(job:Date_Completed),LEFT,TRN
                         STRING(@s30),AT(167,1708,2250),USE(invoice:Name)
                         STRING(@s30),AT(167,1875,2208),USE(invoice:AddressLine1)
                         STRING(@s30),AT(167,2042,2208),USE(invoice:AddressLine2)
                         STRING(@s30),AT(167,2208,2250),USE(invoice:AddressLine3)
                         STRING(@s30),AT(167,2375,2208),USE(invoice:AddressLine4)
                         STRING('Tel:'),AT(2500,1875),USE(?STRING4)
                         STRING('Fax:'),AT(2500,2042,302,167),USE(?STRING4:2)
                         STRING('Mobile:'),AT(2500,2208,417,167),USE(?STRING4:3)
                         STRING(@s30),AT(2958,1875,927),USE(invoice:TelephoneNumber)
                         STRING(@s30),AT(2958,2042,927),USE(invoice:FaxNumber)
                         STRING(@s30),AT(2958,2208,927),USE(invoice:MobileNumber)
                         STRING(@s30),AT(4125,1708,2250),USE(delivery:Name)
                         STRING(@s30),AT(4125,1875,2250),USE(delivery:AddressLine1)
                         STRING(@s30),AT(4125,2042,2250),USE(delivery:AddressLine2)
                         STRING(@s30),AT(4125,2208,2250),USE(delivery:AddressLine3)
                         STRING(@s30),AT(4125,2375,2250),USE(delivery:AddressLine4)
                         STRING('Tel:'),AT(4125,2542,198,167),USE(?STRING4:4)
                         STRING(@s30),AT(4375,2542,2250),USE(delivery:TelephoneNumber)
                         STRING(@s30),AT(6250,375,1375),USE(locFullJobNumber)
                         STRING(@s15),AT(156,3406),USE(job:Account_Number),LEFT,TRN
                         STRING(@s30),AT(1677,3406,1125),USE(job:Order_Number),LEFT,TRN
                         STRING(@s30),AT(3187,3406,1323),USE(job:Authority_Number),LEFT,TRN
                         STRING(@s30),AT(4698,3406,1437),USE(locChargeType)
                         STRING(@s30),AT(6208,3406,1500),USE(locRepairType)
                         STRING(@s30),AT(156,4104,1344),USE(job:Model_Number),LEFT,TRN
                         STRING(@s30),AT(1562,4104,1323),USE(job:Manufacturer),LEFT,TRN
                         STRING(@s30),AT(2958,4104,1042),USE(job:Unit_Type),LEFT,TRN
                         STRING(@s20),AT(4115,4104,1042),USE(locOriginalIMEI),LEFT,TRN
                         STRING(@s30),AT(5219,4104,1125),USE(locFinalIMEI),HIDE
                         STRING(@s20),AT(6479,4104,1187),USE(job:MSN),LEFT,TRN
                         STRING('REPORTED FAULT'),AT(167,4333),USE(?STRING5),FONT(,,,FONT:bold)
                         TEXT,AT(1490,4333,5948,427),USE(jbn:Fault_Description),TRN
                         STRING('ENGINEER REPORT'),AT(167,4792),USE(?STRING6),FONT(,,,FONT:bold)
                         TEXT,AT(1500,4792,5948,677),USE(locEngineerReport),TRN
                         STRING('ACCESSORIES'),AT(167,5500),USE(?STRING7),FONT(,,,FONT:bold)
                         TEXT,AT(1500,5500,5948,302),USE(locAccessories),FONT(,,,FONT:regular+FONT:underline)
                         STRING('EXCHANGE UNIT'),AT(167,5833),USE(?strExchangeUnit),FONT(,,,FONT:bold),HIDE
                         STRING(@s100),AT(1500,5833,5948),USE(locLoanExchangeUnit),HIDE
                         STRING('PARTS USED'),AT(167,6292),USE(?STRING9),FONT(,,,FONT:bold)
                         STRING('Qty'),AT(1500,6250),USE(?STRING10),FONT(,,,FONT:bold)
                         STRING('Part Number'),AT(2000,6250),USE(?STRING11),FONT(,,,FONT:bold)
                         STRING('ID NUMBER'),AT(167,6083),USE(?STRING12),FONT(,,,FONT:bold)
                         STRING(@s20),AT(1500,6042),USE(locIDNumber)
                         STRING('Description'),AT(3750,6250),USE(?STRING13),FONT(,,,FONT:bold)
                         STRING('Unit Type'),AT(5833,6250),USE(?STRING14),FONT(,,,FONT:bold)
                         STRING('Line Cost'),AT(6792,6250),USE(?STRING15),FONT(,,,FONT:bold)
                         LINE,AT(1510,6469,5875,0),USE(?LINE1)
                         LINE,AT(1510,8385,5875,0),USE(?lineTerms)
                         STRING('Customer Signature'),AT(167,8208),USE(?strTerms2),FONT(,,,FONT:bold)
                         STRING('Date'),AT(6000,8208),USE(?strTerms3),FONT(,,,FONT:bold)
                         STRING('Courier:'),AT(167,8917,792,167),USE(?STRING21:2)
                         STRING(@s30),AT(1083,8917,1625),USE(job:Courier),FONT(,,,FONT:bold)
                         STRING('Despatch Date:'),AT(167,8750),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(1083,8750),USE(?ReportDateStamp),FONT(,,,FONT:bold),TRN
                         STRING('Consignment No:'),AT(167,9083),USE(?STRING21)
                         STRING(@s30),AT(1083,9083,1625,167),USE(job:Consignment_Number),FONT(,,,FONT:bold)
                         STRING('Loan Replacement Value:'),AT(167,9333),USE(?strLoanReplacementValue),HIDE
                         STRING(@n10.2),AT(1500,9333),USE(locLoanReplacementValue),FONT(,,,FONT:bold),RIGHT(2),HIDE
                         STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(167,9750),USE(?strLoanTerms), |
  FONT(,7,,FONT:bold+FONT:underline),HIDE
                         STRING('Labour:'),AT(5458,8750),USE(?strLabour)
                         STRING('Parts:'),AT(5458,8917),USE(?strParts)
                         STRING('Carriage:'),AT(5458,9083),USE(?strCarriage)
                         STRING('V.A.T.:'),AT(5458,9250),USE(?strVAT)
                         STRING('Total:'),AT(5458,9500),USE(?strTotal),FONT(,,,FONT:bold)
                         LINE,AT(6344,9469,1000,0),USE(?LINE2)
                         STRING(@n14.2b),AT(6292,8750),USE(locLabour),RIGHT(2)
                         STRING(@n14.2b),AT(6292,8917),USE(locParts),RIGHT(2)
                         STRING(@n14.2b),AT(6292,9083),USE(locCarriage),RIGHT(2)
                         STRING(@n14.2b),AT(6292,9250),USE(locVAT),RIGHT(2)
                         STRING(@n14.2b),AT(6156,9500),USE(locTotal),FONT(,,,FONT:bold),RIGHT(2)
                         STRING(@s20),AT(2573,8750),USE(locBarCodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER
                         STRING(@s20),AT(2573,9167,2594,198),USE(locBarCodeIMEINumber),FONT('C39 High 12pt LJ3',12), |
  CENTER
                         STRING(@s30),AT(2729,8917),USE(locDisplayJobNumber),FONT(,,,FONT:bold),CENTER
                         STRING(@s30),AT(2729,9333),USE(locDisplayIMEINumber),FONT(,,,FONT:bold),CENTER
                         TEXT,AT(156,7917,5885,260),USE(locTermsText),FONT(,7)
                       END
Detail                 DETAIL,AT(0,0,7750,208),USE(?Detail)
                         STRING(@n-14b),AT(667,0),USE(locQuantity),RIGHT(2)
                         STRING(@s30),AT(2000,0),USE(locPartNumber)
                         STRING(@s30),AT(3750,0),USE(locDescription)
                         STRING(@n14.2b),AT(5302,0),USE(locUnitCost),RIGHT(2)
                         STRING(@n14.2b),AT(6229,0),USE(locLineCost),RIGHT(2)
                       END
                       FOOTER,AT(260,10208,7750,833),USE(?Footer)
                         TEXT,AT(125,0,7458,792),USE(stt:Text)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('DESPATCH NOTE'),AT(5708,-42,1917,240),USE(?strTitle),FONT(,16,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('CUSTOMER ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         STRING('Account Number'),AT(156,3156,1042,167),USE(?String91:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1677,3156,1042,167),USE(?String91:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3187,3156,1177,167),USE(?String91:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4698,3156,1042,167),USE(?strChargeableType),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Chargeable Repair Type'),AT(6208,3156,1437),USE(?strRepairType),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Model'),AT(156,3833,1042,167),USE(?String91:7),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1562,3833,1042,167),USE(?String91:8),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(2958,3833,1042,167),USE(?String91:9),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4115,3833,1042,167),USE(?strIMEINumber),FONT(,8,,FONT:bold),TRN
                         STRING('Exch IMEI No'),AT(5219,3833,1042,167),USE(?strExchangeIMEINumber),FONT(,8,,FONT:bold), |
  HIDE,TRN
                         STRING('M.S.N.'),AT(6479,3833,1042,167),USE(?String91:12),FONT(,8,,FONT:bold),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DespatchNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  locJobNumber = p_web.GSV('locJobNumber')
  
  SET(DEFAULT2)
  Access:DEFAULT2.Next()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = locJobNumber
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
  END
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey))
  END
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = wob:HeadAccountNumber
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key)) = Level:Benign
      delivery:Name = tra:Company_Name
      delivery:AddressLine1 = tra:Address_Line1
      delivery:AddressLine2 = tra:Address_Line2
      delivery:AddressLine3 = tra:Address_Line3
      delivery:AddressLine4 = tra:Postcode
      delivery:TelephoneNumber = tra:Telephone_Number
  END
  
  locFullJobNumber = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  
  
  IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
      SETTARGET(Report)
      ?strChargeableType{PROP:Text} = 'Warranty Type'
      ?strRepairType{PROP:Text} = 'Warranty Repair Type'
      SETTARGET()
      locChargeType = job:Warranty_Charge_Type
      locRepairType = job:Repair_Type_Warranty
  ELSE
      locChargeType = job:Charge_Type
      locRepairType = job:Repair_Type
  END
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = job:Account_Number
  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
  END
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = sub:Main_Account_Number
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  IF (tra:Price_Despatch <> 'YES' AND sub:PriceDespatchNotes <> 1)
      SETTARGET(Report)
      ?locUnitCost{PROP:Hide} = 1
      ?locLineCost{PROP:Hide} = 1
      ?locLabour{PROP:Hide} = 1
      ?locParts{PROP:Hide} = 1
      ?locCarriage{PROP:Hide} = 1
      ?locVAT{PROP:Hide} = 1
      ?locTotal{PROP:Hide} = 1
      ?strLabour{PROP:Hide} = 1
      ?strParts{PROP:Hide} = 1
      ?strCarriage{PROP:Hide} = 1
      ?strVAT{PROP:Hide} = 1
      ?strTotal{PROP:Hide} = 1
      SETTARGET()
  END
  
  hideDespatchAddress# = 0
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:HideDespAdd = 1)
          hideDespatchAddress# = 1
      END
      IF (sub:UseCustDespAdd = 'YES')
          invoice:AddressLine1 = job:Address_Line1
          invoice:AddressLine2 = job:Address_Line2
          invoice:AddressLine3 = job:Address_Line3
          invoice:AddressLine4 = job:Postcode
          invoice:Name = job:Company_Name
          invoice:TelephoneNumber = job:Telephone_Number
          invoice:FaxNumber = job:Fax_Number
          
      ELSE
          invoice:AddressLine1 = sub:Address_Line1
          invoice:AddressLine2 = sub:Address_Line2
          invoice:AddressLine3 = sub:Address_Line3
          invoice:AddressLine4 = sub:Postcode
          invoice:TelephoneNumber = sub:Telephone_Number
          invoice:Name = sub:Company_Name
          invoice:FaxNumber = sub:Fax_Number
      END
      
  ELSE
      IF (tra:HideDespAdd = 1)
          hideDespatchAddress# = 1
      END
      IF (tra:UseCustDespAdd = 'YES')
          invoice:AddressLine1 = job:Address_Line1
          invoice:AddressLine2 = job:Address_Line2
          invoice:AddressLine3 = job:Address_Line3
          invoice:AddressLine4 = job:Postcode
          invoice:Name = job:Company_Name
          invoice:TelephoneNumber = job:Telephone_Number
          invoice:FaxNumber = job:Fax_Number        
      ELSE
          invoice:AddressLine1 = tra:Address_Line1
          invoice:AddressLine2 = tra:Address_Line2
          invoice:AddressLine3 = tra:Address_Line3
          invoice:AddressLine4 = tra:Postcode
          invoice:TelephoneNumber = tra:Telephone_Number
          invoice:Name = tra:Company_Name
          invoice:FaxNumber = tra:Fax_Number
      END
  END
  IF (hideDespatchAddress# = 1)
      SETTARGET(Report)
      ?delivery:Name{prop:Hide} = 1
      ?delivery:AddressLine1{PROP:Hide} = 1
      ?delivery:AddressLine2{PROP:Hide} = 1
      ?delivery:AddressLine3{PROP:Hide} = 1
      ?delivery:AddressLine4{PROP:Hide} = 1
      ?delivery:TelephoneNumber{prop:Hide} = 1
      SETTARGET()
  END
  
  locIDNumber = jobe2:IDNumber
  
  IF (job:Title = '')
      locCustomerName = job:Initial
  ELSIF (job:Initial = '')
      locCustomerName = job:Title
  ELSE
      locCustomerName = CLIP(job:Title) & ' ' & CLIP(job:Initial)
  END
  
  IF (locCustomerName = '')
      locCustomerName = job:Surname
  ELSIF(job:Surname = '')
  ELSE
      locCustomerName = CLIP(locCustomerName) & ' ' & CLIP(locCustomerName)
  END
  
  locEndUserTelNo = CLIP(jobe:EndUserTelNo)
  
  IF (locCustomerName <> '')
      locClientName = 'Client: ' & CLIP(locCustomerName) & '    Tel: ' & CLIP(locEndUserTelNo)
  ELSE
      IF (locEndUserTelNo <> '')
          locClientName = 'Tel: ' & CLIP(locEndUserTelNo)
      END
  END
  
  IF NOT (p_web.GSV('BookingSite') <> 'RRC' AND jobe:WebJob = 1)
      delivery:Name = job:Company_Name_Delivery
      delivery:AddressLine1 = job:Address_Line1_Delivery
      delivery:AddressLine2 = job:Address_Line2_Delivery
      delivery:AddressLine3 = job:Address_Line3_Delivery
      delivery:AddressLine4 = job:Postcode_Delivery
      delivery:TelephoneNumber = job:Telephone_Delivery
  END
  
  IF (job:Chargeable_Job = 'YES')
      IF (job:Invoice_Number <> '')
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = job:Invoice_Number
          IF (Access:INVOICE.TryFetch(inv:INvoice_Number_Key))
          END
      
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (inv:ExportedRRCOracle)
                  locLabour = jobe:InvRRCCLabourCost
                  locParts = jobe:InvRRCCPartsCost
                  locCarriage = job:Invoice_Courier_Cost
                  locVAT = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + |
                      (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100) + |
                      (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
              ELSE
                  locLabour = jobe:RRCCLabourCost
                  locParts = jobe:RRCCPartsCost
                  locCarriage = job:Courier_Cost
                  locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                      (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                      (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
              END
          ELSE
              locLabour = job:Invoice_Labour_Cost
              locParts = job:Invoice_Parts_Cost
              locCarriage = job:Invoice_Courier_Cost
              locVAT = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100) + |
                  (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100) + |
                  (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
             
          END
      
      ELSE
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour = jobe:RRCCLabourCost
              locParts = jobe:RRCCPartsCost
              locCarriage = job:Courier_Cost
              locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
          ELSE
              locLabour = job:Labour_Cost
              locParts = job:Parts_Cost
              locCarriage = job:Courier_Cost
              locVAT = (job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
          END
      
      END
      
  END
  
  IF (job:Warranty_Job = 'YES')
      IF (job:Invoice_Number_Warranty <> '')
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = job:Invoice_Number_Warranty
          IF (Access:INVOICE.TryFetch(inv:Invoice_NUmber_Key))
          END
          
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour       = jobe:InvRRCWLabourCost
              locParts        = jobe:InvRRCWPartsCost
              locCarriage = job:WInvoice_Courier_Cost
              locVAT          = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100) + |
                  (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100) + |
                  (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)   
          ELSE
              locLabour       = jobe:InvoiceClaimValue
              locParts        = job_ali:Winvoice_parts_cost
              locCarriage = job_ali:Winvoice_courier_cost
              locVAT          = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour / 100) + |
                  (job:WInvoice_Parts_Cost * inv:Vat_Rate_Parts / 100) +                 |
                  (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          END
          
      ELSE
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour       = jobe:RRCWLabourCost
              locParts        = jobe:RRCWPartsCost
              locCarriage = job_ali:Courier_Cost_Warranty
              locVAT          = (jobe:RRCWLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (jobe:RRCWPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
  
          ELSE
              locLabour       = jobe:ClaimValue
              locParts        = job_ali:Parts_Cost_Warranty
              locCarriage = job_ali:Courier_Cost_Warranty
              locVAT          = (jobe:ClaimValue * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (job:Parts_Cost_Warranty * GetVATRate(job:Account_Number, 'P') / 100) +         |
                  (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
  
          END
          
      END
      
      
  END
  
  locTotal = locLabour + locParts + locCarriage + locVAT
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Despatch_User
  IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
      locDespatchUser = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  END
  
  
  
  
  
  
  ! Accessories & Engineer Report
  locAccessories = ''
  Access:JOBACC.ClearKey(jac:Ref_Number_Key)
  jac:Ref_Number = job:Ref_Number
  SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
  LOOP UNTIL Access:JOBACC.Next()
      IF (jac:Ref_Number <> job:Ref_Number)
          BREAK
      END
      
      IF (locAccessories = '')
          locAccessories = jac:Accessory
      ELSE
          locAccessories = CLIP(locAccessories) & ', ' & CLIP(jac:Accessory)
      END
  END
  
  FREE(qOutfaults)
  Access:JOBOUTFL.ClearKey(joo:LevelKey)
  joo:JobNumber = job:Ref_Number
  SET(joo:LevelKey,joo:LevelKey)
  LOOP UNTIL Access:JOBOUTFL.Next()
      IF (joo:JobNumber <> job:Ref_Number)
          BREAK
      END
      qOutfaults.Description = joo:Description
      GET(qOutfaults,qOutfaults.Description)
      IF (ERROR())
          locEngineerReport = CLIP(locEngineerReport) & ' ' & CLIP(joo:Description)
          qOutFaults.Description = joo:Description
          Add(qOutFaults)
      END
  END
  FREE(qOutfaults)
  
  
      
  ! Exchange Note
  
  exchangeNote# = 0
  IF (job:Exchange_Unit_Number <> '')
      exchangeNote# = 1
  END
  
  ! Check Parts For "Exchange Part"
  Access:PARTS.ClearKey(par:Part_Number_Key)
  par:Ref_Number = job:Ref_Number
  SET(par:Part_Number_Key,par:Part_Number_Key)
  LOOP UNTIL Access:PARTS.Next()
      IF (par:Ref_Number <> job:Ref_Number)
          BREAK
      END
      
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          IF (sto:ExchangeUnit = 'YES')
              exchangeNote# = 1
              BREAK
          END
      END
  END
  
  IF (exchangeNote# = 0)
      ! Check Warranty Parts for "Exchange Part"
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = job:Ref_Number
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP UNTIL Access:WARPARTS.next()
          IF (wpr:Ref_Number <> job:Ref_Number)
              BREAK
          END
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = par:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
              IF (sto:ExchangeUnit = 'YES')
                  exchangeNote# = 1
                  BREAK
              END
          END
      END
  END
  
  IF (exchangeNote# = 1)
      ! Do not put exchange unit repair on the
      ! Despatch Note Heading if it's a failed assessment
      IF (jobe:WebJob = 1 AND p_web.GSV('BookingSite') <> 'RRC' AND jobe:Engineer48HourOption = 1)
          IF (PassExchangeAssessment(job:Ref_Number) = 0)
              exchangeNote# = 0
          END
      END
      
  END
  
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  IF (job:Loan_Unit_Number <> '')
      SETTARGET(Report)
      ?strExchangeUnit{PROP:Text} = 'LOAN UNIT'
      IF (p_web.GSV('BookingSite') = 'RRC' AND jobe:DespatchType = 'LOA') OR |
                      (p_web.GSV('BookingSite') <> 'RRC' AND job:Despatch_Type = 'LOA')
          locIDNumber = jobe2:LoanIDNumber
          ?strTitle{PROP:Text} = 'LOAN DESPATCH NOTE'
      END
      ?strExchangeUnit{PROP:Hide} = 0
      ?locLoanExchangeUnit{PROP:Hide} = 0
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = job:Loan_Unit_Number
      IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
          locLoanExchangeUnit = '(' & loa:Ref_Number & ') ' & CLIP(loa:Manufacturer) & | 
                          ' ' & CLIP(loa:Model_Number) & ' - ' & CLIP(loa:ESN)
      END
      SETTARGET()
      ! #12084 New standard texts (Bryan: 17/05/2011)
      Access:STANTEXT.Clearkey(stt:Description_Key)
      stt:Description = 'LOAN DESPATCH NOTE'
      IF (Access:STANTEXT.TryFetch(stt:Description_Key))
      END
  END
  
  locOriginalIMEI = job:ESN
  
  ! Send To THird Party?
  IF (job:Third_Party_Site <> '')
      IMEIError# = 0
      Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
      jot:RefNumber = job:Ref_Number
      SET(jot:RefNumberKey,jot:RefNumberKey)
      IF (Access:JOBTHIRD.Next())
          IMEIError# = 1
      ELSE
          IF (jot:RefNumber <> job:Ref_Number)
              IMEIError# = 1
          ELSE
              IF (jot:OriginalIMEI <> job:ESN)
                  IMEIError# = 0
              ELSE
                  IMEIError# = 1
              END
          END
      END
  ELSE
      IMEIError# = 1
  END
  
  IF (IMEIError# = 1)
      locOriginalIMEI = job:ESN
  ELSE
      SETTARGET(Report)
      locOriginalIMEI = jot:OriginalIMEI
      locFinalIMEI = job:ESN
      ?strIMEINumber{PROP:Text} = 'Orig I.M.E.I. Number'
      ?strExchangeIMEINumber{PROP:Hide} = 0
      ?locFinalIMEI{PROP:Hide} = 0
      SETTARGET()
  END
  
      
  IF (exchangeNote# = 1)
      SETTARGET(Report)
      ?strTitle{prop:Text} = 'EXCHANGE DESPATCH NOTE'
      ?locLoanExchangeUnit{PROP:Hide} = 0
      ?strExchangeUnit{PROP:Hide} = 0
      ?locPartNumber{PROP:Hide} = 1
      ?locDescription{PROP:Hide} = 1
      ?locQuantity{PROP:Hide} = 1
      ?locUnitCost{prop:Hide} = 1
      ?locLineCost{PROP:Hide} = 1
      
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          locLoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) & |
                          ' - ' & CLip(xch:esn)
          locOriginalIMEI = job:ESN
          locFinalIMEI = xch:ESN
          ?locFinalIMEI{PROP:Hide} = 0
          ?strExchangeIMEINumber{PROP:Hide} = 0
      END
      SETTARGET()
  END
  
  
  
          
          
  
  ! Terms
  locTerms = ''
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign)
      locTerms = stt:Text
  END
  
  IF (job:Loan_Unit_Number <> '' AND job:Exchange_Unit_Number = 0)
      SETTARGET(Report)
      ?locLoanReplacementValue{PROP:Hide} = 0
      ?strLoanReplacementValue{PROP:Hide} = 0
      locLoanReplacementValue = jobe:LoanReplacementValue
      ?strLoanTerms{PROP:Hide} = 0
      !?strTerms1{PROP:Hide} = 0
      ?strTerms2{PROP:Hide} = 0
      ?strTerms3{PROP:Hide} = 0
      ?lineTerms{PROP:Hide} = 0
      SETTARGET()
      locTerms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
      locTerms = Clip(locTerms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
      locTerms = Clip(locTerms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
      locTerms = Clip(locTerms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
      locTerms = Clip(locTerms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
  
  END
  
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI') !! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  ! Barcode Bit
  locBarCodeJobNumber = '*' & job:Ref_Number & '*'
  locBarCodeIMEINumber = '*' & job:ESN & '*'
  locDisplayIMEINumber = 'I.M.E.I. No: ' & job:ESN
  locDisplayJobNumber = 'Job No: ' & job:Ref_Number
  Do DefineListboxStyle
  INIMgr.Fetch('DespatchNote',ProgressWindow)              ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('DespatchNote',ProgressWindow)           ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  countParts# = 0
  IF (job:Warranty_Job = 'YES')
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = job:Ref_Number
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP UNTIL Access:WARPARTS.Next()
          IF (wpr:Ref_Number <> job:Ref_Number)
              BREAK
          END
          locPartNumber = wpr:Part_Number
          locDescription = wpr:Description
          locQuantity = wpr:Quantity
          locUnitCost = wpr:Purchase_Cost
          locLineCost = wpr:Quantity * wpr:Purchase_Cost
          Print(rpt:Detail)
          countParts# += 1
      END
  END
  
  IF (countParts# = 0)
      locPartNumber = ''
      Print(rpt:Detail)
  END
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','DespatchNote','DespatchNote','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

PassExchangeAssessment PROCEDURE  (Long fJobNumber)        ! Declare Procedure
rtnValue             BYTE                                  !
PARTS::State  USHORT
WARPARTS::State  USHORT
MANFAUPA::State  USHORT
MANFAULO::State  USHORT
MANFAULT::State  USHORT
JOBOUTFL::State  USHORT
JOBS::State  USHORT
FilesOpened     BYTE(0)
tmp:FaultCode   STRING(255)
  CODE
    DO openFiles
    DO SaveFiles
    !Loop through outfaults
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = fJobNumber
    IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))
        
    END
    
    rtnValue = 0
    
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
            Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
                        Then Break.  ! End If
    !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                    Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                                Or mfo:Field_Number <> maf:Field_Number      |
                                Or mfo:Field        <> joo:FaultCode      |
                                Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
    !Make sure the descriptions match in case of duplicates
                    If mfo:ReturnToRRC
                        rtnValue = 0
                        Do ExitProc
                    End !If mfo:ReturnToRRC
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
    
        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
    End !Loop
    
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    map:Manufacturer = job:Manufacturer
    
    map:Manufacturer = job:Manufacturer
    
                        
    
    !Is an outfault records on parts for this manufacturer
        Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Found
    !Loop through the parts as see if any of the faults codes are excluded
            If job:Warranty_Job = 'YES'
    
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                        Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
    
    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = wpr:Fault_Code1
                        Of 2
                            tmp:FaultCode        = wpr:Fault_Code2
                        Of 3
                            tmp:FaultCode        = wpr:Fault_Code3
                        Of 4
                            tmp:FaultCode        = wpr:Fault_Code4
                        Of 5
                            tmp:FaultCode        = wpr:Fault_Code5
                        Of 6
                            tmp:FaultCode        = wpr:Fault_Code6
                        Of 7
                            tmp:FaultCode        = wpr:Fault_Code7
                        Of 8
                            tmp:FaultCode        = wpr:Fault_Code8
                        Of 9
                            tmp:FaultCode        = wpr:Fault_Code9
                        Of 10
                            tmp:FaultCode        = wpr:Fault_Code10
                        Of 11
                            tmp:FaultCode        = wpr:Fault_Code11
                        Of 12
                            tmp:FaultCode        = wpr:Fault_Code12
                        End !Case map:Field_Number
    
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop
    
                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
                End !Loop
            End !If job:Warranty_Job = 'YES'
    
            If job:Chargeable_Job = 'YES'
    !Loop through the parts as see if any of the faults codes are excluded
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                        Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
    
    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = par:Fault_Code1
                        Of 2
                            tmp:FaultCode        = par:Fault_Code2
                        Of 3
                            tmp:FaultCode        = par:Fault_Code3
                        Of 4
                            tmp:FaultCode        = par:Fault_Code4
                        Of 5
                            tmp:FaultCode        = par:Fault_Code5
                        Of 6
                            tmp:FaultCode        = par:Fault_Code6
                        Of 7
                            tmp:FaultCode        = par:Fault_Code7
                        Of 8
                            tmp:FaultCode        = par:Fault_Code8
                        Of 9
                            tmp:FaultCode        = par:Fault_Code9
                        Of 10
                            tmp:FaultCode        = par:Fault_Code10
                        Of 11
                            tmp:FaultCode        = par:Fault_Code11
                        Of 12
                            tmp:FaultCode        = par:Fault_Code12
                        End !Case map:Field_Number
    
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop
    
                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
                End !Loop
            End !If job:Chargeable_Job = 'YES'
        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    
        rtnValue = 1
        Do ExitProc
    

ExitProc    ROUTINE
    DO RestoreFiles
    DO CloseFiles
    RETURN rtnValue
SaveFiles  ROUTINE
  PARTS::State = Access:PARTS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  WARPARTS::State = Access:WARPARTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAUPA::State = Access:MANFAUPA.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULO::State = Access:MANFAULO.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULT::State = Access:MANFAULT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBOUTFL::State = Access:JOBOUTFL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS::State = Access:JOBS.SaveFile()                     ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF PARTS::State <> 0
    Access:PARTS.RestoreFile(PARTS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF WARPARTS::State <> 0
    Access:WARPARTS.RestoreFile(WARPARTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAUPA::State <> 0
    Access:MANFAUPA.RestoreFile(MANFAUPA::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULO::State <> 0
    Access:MANFAULO.RestoreFile(MANFAULO::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULT::State <> 0
    Access:MANFAULT.RestoreFile(MANFAULT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBOUTFL::State <> 0
    Access:JOBOUTFL.RestoreFile(JOBOUTFL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS::State <> 0
    Access:JOBS.RestoreFile(JOBS::State)                   ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:MANFAUPA.Close
     Access:MANFAULO.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:JOBS.Close
     FilesOpened = False
  END
NextWaybillNumber    PROCEDURE                             ! Declare Procedure
locKeyField          LONG                                  !
locWaybillNumber     LONG                                  !
WAYBILLS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    DO SaveFiles
    
    ! Remove blanks
    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = 0
    SET(way:WayBillNumberKey,way:WayBillNumberKey)
    LOOP UNTIL Access:WAYBILLS.Next()
        IF (way:WayBillNumber <> 0)
            BREAK
        END
        Relate:WAYBILLS.delete(0)
    END
    
    locWaybillNumber = 0
    locKeyField = 0
    LOOP
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = 99999999
        SET(way:WayBillNumberKey,way:WayBillNumberKey)
        LOOP 
            IF (Access:WAYBILLS.Previous())
                locKeyfield = 1
                BREAK
            END
            locKeyfield = way:WayBillNumber + 1
            BREAK
        END
        
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locKeyField
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
            ! Double check the number doesn't already exists
            IF locKeyField = 1
                
                BREAK
            END
            CYCLE
        ELSE
            ! Doesn't exist. Add it
            IF (Access:WAYBILLS.PrimeRecord() = Level:Benign)
                way:WayBillNumber = locKeyField
                IF (Access:WAYBILLS.TryInsert() = Level:Benign)
                    locWaybillNumber = way:WayBillNumber
                    BREAK
                ELSE
                    Access:WAYBILLS.CancelAutoInc()
                END
            END
        END
       
    END
    
    DO RestoreFiles
    DO CloseFiles
    
    RETURN locWayBillNumber
    
    
SaveFiles  ROUTINE
  WAYBILLS::State = Access:WAYBILLS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF WAYBILLS::State <> 0
    Access:WAYBILLS.RestoreFile(WAYBILLS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:WAYBILLS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WAYBILLS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:WAYBILLS.Close
     FilesOpened = False
  END
BrowsePayments       PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBPAYMT)
                      Project(jpt:Record_Number)
                      Project(jpt:Date)
                      Project(jpt:Payment_Type)
                      Project(jpt:User_Code)
                      Project(jpt:Amount)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('BrowsePayments')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowsePayments:NoForm')
      loc:NoForm = p_web.GetValue('BrowsePayments:NoForm')
      loc:FormName = p_web.GetValue('BrowsePayments:FormName')
    else
      loc:FormName = 'BrowsePayments_frm'
    End
    p_web.SSV('BrowsePayments:NoForm',loc:NoForm)
    p_web.SSV('BrowsePayments:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowsePayments:NoForm')
    loc:FormName = p_web.GSV('BrowsePayments:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowsePayments') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowsePayments')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBPAYMT,jpt:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JPT:DATE') then p_web.SetValue('BrowsePayments_sort','1')
    ElsIf (loc:vorder = 'JPT:PAYMENT_TYPE') then p_web.SetValue('BrowsePayments_sort','2')
    ElsIf (loc:vorder = 'JPT:USER_CODE') then p_web.SetValue('BrowsePayments_sort','3')
    ElsIf (loc:vorder = 'JPT:AMOUNT') then p_web.SetValue('BrowsePayments_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowsePayments:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowsePayments:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowsePayments:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowsePayments:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowsePayments:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowsePayments_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowsePayments_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'jpt:Date','-jpt:Date')
    Loc:LocateField = 'jpt:Date'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jpt:Payment_Type)','-UPPER(jpt:Payment_Type)')
    Loc:LocateField = 'jpt:Payment_Type'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jpt:User_Code)','-UPPER(jpt:User_Code)')
    Loc:LocateField = 'jpt:User_Code'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'jpt:Amount','-jpt:Amount')
    Loc:LocateField = 'jpt:Amount'
  of 5
    Loc:LocateField = ''
  of 6
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+jpt:Ref_Number,+jpt:Date'
  end
  If False ! add range fields to sort order
  Else
    If Instring('JPT:REF_NUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'jpt:Ref_Number,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('jpt:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@d6b')
  Of upper('jpt:Payment_Type')
    loc:SortHeader = p_web.Translate('Payment Type')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@s30')
  Of upper('jpt:User_Code')
    loc:SortHeader = p_web.Translate('User Code')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@s3')
  Of upper('jpt:Amount')
    loc:SortHeader = p_web.Translate('Payment Received')
    p_web.SetSessionValue('BrowsePayments_LocatorPic','@n-14.2')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowsePayments:LookupFrom')
  End!Else
    loc:formaction = 'FormPayments'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowsePayments:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowsePayments:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowsePayments:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBPAYMT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jpt:Record_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePayments',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowsePayments',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowsePayments.locate(''Locator2BrowsePayments'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePayments.cl(''BrowsePayments'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowsePayments_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowsePayments_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowsePayments','Date','Click here to sort by Date',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date')&'">'&p_web.Translate('Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowsePayments','Payment Type','Click here to sort by Payment Type',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Payment Type')&'">'&p_web.Translate('Payment Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowsePayments','User Code','Click here to sort by User Code',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by User Code')&'">'&p_web.Translate('User Code')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowsePayments','Payment Received','Click here to sort by Payment Received',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Payment Received')&'">'&p_web.Translate('Payment Received')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Edit
        do AddPacket
        loc:columns += 1
    End ! Selecting
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Delete
        do AddPacket
        loc:columns += 1
    End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jpt:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('jpt:record_number',lower(Thisview{prop:order}),1,1) = 0 !and JOBPAYMT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'jpt:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jpt:Record_Number'),p_web.GetValue('jpt:Record_Number'),p_web.GetSessionValue('jpt:Record_Number'))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'jpt:Ref_Number = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePayments',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowsePayments_Filter')
    p_web.SetSessionValue('BrowsePayments_FirstValue','')
    p_web.SetSessionValue('BrowsePayments_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBPAYMT,jpt:Record_Number_Key,loc:PageRows,'BrowsePayments',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBPAYMT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBPAYMT,loc:firstvalue)
              Reset(ThisView,JOBPAYMT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBPAYMT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBPAYMT,loc:lastvalue)
            Reset(ThisView,JOBPAYMT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jpt:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Payments')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePayments.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePayments.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePayments.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePayments.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 0
      If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowsePayments')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePayments',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowsePayments_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowsePayments_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowsePayments',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowsePayments.locate(''Locator1BrowsePayments'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePayments.cl(''BrowsePayments'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowsePayments_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowsePayments_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePayments.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePayments.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePayments.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePayments.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 0
    If loc:viewonly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowsePayments')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = jpt:Record_Number
    p_web._thisrow = p_web._nocolon('jpt:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowsePayments:LookupField')) = jpt:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((jpt:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowsePayments.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBPAYMT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBPAYMT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBPAYMT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBPAYMT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','jpt:Record_Number',clip(loc:field),,loc:checked,,,'onclick="BrowsePayments.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','jpt:Record_Number',clip(loc:field),,'checked',,,'onclick="BrowsePayments.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jpt:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jpt:Payment_Type
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jpt:User_Code
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jpt:Amount
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowsePayments.omv(this);" onMouseOut="BrowsePayments.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowsePayments=new browseTable(''BrowsePayments'','''&clip(loc:formname)&''','''&p_web._jsok('jpt:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('jpt:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormPayments'');<13,10>'&|
      'BrowsePayments.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowsePayments.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePayments')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePayments')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePayments')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePayments')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBPAYMT)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBPAYMT)
  Bind(jpt:Record)
  Clear(jpt:Record)
  NetWebSetSessionPics(p_web,JOBPAYMT)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('jpt:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::jpt:Date   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jpt:Date_'&jpt:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jpt:Date,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jpt:Payment_Type   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jpt:Payment_Type_'&jpt:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jpt:Payment_Type,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jpt:User_Code   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jpt:User_Code_'&jpt:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jpt:User_Code,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jpt:Amount   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jpt:Amount_'&jpt:Record_Number,,net:crc)
      loc:total[4] = loc:total[4] + (jpt:Amount)
      packet = clip(packet) & p_web._jsok(Left(Format(jpt:Amount,'@n-14.2')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Edit_'&jpt:Record_Number,,net:crc)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowsePayments',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Delete_'&jpt:Record_Number,,net:crc)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowsePayments',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = jpt:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('jpt:Record_Number',jpt:Record_Number)


MakeFooter  Routine
  TableQueue.Kind = Net:RowFooter
  If records(TableQueue) > 0
    packet = clip(packet) & '<tr>'
    If(loc:SelectionMethod  = Net:Radio)
      packet = clip(packet) & '<td width="1">&#160;</td>' ! first column is the select column
    End
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = 'BrowseFooter'
        If loc:class[1] = ' ' then loc:class = clip('BrowseFooter') & loc:class.
        If loc:class <> '' then loc:class = ' class="'&clip(loc:class)&'"'.
          loc:skip = 1
        packet = clip(packet) & '<td'&clip(loc:class)&'>' & Format(loc:total[4],'@n-14.2') &'</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:Selecting = 0
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      End
      If loc:Selecting = 0
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      End
    packet = clip(packet) & '</tr>'
    TableQueue.Kind = Net:RowFooter
  End
  do AddPacket

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('jpt:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('jpt:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('jpt:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
DisplayBrowsePayments PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WEBJOB::State  USHORT
INVOICE::State  USHORT
JOBSE2::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
    MAP
showHideCreateInvoice       PROCEDURE(),BYTE
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DisplayBrowsePayments')
  loc:formname = 'DisplayBrowsePayments_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DisplayBrowsePayments','')
    p_web._DivHeader('DisplayBrowsePayments',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionvalues  ROUTINE
    p_web.DeleteSessionValue('IgnoreMessage')
    p_web.DeleteSessionValue('Job:ViewOnly')
    p_web.DeleteSessionValue('DisplayBrowsePaymentsReturnURL')
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Return URL
  IF (p_web.IfExistsValue('DisplayBrowsePaymentsReturnURL'))
      p_web.StoreValue('DisplayBrowsePaymentsReturnURL')
  END
  ! Open Files
  ! Assume JOBS is in a session queue
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(JOBSE)
  END
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(JOBSE2)
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(WEBJOB)
  END
  
  
  IF (p_web.IfExistsValue('IgnoreMessage'))
      p_web.StoreValue('IgnoreMessage')
  END
  
  ! Force the cost screen to be view only 
  p_web.SSV('Job:ViewOnly',1)
  
  
  p_web.SetValue('DisplayBrowsePayments_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('DisplayBrowsePayments_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Close'
  
  IF (p_web.GSV('job:Date_Completed') = 0)
      IF (p_web.GSV('IgnoreMessage') <> 1)
          p_web.SSV('Message:Text','Warning! This job has not been completed!\n\nClick ''OK'' To Continue.')
          p_web.SSV('Message:PassURL','DisplayBrowsePayments?IgnoreMessage=1')
          p_web.SSV('Message:FailURL',p_web.GSV('DisplayBrowsePaymentsReturnURL'))
          MessageQuestion(p_web)
          Exit
      END
  END
  
  p_web.SSV('Hide:CreateCreditNoteButton',1)
  
      
  IF (p_web.GSV('BookingSite') = 'RRC')
      ! Only credit if job has been invoiced
      IF (p_web.GSV('job:Invoice_Number') = 0)
      ELSE
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
              IF (inv:RRCInvoiceDate = 0)
              ELSE
                  creditAllowed# = 1
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = p_web.GSV('job:Account_Number')
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:Generic_Account <> 1)
                          Access:TRADEACC.clearkey(tra:Account_Number_Key)
                          tra:Account_Number = p_web.GSV('BookingAccount')
                          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                              IF (tra:CompanyOwned = 1)
                                  creditAllowed# = 0
                              END
                          END
                      ELSE
                          creditAllowed# = 0
                      END
          
                  END
      
                  IF (creditAllowed# = 1)
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = p_web.GSV('BookingAccount')
                      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          IF (tra:AllowCreditNotes)
                              p_web.SSV('Hide:CreateCreditNoteButton',0)
                          END
                      END
                  END
                  
              END
              
          END
      END
        
          
  ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
  END ! IF (p_web.GSV('BookingSite') = 'RRC')
  
      
  
  p_web.SSV('Hide:IssueRefundButton',1)
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ISSUE REFUND') = 0)
      p_web.SSV('Hide:IssueRefundButton',0)
  END
  
  
  p_web.SSV('Hide:CreateInvoiceButton',showHideCreateInvoice())
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('DisplayBrowsePaymentsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DisplayBrowsePayments_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DisplayBrowsePayments_ChainTo')
    loc:formaction = p_web.GetSessionValue('DisplayBrowsePayments_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('DisplayBrowsePaymentsReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="DisplayBrowsePayments" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="DisplayBrowsePayments" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="DisplayBrowsePayments" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Amend Payments') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Amend Payments',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_DisplayBrowsePayments">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_DisplayBrowsePayments" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_DisplayBrowsePayments')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Payments') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_DisplayBrowsePayments')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_DisplayBrowsePayments'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_DisplayBrowsePayments')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Payments') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DisplayBrowsePayments_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowsePayments
      do Comment::BrowsePayments
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DisplayBrowsePayments_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonViewCosts
      do Comment::buttonViewCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonIssueRefund
      do Comment::buttonIssueRefund
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCreateCreditNote
      do Comment::buttonCreateCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCreateInvoice
      do Comment::buttonCreateInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowsePayments  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowsePayments',p_web.GetValue('NewValue'))
    do Value::BrowsePayments
  Else
    p_web.StoreValue('jpt:Record_Number')
  End

Value::BrowsePayments  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowsePayments --
  p_web.SetValue('BrowsePayments:NoForm',1)
  p_web.SetValue('BrowsePayments:FormName',loc:formname)
  p_web.SetValue('BrowsePayments:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayBrowsePayments')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&'"><!-- Net:BrowsePayments --></div><13,10>'
    p_web._DivHeader('DisplayBrowsePayments_' & lower('BrowsePayments') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('DisplayBrowsePayments_' & lower('BrowsePayments') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowsePayments --><13,10>'
  end
  do SendPacket

Comment::BrowsePayments  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('BrowsePayments') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewCosts',p_web.GetValue('NewValue'))
    do Value::buttonViewCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewCosts  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonViewCosts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewCosts','Job Costs','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewCosts?ViewCostsReturnURL=DisplayBrowsePayments')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonViewCosts  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonViewCosts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonIssueRefund  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonIssueRefund',p_web.GetValue('NewValue'))
    do Value::buttonIssueRefund
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonIssueRefund  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonIssueRefund') & '_value',Choose(p_web.GSV('Hide:IssueRefundButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:IssueRefundButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','IssueRefund','Issue Refund','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormPayments?Insert_btn=Insert&PassedType=REFUND')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money_delete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonIssueRefund  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonIssueRefund') & '_comment',Choose(p_web.GSV('Hide:IssueRefundButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:IssueRefundButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateCreditNote',p_web.GetValue('NewValue'))
    do Value::buttonCreateCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateCreditNote  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateCreditNote') & '_value',Choose(p_web.GSV('Hide:CreateCreditNoteButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateCreditNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateCreditNote','Create Credit Note','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreateCreditNote')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money_add.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCreateCreditNote  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateCreditNote') & '_comment',Choose(p_web.GSV('Hide:CreateCreditNoteButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateCreditNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateInvoice') & '_value',Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),'DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateInvoice') & '_comment',Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateInvoiceButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)

PreCopy  Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)

PreDelete       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DisplayBrowsePayments_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('DisplayBrowsePayments_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  DO DeleteSessionValues
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
showHideCreateInvoice       PROCEDURE()
CODE
    IF (p_web.gsv('job:Chargeable_Job') <> 'YES')
        RETURN TRUE
    END
        
    SentToHub(p_web)
    IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('SendToHub') <> 1)
        RETURN TRUE
    END
        
    IF (p_web.GSV('job:Bouncer') = 'X')
        RETURN TRUE
    END
        
    ! Job not completed
    IF (p_web.GSV('job:Date_Completed') = 0 AND p_web.GSV('job:Exchange_Unit_Number') = 0)
        RETURN TRUE
    END
        
    ! Job has not been priced
    IF (p_web.GSV('job:ignore_Chargeable_Charges') = 'YES')
        IF (p_web.GSV('BookingSite') = 'RRC')
            IF (p_web.GSV('jobe:RRCSubTotal') = 0)
                RETURN TRUE
            END
        ELSE
            IF (p_web.GSV('job:Sub_Total') = 0)
                RETURN TRUE
            END
                
        END
    END
    p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DisplayBrowsePayments')
    p_web.SSV('URL:CreateInvoiceTarget','_self')
    p_web.SSV('URL:CreateInvoiceText','Create Invoice')
 
    IF (p_web.GSV('job:Invoice_Number') > 0)
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            IF (inv:Invoice_Type <> 'SIN')
                RETURN TRUE
            END
            ! If job has been invoiced, just print the invoice, rather than asking to create one
            IF (p_web.GSV('BookingSite') = 'RRC')
                IF (inv:RRCInvoiceDate > 0)
                    p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                    p_web.SSV('URL:CreateInvoiceTarget','_blank')
                    p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                END
            ELSE
                If (inv:ARCInvoiceDate > 0)
                    p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                    p_web.SSV('URL:CreateInvoiceTarget','_blank')
                    p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                END
            END
        END
            
    END
    RETURN FALSE
    
        
        
FormPayments         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBPAYMT_ALIAS::State  USHORT
PAYTYPES::State  USHORT
JOBPAYMT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
jpt:Payment_Type_OptionView   View(PAYTYPES)
                          Project(pay:Payment_Type)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormPayments')
  loc:formname = 'FormPayments_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormPayments','')
    p_web._DivHeader('FormPayments',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormPayments',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormPayments',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('PassedType')        
CreditCardBit       ROUTINE
    p_web.SSV('Hide:CreditCard',1)
! #12274 Don't want to see credit card bits (Bryan: 05/09/2011)    
!    IF (p_web.GSV('jpt:Payment_Type') <> '')
!        Access:PAYTYPES.ClearKey(pay:Payment_Type_Key)
!        pay:Payment_Type = p_web.GSV('jpt:Payment_Type')
!        IF (Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign)
!            IF (pay:Credit_Card = 'YES')
!                p_web.SSV('Hide:CreditCard',0)
!            ELSE
!                p_web.SSV('Hide:CreditCard',1)
!            END
!        END
!    END
!    
OpenFiles  ROUTINE
  p_web._OpenFile(JOBPAYMT_ALIAS)
  p_web._OpenFile(PAYTYPES)
  p_web._OpenFile(JOBPAYMT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBPAYMT_ALIAS)
  p_Web._CloseFile(PAYTYPES)
  p_Web._CloseFile(JOBPAYMT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('Title:JobPayments','')
  IF (p_web.IfExistsValue('PassedType'))
      p_web.SToreValue('PassedType')
      p_web.SSV('Title:JobPayments',': ' & p_web.GSV('PassedType'))
  END
  
  p_web.SetValue('FormPayments_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBPAYMT')
  p_web.SetValue('UpdateKey','jpt:Record_Number_Key')
  p_web.SetValue('IDField','jpt:Record_Number')
  do RestoreMem

CancelForm  Routine
  Do DeleteSessionValues
  IF p_web.GetSessionValue('FormPayments:Primed') = 1
    p_web._deleteFile(JOBPAYMT)
    p_web.SetSessionValue('FormPayments:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBPAYMT')
  p_web.SetValue('UpdateKey','jpt:Record_Number_Key')
  If p_web.IfExistsValue('jpt:Date')
    p_web.SetPicture('jpt:Date','@d06b')
  End
  p_web.SetSessionPicture('jpt:Date','@d06b')
  If p_web.IfExistsValue('jpt:Credit_Card_Number')
    p_web.SetPicture('jpt:Credit_Card_Number','@s20')
  End
  p_web.SetSessionPicture('jpt:Credit_Card_Number','@s20')
  If p_web.IfExistsValue('jpt:Expiry_Date')
    p_web.SetPicture('jpt:Expiry_Date','@p##/##p')
  End
  p_web.SetSessionPicture('jpt:Expiry_Date','@p##/##p')
  If p_web.IfExistsValue('jpt:Issue_Number')
    p_web.SetPicture('jpt:Issue_Number','@s5')
  End
  p_web.SetSessionPicture('jpt:Issue_Number','@s5')
  If p_web.IfExistsValue('jpt:Amount')
    p_web.SetPicture('jpt:Amount','@n-14.2')
  End
  p_web.SetSessionPicture('jpt:Amount','@n-14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormPayments_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  DO CreditCardBit
  
  
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'DisplayBrowsePayments'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormPayments_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormPayments_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormPayments_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'DisplayBrowsePayments'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBPAYMT__FileAction" value="'&p_web.getSessionValue('JOBPAYMT:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBPAYMT" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBPAYMT" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="jpt:Record_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormPayments" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormPayments" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormPayments" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','jpt:Record_Number',p_web._jsok(p_web.getSessionValue('jpt:Record_Number'))) & '<13,10>'
  If p_web.Translate('Job Payments' & p_web.GSV('Title:JobPayments')) <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Payments' & p_web.GSV('Title:JobPayments'),0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormPayments">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormPayments" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormPayments')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormPayments')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormPayments'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jpt:Date')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormPayments')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormPayments_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Date
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Payment_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Payment_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Payment_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Credit_Card_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Credit_Card_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Credit_Card_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Expiry_Date
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Expiry_Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Expiry_Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Issue_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Issue_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Issue_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Amount
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Amount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Amount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormPayments_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonViewCosts
      do Comment::buttonViewCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::jpt:Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jpt:Date  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Date',p_web.GetValue('NewValue'))
    jpt:Date = p_web.GetValue('NewValue') !FieldType= DATE Field = jpt:Date
    do Value::jpt:Date
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Date',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    jpt:Date = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If jpt:Date = ''
    loc:Invalid = 'jpt:Date'
    loc:alert = p_web.translate('Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    jpt:Date = Upper(jpt:Date)
    p_web.SetSessionValue('jpt:Date',jpt:Date)
  do Value::jpt:Date
  do SendAlert

Value::jpt:Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jpt:Date
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Date')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Date = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Date'',''formpayments_jpt:date_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Date',p_web.GetSessionValue('jpt:Date'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
      packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(jpt__Date,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''formpayments_pickdate_value'',1,FieldValue(this,1));nextFocus(FormPayments_frm,'''',0);" value="Select Date" name="Date" type="button">...</button>'
      do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Date') & '_value')

Comment::jpt:Date  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jpt:Payment_Type  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Payment Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jpt:Payment_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Payment_Type',p_web.GetValue('NewValue'))
    jpt:Payment_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Payment_Type
    do Value::jpt:Payment_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Payment_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jpt:Payment_Type = p_web.GetValue('Value')
  End
  If jpt:Payment_Type = ''
    loc:Invalid = 'jpt:Payment_Type'
    loc:alert = p_web.translate('Payment Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  DO CreditCardBit
  do Value::jpt:Payment_Type
  do SendAlert
  do Prompt::jpt:Credit_Card_Number
  do Value::jpt:Credit_Card_Number  !1
  do Comment::jpt:Credit_Card_Number
  do Prompt::jpt:Expiry_Date
  do Value::jpt:Expiry_Date  !1
  do Comment::jpt:Expiry_Date
  do Prompt::jpt:Issue_Number
  do Value::jpt:Issue_Number  !1
  do Comment::jpt:Issue_Number

Value::jpt:Payment_Type  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Payment_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Payment_Type = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Payment_Type'',''formpayments_jpt:payment_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jpt:Payment_Type')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('jpt:Payment_Type',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('jpt:Payment_Type') = 0
    p_web.SetSessionValue('jpt:Payment_Type','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('jpt:Payment_Type')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(JOBPAYMT_ALIAS)
  bind(jpt_ali:Record)
  p_web._OpenFile(PAYTYPES)
  bind(pay:Record)
  p_web._OpenFile(JOBPAYMT)
  bind(jpt:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(jpt:Payment_Type_OptionView)
  jpt:Payment_Type_OptionView{prop:order} = 'UPPER(pay:Payment_Type)'
  Set(jpt:Payment_Type_OptionView)
  Loop
    Next(jpt:Payment_Type_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('jpt:Payment_Type') = 0
      p_web.SetSessionValue('jpt:Payment_Type',pay:Payment_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,pay:Payment_Type,choose(pay:Payment_Type = p_web.getsessionvalue('jpt:Payment_Type')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(jpt:Payment_Type_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(JOBPAYMT_ALIAS)
  p_Web._CloseFile(PAYTYPES)
  p_Web._CloseFile(JOBPAYMT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_value')

Comment::jpt:Payment_Type  Routine
    loc:comment = ''
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jpt:Credit_Card_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Credit Card Number')
  If p_web.GSV('Hide:CreditCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_prompt')

Validate::jpt:Credit_Card_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Credit_Card_Number',p_web.GetValue('NewValue'))
    jpt:Credit_Card_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Credit_Card_Number
    do Value::jpt:Credit_Card_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Credit_Card_Number',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    jpt:Credit_Card_Number = p_web.GetValue('Value')
  End
  If jpt:Credit_Card_Number = ''
    loc:Invalid = 'jpt:Credit_Card_Number'
    loc:alert = p_web.translate('Credit Card Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    jpt:Credit_Card_Number = Upper(jpt:Credit_Card_Number)
    p_web.SetSessionValue('jpt:Credit_Card_Number',jpt:Credit_Card_Number)
  do Value::jpt:Credit_Card_Number
  do SendAlert

Value::jpt:Credit_Card_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_value',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Credit_Card_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Credit_Card_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Credit_Card_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Credit_Card_Number'',''formpayments_jpt:credit_card_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Credit_Card_Number',p_web.GetSessionValueFormat('jpt:Credit_Card_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s20'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_value')

Comment::jpt:Credit_Card_Number  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_comment',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_comment')

Prompt::jpt:Expiry_Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Expiry Date')
  If p_web.GSV('Hide:CreditCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_prompt')

Validate::jpt:Expiry_Date  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Expiry_Date',p_web.GetValue('NewValue'))
    jpt:Expiry_Date = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Expiry_Date
    do Value::jpt:Expiry_Date
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Expiry_Date',p_web.dFormat(p_web.GetValue('Value'),'@p##/##p'))
    jpt:Expiry_Date = p_web.Dformat(p_web.GetValue('Value'),'@p##/##p') !
  End
  If jpt:Expiry_Date = ''
    loc:Invalid = 'jpt:Expiry_Date'
    loc:alert = p_web.translate('Expiry Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::jpt:Expiry_Date
  do SendAlert

Value::jpt:Expiry_Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_value',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Expiry_Date
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Expiry_Date')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Expiry_Date = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Expiry_Date'',''formpayments_jpt:expiry_date_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Expiry_Date',p_web.GetSessionValue('jpt:Expiry_Date'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@p##/##p',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_value')

Comment::jpt:Expiry_Date  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_comment',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_comment')

Prompt::jpt:Issue_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Issue Number')
  If p_web.GSV('Hide:CreditCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_prompt')

Validate::jpt:Issue_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Issue_Number',p_web.GetValue('NewValue'))
    jpt:Issue_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Issue_Number
    do Value::jpt:Issue_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Issue_Number',p_web.dFormat(p_web.GetValue('Value'),'@s5'))
    jpt:Issue_Number = p_web.GetValue('Value')
  End
  do Value::jpt:Issue_Number
  do SendAlert

Value::jpt:Issue_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_value',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Issue_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jpt:Issue_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Issue_Number'',''formpayments_jpt:issue_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Issue_Number',p_web.GetSessionValueFormat('jpt:Issue_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s5'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_value')

Comment::jpt:Issue_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_comment',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_comment')

Prompt::jpt:Amount  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Payment')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jpt:Amount  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Amount',p_web.GetValue('NewValue'))
    jpt:Amount = p_web.GetValue('NewValue') !FieldType= REAL Field = jpt:Amount
    do Value::jpt:Amount
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Amount',p_web.dFormat(p_web.GetValue('Value'),'@n-14.2'))
    jpt:Amount = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2') !
  End
  If jpt:Amount = ''
    loc:Invalid = 'jpt:Amount'
    loc:alert = p_web.translate('Payment') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::jpt:Amount
  do SendAlert

Value::jpt:Amount  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jpt:Amount
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Amount')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Amount = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Amount'',''formpayments_jpt:amount_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Amount',p_web.GetSessionValue('jpt:Amount'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n-14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Amount') & '_value')

Comment::jpt:Amount  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewCosts',p_web.GetValue('NewValue'))
    do Value::buttonViewCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewCosts  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('buttonViewCosts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewCosts','Job Costs','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewCosts?ViewCostsReturnURL=FormPayments')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonViewCosts  Routine
    loc:comment = ''
  p_web._DivHeader('FormPayments_' & p_web._nocolon('buttonViewCosts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormPayments_jpt:Date_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Date
      else
        do Value::jpt:Date
      end
  of lower('FormPayments_jpt:Payment_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Payment_Type
      else
        do Value::jpt:Payment_Type
      end
  of lower('FormPayments_jpt:Credit_Card_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Credit_Card_Number
      else
        do Value::jpt:Credit_Card_Number
      end
  of lower('FormPayments_jpt:Expiry_Date_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Expiry_Date
      else
        do Value::jpt:Expiry_Date
      end
  of lower('FormPayments_jpt:Issue_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Issue_Number
      else
        do Value::jpt:Issue_Number
      end
  of lower('FormPayments_jpt:Amount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Amount
      else
        do Value::jpt:Amount
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormPayments',0)
  jpt:Ref_Number = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('jpt:Ref_Number',jpt:Ref_Number)
  jpt:Date = Today()
  p_web.SetSessionValue('jpt:Date',jpt:Date)
  jpt:User_Code = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('jpt:User_Code',jpt:User_Code)
  IF (p_web.GSV('PassedType') = 'REFUND')
      jpt:Amount = 0
      Access:JOBPAYMT_ALIAS.ClearKey(jpt_ali:Loan_Deposit_Key)
      jpt_ali:Ref_Number = p_web.GSV('locSearchJobNumber')
      jpt_ali:Loan_Deposit = 1
      SET(jpt_ali:Loan_Deposit_Key,jpt_ali:Loan_Deposit_Key)
      LOOP UNTIL Access:JOBPAYMT_ALIAS.Next()
          IF (jpt_ali:Ref_Number <> p_web.GSV('locSearchJobNumber'))
              BREAK
          END 
          IF (jpt_ali:Loan_Deposit <> 1)
              BREAK
          END
          jpt:Amount += jpt_ali:Amount   
      END
  ELSIF (p_web.GSV('PassedType') = 'DEPOSIT')
      jpt:Amount = 0
  ELSE        
      invoiced# = 0
      IF (p_web.GSV('job:Invoice_Number') > 0)
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
              IF (inv:RRCInvoiceDate <> '')
                  invoiced# = 1
              END
          END
      END
      IF (invoiced#)
          p_web.SSV('TotalPrice:Type','I')
      ELSE
          p_web.SSV('TotalPrice:Type','C')
      END
      TotalPrice(p_web)
      jpt:Amount = p_web.GSV('TotalPrice:Balance')
  END
  
  ! p_web.SSV('jpt:Amount',jpt:Amount)
  
  

PreCopy  Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormPayments',0)
  p_web._PreCopyRecord(JOBPAYMT,jpt:Record_Number_Key)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormPayments:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  ! Delete: Add To Audit
  p_web.SSV('AddToAudit:Type','JOB')
  p_web.SSV('AddToAudit:Action','PAYMENT DETAIL DELETED')
  p_web.SSV('AddToAudit:Notes','PAYMENT AMOUNT: ' & Format(jpt:Amount,@n14.2) &|
      '<13,10>PAYMENT TYPE: ' & Clip(jpt:Payment_Type))
  IF (jpt:Credit_Card_Number <> '')
      p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10>CREDIT CARD NO: ' & Clip(jpt:Credit_Card_Number) & |
          '<13,10>EXPIRY DATE: ' & Clip(jpt:Expiry_Date) & |
          '<13,10>ISSUE NO: ' & Clip(jpt:Issue_Number))
  END
  AddToAudit(p_web)
  p_web.SetSessionValue('FormPayments_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormPayments:Primed',0)
  p_web.setsessionvalue('showtab_FormPayments',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  CASE p_web.GSV('PassedType')
  OF 'DEPOSIT'
      p_web.SSV('AddToAudit:Action','LOAN DESPOSIT')
  OF 'REFUND'
      IF (jpt:Amount > 0)
          jpt:Amount = -(jpt:Amount)
      END
      
      IF (Access:CONTHIST.PrimeRecord() = Level:Benign)
          cht:Ref_Number = p_web.GSV('job:Ref_Number')
          cht:Date = TODAY()
          cht:Time = CLOCK()
          cht:User = p_web.GSV('BookingUsercode')
          cht:Action = 'REFUND MADE ON LOAN UNIT'
          cht:Notes = 'AMOUNT TAKEN: ' & FORMAT(jpt:Amount,@n14.2) & | 
              '<13,10>PAYMENT TYPE: ' & CLIP(jpt:Payment_Type) & '<13,10>'
          IF (Access:CONTHIST.TryUpdate())
          END
          
      END
      p_web.SSV('AddToAudit:Action','REFUND ISSED')
  ELSE
      p_web.SSV('AddToAudit:Action','PAYMENT RECEIVED')
  END  
  
  p_web.SSV('AddToAudit:Type','JOB')
  p_web.SSV('AddToAudit:Notes','PAYMENT TYPE: ' & clip(jpt:Payment_Type) & | 
      '<13,10>AMOUNT: ' & FORMAT(jpt:Amount,@n14.2))
  AddToAudit(p_web)
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormPayments_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  IF (p_web.GSV('PassedType') = '')
      IF (p_web.GSV('jpt:Amount') > p_web.GSV('TotalPrice:Balance'))
          loc:Invalid = 'jpt:Amount'
          loc:Alert = 'This amount exceeds the current outstanding balance.'
          EXIT
      END
  END
  Do DeleteSessionValues
  p_web.DeleteSessionValue('FormPayments_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If jpt:Date = ''
          loc:Invalid = 'jpt:Date'
          loc:alert = p_web.translate('Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          jpt:Date = Upper(jpt:Date)
          p_web.SetSessionValue('jpt:Date',jpt:Date)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If jpt:Payment_Type = ''
          loc:Invalid = 'jpt:Payment_Type'
          loc:alert = p_web.translate('Payment Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:CreditCard') = 1)
        If jpt:Credit_Card_Number = ''
          loc:Invalid = 'jpt:Credit_Card_Number'
          loc:alert = p_web.translate('Credit Card Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          jpt:Credit_Card_Number = Upper(jpt:Credit_Card_Number)
          p_web.SetSessionValue('jpt:Credit_Card_Number',jpt:Credit_Card_Number)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:CreditCard') = 1)
        If jpt:Expiry_Date = ''
          loc:Invalid = 'jpt:Expiry_Date'
          loc:alert = p_web.translate('Expiry Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
        If jpt:Amount = ''
          loc:Invalid = 'jpt:Amount'
          loc:alert = p_web.translate('Payment') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormPayments:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormPayments:Primed',0)
  p_web.StoreValue('')

PostDelete      Routine
MultipleBatchDespatch PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locErrorMessage      STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locAccessoryErrorMessage STRING(255)                       !
locAccessoryPassword STRING(30)                            !
locAuditTrail        STRING(255)                           !
FilesOpened     Long
MULDESPJ::State  USHORT
MULDESP::State  USHORT
LOAN::State  USHORT
EXCHAMF::State  USHORT
COURIER::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('MultipleBatchDespatch')
  loc:formname = 'MultipleBatchDespatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('MultipleBatchDespatch','')
    p_web._DivHeader('MultipleBatchDespatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      ROUTINE
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('locSecurityPackNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('Hide:Accessories',1)
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')
    p_web.SSV('AccessoryConfirmationRequired','')
    p_web.SSV('AccessoriesValidated','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('Hide:AddToBatchButton',1)
    p_web.SSV('Hide:ValidateAccessoriesButton',0)
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locSecurityPackNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('Hide:Accessories')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('Hide:AddToBatchButton')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('UnitValidated')
ValidateUnitDetails         ROUTINE
DATA
txtJobInUse EQUATE('Error! The selected job is in use.')
txtMissingJob       EQUATE('Error! Cannot find the selected job.')
txtDespatchString   EQUATE('Error! The selected job is not ready for despatch.')
txtOutofRegion    EQUATE('Error! The delivery address is outside your region.')
txtNotPaid  EQUATE('Error! The selected job has not been paid.')
txtNotInvoiced      EQUATE('Error! The selected job has not been invoiced.')
txtNoCourier        EQUATE('Error! The selected unit does not have a courier assigned to it.')
txtLoanNotReturned  EQUATE('Error! The loan unit attached to this job has not been returned.')
txtAlreadyInBatch   EQUATE('Error! The selected job is already on a batch.')
txtWrongIMEI        EQUATE('Error! I.M.E.I. Number does not match the selected job.')
txtWrongExchangeIMEI        EQUATE('Error! I.M.E.I. Number does not match the selected job''s Exchange Unit.')
txtWrongLoanIMEI    EQUATE('Error! I.M.E.I. Number does not match the selected job''s Loan Unit.')
txtIndividualOnly   EQUATE('Error! The selected job can only be despatched individually and not as part of a batch.')
txtAccountOnStop    EQUATE('Error! Cannot despatch. The selected account is "on stop".')
locDespatchType     STRING(3)
CODE
    ClearTagFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end
    
    p_web.SSV('locErrorMessage','')
    IF (JobInUse(p_web.GSV('locJobNumber')))
        p_web.SSV('locErrorMessage',txtJobInUse)
            
        EXIT
    END
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage',txtMissingJob)
        EXIT
    END
    
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey)) 
    END
    
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END
    
    
    IF (p_web.GSV('BookingSite') = 'RRC')
        locDespatchType = jobe:DespatchType
        IF (Inlist(jobe:DespatchType,'JOB','EXC','LOA') = 0)
            p_web.SSV('locErrorMessage',txtDespatchString)
            EXIT
        END
        
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            IF (HubOutOfRegion(p_web.GSV('BookingAccount'),sub:Hub) = 1)
                IF (ReleasedForDespatch(job:Ref_Number) = 0)
                    p_web.SSV('locErrorMessage',txtOutOfRegion) 
                    EXIT
                END
            END
        END
        
        
    ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
        locDespatchType = job:Despatch_Type
        IF (InList(job:Despatch_Type,'JOB','EXC','LOA') = 0)
            p_web.SSV('locErrorMessage',txtDespatchString)
            EXIT
        END
        CASE job:Despatch_Type
        OF 'JOB'
            IF (job:Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        OF 'EXC'
            IF (job:Exchange_Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        OF 'LOA'
            IF (job:Loan_Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        END
        
    END ! IF (p_web.GSV('BookingSite') = 'RRC')
    
    ! Job Been Paid/Invoiced?
    IF (job:Chargeable_Job = 'YES')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            IF (tra:Despatch_Paid_Jobs = 'YES' AND NOT JobPaid(job:Ref_Number))
                p_web.SSV('locerrorMessage',txtNotPaid)
                EXIT
            END
            IsJobInvoiced(p_web)
            IF (tra:Despatch_Invoiced_Jobs = 'YES' AND p_web.GSV('IsJobInvoiced') <> 1)
                p_web.SSV('locErrorMessage',txtNotInvoiced)
                EXIT
            END
            
        END
        
    END
    
    ! Have courier attached?
    Access:COURIER.ClearKey(cou:Courier_Key)
    CASE locDespatchType
    OF 'JOB'
        IF (job:Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Courier
    OF 'EXC'
        IF (job:Exchange_Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Exchange_Courier
    OF 'LOA'
        IF (job:Loan_Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Loan_Courier
    END
    IF (Access:COURIER.tryfetch(cou:Courier_Key))
    END
    
    
    !Has the job got a loan attached?
    If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        If job:Loan_Unit_Number <> 0 And locDespatchType = 'JOB'
            p_web.SSV('locErrorMessage',txtLoanNotReturned)
            EXIT
        End !If job:Loan_Unit_Number <> 0
    End !If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1    
    
    IF (vod.IsTheJobAlreadyInBatch(job:Ref_Number,p_web.GSV('BookingAccount')))
        p_web.SSV('locErrorMessage',txtAlreadyInBatch)
        EXIT
    END
    p_web.SSV('AccessoryRefNumber',job:Ref_Number)
            
    ! Check IMEI Number
    CASE locDespatchType
    OF 'JOB'
        IF (p_web.GSV('locIMEINumber') <> job:ESN)
            p_web.SSV('locErrorMessage',txtWrongIMEI)
            EXIT
        END
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
    OF 'EXC'
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage',txtWrongExchangeIMEI)
            EXIT
        ELSE
            IF (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage',txtWrongExchangeIMEI)
                EXIT
            END
            
        END
        
    OF 'LOA'
        Access:LOAN.ClearKey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage',txtWrongLoanIMEI)
            EXIT
        ELSE
            IF (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage',txtWrongLoanIMEI)
                EXIT
            END
            p_web.SSV('AccessoryRefNumber',loa:Ref_Number)
        END
        
    END
    
    ! Check if it has to be indivdual despatch only.
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                    p_web.SSV('locErrorMessage',txtAccountOnStop)
                    EXIT
                End !If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                If sub:UseCustDespAdd = 'YES'
                    p_web.SSV('locErrorMessage',txtIndividualOnly)
                    EXIT
                End !If sub:UseCustDespAdd = 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                    p_web.SSV('locErrorMessage',txtAccountOnStop)
                    EXIT
                End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                If tra:UseCustDespAdd = 'YES'
                    p_web.SSV('locErrorMessage',txtIndividualOnly)
                    EXIT
                End !If tra:UseCustDespAdd = 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        END
    END
    
    
! A Ok
    p_web.SSV('Hide:Accessories',0)
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('DespatchType',locDespatchType)
    p_web.SSV('UnitValidated',1)
    p_web.SSV('ValidateButtonText','Despatch Another Unit')        
    
    p_web.FileToSessionQueue(JOBS)
    p_web.FileToSessionQueue(WEBJOB)
    p_web.FileToSessionQueue(JOBSE)
    p_web.FileToSessionQueue(COURIER)
        
    
    
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHAMF)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHAMF)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SetValue('MultipleBatchDespatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locSecurityPackNumber')
    locSecurityPackNumber = p_web.GetValue('locSecurityPackNumber')
    p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('MultipleBatchDespatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Add To Batch
  IF p_web.IfExistsValue('Action')
      p_web.StoreValue('Action')
      IF p_web.IfExistsValue('BatchNumber')
          p_web.StoreValue('BatchNumber')
          IF (p_web.GSV('Action') = 'AddToBatch')
              ! Start
              IF (p_web.GSV('BatchNumber') > 0)
                  Access:MULDESP.ClearKey(muld:RecordNumberKey)
                  muld:RecordNumber = p_web.GSV('BatchNumber')
                  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
                      IF (p_web.GSV('job:Ref_Number') > 0)
                          ! Check job isn't already there
                          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                          mulj:RefNumber = muld:RecordNumber
                          mulj:JobNumber = p_web.GSV('job:Ref_Number')
                          IF (Access:MULDESPJ.TryFetch(mulj:JobNumberKey))
                              IF (Access:MULDESPJ.PrimeRecord() = Level:Benign)
                                  mulj:RefNumber = muld:RecordNumber
                                  mulj:JobNumber = p_web.GSV('job:Ref_Number')
                                  mulj:IMEINumber = p_web.GSV('job:ESN')
                                  mulj:MSN = p_web.GSV('job:MSN')
                                  mulj:AccountNumber = p_web.GSV('job:Account_Number')
                                  mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
                                  IF (p_web.GSV('BookingSite') = 'RRC')
                                      CASE p_web.GSV('jobe:DespatchType')
                                      OF 'JOB'
                                          mulj:Courier = p_web.GSV('job:Courier')
                                      OF 'EXC'
                                          mulj:Courier = p_web.GSV('job:Exchange_Courier')
                                      OF 'LOA'
                                          mulj:Courier = p_web.GSV('job:Loan_Courier')
                                      END
                  
                                  ELSE
                                      CASE p_web.GSV('job:Despatch_Type')
                                      OF 'JOB'
                                          mulj:Courier = p_web.GSV('job:Courier')
                                      OF 'EXC'
                                          mulj:Courier = p_web.GSV('job:Exchange_Courier')
                                      OF 'LOA'
                                          mulj:Courier = p_web.GSV('job:Loan_Courier')
                                      END
  
                                  END
                                  IF (muld:BatchType = 'TRA')
                                      mulj:AccountNumber = p_web.GSV('job:Account_Number')
                                      mulj:Courier = muld:Courier
                                  END
                                  IF (Access:MULDESPJ.TryInsert() = Level:Benign)
                                      CountBatch# = 0
                                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                                      mulj:RefNumber = muld:RecordNumber
                                      Set(mulj:JobNumberKey,mulj:JobNumberKey)
                                      Loop
                                          If Access:MULDESPJ.NEXT()
                                              Break
                                          End !If
                                          If mulj:RefNumber <> muld:RecordNumber      |
                                              Then Break.  ! End If
                                          CountBatch# += 1
                                      End !Loop
  
                                      muld:BatchTotal = CountBatch#
                                      Access:MULDESP.Update()
                                  ELSE
                                      Access:MULDESPJ.CancelAutoInc()
                                  END
                              END
                          END
                      END
                  END
              END
          END
      END
  END
        
  p_web.DeleteSessionValue('Action')
  p_web.DeleteSessionValue('BatchNumber')
  p_web.DeleteSessionValue('job:Ref_Number')
  
  
  ClearTagFile(p_web)
  DO ClearVariables
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locSecurityPackNumber = p_web.RestoreValue('locSecurityPackNumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferMultipleBatchDespatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('MultipleBatchDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('MultipleBatchDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('MultipleBatchDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="MultipleBatchDespatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="MultipleBatchDespatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="MultipleBatchDespatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Multiple Batch Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Multiple Batch Despatch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_MultipleBatchDespatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_MultipleBatchDespatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_MultipleBatchDespatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Batches In Progress') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Input Batch') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_MultipleBatchDespatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_MultipleBatchDespatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('MultipleBatchDespatch_BrowseBatchesInProgress_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('MultipleBatchDespatch_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_MultipleBatchDespatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Batches In Progress') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseBatchesInProgress
      do Comment::BrowseBatchesInProgress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Input Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSecurityPackNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAddToBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonAddToBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseBatchesInProgress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseBatchesInProgress',p_web.GetValue('NewValue'))
    do Value::BrowseBatchesInProgress
  Else
    p_web.StoreValue('muld:RecordNumber')
  End
  do SendAlert

Value::BrowseBatchesInProgress  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseBatchesInProgress --
  p_web.SetValue('BrowseBatchesInProgress:NoForm',1)
  p_web.SetValue('BrowseBatchesInProgress:FormName',loc:formname)
  p_web.SetValue('BrowseBatchesInProgress:parentIs','Form')
  p_web.SetValue('_parentProc','MultipleBatchDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('MultipleBatchDespatch_BrowseBatchesInProgress_embedded_div')&'"><!-- Net:BrowseBatchesInProgress --></div><13,10>'
    p_web._DivHeader('MultipleBatchDespatch_' & lower('BrowseBatchesInProgress') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('MultipleBatchDespatch_' & lower('BrowseBatchesInProgress') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseBatchesInProgress --><13,10>'
  end
  do SendPacket

Comment::BrowseBatchesInProgress  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('BrowseBatchesInProgress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locJobNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''multiplebatchdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''multiplebatchdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSecurityPackNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Security Pack No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSecurityPackNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSecurityPackNumber',p_web.GetValue('NewValue'))
    locSecurityPackNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSecurityPackNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSecurityPackNumber',p_web.GetValue('Value'))
    locSecurityPackNumber = p_web.GetValue('Value')
  End
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locSecurityPackNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locSecurityPackNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSecurityPackNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSecurityPackNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackNumber'',''multiplebatchdespatch_locsecuritypacknumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSecurityPackNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackNumber',p_web.GetSessionValueFormat('locSecurityPackNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_value')

Comment::locSecurityPackNumber  Routine
      loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonProcessJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonProcessJob',p_web.GetValue('NewValue'))
    do Value::buttonProcessJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  DO ValidateUnitDetails
  do Value::buttonProcessJob
  do SendAlert
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Value::locJobNumber  !1
  do Value::locSecurityPackNumber  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonFailAccessory  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonAddToBatch  !1

Value::buttonProcessJob  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonProcessJob'',''multiplebatchdespatch_buttonprocessjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ProcessJob',p_web.GSV('ValidateButtonText'),'button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_value')

Comment::buttonProcessJob  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('Hide:Accessories') = 1,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','MultipleBatchDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('MultipleBatchDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('MultipleBatchDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('MultipleBatchDespatch_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  p_web.SSV('Hide:AddToBatchButton',1)
  
  ! Validate
  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  AccessoryCheck(p_web)
  Case p_web.GSV('AccessoryCheck:Return')
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end            
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')    
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end                
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('Hide:AddToBatchButton',0)
      !DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::buttonConfirmMismatch  !1
  do Value::buttonFailAccessory  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonAddToBatch  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''multiplebatchdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('NewValue'))
    locAccessoryMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('Value'))
    locAccessoryMessage = p_web.GetValue('Value')
  End

Value::locAccessoryMessage  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1)
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value')

Comment::locAccessoryMessage  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('NewValue'))
    locAccessoryErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('Value'))
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End

Value::locAccessoryErrorMessage  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1)
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value')

Comment::locAccessoryErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmMismatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryErrorMessage','Password Required')    
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryErrorMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
              p_web.SSV('Hide:AddToBatchButton',0)
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      p_web.SSV('Hide:AddToBatchButton',0)
      !DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonAddToBatch  !1
  do Value::buttonFailAccessory  !1
  do Value::buttonProcessJob  !1
  do Value::buttonValidateAccessories  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryPassword  !1

Value::buttonConfirmMismatch  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''multiplebatchdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmAccessoryValidation','Confirm Access. Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'images/tick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value')

Comment::buttonConfirmMismatch  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFailAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFailAccessory',p_web.GetValue('NewValue'))
    do Value::buttonFailAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Fail Accessory Check
  p_web.SSV('GetStatus:StatusNumber',850)
  p_web.SSV('GetStatus:Type',p_web.GSV('DespatchType'))
  GetStatus(p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
      IF (tag:tagged = 1)
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tag:TaggedValue)
      END
  END
  
  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web)
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonAddToBatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonProcessJob  !1
  do Value::buttonValidateAccessories  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryPassword  !1
  do Value::locErrorMessage  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::locSecurityPackNumber  !1

Value::buttonFailAccessory  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''multiplebatchdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FailAccessory','Fail Accessory Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'images/cross.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value')

Comment::buttonFailAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPassword  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt')

Validate::locAccessoryPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('NewValue'))
    locAccessoryPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('Value'))
    locAccessoryPassword = p_web.GetValue('Value')
  End
    locAccessoryPassword = Upper(locAccessoryPassword)
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  do Value::locAccessoryPassword
  do SendAlert

Value::locAccessoryPassword  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locAccessoryPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''multiplebatchdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value')

Comment::locAccessoryPassword  Routine
      loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAddToBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAddToBatch',p_web.GetValue('NewValue'))
    do Value::buttonAddToBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAddToBatch  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_value',Choose(p_web.GSV('Hide:AddToBatchButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:AddToBatchButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddToBatch','Add Job To Batch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormAddToBatch')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_value')

Comment::buttonAddToBatch  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_comment',Choose(p_web.GSV('Hide:AddToBatchButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:AddToBatchButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('MultipleBatchDespatch_BrowseBatchesInProgress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseBatchesInProgress
      else
        do Value::BrowseBatchesInProgress
      end
  of lower('MultipleBatchDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('MultipleBatchDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('MultipleBatchDespatch_locSecurityPackNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackNumber
      else
        do Value::locSecurityPackNumber
      end
  of lower('MultipleBatchDespatch_buttonProcessJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonProcessJob
      else
        do Value::buttonProcessJob
      end
  of lower('MultipleBatchDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('MultipleBatchDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('MultipleBatchDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('MultipleBatchDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)

PreCopy  Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('MultipleBatchDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('MultipleBatchDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
          locAccessoryPassword = Upper(locAccessoryPassword)
          p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locSecurityPackNumber')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')
