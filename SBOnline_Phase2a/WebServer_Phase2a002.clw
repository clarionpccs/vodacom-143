

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A019.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseAuditFilter    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:StatusTypeFilter STRING(3)                             !Status Type Filter
tmp:JOB              STRING('JOB')                         !
tmp:EXC              STRING('EXC')                         !
tmp:2NE              STRING('2NE')                         !
tmp:LOA              STRING('LOA')                         !
FilesOpened     Long
AUDSTATS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BrowseAuditFilter')
  loc:formname = 'BrowseAuditFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BrowseAuditFilter','None')
    p_web._DivHeader('BrowseAuditFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BrowseAuditFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BrowseAuditFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('tmp:StatusTypeFilter','JOB')
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  
  p_web.SetValue('BrowseAuditFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:StatusTypeFilter')
    tmp:StatusTypeFilter = p_web.GetValue('tmp:StatusTypeFilter')
    p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BrowseAuditFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BrowseAuditFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BrowseAuditFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('BrowseAuditFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BrowseAuditFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BrowseAuditFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BrowseAuditFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Audit Trail') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Audit Trail',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BrowseAuditFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BrowseAuditFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BrowseAuditFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Filter') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseAuditFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BrowseAuditFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('BrowseAuditFilter_BrowseAuditTrail_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:StatusTypeFilter')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseAuditFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Filter') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseAuditFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StatusTypeFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseAuditFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browseAuditTrail
      do Comment::browseAuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Audit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StatusTypeFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('NewValue'))
    tmp:StatusTypeFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StatusTypeFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('Value'))
    tmp:StatusTypeFilter = p_web.GetValue('Value')
  End
  do Value::tmp:StatusTypeFilter
  do SendAlert
  do Value::browseAuditTrail  !1

Value::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:StatusTypeFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browseauditfilter_tmp:statustypefilter_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:StatusTypeFilter',loc:fieldclass,loc:readonly,,178,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:StatusTypeFilter') = 0
    p_web.SetSessionValue('tmp:StatusTypeFilter','JOB')
  end
    packet = clip(packet) & p_web.CreateOption('Job','JOB',choose('JOB' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Exchange','EXC',choose('EXC' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Loan','LOA',choose('LOA' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('2nd Exchange','2NE',choose('2NE' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('All','',choose('' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value')

Comment::tmp:StatusTypeFilter  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browseAuditTrail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browseAuditTrail',p_web.GetValue('NewValue'))
    do Value::browseAuditTrail
  Else
    p_web.StoreValue('aud:record_number')
  End

Value::browseAuditTrail  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAuditTrail --
  p_web.SetValue('BrowseAuditTrail:NoForm',1)
  p_web.SetValue('BrowseAuditTrail:FormName',loc:formname)
  p_web.SetValue('BrowseAuditTrail:parentIs','Form')
  p_web.SetValue('_parentProc','BrowseAuditFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('BrowseAuditFilter_BrowseAuditTrail_embedded_div')&'"><!-- Net:BrowseAuditTrail --></div><13,10>'
    p_web._DivHeader('BrowseAuditFilter_' & lower('BrowseAuditTrail') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('BrowseAuditFilter_' & lower('BrowseAuditTrail') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAuditTrail --><13,10>'
  end
  do SendPacket

Comment::browseAuditTrail  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('browseAuditTrail') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BrowseAuditFilter_tmp:StatusTypeFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StatusTypeFilter
      else
        do Value::tmp:StatusTypeFilter
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BrowseAuditFilter',0)

PreCopy  Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BrowseAuditFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BrowseAuditFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BrowseAuditFilter:Primed',0)
  p_web.setsessionvalue('showtab_BrowseAuditFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BrowseAuditFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BrowseAuditFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BrowseAuditFilter:Primed',0)
  p_web.StoreValue('tmp:StatusTypeFilter')
  p_web.StoreValue('')
BrowseAuditTrail     PROCEDURE  (NetWebServerWorker p_web)
tmp:StatusTypeFilter STRING(3)                             !Status Type Filer
tmp:UserName         STRING(60)                            !User Name
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(AUDIT)
                      Project(aud:record_number)
                      Project(aud:Date)
                      Project(aud:Time)
                      Project(aud:Action)
                      Project(aud:Notes)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseAuditTrail')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAuditTrail:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAuditTrail:NoForm')
      loc:FormName = p_web.GetValue('BrowseAuditTrail:FormName')
    else
      loc:FormName = 'BrowseAuditTrail_frm'
    End
    p_web.SSV('BrowseAuditTrail:NoForm',loc:NoForm)
    p_web.SSV('BrowseAuditTrail:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAuditTrail:NoForm')
    loc:FormName = p_web.GSV('BrowseAuditTrail:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAuditTrail') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAuditTrail')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(AUDIT,aud:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'AUD:DATE') then p_web.SetValue('BrowseAuditTrail_sort','1')
    ElsIf (loc:vorder = 'AUD:TIME') then p_web.SetValue('BrowseAuditTrail_sort','2')
    ElsIf (loc:vorder = 'TMP:USERNAME') then p_web.SetValue('BrowseAuditTrail_sort','4')
    ElsIf (loc:vorder = 'AUD:ACTION') then p_web.SetValue('BrowseAuditTrail_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAuditTrail:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAuditTrail:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseAuditTrail:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAuditTrail:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAuditTrail:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Both
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:None
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAuditTrail_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAuditTrail_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'aud:Date','-aud:Date')
    Loc:LocateField = 'aud:Date'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'aud:Time','-aud:Time')
    Loc:LocateField = 'aud:Time'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:UserName','-tmp:UserName')
    Loc:LocateField = 'tmp:UserName'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aud:Action)','-UPPER(aud:Action)')
    Loc:LocateField = 'aud:Action'
  end
  if loc:vorder = ''
    loc:vorder = '-aud:record_number'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
  ElsIf (loc:Parent = 'BrowseAuditFilter')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('aud:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@d6')
  Of upper('aud:Time')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@t1b')
  Of upper('tmp:UserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@s50')
  Of upper('aud:Action')
    loc:SortHeader = p_web.Translate('Action')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@s80')
  Of upper('aud:Notes')
    loc:SortHeader = p_web.Translate('Notes')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@s255')
  End
  If loc:selecting = 1
    loc:selectaction = 'ViewJob'
  End!Else
  loc:formaction = 'BrowseAuditTrail'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAuditTrail:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAuditTrail:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAuditTrail:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="AUDIT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="aud:Record_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditTrail',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseAuditTrail',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAuditTrail.locate(''Locator2BrowseAuditTrail'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAuditTrail.cl(''BrowseAuditTrail'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseAuditTrail_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseAuditTrail_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseAuditTrail','Date','Click here to sort by Date Changed',,,50,1)
        Else
          packet = clip(packet) & '<th width="'&clip(50)&'" Title="'&p_web.Translate('Click here to sort by Date Changed')&'">'&p_web.Translate('Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseAuditTrail','Time','Click here to sort by Time Changed',,,50,1)
        Else
          packet = clip(packet) & '<th width="'&clip(50)&'" Title="'&p_web.Translate('Click here to sort by Time Changed')&'">'&p_web.Translate('Time')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseAuditTrail','Username',,,'LeftJustify',150,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(150)&'">'&p_web.Translate('Username')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseAuditTrail','Action','Click here to sort by Action',,'LeftJustify',300,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(300)&'" Title="'&p_web.Translate('Click here to sort by Action')&'">'&p_web.Translate('Action')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th class="'&clip('SmallText')&'" width="'&clip(350)&'" Title="'&p_web.Translate('Click here to sort by New Status')&'">'&p_web.Translate('Notes')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'aud:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('aud:record_number',lower(Thisview{prop:order}),1,1) = 0 !and AUDIT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'aud:record_number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('aud:record_number'),p_web.GetValue('aud:record_number'),p_web.GetSessionValue('aud:record_number'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
      loc:FilterWas = 'aud:Ref_Number = ' & p_web.GSV('job:Ref_Number') & ' AND Upper(aud:Type) = Upper(''' & p_web.GSV('tmp:StatusTypeFilter') & ''')'
  ElsIf (loc:Parent = 'BrowseAuditFilter')
      loc:FilterWas = 'aud:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditTrail',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAuditTrail_Filter')
    p_web.SetSessionValue('BrowseAuditTrail_FirstValue','')
    p_web.SetSessionValue('BrowseAuditTrail_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,AUDIT,aud:Record_Number_Key,loc:PageRows,'BrowseAuditTrail',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If AUDIT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(AUDIT,loc:firstvalue)
              Reset(ThisView,AUDIT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If AUDIT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(AUDIT,loc:lastvalue)
            Reset(ThisView,AUDIT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(aud:record_number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAuditTrail.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAuditTrail.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAuditTrail.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAuditTrail.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditTrail',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAuditTrail_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAuditTrail_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseAuditTrail',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAuditTrail.locate(''Locator1BrowseAuditTrail'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAuditTrail.cl(''BrowseAuditTrail'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseAuditTrail_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAuditTrail_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAuditTrail.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAuditTrail.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAuditTrail.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAuditTrail.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = aud:User
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        tmp:UserName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
    Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        tmp:UserName = '* Unknown User Code: ' & Clip(aus:UserCode) & ' *'
    End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    
    loc:field = aud:record_number
    p_web._thisrow = p_web._nocolon('aud:record_number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAuditTrail:LookupField')) = aud:record_number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((aud:record_number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseAuditTrail.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If AUDIT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(AUDIT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If AUDIT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(AUDIT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aud:record_number',clip(loc:field),,loc:checked,,,'onclick="BrowseAuditTrail.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aud:record_number',clip(loc:field),,'checked',,,'onclick="BrowseAuditTrail.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(50)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(50)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Time
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(150)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:UserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(300)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Action
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&clip('SmallText')&'" width="'&clip(350)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If Loc:Eip = 0
        do StartRowHTML
      End
      If(loc:SelectionMethod  = Net:Radio)
        packet = clip(packet) & '<td width="1">&#160;</td>' ! first column is the select column
      End
      loc:RowsIn += 1
      if loc:RowCount = 1 then loc:RowsHigh += 1.
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseAuditTrail.omv(this);" onMouseOut="BrowseAuditTrail.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseAuditTrail=new browseTable(''BrowseAuditTrail'','''&clip(loc:formname)&''','''&p_web._jsok('aud:record_number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('aud:record_number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseAuditTrail.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseAuditTrail.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAuditTrail')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAuditTrail')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAuditTrail')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAuditTrail')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(AUDIT)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(AUDIT)
  Bind(aud:Record)
  Clear(aud:Record)
  NetWebSetSessionPics(p_web,AUDIT)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('aud:record_number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::aud:Date   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Date_'&aud:record_number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Date,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aud:Time   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Time_'&aud:record_number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Time,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:UserName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:UserName_'&aud:record_number,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:UserName,'@s50')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aud:Action   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Action_'&aud:record_number,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Action,'@s80')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aud:Notes   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Notes_'&aud:record_number,'SmallText',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Notes,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = aud:record_number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('aud:record_number',aud:record_number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('aud:record_number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aud:record_number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aud:record_number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
AddToStockAllocation PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
STOCKALL::State  USHORT
STOCK::State  USHORT
FilesOpened     BYTE(0)
  CODE
    !p_web.SSV('AddToStockAllocation:Type','WAR')
    !p_web.SSV('AddToStockAllocation:Status','STatus?')
    !p_web.SSV('AddToSTockAllocation:Qty',?)
    
    do openFiles
    do saveFiles
    Access:STOCKALL.Clearkey(stl:PartRecordNumberKey)
    stl:Location = p_web.GSV('BookingSiteLocation')
    stl:PartType = p_web.GSV('AddToStockAllocation:Type')
    Case stl:PartType
    of 'CHA'
        stl:PartRecordNumber = p_web.GSV('par:Record_Number')
                
        IF (p_web.GSV('par:Correction') = 1)
            DO ExitProc
                
        END
        
    of 'WAR'
        stl:PartRecordNumber = p_web.GSV('wpr:Record_Number') 
                    
        IF (p_web.GSV('wpr:Correction') = 1)
            DO ExitProc
        END 
                
    of 'EST'
        stl:PartRecordNumber = p_web.GSV('epr:Record_Number')
        IF (p_web.GSV('epr:Correction') = 1)
            DO ExitProc
        END 
                
    end
    if (Access:STOCKALL.TryFetch(stl:PartRecordNumberKey) = Level:Benign)
        Do WriteRecord
    else
        If (Access:STOCKALL.PrimeRecord() = Level:Benign)
            Do WriteRecord
            if (Access:STOCKALL.TryInsert())
            End
        End
    end
    
    DO ExitProc
              
ExitProc            ROUTINE
    do restoreFiles
    do closeFiles
            
    p_web.DeleteSessionValue('AddToStockAllocation:Type')
    p_web.DeleteSessionValue('AddToStockAllocation:Status')
    p_web.DeleteSessionValue('AddToStockAllocation:Qty')
WriteRecord         Routine
    stl:Location = p_web.GSV('BookingSiteLocation')
    stl:PartType = p_web.GSV('AddToStockAllocation:Type')
    stl:Status  = p_web.GSV('AddToStockAllocation:Status')
    stl:Engineer = p_web.GSV('job:Engineer')
    Case stl:PartType
    of 'CHA'
        stl:PartNumber = p_web.GSV('par:Part_Number')
        stl:Description = p_web.GSV('par:Description')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('par:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            stl:PartRefNumber = sto:Ref_Number
            stl:ShelfLocation = sto:Shelf_Location
        end
        stl:JobNumber = p_web.GSV('par:Ref_Number')
        stl:PartRecordNumber = p_web.GSV('par:Record_Number')
        stl:Quantity = p_web.GSV('AddToSTockAllocation:Qty')

    of 'WAR'
        stl:PartNumber = p_web.GSV('wpr:Part_Number')
        stl:Description = p_web.GSV('wpr:Description')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('wpr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            stl:PartRefNumber = sto:Ref_Number
            stl:ShelfLocation = sto:Shelf_Location
        end
        stl:JobNumber = p_web.GSV('wpr:Ref_Number')
        stl:PartRecordNumber = p_web.GSV('wpr:Record_Number')
        stl:Quantity = p_web.GSV('AddToStockAllocation:Qty')
    of 'EST'
        stl:PartNumber = p_web.GSV('epr:Part_Number')
        stl:Description = p_web.GSV('epr:Description')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('epr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            stl:PartRefNumber = sto:Ref_Number
            stl:ShelfLocation = sto:Shelf_Location
        end
        stl:JobNumber = p_web.GSV('epr:Ref_Number')
        stl:PartRecordNumber = p_web.GSV('epr:Record_Number')
        stl:Quantity = p_web.GSV('AddToStockAllocation:Qty')
    End
SaveFiles  ROUTINE
  STOCKALL::State = Access:STOCKALL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  STOCK::State = Access:STOCK.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF STOCKALL::State <> 0
    Access:STOCKALL.RestoreFile(STOCKALL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STOCK::State <> 0
    Access:STOCK.RestoreFile(STOCK::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKALL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKALL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKALL.Close
     Access:STOCK.Close
     FilesOpened = False
  END
FormChargeableParts  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:Location         STRING(30)                            !
tmp:ShelfLocation    STRING(30)                            !
tmp:SecondLocation   STRING(30)                            !
tmp:PurchaseCost     REAL                                  !
tmp:OutWarrantyCost  REAL                                  !
tmp:OutWarrantyMarkup REAL                                 !
tmp:ARCPart          BYTE                                  !
tmp:FaultCodesChecked BYTE                                 !
tmp:FaultCodes2      STRING(30)                            !
tmp:FaultCodes4      STRING(30)                            !
tmp:FaultCodes5      STRING(30)                            !
tmp:FaultCodes6      STRING(30)                            !
tmp:FaultCodes7      STRING(30)                            !
tmp:FaultCodes8      STRING(30)                            !
tmp:FaultCodes9      STRING(30)                            !
tmp:FaultCodes10     STRING(30)                            !
tmp:FaultCodes11     STRING(30)                            !
tmp:FaultCodes12     STRING(30)                            !
tmp:FaultCodes3      STRING(30)                            !
tmp:FaultCodes1      STRING(30)                            !
tmp:CreateOrder      BYTE                                  !
tmp:UnallocatePart   BYTE                                  !
locUserCode          STRING(3)                             !
FilesOpened     Long
PARTS::State  USHORT
STOMODEL::State  USHORT
STOCK::State  USHORT
SUPPLIER::State  USHORT
LOCATION::State  USHORT
CHARTYPE::State  USHORT
MANFAUPA::State  USHORT
MANFAULT::State  USHORT
MANFPALO::State  USHORT
DEFAULTS::State  USHORT
PARTS_ALIAS::State  USHORT
STOHIST::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       Class
AfterFaultCodeLookup Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormChargeableParts')
  loc:formname = 'FormChargeableParts_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormChargeableParts','')
    p_web._DivHeader('FormChargeableParts',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
buildFaultCodes    Routine
data
locFoundFault      Byte(0)
code
    ! Clear Variables
    if (p_web.GSV('FormChargeableParts:FirstTime') = 0)
        loop x# = 1 To 12
            p_web.SSV('Hide:PartFaultCode' & x#,1)
            p_web.SSV('Req:PartFaultCode' & x#,0)
            p_web.SSV('ReadOnly:PartFaultCode' & x#,0)
            p_web.SSV('Prompt:PartFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:PartFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:PartFaultCode' & x#,0)
            p_web.SSV('Lookup:PartFaultCode' & x#,0)
            p_web.SSV('Comment:PartFaultCode' & x#,'')
        end ! loop x# = 1 To 12
        p_web.SSV('Hide:FaultCodesChecked',1)
    end ! if (p_web.GSV('FormChargeableParts:FirstTime') = 0)

    locMainFaultOnly# = 0
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
            !Main Fault Only
            locMainFaultOnly# = 1
        end ! if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:ScreenOrder    = 0
    set(map:ScreenOrderKey,map:ScreenOrderKey)
    loop
        if (Access:MANFAUPA.Next())
            Break
        end ! if (Access:MANFAUPA.Next())
        if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))

        if (map:ScreenOrder = 0)
            cycle
        end ! if (map:ScreenOrder = 0)

        if (locMainFaultOnly# = 1)
            if (map:MainFault = 0)
                cycle
            end ! if (map:MainFault = 0)
        end ! if (locMainFaultOnly# = 1)

        p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,0)
        p_web.SSV('Prompt:PartFaultCode' & map:ScreenOrder,map:Field_Name)

        if (map:Compulsory = 'YES')
            if (p_web.GSV('par:Adjustment') = 'YES')
                if (map:CompulsoryForAdjustment)
                    p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
                    p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
                end ! if (map:CompulsoryForAdjustment)
            else !if (p_web.GSV('par:Adjustment') = 'YES')
                p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
                p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
            end ! if (p_web.GSV('par:Adjustment') = 'YES')
        else ! if (map:Compulsory = 'YES')
            p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,0)
        end ! if (map:Compulsory = 'YES')

        if (map:MainFault)
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
                case maf:Field_Type
                of 'DATE'
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(maf:DateType))
                    ! Date Lookup Required
                of 'STRING'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & maf:LengthTo)
                    end !~ if (maf:RestrictLength)

                    ! Lookup Required
                of 'NUMBER'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & maf:LengthTo)
                    else !end !~ if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                    end !~ if (maf:RestrictLength)
                end ! case maf:Field_Type

                if (maf:Lookup = 'YES')
                    p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)
                end ! if (map:Lookup = 'YES')
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
        else ! if (map:MainFault)
            case map:Field_Type
            of 'DATE'
                p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(map:DateType))
                p_web.SSV('ShowDate:PartFaultCode' & map:ScreenOrder,1)
                ! Date Lookup Required
            of 'STRING'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & map:LengthTo)
                end !~ if (maf:RestrictLength)

                ! Lookup Required
            of 'NUMBER'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & map:LengthTo)
                else !end !~ if (maf:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                end !~ if (maf:RestrictLength)
            end ! case maf:Field_Type
        end !if (map:MainFault)

        if (map:Lookup = 'YES')
            p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)

        end ! if (map:Lookup = 'YES')

        if (map:NotAvailable = 1)
            if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,1)
            else ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('ReadOnly:PartFaultCode' & map:ScreenOrder,1)
            end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
        end ! if (map:NotAvailable = 1)

        if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')
            if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('job:Fault_Code' & map:CopyJobFaultCode))
            else ! if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('wob:FaultCode' & map:CopyJobFaultCode))
            end ! if (map:ScreenOrder < 13)
        end ! if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')

        if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Manufacturer    = p_web.GSV('job:Manufacturer')
            stm:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Found
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode1)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode2)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode3)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode4)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode5)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode6)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode7)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode8)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode9)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode10)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode11)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode12)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            else ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
        locFoundFault = 1
    end ! loop

    !Check if the part main fault is set to assign value to anouther fault code

    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
        mfp:Field_Number    = map:Field_Number
        mfp:Field    = p_web.GSV('tmp:FaultCode' & map:ScreenOrder)
        if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Found
            if (mfp:SetPartFaultCode)
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer    = p_web.GSV('job:Manufacturer')
                map:Field_Number    = mfp:SelectPartFaultCode
                if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                       p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                       p_web.SSV('tmp:FaultCode' & map:ScreenOrder,mfp:PartFaultCodeValue)
                    end ! if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                else ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
            end ! if (mfp:SetPartFaultCode)
        else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Error
        end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    if (locFoundFault)
        p_web.SSV('Hide:FaultCodesChecked',0)
    end ! if (locFoundFault)
deleteSessionValues     Routine
    p_web.deleteSessionValue('adjustment')
    p_web.deleteSessionValue('FormChargeableParts:FirstTime')
    p_web.deleteSessionValue('locOrderRequired')
    p_web.deleteSessionValue('locOutFaultCode')

    p_web.deleteSessionValue('ReadOnly:OutWarrantyMarkup')
    p_web.deleteSessionValue('ReadOnly:PurchaseCost')
    p_web.deleteSessionValue('ReadOnly:OutWarrantyCost')
    p_web.deleteSessionValue('ReadOnly:Quantity')

    p_web.deleteSessionValue('Parts:ViewOnly')

    p_web.deleteSessionValue('Show:UnallocatePart')
enableDisableCosts    Routine
    p_web.SSV('tmp:FixedPrice','')
    if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',0)
    else ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',1)
    end ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)

    if (p_web.GSV('job:Invoice_Number') = 0)
        if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
            p_web.SSV('tmp:InWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:InWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:InWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
        if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
            p_web.SSV('tmp:OutWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:OutWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:OutWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
    end ! if (p_web.GSV('job:Invoice_Number') = 0)

    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type    = p_web.GSV('job:Charge_Type')
    if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Found
        if (cha:Zero_Parts_ARC)
           if (p_web.GSV('tmp:ARCPart') = 1)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts_ARC)

        if (cha:Zero_Parts = 'YES')
           if (p_web.GSV('tmp:ARCPart') = 0)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts = 'YES')
    else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Error
    end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
lookupLocation    Routine
    if (p_web.GSV('par:Part_Ref_Number') <> '')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number    = p_web.GSV('par:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Found
            p_web.SSV('tmp:Location',sto:Location)
            p_web.SSV('tmp:ShelfLocation',sto:Shelf_Location)
            p_web.SSV('tmp:SecondLocation',sto:Second_Location)            
        else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Error
            p_web.SSV('tmp:Location','')
            p_web.SSV('tmp:ShelfLocation','')
            p_web.SSV('tmp:SecondLocation','')
        end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    else ! if (p_web.GSV('par:Part_Ref_Number') <> '')
        p_web.SSV('tmp:Location','')
        p_web.SSV('tmp:ShelfLocation','')
        p_web.SSV('tmp:SecondLocation','')
    end ! if (p_web.GSV('par:Part_Ref_Number') <> '')
lookupMainFault  Routine
    p_web.SSV('locOutFaultCode','')
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFAULT.Clearkey(maf:MainFaultKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:MainFault    = 1
        if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Found

            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCodes' & map:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Found
                p_web.SSV('locOutFaultCode','Out Fault Code: ' & mfo:Description)
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

        else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)

    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
showCosts      Routine
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = p_web.GSV('par:Part_Ref_Number')
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
        if (sto:Location <> p_web.GSV('ARC:SiteLocation'))
            p_web.SSV('tmp:ARCPart',0)
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND RRC PART'))
                    p_web.SSV('Parts:ViewOnly',1)
                end ! if (SecurityCheckFailed(p_web.GSV('BookingUser'),'AMEND RRC PART'))
            end ! if (p_web.GSV('BookingSite') <> 'RRC')
        else ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
            p_web.SSV('tmp:ARCPart',1)
            if (p_web.GSV('BookingSite') <> 'ARC')
                p_web.SSV('Parts:ViewOnly',1)
            end ! if (p_web.GSV('BookingSite') <> 'ARC')
        end ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    if (p_web.GSV('tmp:ARCPart') = 1)
        p_web.SSV('tmp:PurchaseCost',p_web.GSV('par:AveragePurchaseCost'))
        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('par:Purchase_Cost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('par:Sale_Cost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('par:InWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('par:OutWarrantyMarkup'))
    else ! if (tmp:ARCPart)
        if (p_web.GSV('par:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('par:RRCAveragePurchaseCost'))
        else ! if (p_web.GSV('par:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('par:RRCPurchaseCost'))
        end ! if (p_web.GSV('par:RRCAveragePurchaseCost') > 0)
        
        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('par:RRCPurchaseCost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('par:RRCSaleCost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('par:RRCInWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('par:RRCOutWarrantyMarkup'))
    end !if (tmp:ARCPart)
UpdateComments    Routine
    loop x# = 1 to 12
        if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
            cycle
        end ! if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        map:ScreenOrder    = x#
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
            if (map:MainFault)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf:MainFault    = 1
                if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Found
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:PartFaultCode' & x#,mfo:Description)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:PartFaultCode' & x#,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            else ! if (map:MainFault)
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
                mfp:Field_Number    = map:Field_Number
                mfp:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Found
                    p_web.SSV('Comment:PartFaultCode' & x#,mfp:Description)
                else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Error
                    p_web.SSV('Comment:PartFaultCode' & x#,'')
                end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            end ! if (map:MainFault)
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
    end ! loop x# = 1 to 12


updatePartDetails      routine
! Update Part Details
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = stm:Ref_Number
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)

    Access:LOCATION.Clearkey(loc:Location_Key)
    loc:Location    = sto:Location
    if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    p_web.SSV('par:Description',stm:Description)
    p_web.SSV('par:Part_Ref_Number',stm:Ref_Number)
    p_web.SSV('par:Supplier',sto:Supplier)
    p_web.SSV('par:Purchase_Cost',sto:Purchase_Cost)
    p_web.SSV('par:Sale_Cost',sto:Sale_Cost)
    p_web.SSV('par:Retail_Cost',sto:Retail_Cost)
    p_web.SSV('par:InWarrantyMarkup',sto:PurchaseMarkup)
    p_web.SSV('par:OutWarrantyMarkup',sto:Percentage_Mark_Up)

    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('par:RRCAveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('par:PurchaseCost',sto:Purchase_Cost)
        p_web.SSV('par:RRCSaleCost',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts = 'YES')
                p_web.SSV('par:RRCSaleCost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('par:RRCInWarrantyMarkup',sto:PurchaseMarkUp)
        p_web.SSV('par:RRCOutWarrantyMarkup',sto:Percentage_Mark_Up)
        p_web.SSV('par:Purchase_Cost',par:RRCPurchaseCost)
        p_web.SSV('par:Sale_Cost',par:RRCSaleCost)
        p_web.SSV('par:AveragePurchaseCost',par:RRCAveragePurchaseCost)
    end ! if (p_web.GSV('BookingSite') = 'RRC')

    if (p_web.GSV('BookingSite') = 'ARC')
        p_web.SSV('par:AveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('par:Purchase_Cost',sto:Purchase_Cost)
        p_web.SSV('par:Sale_Code',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts_ARC)
                p_web.SSV('par:Sale_Cost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('par:RRCPurchaseCost',0)
        p_web.SSV('par:RRCSaleCost',0)

        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('par:RRCAveragePurchaseCost',par:Sale_Cost)
            p_web.SSV('par:RRCPurchaseCost',VodacomClass.Markup(p_web.GSV('par:RRCPurchaseCost'),|
                                                p_web.GSV('par:RRCAveragePurchaseCost'),|
                                                InWarrantyMarkup(p_web.GSV('job:Manufacturer'),|
                                                sto:Location)))
            p_web.SSV('par:RRCSaleCost',VodacomClass.Markup(p_web.GSV('par:RRCSaleCost'),|
                                            p_web.GSV('par:RRCAveragePurchaseCost'),|
                                            loc:OutWarrantyMarkup))
            p_web.SSV('par:RRCInWarrantyMarkup',p_web.GSV('par:InWarrantyMarkup'))
            p_web.SSV('par:RRCOutWarrantyMarkup',p_web.GSV('par:OutWarrantyMarkup'))
        end ! if (p_web.GSV('jobe:WebJob') = 1)
    end ! if (p_web.GSV('BookingSite') = 'ARC')

    if (sto:Assign_Fault_Codes = 'YES')
        p_web.SSV('tmp:FaultCode1',stm:FaultCode1)
        p_web.SSV('tmp:FaultCode2',stm:FaultCode2)
        p_web.SSV('tmp:FaultCode3',stm:FaultCode3)
        p_web.SSV('tmp:FaultCode4',stm:FaultCode4)
        p_web.SSV('tmp:FaultCode5',stm:FaultCode5)
        p_web.SSV('tmp:FaultCode6',stm:FaultCode6)
        p_web.SSV('tmp:FaultCode7',stm:FaultCode7)
        p_web.SSV('tmp:FaultCode8',stm:FaultCode8)
        p_web.SSV('tmp:FaultCode9',stm:FaultCode9)
        p_web.SSV('tmp:FaultCode10',stm:FaultCode10)
        p_web.SSV('tmp:FaultCode11',stm:FaultCode11)
        p_web.SSV('tmp:FaultCode12',stm:FaultCode12)
    end ! if (sto:Assign_Fault_Codes = 'YES')

    p_web.SSV('par:Part_Ref_Number',sto:Ref_Number)
    p_web.SSV('locOrderRequired',0)

    do ShowCosts
    do lookupLocation
    do enableDisableCosts
OpenFiles  ROUTINE
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(PARTS_ALIAS)
  p_web._OpenFile(STOHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(PARTS_ALIAS)
  p_Web._CloseFile(STOHIST)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !Initialize
      do buildFaultCodes
  
      p_web.SSV('Comment:OutWarrantyMarkup','')
      p_web.SSV('Comment:PartNumber','Required')
  p_web.SetValue('FormChargeableParts_form:inited_',1)
  p_web.SetValue('UpdateFile','PARTS')
  p_web.SetValue('UpdateKey','par:recordnumberkey')
  p_web.SetValue('IDField','par:Record_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormChargeableParts:Primed') = 1
    p_web._deleteFile(PARTS)
    p_web.SetSessionValue('FormChargeableParts:Primed',0)
  End
      do deleteSessionValues
      

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','PARTS')
  p_web.SetValue('UpdateKey','par:recordnumberkey')
  If p_web.IfExistsValue('par:Order_Number')
    p_web.SetPicture('par:Order_Number','@n08b')
  End
  p_web.SetSessionPicture('par:Order_Number','@n08b')
  If p_web.IfExistsValue('par:Date_Ordered')
    p_web.SetPicture('par:Date_Ordered','@d6b')
  End
  p_web.SetSessionPicture('par:Date_Ordered','@d6b')
  If p_web.IfExistsValue('par:Date_Received')
    p_web.SetPicture('par:Date_Received','@d6b')
  End
  p_web.SetSessionPicture('par:Date_Received','@d6b')
  If p_web.IfExistsValue('par:Part_Number')
    p_web.SetPicture('par:Part_Number','@s30')
  End
  p_web.SetSessionPicture('par:Part_Number','@s30')
  If p_web.IfExistsValue('par:Description')
    p_web.SetPicture('par:Description','@s30')
  End
  p_web.SetSessionPicture('par:Description','@s30')
  If p_web.IfExistsValue('par:Despatch_Note_Number')
    p_web.SetPicture('par:Despatch_Note_Number','@s30')
  End
  p_web.SetSessionPicture('par:Despatch_Note_Number','@s30')
  If p_web.IfExistsValue('par:Quantity')
    p_web.SetPicture('par:Quantity','@n4')
  End
  p_web.SetSessionPicture('par:Quantity','@n4')
  If p_web.IfExistsValue('tmp:PurchaseCost')
    p_web.SetPicture('tmp:PurchaseCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:PurchaseCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyCost')
    p_web.SetPicture('tmp:OutWarrantyCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    p_web.SetPicture('tmp:OutWarrantyMarkup','@n3')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyMarkup','@n3')
  If p_web.IfExistsValue('par:Supplier')
    p_web.SetPicture('par:Supplier','@s30')
  End
  p_web.SetSessionPicture('par:Supplier','@s30')
  If p_web.IfExistsValue('tmp:FaultCodes1')
    p_web.SetPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCodes2')
    p_web.SetPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCodes3')
    p_web.SetPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCodes4')
    p_web.SetPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCodes5')
    p_web.SetPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCodes6')
    p_web.SetPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCodes7')
    p_web.SetPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCodes8')
    p_web.SetPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCodes9')
    p_web.SetPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCodes10')
    p_web.SetPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCodes11')
    p_web.SetPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCodes12')
    p_web.SetPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
AfterLookup Routine
  loc:TabNumber = -1
  If loc:act = ChangeRecord
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'par:Part_Number'
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOMODEL)
      ! After Lookup
      ! After Lookup Assignments
      do updatePartDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.par:Description')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
      
  
  Of 'par:Supplier'
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUPPLIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.par:Exclude_From_Order')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
      
  
  End
  If p_web.GSV('locOrderRequired') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:Location',tmp:Location)
  p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tmp:Location')
    tmp:Location = p_web.GetValue('tmp:Location')
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  End
  if p_web.IfExistsValue('tmp:SecondLocation')
    tmp:SecondLocation = p_web.GetValue('tmp:SecondLocation')
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  End
  if p_web.IfExistsValue('tmp:ShelfLocation')
    tmp:ShelfLocation = p_web.GetValue('tmp:ShelfLocation')
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  End
  if p_web.IfExistsValue('tmp:PurchaseCost')
    tmp:PurchaseCost = p_web.GetValue('tmp:PurchaseCost')
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost')
    tmp:OutWarrantyCost = p_web.GetValue('tmp:OutWarrantyCost')
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    tmp:OutWarrantyMarkup = p_web.GetValue('tmp:OutWarrantyMarkup')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  End
  if p_web.IfExistsValue('tmp:UnallocatePart')
    tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  End
  if p_web.IfExistsValue('tmp:CreateOrder')
    tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked')
    tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  End
  if p_web.IfExistsValue('tmp:FaultCodes1')
    tmp:FaultCodes1 = p_web.GetValue('tmp:FaultCodes1')
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  End
  if p_web.IfExistsValue('tmp:FaultCodes2')
    tmp:FaultCodes2 = p_web.GetValue('tmp:FaultCodes2')
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  End
  if p_web.IfExistsValue('tmp:FaultCodes3')
    tmp:FaultCodes3 = p_web.GetValue('tmp:FaultCodes3')
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  End
  if p_web.IfExistsValue('tmp:FaultCodes4')
    tmp:FaultCodes4 = p_web.GetValue('tmp:FaultCodes4')
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  End
  if p_web.IfExistsValue('tmp:FaultCodes5')
    tmp:FaultCodes5 = p_web.GetValue('tmp:FaultCodes5')
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  End
  if p_web.IfExistsValue('tmp:FaultCodes6')
    tmp:FaultCodes6 = p_web.GetValue('tmp:FaultCodes6')
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  End
  if p_web.IfExistsValue('tmp:FaultCodes7')
    tmp:FaultCodes7 = p_web.GetValue('tmp:FaultCodes7')
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  End
  if p_web.IfExistsValue('tmp:FaultCodes8')
    tmp:FaultCodes8 = p_web.GetValue('tmp:FaultCodes8')
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  End
  if p_web.IfExistsValue('tmp:FaultCodes9')
    tmp:FaultCodes9 = p_web.GetValue('tmp:FaultCodes9')
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  End
  if p_web.IfExistsValue('tmp:FaultCodes10')
    tmp:FaultCodes10 = p_web.GetValue('tmp:FaultCodes10')
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  End
  if p_web.IfExistsValue('tmp:FaultCodes11')
    tmp:FaultCodes11 = p_web.GetValue('tmp:FaultCodes11')
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  End
  if p_web.IfExistsValue('tmp:FaultCodes12')
    tmp:FaultCodes12 = p_web.GetValue('tmp:FaultCodes12')
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormChargeableParts_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('FormChargeableParts:FirstTime') = 0)
          !Write Fault Codes
          p_web.SSV('par:Fault_Code1',par:Fault_Code1)
          p_web.SSV('par:Fault_Code2',par:Fault_Code2)
          p_web.SSV('par:Fault_Code3',par:Fault_Code3)
          p_web.SSV('par:Fault_Code4',par:Fault_Code4)
          p_web.SSV('par:Fault_Code5',par:Fault_Code5)
          p_web.SSV('par:Fault_Code6',par:Fault_Code6)
          p_web.SSV('par:Fault_Code7',par:Fault_Code7)
          p_web.SSV('par:Fault_Code8',par:Fault_Code8)
          p_web.SSV('par:Fault_Code9',par:Fault_Code9)
          p_web.SSV('par:Fault_Code10',par:Fault_Code10)
          p_web.SSV('par:Fault_Code11',par:Fault_Code11)
          p_web.SSV('par:Fault_Code12',par:Fault_Code12)
  
  
          Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
          map:Manufacturer    = p_web.GSV('job:Manufacturer')
          map:ScreenOrder    = 0
          set(map:ScreenOrderKey,map:ScreenOrderKey)
          loop
              if (Access:MANFAUPA.Next())
                  Break
              end ! if (Access:MANFAUPA.Next())
              if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (map:ScreenOrder    = 0)
                  cycle
              end ! if (map:ScreenOrder    <> 0)
  
              p_web.SSV('tmp:FaultCodes' & map:ScreenOrder,p_web.GSV('par:Fault_Code' & map:Field_Number))
          end ! loop
  
  !        loop x# = 1 To 12
  !            p_web.SSV('tmp:FaultCodes' & x#,p_web.GSV('par:Fault_Code' & x#))
  !            linePrint('tmp:FaultCode' & x# & ' - ' & p_web.GSV('tmp:FaultCode' & x#),'c:\log.log')
  !        end ! loop x# = 1 To 12
          p_web.SSV('FormChargeableParts:FirstTime',1)
          !p_web.SSV('locOrderRequired',0)
  
          ! Save For Later
          p_web.SSV('Save:Quantity',par:Quantity)
      end ! if (p_web.GSV('FormChargeableParts:FirstTime',0))
      do updateComments
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:Location = p_web.RestoreValue('tmp:Location')
 tmp:SecondLocation = p_web.RestoreValue('tmp:SecondLocation')
 tmp:ShelfLocation = p_web.RestoreValue('tmp:ShelfLocation')
 tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
 tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
 tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
 tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
 tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
 tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
 tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
 tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
 tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
 tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
 tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
 tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
 tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
 tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
 tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
 tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
 tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
 tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormChargeableParts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormChargeableParts_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormChargeableParts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Parts:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="PARTS__FileAction" value="'&p_web.getSessionValue('PARTS:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="PARTS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="PARTS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="par:recordnumberkey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormChargeableParts" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormChargeableParts" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormChargeableParts" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','par:Record_Number',p_web._jsok(p_web.getSessionValue('par:Record_Number'))) & '<13,10>'
  If p_web.Translate('Insert / Amend Chargeable Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Insert / Amend Chargeable Parts',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormChargeableParts">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormChargeableParts" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormChargeableParts')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If loc:act = ChangeRecord
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Status') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        If p_web.GSV('locOrderRequired') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Order Required') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormChargeableParts')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormChargeableParts'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STOMODEL'
            p_web.SetValue('SelectField',clip(loc:formname) & '.par:Description')
    End
    If upper(p_web.getvalue('LookupFile'))='SUPPLIER'
        If p_web.GSV('Hide:PartFaultCode1') <> 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes1')
        End
    End
  Else
    If False
    ElsIf loc:act = ChangeRecord
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.par:Part_Number')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If loc:act = ChangeRecord
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('locOrderRequired') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormChargeableParts')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if loc:act = ChangeRecord
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('locOrderRequired') = 1
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If loc:act = ChangeRecord
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Status') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChargeableParts_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Order_Number
      do Value::par:Order_Number
      do Comment::par:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Date_Ordered
      do Value::par:Date_Ordered
      do Comment::par:Date_Ordered
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Date_Received
      do Value::par:Date_Received
      do Comment::par:Date_Received
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locOutFaultCode
      do Comment::locOutFaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChargeableParts_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Despatch_Note_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:Despatch_Note_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:Despatch_Note_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Quantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:Quantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:Quantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      !Set Width
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:SecondLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:SecondLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:SecondLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShelfLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:PurchaseCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:PurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:PurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OutWarrantyCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OutWarrantyCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OutWarrantyCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OutWarrantyMarkup
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OutWarrantyMarkup
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OutWarrantyMarkup
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FixedPrice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FixedPrice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FixedPrice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Supplier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:Exclude_From_Order
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:Exclude_From_Order
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:Exclude_From_Order
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::par:PartAllocated
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::par:PartAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::par:PartAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Show:UnallocatePart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:UnallocatePart
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:UnallocatePart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:UnallocatePart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('locOrderRequired') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Order Required') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChargeableParts_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3" class="'&clip('RedBold')&'">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3" class="'&clip('RedBold')&'">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:OrderRequired
      do Comment::text:OrderRequired
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:OrderRequired2
      do Comment::text:OrderRequired2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CreateOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChargeableParts_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCodesChecked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCodesChecked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCodesChecked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode1') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCodes1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCodes1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCodes1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode2') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode3') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode4') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode5') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode6') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode7') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode8') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode9') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode10') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode11') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode12') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::par:Order_Number  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Order_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Order_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Order_Number',p_web.GetValue('NewValue'))
    par:Order_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = par:Order_Number
    do Value::par:Order_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@n08b'))
    par:Order_Number = p_web.Dformat(p_web.GetValue('Value'),'@n08b') !
  End

Value::par:Order_Number  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Order_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- par:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('par:Order_Number'),'@n08b')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::par:Order_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Order_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:Date_Ordered  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Ordered') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Ordered')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Date_Ordered  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Date_Ordered',p_web.GetValue('NewValue'))
    par:Date_Ordered = p_web.GetValue('NewValue') !FieldType= DATE Field = par:Date_Ordered
    do Value::par:Date_Ordered
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Date_Ordered',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    par:Date_Ordered = p_web.GetValue('Value')
  End

Value::par:Date_Ordered  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Ordered') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- par:Date_Ordered
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('par:Date_Ordered'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::par:Date_Ordered  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Ordered') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:Date_Received  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Received') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Received')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Date_Received  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Date_Received',p_web.GetValue('NewValue'))
    par:Date_Received = p_web.GetValue('NewValue') !FieldType= DATE Field = par:Date_Received
    do Value::par:Date_Received
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Date_Received',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    par:Date_Received = p_web.GetValue('Value')
  End

Value::par:Date_Received  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Received') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- par:Date_Received
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('par:Date_Received'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::par:Date_Received  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Received') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locOutFaultCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOutFaultCode',p_web.GetValue('NewValue'))
    do Value::locOutFaultCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locOutFaultCode  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('locOutFaultCode') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locOutFaultCode'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locOutFaultCode  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('locOutFaultCode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:Part_Number  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Part_Number',p_web.GetValue('NewValue'))
    par:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = par:Part_Number
    do Value::par:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    par:Part_Number = p_web.GetValue('Value')
  End
  If par:Part_Number = ''
    loc:Invalid = 'par:Part_Number'
    loc:alert = p_web.translate('Part Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    par:Part_Number = Upper(par:Part_Number)
    p_web.SetSessionValue('par:Part_Number',par:Part_Number)
      if (p_web.GSV('job:Engineer') <> '')
          locUserCode = p_web.GSV('job:Engineer')
      else !
          locUserCode = p_web.GSV('BookingUSerCOde')
      end !if (p_web.GSV('job:Engineer') <> '')
  
      case validFreeTextPart('C',locUserCode,p_web.GSV('job:Manufacturer'),|
                                  p_web.GSV('job:Model_Number'),p_web.GSV('par:part_Number'))
      of 0
          p_web.SSV('Comment:PartNumber','')
  
          Access:STOMODEL.Clearkey(stm:Location_Part_Number_Key)
          stm:Model_Number    = p_web.GSV('job:Model_Number')
          stm:Location    = p_web.GSV('BookingSiteLocation')
          stm:Part_Number    = p_web.GSV('par:Part_Number')
          if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Found
          else ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
  
          do updatePartDetails
      of 1
          p_web.SSV('par:Part_Number','')
          p_web.SSV('Comment:PartNumber','Invalid User')
      of 2
          p_web.SSV('par:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Not In Stock Location')
      of 3
          p_web.SSV('par:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Suspended')
      of 4
          p_web.SSV('Comment:PartNumber','Warning! Cannot Find The Selected Part In Stock')
      of 5
          p_web.SSV('par:part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Access Level Is Not High Enough For Part')
  
      end ! case
  
  p_Web.SetValue('lookupfield','par:Part_Number')
  do AfterLookup
  do Value::par:Part_Number
  do SendAlert
  do Comment::par:Part_Number
  do Comment::par:Part_Number
  do Value::par:Description  !1
  do Value::par:Supplier  !1
  do Value::tmp:PurchaseCost  !1
  do Value::tmp:OutWarrantyCost  !1
  do Value::tmp:OutWarrantyMarkup  !1
  do Prompt::tmp:SecondLocation
  do Value::tmp:SecondLocation  !1
  do Prompt::tmp:ShelfLocation
  do Value::tmp:ShelfLocation  !1
  do Prompt::tmp:Location
  do Value::tmp:Location  !1

Value::par:Part_Number  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- par:Part_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('par:Part_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If par:Part_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Part_Number'',''formchargeableparts_par:part_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('par:Part_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','par:Part_Number',p_web.GetSessionValue('par:Part_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseModelStock')&'?LookupField=par:Part_Number&Tab=3&ForeignField=stm:Part_Number&_sort=stm:Description&Refresh=sort&LookupFrom=FormChargeableParts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_value')

Comment::par:Part_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartNumber'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_comment')

Prompt::par:Description  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Description',p_web.GetValue('NewValue'))
    par:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = par:Description
    do Value::par:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    par:Description = p_web.GetValue('Value')
  End
  If par:Description = ''
    loc:Invalid = 'par:Description'
    loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    par:Description = Upper(par:Description)
    p_web.SetSessionValue('par:Description',par:Description)
  do Value::par:Description
  do SendAlert

Value::par:Description  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Description') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- par:Description
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('par:Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If par:Description = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Description'',''formchargeableparts_par:description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','par:Description',p_web.GetSessionValueFormat('par:Description'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Description') & '_value')

Comment::par:Description  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:Despatch_Note_Number  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch Note Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Despatch_Note_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Despatch_Note_Number',p_web.GetValue('NewValue'))
    par:Despatch_Note_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = par:Despatch_Note_Number
    do Value::par:Despatch_Note_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Despatch_Note_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    par:Despatch_Note_Number = p_web.GetValue('Value')
  End
    par:Despatch_Note_Number = Upper(par:Despatch_Note_Number)
    p_web.SetSessionValue('par:Despatch_Note_Number',par:Despatch_Note_Number)
  do Value::par:Despatch_Note_Number
  do SendAlert

Value::par:Despatch_Note_Number  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- par:Despatch_Note_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('par:Despatch_Note_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Despatch_Note_Number'',''formchargeableparts_par:despatch_note_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','par:Despatch_Note_Number',p_web.GetSessionValueFormat('par:Despatch_Note_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_value')

Comment::par:Despatch_Note_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:Quantity  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Quantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Quantity',p_web.GetValue('NewValue'))
    par:Quantity = p_web.GetValue('NewValue') !FieldType= REAL Field = par:Quantity
    do Value::par:Quantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Quantity',p_web.dFormat(p_web.GetValue('Value'),'@n4'))
    par:Quantity = p_web.Dformat(p_web.GetValue('Value'),'@n4') !
  End
  If par:Quantity = ''
    loc:Invalid = 'par:Quantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    par:Quantity = Upper(par:Quantity)
    p_web.SetSessionValue('par:Quantity',par:Quantity)
  do Value::par:Quantity
  do SendAlert

Value::par:Quantity  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- par:Quantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('par:Quantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If par:Quantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Quantity'',''formchargeableparts_par:quantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','par:Quantity',p_web.GetSessionValue('par:Quantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n4',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_value')

Comment::par:Quantity  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Location  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_prompt',Choose(p_web.GSV('tmp:Location') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Location')
  If p_web.GSV('tmp:Location') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_prompt')

Validate::tmp:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Location',p_web.GetValue('NewValue'))
    tmp:Location = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Location',p_web.GetValue('Value'))
    tmp:Location = p_web.GetValue('Value')
  End

Value::tmp:Location  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_value',Choose(p_web.GSV('tmp:Location') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:Location') = '')
  ! --- DISPLAY --- tmp:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_value')

Comment::tmp:Location  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_comment',Choose(p_web.GSV('tmp:Location') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:Location') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:SecondLocation  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Second Location')
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt')

Validate::tmp:SecondLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:SecondLocation',p_web.GetValue('NewValue'))
    tmp:SecondLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:SecondLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:SecondLocation',p_web.GetValue('Value'))
    tmp:SecondLocation = p_web.GetValue('Value')
  End

Value::tmp:SecondLocation  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_value',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:SecondLocation') = '')
  ! --- DISPLAY --- tmp:SecondLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:SecondLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_value')

Comment::tmp:SecondLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_comment',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ShelfLocation  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Shelf Location')
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt')

Validate::tmp:ShelfLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShelfLocation',p_web.GetValue('NewValue'))
    tmp:ShelfLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShelfLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShelfLocation',p_web.GetValue('Value'))
    tmp:ShelfLocation = p_web.GetValue('Value')
  End

Value::tmp:ShelfLocation  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ShelfLocation') = '')
  ! --- DISPLAY --- tmp:ShelfLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:ShelfLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value')

Comment::tmp:ShelfLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_comment',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:PurchaseCost  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Purchase Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:PurchaseCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:PurchaseCost',p_web.GetValue('NewValue'))
    tmp:PurchaseCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:PurchaseCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:PurchaseCost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    tmp:PurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End
  do Value::tmp:PurchaseCost
  do SendAlert

Value::tmp:PurchaseCost  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:PurchaseCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:PurchaseCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:PurchaseCost'',''formchargeableparts_tmp:purchasecost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:PurchaseCost',p_web.GetSessionValue('tmp:PurchaseCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value')

Comment::tmp:PurchaseCost  Routine
      loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OutWarrantyCost  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Out Of Warranty Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OutWarrantyCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OutWarrantyCost',p_web.GetValue('NewValue'))
    tmp:OutWarrantyCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OutWarrantyCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OutWarrantyCost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    tmp:OutWarrantyCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End
  do enableDisableCosts
  do Value::tmp:OutWarrantyCost
  do SendAlert
  do Value::tmp:OutWarrantyMarkup  !1

Value::tmp:OutWarrantyCost  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:OutWarrantyCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:OutWarrantyCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyCost'',''formchargeableparts_tmp:outwarrantycost_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyCost')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyCost',p_web.GetSessionValue('tmp:OutWarrantyCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value')

Comment::tmp:OutWarrantyCost  Routine
      loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OutWarrantyMarkup  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Markup')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OutWarrantyMarkup  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',p_web.GetValue('NewValue'))
    tmp:OutWarrantyMarkup = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OutWarrantyMarkup
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',p_web.dFormat(p_web.GetValue('Value'),'@n3'))
    tmp:OutWarrantyMarkup = p_web.Dformat(p_web.GetValue('Value'),'@n3') !
  End
  !Out Warranty Markup
  markup# = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  if (p_web.GSV('tmp:OutWarrantyMarkup') > markup#)
      p_web.SSV('Comment:OutWarrantyMarkup','You cannot markup more than ' & markup# & '%')
      p_web.SSV('tmp:OutWarrantyMarkup',markup#)
  else! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
      p_web.SSV('Comment:OutWarrantyMarkup','')
  end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
      p_web.SSV('tmp:OutWarrantyCost',format(p_web.GSV('tmp:PurchaseCost') + (p_web.GSV('tmp:PurchaseCost') * (p_web.GSV('tmp:OutWarrantyMarkup')/100)),@n_14.2))
  end !if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
  do Value::tmp:OutWarrantyMarkup
  do SendAlert
  do Value::tmp:OutWarrantyCost  !1
  do Comment::tmp:OutWarrantyMarkup

Value::tmp:OutWarrantyMarkup  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:OutWarrantyMarkup
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:OutWarrantyMarkup')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyMarkup'',''formchargeableparts_tmp:outwarrantymarkup_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyMarkup')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyMarkup',p_web.GetSessionValue('tmp:OutWarrantyMarkup'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n3',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value')

Comment::tmp:OutWarrantyMarkup  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:OutWarrantyMarkup'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment')

Prompt::tmp:FixedPrice  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FixedPrice') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FixedPrice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FixedPrice',p_web.GetValue('NewValue'))
    do Value::tmp:FixedPrice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::tmp:FixedPrice  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FixedPrice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::tmp:FixedPrice  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FixedPrice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:Supplier  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Supplier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Supplier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Supplier',p_web.GetValue('NewValue'))
    par:Supplier = p_web.GetValue('NewValue') !FieldType= STRING Field = par:Supplier
    do Value::par:Supplier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Supplier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    par:Supplier = p_web.GetValue('Value')
  End
    par:Supplier = Upper(par:Supplier)
    p_web.SetSessionValue('par:Supplier',par:Supplier)
  p_Web.SetValue('lookupfield','par:Supplier')
  do AfterLookup
  do Value::par:Supplier
  do SendAlert
  do Comment::par:Supplier

Value::par:Supplier  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- par:Supplier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('par:Supplier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Supplier'',''formchargeableparts_par:supplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(par:Supplier)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','par:Supplier',p_web.GetSessionValue('par:Supplier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectSuppliers')&'?LookupField=par:Supplier&Tab=3&ForeignField=sup:Company_Name&_sort=sup:Company_Name&Refresh=sort&LookupFrom=FormChargeableParts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_value')

Comment::par:Supplier  Routine
      loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_comment')

Prompt::par:Exclude_From_Order  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exclude From Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:Exclude_From_Order  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:Exclude_From_Order',p_web.GetValue('NewValue'))
    par:Exclude_From_Order = p_web.GetValue('NewValue') !FieldType= STRING Field = par:Exclude_From_Order
    do Value::par:Exclude_From_Order
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:Exclude_From_Order',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    par:Exclude_From_Order = p_web.GetValue('Value')
  End
  do Value::par:Exclude_From_Order
  do SendAlert

Value::par:Exclude_From_Order  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- par:Exclude_From_Order
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('par:Exclude_From_Order')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('par:Exclude_From_Order') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:Exclude_From_Order'',''formchargeableparts_par:exclude_from_order_value'',1,'''&clip('YES')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:Exclude_From_Order',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'par:Exclude_From_Order_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('par:Exclude_From_Order') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:Exclude_From_Order'',''formchargeableparts_par:exclude_from_order_value'',1,'''&clip('NO')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:Exclude_From_Order',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'par:Exclude_From_Order_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_value')

Comment::par:Exclude_From_Order  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::par:PartAllocated  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Allocated')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::par:PartAllocated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('par:PartAllocated',p_web.GetValue('NewValue'))
    par:PartAllocated = p_web.GetValue('NewValue') !FieldType= BYTE Field = par:PartAllocated
    do Value::par:PartAllocated
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('par:PartAllocated',p_web.dFormat(p_web.GetValue('Value'),'@n1'))
    par:PartAllocated = p_web.GetValue('Value')
  End
  do Value::par:PartAllocated
  do SendAlert

Value::par:PartAllocated  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- par:PartAllocated
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('par:PartAllocated')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('par:PartAllocated') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:PartAllocated'',''formchargeableparts_par:partallocated_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:PartAllocated',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','par:PartAllocated_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('par:PartAllocated') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:PartAllocated'',''formchargeableparts_par:partallocated_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:PartAllocated',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','par:PartAllocated_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_value')

Comment::par:PartAllocated  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:UnallocatePart  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unallocate Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:UnallocatePart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:UnallocatePart',p_web.GetValue('NewValue'))
    tmp:UnallocatePart = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:UnallocatePart
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:UnallocatePart',p_web.GetValue('Value'))
    tmp:UnallocatePart = p_web.GetValue('Value')
  End
  IF (p_web.GSV('tmp:UnAllocatePart') = 1)
      p_web.SSV('Comment:UnallocatePart','Part will appear in "Parts On Order" section of Stock Allocation')
  ELSE
      p_web.SSV('Comment:UnallocatePart','Return Part To Stock')
  END
  
      
  do Value::tmp:UnallocatePart
  do SendAlert
  do Comment::tmp:UnallocatePart

Value::tmp:UnallocatePart  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:UnallocatePart
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UnallocatePart'',''formchargeableparts_tmp:unallocatepart_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:UnallocatePart')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:UnallocatePart') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:UnallocatePart',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value')

Comment::tmp:UnallocatePart  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:UnallocatePart'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_comment')

Validate::text:OrderRequired  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OrderRequired',p_web.GetValue('NewValue'))
    do Value::text:OrderRequired
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OrderRequired  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate(p_web.GSV('text:OrderRequired'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OrderRequired  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::text:OrderRequired2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OrderRequired2',p_web.GetValue('NewValue'))
    do Value::text:OrderRequired2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OrderRequired2  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('Either reduce the quanitity required, or select the check box to create an order for the excess items',) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OrderRequired2  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CreateOrder  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Create Order For Selected Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CreateOrder',p_web.GetValue('NewValue'))
    tmp:CreateOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CreateOrder
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:CreateOrder',p_web.GetValue('Value'))
    tmp:CreateOrder = p_web.GetValue('Value')
  End
  do Value::tmp:CreateOrder
  do SendAlert

Value::tmp:CreateOrder  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:CreateOrder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CreateOrder'',''formchargeableparts_tmp:createorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:CreateOrder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CreateOrder',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_value')

Comment::tmp:CreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCodesChecked  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_prompt',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Codes Checked')
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCodesChecked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCodesChecked',p_web.GetValue('NewValue'))
    tmp:FaultCodesChecked = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCodesChecked
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:FaultCodesChecked',p_web.GetValue('Value'))
    tmp:FaultCodesChecked = p_web.GetValue('Value')
  End
  do Value::tmp:FaultCodesChecked
  do SendAlert

Value::tmp:FaultCodesChecked  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:FaultCodesChecked') = 1)
  ! --- CHECKBOX --- tmp:FaultCodesChecked
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:FaultCodesChecked'',''formchargeableparts_tmp:faultcodeschecked_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:FaultCodesChecked') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:FaultCodesChecked',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value')

Comment::tmp:FaultCodesChecked  Routine
    loc:comment = ''
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_comment',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCodes1  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode1'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCodes1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCodes1',p_web.GetValue('NewValue'))
    tmp:FaultCodes1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCodes1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes1',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1')))
    tmp:FaultCodes1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1')) !
  End
  If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
    loc:Invalid = 'tmp:FaultCodes1'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  do Value::tmp:FaultCodes1
  do SendAlert

Value::tmp:FaultCodes1  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFautlCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes1 = '' and (p_web.GSV('Req:PartFautlCode1') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCodes1'',''formchargeableparts_tmp:faultcodes1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes1',p_web.GetSessionValue('tmp:FaultCodes1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode1'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(1)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value')

Comment::tmp:FaultCodes1  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode1'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode2  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode2'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.GetValue('NewValue'))
    tmp:FaultCodes2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes2',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2')))
    tmp:FaultCodes2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2')) !
  End
  If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCodes2'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  do Value::tmp:FaultCode2
  do SendAlert

Value::tmp:FaultCode2  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes2 = '' and (p_web.GSV('Req:PartFaultCode2') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''formchargeableparts_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes2',p_web.GetSessionValue('tmp:FaultCodes2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode2'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(2)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_value')

Comment::tmp:FaultCode2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode2'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode3  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode3'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.GetValue('NewValue'))
    tmp:FaultCodes3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes3',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3')))
    tmp:FaultCodes3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3')) !
  End
  If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCodes3'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  do Value::tmp:FaultCode3
  do SendAlert

Value::tmp:FaultCode3  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes3 = '' and (p_web.GSV('Req:PartFaultCode3') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''formchargeableparts_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes3',p_web.GetSessionValue('tmp:FaultCodes3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode3'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(3)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_value')

Comment::tmp:FaultCode3  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode3'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode4  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode4'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.GetValue('NewValue'))
    tmp:FaultCodes4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes4',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4')))
    tmp:FaultCodes4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4')) !
  End
  If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCodes4'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  do Value::tmp:FaultCode4
  do SendAlert

Value::tmp:FaultCode4  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode4') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode4') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes4 = '' and (p_web.GSV('Req:PartFaultCode4') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''formchargeableparts_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes4',p_web.GetSessionValue('tmp:FaultCodes4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode4'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(4)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_value')

Comment::tmp:FaultCode4  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode4'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode5  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode5'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.GetValue('NewValue'))
    tmp:FaultCodes5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes5',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5')))
    tmp:FaultCodes5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5')) !
  End
  If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCodes5'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  do Value::tmp:FaultCode5
  do SendAlert

Value::tmp:FaultCode5  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode5') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode5') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes5 = '' and (p_web.GSV('Req:PartFaultCode5') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''formchargeableparts_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes5',p_web.GetSessionValue('tmp:FaultCodes5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode5'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(5)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_value')

Comment::tmp:FaultCode5  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode5'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode6  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode6'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.GetValue('NewValue'))
    tmp:FaultCodes6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes6',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6')))
    tmp:FaultCodes6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6')) !
  End
  If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCodes6'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  do Value::tmp:FaultCode6
  do SendAlert

Value::tmp:FaultCode6  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode6') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode6') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes6 = '' and (p_web.GSV('Req:PartFaultCode6') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''formchargeableparts_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes6',p_web.GetSessionValue('tmp:FaultCodes6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode6'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(6)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_value')

Comment::tmp:FaultCode6  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode6'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode7  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode7'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.GetValue('NewValue'))
    tmp:FaultCodes7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes7',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7')))
    tmp:FaultCodes7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7')) !
  End
  If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCodes7'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  do Value::tmp:FaultCode7
  do SendAlert

Value::tmp:FaultCode7  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode7') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode7') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes7 = '' and (p_web.GSV('Req:PartFaultCode7') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''formchargeableparts_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes7',p_web.GetSessionValue('tmp:FaultCodes7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode7'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(7)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_value')

Comment::tmp:FaultCode7  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode7'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode8  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode8'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.GetValue('NewValue'))
    tmp:FaultCodes8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes8',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8')))
    tmp:FaultCodes8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8')) !
  End
  If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCodes8'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  do Value::tmp:FaultCode8
  do SendAlert

Value::tmp:FaultCode8  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode8') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode8') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes8 = '' and (p_web.GSV('Req:PartFaultCode8') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''formchargeableparts_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes8',p_web.GetSessionValue('tmp:FaultCodes8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode8'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(8)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_value')

Comment::tmp:FaultCode8  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode8'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode9  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode9'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.GetValue('NewValue'))
    tmp:FaultCodes9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes9',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9')))
    tmp:FaultCodes9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9')) !
  End
  If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCodes9'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  do Value::tmp:FaultCode9
  do SendAlert

Value::tmp:FaultCode9  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode9') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode9') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes9')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes9 = '' and (p_web.GSV('Req:PartFaultCode9') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''formchargeableparts_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes9',p_web.GetSessionValue('tmp:FaultCodes9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode9'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(9)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_value')

Comment::tmp:FaultCode9  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode9'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode10  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode10'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.GetValue('NewValue'))
    tmp:FaultCodes10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes10',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10')))
    tmp:FaultCodes10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10')) !
  End
  If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCodes10'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  do Value::tmp:FaultCode10
  do SendAlert

Value::tmp:FaultCode10  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode10') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode10') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes10')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes10 = '' and (p_web.GSV('Req:PartFaultCode10') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''formchargeableparts_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes10',p_web.GetSessionValue('tmp:FaultCodes10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode10'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(10)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_value')

Comment::tmp:FaultCode10  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode10'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode11  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode11'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.GetValue('NewValue'))
    tmp:FaultCodes11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes11',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11')))
    tmp:FaultCodes11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11')) !
  End
  If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCodes11'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  do Value::tmp:FaultCode11
  do SendAlert

Value::tmp:FaultCode11  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode11') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode11') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes11')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes11 = '' and (p_web.GSV('Req:PartFaultCode11') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''formchargeableparts_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes11',p_web.GetSessionValue('tmp:FaultCodes11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode11'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(11)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_value')

Comment::tmp:FaultCode11  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode11'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode12  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode12'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.GetValue('NewValue'))
    tmp:FaultCodes12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes12',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12')))
    tmp:FaultCodes12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12')) !
  End
  If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCodes12'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  do Value::tmp:FaultCode12
  do SendAlert

Value::tmp:FaultCode12  Routine
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode12') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode12') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes12')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes12 = '' and (p_web.GSV('Req:PartFaultCode12') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''formchargeableparts_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes12',p_web.GetSessionValue('tmp:FaultCodes12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode12'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(12)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_value')

Comment::tmp:FaultCode12  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode12'))
  p_web._DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormChargeableParts_par:Part_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Part_Number
      else
        do Value::par:Part_Number
      end
  of lower('FormChargeableParts_par:Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Description
      else
        do Value::par:Description
      end
  of lower('FormChargeableParts_par:Despatch_Note_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Despatch_Note_Number
      else
        do Value::par:Despatch_Note_Number
      end
  of lower('FormChargeableParts_par:Quantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Quantity
      else
        do Value::par:Quantity
      end
  of lower('FormChargeableParts_tmp:PurchaseCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PurchaseCost
      else
        do Value::tmp:PurchaseCost
      end
  of lower('FormChargeableParts_tmp:OutWarrantyCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyCost
      else
        do Value::tmp:OutWarrantyCost
      end
  of lower('FormChargeableParts_tmp:OutWarrantyMarkup_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyMarkup
      else
        do Value::tmp:OutWarrantyMarkup
      end
  of lower('FormChargeableParts_par:Supplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Supplier
      else
        do Value::par:Supplier
      end
  of lower('FormChargeableParts_par:Exclude_From_Order_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Exclude_From_Order
      else
        do Value::par:Exclude_From_Order
      end
  of lower('FormChargeableParts_par:PartAllocated_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:PartAllocated
      else
        do Value::par:PartAllocated
      end
  of lower('FormChargeableParts_tmp:UnallocatePart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UnallocatePart
      else
        do Value::tmp:UnallocatePart
      end
  of lower('FormChargeableParts_tmp:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CreateOrder
      else
        do Value::tmp:CreateOrder
      end
  of lower('FormChargeableParts_tmp:FaultCodesChecked_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodesChecked
      else
        do Value::tmp:FaultCodesChecked
      end
  of lower('FormChargeableParts_tmp:FaultCodes1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodes1
      else
        do Value::tmp:FaultCodes1
      end
  of lower('FormChargeableParts_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('FormChargeableParts_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('FormChargeableParts_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('FormChargeableParts_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('FormChargeableParts_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('FormChargeableParts_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('FormChargeableParts_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('FormChargeableParts_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('FormChargeableParts_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('FormChargeableParts_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('FormChargeableParts_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormChargeableParts',0)
  par:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('par:Ref_Number',par:Ref_Number)
  par:Quantity = 1
  p_web.SetSessionValue('par:Quantity',par:Quantity)
  par:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('par:Exclude_From_Order',par:Exclude_From_Order)

PreCopy  Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormChargeableParts',0)
  p_web._PreCopyRecord(PARTS,par:recordnumberkey)
  ! here we need to copy the non-unique fields across
  par:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('par:Ref_Number',p_web.GSV('wob:RefNumber'))
  par:Quantity = 1
  p_web.SetSessionValue('par:Quantity',1)

PreUpdate       Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormChargeableParts:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormChargeableParts:Primed',0)
  p_web.setsessionvalue('showtab_FormChargeableParts',0)

LoadRelatedRecords  Routine
  if (p_web.ifExistsValue('adjustment'))
      p_web.storeValue('adjustment')
  !        p_web.SSV('adjustment',p_web.getValue('adjustment'))
      if (p_web.getValue('adjustment') = 1)
          par:Part_Number = 'ADJUSTMENT'
          par:Description = 'ADJUSTMENT'
          par:Adjustment = 'YES'
          par:Quantity = 1
          par:Warranty_Part = 'NO'
          par:Exclude_From_Order = 'YES'
  
          p_web.SSV('par:Part_Number',par:Part_Number)
          p_web.SSV('par:Description',par:Description)
          p_web.SSV('par:Adjustment',par:Adjustment)
          p_web.SSV('par:Quantity',par:Quantity)
          p_web.SSV('par:Warranty_Part',par:Warranty_Part)
          p_web.SSV('par:Exclude_From_Order',par:Exclude_From_Order)
  
      end ! if (p_web.getValue('adjustment') = 1)
  end !if (p_web.ifExistsValue('adjustment'))
  
  if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
      p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
      p_web.SSV('ReadOnly:PurchaseCost',1)
      p_web.SSV('ReadOnly:OutWarrantyCOst',1)
  else !if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
      p_web.SSV('ReadOnly:OutWarrantyMarkup',0)
      p_web.SSV('ReadOnly:PurchaseCost',0)
      p_web.SSV('ReadOnly:OutWarrantyCOst',0)
  end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
  
  if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PART ALLOCATED'))
      p_web.SSV('ReadOnly:PartAllocated',1)
  else ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
      p_web.SSV('ReadOnly:PartAllocated',0)
  end ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
  
  if (par:WebOrder = 1)
      p_web.SSV('ReadOnly:Quantity',1)
  end ! if (par:WebOrder = 1)
  p_web.SSV('ReadOnly:ExcludeFromOrder',0)
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PARTS - EXCLUDE FROM ORDER'))
      p_web.SSV('ReadOnly:ExcludeFromOrder',1)    
  END
  
  if (loc:Act = ChangeRecord)
      if (p_web.GSV('job:Date_Completed') > 0)
          if (~SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS COSTS - EDIT POST COMPLETE'))
              p_web.SSV('ReadOnly:OutWarrantyCost',1)
              p_web.SSV('ReadOnly:Quantity',1)
          end !if (SecurityCheck('JOBS COSTS - EDIT POST COMPLETE'))
      end ! if (p_web.GSV('job:Date_Completed') > 0)
  
      if (p_web.GSV('job:Estimate') = 'YES')
          Access:ESTPARTS.Clearkey(epr:part_Ref_Number_Key)
          epr:Ref_Number    = p_web.GSV('par:Ref_Number')
          epr:Part_Ref_Number    = p_web.GSV('par:Part_Ref_Number')
          if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
              ! Found
              ! This is same part as on the estimate
              p_web.SSV('ReadOnly:OutWarrantyCost',1)
              p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
          else ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
      end ! if (p_web.GSV('job:Estimate') = 'YES')
      p_web.SSV('ReadOnly:ExcludeFromOrder',1)
  end ! if (loc:Act = ChangeRecord)
  
  do enableDisableCosts
  do showCosts
  do lookupLocation
  do lookupMainFault
      
  
  if (p_web.GSV('Part:ViewOnly') <> 1)
      ! Show unallodate part tick box
  
      if (loc:Act = ChangeRecord)
          if (p_web.GSV('par:Part_Ref_Number') <> '' And |
              p_web.GSV('par:PartAllocated') = 1 And |
                p_web.GSV('par:WebOrder') = 0)
                
              IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'UNALLOCATE PART'))
              ELSE
                  p_web.SSV('Show:UnallocatePart',1)
                  p_web.SSV('Comment:UnallocatePart','Return Part To Stock')
              END
          end ! if (p_web.GSV('par:Part_Ref_Number') <> '' And |
      end ! if (loc:Act = ChangeRecord)
  end ! if (p_web.GSV('Part:ViewOnly') <> 1)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If loc:act = ChangeRecord
  End
    If (p_web.GSV('Show:UnallocatePart') = 1)
          If p_web.IfExistsValue('tmp:UnallocatePart') = 0
            p_web.SetValue('tmp:UnallocatePart',0)
            tmp:UnallocatePart = 0
          End
    End
  If p_web.GSV('locOrderRequired') = 1
          If p_web.IfExistsValue('tmp:CreateOrder') = 0
            p_web.SetValue('tmp:CreateOrder',0)
            tmp:CreateOrder = 0
          End
  End
    If (0)
      If(p_web.GSV('Hide:FaultCodesChecked') = 1)
          If p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
            p_web.SetValue('tmp:FaultCodesChecked',0)
            tmp:FaultCodesChecked = 0
          End
      End
    End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  ! Insert Record Validation
  p_web.SSV('AddToStockAllocation:Type','')
  par:PartAllocated = 1
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKey)
  def:Record_Number    = 1
  set(def:RecordNumberKey,def:RecordNumberKey)
  loop
      if (Access:DEFAULTS.Next())
          Break
      end ! if (Access:DEFAULTS.Next())
      break
  end ! loop
  
  stockPart# = 0
  if (par:part_Ref_Number <> '')
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = par:part_Ref_Number
      if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Found
          ! Check for duplicate?
          stockPart# = 1
          found# = 0
          if (sto:AllowDuplicate = 0)
  
              Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
              par_ali:Ref_Number    = p_web.GSV('job:Ref_Number')
              par_Ali:Part_Number = par:part_Number
              set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
              loop
                  if (Access:PARTS_ALIAS.Next())
                      Break
                  end ! if (Access:PARTS.Next())
                  if (par_ali:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (par:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (par_ali:part_Number <> par:Part_Number)
                      Break
                  end ! if (par:Part_Number    <> p_web.GSV('par:Part_Number'))
                  if (par_ali:Date_Received = '')
                      found# = 1
                      break
                  end ! if (par:Date_Received = '')
              end ! loop
          end ! if (sto:AllowDuplicate = 0)
  
          if (found# = 1)
              loc:Invalid = 'par:Part_Number'
              loc:Alert = 'This part is already attached to this job.'
              exit
          end ! if (par:Date_Received = '')
      else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  else ! if (p_web.GSV('par:Part_Ref_Number') <> '')
  end ! if (p_web.GSV('par:Part_Ref_Number') <> '')
  
  ! update stock details
  p_web.SSV('locOrderRequired',0)
  if (par:part_Number <> 'ADJUSTMENT')
      if (par:exclude_From_Order <> 'YES')
          if (stockPart# = 1)
              if (sto:Sundry_Item <> 'YES')
                  if (par:Quantity > sto:Quantity_Stock)
                      if (p_web.GSV('locOrderRequired') <> 1 and p_web.GSV('tmp:CreateOrder') = 0)
                          p_web.SSV('locOrderRequired',1)
                          loc:Invalid = 'tmp:OrderPart'
                          loc:Alert = 'Insufficient Items In Stock'
                          p_web.SSV('text:OrderRequired','Qty Required: ' & p_web.GSV('par:Quantity') & ', Qty In Stock: ' & sto:Quantity_Stock)
                      else ! if (p_web.GSV('locOrderRequired') <> 1)
                          ! Have selected to create an order
                          if (virtualSite(sto:Location))
                              createWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,|
                                  par:Description,(par:Quantity - sto:Quantity_Stock),par:Retail_Cost)
  
                              if (sto:Quantity_Stock > 0)
                                  ! New Part
                                  stockQty# = sto:Quantity_Stock
                                  sto:Quantity_Stock = 0
                                  if (access:STOCK.tryUpdate() = level:benign)
                                      rtn# = AddToStockHistory(sto:Ref_Number, |
                                          'DEC', |
                                          par:Despatch_Note_Number, |
                                          p_web.GSV('job:Ref_Number'), |
                                          0, |
                                          sto:Quantity_Stock, |
                                          p_web.GSV('tmp:InWarrantyCost'), |
                                          p_web.GSV('tmp:OutWarrantyCost'), |
                                          par:Retail_Cost, |
                                          'STOCK DECREMENTED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'), |
                                          sto:Quantity_Stock)
                                          
                                      if (Access:PARTS_ALIAS.PrimeRecord() = Level:Benign)
                                          recNo# = par_ali:Record_Number
                                          par_ali:Record :=: par:Record
                                          par_ali:Record_Number = recNo#
                                          par:Quantity = par:Quantity - stockQty#
                                          par:WebOrder = 1
                                          if (Access:PARTS_ALIAS.TryInsert() = Level:Benign)
                                              ! Inserted
                                              p_web.SSV('AddToStockAllocation:Type','CHA')
                                              p_web.SSV('AddToStockAllocation:Status','WEB')
                                              p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                                              
                                          else ! if (Access:PARTS_ALIAS.TryInsert() = Level:Benign)
                                              ! Error
                                              Access:PARTS_ALIAS.CancelAutoInc()
                                          end ! if (Access:PARTS_ALIAS.TryInsert() = Level:Benign)
                                      end ! if (Access:PARTS_ALIAS.PrimeRecord() = Level:Benign)
                                      par:Quantity = stockQty#
                                      par:Date_Ordered = Today()
                                  end ! if (access:STOCK.tryUpdate() = level:benign)
                              else ! if (sto:Quantity_Stock > 0)
                                  par:WebOrder = 1
                                  par:PartAllocated = 0
                                  p_web.SSV('AddToStockAllocation:Type','CHA')
                                  p_web.SSV('AddToStockAllocation:Status','WEB')
                                  p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                                  
                              end ! if (sto:Quantity_Stock > 0)
                          else ! if (virtualSite(sto:Location))
                              ! this doesn't apply to vodacom
                          end ! if (virtualSite(sto:Location))
                      end ! if (p_web.GSV('locOrderRequired') <> 1)
                  else ! if (par:Quantity > sto:Quantity_Stock)
                      par:date_Ordered = Today()
                      if rapidLocation(sto:Location)
                          par:PartAllocated = 0
                          p_web.SSV('AddToStockAllocation:Type','CHA')
                          p_web.SSV('AddToStockAllocation:Status','')
                          p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                          
                      else
                          par:PartAllocated = 1 ! #11704 Allocate the part if not using Rapid Stock Allocation (Bryan: 01/10/2010)
                      end ! if rapidLocation(sto:Location)
  
                      sto:quantity_Stock -= par:Quantity
                      if (sto:quantity_Stock < 0)
                          sto:quantity_Stock = 0
                      end ! if rapidLocation(sto:Location)
                      if (access:STOCK.tryUpdate() = level:Benign)
                          rtn# = AddToStockHistory(sto:Ref_Number, |
                              'DEC', |
                              par:Despatch_Note_Number, |
                              p_web.GSV('job:Ref_Number'), |
                              0, |
                              sto:Quantity_Stock, |
                              p_web.GSV('tmp:InWarrantyCost'), |
                              p_web.GSV('tmp:OutWarrantyCost'), |
                              par:Retail_Cost, |
                              'STOCK DECREMENTED', |
                              '', |
                              p_web.GSV('BookingUserCode'), |
                              sto:Quantity_Stock)
                      end ! if (access:STOCK.tryUpdate() = level:Benign)
                  end ! if (par:Quantity > sto:Quantity_Stock)
              else ! if (sto:Sundry_Item <> 'YES')
                  par:date_Ordered = Today()
              end ! if (sto:Sundry_Item <> 'YES')
          else ! if (stockPart# = 1)
  
          end ! if (stockPart# = 1)
      else ! if (par:exclude_From_Order <> 'YES')
          par:date_Ordered = Today()
      end ! if (par:exclude_From_Order <> 'YES')
  else ! if (par:part_Number <> 'ADJUSTMENT')
      par:date_Ordered = Today()
  end !if (par:part_Number <> 'ADJUSTMENT')

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  ! Change Record Validation
  IF (p_web.GSV('Show:UnallocatePart') = 1 AND p_web.GSV('tmp:UnAllocatePart') = 1)
      ! Unallocate Part
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      ELSE
          Pointer# = Pointer(STOCK)
          Hold(STOCK,1)
          Get(STOCK,Pointer#)
          If Errorcode() = 43
              loc:Invalid = 'tmp:UnallocatePart'
              loc:Alert = 'Cannot unallocate part, the stock item is in use.'
              EXIT
          End !If Errorcode() = 43
          sto:Quantity_Stock += par:Quantity
          If Access:STOCK.Update() = Level:Benign
              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                  'REC', | ! Transaction_Type
                  par:Despatch_Note_Number, | ! Depatch_Note_Number
                  job:Ref_Number, | ! Job_Number
                  0, | ! Sales_Number
                  par:Quantity, | ! Quantity
                  par:Purchase_Cost, | ! Purchase_Cost
                  par:Sale_Cost, | ! Sale_Cost
                  par:Retail_Cost, | ! Retail_Cost
                  'STOCK UNALLOCATED', | ! Notes
                  '', |
                  p_web.GSV('BookingUserCode'), |
                  sto:Quantity_Stock) ! Information
                  ! Added OK
  
              Else ! AddToStockHistory
                  ! Error
              End ! AddToStockHistory
              par:Status = 'WEB'
              par:PartAllocated = 0
              par:WebOrder = 1
          End !If Access:STOCK.Update() = Level:Benign    
      END
  
  END
  
  do CompleteCheckBoxes
  do ValidateRecord
  ! Change Record Validation

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormChargeableParts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormChargeableParts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
  If loc:act = ChangeRecord
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If par:Part_Number = ''
          loc:Invalid = 'par:Part_Number'
          loc:alert = p_web.translate('Part Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          par:Part_Number = Upper(par:Part_Number)
          p_web.SetSessionValue('par:Part_Number',par:Part_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If par:Description = ''
          loc:Invalid = 'par:Description'
          loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          par:Description = Upper(par:Description)
          p_web.SetSessionValue('par:Description',par:Description)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          par:Despatch_Note_Number = Upper(par:Despatch_Note_Number)
          p_web.SetSessionValue('par:Despatch_Note_Number',par:Despatch_Note_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If par:Quantity = ''
          loc:Invalid = 'par:Quantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          par:Quantity = Upper(par:Quantity)
          p_web.SetSessionValue('par:Quantity',par:Quantity)
        If loc:Invalid <> '' then exit.
      if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
          loc:Invalid = 'tmp:OutWarrantyMarkup'
          loc:Alert = 'You cannot mark-up parts more than ' & GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI') & '%.'
      end !if p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          par:Supplier = Upper(par:Supplier)
          p_web.SetSessionValue('par:Supplier',par:Supplier)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('locOrderRequired') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
    loc:InvalidTab += 1
    If p_web.GSV('Hide:PartFaultCode1') <> 1
        If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
          loc:Invalid = 'tmp:FaultCodes1'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
          p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode2') <> 1
        If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
          loc:Invalid = 'tmp:FaultCodes2'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
          p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode3') <> 1
        If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
          loc:Invalid = 'tmp:FaultCodes3'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
          p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode4') <> 1
        If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
          loc:Invalid = 'tmp:FaultCodes4'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
          p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode5') <> 1
        If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
          loc:Invalid = 'tmp:FaultCodes5'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
          p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode6') <> 1
        If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
          loc:Invalid = 'tmp:FaultCodes6'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
          p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode7') <> 1
        If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
          loc:Invalid = 'tmp:FaultCodes7'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
          p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode8') <> 1
        If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
          loc:Invalid = 'tmp:FaultCodes8'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
          p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode9') <> 1
        If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
          loc:Invalid = 'tmp:FaultCodes9'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
          p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode10') <> 1
        If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
          loc:Invalid = 'tmp:FaultCodes10'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
          p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode11') <> 1
        If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
          loc:Invalid = 'tmp:FaultCodes11'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
          p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode12') <> 1
        If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
          loc:Invalid = 'tmp:FaultCodes12'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
          p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
        If loc:Invalid <> '' then exit.
    End
  ! The following fields are not on the form, but need to be checked anyway.
  ! Write Fields
      
  
      !Write Fault Codes
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer    = p_web.GSV('job:Manufacturer')
      map:ScreenOrder    = 0
      set(map:ScreenOrderKey,map:ScreenOrderKey)
      loop
          if (Access:MANFAUPA.Next())
              Break
          end ! if (Access:MANFAUPA.Next())
          if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              Break
          end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
          if (map:ScreenOrder    = 0)
              cycle
          end ! if (map:ScreenOrder    <> 0)
  
          p_web.SSV('par:Fault_Code' & map:Field_Number,p_web.GSV('tmp:FaultCodes' & map:ScreenOrder))
      end ! loop
  
  !    loop x# = 1 To 12
  !        p_web.SSV('par:Fault_Code' & x#,p_web.GSV('tmp:FaultCodes' & x#))
  !        linePrint('par:Fault_Code' & x# & ' - ' & p_web.GSV('tmp:FaultCodes' & x#),'c:\log.log')
  !    end ! loop x# = 1 To 12
  
      par:Fault_Code1 = p_web.GSV('par:Fault_Code1')
      par:Fault_Code2 = p_web.GSV('par:Fault_Code2')
      par:Fault_Code3 = p_web.GSV('par:Fault_Code3')
      par:Fault_Code4 = p_web.GSV('par:Fault_Code4')
      par:Fault_Code5 = p_web.GSV('par:Fault_Code5')
      par:Fault_Code6 = p_web.GSV('par:Fault_Code6')
      par:Fault_Code7 = p_web.GSV('par:Fault_Code7')
      par:Fault_Code8 = p_web.GSV('par:Fault_Code8')
      par:Fault_Code9 = p_web.GSV('par:Fault_Code9')
      par:Fault_Code10 = p_web.GSV('par:Fault_Code10')
      par:Fault_Code11 = p_web.GSV('par:Fault_Code11')
      par:Fault_Code12 = p_web.GSV('par:Fault_Code12')
  
      If p_web.GSV('tmp:ARCPart') = 1
          par:AveragePurchaseCost = p_web.GSV('tmp:PurchaseCost')
          par:Purchase_Cost       = p_web.GSV('tmp:InWarrantyCost')
          par:Sale_Cost           = p_web.GSV('tmp:OutWarrantyCost')
          par:InWarrantyMarkup    = p_web.GSV('tmp:InWarrantyMarkup')
          par:OutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
      Else !If tmp:ARCPart
          par:RRCAveragePurchaseCost  = p_web.GSV('tmp:PurchaseCost')
          par:RRCPurchaseCost     = p_web.GSV('tmp:InWarrantyCost')
          par:RRCSaleCost         = p_web.GSV('tmp:OutWarrantyCost')
          par:RRCInWarrantyMarkup = p_web.GSV('tmp:InWarrantyMarkup')
          par:RRCOutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
          par:Purchase_Cost       = par:RRCPurchaseCost
          par:Sale_Cost           = par:RRCSaleCost
      End !If tmp:ARCPart
  
  
      
! NET:WEB:StagePOST
PostInsert      Routine
  IF (p_web.GSV('AddToStockAllocation:Type') <> '')
      addToStockAllocation(p_web)
  END
  do deleteSessionValues

PostCopy        Routine
  p_web.SetSessionValue('FormChargeableParts:Primed',0)

PostUpdate      Routine
  do deleteSessionValues
  p_web.SetSessionValue('FormChargeableParts:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Location')
  p_web.StoreValue('tmp:SecondLocation')
  p_web.StoreValue('tmp:ShelfLocation')
  p_web.StoreValue('tmp:PurchaseCost')
  p_web.StoreValue('tmp:OutWarrantyCost')
  p_web.StoreValue('tmp:OutWarrantyMarkup')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:UnallocatePart')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:CreateOrder')
  p_web.StoreValue('tmp:FaultCodesChecked')
  p_web.StoreValue('tmp:FaultCodes1')
  p_web.StoreValue('tmp:FaultCodes2')
  p_web.StoreValue('tmp:FaultCodes3')
  p_web.StoreValue('tmp:FaultCodes4')
  p_web.StoreValue('tmp:FaultCodes5')
  p_web.StoreValue('tmp:FaultCodes6')
  p_web.StoreValue('tmp:FaultCodes7')
  p_web.StoreValue('tmp:FaultCodes8')
  p_web.StoreValue('tmp:FaultCodes9')
  p_web.StoreValue('tmp:FaultCodes10')
  p_web.StoreValue('tmp:FaultCodes11')
  p_web.StoreValue('tmp:FaultCodes12')

PostDelete      Routine
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:TabNumber)
    if loc:LookupDone
        
!        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
!        map:ScreenOrder    = fNumber
!        map:Manufacturer   = p_web.GSV('job:Manufacturer')
!        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Found
!        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Error
!        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!
!        if (map:MainFault)
!            p_web.FileToSessionQueue(MANFAULO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfo:Description)
!
!        else ! if (map:MainFault)
!            p_web.FileToSessionQueue(MANFPALO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfp:Description)
!        end ! if (map:MainFault)
        do buildFaultCodes
        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes' & fNumber)
local.SetLookupButton      Procedure(Long fNumber)
locUseRelatedPart           String(30)
Code
    if (p_web.GSV('ShowDate:PartFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''FormChargeableParts_pickdate_value'',1,FieldValue(this,1));nextFocus(FormChargeableParts_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:PartFaultCode' & fNumber) = 1)

        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:ScreenOrder    = fNumber
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
        if (map:UseRelatedJobCode <> 0)
            locUseRelatedPart = 'relatedPartCode=' & map:Field_Number
        else
            locUseRelatedPart = ''
        end ! if (map:UseRelatedJobCode <> 0)

        if (map:MainFault)

            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                        'sort&LookupFrom=FormChargeableParts&' & |
                        'fieldNumber=' & maf:Field_Number & '&partType=C&partMainFault=1&' & clip(locUseRelatedPart)),) !lookupextra

        else ! if (map:MainFault)
            found# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number    = p_web.GSV('par:Part_Ref_Number')
            if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Found
                ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
                If sto:Assign_Fault_Codes = 'YES'
                    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                    stm:Ref_Number = sto:Ref_Number
                    stm:Manufacturer = p_web.GSV('job:Manufacturer')
                    stm:Model_Number = p_web.GSV('job:Model_Number')
                    If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                        
                        Case map:Field_Number
                        Of 1
                            If stm:FaultCode1 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 2
                            If stm:FaultCode2 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 3
                            If stm:FaultCode3 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 4
                            If stm:FaultCode4 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 5
                            If stm:FaultCode5 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 6
                            If stm:FaultCode6 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 7
                            If stm:FaultCode7 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 8
                            If stm:FaultCode8 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 9
                            If stm:FaultCode9 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 10
                            If stm:FaultCode10 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 11
                            If stm:FaultCode11 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 12
                            If stm:FaultCode12 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        End ! Case map:Field_Number
                        If Found# = 1
                            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseStockPartFaultCodeLookup')&|
                                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=stu:Field&_sort=stu:Field&Refresh=' & |
                                        'sort&LookupFrom=FormChargeableParts&' & |
                                        'fieldNumber=' & map:Field_Number & '&stockRefNumber=' & stm:RecordNumber),) !lookupextra

                        End ! If Found# = 1
                    End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                End ! If sto:Assign_Fault_Codes = 'YES'
            else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)

            if (found# = 0)
                packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowsePartFaultCodeLookup')&|
                            '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfp:Field&_sort=mfp:Field&Refresh=' & |
                            'sort&LookupFrom=FormChargeableParts&' & |
                            'fieldNumber=' & map:Field_Number & '&partType=C&'),) !lookupextra
            end ! if (found# = 0)
        end ! if (map:MainFault)
        do sendPacket
    end !if (p_web.GSV('Lookup:PartFaultCode1') = 1)
BrowseJobFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
local:FaultCodeValue STRING(255)                           !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MANFAULO)
                      Project(mfo:RecordNumber)
                      Project(mfo:Field)
                      Project(mfo:Description)
                      Project(mfo:Manufacturer)
                      Project(mfo:Field_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseJobFaultCodeLookup')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowseJobFaultCodeLookup_frm'
    End
    p_web.SSV('BrowseJobFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowseJobFaultCodeLookup:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFAULO,mfo:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFO:FIELD') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFO:DESCRIPTION') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobFaultCodeLookup:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseJobFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  if (p_web.ifExistsValue('fieldNumber'))
      p_web.storeValue('fieldNumber')
  end !if (p_web.ifExistsValue('fieldNumber'))
  if (p_web.ifExistsValue('partType'))
      p_web.storeValue('partType')
  end ! if (p_web.ifExistsValue('partType'))
  ! Has this been called from Parts Main Fault?
  if (p_web.ifExistsValue('partMainFault'))
      p_web.storeValue('partMainFault')
  end ! if (p_web.ifExistsValue('partMainFault'))
  if (p_web.ifExistsValue('relatedPartCode'))
      p_web.storeValue('relatedPartCode')
  end ! if (p-web.ifExistsValue('relatedPartCode'))
  
  !Clear queue for this user
  clear(tmpfau:Record)
  tmpfau:sessionID = p_web.SessionID
  set(tmpfau:keySessionID,tmpfau:keySessionID)
  loop
      next(tempFaultCodes)
      if (error())
          break
      end! if (error())
      if (tmpfau:sessionID <> p_web.sessionID)
          break
      end ! if (tmpfau:sessionID <> p_web.sessionID)
      delete(tempFaultCodes)
  end ! loop
  
  if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
      loop x# = 1 to 20
          if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
              cycle
          end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
          if (p_web.GSV('tmp:FaultCode' & x#) = '')
              cycle
          end ! if (p_web.GSV('tmp:FaultCode' & x#) = '')
  
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = x#
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (maf:Lookup = 'YES')
                  Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
                  mfo_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfo_ali:Field_Number    = maf:Field_Number
                  mfo_ali:Field = p_web.GSV('tmp:FaultCode' & x#)
                  if (Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign)
                      ! Found
                      found# = 0
                      Access:MANFAURL.Clearkey(mnr:FieldKey)
                      mnr:MANFAULORecordNumber    = mfo_ali:RecordNumber
  !                    mnr:PartFaultCode    = 1
                      mnr:FieldNumber    = p_web.GSV('fieldNumber')
                      set(mnr:FieldKey,mnr:FieldKey)
                      loop
                          if (Access:MANFAURL.Next())
                              Break
                          end ! if (Access:MANFAURL.Next())
                          if (mnr:MANFAULORecordNumber    <> mfo_ali:RecordNumber)
                              Break
                          end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
  !                        if (mnr:PartFaultCode    <> 1)
  !                            Break
  !                        end ! if (mnr:PartFaultCode    <> 1)
                          if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                              Break
                          end ! if (mnr:FieldNumber    <> fieldNumber)
                          found# = 1
                          break
                      end ! loop
                      if (found# = 1)
                          tmpfau:sessionID = p_web.sessionID
                          tmpfau:recordNumber = mfo_ali:RecordNumber
                          tmpfau:faultType = 'J'
                          add(tempFaultCodes)
                      end ! if (found# = 1)
                  else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
              end ! if (maf:Lookup = 'YES')
          else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  
  
          local:FaultCodeValue = ''
          Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
          wpr:Ref_Number = p_web.GSV('job:Ref_Number')
          Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
          Loop
              If Access:WARPARTS.Next()
                  Break
              End ! If Access:WARPARTS.Next()
              If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                  Break
              End ! If wpr:Ref_Number <> job:Ref_Number
              Loop f# = 1 To 12
                  Case f#
                  Of 1
                      If wpr:Fault_Code1 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code1
                  Of 2
                      If wpr:Fault_Code2 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code2
                  Of 3
                      If wpr:Fault_Code3 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code3
                  Of 4
                      If wpr:Fault_Code4 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code4
                  Of 5
                      If wpr:Fault_Code5 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code5
                  Of 6
                      If wpr:Fault_Code6 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code6
                  Of 7
                      If wpr:Fault_Code7 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code7
                  Of 8
                      If wpr:Fault_Code8 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code8
                  Of 9
                      If wpr:Fault_Code9 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code9
                  Of 10
                      If wpr:Fault_Code10 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code10
                  Of 11
                      If wpr:Fault_Code11 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code11
                  Of 12
                      If wpr:Fault_Code12 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code12
                  End ! Case
  
                  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                  map:Manufacturer = p_web.GSV('job:Manufacturer')
                  map:Field_Number = f#
                  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                      ! Found
                      If map:UseRelatedJobCode
                          Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
                          maf_ali:Manufacturer = p_web.GSV('job:Manufacturer')
                          maf_ali:MainFault = 1
                          If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                              ! Found
                              Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
                              mfo_ali:Manufacturer = p_web.GSV('job:Manufacturer')
                              mfo_ali:RelatedPartCode = map:Field_Number
                              mfo_ali:Field_Number = maf_ali:Field_Number
                              mfo_ali:Field = local:FaultCodeValue
                              If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                                  ! Found
                                  Found# = 0
                                  Access:MANFAURL.Clearkey(mnr:FieldKey)
                                  mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                                  mnr:PartFaultCode = 0
                                  mnr:FieldNumber = p_web.GSV('fieldNumber')
                                  Set(mnr:FieldKey,mnr:FieldKey)
                                  Loop
                                      If Access:MANFAURL.Next()
                                          Break
                                      End ! If Access:MANFAURL.Next()
                                      If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                          Break
                                      End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                      If mnr:PartFaultCode <> 0
                                          Break
                                      End ! If mnr:PartFaultCOde <> 1
                                      If mnr:FieldNUmber <> p_web.GSV('fieldNumber')
                                          Break
                                      End ! If mnr:FIeldNUmber <> maf:Field_Number
                                      If mnr:RelatedPartFaultCode > 0
                                          If mnr:RelatedPartFaultCode <> map:Field_Number
                                              Cycle
                                          End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
                                      End ! If mnr:RelaterdPartFaultCode > 0
                                      Found# = 1
                                      Break
                                  End ! Loop (MANFAURL)
                                  If Found# = 1
                                      tmpfau:sessionID = p_web.sessionID
                                      tmpfau:recordNumber = mfo_ali:RecordNumber
                                      tmpfau:faultType = 'J'
                                      add(tempFaultCodes)
                                  End ! If Found# = 1
  
                              Else ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                              End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                          Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                          End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                      Else ! If map:UseRelatedJobCode
                          Access:MANFPALO.Clearkey(mfp:Field_Key)
                          mfp:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfp:Field_Number = map:Field_Number
                          mfp:Field = local:FaultCodeValue
                          If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                              ! Found
                              Access:MANFPARL.Clearkey(mpr:FieldKey)
                              mpr:MANFPALORecordNumber = mfp:RecordNumber
                              mpr:JobFaultCode = 1
                              mpr:FieldNumber = p_web.GSV('fieldNumber')
                              Set(mpr:FieldKey,mpr:FieldKey)
                              Loop
                                  If Access:MANFPARL.Next()
                                      Break
                                  End ! If Access:MANFPARL.Next()
                                  If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                      Break
                                  End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                  If mpr:JobFaultCode <> 1
                                      Break
                                  End ! If mpr:JobFaultCode <> 1
                                  If mpr:FieldNumber <> p_web.GSV('fieldNumber')
                                      Break
                                  End ! If mpr:FieldNumber <> maf:Field_Number
  
                                  Found# = 1
                                  Break
                              End ! Loop (MANFPARL)
                              If Found# = 1
                                  tmpfau:sessionID = p_web.sessionID
                                  tmpfau:recordNumber = mfp:recordNumber
                                  tmpfau:faultType = 'P'
                                  add(tempFaultCodes)
                              End ! If Found# = 1
                          Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                          End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                      End ! If map:UseRelatedJobCode
                  Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
              End ! Loop f# = 1 To 20
  
          End ! Loop (WARPARTS)
  
      end
  else ! if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
      if (p_web.GSV('partMainFault') = 0)
          ! Build Associated Fault Codes Queue
  
          loop x# = 1 to 12
              if (x# = p_web.GSV('fieldNumber'))
                  cycle
              end ! if (x# = fieldNumber)
  
              if (p_web.GSV('Hide:PartFaultCode' & x#))
                  cycle
              end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
              if (p_web.GSV('tmp:FaultCodes' & x#) = '')
                  cycle
              end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
              Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
              map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
              map_ali:ScreenOrder    = x#
              if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
                  ! Found
                  Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
                  mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfp_ali:Field_Number    = map_ali:Field_Number
                  mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                  if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Found
                      found# = 0
  
                      Access:MANFPARL.Clearkey(mpr:FieldKey)
                      mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
                      mpr:FieldNumber    = p_web.GSV('fieldNumber')
                      set(mpr:FieldKey,mpr:FieldKey)
                      loop
                          if (Access:MANFPARL.Next())
                              Break
                          end ! if (Access:MANFPARL.Next())
                          if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                              Break
                          end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                          if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                              Break
                          end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                          found# = 1
                          break
                      end ! loop
  
                      if (found# = 1)
                          tmpfau:sessionID = p_web.sessionID
                          tmpfau:recordNumber = mfp_ali:recordNumber
                          tmpfau:faultType = 'P'
                          add(tempFaultCodes)
                      end ! if (found# = 1)
  
                  else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
          end
          loop x# = 1 to 20
              Access:MANFAULT.Clearkey(maf:Field_Number_Key)
              maf:Manufacturer    = p_web.GSV('job:Manufacturer')
              maf:Field_Number    = x#
              if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Found
                  if (maf:Lookup = 'YES')
  
                      Access:MANFAULO.Clearkey(mfo:Field_Key)
                      mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                      mfo:Field_Number    = maf:Field_Number
                      if (x# < 13)
                          mfo:Field = p_web.GSV('job:Fault_Code' & x#)
                      else ! if (x# < 13)
                          mfo:Field = p_web.GSV('wob:FaultCode' & x#)
                      end !if (x# < 13)
                      if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Found
                          found# = 0
                          Access:MANFAURL.Clearkey(mnr:FieldKey)
                          mnr:MANFAULORecordNumber    = mfo:RecordNumber
                          mnr:PartFaultCode    = 1
                          mnr:FieldNumber    = p_web.GSV('fieldNumber')
                          set(mnr:FieldKey,mnr:FieldKey)
                          loop
                              if (Access:MANFAURL.Next())
                                  Break
                              end ! if (Access:MANFAURL.Next())
                              if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                                  Break
                              end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                              if (mnr:PartFaultCode    <> 1)
                                  Break
                              end ! if (mnr:PartFaultCode    <> 1)
                              if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                                  Break
                              end ! if (mnr:FieldNumber    <> fieldNumber)
                              found# = 1
                              break
                          end ! loop
                          if (found# = 1)
                              tmpfau:sessionID = p_web.sessionID
                              tmpfau:recordNumber = mfo:recordNumber
                              tmpfau:faultType = 'J'
                              add(tempFaultCodes)
                          end ! if (found# = 1)
                      else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  end ! if (maf:Lookup = 'YES')
              else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
              
          end ! loop x# = 1 to 12
  
      end ! if (p_web.GSV('partMainFault') = 0)
  
  end !if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
  
  
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Field)','-UPPER(mfo:Field)')
    Loc:LocateField = 'mfo:Field'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Description)','-UPPER(mfo:Description)')
    Loc:LocateField = 'mfo:Description'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfo:Manufacturer),+mfo:Field_Number,+UPPER(mfo:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfo:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfo:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupFrom')
  End!Else
  loc:formaction = 'BrowseJobFaultCodeLookup'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFAULO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfo:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Fault Codes',0)&'</span>'&CRLF
  End
  If clip('Select Fault Codes') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobFaultCodeLookup.locate(''Locator2BrowseJobFaultCodeLookup'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseJobFaultCodeLookup_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseJobFaultCodeLookup_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseJobFaultCodeLookup','Field','Click here to sort by Field',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Field')&'">'&p_web.Translate('Field')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseJobFaultCodeLookup','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mfo:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MANFAULO{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mfo:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfo:RecordNumber'),p_web.GetValue('mfo:RecordNumber'),p_web.GetSessionValue('mfo:RecordNumber'))
      loc:FilterWas = 'Upper(mfo:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND Upper(mfo:Field_Number) = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFAULO,mfo:RecordNumberKey,loc:PageRows,'BrowseJobFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFAULO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFAULO,loc:firstvalue)
              Reset(ThisView,MANFAULO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFAULO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFAULO,loc:lastvalue)
            Reset(ThisView,MANFAULO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (mfo:NotAvailable)
          ! #11655 Don't show if not available (Bryan: 24/08/2010)
          CYCLE
      end
      
      if (p_web.GSV('partType') = 'C')
          if (mfo:RestrictLookup = 1)
              if (p_web.GSV('par:Adjustment') = 'YES')
                  if (mfo:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfo:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfo:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (p_web.GSV('partType') = 'W')
          if (mfo:RestrictLookup = 1)
              if (p_web.GSV('wpr:Adjustment') = 'YES')
                  if (mfo:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfo:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('wpr:Correction') = 1)
                      if (mfo:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfo:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (mfo:JobTypeAvailability = 1 and p_web.GSV('partType') <> '')
          if (p_web.GSV('partType') <> 'C')
              Cycle
          end ! if (p_web.GSV('job:Warranty_Job') = 'YES')
      end ! if (mfo:JobTypeAvailability = 1)
      
      if (mfo:JobTypeAvailability = 2 and p_web.GSV('partType') <> '')
          if (p_web.GSV('parType') <> 'W')
              Cycle
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
      end ! if (mfo:JobTypeAvailability = 2)
      
      
      !if (p_web.GSV('partMainFault') = 0)
          found# = 1
          clear(tmpfau:record)
          tmpfau:sessionID = p_web.sessionID
          set(tmpfau:keySessionID,tmpfau:keySessionID)
          loop
              next(tempFaultCodes)
              if (error())
                  break
              end ! if (error())
              if (tmpfau:sessionID <> p_web.sessionID)
                  break
              end ! if (tmpfau:sessionID <> p_web.sessionID)
      
              found# = 0
              case tmpfau:FaultType
              of 'P' ! Part Fault Code
                  Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
                  mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
                  mpr:LinkedRecordNumber    = mfo:RecordNumber
                  mpr:JobFaultCode    = 0
                  if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                      ! Found
                      found# = 1
                      break
                  else ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      
              of 'J' ! Job Fault Code
      
                  Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
                  mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
                  mnr:LinkedRecordNumber    = mfo:RecordNumber
                  mnr:PartFaultCode    = 1
                  if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                      ! Found
                      found# = 1
                      break
                  else ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
              end ! case faultQueue.FaultType
          end ! loop
      
          if (found# = 0)
              cycle
          end ! if (found# = 0)
      !end !if (p_web.GSV('partMainFault') = 0)
      
      if (p_web.GSV('partMainFault') = 1 and p_web.GSV('relatedPartCode') <> 0)
          if (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
              Cycle
          end ! if (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
      end ! if (p_web.GSV('relatedPartCode') <> 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfo:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup')
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobFaultCodeLookup.locate(''Locator1BrowseJobFaultCodeLookup'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup')
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mfo:RecordNumber
    p_web._thisrow = p_web._nocolon('mfo:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')) = mfo:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mfo:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseJobFaultCodeLookup.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFAULO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFAULO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFAULO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFAULO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfo:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseJobFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfo:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseJobFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfo:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfo:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseJobFaultCodeLookup.omv(this);" onMouseOut="BrowseJobFaultCodeLookup.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseJobFaultCodeLookup=new browseTable(''BrowseJobFaultCodeLookup'','''&clip(loc:formname)&''','''&p_web._jsok('mfo:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mfo:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseJobFaultCodeLookup.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobFaultCodeLookup.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  Clear(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mfo:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&mfo:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Field   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfo:Field_'&mfo:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfo:Field,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfo:Description_'&mfo:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfo:Description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  p_web.deleteSessionValue('partMainFault')
  p_web.deleteSessionValue('relatedPartCode')
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = mfo:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mfo:RecordNumber',mfo:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mfo:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfo:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfo:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseModelStock     PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(STOMODEL)
                      Project(stm:RecordNumber)
                      Project(stm:Description)
                      Project(stm:Part_Number)
                      Project(stm:Model_Number)
                      Project(stm:Location)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
STOCK::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseModelStock')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseModelStock:NoForm')
      loc:NoForm = p_web.GetValue('BrowseModelStock:NoForm')
      loc:FormName = p_web.GetValue('BrowseModelStock:FormName')
    else
      loc:FormName = 'BrowseModelStock_frm'
    End
    p_web.SSV('BrowseModelStock:NoForm',loc:NoForm)
    p_web.SSV('BrowseModelStock:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseModelStock:NoForm')
    loc:FormName = p_web.GSV('BrowseModelStock:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseModelStock') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseModelStock')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMODEL,stm:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STM:DESCRIPTION') then p_web.SetValue('BrowseModelStock_sort','2')
    ElsIf (loc:vorder = 'STM:PART_NUMBER') then p_web.SetValue('BrowseModelStock_sort','7')
    ElsIf (loc:vorder = 'STO:PURCHASE_COST') then p_web.SetValue('BrowseModelStock_sort','5')
    ElsIf (loc:vorder = 'STO:SALE_COST') then p_web.SetValue('BrowseModelStock_sort','6')
    ElsIf (loc:vorder = 'STO:QUANTITY_STOCK') then p_web.SetValue('BrowseModelStock_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseModelStock:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseModelStock:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseModelStock:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseModelStock:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseModelStock:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseModelStock_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseModelStock_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 4
    Loc:LocateField = ''
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Description)','-UPPER(stm:Description)')
    Loc:LocateField = 'stm:Description'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Part_Number)','-UPPER(stm:Part_Number)')
    Loc:LocateField = 'stm:Part_Number'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Purchase_Cost','-sto:Purchase_Cost')
    Loc:LocateField = 'sto:Purchase_Cost'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Sale_Cost','-sto:Sale_Cost')
    Loc:LocateField = 'sto:Sale_Cost'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Quantity_Stock','-sto:Quantity_Stock')
    Loc:LocateField = 'sto:Quantity_Stock'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stm:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@s30')
  Of upper('stm:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@s30')
  Of upper('sto:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warranty')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@n14.2')
  Of upper('sto:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warranty')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@n14.2')
  Of upper('sto:Quantity_Stock')
    loc:SortHeader = p_web.Translate('Quantity In Stock')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@N8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseModelStock:LookupFrom')
  End!Else
  loc:formaction = 'BrowseModelStock'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseModelStock:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseModelStock:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseModelStock:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMODEL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stm:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Stock',0)&'</span>'&CRLF
  End
  If clip('Browse Stock') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseModelStock.locate(''Locator2BrowseModelStock'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseModelStock.cl(''BrowseModelStock'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseModelStock_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseModelStock_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseModelStock','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseModelStock','Part Number','Click here to sort by Part Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Part Number')&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseModelStock','In Warranty','Click here to sort by In Warranty Cost',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by In Warranty Cost')&'">'&p_web.Translate('In Warranty')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseModelStock','Out Warranty','Click here to sort by Out Warranty Cost',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Out Warranty Cost')&'">'&p_web.Translate('Out Warranty')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseModelStock','Quantity In Stock','Click here to sort by Quantity In Stock',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Quantity In Stock')&'">'&p_web.Translate('Quantity In Stock')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('stm:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and STOMODEL{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'stm:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stm:RecordNumber'),p_web.GetValue('stm:RecordNumber'),p_web.GetSessionValue('stm:RecordNumber'))
      loc:FilterWas = 'Upper(stm:Model_Number) = Upper(''' & p_web.GSV('job:Model_Number') & ''') AND Upper(stm:Location)  = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseModelStock_Filter')
    p_web.SetSessionValue('BrowseModelStock_FirstValue','')
    p_web.SetSessionValue('BrowseModelStock_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMODEL,stm:RecordNumberKey,loc:PageRows,'BrowseModelStock',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMODEL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMODEL,loc:firstvalue)
              Reset(ThisView,STOMODEL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMODEL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMODEL,loc:lastvalue)
            Reset(ThisView,STOMODEL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (stm:Part_Number = 'EXCH')
          cycle
      end !if (stm:Part_Number = 'EXCH')
      
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = stm:Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
          cycle
      end 
      
      if (p_web.GSV('BookingRestrictParts') = 1 and |
          p_web.GSV('BookingRestrictChargeable') = 1 and |
          sto:SkillLevel > p_web.GSV('BookingSkillLevel'))
          cycle
      end ! if (p_web.GSV('BookingRestrictParts') = 1
      
      if (p_web.GSV('BookingSkillLevel') > 0)
          case p_web.GSV('BookingSkillLevel')
          of 1
              if (~sto:E1)
                  cycle
              end ! if (~sto:E1)
          of 2
              if ~(sto:E1 or sto:E2)
                  cycle
              end!if ~(sto:E1 or sto:E2)
          of 3
              if ~(sto:E1 or sto:E2 or sto:E3)
                  cycle
              end ! if ~(sto:E1 or sto:E2 or sto:E3)
          end ! case
      
      end ! if (p_web.GSV('BookingSkillLevel') > 0)
        
            
      if (sto:Suspend)
          cycle
      end ! if (stm:Part_Number = 'EXCH')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseModelStock_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseModelStock_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseModelStock.locate(''Locator1BrowseModelStock'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseModelStock.cl(''BrowseModelStock'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseModelStock_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseModelStock_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = stm:RecordNumber
    p_web._thisrow = p_web._nocolon('stm:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseModelStock:LookupField')) = stm:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((stm:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseModelStock.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMODEL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMODEL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMODEL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMODEL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stm:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseModelStock.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stm:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseModelStock.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stm:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stm:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sto:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sto:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sto:Quantity_Stock
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseModelStock.omv(this);" onMouseOut="BrowseModelStock.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseModelStock=new browseTable(''BrowseModelStock'','''&clip(loc:formname)&''','''&p_web._jsok('stm:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('stm:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseModelStock.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseModelStock.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseModelStock')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseModelStock')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseModelStock')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseModelStock')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMODEL)
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMODEL)
  Bind(stm:Record)
  Clear(stm:Record)
  NetWebSetSessionPics(p_web,STOMODEL)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('stm:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&stm:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseModelStock',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseModelStock',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseModelStock',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stm:Description_'&stm:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stm:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stm:Part_Number_'&stm:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stm:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Purchase_Cost   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sto:Purchase_Cost_'&stm:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sto:Purchase_Cost,'@n14.2')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Sale_Cost   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sto:Sale_Cost_'&stm:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sto:Sale_Cost,'@n14.2')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Quantity_Stock   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sto:Quantity_Stock_'&stm:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sto:Quantity_Stock,'@N8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(stm:Model_Number_Key)
    loc:Invalid = 'stm:Ref_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> stm:Ref_Number, stm:Manufacturer, stm:Model_Number'
  End
PushDefaultSelection  Routine
  loc:default = stm:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('stm:RecordNumber',stm:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('stm:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stm:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stm:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowsePartFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MANFPALO)
                      Project(mfp:RecordNumber)
                      Project(mfp:Field)
                      Project(mfp:Description)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowsePartFaultCodeLookup')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowsePartFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowsePartFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowsePartFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowsePartFaultCodeLookup_frm'
    End
    p_web.SSV('BrowsePartFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowsePartFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowsePartFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowsePartFaultCodeLookup:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowsePartFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowsePartFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFPALO,mfp:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFP:FIELD') then p_web.SetValue('BrowsePartFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFP:DESCRIPTION') then p_web.SetValue('BrowsePartFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowsePartFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowsePartFaultCodeLookup:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowsePartFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowsePartFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowsePartFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    if (p_web.ifExistsValue('fieldNumber'))
        p_web.storeValue('fieldNumber')
    end !if (p_web.ifExistsValue('fieldNumber'))
    if (p_web.ifExistsValue('partType'))
        p_web.storeValue('partType')
    end ! if (p_web.ifExistsValue('partType'))  
  
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  p_web.SSV('CCT',0)
  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  map:Manufacturer = p_web.GSV('job:Manufacturer')
  map:Field_Number = p_web.GSV('fieldNumber')
  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
      ! Found
      ! Is this fault a CCT? (DBH: 30/04/2008)
      If map:CCTReferenceFaultCode
          p_web.SSV('CCT',1)
      End ! If map:CCTReferenceFaultCode
  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  ! End (DBH 30/04/2008) #9723
  
  ! Build Associated Fault Codes Queue
  
  !Clear queue for this user
  clear(tmpfau:Record)
  tmpfau:sessionID = p_web.SessionID
  set(tmpfau:keySessionID,tmpfau:keySessionID)
  loop
      next(tempFaultCodes)
      if (error())
          break
      end ! if (error())
      if (tmpfau:sessionID <> p_web.sessionID)
          break
      end ! if (tmpfau:sessionID <> p_web.sessionID)
      delete(tempFaultCodes)
  end ! loop
  
  loop x# = 1 to 12
      if (x# = p_web.GSV('fieldNumber'))
          cycle
      end ! if (x# = fieldNumber)
  
      if (p_web.GSV('Hide:PartFaultCode' & x#))
          cycle
      end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
      if (p_web.GSV('tmp:FaultCodes' & x#) = '')
          cycle
      end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
      Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
      map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
      map_ali:Field_Number    = x#
      if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
          ! Found
          Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
          mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
          mfp_ali:Field_Number    = map_ali:Field_Number
          mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
          if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              ! Found
              found# = 0
  
              Access:MANFPARL.Clearkey(mpr:FieldKey)
              mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
              mpr:FieldNumber    = p_web.GSV('fieldNumber')
              set(mpr:FieldKey,mpr:FieldKey)
              loop
                  if (Access:MANFPARL.Next())
                      Break
                  end ! if (Access:MANFPARL.Next())
                  if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                      Break
                  end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                  if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                      Break
                  end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                  found# = 1
                  break
              end ! loop
  
              if (found# = 1)
                  tmpfau:sessionID = p_web.sessionID
                  tmpfau:recordNumber = mfp_ali:recordNumber
                  tmpfau:faultType = 'P'
                  add(tempFaultCodes)
              end ! if (found# = 1)
  
          else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
      else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
  
      Access:MANFAULT.Clearkey(maf:Field_Number_Key)
      maf:Manufacturer    = p_web.GSV('job:Manufacturer')
      maf:Field_Number    = x#
      if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
          ! Found
          if (maf:Lookup = 'YES')
  
              Access:MANFAULO.Clearkey(mfo:Field_Key)
              mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
              mfo:Field_Number    = maf:Field_Number
              if (x# < 13)
                  mfo:Field = p_web.GSV('job:Fault_Code' & x#)
              else ! if (x# < 13)
                  mfo:Field = p_web.GSV('wob:FaultCode' & x#)
              end !if (x# < 13)
              if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  ! Found
                  found# = 0
                  Access:MANFAURL.Clearkey(mnr:FieldKey)
                  mnr:MANFAULORecordNumber    = mfo:RecordNumber
                  mnr:PartFaultCode    = 1
                  mnr:FieldNumber    = p_web.GSV('fieldNumber')
                  set(mnr:FieldKey,mnr:FieldKey)
                  loop
                      if (Access:MANFAURL.Next())
                          Break
                      end ! if (Access:MANFAURL.Next())
                      if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                          Break
                      end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                      if (mnr:PartFaultCode    <> 1)
                          Break
                      end ! if (mnr:PartFaultCode    <> 1)
                      if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                          Break
                      end ! if (mnr:FieldNumber    <> fieldNumber)
                      found# = 1
                      break
                  end ! loop
                  if (found# = 1)
                      tmpfau:sessionID = p_web.sessionID
                      tmpfau:recordNumber = mfo:recordNumber
                      tmpfau:faultType = 'J'
                      add(tempFaultCodes)
                  end ! if (found# = 1)
              else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
          end ! if (maf:Lookup = 'YES')
      else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
      
  end ! loop x# = 1 to 12
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowsePartFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfp:Field)','-UPPER(mfp:Field)')
    Loc:LocateField = 'mfp:Field'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfp:Description)','-UPPER(mfp:Description)')
    Loc:LocateField = 'mfp:Description'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfp:Manufacturer),+mfp:Field_Number,+UPPER(mfp:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfp:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfp:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupFrom')
  End!Else
  loc:formaction = 'BrowsePartFaultCodeLookup'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowsePartFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowsePartFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFPALO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfp:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Part Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Part Fault Codes',0)&'</span>'&CRLF
  End
  If clip('Select Part Fault Codes') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowsePartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowsePartFaultCodeLookup.locate(''Locator2BrowsePartFaultCodeLookup'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePartFaultCodeLookup.cl(''BrowsePartFaultCodeLookup'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowsePartFaultCodeLookup_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowsePartFaultCodeLookup_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowsePartFaultCodeLookup','Field','Click here to sort by Field',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Field')&'">'&p_web.Translate('Field')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowsePartFaultCodeLookup','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mfp:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MANFPALO{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mfp:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfp:RecordNumber'),p_web.GetValue('mfp:RecordNumber'),p_web.GetSessionValue('mfp:RecordNumber'))
      loc:FilterWas = 'Upper(mfp:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND Upper(mfp:Field_Number) = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowsePartFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFPALO,mfp:RecordNumberKey,loc:PageRows,'BrowsePartFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFPALO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFPALO,loc:firstvalue)
              Reset(ThisView,MANFPALO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFPALO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFPALO,loc:lastvalue)
            Reset(ThisView,MANFPALO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (p_web.GSV('partType') = 'C')
          if (mfp:RestrictLookup = 1)
              if (p_web.GSV('par:Adjustment') = 'YES')
                  if (mfp:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfp:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfp:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      if (p_web.GSV('partType') = 'W')
          if (mfp:RestrictLookup = 1)
              if (p_web.GSV('wpr:Adjustment') = 'YES')
                  if (mfp:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfp:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('wpr:Correction') = 1)
                      if (mfp:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfp:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (mfp:JobTypeAvailability = 1)
          if (p_web.GSV('partType') <> 'C')
              Cycle
          end ! if (p_web.GSV('job:Warranty_Job') = 'YES')
      end ! if (mfp:JobTypeAvailability = 1)
      
      if (mfp:JobTypeAvailability = 2)
          if (p_web.GSV('partType') <> 'W')
              Cycle
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
      end ! if (mfp:JobTypeAvailability = 2)
      
      if (p_web.GSV('CCT') = 1)
          Access:MODELCCT.Clearkey(mcc:CCTRefKey)
          mcc:ModelNumber    = p_web.GSV('job:Model_Number')
          mcc:CCTReferenceNumber    = mfp:Field
          if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
              ! Found
          else ! if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
              ! Error
              Cycle
          end ! if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
      end !if (p_web.GSV('CCT') = 1)
      
      found# = 1
      clear(tmpfau:record)
      tmpfau:sessionID = p_web.sessionID
      set(tmpfau:keySessionID,tmpfau:keySessionID)
      loop
          next(tempFaultCodes)
          if (error())
              break
          end ! if (error())
          if (tmpfau:sessionID <> p_web.sessionID)
              break
          end ! if (tmpfau:sessionID <> p_web.sessionID)
      
          found# = 0
          case tmpfau:FaultType
          of 'P' ! Part Fault Code
              Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
              mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
              mpr:LinkedRecordNumber    = mfp:RecordNumber
              mpr:JobFaultCode    = 0
              if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                  ! Found
                  found# = 1
                  break
              else ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      
          of 'J' ! Job Fault Code
      
              Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
              mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
              mnr:LinkedRecordNumber    = mfp:RecordNumber
              mnr:PartFaultCode    = 1
              if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  ! Found
                  found# = 1
                  break
              else ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
          end ! case faultQueue.FaultType
      end ! loop
      
      if (found# = 0)
          cycle
      end ! if (found# = 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfp:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowsePartFaultCodeLookup')
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowsePartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowsePartFaultCodeLookup.locate(''Locator1BrowsePartFaultCodeLookup'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePartFaultCodeLookup.cl(''BrowsePartFaultCodeLookup'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowsePartFaultCodeLookup')
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mfp:RecordNumber
    p_web._thisrow = p_web._nocolon('mfp:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupField')) = mfp:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mfp:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowsePartFaultCodeLookup.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFPALO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFPALO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFPALO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFPALO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfp:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowsePartFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfp:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowsePartFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfp:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfp:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowsePartFaultCodeLookup.omv(this);" onMouseOut="BrowsePartFaultCodeLookup.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowsePartFaultCodeLookup=new browseTable(''BrowsePartFaultCodeLookup'','''&clip(loc:formname)&''','''&p_web._jsok('mfp:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mfp:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowsePartFaultCodeLookup.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowsePartFaultCodeLookup.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePartFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePartFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePartFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePartFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFPALO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFPALO)
  Bind(mfp:Record)
  Clear(mfp:Record)
  NetWebSetSessionPics(p_web,MANFPALO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mfp:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&mfp:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfp:Field   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfp:Field_'&mfp:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfp:Field,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfp:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfp:Description_'&mfp:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfp:Description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mfp:Field_Key)
    loc:Invalid = 'mfp:Manufacturer'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Field_Key --> mfp:Manufacturer, mfp:Field_Number, '&clip('Field')&''
  End
PushDefaultSelection  Routine
  loc:default = mfp:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mfp:RecordNumber',mfp:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mfp:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfp:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfp:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseStockPartFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
FaultQueue           QUEUE,PRE()                           !
ID                   STRING(20)                            !
RecordNumber         LONG                                  !
FaultType            STRING(1)                             !
                     END                                   !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(STOMPFAU)
                      Project(stu:RecordNumber)
                      Project(stu:Field)
                      Project(stu:Description)
                      Project(stu:Manufacturer)
                      Project(stu:FieldNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseStockPartFaultCodeLookup')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStockPartFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStockPartFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowseStockPartFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowseStockPartFaultCodeLookup_frm'
    End
    p_web.SSV('BrowseStockPartFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowseStockPartFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStockPartFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowseStockPartFaultCodeLookup:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStockPartFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStockPartFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMPFAU,stu:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STU:FIELD') then p_web.SetValue('BrowseStockPartFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'STU:DESCRIPTION') then p_web.SetValue('BrowseStockPartFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStockPartFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStockPartFaultCodeLookup:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseStockPartFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStockPartFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStockPartFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  if (p_web.ifExistsValue('fieldNumber'))
      p_web.storeValue('fieldNumber')
  end !if (p_web.ifExistsValue('fieldNumber'))
  if (p_web.ifExistsValue('stockRefNumber'))
      p_web.storeValue('stockRefNumber')
  end ! if (p_web.ifExistsValue('partType'))
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStockPartFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stu:Field)','-UPPER(stu:Field)')
    Loc:LocateField = 'stu:Field'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stu:Description)','-UPPER(stu:Description)')
    Loc:LocateField = 'stu:Description'
  end
  if loc:vorder = ''
    loc:vorder = '+stu:RefNumber,+stu:FieldNumber,+UPPER(stu:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stu:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_LocatorPic','@s30')
  Of upper('stu:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseStockPartFaultCodeLookup:LookupFrom')
  End!Else
  loc:formaction = 'BrowseStockPartFaultCodeLookup'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStockPartFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStockPartFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStockPartFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMPFAU"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stu:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Stock Part Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Stock Part Fault Codes',0)&'</span>'&CRLF
  End
  If clip('Select Stock Part Fault Codes') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockPartFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseStockPartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStockPartFaultCodeLookup.locate(''Locator2BrowseStockPartFaultCodeLookup'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockPartFaultCodeLookup.cl(''BrowseStockPartFaultCodeLookup'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseStockPartFaultCodeLookup_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseStockPartFaultCodeLookup_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseStockPartFaultCodeLookup','Field','Click here to sort by Field',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Field')&'">'&p_web.Translate('Field')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseStockPartFaultCodeLookup','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('stu:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and STOMPFAU{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'stu:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stu:RecordNumber'),p_web.GetValue('stu:RecordNumber'),p_web.GetSessionValue('stu:RecordNumber'))
      loc:FilterWas = 'stu:RefNumber = ' & p_web.GSV('stockRefNumber') & 'AND stu:FieldNumber = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockPartFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStockPartFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMPFAU,stu:RecordNumberKey,loc:PageRows,'BrowseStockPartFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMPFAU{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMPFAU,loc:firstvalue)
              Reset(ThisView,STOMPFAU)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMPFAU{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMPFAU,loc:lastvalue)
            Reset(ThisView,STOMPFAU)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stu:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockPartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockPartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockPartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockPartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseStockPartFaultCodeLookup')
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockPartFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseStockPartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStockPartFaultCodeLookup.locate(''Locator1BrowseStockPartFaultCodeLookup'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockPartFaultCodeLookup.cl(''BrowseStockPartFaultCodeLookup'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockPartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockPartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockPartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockPartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseStockPartFaultCodeLookup')
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = stu:RecordNumber
    p_web._thisrow = p_web._nocolon('stu:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStockPartFaultCodeLookup:LookupField')) = stu:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((stu:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseStockPartFaultCodeLookup.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMPFAU{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMPFAU)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMPFAU{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMPFAU)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stu:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseStockPartFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stu:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseStockPartFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stu:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stu:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseStockPartFaultCodeLookup.omv(this);" onMouseOut="BrowseStockPartFaultCodeLookup.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseStockPartFaultCodeLookup=new browseTable(''BrowseStockPartFaultCodeLookup'','''&clip(loc:formname)&''','''&p_web._jsok('stu:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('stu:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseStockPartFaultCodeLookup.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseStockPartFaultCodeLookup.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockPartFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockPartFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockPartFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockPartFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMPFAU)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMPFAU)
  Bind(stu:Record)
  Clear(stu:Record)
  NetWebSetSessionPics(p_web,STOMPFAU)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('stu:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&stu:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseStockPartFaultCodeLookup',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseStockPartFaultCodeLookup',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseStockPartFaultCodeLookup',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stu:Field   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stu:Field_'&stu:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stu:Field,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stu:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stu:Description_'&stu:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stu:Description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('stockRefNumber')
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = stu:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('stu:RecordNumber',stu:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('stu:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stu:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stu:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
InWarrantyMarkup     PROCEDURE  (func:Manufacturer,func:SiteLocation) ! Declare Procedure
returnValue          LONG                                  !
MANUFACT::State  USHORT
MANMARK::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do OpenFiles
    do SaveFiles

    returnValue = 0

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
        Access:MANMARK.Clearkey(mak:SiteLocationKey)
        mak:RefNumber   = man:RecordNumber
        mak:SiteLocation    = func:SiteLocation
        If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Found
            returnValue = mak:InWarrantyMarkup
        Else ! If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Error
        End !If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

    do RestoreFiles
    do CloseFiles

    return returnValue
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANMARK::State = Access:MANMARK.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANMARK::State <> 0
    Access:MANMARK.RestoreFile(MANMARK::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANMARK.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANMARK.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANMARK.Close
     FilesOpened = False
  END
Markup PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'Markup')    ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
