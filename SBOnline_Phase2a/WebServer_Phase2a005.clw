

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A024.INC'),ONCE        !Req'd for module callout resolution
                     END


SelectAccessJobStatus PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(ACCSTAT)
                      Project(acs:RecordNumber)
                      Project(acs:Status)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('SelectAccessJobStatus')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectAccessJobStatus:NoForm')
      loc:NoForm = p_web.GetValue('SelectAccessJobStatus:NoForm')
      loc:FormName = p_web.GetValue('SelectAccessJobStatus:FormName')
    else
      loc:FormName = 'SelectAccessJobStatus_frm'
    End
    p_web.SSV('SelectAccessJobStatus:NoForm',loc:NoForm)
    p_web.SSV('SelectAccessJobStatus:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectAccessJobStatus:NoForm')
    loc:FormName = p_web.GSV('SelectAccessJobStatus:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectAccessJobStatus') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectAccessJobStatus')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ACCSTAT,acs:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'ACS:STATUS') then p_web.SetValue('SelectAccessJobStatus_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectAccessJobStatus:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectAccessJobStatus:LookupFrom','LookupFrom')
    p_web.StoreValue('SelectAccessJobStatus:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectAccessJobStatus:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectAccessJobStatus:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectAccessJobStatus_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectAccessJobStatus_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(acs:Status)','-UPPER(acs:Status)')
    Loc:LocateField = 'acs:Status'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(acs:AccessArea),+UPPER(acs:Status)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('acs:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('SelectAccessJobStatus_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('SelectAccessJobStatus:LookupFrom')
  End!Else
  loc:formaction = 'SelectAccessJobStatus'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectAccessJobStatus:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectAccessJobStatus:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectAccessJobStatus:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ACCSTAT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="acs:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Status') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Status',0)&'</span>'&CRLF
  End
  If clip('Select Status') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2SelectAccessJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectAccessJobStatus.locate(''Locator2SelectAccessJobStatus'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectAccessJobStatus.cl(''SelectAccessJobStatus'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="SelectAccessJobStatus_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="SelectAccessJobStatus_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','SelectAccessJobStatus','Status','Click here to sort by Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('acs:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and ACCSTAT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'acs:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('acs:RecordNumber'),p_web.GetValue('acs:RecordNumber'),p_web.GetSessionValue('acs:RecordNumber'))
      loc:FilterWas = 'Upper(acs:AccessArea) = Upper(<39>' & p_web.GSV('filter:AccessLevel') & '<39>)'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectAccessJobStatus_Filter')
    p_web.SetSessionValue('SelectAccessJobStatus_FirstValue','')
    p_web.SetSessionValue('SelectAccessJobStatus_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ACCSTAT,acs:RecordNumberKey,loc:PageRows,'SelectAccessJobStatus',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ACCSTAT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ACCSTAT,loc:firstvalue)
              Reset(ThisView,ACCSTAT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ACCSTAT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ACCSTAT,loc:lastvalue)
            Reset(ThisView,ACCSTAT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acs:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectAccessJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectAccessJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectAccessJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectAccessJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectAccessJobStatus_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectAccessJobStatus_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1SelectAccessJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectAccessJobStatus.locate(''Locator1SelectAccessJobStatus'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectAccessJobStatus.cl(''SelectAccessJobStatus'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('SelectAccessJobStatus_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectAccessJobStatus_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectAccessJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectAccessJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectAccessJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectAccessJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = acs:RecordNumber
    p_web._thisrow = p_web._nocolon('acs:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectAccessJobStatus:LookupField')) = acs:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((acs:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="SelectAccessJobStatus.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ACCSTAT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ACCSTAT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ACCSTAT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ACCSTAT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acs:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="SelectAccessJobStatus.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acs:RecordNumber',clip(loc:field),,'checked',,,'onclick="SelectAccessJobStatus.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::acs:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="SelectAccessJobStatus.omv(this);" onMouseOut="SelectAccessJobStatus.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var SelectAccessJobStatus=new browseTable(''SelectAccessJobStatus'','''&clip(loc:formname)&''','''&p_web._jsok('acs:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('acs:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'SelectAccessJobStatus.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'SelectAccessJobStatus.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectAccessJobStatus')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectAccessJobStatus')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectAccessJobStatus')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectAccessJobStatus')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ACCSTAT)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ACCSTAT)
  Bind(acs:Record)
  Clear(acs:Record)
  NetWebSetSessionPics(p_web,ACCSTAT)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('acs:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&acs:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::acs:Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('acs:Status_'&acs:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(acs:Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = acs:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('acs:RecordNumber',acs:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('acs:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acs:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acs:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SelectNetworks       PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(NETWORKS)
                      Project(net:RecordNumber)
                      Project(net:Network)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('SelectNetworks')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectNetworks:NoForm')
      loc:NoForm = p_web.GetValue('SelectNetworks:NoForm')
      loc:FormName = p_web.GetValue('SelectNetworks:FormName')
    else
      loc:FormName = 'SelectNetworks_frm'
    End
    p_web.SSV('SelectNetworks:NoForm',loc:NoForm)
    p_web.SSV('SelectNetworks:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectNetworks:NoForm')
    loc:FormName = p_web.GSV('SelectNetworks:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectNetworks') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectNetworks')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(NETWORKS,net:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'NET:NETWORK') then p_web.SetValue('SelectNetworks_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectNetworks:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectNetworks:LookupFrom','LookupFrom')
    p_web.StoreValue('SelectNetworks:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectNetworks:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectNetworks:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectNetworks_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectNetworks_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(net:Network)','-UPPER(net:Network)')
    Loc:LocateField = 'net:Network'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(net:Network)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('net:Network')
    loc:SortHeader = p_web.Translate('Network')
    p_web.SetSessionValue('SelectNetworks_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('SelectNetworks:LookupFrom')
  End!Else
  loc:formaction = 'SelectNetworks'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectNetworks:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectNetworks:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectNetworks:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="NETWORKS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="net:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Network') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Network',0)&'</span>'&CRLF
  End
  If clip('Select Network') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectNetworks',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2SelectNetworks',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectNetworks.locate(''Locator2SelectNetworks'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectNetworks.cl(''SelectNetworks'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="SelectNetworks_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="SelectNetworks_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','SelectNetworks','Network','Click here to sort by Network',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Network')&'">'&p_web.Translate('Network')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('net:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and NETWORKS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'net:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('net:RecordNumber'),p_web.GetValue('net:RecordNumber'),p_web.GetSessionValue('net:RecordNumber'))
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectNetworks',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectNetworks_Filter')
    p_web.SetSessionValue('SelectNetworks_FirstValue','')
    p_web.SetSessionValue('SelectNetworks_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,NETWORKS,net:RecordNumberKey,loc:PageRows,'SelectNetworks',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If NETWORKS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(NETWORKS,loc:firstvalue)
              Reset(ThisView,NETWORKS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If NETWORKS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(NETWORKS,loc:lastvalue)
            Reset(ThisView,NETWORKS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(net:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectNetworks.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectNetworks.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectNetworks.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectNetworks.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectNetworks',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectNetworks_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectNetworks_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1SelectNetworks',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectNetworks.locate(''Locator1SelectNetworks'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectNetworks.cl(''SelectNetworks'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('SelectNetworks_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectNetworks_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectNetworks.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectNetworks.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectNetworks.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectNetworks.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = net:RecordNumber
    p_web._thisrow = p_web._nocolon('net:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectNetworks:LookupField')) = net:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((net:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="SelectNetworks.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If NETWORKS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(NETWORKS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If NETWORKS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(NETWORKS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','net:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="SelectNetworks.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','net:RecordNumber',clip(loc:field),,'checked',,,'onclick="SelectNetworks.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::net:Network
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="SelectNetworks.omv(this);" onMouseOut="SelectNetworks.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var SelectNetworks=new browseTable(''SelectNetworks'','''&clip(loc:formname)&''','''&p_web._jsok('net:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('net:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'SelectNetworks.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'SelectNetworks.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectNetworks')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectNetworks')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectNetworks')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectNetworks')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(NETWORKS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(NETWORKS)
  Bind(net:Record)
  Clear(net:Record)
  NetWebSetSessionPics(p_web,NETWORKS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('net:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&net:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectNetworks',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectNetworks',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectNetworks',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::net:Network   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('net:Network_'&net:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(net:Network,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(net:NetworkKey)
    loc:Invalid = 'net:Network'
    loc:Alert = clip(p_web.site.DuplicateText) & ' NetworkKey --> '&clip('Network')&' = ' & clip(net:Network)
  End
PushDefaultSelection  Routine
  loc:default = net:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('net:RecordNumber',net:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('net:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('net:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('net:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ClearUpdateJobVariables PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ClearUpdateJobVariables -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
FilesOpened     Long
JOBACC::State  USHORT
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('ClearUpdateJobVariables')
! Clear Variables
    p_web.SSV('FirstTime',1)
    p_web.SSV('Job:ViewOnly',0)
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBACC)
     FilesOpened = False
  END
IndexPage            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('IndexPage')
  loc:formname = 'IndexPage_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('IndexPage','')
    p_web._DivHeader('IndexPage',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndexPage',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndexPage',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_IndexPage',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('IndexPage_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('IndexPage_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
      ClearJobVariables(p_web)
  
      ClearUpdateJobVariables(p_web)
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferIndexPage')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('IndexPage_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('IndexPage_ChainTo')
    loc:formaction = p_web.GetSessionValue('IndexPage_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="IndexPage" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="IndexPage" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="IndexPage" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Main Menu') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Main Menu',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_IndexPage">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_IndexPage" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_IndexPage')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Login Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Rapid Procedures') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Procedures') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_IndexPage')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_IndexPage'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_IndexPage')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Login Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndexPage_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text_Site
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text_User
      do Value::text_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text_Version
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Rapid Procedures') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndexPage_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Rapid Procedures')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Rapid Procedures')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Rapid Procedures')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Rapid Procedures')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:CreateNewJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::button:JobSearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:PrintRoutines
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::button:CreateMultipleJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch Procedures') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndexPage_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Procedures')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Procedures')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Procedures')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Procedures')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonIndivdualDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonMultipleDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::blankString
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndexPage_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:LogOut
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::text_Site  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text_Site',p_web.GetValue('NewValue'))
    do Value::text_Site
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text_Site  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('text_Site') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('LoginDetails1'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::text_User  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('text_User') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text_User  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text_User',p_web.GetValue('NewValue'))
    do Value::text_User
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text_User  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('text_User') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('LoginDetails2'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::text_Version  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text_Version',p_web.GetValue('NewValue'))
    do Value::text_Version
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text_Version  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('text_Version') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('VersionNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::Button:CreateNewJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CreateNewJob',p_web.GetValue('NewValue'))
    do Value::Button:CreateNewJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:CreateNewJob  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('Button:CreateNewJob') & '_value',Choose(p_web.GSV('Hide:ButtonCreateNewJob') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonCreateNewJob') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('Submit','Create Single Job','Create Single Job','DoubleButton',loc:formname,'PreNewJobBooking?MultipleJobBooking=0','_self',,loc:javascript,Choose(lower('_self') = '_self',-1,0),,,,,'Book a new job')

  do SendPacket
  End
  p_web._DivFooter()


Validate::button:JobSearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:JobSearch',p_web.GetValue('NewValue'))
    do Value::button:JobSearch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:JobSearch  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('button:JobSearch') & '_value',Choose(p_web.GSV('Hide:ButtonJobSearch') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonJobSearch') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','JobSearch','Job Search','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobSearch')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,'Search for a job')

  do SendPacket
  End
  p_web._DivFooter()


Validate::Button:PrintRoutines  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:PrintRoutines',p_web.GetValue('NewValue'))
    do Value::Button:PrintRoutines
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:PrintRoutines  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('Button:PrintRoutines') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintRoutines','Print Routines','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PrintRoutines')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,'Print paperwork')

  do SendPacket
  p_web._DivFooter()


Validate::button:CreateMultipleJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:CreateMultipleJobs',p_web.GetValue('NewValue'))
    do Value::button:CreateMultipleJobs
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:CreateMultipleJobs  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('button:CreateMultipleJobs') & '_value',Choose(p_web.GSV('Hide:ButtonCreateNewJob') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonCreateNewJob') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateMultipleJobs','Create Multiple Jobs','DoubleButton',loc:formname,,,,loc:javascript,0,,,,,'Book multiple jobs')

  do SendPacket
  End
  p_web._DivFooter()


Validate::buttonIndivdualDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonIndivdualDespatch',p_web.GetValue('NewValue'))
    do Value::buttonIndivdualDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonIndivdualDespatch  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('buttonIndivdualDespatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','IndividualDespatch','Individual Despatch','DoubleButton',loc:formname,,,,loc:javascript,0,,,,,'Despatch single jobs')

  do SendPacket
  p_web._DivFooter()


Validate::buttonMultipleDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonMultipleDespatch',p_web.GetValue('NewValue'))
    do Value::buttonMultipleDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonMultipleDespatch  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('buttonMultipleDespatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','MultipleDespatch','Multiple Despatch','DoubleButton',loc:formname,,,,loc:javascript,0,,,,,'Despatch batches of jobs')

  do SendPacket
  p_web._DivFooter()


Validate::blankString  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('blankString',p_web.GetValue('NewValue'))
    do Value::blankString
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::blankString  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('blankString') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('DoubleButton hidden')&'">' & p_web.Translate(' ',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::Button:LogOut  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:LogOut',p_web.GetValue('NewValue'))
    do Value::Button:LogOut
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:LogOut  Routine
  p_web._DivHeader('IndexPage_' & p_web._nocolon('Button:LogOut') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Logout','Log Out','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('LoginForm')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/pcancel.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_IndexPage',0)

PreCopy  Routine
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_IndexPage',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('IndexPage:Primed',0)

PreDelete       Routine
  p_web.SetValue('IndexPage_form:ready_',1)
  p_web.SetSessionValue('IndexPage_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('IndexPage:Primed',0)
  p_web.setsessionvalue('showtab_IndexPage',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('IndexPage_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('IndexPage_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 5
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 6
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('IndexPage:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
ClearJobVariables    PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ClearJobVariables -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('ClearJobVariables')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    ! Clear Insert Job Variables
    p_web.DeleteSessionValue('tmp:ESN')
    p_web.DeleteSessionValue('tmp:TransitType')
    p_web.DeleteSessionValue('tmp:Manufacturer')
    p_web.DeleteSessionValue('tmp:ModelNumber')
    If p_web.gsv('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:ARC = 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:RRC = 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'
    p_web.SSV('tmp:TransitType',GETINI(p_web.GetSessionValue('BookingAccount'), 'InitialTransitType',, CLIP(PATH()) & '\SB2KDEF.INI'))
    p_web.SSV('Comment:TransitType','Required')
    p_web.DeleteSessionValue('Comment:IMEINumber')
    p_web.SSV('Comment:DOP','dd/mm/yyyy')

    p_web.DeleteSessionValue('Filter:Manufacturer')
    p_web.DeleteSessionValue('Filter:ModelNumber')

    p_web.SSV('Hide:IMEINumberButton',1)

    p_web.DeleteSessionValue('OBFValidation')
    p_web.DeleteSessionValue('Prompt:Validation')
    p_web.DeleteSessionValue('Error:Validation')
    p_web.DeleteSessionValue('Passed:Validation')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:OldDOP')
    p_web.SSV('Hide:ChangeDOP',1)
    p_web.SSV('Hide:ChangeDOPButton',1)
    p_web.DeleteSessionValue('tmp:NewDOP')
    p_web.DeleteSessionValue('tmp:ChangeReason')


    p_web.DeleteSessionValue('tmp:DateOfPurchase')

    p_web.DeleteSessionValue('Save:DOP')
    p_web.DeleteSessionValue('Required:DOP')
    p_web.DeleteSessionValue('ReadOnly:DOP')
    p_web.SSV('ReadOnly:Manufacturer',1)
    p_web.SSV('ReadOnly:ModelNumber',1)
    p_web.DeleteSessionValue('ReadOnly:TransitType')
    p_web.DeleteSessionValue('ReadOnly:IMEINumber')
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')

    p_web.DeleteSessionValue('tmp:Network')
    p_web.DeleteSessionValue('tmp:ReturnDate')
    p_web.DeleteSessionValue('tmp:DateOfPurchase')
    p_web.DeleteSessionValue('tmp:BOXIMEINumber')
    p_web.DeleteSessionValue('tmp:BranchOfReturn')
    p_web.DeleteSessionValue('tmp:LAccountNumber')
    p_web.DeleteSessionValue('tmp:OriginalAccessories')
    p_web.DeleteSessionValue('tmp:OriginalDealer')
    p_web.DeleteSessionValue('tmp:OriginalManuals')
    p_web.DeleteSessionValue('tmp:OriginalPackaging')
    p_web.DeleteSessionValue('tmp:PhysicalDamage')
    p_web.DeleteSessionValue('tmp:ProofOfPurchase')
    p_web.DeleteSessionValue('tmp:Replacement')
    p_web.DeleteSessionValue('tmp:ReplacementIMEINumber')
    p_web.DeleteSessionValue('StoreReferenceNumber')
    p_web.DeleteSessionValue('tmp:TalkTime')
    p_web.SSV('Hide:PreviousAddress',1)
    p_web.DeleteSessionValue('tmp:StoreReferenceNumber')

    p_web.SSV('ReadyForNewJobBooking',1)
    p_web.SSV('FirstTime',1)


    p_web.DeleteSessionValue('Required:Engineer')

    p_web.DeleteSessionValue('tmp:ExternalDamageCheckList')
    p_web.DeleteSessionValue('tmp:AmendFaultCodes')

    p_web.DeleteSessionValue('tmp:AmendAddressess')

    p_web.DeleteSessionValue('FranchiseAccount')


    p_web.DeleteSessionValue('tmp:AmendAccessories')
    p_web.DeleteSessionValue('tmp:ShowAccessory')
    p_web.DeleteSessionValue('tmp:TheAccessory')
    p_web.DeleteSessionValue('tmp:TheJobAccessory')
    p_web.DeleteSessionValue('tmp:AmendAddresses')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:POPType')
    p_web.DeleteSessionValue('tmp:PreviousAddress')
    p_web.DeleteSessionValue('tmp:UsePreviousAddress')
    p_web.DeleteSessionValue('AmendFaultCodes')
    p_web.DeleteSessionValue('tmp:FaultCode1')
    p_web.DeleteSessionValue('tmp:FaultCode2')
    p_web.DeleteSessionValue('tmp:FaultCode3')
    p_web.DeleteSessionValue('tmp:FaultCode4')
    p_web.DeleteSessionValue('tmp:FaultCode5')
    p_web.DeleteSessionValue('tmp:FaultCode6')
    p_web.DeleteSessionValue('tmp:ExternalDamageChecklist')
    p_web.DeleteSessionValue('filter:WarrantyChargeTYpe')
    p_web.DeleteSessionValue('Comment:POP')
    p_web.SSV('Drop:JobType','-------------------------------------')
    p_web.DeleteSessionValue('Drop:JobTypeDefault')


    ! --- Clear passed mq variables ----
    ! Inserting:  DBH 30/01/2009 #N/A
    p_web.DeleteSessionValue('mq:ChargeableJob')
    p_web.DeleteSessionValue('mq:Charge_Type')
    p_web.DeleteSessionValue('mq:WarrantyJob')
    p_web.DeleteSessionValue('mq:Warranty_Charge_Type')
    p_web.DeleteSessionValue('mq:DOP')
    p_web.DeleteSessionValue('mq:POP')
    p_web.DeleteSessionValue('mq:IMEIValidation')
    p_web.DeleteSessionValue('mq:IMEIValidationText')
    ! End: DBH 30/01/2009 #N/A
    ! -----------------------------------------

!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
JobSearch            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobPassword       STRING(30)                            !
locSearchJobNumber   STRING(20)                            !
locPasswordMessage   STRING(100)                           !
FilesOpened     Long
USERS::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobSearch')
  loc:formname = 'JobSearch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobSearch','Change')
    p_web._DivHeader('JobSearch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobSearch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobSearch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locJobPassword')
    p_web.DeleteSessionValue('locSearchJobNumber')
    p_web.DeleteSessionValue('locPasswordMessage')

    ! Other Variables

ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString CSTRING(30)
CODE
    
    IF (p_web.GSV('locJobFound') = 1)
        paymentFail# = 0
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            locPaymentString = locPaymentStringRRC
        ELSE
            locPaymentString = locPaymentStringARC
        END
    
        IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('locSearchJobNumber')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                IF (job:Loan_Unit_Number = 0)
                    IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
                        paymentFail# = 1
                    END
                END
            END
        ELSE
            paymentFail# = 1
        END
        IF (paymentFail# = 1)
            p_web.SSV('Hide:PaymentDetails',1)
            p_web.SSV('Comment:PaymentDetails','')
        ELSE
            p_web.SSV('Hide:PaymentDetails',0)
        END
    END
    
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Clear Fields
  DO DeleteSessionValues
  p_web.SSV('Hide:PaymentDetails',1)
  p_web.SSV('Hide:ViewJobButton',1)
  p_web.SSV('comment:locSearchJobNumber','Enter Job Number and press [TAB]')
  p_web.SSV('comment:locJobPassword','Enter Password and press [TAB]')
  p_web.SSV('locJobFound',0)
  p_web.SSV('locPasswordRequired',0)
  
  p_web.site.CancelButton.TextValue = 'Close'
  
  ClearUpdateJobVariables(p_web)
  p_web.SetValue('JobSearch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber)
  p_web.SetSessionValue('locPasswordMessage',locPasswordMessage)
  p_web.SetSessionValue('locJobPassword',locJobPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locSearchJobNumber')
    locSearchJobNumber = p_web.GetValue('locSearchJobNumber')
    p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber)
  End
  if p_web.IfExistsValue('locPasswordMessage')
    locPasswordMessage = p_web.GetValue('locPasswordMessage')
    p_web.SetSessionValue('locPasswordMessage',locPasswordMessage)
  End
  if p_web.IfExistsValue('locJobPassword')
    locJobPassword = p_web.GetValue('locJobPassword')
    p_web.SetSessionValue('locJobPassword',locJobPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobSearch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locSearchJobNumber = p_web.RestoreValue('locSearchJobNumber')
 locPasswordMessage = p_web.RestoreValue('locPasswordMessage')
 locJobPassword = p_web.RestoreValue('locJobPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferJobSearch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobSearch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobSearch_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobSearch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobSearch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobSearch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobSearch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Search') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Search',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobSearch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobSearch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobSearch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobSearch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobSearch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locSearchJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobSearch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Job Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobSearch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSearchJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSearchJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSearchJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobSearch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobSearch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::button:OpenJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:OpenJob
      do Comment::button:OpenJob
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonViewJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonViewJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPaymentDetails
      do Comment::buttonPaymentDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locSearchJobNumber  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSearchJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSearchJobNumber',p_web.GetValue('NewValue'))
    locSearchJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSearchJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSearchJobNumber',p_web.GetValue('Value'))
    locSearchJobNumber = p_web.GetValue('Value')
  End
  If locSearchJobNumber = ''
    loc:Invalid = 'locSearchJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  ! Validate job
  p_web.SSV('Hide:ViewJobButton',1)
  fail# = 0
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber    =  p_web.GSV('locSearchJobNumber')
  if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      ! Found
      p_web.SSV('locSearchRecordNumber',wob:RecordNumber)
      if (p_web.GSV('BookingSite') = 'RRC')
          if( wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
              p_web.SSV('comment:locSearchJobNumber','Error: The selected job was not booked at this site')
              fail# = 1
          end ! if( wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
      end ! if (p_web.GSV('BookingSite') = 'RRC')
  else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      ! Error
      p_web.SSV('comment:locSearchJobNumber','Error: Cannot find selected Job Number')
      fail# = 1
  end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
  
  if (fail# = 1)
      p_web.SSV('locJobFound',0)
      P_web.SSV('locSearchRecordNumber','')
      p_web.SSV('Hide:PaymentDetails',1)
  else ! if (fail# = 1)
      
      p_web.SSV('comment:locSearchJobNumber','Job Found')
  
      p_web.SSV('locPasswordRequired',0)
      p_web.SSV('locJobFound',1)
      p_web.SSV('Hide:ViewJobButton',1)
  
      
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = wob:RefNumber
      IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
      END
      p_web.FileToSessionQueue(JOBS)
      
      Access:JOBSE.ClearKey(jobe:RefNumberKey)
      jobe:RefNumber = job:Ref_Number
      IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
      END
      p_web.FileToSessionQueue(JOBSE)
      
      
      
      IF (p_web.GSV('BookingSite') = 'RRC')
          
          IF (Inlist(job:Location,p_web.GSV('Default:RRCLocation'), |
              p_web.GSV('Default:DespatchToCustomer'), |
              p_web.GSV('Default:InTransitPUP'), |
              p_web.GSV('Default:PUPLocation')) > 0)
              SentToHub(p_web)
              ! Job is RRC and at RRC
              IF (job:Warranty_Job = 'YES' AND wob:EDI <> 'XXX')
                  IF (job:Date_Completed <> '' AND p_web.GSV('SentToHub') = 0)
                      IF (wob:EDI = 'NO' OR wob:EDI = 'YES' OR wob:EDI = 'PAY' | 
                          OR wob:EDI = 'APP')
                          ! Job should be disabled
                      ELSE
                          p_web.SSV('locJobFound',0)
                          p_web.SSV('locPasswordRequired',1)
                          
                          p_web.SSV('comment:locSearchJobNumber','Job Found: Password required.')
  
                          p_web.SSV('locPasswordRequired',1)
                          p_web.SSV('locJobFound',0)
                          p_web.SSV('Hide:ViewJobButton',0)
                          p_web.SSV('locPasswordMessage','The selected warranty job has been rejected. Enter password to EDIT the job, or click "View Job" to view the job.')
                      END
                  END
              END
          END
      END ! IF (p_web.GSV('BookingSite') = 'RRC')
      
     
      DO ShowHidePaymentDetails
      
  end ! if (fail# = 0)
  do Value::locSearchJobNumber
  do SendAlert
  do Comment::locSearchJobNumber
  do Value::button:OpenJob  !1
  do Value::buttonPaymentDetails  !1
  do Comment::buttonPaymentDetails
  do Prompt::locJobPassword
  do Value::locJobPassword  !1
  do Comment::locJobPassword
  do Value::buttonViewJob  !1
  do Value::locPasswordMessage  !1

Value::locSearchJobNumber  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSearchJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locSearchJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locSearchJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSearchJobNumber'',''jobsearch_locsearchjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSearchJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSearchJobNumber',p_web.GetSessionValueFormat('locSearchJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_value')

Comment::locSearchJobNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:locSearchJobNumber'))
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_comment')

Validate::locPasswordMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPasswordMessage',p_web.GetValue('NewValue'))
    locPasswordMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPasswordMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPasswordMessage',p_web.GetValue('Value'))
    locPasswordMessage = p_web.GetValue('Value')
  End

Value::locPasswordMessage  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_value',Choose(p_web.GSV('locPasswordMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordMessage') = '')
  ! --- DISPLAY --- locPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPasswordMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_value')

Comment::locPasswordMessage  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_comment',Choose(p_web.GSV('locPasswordMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobPassword  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_prompt',Choose(p_web.GSV('locPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('locPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locJobPassword') & '_prompt')

Validate::locJobPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobPassword',p_web.GetValue('NewValue'))
    locJobPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobPassword',p_web.GetValue('Value'))
    locJobPassword = p_web.GetValue('Value')
  End
    locJobPassword = Upper(locJobPassword)
    p_web.SetSessionValue('locJobPassword',locJobPassword)
  ! Check password
  p_web.SSV('locJobFound',0)
  Access:USERS.ClearKey(use:password_key)
  use:password = p_web.GSV('locJobPassword')
  IF (Access:USERS.tryfetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'EDIT REJECTED WARRANTY JOB'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('locJobFound',1)    
          p_web.SSV('comment:locJobPassword','Password accepted')
          
          DO ShowHidePaymentDetails
      ELSE
          p_web.SSV('comment:locJobPassword','User does not have access to edit job.')
      END
          
      ELSE
      p_web.SSV('comment:locJobPassword','Invalid password')
  END
  do Value::locJobPassword
  do SendAlert
  do Value::buttonPaymentDetails  !1
  do Value::button:OpenJob  !1
  do Comment::locJobPassword
  do Value::locSearchJobNumber  !1

Value::locJobPassword  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_value',Choose(p_web.GSV('locPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordRequired') <> 1)
  ! --- STRING --- locJobPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locJobFound') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locJobFound') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locJobPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobPassword'',''jobsearch_locjobpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locJobPassword',p_web.GetSessionValueFormat('locJobPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locJobPassword') & '_value')

Comment::locJobPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:locJobPassword'))
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_comment',Choose(p_web.GSV('locPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locJobPassword') & '_comment')

Prompt::button:OpenJob  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::button:OpenJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:OpenJob',p_web.GetValue('NewValue'))
    do Value::button:OpenJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:OpenJob  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','OpenJob','Edit Job','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Change_btn=Change&BackURL=JobSearch')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/zoom.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('button:OpenJob') & '_value')

Comment::button:OpenJob  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewJob',p_web.GetValue('NewValue'))
    do Value::buttonViewJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewJob  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonViewJob') & '_value',Choose(p_web.GSV('Hide:ViewJobButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ViewJobButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewJob','View Job','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Change_btn=Change&BackURL=JobSearch&ViewOnly=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/zoom.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('buttonViewJob') & '_value')

Comment::buttonViewJob  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonViewJob') & '_comment',Choose(p_web.GSV('Hide:ViewJobButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ViewJobButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPaymentDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPaymentDetails',p_web.GetValue('NewValue'))
    do Value::buttonPaymentDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPaymentDetails  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_value',Choose(p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PaymentDetails','Payment Details','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=JobSearch')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_value')

Comment::buttonPaymentDetails  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PaymentDetails'))
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_comment',Choose(p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobSearch_locSearchJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSearchJobNumber
      else
        do Value::locSearchJobNumber
      end
  of lower('JobSearch_locJobPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobPassword
      else
        do Value::locJobPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobSearch',0)

PreCopy  Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobSearch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobSearch:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobSearch:Primed',0)
  p_web.setsessionvalue('showtab_JobSearch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobSearch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobSearch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locSearchJobNumber = ''
          loc:Invalid = 'locSearchJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('locPasswordRequired') <> 1)
          locJobPassword = Upper(locJobPassword)
          p_web.SetSessionValue('locJobPassword',locJobPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobSearch:Primed',0)
  p_web.StoreValue('locSearchJobNumber')
  p_web.StoreValue('locPasswordMessage')
  p_web.StoreValue('locJobPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
CopyDOPFromBouncer   PROCEDURE  (String f:Manufacturer, Date f:PreviousDOP, *Date f:DOP,  String f:IMEINumber, String f:Bouncer, *Byte f:Found) ! Declare Procedure
    include('CopyDOPFromBouncer.inc','Local Data')
  CODE
    include('CopyDOPFromBouncer.inc','Processed Code')
PreNewJobBooking     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:TransitType      STRING(30)                            !Transit Type
tmp:ESN              STRING(30)                            !I.M.E.I. Number
tmp:Manufacturer     STRING(30)                            !Manufacturr
tmp:ModelNumber      STRING(30)                            !Model Number
filter:Manufacturer  STRING(1000)                          !filter:Manufacturer
filter:ModelNumber   STRING(1000)                          !filter:ModelNumber
tmp:DOP              DATE                                  !
tmp:NewDOP           DATE                                  !
tmp:ChangeDOPReason  STRING(255)                           !
mj:ModelNumber       BYTE                                  !
mj:Colour            BYTE                                  !
mj:OrderNumber       BYTE                                  !
mj:FaultCode         BYTE                                  !
mj:EngineersNotes    BYTE                                  !
mj:ContactHistory    BYTE                                  !
local       Class
CheckLength         Procedure(String f:Type),Byte
IMEIModelNumber     Procedure(),Byte
IsIMEIValid         Procedure(),Byte
ValidateIMEI        Procedure(),Byte
ValidateMSN         Procedure(),Byte
            End ! local       Class
!MQ Setup
mqFileName  string(260), static
! Input / Output files - pipe delimited (|)
MQIInputFile file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQIF), name(mqFileName), create
MQIFRec         record
imei                string(30)
                end ! record
            end ! file

MQIOutputFile file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQOF), name(mqFileName)
MQOFRec         record
activationDate      string(255)
activity            string(255)
warranty            string(255)
blackGrey           string(255)
supplier            string(255)
swap                string(255)
usage               string(255)
statusCode          string(255)
statusDescription   string(255)
                end ! record
            end ! file



FilesOpened     Long
JOBS::State  USHORT
TRANTYPE::State  USHORT
MANUFACT::State  USHORT
MODELNUM::State  USHORT
ESNMODEL::State  USHORT
ESNMODAL::State  USHORT
JOBS_ALIAS::State  USHORT
EXCHANGE::State  USHORT
LOAN::State  USHORT
USERS::State  USHORT
ACCAREAS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PreNewJobBooking')
  loc:formname = 'PreNewJobBooking_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PreNewJobBooking','')
    p_web._DivHeader('PreNewJobBooking',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PreNewJobBooking',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      ROUTINE
    !Multiple Job Booking
    p_web.DeleteSessionValue('mj:InProgress')
    p_web.DeleteSessionValue('mj:Colour')
    p_web.DeleteSessionValue('mj:OrderNumber')
    p_web.DeleteSessionValue('mj:FaultCode')
    p_web.DeleteSessionValue('mj:EngineersNotes')
    p_web.DeleteSessionValue('mj:PreviousJobNumber')
    

CheckManModel       Routine
    Data
local:LastJobNumber     Long()
local:Bouncers  Long()
local:PreviousDOP       Date()
local:Found     Byte(0)
    Code
        Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
        mod:Manufacturer = p_web.GSV('tmp:Manufacturer')
        mod:Model_Number = p_web.GSV('tmp:ModelNumber')
        If Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
            p_web.SSV('tmp:ModelNumber','')
            p_web.SSV('Hide:IMEINumberButton',1)
            Exit
        End ! If Access:MODELNUM.TryFetch(mod:Manufacturer_Key)

        If p_web.GSV('tmp:Manufacturer') <> '' And p_web.GSV('tmp:ModelNumber') <> '' And p_web.GSV('tmp:TransitType') <> '' And p_web.GSV('tmp:ESN') <> ''
            p_web.SSV('Filter:Manufacturer','man:Manufacturer = <39>' & p_web.GSV('tmp:Manufacturer') & '<39>')
            p_web.SSV('Drop:ManufacturerDefault',p_web.GSV('tmp:Manufacturer'))
            p_web.SSV('Filter:ModelNumber','mod:Model_Number = <39>' & p_web.GSV('tmp:ModelNumber') & '<39>')
            p_web.SSV('Drop:ModelNumberDefault',p_web.GSV('tmp:ModelNumber'))
            p_web.SSV('Hide:IMEINumberButton','')

            Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
            trt:Transit_Type = p_web.GSV('tmp:TransitType')
            If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                If trt:OBF
                    p_web.SSV('OBFValidation','Pending')
                Else ! If trt:OBF
                    p_web.SSV('OBFValidation','')
                End ! If trt:OBF
            End ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign

            If p_web.GSV('OBFValidation') = 'Pending'
                p_web.SSV('IMEINumberButtonURL','OBFValidation')
            Else ! If p_web.SSV('OBFValidation','Pending')
                p_web.SSV('IMEINumberButtonURL','NewJobBooking?&Insert_btn=Insert&')
            End ! If p_web.SSV('OBFValidation','Pending')
            p_web.SSV('ReadOnly:Manufacturer',1)
            p_web.SSV('ReadOnly:ModelNumber',1)
            p_web.SSV('ReadOnly:TransitType',1)
            p_web.SSV('ReadOnly:IMEINumber',1)

            local:LastJobNumber = 0
            local:Bouncers = 0
            Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
            job_ali:ESN    = p_web.GSV('tmp:ESN')
            set(job_ali:ESN_Key,job_ali:ESN_Key)
            loop
                if (Access:JOBS_ALIAS.Next())
                    Break
                end ! if (Access:JOBS_ALIAS.Next())
                if (job_ali:ESN    <> p_web.GSV('tmp:ESN'))
                    Break
                end ! if (job_ali:ESN    <> p_web.GSV('tmp:ESN'))
                if (job_ali:Cancelled = 'YES')
                    cycle
                end !if (job_ali:Cancelled = 'YES')
                if (job_ali:Ref_Number > local:LastJobNumber)
                ! Get the recent job DOP
                    local:LastJobNumber = job_ali:Ref_Number
                    local:PreviousDOP = job_ali:DOP
                end ! if (job_ali:Ref_Number > lastJobNumber#)
                local:Bouncers += 1
            end ! loop

        ! Insert --- Give user option to change DOP (DBH: 25/02/2009) #10591
            CopyDOPFromBouncer(p_web.GSV('tmp:Manufacturer'),local:PreviousDOP,job:DOP,p_web.GSV('tmp:ESN'),local:Bouncers,local:Found)
            if (local:Found)
                p_web.SSV('job:DOP',job:DOP)
                p_web.SSV('Hide:ChangeDOP',1)
                p_web.SSV('Hide:ChangeDOPButton',1)
                p_web.SSV('mq:DOP',job:DOP)
                p_web.SSV('tmp:DateOfPurchase',job:DOP)
                p_web.SSV('Hide:ChangeDOP',0)
            end ! if (local:Found)
            ! end --- (DBH: 25/02/2009) #10591
            
            Do MQ:Process
            
            ! #11344 Check One Year Warranty again, just incase model/manufact hadn't been filled in (DBH: 10/06/2010)
            IF (p_web.GSV('job:POP') = 'S')
                If (IsOneYearWarranty(p_web.GSV('tmp:Manufacturer'),p_web.GSV('tmp:ModelNumber')))
                    p_web.SSV('job:Chargeable_Job','YES')
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                    p_web.SSV('job:Warranty_Job','NO')
                    p_web.SSV('job:Warranty_Charge_Type','')
                    p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    This Phone must be repaired out of Warranty')
                    p_web.SSV('job:POP','C')
                    p_web.SSV('mq:ChargeableJob',p_web.GSV('job:Chargeable_Job'))
                    p_web.SSV('mq:Charge_Type',p_web.GSV('job:Charge_Type'))
                    p_web.SSV('mq:WarrantyJob',p_web.GSV('job:Warranty_Job'))
                    p_web.SSV('mq:Warranty_Charge_Type',p_web.GSV('job:Warranty_Charge_Type'))
                    p_web.SSV('mq:DOP',job:DOP)
                    p_web.SSV('mq:POP',p_web.GSV('job:POP'))
                    p_web.SSV('tmp:DateOfPurchase',p_web.GSV('job:DOP'))
                    p_web.SSV('mq:IMEIValidation',p_web.GSV('IMEIValidation'))
                    p_web.SSV('mq:IMEIValidationText','This Phone must be repaired out of Warranty')
                       
                end                
            END
            
        Else
            p_web.SSV('Hide:IMEINumberButton',1)
        End ! If p_web.GSV('tmp:Manufacturer') <> '' And p_web.GSV('tmp:ModelNumber') <> ''
MQ:Process          Routine
    Data
local:SystemError       Byte(0)
local:Error     Byte(0)
local:ErrorMessage      String(255)
locOneYearWarranty      STRING(1)
    Code
        IF (p_web.GSV('tmp:ESN') = '')
            EXIT
        END
        ! #11344 Don't validate if man/model aren't filled in. (DBH: 29/06/2010)
        IF (p_web.GSV('tmp:Manufacturer') = '' OR p_web.GSV('tmp:ModelNumber') = '')
            EXIT
        END
            
        tmp:ESN = p_web.GSV('tmp:ESN')

        locOneYearWarranty = 'N'
        
        If (IsOneYearWarranty(p_web.GSV('tmp:Manufacturer'),p_web.GSV('tmp:ModelNumber')))
            locOneYearWarranty = 'Y'
        else
            locOneYearWarranty = 'N'
        end

        local:SystemError = MQProcess(tmp:ESN,job:POP,job:DOP,local:Error,local:errorMessage,locOneYearWarranty)

        if (local:SystemError)
            p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    ' &  Clip(local:ErrorMessage))
            p_web.SSV('IMEIValidation','Failed')
            p_web.SSV('IMEIValidationText',local:ErrorMessage)
        ELSE

            p_web.SSV('job:DOP',job:DOP)
            p_web.SSV('tmp:DOP',p_web.GSV('job:DOP'))
            p_web.SSV('IMEIActivationDate',format(job:DOP,@d06))
            p_web.SSV('job:POP',job:POP)

            case job:POP
            of 'C' ! Chargeable
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Charge_Type','NON-WARRANTY')
                p_web.SSV('job:Warranty_Job','NO')
                p_web.SSV('job:Warranty_Charge_Type','')
                p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    ' & Clip(local:ErrorMessage))
                p_web.SSV('job:POP','C')
            of 'F' ! First Year Warranty
                p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    This IMEI can be reparied under the First Year Manufacturer Warranty as per manufacturer criteria.')
                p_web.SSV('job:Warranty_Job','YES')
                p_web.SSV('job:Warranty_Charge_Type','WARRANTY (MFTR)')
                p_web.SSV('job:POP','F')
            of 'S' ! Second Year Warranty
                p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    The IMEI can be repaired under the Second Year Warranty.')
                p_web.SSV('job:Warranty_Job','YES')
                p_web.SSV('job:Warranty_Charge_Type','WARRANTY (2ND YR)')
                p_web.SSV('job:POP','S')
            end ! case job:POP

            p_web.SSV('IMEIValidation','Passed')
            p_web.SSV('IMEIValidationText',Clip(local:ErrorMessage))

            p_web.SSV('mq:ChargeableJob',p_web.GSV('job:Chargeable_Job'))
            p_web.SSV('mq:Charge_Type',p_web.GSV('job:Charge_Type'))
            p_web.SSV('mq:WarrantyJob',p_web.GSV('job:Warranty_Job'))
            p_web.SSV('mq:Warranty_Charge_Type',p_web.GSV('job:Warranty_Charge_Type'))
            p_web.SSV('mq:DOP',job:DOP)
            p_web.SSV('mq:POP',p_web.GSV('job:POP'))
            p_web.SSV('tmp:DateOfPurchase',p_web.GSV('job:DOP'))

            p_web.SSV('mq:IMEIValidation',p_web.GSV('IMEIValidation'))
            p_web.SSV('mq:IMEIValidationText',Clip(local:ErrorMessage))

        end ! if (local:SystemError)
        
        IF (IsUnitLiquidDamaged(p_web.GSV('tmp:ESN'),0,TRUE) = TRUE)
            p_web.SSV('mq:ChargeableJob','YES')
            p_web.SSV('mq:WarrantyJob','NO')
            p_web.SSV('mq:Charge_Type','NON-WARRANTY')
            p_web.SSV('mq:Warranty_Charge_Type','')
            p_web.SSV('mq:POP','C')
            local:ErrorMessage = 'This IMEI must be repaired out of warranty due to the following reason: ' & |
                'Previous Repair, handset deemed Liquid Damage.'
            p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    ' & CLIP(local:ErrorMessage))  
            p_web.SSV('mq:IMEIValidationText',Clip(local:ErrorMessage))
        END 
    
Reset:IMEINumber     Routine
    p_web.SetSessionValue('Drop:Manufacturer','- Enter A Valid IMEI Number -')
    p_web.SetSessionValue('Drop:ModelNumber','- Enter A Valid Manufacturer -')
    p_web.SetSessionValue('Drop:ManufacturerDefault','')
    p_web.SetSessionValue('Drop:ModelNumberDefault','')

    p_web.SetSessionValue('tmp:ESN','')

    p_web.SetSessionValue('Filter:ModelNumber',0)
    p_web.SetSessionValue('Filter:Manufacturer',0)

    p_web.SetSessionValue('tmp:ModelNumber','')
    p_web.SetSessionValue('tmp:Manufacturer','')

    p_web.SetSessionValue('OBFValidation','')

    p_web.SetSessionValue('Hide:IMEINumberButton',1)
    p_web.SetSessionValue('IMEINumberAccepted',0)

    If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SetSessionValue('Filter:TransitType','trt:ARC = 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SetSessionValue('Filter:TransitType','trt:RRC = 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'

    p_web.SetSessionValue('ReadOnly:Manufacturer',1)
    p_web.SetSessionValue('ReadOnly:ModelNumber',1)
    p_web.SetSessionValue('ReadOnly:TransitType','')
    p_web.SetSessionValue('ReadOnly:IMEINumber','')
Value:IMEINumber    Routine
    Data
local:Bouncers  Long()
local:LiveBouncers      Long()
local:PreviousDOP       Date()
local:Found     Byte()
    Code
        If p_web.GSV('save:IMEINumber') = p_web.GSV('tmp:ESN')
            Exit
        End ! Comment:IMEINumber

        p_web.SSV('Comment:IMEINumber','')
        If p_web.GSV('tmp:TransitType') = ''
            Do Reset:IMEINumber
            p_web.SSV('Comment:IMEINumber','Transit Type Required')
            Exit
        End ! If p_web.GSV('tmp:TransitType') = ''

        If p_web.GSV('tmp:ESN') = ''
            Do Reset:IMEINumber
            Exit
        End ! If p_web.GSV('tmp:ESN') = ''

! Insert --- Check the unit isn't available somewhere else (DBH: 24/02/2009) #10545
        Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
        xch:ESN    = p_web.GSV('tmp:ESN')
        if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
        ! Found
            if (xch:Available = 'AVL')
                error# = 1
            end ! if (xch:Available <> 'AVL')
        else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
        ! Error
        end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)

        if (error# = 0)
            Access:LOAN.Clearkey(loa:ESN_Only_Key)
            loa:ESN    = p_web.GSV('tmp:ESN')
            if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
            ! Found
                if (loa:Available = 'AVL')
                    error# = 1
                end ! if (loa:Available = 'AVL')
            else ! if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
            ! Error
            end ! if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
        end !if (error# = 0)

        if (error# = 1)
            p_web.SSV('Comment:IMEINumber','Error! The selected unit is available in another location.')
            Do Reset:IMEINumber
            Exit
        end ! if (error# = 1)
! end --- (DBH: 24/02/2009) #10545


        If local.IMEIModelNumber()
            Do Reset:IMEINumber
            Exit
        End ! If local.IMEIModelNumber()

        If local.ValidateIMEI()
            Do Reset:IMEINumber
            Exit
        End ! If local.ValidateIMEI()

        Access:DEFAULTS.Clearkey(def:RecordNumberKey)
        def:Record_Number = 1
        Set(def:RecordNumberKey,def:RecordNumberKey)
        Loop ! Begin Loop
            If Access:DEFAULTS.Next()
                Break
            End ! If Access:DEFAULTS.Next()
            Break
        End ! Loop

        local:Bouncers = 0
        local:LiveBouncers = 0
        Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
        job_ali:ESN = p_web.GSV('tmp:ESN')
        Set(job_ali:ESN_Key,job_ali:ESN_Key)
        Loop
            If Access:JOBS_ALIAS.Next()
                Break
            End ! If Access:JOBS_ALIAS.Next()
            If job_ali:ESN <> p_web.GSV('tmp:ESN')
                Break
            End ! If job_ali:ESN <> tmp:ESN
            If job_ali:Cancelled = 'YES'
                Cycle
            End ! If job_ali:Cancelled = 'YES'
        ! Insert --- Autofil DOP with date from previous job (DBH: 26/02/2009) #10591
            local:PreviousDOP = job_ali:DOP

        ! end --- (DBH: 26/02/2009) #10591
            If job_ali:Date_Completed = ''
                local:LiveBouncers += 1
            End ! If job_ali:Date_Completed = ''
            local:Bouncers += 1
            p_web.SSV('tmp:PreviousAddress',Clip(job_ali:Company_Name) & '<13,10>' & Clip(job_ali:Address_Line1) & '<13,10>' & Clip(job_ali:Address_Line2) & '<13,10>' & Clip(job_ali:Address_Line3) & '<13,10>')
        End ! Loop

        If def:Allow_Bouncer <> 'YES'
            If local:Bouncers
                Do Reset:IMEINumber
                p_web.SSV('Comment:IMEINumber','IMEI Number has been previously entered within the default period.')
                Exit
            End ! If local:Bouncers
        Else ! If def:Allow_Bouncer <> 'YES'
            If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
                If local:LiveBouncers > 0
                    Do Reset:IMEINumber
                    p_web.SSV('Comment:IMEINumber','IMEI Number already exists on a Live Job.')
                    Exit
                End ! If LiveBouncers(tmp:ESN,Today())
            End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
        End ! If def:Allow_Bouncer <> 'YES'

        If local:Bouncers = 0
            If PreviousIMEI(p_web.GSV('tmp:ESN'))
                p_web.SSV('Comment:Bouncer','The selected IMEI Number has been entered on the system before.')
            End ! If PreviousIMEI(job:ESN)
        End ! If local:Bouncers = 0

        If def:Allow_Bouncer <> 'YES'
            If local:Bouncers
                p_web.SSV('Comment:Bouncer','IMEI Number has been previously entered within the default period.')
                p_web.SSV('tmp:ESN','')
                Do Reset:IMEINumber
                Exit
            End ! If local:Bouncers
        Else ! If def:Allow_Bouncer <> 'YES'
            If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
                If local:LiveBouncers > 0
                    p_web.SSV('Comment:IMEINumber','IMEI Number already exists on a Live Job.')
                    p_web.SSV('tmp:ESN','')
                    Do Reset:IMEINumber
                    Exit
                End ! If LiveBouncers(job:ESN,Today())
            End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
            If local:Bouncers > 0
                p_web.SSV('IMEIHistory','1')
                p_web.SSV('tmp:Bouncer','B')
                p_web.SSV('Comment:Bouncer',local:Bouncers & ' Previous Jobs(s)')
                p_web.SSV('Hide:PreviousAddress',0)
            End ! If local:Bouncers > 0
        End ! If def:Allow_Bouncer <> 'YES'

        !p_web.SSV('Comment:IMEINumber','Valid')
        p_web.SSV('Filter:TransitType','Upper(trt:Transit_Type) = Upper(<39>' & p_web.GSV('tmp:TransitType') & '<39>)')
        p_web.SSV('ReadOnly:TransitType',1)

        Do CheckManModel

        ! #11344 Move to above routine (DBH: 29/06/2010)    
        !Do MQ:Process


        p_web.SSV('save:IMEINumber',p_web.GSV('tmp:ESN'))
Value:Manufacturer      Routine
    If p_web.GetSessionValue('man:Manufacturer') <> ''
        p_web.SetSessionValue('tmp:Manufacturer',p_web.GetSessionValue('man:Manufacturer'))
        If local.IMEIModelNumber() = 0
        End ! If local.IMEIModelNumber()
        If local.ValidateIMEI()
            p_web.SetSessionValue('man:Manufacturer','')
            Do Reset:IMEINumber
            Exit
        End ! If local.ValidateIMEI()
        p_web.SetSessionValue('Filter:ModelNumber',p_web.GetSessionValue('Filter:ModelNumber') & ' And Upper(mod:Manufacturer) = Upper(<39>' & p_web.GetSessionValue('tmp:Manufacturer') & '<39>)')

        Do CheckManMOdel
    End ! If p_web.GetSessionValue('mod:Manufacturer') <> ''
Value:ModelNumber       Routine
    p_web.SetSessionValue('tmp:ModelNumber',p_web.GetSessionValue('mod:Model_Number'))
    If p_web.GetSessionValue('tmp:ModelNumber') = ''
        p_web.SetSessionValue('mod:Model_Number','')
        Exit
    End ! If p_web.GetSessionValue('tmp:ModelNumber') = ''

    If local.ValidateIMEI()
        p_web.SetSessionValue('mod:Model_Number','')
        Do Reset:IMEINumber
        Exit
    End ! If local.ValidateIMEI()

    Do CheckManModel
Value:TransitType       Routine
    If trt:OBF = 1
        p_web.SetSessionValue('Comment:TransitType','OBF Required')
        p_web.SetSessionValue('OBFValidation','Pending')
    Else ! If trt:OBF
        p_web.SetSessionValue('Comment:TransitType','Required')
        p_web.SetSessionValue('OBFValidation','')
    End ! If trt:OBF
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(ESNMODEL)
  p_web._OpenFile(ESNMODAL)
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(ESNMODEL)
  p_Web._CloseFile(ESNMODAL)
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PreNewJobBooking_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  Do ClearVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('MultipleJobBooking') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:TransitType'
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRANTYPE)
      Do Value:TransitType
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ESN')
  Of 'tmp:Manufacturer'
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      DO Value:Manufacturer
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ModelNumber')
  Of 'tmp:ModelNumber'
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
        p_web.setsessionvalue('tmp:Manufacturer',mod:Manufacturer)
      Do Value:ModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('mj:Colour',mj:Colour)
  p_web.SetSessionValue('mj:OrderNumber',mj:OrderNumber)
  p_web.SetSessionValue('mj:FaultCode',mj:FaultCode)
  p_web.SetSessionValue('mj:EngineersNotes',mj:EngineersNotes)
  p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  p_web.SetSessionValue('tmp:ESN',tmp:ESN)
  p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
  p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('mj:Colour')
    mj:Colour = p_web.GetValue('mj:Colour')
    p_web.SetSessionValue('mj:Colour',mj:Colour)
  End
  if p_web.IfExistsValue('mj:OrderNumber')
    mj:OrderNumber = p_web.GetValue('mj:OrderNumber')
    p_web.SetSessionValue('mj:OrderNumber',mj:OrderNumber)
  End
  if p_web.IfExistsValue('mj:FaultCode')
    mj:FaultCode = p_web.GetValue('mj:FaultCode')
    p_web.SetSessionValue('mj:FaultCode',mj:FaultCode)
  End
  if p_web.IfExistsValue('mj:EngineersNotes')
    mj:EngineersNotes = p_web.GetValue('mj:EngineersNotes')
    p_web.SetSessionValue('mj:EngineersNotes',mj:EngineersNotes)
  End
  if p_web.IfExistsValue('tmp:TransitType')
    tmp:TransitType = p_web.GetValue('tmp:TransitType')
    p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  End
  if p_web.IfExistsValue('tmp:ESN')
    tmp:ESN = p_web.GetValue('tmp:ESN')
    p_web.SetSessionValue('tmp:ESN',tmp:ESN)
  End
  if p_web.IfExistsValue('tmp:Manufacturer')
    tmp:Manufacturer = p_web.GetValue('tmp:Manufacturer')
    p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
  End
  if p_web.IfExistsValue('tmp:ModelNumber')
    tmp:ModelNumber = p_web.GetValue('tmp:ModelNumber')
    p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PreNewJobBooking_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
  p_web.SSV('Hide:PleaseWaitMessage',1)
  p_web.site.CancelButton.TextValue = 'Quit'
  If p_web.GSV('ReadyForNewJobBooking') <> 1
      loc:Invalid = 'job:ESN'
      loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
      !Exit
  End ! If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
  
  If p_web.GSV('FirstTime') = 1
      p_web.SSV('FirstTime','')
  
  End ! If p_web.GetSessionValue('FirstTime') = 1
  
  ! Only run certain code when the IMEI Number changes
  p_web.SSV('save:IMEINumber','')
  
  ! Is this multiple job booking?
  If (p_web.IfExistsValue('MultipleJobBooking'))
      p_web.StoreValue('MultipleJobBooking')
  END
  !if (p_web.GSV('mj:InProgress') = 1)
  !    Access:JOBS.Clearkey(job:Ref_Number_Key)
  !    job:Ref_Number = p_web.GSV('mj:PreviousJobNumber')
  !    if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  !!        if (p_web.GSV('mj:ModelNumber') <> 1)
  !!            p_web.SSV('tmp:ModelNumber',job:Model_Number)
  !!            p_web.SSV('tmp:Manufacturer',job:Manufacturer)
  !!        END
  !    END
  !END
  
  
  
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 mj:Colour = p_web.RestoreValue('mj:Colour')
 mj:OrderNumber = p_web.RestoreValue('mj:OrderNumber')
 mj:FaultCode = p_web.RestoreValue('mj:FaultCode')
 mj:EngineersNotes = p_web.RestoreValue('mj:EngineersNotes')
 tmp:TransitType = p_web.RestoreValue('tmp:TransitType')
 tmp:ESN = p_web.RestoreValue('tmp:ESN')
 tmp:Manufacturer = p_web.RestoreValue('tmp:Manufacturer')
 tmp:ModelNumber = p_web.RestoreValue('tmp:ModelNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'NewJobBooking?&Insert_btn=Insert&'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PreNewJobBooking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PreNewJobBooking_ChainTo')
    loc:formaction = p_web.GetSessionValue('PreNewJobBooking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PreNewJobBooking" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PreNewJobBooking" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PreNewJobBooking" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('New Job Booking') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('New Job Booking',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PreNewJobBooking">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PreNewJobBooking" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PreNewJobBooking')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('MultipleJobBooking') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Multiple Job Booking Options') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PreNewJobBooking')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PreNewJobBooking'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='TRANTYPE'
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ESN')
    End
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ModelNumber')
    End
  Else
    If False
    ElsIf p_web.GSV('MultipleJobBooking') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.mj:Colour')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:TransitType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('MultipleJobBooking') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PreNewJobBooking')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('MultipleJobBooking') = 1
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('MultipleJobBooking') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Multiple Job Booking Options') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PreNewJobBooking_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:SelectFields
      do Comment::text:SelectFields
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:Colour
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:OrderNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:OrderNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:OrderNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:FaultCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:FaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:FaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:EngineersNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:EngineersNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:EngineersNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PreNewJobBooking_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TransitType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TransitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:TransitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::pleasewaitmessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::pleasewaitmessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::pleasewaitmessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Manufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Manufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Manufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PreNewJobBooking_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:AcceptIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:AcceptIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:AcceptIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::text:SelectFields  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:SelectFields',p_web.GetValue('NewValue'))
    do Value::text:SelectFields
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:SelectFields  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('text:SelectFields') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Select the fields that will change from one job to the another.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:SelectFields  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('text:SelectFields') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:Colour  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Colour')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:Colour  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:Colour',p_web.GetValue('NewValue'))
    mj:Colour = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:Colour
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:Colour',p_web.GetValue('Value'))
    mj:Colour = p_web.GetValue('Value')
  End
  do Value::mj:Colour
  do SendAlert

Value::mj:Colour  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:Colour
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:Colour'',''prenewjobbooking_mj:colour_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:Colour') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:Colour',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_value')

Comment::mj:Colour  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:OrderNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:OrderNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:OrderNumber',p_web.GetValue('NewValue'))
    mj:OrderNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:OrderNumber
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:OrderNumber',p_web.GetValue('Value'))
    mj:OrderNumber = p_web.GetValue('Value')
  End
  do Value::mj:OrderNumber
  do SendAlert

Value::mj:OrderNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:OrderNumber
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:OrderNumber'',''prenewjobbooking_mj:ordernumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:OrderNumber') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:OrderNumber',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_value')

Comment::mj:OrderNumber  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:FaultCode  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fault Codes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:FaultCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:FaultCode',p_web.GetValue('NewValue'))
    mj:FaultCode = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:FaultCode
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:FaultCode',p_web.GetValue('Value'))
    mj:FaultCode = p_web.GetValue('Value')
  End
  do Value::mj:FaultCode
  do SendAlert

Value::mj:FaultCode  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:FaultCode
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:FaultCode'',''prenewjobbooking_mj:faultcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:FaultCode') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:FaultCode',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_value')

Comment::mj:FaultCode  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:EngineersNotes  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineers Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:EngineersNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:EngineersNotes',p_web.GetValue('NewValue'))
    mj:EngineersNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:EngineersNotes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:EngineersNotes',p_web.GetValue('Value'))
    mj:EngineersNotes = p_web.GetValue('Value')
  End
  do Value::mj:EngineersNotes
  do SendAlert

Value::mj:EngineersNotes  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:EngineersNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:EngineersNotes'',''prenewjobbooking_mj:engineersnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:EngineersNotes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:EngineersNotes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_value')

Comment::mj:EngineersNotes  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:TransitType  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Transit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TransitType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TransitType',p_web.GetValue('NewValue'))
    tmp:TransitType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TransitType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TransitType',p_web.GetValue('Value'))
    tmp:TransitType = p_web.GetValue('Value')
  End
  If tmp:TransitType = ''
    loc:Invalid = 'tmp:TransitType'
    loc:alert = p_web.translate('Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','tmp:TransitType')
  do AfterLookup
  do Value::tmp:TransitType
  do SendAlert
  do Comment::tmp:TransitType
  do Comment::tmp:TransitType

Value::tmp:TransitType  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:TransitType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:TransitType') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GetSessionValue('ReadOnly:TransitType') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:TransitType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:TransitType = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TransitType'',''prenewjobbooking_tmp:transittype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TransitType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:TransitType',p_web.GetSessionValueFormat('tmp:TransitType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectTransitTypes')&'?LookupField=tmp:TransitType&Tab=2&ForeignField=trt:Transit_Type&_sort=&Refresh=sort&LookupFrom=PreNewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_value')

Comment::tmp:TransitType  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:TransitType'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_comment')

Prompt::tmp:ESN  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ESN',p_web.GetValue('NewValue'))
    tmp:ESN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ESN',p_web.GetValue('Value'))
    tmp:ESN = p_web.GetValue('Value')
  End
  If tmp:ESN = ''
    loc:Invalid = 'tmp:ESN'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:ESN = Upper(tmp:ESN)
    p_web.SetSessionValue('tmp:ESN',tmp:ESN)
      p_web.SSV('ReadOnly:Manufacturer',1)
      p_web.SSV('ReadOnly:ModelNumber',1)
  do Value::tmp:ESN
  do SendAlert
  do Value::tmp:Manufacturer  !1
  do Comment::tmp:Manufacturer
  do Value::tmp:ModelNumber  !1
  do Comment::tmp:ModelNumber
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:AcceptIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Comment::tmp:ESN
  do Value::pleasewaitmessage  !1

Value::tmp:ESN  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ESN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:IMEINumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:IMEINumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:ESN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ESN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ESN'',''prenewjobbooking_tmp:esn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ESN')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ESN',p_web.GetSessionValueFormat('tmp:ESN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_value')

Comment::tmp:ESN  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_comment')

Validate::buttonValidateIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateIMEI',p_web.GetValue('NewValue'))
    do Value::buttonValidateIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  Do Value:IMEINumber
  do Value::buttonValidateIMEI
  do SendAlert
  do Value::tmp:Manufacturer  !1
  do Value::tmp:ModelNumber  !1
  do Value::tmp:TransitType  !1
  do Value::tmp:ESN  !1
  do Value::pleasewaitmessage  !1
  do Comment::buttonValidateIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI

Value::buttonValidateIMEI  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_value',Choose(p_web.GetSessionValue('ReadOnly:IMEINumber') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('ReadOnly:IMEINumber') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateIMEI'',''prenewjobbooking_buttonvalidateimei_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateIMEI','Validate I.M.E.I.','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_value')

Comment::buttonValidateIMEI  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:IMEINumber'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_comment',Choose(p_web.GetSessionValue('ReadOnly:IMEINumber') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('ReadOnly:IMEINumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_comment')

Prompt::pleasewaitmessage  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::pleasewaitmessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('pleasewaitmessage',p_web.GetValue('NewValue'))
    do Value::pleasewaitmessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::pleasewaitmessage  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('SmallText')&'">' & p_web.Translate('Note: IMEI Validation may take a few mintues to complete.',0) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_value')

Comment::pleasewaitmessage  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Manufacturer  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_prompt')

Validate::tmp:Manufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Manufacturer',p_web.GetValue('NewValue'))
    tmp:Manufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Manufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Manufacturer',p_web.GetValue('Value'))
    tmp:Manufacturer = p_web.GetValue('Value')
  End
  If tmp:Manufacturer = ''
    loc:Invalid = 'tmp:Manufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:Manufacturer = Upper(tmp:Manufacturer)
    p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
  p_Web.SetValue('lookupfield','tmp:Manufacturer')
  do AfterLookup
  do Value::tmp:Manufacturer
  do SendAlert
  do Comment::tmp:Manufacturer
  do Value::tmp:ModelNumber  !1
  do Comment::tmp:ModelNumber
  do Value::tmp:ESN  !1
  do Comment::tmp:ESN
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:AcceptIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Value::buttonValidateIMEI  !1
  do Comment::buttonValidateIMEI

Value::tmp:Manufacturer  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:Manufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:Manufacturer') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:Manufacturer') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:Manufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:Manufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:Manufacturer'',''prenewjobbooking_tmp:manufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Manufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:Manufacturer',p_web.GetSessionValueFormat('tmp:Manufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=tmp:Manufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=PreNewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_value')

Comment::tmp:Manufacturer  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:Manufacturer'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_comment')

Prompt::tmp:ModelNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ModelNumber',p_web.GetValue('NewValue'))
    tmp:ModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ModelNumber',p_web.GetValue('Value'))
    tmp:ModelNumber = p_web.GetValue('Value')
  End
  If tmp:ModelNumber = ''
    loc:Invalid = 'tmp:ModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:ModelNumber = Upper(tmp:ModelNumber)
    p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
  p_Web.SetValue('lookupfield','tmp:ModelNumber')
  do AfterLookup
  do Value::tmp:Manufacturer
  do Value::tmp:ModelNumber
  do SendAlert
  do Comment::tmp:ModelNumber
  do Value::tmp:ESN  !1
  do Comment::tmp:ESN
  do Value::tmp:Manufacturer  !1
  do Comment::tmp:Manufacturer
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:AcceptIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Comment::tmp:ModelNumber
  do Value::buttonValidateIMEI  !1
  do Comment::buttonValidateIMEI

Value::tmp:ModelNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:ModelNumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:ModelNumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:ModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ModelNumber'',''prenewjobbooking_tmp:modelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:ModelNumber',p_web.GetSessionValueFormat('tmp:ModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=tmp:ModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=&Refresh=sort&LookupFrom=PreNewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_value')

Comment::tmp:ModelNumber  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:ModelNumber'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_comment')

Prompt::Button:AcceptIMEI  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_prompt',Choose(p_web.GetSessionValue('Hide:IMEINumberButton') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('Hide:IMEINumberButton') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_prompt')

Validate::Button:AcceptIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:AcceptIMEI',p_web.GetValue('NewValue'))
    do Value::Button:AcceptIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:AcceptIMEI  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_value',Choose(p_web.GetSessionValue('Hide:IMEINumberButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:IMEINumberButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AcceptIMEI','Accept IMEI / Model','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GetSessionValue('IMEINumberButtonURL'))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/ptick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_value')

Comment::Button:AcceptIMEI  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:IMEINumber'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_comment',Choose(p_web.GetSessionValue('Hide:IMEINumberButton') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:IMEINumberButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PreNewJobBooking_mj:Colour_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:Colour
      else
        do Value::mj:Colour
      end
  of lower('PreNewJobBooking_mj:OrderNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:OrderNumber
      else
        do Value::mj:OrderNumber
      end
  of lower('PreNewJobBooking_mj:FaultCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:FaultCode
      else
        do Value::mj:FaultCode
      end
  of lower('PreNewJobBooking_mj:EngineersNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:EngineersNotes
      else
        do Value::mj:EngineersNotes
      end
  of lower('PreNewJobBooking_tmp:TransitType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TransitType
      else
        do Value::tmp:TransitType
      end
  of lower('PreNewJobBooking_tmp:ESN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ESN
      else
        do Value::tmp:ESN
      end
  of lower('PreNewJobBooking_buttonValidateIMEI_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateIMEI
      else
        do Value::buttonValidateIMEI
      end
  of lower('PreNewJobBooking_tmp:Manufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:Manufacturer
      else
        do Value::tmp:Manufacturer
      end
  of lower('PreNewJobBooking_tmp:ModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ModelNumber
      else
        do Value::tmp:ModelNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PreNewJobBooking',0)

PreCopy  Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PreNewJobBooking',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PreNewJobBooking:Primed',0)

PreDelete       Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PreNewJobBooking:Primed',0)
  p_web.setsessionvalue('showtab_PreNewJobBooking',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('MultipleJobBooking') = 1
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:Colour') = 0
            p_web.SetValue('mj:Colour',0)
            mj:Colour = 0
          End
        End
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:OrderNumber') = 0
            p_web.SetValue('mj:OrderNumber',0)
            mj:OrderNumber = 0
          End
        End
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:FaultCode') = 0
            p_web.SetValue('mj:FaultCode',0)
            mj:FaultCode = 0
          End
        End
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:EngineersNotes') = 0
            p_web.SetValue('mj:EngineersNotes',0)
            mj:EngineersNotes = 0
          End
        End
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PreNewJobBooking_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PreNewJobBooking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 5
  If p_web.GSV('MultipleJobBooking') = 1
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
        If tmp:TransitType = ''
          loc:Invalid = 'tmp:TransitType'
          loc:alert = p_web.translate('Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If tmp:ESN = ''
          loc:Invalid = 'tmp:ESN'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:ESN = Upper(tmp:ESN)
          p_web.SetSessionValue('tmp:ESN',tmp:ESN)
        If loc:Invalid <> '' then exit.
        If tmp:Manufacturer = ''
          loc:Invalid = 'tmp:Manufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:Manufacturer = Upper(tmp:Manufacturer)
          p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
        If loc:Invalid <> '' then exit.
        If tmp:ModelNumber = ''
          loc:Invalid = 'tmp:ModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:ModelNumber = Upper(tmp:ModelNumber)
          p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  Do ClearVariables
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PreNewJobBooking:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('mj:Colour')
  p_web.StoreValue('mj:OrderNumber')
  p_web.StoreValue('mj:FaultCode')
  p_web.StoreValue('mj:EngineersNotes')
  p_web.StoreValue('tmp:TransitType')
  p_web.StoreValue('tmp:ESN')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Manufacturer')
  p_web.StoreValue('tmp:ModelNumber')
  p_web.StoreValue('')
local.CheckLength       Procedure(String f:Type)
local:String            CString(31)
local:LengthFrom        Long()
local:LengthTo          Long()
Code
    Case f:Type
    Of 'IMEI'
        tmp:ModelNumber = p_web.GetSessionValue('tmp:ModelNumber')
        If tmp:ModelNumber = ''
            Return 0
        End ! If p_web.GetSessionValue('tmp:ModelNumber') = '' Or p_web.GetSessionValue('tmp:ModelNumber') = '- Select Model Number -'
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = tmp:ModelNumber
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            local:String = p_web.GetSessionValue('tmp:ESN')
            If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
                If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:IMEINumber','IMEI Number should be ' & Clip(mod:ESN_Length_To) & ' characters long.')
                Else ! If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:IMEINumber','IMEI Number should be ' & Clip(mod:ESN_Length_From) & ' to ' & |
                                                                Clip(mod:ESN_Length_To) & ' characters long.')
                End ! If mod:ESN_Length_To = mod:ESN_Length_From
                Do Reset:IMEINumber
                Return 1
            End ! If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign


    End ! Case f:Type

    Return 0
local.IMEIModelNumber       Procedure()
ModelQueue                      Queue(),Pre(local)
ModManufacturer                     String(30)
ModelNumber                         String(30)
                                End ! ModelQueue        Queue()
ManufacturerQueue               Queue(),Pre(local)
Manufacturer                        String(30)
                                End ! Manufacturer        Queue(),Pre(local)
local:Filter                    String(1000)
    Code
        filter:Manufacturer = ''
        filter:ModelNumber = ''
        p_web.SSV('filter:ModelNumber','')
        p_web.SSV('filter:Manufacturer','')
    !Match the first 6 digits of the IMEI Number
        tmp:ESN = p_web.GSV('tmp:ESN')
        tmp:Manufacturer = p_web.GSV('tmp:Manufacturer')
        Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
        esn:ESN = Sub(tmp:ESN,1,6)
        Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
        Loop ! Begin Loop
            If Access:ESNMODEL.Next()
                Break
            End ! If Access:ESNMODEL.Next()
            If esn:ESN <> Sub(tmp:ESN,1,6)
                Break
            End ! If esn_ESN <> Sub(p_web.GSV('tmp:ESN'),1,6)
            
            ! #11466 Only include active models (DBH: 30/06/2010)
            IF (esn:Active <> 'Y')
                CYCLE
            END
            
            ! #11466 Only include active models (DBH: 30/06/2010)
            Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
            mod:Manufacturer = esn:Manufacturer
            mod:Model_Number = esn:Model_Number
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
            END
            IF (mod:Active <> 'Y')
                CYCLE
            END            

            local:Manufacturer = esn:Manufacturer
            Get(ManufacturerQueue,local:Manufacturer)
            If Error()
                local:Manufacturer = esn:Manufacturer
                Add(ManufacturerQueue)
            End ! If Error()

            If tmp:Manufacturer <> ''
                If tmp:Manufacturer <> esn:Manufacturer
                    Cycle
                End ! If tmp:Manufacturer <> esn:Manufacturer
            End ! If tmp:Manufacturer <> ''
            
            local:ModelNumber = esn:Model_Number
            local:ModManufacturer = esn:Manufacturer
            Add(ModelQueue)

        End ! Loop

    !Match the first 8 digits of the IMEI Number
        Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
        esn:ESN = Sub(tmp:ESN,1,8)
        Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
        Loop ! Begin Loop
            If Access:ESNMODEL.Next()
                Break
            End ! If Access:ESNMODEL.Next()
            If esn:ESN <> Sub(tmp:ESN,1,8)
                Break
            End ! If esn_ESN <> Sub(p_web.GSV('tmp:ESN'),1,6)
            
            ! #11466 Remove models if allowed in 6 digit (DBH: 30/06/2010)
            IF (esn:Active <> 'Y')
                CYCLE
            END
            
            ! #11466 Only include active models (DBH: 30/06/2010)
            Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
            mod:Manufacturer = esn:Manufacturer
            mod:Model_Number = esn:Model_Number
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
            END
            IF (mod:Active <> 'Y')
                CYCLE
            END

            local:Manufacturer = esn:Manufacturer
            Get(ManufacturerQueue,local:Manufacturer)
            If Error()
                local:Manufacturer = esn:Manufacturer
                Add(ManufacturerQueue)
            End ! If Error()

            If tmp:Manufacturer <> ''
                If tmp:Manufacturer <> esn:Manufacturer
                    Cycle
                End ! If tmp:Manufacturer <> esn:Manufacturer
            End ! If tmp:Manufacturer <> ''
            
            local:ModelNumber = esn:Model_Number
            Get(ModelQueue,local:ModelNumber)
            If Error()
                local:ModelNumber = esn:Model_Number
                local:ModManufacturer = esn:Manufacturer
                Add(ModelQueue)
            End ! If Error()

        End ! Loop

        p_web.SSV('save:FilterModelNumber','')
        Case Records(ModelQueue)
        Of 0
            p_web.SSV('Comment:IMEINumber','There are no model numbers matching the selected IMEI Number.')
            p_web.SSV('tmp:ESN','')
            Do Reset:IMEINumber
            Return True
        Of 1
            Get(ModelQueue,1)
            p_web.SSV('tmp:ModelNumber',local:ModelNumber)
            p_web.SSV('tmp:Manufacturer',local:ModManufacturer)
            p_web.SSV('filter:ModelNumber','mod:Model_Number = ''' & Clip(local:ModelNumber) & '''')
            Case Records(ManufacturerQueue)
            Of 1
                p_web.SSV('filter:Manufacturer','man:Manufacturer = ''' & Clip(local:ModManufacturer) & '''')
            Else
                Loop x# = 1 To Records(ManufacturerQueue)
                    Get(ManufacturerQueue,x#)
                    filter:Manufacturer = Clip(filter:Manufacturer) & ' OR man:Manufacturer = ''' & Clip(local:Manufacturer) & ''''
                End ! Loop x# = 1 To Records(ManufacturerQueue)
                filter:Manufacturer = Clip(Sub(filter:Manufacturer,5,1000))
                p_web.SSV('filter:Manufacturer',filter:Manufacturer)
            End ! Case Record(ManufacturerQueue)
!        p_web.SSV('Comment:IMEINumber','Valid')
            p_web.SSV('ReadOnly:Manufacturer',1)
            p_web.SSV('ReadOnly:ModelNumber',1)
        Else
            p_web.SSV('ReadOnly:Manufacturer','')
            p_web.SSV('ReadOnly:ModelNumber','')

            Loop x# = 1 To Records(ModelQueue)
                Get(ModelQueue,x#)
                filter:ModelNumber = Clip(filter:ModelNumber) & ' OR Upper(mod:Model_Number) = Upper(<39>' & Clip(local:ModelNumber) & '<39>)'
            End ! Loop x# = 1 To Records(ModelQueue)

            If Records(ManufacturerQueue) = 1
                Get(ManufacturerQueue,1)
                filter:Manufacturer = 'Upper(man:Manufacturer) = Upper(<39>' & Clip(local:Manufacturer) & '<39>)'
                p_web.SSV('tmp:Manufacturer',local:Manufacturer)
                p_web.SSV('ReadOnly:Manufacturer',1)
            Else ! If Records(ManufacturerQueue) = 1
                Loop x# = 1 To Records(ManufacturerQueue)
                    Get(ManufacturerQueue,x#)
                    filter:Manufacturer = Clip(filter:Manufacturer) & ' OR Upper(man:Manufacturer) = Upper(<39>' & Clip(local:Manufacturer) & '<39>)'
                End ! Loop x# = 1 To Records(ManufacturerQueue)
                filter:Manufacturer = Clip(Sub(filter:Manufacturer,5,1000))
            End ! If Records(ManufacturerQueue) = 1

            filter:ModelNumber = Clip(Sub(filter:ModelNumber,5,1000))

            p_web.SSV('filter:Manufacturer',filter:Manufacturer)
            p_web.SSV('filter:ModelNumber',filter:ModelNumber)
            p_web.SSV('save:FilterModelNumber',filter:ModelNumber)
            
            ! #11344 Show that the man/mod must be entered before a geniune validation will be done. (DBH: 29/06/2010)
            p_web.SSV('Comment:IMEINumber','IMEI Number will be validated once the Manufacturer and Model Number have been selected.')

        End ! Case Records(ModelQueue)
       

        Return False
local.IsIMEIValid        Procedure()
Code
    tmp:ESN = p_web.GetSessionValue('tmp:ESN')
    tmp:ModelNumber = p_web.GetSessionValue('tmp:ModelNumber')

    If tmp:ESN = '' Or tmp:ModelNumber = ''
        Return True
    End ! If tmp:ESN = '' Or tmp:ModelNumber = ''

    Found# = False
    Loop x# = 1 To Len(Clip(tmp:ESN))
        If IsAlpha(Sub(tmp:ESN,x#,1))
            Found# = True
        End ! If IsAlpha(Sub(tmp:ESN,x#,1)
    End ! Loop x# = 1 To Len(Clip(tmp:ESN))

    If Found# = True
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = tmp:ModelNumber
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            If mod:AllowIMEICharacters
                Return True
            Else ! If mod:AllowIMEICharacter
                p_web.SetSessionValue('Comment:IMEINumber','IMEI Number Format is invalid.')
                p_web.SetSessionValue('tmp:ESN','')
                p_web.SetSessionValue('Save:IMEINumber','')
                Do Reset:IMEINumber
                Return False
            End ! If mod:AllowIMEICharacter
        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
    End ! If Found# = True
    Return True
local.ValidateIMEI        Procedure()
Code
    If p_web.GetSessionValue('tmp:ModelNumber') = ''
        Return False
    End ! If p_web.GetSessionValue('tmp:ModelNumber') = ''
    If p_web.GetSessionValue('tmp:ESN') = ''
        Return False
    End ! If p_web.GetSessionValue('tmp:ESN') = ''
    If CheckLength('IMEI',p_web.GetSessionValue('tmp:ModelNumber'),p_web.GetSessionValue('tmp:ESN'))
        Do Reset:IMEINumber
        p_web.SetSessionValue('Comment:IMEINumber','Invalid Length.')
        Return True
    End ! If local.CheckLength('IMEI')

    If local.IsIMEIValid() = 0
        Do Reset:IMEINumber
        Return True
    End ! If local.IsIMEIValid() = 0

    Return False
local.ValidateMSN     Procedure()
Code
    If p_web.GetSessionValue('Hide:MSN') = 1
        Return 0
    End ! If p_web.GetSessionValue('job:MSN') = '' Or p_web.GetSessionValue('Hide:MSN') = 1


    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:ApplyMSNFormat
            If CheckFaultFormat(job:MSN,man:MSNFormat)
                p_web.SetSessionValue('job:MSN','')
                p_web.SetSessionValue('Comment:MSN','Invalid Format.')
                Return 1
            End ! If CheckFaultFormat(job:MSN,man:MSNFormat)
        Else ! If man:ApplyMSNFormat
            If CheckLength('MSN',job:Model_Number,job:MSN)
                p_web.SetSessionValue('job:MSN','')
                p_web.SetSessionValue('Comment:MSN','Invalid Length.')
                Return 1
            End ! If CheckLength('MSN',p_web.GetSessionValue('tmp:Manufacturer'),p_web.GetSessionValue('job:MSN'))

        End ! If man:ApplyMSNFormat
    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Return 0
MQProcess            PROCEDURE  (String f:IMEINumber,*String f:POP,*Date f:DOP,*Byte f:Error,*String f:ErrorMessage,String f:OneYearWarranty) ! Declare Procedure
    include('MQProcess.inc','Local Data')
  CODE
    include('MQProcess.inc','Processed Code')
AmendAddress         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
VariablesGroup       GROUP,PRE(tmp)                        !
CompanyName          STRING(30)                            !TempCompanyName
AddressLine1         STRING(30)                            !TempAddressLine1
AddressLine2         STRING(30)                            !TempAddressLine2
Suburb               STRING(30)                            !TempSuburb
Postcode             STRING(30)                            !TempPostcode
TelephoneNumber      STRING(30)                            !TempTelephoneNumber
FaxNumber            STRING(30)                            !TempFaxNumber
EmailAddress         STRING(255)                           !Email Address
EndUserTelephoneNumber STRING(30)                          !End User Tel No
IDNumber             STRING(30)                            !ID Number
SMSNotification      BYTE(0)                               !SMS Notification
NotificationMobileNumber STRING(30)                        !NotificationMobileNumber
EmailNotification    BYTE(0)                               !EmailNotification
NotificationEmailAddress STRING(255)                       !Notification Email Address
CompanyNameDelivery  STRING(30)                            !tmp:CompanyNameDelivery
AddressLine1Delivery STRING(30)                            !tmp:AddressLine1Delivery
AddressLine2Delivery STRING(30)                            !AddressLine2Delivery
SuburbDelivery       STRING(30)                            !SubrubDelivery
PostcodeDelivery     STRING(30)                            !PostcodeDelivery
TelephoneNumberDelivery STRING(30)                         !TelephoneNumberDelivery
CompanyNameCollection STRING(30)                           !CompanyNameCollection
AddressLine1Collection STRING(30)                          !AddressLine1Collection
AddressLine2Collection STRING(30)                          !AddressLine2Collection
SuburbCollection     STRING(30)                            !SuburbCollection
PostcodeCollection   STRING(30)                            !PostcodeCollection
TelephoneNumberCollection STRING(30)                       !TelephoneNumberCollection
HubCustomer          STRING(30)                            !Hub
HubCollection        STRING(30)                            !Hub
HubDelivery          STRING(30)                            !Hub
                     END                                   !
ReplicateCustomerToDelivery BYTE(0)                        !ReplicateCustomerToDelivery
ReplicateCustomerToCollection BYTE(0)                      !ReplicateCustomerToCollection
ReplicateCollectionToDelivery BYTE(0)                      !ReplicateCollectionToDelivery
ReplicateDeliveryToCollection BYTE(0)                      !ReplicateDeliveryToCollection
FilesOpened     Long
SUBTRACC::State  USHORT
COURIER::State  USHORT
JOBNOTES::State  USHORT
SUBACCAD::State  USHORT
SUBURB::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBSE2::State  USHORT
TRANTYPE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AmendAddress')
  loc:formname = 'AmendAddress_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AmendAddress','')
    p_web._DivHeader('AmendAddress',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AmendAddress',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(SUBACCAD)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRANTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(SUBACCAD)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRANTYPE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('AmendAddress_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCustomer',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubCustomer')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3_Delivery'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Delivery',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubDelivery',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
  Of 'job:Address_Line3_Collection'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Collection',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCollection',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubDelivery')
  End
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Sub_Sub_Account'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBACCAD)
        p_web.setsessionvalue('job:Postcode_Delivery',sua:Postcode)
        p_web.setsessionvalue('job:Address_Line1_Delivery',sua:addressline1)
        p_web.setsessionvalue('job:Address_Line2_Delivery',sua:addressline2)
        p_web.setsessionvalue('job:Address_Line3_Delivery',sua:addressline3)
        p_web.setsessionvalue('job:Telephone_Delivery',sua:TelephoneNumber)
        p_web.setsessionvalue('job:Company_Name_Delivery',sua:CompanyName)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  p_web.SetSessionValue('job:Postcode',job:Postcode)
  p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Company_Name')
    job:Company_Name = p_web.GetValue('job:Company_Name')
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  End
  if p_web.IfExistsValue('job:Address_Line1')
    job:Address_Line1 = p_web.GetValue('job:Address_Line1')
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  End
  if p_web.IfExistsValue('job:Address_Line2')
    job:Address_Line2 = p_web.GetValue('job:Address_Line2')
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  End
  if p_web.IfExistsValue('job:Address_Line3')
    job:Address_Line3 = p_web.GetValue('job:Address_Line3')
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  End
  if p_web.IfExistsValue('jobe2:HubCustomer')
    jobe2:HubCustomer = p_web.GetValue('jobe2:HubCustomer')
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  End
  if p_web.IfExistsValue('job:Postcode')
    job:Postcode = p_web.GetValue('job:Postcode')
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  End
  if p_web.IfExistsValue('job:Telephone_Number')
    job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  End
  if p_web.IfExistsValue('job:Fax_Number')
    job:Fax_Number = p_web.GetValue('job:Fax_Number')
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress')
    jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  End
  if p_web.IfExistsValue('jobe:VatNumber')
    jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  End
  if p_web.IfExistsValue('jobe2:IDNumber')
    jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  End
  if p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber')
    jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  End
  if p_web.IfExistsValue('jobe2:EmailNotification')
    jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress')
    jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery')
    job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  End
  if p_web.IfExistsValue('job:Company_Name_Collection')
    job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery')
    job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection')
    job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery')
    job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection')
    job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery')
    job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection')
    job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  End
  if p_web.IfExistsValue('jobe2:HubDelivery')
    jobe2:HubDelivery = p_web.GetValue('jobe2:HubDelivery')
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  End
  if p_web.IfExistsValue('jobe2:HubCollection')
    jobe2:HubCollection = p_web.GetValue('jobe2:HubCollection')
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  End
  if p_web.IfExistsValue('job:Postcode_Delivery')
    job:Postcode_Delivery = p_web.GetValue('job:Postcode_Delivery')
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  End
  if p_web.IfExistsValue('job:Postcode_Collection')
    job:Postcode_Collection = p_web.GetValue('job:Postcode_Collection')
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  End
  if p_web.IfExistsValue('job:Telephone_Delivery')
    job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  End
  if p_web.IfExistsValue('job:Telephone_Collection')
    job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  End
  if p_web.IfExistsValue('jbn:Delivery_Text')
    jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  End
  if p_web.IfExistsValue('jbn:Collection_Text')
    jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  End
  if p_web.IfExistsValue('jbn:DelContactName')
    jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  End
  if p_web.IfExistsValue('jbn:ColContatName')
    jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  End
  if p_web.IfExistsValue('jbn:DelDepartment')
    jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  End
  if p_web.IfExistsValue('jbn:ColDepartment')
    jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account')
    jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('AmendAddress_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
      p_web.SSV('Hide:DeliveryAddress',1)
      p_web.SSV('Hide:CollectionAddress',1)
      Access:TRANTYPE.Clearkey(trt:transit_Type_Key)
      trt:transit_Type    = p_web.GSV('job:transit_Type')
      if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Found
          if (trt:Delivery_Address = 'YES')
              p_web.SSV('Hide:DeliveryAddress',0)
          end ! if (trt:DeliveryAddress = 'YES')
  
          if (trt:Collection_Address = 'YES')
              p_web.SSV('Hide:CollectionAddress',0)
          end ! if (trt:Collection_Address = 'YES')
  
      else ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Error
      end ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
  
      p_web.storeValue('FromURL')
  
  
      p_web.SSV('Comment:TelephoneNumber','')
      p_web.SSV('Comment:FaxNumber','')
      p_web.SSV('Comment:SMSAlertNumber','')
      p_web.SSV('Comment:TelephoneNumberDelivery','')
      p_web.SSV('Comment:TelephoneNumberCollection','')
  
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('job:Account_Number')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          IF (sub:UseAlternativeAdd)
              p_web.SSV('Hide:SubSubAccount',0)
          ELSE
              p_web.SSV('Hide:SubSubAccount',1)
          END
          ! Start - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
          If sub:Vat_Number = '' AND sub:OverrideHeadVATNo = True
              p_web.SSV('Hide:VatNumber',0)
          Else ! sub:Vat_Number = '' AND tra:Vat_Number = ''
              p_web.SSV('Hide:VatNumber',1)
          End ! sub:Vat_Number = '' AND tra:Vat_Number = ''
          ! End   - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
  
      END
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('FromURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AmendAddress_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AmendAddress_ChainTo')
    loc:formaction = p_web.GetSessionValue('AmendAddress_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="AmendAddress" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="AmendAddress" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="AmendAddress" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Amend Addresses') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Amend Addresses',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_AmendAddress">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_AmendAddress" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_AmendAddress')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('End User Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Collection / Delivery Details') & ''''
        If p_web.GSV('Hide:SubSubAccount') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Other Details') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_AmendAddress')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_AmendAddress'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Number')
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:CollectionAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Delivery')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Company_Name')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('Hide:SubSubAccount') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_AmendAddress')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('Hide:SubSubAccount') <> 1
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('End User Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::temp:Title
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::temp:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::temp:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubCustomer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubCustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubCustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Fax_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserEmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserTelNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:VatNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:VatNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:VatNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:VatNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:IDNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:IDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:IDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:SMSNotification
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:SMSNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:SMSNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:SMSAlertNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:SMSAlertNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:SMSAlertNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:EmailNotification
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:EmailNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:EmailNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:EmailAlertAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:EmailAlertAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:EmailAlertAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:CourierWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Collection / Delivery Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::DeliveryAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::DeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::DeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::CollectionAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::CollectionAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::CollectionAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubDelivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubDelivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubDelivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubCollection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubCollection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubCollection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:CopyEndUserAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:CopyEndUserAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:CopyEndUserAddress1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:CopyEndUserAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:CopyEndUserAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Delivery_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Collection_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Collection_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Collection_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:DelContactName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:DelContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:DelContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:ColContatName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:ColContatName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:ColContatName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:DelDepartment
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:DelDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:DelDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:ColDepartment
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:ColDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:ColDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('Hide:SubSubAccount') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Other Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Sub_Sub_Account
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Sub_Sub_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:Sub_Sub_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::temp:Title  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::temp:Title  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('temp:Title',p_web.GetValue('NewValue'))
    do Value::temp:Title
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::temp:Title  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('End User Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::temp:Title  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Company_Name  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Company_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name',p_web.GetValue('NewValue'))
    job:Company_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name
    do Value::job:Company_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name = p_web.GetValue('Value')
  End
    job:Company_Name = Upper(job:Company_Name)
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  do Value::job:Company_Name
  do SendAlert

Value::job:Company_Name  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Company_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name'',''amendaddress_job:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name',p_web.GetSessionValueFormat('job:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value')

Comment::job:Company_Name  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1',p_web.GetValue('NewValue'))
    job:Address_Line1 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1
    do Value::job:Address_Line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1 = p_web.GetValue('Value')
  End
    job:Address_Line1 = Upper(job:Address_Line1)
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  do Value::job:Address_Line1
  do SendAlert

Value::job:Address_Line1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Address_Line1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1'',''amendaddress_job:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1',p_web.GetSessionValueFormat('job:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value')

Comment::job:Address_Line1  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line2  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2',p_web.GetValue('NewValue'))
    job:Address_Line2 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2
    do Value::job:Address_Line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2 = p_web.GetValue('Value')
  End
    job:Address_Line2 = Upper(job:Address_Line2)
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  do Value::job:Address_Line2
  do SendAlert

Value::job:Address_Line2  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Address_Line2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2'',''amendaddress_job:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2',p_web.GetSessionValueFormat('job:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value')

Comment::job:Address_Line2  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line3  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3',p_web.GetValue('NewValue'))
    job:Address_Line3 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3
    do Value::job:Address_Line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3 = p_web.GetValue('Value')
  End
    job:Address_Line3 = Upper(job:Address_Line3)
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode',sur:Postcode)
          p_web.SSV('jobe2:HubCustomer',sur:Hub)
      END
  
  p_Web.SetValue('lookupfield','job:Address_Line3')
  do AfterLookup
  do Value::job:Postcode
  do Value::jobe2:HubCustomer
  do Value::job:Address_Line3
  do SendAlert
  do Comment::job:Address_Line3
  do Value::jobe2:HubCustomer  !1
  do Value::job:Postcode  !1

Value::job:Address_Line3  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Address_Line3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3'',''amendaddress_job:address_line3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3',p_web.GetSessionValue('job:Address_Line3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value')

Comment::job:Address_Line3  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_comment')

Prompt::jobe2:HubCustomer  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt')

Validate::jobe2:HubCustomer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetValue('NewValue'))
    jobe2:HubCustomer = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubCustomer
    do Value::jobe2:HubCustomer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubCustomer',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubCustomer = p_web.GetValue('Value')
  End
    jobe2:HubCustomer = Upper(jobe2:HubCustomer)
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  do Value::jobe2:HubCustomer
  do SendAlert

Value::jobe2:HubCustomer  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:HubCustomer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubCustomer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCustomer'',''amendaddress_jobe2:hubcustomer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCustomer',p_web.GetSessionValueFormat('jobe2:HubCustomer'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value')

Comment::jobe2:HubCustomer  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_comment')

Prompt::job:Postcode  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt')

Validate::job:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode',p_web.GetValue('NewValue'))
    job:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode
    do Value::job:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode = p_web.GetValue('Value')
  End
  do Value::job:Postcode
  do SendAlert

Value::job:Postcode  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode'',''amendaddress_job:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode',p_web.GetSessionValueFormat('job:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value')

Comment::job:Postcode  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_comment')

Prompt::job:Telephone_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Telephone_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Number',p_web.GetValue('NewValue'))
    job:Telephone_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Number
    do Value::job:Telephone_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Number = p_web.GetValue('Value')
  End
    job:Telephone_Number = Upper(job:Telephone_Number)
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Number')) = 0
          p_web.SSV('job:Telephone_Number','')
          p_web.SSV('Comment:TelephoneNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Number
  do SendAlert
  do Comment::job:Telephone_Number

Value::job:Telephone_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Telephone_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Number'',''amendaddress_job:telephone_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Number',p_web.GetSessionValueFormat('job:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value')

Comment::job:Telephone_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_comment')

Prompt::job:Fax_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Fax_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Fax_Number',p_web.GetValue('NewValue'))
    job:Fax_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Fax_Number
    do Value::job:Fax_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Fax_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Fax_Number = p_web.GetValue('Value')
  End
    job:Fax_Number = Upper(job:Fax_Number)
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
      If PassMobileNumberFormat(p_web.GSV('job:Fax_Number')) = 0
          p_web.SSV('job:Fax_Number','')
          p_web.SSV('Comment:FaxNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:FaxNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Fax_Number
  do SendAlert
  do Comment::job:Fax_Number

Value::job:Fax_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Fax_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Fax_Number'',''amendaddress_job:fax_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Fax_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Fax_Number',p_web.GetSessionValueFormat('job:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value')

Comment::job:Fax_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:FaxNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_comment')

Prompt::jobe:EndUserEmailAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserEmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetValue('NewValue'))
    jobe:EndUserEmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserEmailAddress
    do Value::jobe:EndUserEmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe:EndUserEmailAddress = p_web.GetValue('Value')
  End
  do Value::jobe:EndUserEmailAddress
  do SendAlert

Value::jobe:EndUserEmailAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe:EndUserEmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserEmailAddress'',''amendaddress_jobe:enduseremailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserEmailAddress',p_web.GetSessionValueFormat('jobe:EndUserEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value')

Comment::jobe:EndUserEmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:EndUserTelNo  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End User Tel No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserTelNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetValue('NewValue'))
    jobe:EndUserTelNo = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserTelNo
    do Value::jobe:EndUserTelNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:EndUserTelNo = p_web.GetValue('Value')
  End
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  do Value::jobe:EndUserTelNo
  do SendAlert

Value::jobe:EndUserTelNo  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:EndUserTelNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''amendaddress_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value')

Comment::jobe:EndUserTelNo  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:VatNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Vat Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:VatNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:VatNumber',p_web.GetValue('NewValue'))
    jobe:VatNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:VatNumber
    do Value::jobe:VatNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:VatNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:VatNumber = p_web.GetValue('Value')
  End
    jobe:VatNumber = Upper(jobe:VatNumber)
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  do Value::jobe:VatNumber
  do SendAlert

Value::jobe:VatNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:VatNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:VatNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:VatNumber'',''amendaddress_jobe:vatnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:VatNumber',p_web.GetSessionValueFormat('jobe:VatNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Vat Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value')

Comment::jobe:VatNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:IDNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:IDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:IDNumber',p_web.GetValue('NewValue'))
    jobe2:IDNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:IDNumber
    do Value::jobe2:IDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:IDNumber',p_web.dFormat(p_web.GetValue('Value'),'@s13'))
    jobe2:IDNumber = p_web.GetValue('Value')
  End
    jobe2:IDNumber = Upper(jobe2:IDNumber)
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  do Value::jobe2:IDNumber
  do SendAlert

Value::jobe2:IDNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:IDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:IDNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:IDNumber'',''amendaddress_jobe2:idnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:IDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:IDNumber',p_web.GetSessionValueFormat('jobe2:IDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'I.D. Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value')

Comment::jobe2:IDNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:SMSNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('SMS Notification')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:SMSNotification  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetValue('NewValue'))
    jobe2:SMSNotification = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:SMSNotification
    do Value::jobe2:SMSNotification
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetValue('Value'))
    jobe2:SMSNotification = p_web.GetValue('Value')
  End
  do Value::jobe2:SMSNotification
  do SendAlert
  do Prompt::jobe2:SMSAlertNumber
  do Value::jobe2:SMSAlertNumber  !1

Value::jobe2:SMSNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:SMSNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:SMSNotification'',''amendaddress_jobe2:smsnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:SMSNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:SMSNotification',clip(1),,loc:readonly,,,loc:javascript,,'SMS Notification') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value')

Comment::jobe2:SMSNotification  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:SMSAlertNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Mobile No')
  If p_web.GSV('jobe2:SMSNotification') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt')

Validate::jobe2:SMSAlertNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetValue('NewValue'))
    jobe2:SMSAlertNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:SMSAlertNumber
    do Value::jobe2:SMSAlertNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:SMSAlertNumber = p_web.GetValue('Value')
  End
    jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
      If PassMobileNumberFormat(p_web.GSV('jobe2:SMSAlertNumber')) = 0
          p_web.SSV('jobe2:SMSAlertNumber','')
          p_web.SSV('Comment:SMSAlertNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:SMSAlertNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::jobe2:SMSAlertNumber
  do SendAlert
  do Comment::jobe2:SMSAlertNumber

Value::jobe2:SMSAlertNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe2:SMSNotification') = 0)
  ! --- STRING --- jobe2:SMSAlertNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:SMSAlertNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:SMSAlertNumber'',''amendaddress_jobe2:smsalertnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSAlertNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:SMSAlertNumber',p_web.GetSessionValueFormat('jobe2:SMSAlertNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'SMS Alert Mobile Number') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value')

Comment::jobe2:SMSAlertNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:SMSAlertNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_comment',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('jobe2:SMSNotification') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_comment')

Prompt::jobe2:EmailNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Notification')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:EmailNotification  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetValue('NewValue'))
    jobe2:EmailNotification = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:EmailNotification
    do Value::jobe2:EmailNotification
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetValue('Value'))
    jobe2:EmailNotification = p_web.GetValue('Value')
  End
  do Value::jobe2:EmailNotification
  do SendAlert
  do Prompt::jobe2:EmailAlertAddress
  do Value::jobe2:EmailAlertAddress  !1

Value::jobe2:EmailNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:EmailNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:EmailNotification'',''amendaddress_jobe2:emailnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:EmailNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:EmailNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:EmailNotification',clip(1),,loc:readonly,,,loc:javascript,,'Email Notification') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value')

Comment::jobe2:EmailNotification  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:EmailAlertAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Email Address')
  If p_web.GSV('jobe2:EmailNotification') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt')

Validate::jobe2:EmailAlertAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetValue('NewValue'))
    jobe2:EmailAlertAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:EmailAlertAddress
    do Value::jobe2:EmailAlertAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe2:EmailAlertAddress = p_web.GetValue('Value')
  End
  do Value::jobe2:EmailAlertAddress
  do SendAlert

Value::jobe2:EmailAlertAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe2:EmailNotification') = 0)
  ! --- STRING --- jobe2:EmailAlertAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe2:EmailAlertAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:EmailAlertAddress'',''amendaddress_jobe2:emailalertaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:EmailAlertAddress',p_web.GetSessionValueFormat('jobe2:EmailAlertAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Alert Address') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value')

Comment::jobe2:EmailAlertAddress  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_comment',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('jobe2:EmailNotification') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Waybill Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:CourierWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.GetValue('NewValue'))
    jobe2:CourierWaybillNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:CourierWaybillNumber
    do Value::jobe2:CourierWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:CourierWaybillNumber = p_web.GetValue('Value')
  End
    jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  do Value::jobe2:CourierWaybillNumber
  do SendAlert

Value::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:CourierWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:CourierWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:CourierWaybillNumber'',''amendaddress_jobe2:courierwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:CourierWaybillNumber',p_web.GetSessionValueFormat('jobe2:CourierWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Courier Waybill Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value')

Comment::jobe2:CourierWaybillNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::DeliveryAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::DeliveryAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('DeliveryAddress',p_web.GetValue('NewValue'))
    do Value::DeliveryAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::DeliveryAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Delivery Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::DeliveryAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::CollectionAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::CollectionAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('CollectionAddress',p_web.GetValue('NewValue'))
    do Value::CollectionAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::CollectionAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Collection Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::CollectionAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Company_Name_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Company Name')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt')

Validate::job:Company_Name_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetValue('NewValue'))
    job:Company_Name_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name_Delivery
    do Value::job:Company_Name_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name_Delivery = p_web.GetValue('Value')
  End
    job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  do Value::job:Company_Name_Delivery
  do SendAlert

Value::job:Company_Name_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Company_Name_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Delivery'',''amendaddress_job:company_name_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Delivery',p_web.GetSessionValueFormat('job:Company_Name_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value')

Comment::job:Company_Name_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_comment')

Prompt::job:Company_Name_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Company Name')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Company_Name_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetValue('NewValue'))
    job:Company_Name_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name_Collection
    do Value::job:Company_Name_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name_Collection = p_web.GetValue('Value')
  End
  do Value::job:Company_Name_Collection
  do SendAlert

Value::job:Company_Name_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Company_Name_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Collection'',''amendaddress_job:company_name_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Collection',p_web.GetSessionValueFormat('job:Company_Name_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value')

Comment::job:Company_Name_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line1_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Address')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt')

Validate::job:Address_Line1_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line1_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1_Delivery
    do Value::job:Address_Line1_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  do Value::job:Address_Line1_Delivery
  do SendAlert

Value::job:Address_Line1_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line1_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Delivery'',''amendaddress_job:address_line1_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Delivery',p_web.GetSessionValueFormat('job:Address_Line1_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value')

Comment::job:Address_Line1_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_comment')

Prompt::job:Address_Line1_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Address')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line1_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetValue('NewValue'))
    job:Address_Line1_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1_Collection
    do Value::job:Address_Line1_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1_Collection = p_web.GetValue('Value')
  End
    job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  do Value::job:Address_Line1_Collection
  do SendAlert

Value::job:Address_Line1_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line1_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Collection'',''amendaddress_job:address_line1_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Collection',p_web.GetSessionValueFormat('job:Address_Line1_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value')

Comment::job:Address_Line1_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line2_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt')

Validate::job:Address_Line2_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line2_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2_Delivery
    do Value::job:Address_Line2_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  do Value::job:Address_Line2_Delivery
  do SendAlert

Value::job:Address_Line2_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line2_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Delivery'',''amendaddress_job:address_line2_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Delivery',p_web.GetSessionValueFormat('job:Address_Line2_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value')

Comment::job:Address_Line2_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_comment')

Prompt::job:Address_Line2_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line2_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2_Collection',p_web.GetValue('NewValue'))
    job:Address_Line2_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2_Collection
    do Value::job:Address_Line2_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2_Collection = p_web.GetValue('Value')
  End
    job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  do Value::job:Address_Line2_Collection
  do SendAlert

Value::job:Address_Line2_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line2_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Collection'',''amendaddress_job:address_line2_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Collection',p_web.GetSessionValueFormat('job:Address_Line2_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value')

Comment::job:Address_Line2_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line3_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suburb')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt')

Validate::job:Address_Line3_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line3_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3_Delivery
    do Value::job:Address_Line3_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Delivery')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Delivery',sur:postcode)
          p_web.SSV('jobe2:HubDelivery',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Delivery')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::jobe2:HubDelivery
  do Value::job:Address_Line3_Delivery
  do SendAlert
  do Comment::job:Address_Line3_Delivery
  do Value::jobe2:HubDelivery  !1

Value::job:Address_Line3_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line3_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Delivery'',''amendaddress_job:address_line3_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Delivery')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Delivery',p_web.GetSessionValue('job:Address_Line3_Delivery'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3_Delivery&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value')

Comment::job:Address_Line3_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_comment')

Prompt::job:Address_Line3_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suburb')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line3_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetValue('NewValue'))
    job:Address_Line3_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3_Collection
    do Value::job:Address_Line3_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3_Collection = p_web.GetValue('Value')
  End
    job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Collection')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Collection',sur:postcode)
          p_web.SSV('jobe2:HubCollection',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Collection')
  do AfterLookup
  do Value::job:Postcode_Collection
  do Value::jobe2:HubCollection
  do Value::job:Address_Line3_Collection
  do SendAlert
  do Comment::job:Address_Line3_Collection
  do Value::jobe2:HubCollection  !1

Value::job:Address_Line3_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line3_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Collection'',''amendaddress_job:address_line3_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Collection')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Collection',p_web.GetSessionValue('job:Address_Line3_Collection'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3_Collection&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value')

Comment::job:Address_Line3_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_comment')

Prompt::jobe2:HubDelivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Hub')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt')

Validate::jobe2:HubDelivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetValue('NewValue'))
    jobe2:HubDelivery = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubDelivery
    do Value::jobe2:HubDelivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubDelivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubDelivery = p_web.GetValue('Value')
  End
    jobe2:HubDelivery = Upper(jobe2:HubDelivery)
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  do Value::jobe2:HubDelivery
  do SendAlert
  do Value::job:Postcode_Delivery  !1

Value::jobe2:HubDelivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jobe2:HubDelivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubDelivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubDelivery'',''amendaddress_jobe2:hubdelivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubDelivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubDelivery',p_web.GetSessionValueFormat('jobe2:HubDelivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value')

Comment::jobe2:HubDelivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_comment')

Prompt::jobe2:HubCollection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Hub')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt')

Validate::jobe2:HubCollection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubCollection',p_web.GetValue('NewValue'))
    jobe2:HubCollection = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubCollection
    do Value::jobe2:HubCollection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubCollection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubCollection = p_web.GetValue('Value')
  End
    jobe2:HubCollection = Upper(jobe2:HubCollection)
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  do Value::jobe2:HubCollection
  do SendAlert
  do Value::job:Postcode_Collection  !1

Value::jobe2:HubCollection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- jobe2:HubCollection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubCollection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCollection'',''amendaddress_jobe2:hubcollection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubCollection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCollection',p_web.GetSessionValueFormat('jobe2:HubCollection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value')

Comment::jobe2:HubCollection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_comment')

Prompt::job:Postcode_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Postcode')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt')

Validate::job:Postcode_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode_Delivery',p_web.GetValue('NewValue'))
    job:Postcode_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode_Delivery
    do Value::job:Postcode_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode_Delivery = p_web.GetValue('Value')
  End
  do Value::job:Postcode_Delivery
  do SendAlert

Value::job:Postcode_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Postcode_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Delivery'',''amendaddress_job:postcode_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Delivery',p_web.GetSessionValueFormat('job:Postcode_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value')

Comment::job:Postcode_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_comment')

Prompt::job:Postcode_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Postcode')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt')

Validate::job:Postcode_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode_Collection',p_web.GetValue('NewValue'))
    job:Postcode_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode_Collection
    do Value::job:Postcode_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode_Collection = p_web.GetValue('Value')
  End
  do Value::job:Postcode_Collection
  do SendAlert

Value::job:Postcode_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Postcode_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Collection'',''amendaddress_job:postcode_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Collection',p_web.GetSessionValueFormat('job:Postcode_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value')

Comment::job:Postcode_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_comment')

Prompt::job:Telephone_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt')

Validate::job:Telephone_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetValue('NewValue'))
    job:Telephone_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Delivery
    do Value::job:Telephone_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Delivery = p_web.GetValue('Value')
  End
    job:Telephone_Delivery = Upper(job:Telephone_Delivery)
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Delivery')) = 0
          p_web.SSV('job:Telephone_Delivery','')
          p_web.SSV('Comment:TelephoneNumberDelivery','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberDelivery','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Delivery
  do SendAlert
  do Comment::job:Telephone_Delivery

Value::job:Telephone_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Telephone_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Delivery'',''amendaddress_job:telephone_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Delivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Delivery',p_web.GetSessionValueFormat('job:Telephone_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value')

Comment::job:Telephone_Delivery  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumberDelivery'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_comment')

Prompt::job:Telephone_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Telephone_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Collection',p_web.GetValue('NewValue'))
    job:Telephone_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Collection
    do Value::job:Telephone_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Collection = p_web.GetValue('Value')
  End
    job:Telephone_Collection = Upper(job:Telephone_Collection)
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Collection')) = 0
          p_web.SSV('job:Telephone_Collection','')
          p_web.SSV('Comment:TelephoneNumberCollection','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberCollection','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Collection
  do SendAlert
  do Comment::job:Telephone_Collection

Value::job:Telephone_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Telephone_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Collection'',''amendaddress_job:telephone_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Collection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Collection',p_web.GetSessionValueFormat('job:Telephone_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value')

Comment::job:Telephone_Collection  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumberCollection'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_comment')

Validate::Button:CopyEndUserAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CopyEndUserAddress',p_web.GetValue('NewValue'))
    do Value::Button:CopyEndUserAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
  p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
  p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
  p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
  p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
  p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))
  p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress
  do SendAlert
  do Value::job:Address_Line1_Delivery  !1
  do Value::job:Address_Line2_Delivery  !1
  do Value::job:Address_Line3_Delivery  !1
  do Value::job:Company_Name_Delivery  !1
  do Value::job:Telephone_Delivery  !1
  do Value::jobe2:HubDelivery  !1
  do Value::job:Postcode_Delivery  !1

Value::Button:CopyEndUserAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress')&'.disabled=true;sv(''Button:CopyEndUserAddress'',''amendaddress_button:copyenduseraddress_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CopyEndUserAddress','Copy / Clear Address','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ClearAddressDetails&AddType=D')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value')

Comment::Button:CopyEndUserAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:CopyEndUserAddress1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:CopyEndUserAddress1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CopyEndUserAddress1',p_web.GetValue('NewValue'))
    do Value::Button:CopyEndUserAddress1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
  p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
  p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
  p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
  p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
  p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))
  p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress1
  do SendAlert
  do Value::job:Address_Line1_Collection  !1
  do Value::job:Address_Line2_Collection  !1
  do Value::job:Address_Line3_Collection  !1
  do Value::job:Company_Name_Collection  !1
  do Value::job:Telephone_Collection  !1
  do Value::jobe2:HubCollection  !1
  do Value::job:Postcode_Collection  !1

Value::Button:CopyEndUserAddress1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress1')&'.disabled=true;sv(''Button:CopyEndUserAddress1'',''amendaddress_button:copyenduseraddress1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CopyEndUserAddress1','Copy / Clear Address','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ClearAddressDetails&AddType=C')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value')

Comment::Button:CopyEndUserAddress1  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Delivery_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Delivery Text')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Delivery_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Delivery_Text',p_web.GetValue('NewValue'))
    jbn:Delivery_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Delivery_Text
    do Value::jbn:Delivery_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Delivery_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Delivery_Text = p_web.GetValue('Value')
  End
    jbn:Delivery_Text = Upper(jbn:Delivery_Text)
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  do Value::jbn:Delivery_Text
  do SendAlert

Value::jbn:Delivery_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- TEXT --- jbn:Delivery_Text
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Delivery_Text')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Delivery_Text'',''amendaddress_jbn:delivery_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Delivery_Text',p_web.GetSessionValue('jbn:Delivery_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value')

Comment::jbn:Delivery_Text  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Collection_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Collection Text')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Collection_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Collection_Text',p_web.GetValue('NewValue'))
    jbn:Collection_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Collection_Text
    do Value::jbn:Collection_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Collection_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Collection_Text = p_web.GetValue('Value')
  End
    jbn:Collection_Text = Upper(jbn:Collection_Text)
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  do Value::jbn:Collection_Text
  do SendAlert

Value::jbn:Collection_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- TEXT --- jbn:Collection_Text
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Collection_Text')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Collection_Text'',''amendaddress_jbn:collection_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Collection_Text',p_web.GetSessionValue('jbn:Collection_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Collection_Text),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value')

Comment::jbn:Collection_Text  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:DelContactName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contact Name')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:DelContactName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:DelContactName',p_web.GetValue('NewValue'))
    jbn:DelContactName = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:DelContactName
    do Value::jbn:DelContactName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:DelContactName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:DelContactName = p_web.GetValue('Value')
  End
    jbn:DelContactName = Upper(jbn:DelContactName)
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  do Value::jbn:DelContactName
  do SendAlert

Value::jbn:DelContactName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:DelContactName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelContactName'',''amendaddress_jbn:delcontactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelContactName',p_web.GetSessionValueFormat('jbn:DelContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_value')

Comment::jbn:DelContactName  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:ColContatName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contact Name')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:ColContatName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:ColContatName',p_web.GetValue('NewValue'))
    jbn:ColContatName = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:ColContatName
    do Value::jbn:ColContatName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:ColContatName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:ColContatName = p_web.GetValue('Value')
  End
    jbn:ColContatName = Upper(jbn:ColContatName)
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  do Value::jbn:ColContatName
  do SendAlert

Value::jbn:ColContatName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColContatName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:ColContatName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColContatName'',''amendaddress_jbn:colcontatname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColContatName',p_web.GetSessionValueFormat('jbn:ColContatName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_value')

Comment::jbn:ColContatName  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:DelDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Department')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:DelDepartment  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:DelDepartment',p_web.GetValue('NewValue'))
    jbn:DelDepartment = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:DelDepartment
    do Value::jbn:DelDepartment
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:DelDepartment',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:DelDepartment = p_web.GetValue('Value')
  End
    jbn:DelDepartment = Upper(jbn:DelDepartment)
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  do Value::jbn:DelDepartment
  do SendAlert

Value::jbn:DelDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:DelDepartment')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelDepartment'',''amendaddress_jbn:deldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelDepartment',p_web.GetSessionValueFormat('jbn:DelDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_value')

Comment::jbn:DelDepartment  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:ColDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Department')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:ColDepartment  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:ColDepartment',p_web.GetValue('NewValue'))
    jbn:ColDepartment = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:ColDepartment
    do Value::jbn:ColDepartment
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:ColDepartment',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:ColDepartment = p_web.GetValue('Value')
  End
    jbn:ColDepartment = Upper(jbn:ColDepartment)
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  do Value::jbn:ColDepartment
  do SendAlert

Value::jbn:ColDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:ColDepartment')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColDepartment'',''amendaddress_jbn:coldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColDepartment',p_web.GetSessionValueFormat('jbn:ColDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_value')

Comment::jbn:ColDepartment  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:Sub_Sub_Account  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:Sub_Sub_Account  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',p_web.GetValue('NewValue'))
    jobe:Sub_Sub_Account = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:Sub_Sub_Account
    do Value::jobe:Sub_Sub_Account
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:Sub_Sub_Account = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','jobe:Sub_Sub_Account')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::job:Address_Line1_Delivery
  do Value::job:Address_Line2_Delivery
  do Value::job:Address_Line3_Delivery
  do Value::job:Telephone_Delivery
  do Value::job:Company_Name_Delivery
  do Value::jobe:Sub_Sub_Account
  do SendAlert
  do Comment::jobe:Sub_Sub_Account

Value::jobe:Sub_Sub_Account  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:Sub_Sub_Account
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe:Sub_Sub_Account')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Sub_Sub_Account'',''amendaddress_jobe:sub_sub_account_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Sub_Sub_Account')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','jobe:Sub_Sub_Account',p_web.GetSessionValue('jobe:Sub_Sub_Account'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseSubAddresses')&'?LookupField=jobe:Sub_Sub_Account&Tab=2&ForeignField=sua:AccountNumber&_sort=sua:CompanyName&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value')

Comment::jobe:Sub_Sub_Account  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('AmendAddress_job:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name
      else
        do Value::job:Company_Name
      end
  of lower('AmendAddress_job:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1
      else
        do Value::job:Address_Line1
      end
  of lower('AmendAddress_job:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2
      else
        do Value::job:Address_Line2
      end
  of lower('AmendAddress_job:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3
      else
        do Value::job:Address_Line3
      end
  of lower('AmendAddress_jobe2:HubCustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCustomer
      else
        do Value::jobe2:HubCustomer
      end
  of lower('AmendAddress_job:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode
      else
        do Value::job:Postcode
      end
  of lower('AmendAddress_job:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Number
      else
        do Value::job:Telephone_Number
      end
  of lower('AmendAddress_job:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Fax_Number
      else
        do Value::job:Fax_Number
      end
  of lower('AmendAddress_jobe:EndUserEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserEmailAddress
      else
        do Value::jobe:EndUserEmailAddress
      end
  of lower('AmendAddress_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('AmendAddress_jobe:VatNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VatNumber
      else
        do Value::jobe:VatNumber
      end
  of lower('AmendAddress_jobe2:IDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:IDNumber
      else
        do Value::jobe2:IDNumber
      end
  of lower('AmendAddress_jobe2:SMSNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSNotification
      else
        do Value::jobe2:SMSNotification
      end
  of lower('AmendAddress_jobe2:SMSAlertNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSAlertNumber
      else
        do Value::jobe2:SMSAlertNumber
      end
  of lower('AmendAddress_jobe2:EmailNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailNotification
      else
        do Value::jobe2:EmailNotification
      end
  of lower('AmendAddress_jobe2:EmailAlertAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailAlertAddress
      else
        do Value::jobe2:EmailAlertAddress
      end
  of lower('AmendAddress_jobe2:CourierWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:CourierWaybillNumber
      else
        do Value::jobe2:CourierWaybillNumber
      end
  of lower('AmendAddress_job:Company_Name_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Delivery
      else
        do Value::job:Company_Name_Delivery
      end
  of lower('AmendAddress_job:Company_Name_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Collection
      else
        do Value::job:Company_Name_Collection
      end
  of lower('AmendAddress_job:Address_Line1_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Delivery
      else
        do Value::job:Address_Line1_Delivery
      end
  of lower('AmendAddress_job:Address_Line1_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Collection
      else
        do Value::job:Address_Line1_Collection
      end
  of lower('AmendAddress_job:Address_Line2_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Delivery
      else
        do Value::job:Address_Line2_Delivery
      end
  of lower('AmendAddress_job:Address_Line2_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Collection
      else
        do Value::job:Address_Line2_Collection
      end
  of lower('AmendAddress_job:Address_Line3_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Delivery
      else
        do Value::job:Address_Line3_Delivery
      end
  of lower('AmendAddress_job:Address_Line3_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Collection
      else
        do Value::job:Address_Line3_Collection
      end
  of lower('AmendAddress_jobe2:HubDelivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubDelivery
      else
        do Value::jobe2:HubDelivery
      end
  of lower('AmendAddress_jobe2:HubCollection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCollection
      else
        do Value::jobe2:HubCollection
      end
  of lower('AmendAddress_job:Postcode_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Delivery
      else
        do Value::job:Postcode_Delivery
      end
  of lower('AmendAddress_job:Postcode_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Collection
      else
        do Value::job:Postcode_Collection
      end
  of lower('AmendAddress_job:Telephone_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Delivery
      else
        do Value::job:Telephone_Delivery
      end
  of lower('AmendAddress_job:Telephone_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Collection
      else
        do Value::job:Telephone_Collection
      end
  of lower('AmendAddress_Button:CopyEndUserAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress
      else
        do Value::Button:CopyEndUserAddress
      end
  of lower('AmendAddress_Button:CopyEndUserAddress1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress1
      else
        do Value::Button:CopyEndUserAddress1
      end
  of lower('AmendAddress_jbn:Delivery_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Delivery_Text
      else
        do Value::jbn:Delivery_Text
      end
  of lower('AmendAddress_jbn:Collection_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Collection_Text
      else
        do Value::jbn:Collection_Text
      end
  of lower('AmendAddress_jbn:DelContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelContactName
      else
        do Value::jbn:DelContactName
      end
  of lower('AmendAddress_jbn:ColContatName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColContatName
      else
        do Value::jbn:ColContatName
      end
  of lower('AmendAddress_jbn:DelDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelDepartment
      else
        do Value::jbn:DelDepartment
      end
  of lower('AmendAddress_jbn:ColDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColDepartment
      else
        do Value::jbn:ColDepartment
      end
  of lower('AmendAddress_jobe:Sub_Sub_Account_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Sub_Sub_Account
      else
        do Value::jobe:Sub_Sub_Account
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)

PreCopy  Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)

PreDelete       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.setsessionvalue('showtab_AmendAddress',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('jobe2:SMSNotification') = 0
            p_web.SetValue('jobe2:SMSNotification',0)
            jobe2:SMSNotification = 0
          End
          If p_web.IfExistsValue('jobe2:EmailNotification') = 0
            p_web.SetValue('jobe2:EmailNotification',0)
            jobe2:EmailNotification = 0
          End
  If p_web.GSV('Hide:SubSubAccount') <> 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          job:Company_Name = Upper(job:Company_Name)
          p_web.SetSessionValue('job:Company_Name',job:Company_Name)
        If loc:Invalid <> '' then exit.
          job:Address_Line1 = Upper(job:Address_Line1)
          p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
        If loc:Invalid <> '' then exit.
          job:Address_Line2 = Upper(job:Address_Line2)
          p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
        If loc:Invalid <> '' then exit.
          job:Address_Line3 = Upper(job:Address_Line3)
          p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
        If loc:Invalid <> '' then exit.
          jobe2:HubCustomer = Upper(jobe2:HubCustomer)
          p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
        If loc:Invalid <> '' then exit.
          job:Telephone_Number = Upper(job:Telephone_Number)
          p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
        If loc:Invalid <> '' then exit.
          job:Fax_Number = Upper(job:Fax_Number)
          p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
        If loc:Invalid <> '' then exit.
          jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
          p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
        If loc:Invalid <> '' then exit.
    If p_web.GSV('Hide:VatNumber') <> 1
          jobe:VatNumber = Upper(jobe:VatNumber)
          p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
        If loc:Invalid <> '' then exit.
    End
          jobe2:IDNumber = Upper(jobe2:IDNumber)
          p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('jobe2:SMSNotification') = 0)
          jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
          p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
        If loc:Invalid <> '' then exit.
      End
          jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
          p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
        If loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
          p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
          p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
          p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
          p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
          p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
          p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
          p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          jobe2:HubDelivery = Upper(jobe2:HubDelivery)
          p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          jobe2:HubCollection = Upper(jobe2:HubCollection)
          p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Telephone_Delivery = Upper(job:Telephone_Delivery)
          p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Telephone_Collection = Upper(job:Telephone_Collection)
          p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:Delivery_Text = Upper(jbn:Delivery_Text)
          p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:Collection_Text = Upper(jbn:Collection_Text)
          p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:DelContactName = Upper(jbn:DelContactName)
          p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:ColContatName = Upper(jbn:ColContatName)
          p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:DelDepartment = Upper(jbn:DelDepartment)
          p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:ColDepartment = Upper(jbn:ColDepartment)
          p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 4
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  !p_web.SetSessionValue('job:Company_Name',p_web.GetSessionValue('tmp:CompanyName'))
  !p_web.SetSessionValue('job:Address_Line1',p_web.GetSessionValue('tmp:AddressLine1'))
  !p_web.SetSessionValue('job:Address_Line2',p_web.GetSessionValue('tmp:AddressLine2'))
  !p_web.SetSessionValue('job:Address_Line3',p_web.GetSessionValue('tmp:Suburb'))
  !p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetSessionValue('tmp:HubCustomer'))
  !p_web.SetSessionValue('job:Telephone_Number',p_web.GetSessionValue('tmp:TelephoneNumber'))
  !p_web.SetSessionValue('job:Fax_Number',p_web.GetSessionValue('tmp:FaxNumber'))
  !p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetSessionValue('tmp:EmailAddress'))
  !p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetSessionValue('tmp:EndUserTelephoneNumber'))
  !p_web.SetSessionValue('jobe2:IDNumber',p_web.GetSessionValue('tmp:IDNumber'))
  !p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetSessionValue('tmp:SMSNotification'))
  !p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetSessionValue('tmp:NotificationMobileNumber'))
  !p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetSessionValue('tmp:EmailNotification'))
  !p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetSessionValue('tmp:NotificationEmailAddress'))
  !
  !p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetSessionValue('tmp:CompanyNameDelivery'))
  !p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetSessionValue('tmp:CompanyNameCollection'))
  !p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetSessionValue('tmp:AddressLine1Delivery'))
  !p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetSessionValue('tmp:AddressLine1Collection'))
  !p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetSessionValue('tmp:SuburbDelivery'))
  !p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetSessionValue('tmp:SuburbCollection'))
  !p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetSessionValue('tmp:HubDelivery'))
  !p_web.SetSessionValue('jobe2:HubCollection',p_web.GetSessionValue('tmp:HubCollection'))
  !p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetSessionValue('tmp:TelephoneNumberDelivery'))
  !p_web.SetSessionValue('job:Telephone_Collection',p_web.GetSessionValue('tmp:TelephoneNumberCollection'))
  !
  !
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name')
  p_web.StoreValue('job:Address_Line1')
  p_web.StoreValue('job:Address_Line2')
  p_web.StoreValue('job:Address_Line3')
  p_web.StoreValue('job:Postcode')
  p_web.StoreValue('job:Telephone_Number')
  p_web.StoreValue('job:Fax_Number')
  p_web.StoreValue('jobe:EndUserEmailAddress')
  p_web.StoreValue('jobe:EndUserTelNo')
  p_web.StoreValue('jobe:VatNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name_Delivery')
  p_web.StoreValue('job:Company_Name_Collection')
  p_web.StoreValue('job:Address_Line1_Delivery')
  p_web.StoreValue('job:Address_Line1_Collection')
  p_web.StoreValue('job:Address_Line2_Delivery')
  p_web.StoreValue('job:Address_Line2_Collection')
  p_web.StoreValue('job:Address_Line3_Delivery')
  p_web.StoreValue('job:Address_Line3_Collection')
  p_web.StoreValue('job:Postcode_Delivery')
  p_web.StoreValue('job:Postcode_Collection')
  p_web.StoreValue('job:Telephone_Delivery')
  p_web.StoreValue('job:Telephone_Collection')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Delivery_Text')
  p_web.StoreValue('jbn:Collection_Text')
  p_web.StoreValue('jbn:DelContactName')
  p_web.StoreValue('jbn:ColContatName')
  p_web.StoreValue('jbn:DelDepartment')
  p_web.StoreValue('jbn:ColDepartment')
  p_web.StoreValue('jobe:Sub_Sub_Account')
