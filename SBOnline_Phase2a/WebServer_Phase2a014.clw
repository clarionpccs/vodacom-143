

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A014.INC'),ONCE        !Local module procedure declarations
                     END


ForceCourier         PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    !Incoming Courier
    If (def:Force_Outoing_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Outoing_Courier <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Incoming_Courier = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     FilesOpened = False
  END
ForceFaultCodes      PROCEDURE  (func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,func:Type,fManufacturer) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    !(func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,fManufacturer)

    If func:Type = 'C' or func:type = 'X'
        If func:ChargeableJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:CChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:CRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = func:CRepairType
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
! Deleted (DBH 20/05/2006) #6733 - Do not check the manufacturer for char fault codes
!                                 Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!                                 man:Manufacturer    = fManufacturer
!                                 If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Found
!                                     If man:ForceCharFaultCodes
!                                         Return Level:Fatal
!                                     End !If man:ForceCharFaultCodes
!                                 Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Error
!                                 End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
! End (DBH 20/05/2006) #6733
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                    Else !If func:CRepairType <> ''
                        Return Level:Fatal
                    End !If func:CRepairType <> ''
                End !If cha:Force_Warranty = 'YES'
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Chargeable_Job = 'YES'
    End !If func:Type = 'C' or func:type = 'X'

    If func:Type = 'W' or func:Type = 'X'
        If func:WarrantyJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:WChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:WRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = func:WRepairType
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                    Else !If WRetairType <> ''
                        Return Level:Fatal
                    End !If WRetairType <> ''
                End !If cha:Force_Warranty = 'YES'

            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Warranty_Job = 'YES'
    End !If func:Type = 'W' or func:Type = 'X'
    Return Level:Benign
ForceAccessories     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    If (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B' And func:Type = 'B') Or |
        (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
        Return Level:Fatal
    End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
    Return Level:Benign
ForceNetwork         PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    If (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 'B' and func:Type = 'B') Or |
        (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') <> 'I' and func:Type = 'C')
        Return 1
    Else !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Return 0
    End !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
CustomerNameRequired PROCEDURE  (func:AccountNumber)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    !Check the trade account details to see if a customer name is required
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = func:AccountNumber
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            If tra:Invoice_Sub_Accounts = 'YES'
                If sub:ForceEndUserName
                    Return# = 1
                End !If sub:ForceEndUserName
            Else !If tra:Invoice_Sub_Accounts = 'YES'
                If tra:Use_Contact_Name = 'YES'
                    Return# = 1
                End!If tra:Use_Contact_Name = 'YES'

            End !If tra:Invoice_Sub_Accounts = 'YES'
        End!If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    End!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles

    Return Return#

SaveFiles  ROUTINE
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     FilesOpened = False
  END
Pricing_Routine      PROCEDURE  (f_type,f_labour,f_parts,f_pass,f_claim,f_handling,f_exchange,f_RRCRate,f_RRCParts) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
tmp:AccessoryExists  BYTE(0)                               !
tmp:TradeAccount     STRING(15)                            !
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_epr_id   ushort,auto
  CODE
    !This does nothing anymore...
    !Replaced by JobPricingRoutine
    Return




!
!
!! Source Bit
!    f_labour = 0
!    f_parts = 0
!    f_pass = 0
!    f_claim = 0
!    f_handling = 0
!    f_exchange = 0
!    f_RRCRate   = 0
!    f_RRCParts = 0
!! Chargeable Job?
!
!    If job:account_number = ''
!        Return
!    End
!    Case f_type
!        Of 'C'
!            If job:chargeable_job = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!            ! No Charge?
!!                If job:ignore_chargeable_charges = 'YES'
!!                    Return
!!                End
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate = 0
!                        f_RRCParts = 0
!
!                    Else !If cha:no_charge = 'YES'
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            rrc_total$   = 0
!                            save_par_id = access:parts.savefile()
!                            access:parts.clearkey(par:part_number_key)
!                            par:ref_number  = job:ref_number
!                            set(par:part_number_key,par:part_number_key)
!                            loop
!                                if access:parts.next()
!                                   break
!                                end !if
!                                if par:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += par:sale_cost * par:quantity
!                                rrc_total$  += par:RRCSaleCost * par:Quantity   !was + changed JC
!                                !message(par:rrcSaleCost * par:quantity&' - '&rcc_total$)
!                            end !loop
!                            access:parts.restorefile(save_par_id)
!                            f_parts = parts_total$
!                            f_rrcparts  = rrc_total$
!
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_rrcparts = 0
!
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!
!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    f_handling  = trc:HandlingFee
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                f_handling  = suc:HandlingFee
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:invoice_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                f_handling  = trc:HandlingFee
!                                use_standard# = 0
!
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:invoice_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                                f_handling  = sta:HandlingFee
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'W'
!
!        !Warranty Job
!            If job:warranty_job = 'YES'
!                !Do not reprice Warranty Jobs that are completed - L945 (DBH: 03-09-2003)
!                If job:Date_Completed <> 0
!
!                    Return
!                End !If job:Date_Completed <> 0
!                parts_discount# = 0
!                labour_discount# = 0
!                f_pass = 1
!!               If job:ignore_warranty_charges = 'YES'
!!                   Return
!!               End
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:warranty_charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCParts  = 0
!                    f_RRCRate   = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCParts  = 0
!                        f_RRCRate   = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!
!                            Access:STOCK.Open()
!                            Access:STOCK.UseFile()
!
!                            tmp:AccessoryExists = false
!                            tmp:TradeAccount = ''
!
!                            save_wpr_id = access:warparts.savefile()
!                            access:warparts.clearkey(wpr:part_number_key)
!                            wpr:ref_number  = job:ref_number
!                            set(wpr:part_number_key,wpr:part_number_key)
!                            loop
!                                if access:warparts.next()
!                                   break
!                                end !if
!                                if wpr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += wpr:purchase_cost * wpr:quantity
!                                RRCParts_Total$ += wpr:RRCPurchaseCost * wpr:Quantity
!
!                                ! --------------------------------------------------------------------
!                                ! Check if any parts are accessories
!                                Access:STOCK.ClearKey(sto:Ref_Number_Key)
!                                sto:Ref_Number = wpr:Part_Ref_Number
!                                if not Access:STOCK.Fetch(sto:Ref_Number_Key)
!                                    if sto:Accessory = 'YES'
!                                        tmp:AccessoryExists = true
!                                    end
!                                    i# = instring('<32>',clip(sto:Location),1,1)
!                                    if i# <> 0
!                                        tmp:TradeAccount = sto:Location[1:i#]
!                                    end
!                                end
!                                ! --------------------------------------------------------------------
!                            end !loop
!                            access:warparts.restorefile(save_wpr_id)
!                            Access:STOCK.Close()
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!
!                        use_standard# = 1
!!                        If TRA:ZeroChargeable = 'YES'
!!                            f_parts = 0
!!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:Warranty_charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type_warranty
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:warranty_charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type_warranty
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_claim     = trc:WarrantyClaimRate
!                                    f_handling  = trc:HandlingFee
!                                    f_Exchange  = trc:Exchange
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_claim     = suc:WarrantyClaimRate
!                                f_handling  = suc:HandlingFee
!                                f_Exchange  = suc:Exchange
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = tra:account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:warranty_charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type_warranty
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_claim     = trc:WarrantyClaimRate
!                                f_handling  = trc:HandlingFee
!                                f_Exchange  = trc:Exchange
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:warranty_charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type_warranty
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_claim     = sta:WarrantyClaimRate
!                                f_handling  = sta:HandlingFee
!                                f_Exchange  = sta:Exchange
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                        ! ------------------------------------------------------------------------
!!                        if tmp:AccessoryExists = true and job:warranty_charge_type = 'WARRANTY (MFTR)'
!!
!!                            if tmp:TradeAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!!                                ! ARC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_Exchange = 0
!!                                else
!!                                    f_Exchange = 0
!!                                    f_handling  = 0
!!                                end
!!                            else
!!                                ! RRC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_handling  = 0
!!                                else
!!                                    f_handling  = 0
!!                                end
!!                            end
!!                        else
!                            If job:Exchange_Unit_Number <> 0 AND jobe:ExchangedATRRC = TRUE
!                                !jobe:ExchangeRate = Exchange"
!                                f_handling  = 0
!                            Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                                IF SentToHub(job:Ref_Number)
!                                    f_Exchange = 0
!                                    !jobe:HandlingFee  = Handling"
!                                ELSE
!                                    f_Exchange = 0
!                                    f_handling  = 0
!                                END
!                            End
!                        !End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                        ! ------------------------------------------------------------------------
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'E'
!            If job:estimate = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!!                If job:ignore_estimate_charges = 'YES'
!!                    Return
!!                End!If job:ignore_estimate_charges = 'YES'
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCRate   = 0
!                    f_RRCParts  = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate  = 0
!                        f_RRCParts = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!                            save_epr_id = access:estparts.savefile()
!                            access:estparts.clearkey(epr:part_number_key)
!                            epr:ref_number  = job:ref_number
!                            set(epr:part_number_key,epr:part_number_key)
!                            loop
!                                if access:estparts.next()
!                                   break
!                                end !if
!                                if epr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += epr:sale_cost * epr:quantity
!                                RRCParts_Total$ += epr:RRCSaleCost * epr:Quantity
!                            end !loop
!                            access:estparts.restorefile(save_epr_id)
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                        End!If TRA:ZeroChargeable = 'YES'
!
!                        If tra:invoice_sub_accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:estimate = 'YES'
!    End!Case f_type
!
PendingJob           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:EDIFileType    = 'ALCATEL' Or  |
            man:EDIFileType   = 'BOSCH' Or    |
            man:EDIFileType   = 'ERICSSON' Or |
            man:EDIFileType   = 'LG' Or    |
            man:EDIFileType   = 'MAXON' Or    |
            man:EDIFileType   = 'MOTOROLA' Or |
            man:EDIFileType   = 'NEC' Or      |
            man:EDIFileType   = 'NOKIA' Or    |
            man:EDIFileType   = 'SIEMENS' Or  |
            man:EDIFileType   = 'SAMSUNG' Or  |
            man:EDIFileType   = 'SAMSUNG SA' Or  |
            man:EDIFileType   = 'MITSUBISHI' Or   |
            man:EDIFileType   = 'TELITAL' Or  |
            man:EDIFileType   = 'SAGEM' Or    |
            man:EDIFileType   = 'SONY' Or     |
            man:EDIFileType   = 'PHILIPS' Or     |
            man:EDIFileType   = 'PANASONIC'

            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

            !Job should never becaome an EDI exception by mistake
            !So no need to double check if it's edi is set to exception

            case p_web.GSV('job:EDI')
            of 'EXC' orof 'REJ'
                p_web.SSV('wob:EDI',p_web.GSV('job:EDI'))
                return
            of 'EXM' orof 'AAJ'
                return
            end ! case p_web.GSV('job:EDI')

            !Do the calculation to check if a job should appear in the Pending Claims Table
            ! Job must be completed, a warranty job, and not already have a batch number. Not be an exception, a bouncer and not be excluded (by Charge TYpe or Repair Type)

            found# = 0
            If p_web.GSV('job:Date_Completed') <> ''
!Stop(2)
                If p_web.GSV('job:Warranty_Job') = 'YES'
!Stop(3)
                    If p_web.GSV('job:EDI_Batch_Number') = 0
!Stop(4)
                        If p_web.GSV('job:EDI') <> 'AAJ'
!Stop(5)
                            If p_web.GSV('job:EDI') <> 'EXC'
!Stop(6)
                                If ~(p_web.GSV('job:Bouncer') <> '' And (p_web.GSV('job:Bouncer_Type') = 'BOT' Or p_web.GSV('job:Bouncer_Type') = 'WAR'))
!Stop(7)
                                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                                    cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
                                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Found
                                        If cha:Exclude_EDI <> 'YES'
!Stop(8)
                                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                                            rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                                            rtd:Warranty = 'YES'
                                            rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
                                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Found
                                                If ~rtd:ExcludeFromEDI
!Stop(9)
                                                    If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                                        p_web.SSV('wob:EDI','NO')
                                                    End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'

                                                    !Check if the job is a 48 Hour Scrap/RTM with only one exchange - 4449
                                                    ScrapCheck# = 0
                                                    ! Is this a 48 hour job? (DBH: 05/02/2007)
                                                    If p_web.GSV('jobe:Engineer48HourOption') = 1
                                                        ! Is there only one exchange attached? (DBH: 05/02/2007)
                                                        If p_web.GSV('job:Exchange_Unit_Number') > 0
                                                            If p_web.GSV('jobe:SecondExchangeNumber') = 0
                                                                If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.' Or p_web.GSV('job:Repair_Type_Warranty') = 'SCRAP'
                                                                    ScrapCheck# = 1
                                                                    ! Do not claim for this job (DBH: 05/02/2007)
                                                                End ! If job:Repair_Type_Warranty = 'R.T.M.' Or job:Repair_Type_Warranty = 'SCRAP'
                                                            End ! If jobe:SecondExchangeNumber = 0
                                                        End ! If job:Exchange_Unit_Number > 0
                                                    End ! If jobe:Engineer48HourOption = 1

                                                    If ScrapCheck# = 0
!Stop(10)
                                                        If p_web.GSV('job:EDI') <> 'NO'
                                                            p_web.SSV('jobe2:InPendingDate',Today())
                                                        End ! If job:EDI <> 'NO'
                                                        p_web.SSV('job:EDI','NO')
                                                        found# = 1
                                                    End ! If ScrapCheck# = 0
                                                End ! If ~rtd:ExcludeFromEDI
                                            Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Error
                                            End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign

                                        End ! If cha:Exclude_EDI <> 'YES'
                                    Else ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Error
                                    End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign

                                End ! If (~job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR'))
                            Else ! If job:EDI <> 'EXC'
                                if (p_web.GSV('BookingSite') = 'RRC')
                                    p_web.SSV('wob:EDI','AAJ')
                                    p_web.SSV('job:EDI','AAJ')
                                Else ! If glo:WebJob
                                    p_web.SSV('job:EDI','EXC')
                                End ! If glo:WebJob
                                found# = 1
                            End ! If job:EDI <> 'EXC'
                        Else ! If job:EDI <> 'AAJ'
                        End ! If job:EDI <> 'AAJ'
                    Else ! If job:EDI_Batch_Number = 0
                        ! This is an error check (DBH: 05/02/2007)
                        ! If the job has a batch number then it can only be 3 things. Invoice, Reconciled or exception (DBH: 05/02/2007)
                        If p_web.GSV('job:Invoice_Number_Warranty') <> ''
                            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                p_web.SSV('wob:EDI','FIN')
                            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                            p_web.SSV('job:EDI','FIN')
                        Else ! If job:Invoice_Number_Warranty <> ''
                            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                p_web.SSV('wob:EDI','YES')
                            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                            p_web.SSV('job:EDI','YES')
                        End ! If job:Invoice_Number_Warranty <> ''
                        found# = 1
                    End ! If job:EDI_Batch_Number = 0
                End ! If job:Warranty_Job = 'YES'
            End ! If job:Date_Completed <> ''
        End ! man:EDIFileType   = 'PANASONIC'

        ! Make sure the EDI value doesn't change once it's set (DBH: 05/02/2007)

        ! Reconciled (DBH: 05/02/2007)
        If (found# = 0)
            If (p_web.GSV('job:EDI') = 'YES')
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') = 'APP'
                    p_web.SSV('wob:EDI','YES')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI = 'APP'
                p_web.SSV('job:EDI','YES')
                found# = 1
            End ! If job:EDI = 'YES'
        End ! If Clip(tmp:Return) = ''

        ! Invoiced (DBH: 05/02/2007)
        If (found# = 0)
            If p_web.GSV('job:EDI') = 'FIN'
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                    p_web.SSV('wob:EDI','FIN')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                p_web.SSV('job:EDI','FIN')
                found# = 1
            End ! If job:EDI = 'FIN'
        End ! If Clip(tmp:Return) = ''


        ! If all else failes, then this shouldn't be a warranty claim (DBH: 05/02/2007)
        If (found# = 0)
            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                p_web.SSV('wob:EDI','XXX')
            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
            p_web.SSV('job:EDI','XXX')
            found# = 1
        End ! If Clip(tmp:Return) = ''

        ! Inserting (DBH 05/02/2007) # 8707 - Write warranty information to new file
        Access:JOBSWARR.ClearKey(jow:RefNumberKey)
        jow:RefNumber = p_web.GSV('job:Ref_Number')
        If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Error
            If Access:JOBSWARR.PrimeRecord() = Level:Benign
                jow:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:JOBSWARR.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:JOBSWARR.TryInsert() = Level:Benign
                    Access:JOBSWARR.CancelAutoInc()
                End ! If Access:JOBSWARR.TryInsert() = Level:Benign
            End ! If Access.JOBSWARR.PrimeRecord() = Level:Benign
        End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign


        If (p_web.GSV('job:EDI') = 'XXX' Or found#= 0)
            Relate:JOBSWARR.Delete(0)
        Else ! If tmp:Return = 'XXX'
            jow:Status = p_web.GSV('job:EDI')
            jow:BranchID = tra:BranchIdentification
!            If SentToHub(job:Ref_Number)
!                jow:RepairedAt = 'ARC'
!            Else ! If SentToHub(job:Ref_Number)
!                jow:RepairedAt = 'RRC'
!            End ! If SentToHub(job:Ref_Number)
            jow:Manufacturer = p_web.GSV('job:Manufacturer')

            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty = 'YES'
            cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
            Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
            End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            If cha:SecondYearWarranty
                jow:FirstSecondYear = 1
            Else ! If cha:SecondYearWarranty
                jow:FirstSecondYear = 0
            End ! If cha:SecondYearWarranty

            If (p_web.GSV('job:EDI') = 'NO')
                If jow:Submitted = 0
                    jow:Submitted = 1
                End ! If jow:Submitted = 0
            End ! If tmp:Return = 'NO'
            Access:JOBSWARR.Update()
        End ! If tmp:Return = 'XXX'
        ! End (DBH 05/02/2007) #8707

    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:CHARTYPE.Close
     Access:REPTYDEF.Close
     Access:MANUFACT.Close
     Access:JOBSWARR.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
SentToHub            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)
  CODE
    do openFiles
    p_web.SSV('SentToHub',0)
    if (p_web.GSV('jobe:HubRepair') = 1 Or p_web.GSV('jobe:HubRepairDate') > 0)
        p_web.SSV('SentToHub',1)
    Else !If jobe:HubRepair
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = p_web.GSV('job:Ref_Number')
        lot:NewLocation = p_web.GSV('Default:ARCLocation')
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
            p_web.SSV('SentToHub',1)
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
    End !If jobe:HubRepair
    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
ProductCodeRequired  PROCEDURE  (func:Manufacturer)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseProductCode
            Return# = 1
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     FilesOpened = False
  END
DateCodeValidation   PROCEDURE  (func:Manufacturer,func:DateCode,func:DateBooked) ! Declare Procedure
tmp:YearCode         STRING(30)                            !Year COde
tmp:MonthCode        STRING(30)                            !Month Code
tmp:Year             STRING(30)                            !Year
tmp:Month            STRING(30)                            !Month
  CODE
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer

    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

    Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


    !Now depending on the manufacturer determines where the
    !The date code is picked up from
    DoWeekCheck# = 0
    Case func:Manufacturer
        Of 'ALCATEL'
            tmp:YearCode    = Sub(func:DateCode,3,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'ERICSSON'
            tmp:YearCode    = Sub(func:DateCode,1,2)
            tmp:MonthCode   = Sub(func:DateCode,3,2)
            DoWeekCheck#    = 1
        Of 'PHILIPS'
            tmp:YearCode    = Sub(func:DateCode,5,2)
            tmp:MonthCode   = Sub(func:DateCode,7,2)
            DoWeekCheck#    = 1
        Of 'SAMSUNG'
            tmp:YearCode    = Sub(func:DateCode,4,1)
            tmp:MonthCode   = Sub(func:DateCode,5,1)
        Of 'BOSCH'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'SIEMENS'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode    = Sub(func:DateCode,2,1)
        Of 'MOTOROLA'
            tmp:YearCode    = Sub(func:DateCode,5,1)
            tmp:MonthCode   = Sub(func:DateCode,6,1)
    End !Case func:Manufacturer

    If DoWeekCheck#
        tmp:Year    = tmp:YearCode
        StartOfYear# = Deformat('1/1/' & tmp:Year,@d5)

        DayOfYear#   = StartOfYear# + (tmp:MonthCode * 7)

        tmp:Year    = Year(DayOfYear#)
        tmp:Month   = Month(DayOfYear#)

    Else
        Access:MANUDATE.Clearkey(mad:DateCodeKey)
        mad:Manufacturer    = func:Manufacturer
        If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Sub(func:DateCode,1,1) & Clip(tmp:MonthCode) & Clip(tmp:YearCode)
        Else !If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Clip(tmp:YearCode) & Clip(tmp:MonthCode)
        End !If func:Manufacturer = 'ALCATEL'

        If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Found
            tmp:Year    = mad:TheYear
            tmp:Month   = mad:TheMonth

        Else! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
    End !If DoWeekCheck#

    Loop x# = 1 To man:ClaimPeriod
        tmp:Month += 1
        If tmp:Month > 12
            tmp:Month = 1
            tmp:Year += 1
        End !If tmp:Month > 12
    End !Loop x# = 1 To man:ClaimDate

    If tmp:Year < Year(func:DateBooked)
        !POP Required
        Return Level:Fatal
    End !If tmp:Year > func:DateBooked
    If tmp:Year = Year(func:DateBooked) And tmp:Month < Month(func:DateBooked)
        Return Level:Fatal
    End !If tmp:Year = func:DateBooked And tmp:Month < Month(func:DateBooked)

    Return Level:Benign
