

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A023.INC'),ONCE        !Req'd for module callout resolution
                     END


RapidLocation        PROCEDURE  (STRING fLocation)         ! Declare Procedure
LOCATION::State  USHORT
FilesOpened     BYTE(0)
  CODE
    RETURN vod.IsRapidLocation(fLocation)
SaveFiles  ROUTINE
  LOCATION::State = Access:LOCATION.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF LOCATION::State <> 0
    Access:LOCATION.RestoreFile(LOCATION::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
SecurityCheckFailed  PROCEDURE  (f:Password,f:Area)        ! Declare Procedure
USERS::State  USHORT
ACCAREAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 1

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = f:Password
    If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
        acc:User_Level = use:User_Level
        acc:Access_Area = f:Area
        If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
            Return# = 0
        End ! If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
    End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  USERS::State = Access:USERS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  ACCAREAS::State = Access:ACCAREAS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF USERS::State <> 0
    Access:USERS.RestoreFile(USERS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ACCAREAS::State <> 0
    Access:ACCAREAS.RestoreFile(ACCAREAS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:ACCAREAS.Close
     FilesOpened = False
  END
BrowseChargeableParts PROCEDURE  (NetWebServerWorker p_web)
tmp:partStatus       STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(PARTS)
                      Project(par:Record_Number)
                      Project(par:Part_Number)
                      Project(par:Description)
                      Project(par:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
STOCK::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseChargeableParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseChargeableParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseChargeableParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseChargeableParts:FormName')
    else
      loc:FormName = 'BrowseChargeableParts_frm'
    End
    p_web.SSV('BrowseChargeableParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseChargeableParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseChargeableParts:NoForm')
    loc:FormName = p_web.GSV('BrowseChargeableParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseChargeableParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseChargeableParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(PARTS,par:recordnumberkey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'PAR:PART_NUMBER') then p_web.SetValue('BrowseChargeableParts_sort','1')
    ElsIf (loc:vorder = 'PAR:DESCRIPTION') then p_web.SetValue('BrowseChargeableParts_sort','2')
    ElsIf (loc:vorder = 'PAR:QUANTITY') then p_web.SetValue('BrowseChargeableParts_sort','3')
    ElsIf (loc:vorder = 'TMP:PARTSTATUS') then p_web.SetValue('BrowseChargeableParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseChargeableParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseChargeableParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseChargeableParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseChargeableParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseChargeableParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.SSV('Hide:InsertButton',0)
  p_web.SSV('Hide:ChangeButton',0)
  p_web.SSV('Hide:DeleteButton',0)
  
  p_web.SSV('RepairTypesNoParts',0)
  
  if (p_web.GSV('job:Date_Completed') > 0)
      p_web.SSV('Hide:DeleteButton',1)
      p_web.SSV('Hide:InsertButton',1)
  end ! if (p_web.GSV('job:Date_Completed') > 0)
  
  isJobInvoiced(p_web)
  if (p_web.GSV('isJobInvoiced') = 1)
      p_web.SSV('Hide:DeleteButton',1)
      p_web.SSV('Hide:InsertButton',1)
  end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
  
  if (p_web.GSV('Hide:InsertButton') = 0)
      IF (p_web.GSV('locEngineeringOption') = 'Not Set')
          p_web.SSV('Hide:InsertButton',1)
      ELSE
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
  
              Access:LOCATION.Clearkey(loc:location_Key)
              loc:location    = use:Location
              if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Found
                  if (loc:Active = 0)
                      ! can add parts from an inactive location
                      p_web.SSV('Hide:InsertButton',1)
                  end !if (loc:Active = 0)
              else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Error
              end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
          
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:InsertButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      END
  end ! if (error# = 0)
  
  ! DBH #10544 - Liquid Damage. Don't amend parts
  IF (p_web.GSV('jobe:Booking48HourOption') = 4)  OR (p_web.GSV('Job:ViewOnly') = 1)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:ChangeBUtton',1)
      p_web.SSV('Hide:DeleteButton',1)
  END ! IF (p_web.GSV('jobe:Booking48HourOption') = 4)
  p_web.site.SmallChangeButton.TextValue = p_web.Translate('Edit')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseChargeableParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseChargeableParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(par:Part_Number)','-UPPER(par:Part_Number)')
    Loc:LocateField = 'par:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(par:Description)','-UPPER(par:Description)')
    Loc:LocateField = 'par:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'par:Quantity','-par:Quantity')
    Loc:LocateField = 'par:Quantity'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:partStatus','-tmp:partStatus')
    Loc:LocateField = 'tmp:partStatus'
  of 5
    Loc:LocateField = ''
  of 7
    Loc:LocateField = ''
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('par:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseChargeableParts_LocatorPic','@s30')
  Of upper('par:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseChargeableParts_LocatorPic','@s30')
  Of upper('par:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BrowseChargeableParts_LocatorPic','@n8')
  Of upper('tmp:partStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseChargeableParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseChargeableParts:LookupFrom')
  End!Else
    loc:formaction = 'FormChargeableParts'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseChargeableParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseChargeableParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseChargeableParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="PARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="par:recordnumberkey"></input><13,10>'
  end
  If p_web.Translate('Chargeable Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeadingAlt')&'">'&p_web.Translate('Chargeable Parts',0)&'</span>'&CRLF
  End
  If clip('Chargeable Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseChargeableParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseChargeableParts.locate(''Locator2BrowseChargeableParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseChargeableParts.cl(''BrowseChargeableParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseChargeableParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseChargeableParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseChargeableParts','Part Number',,,,100,1)
        Else
          packet = clip(packet) & '<th width="'&clip(100)&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseChargeableParts','Description',,,,200,1)
        Else
          packet = clip(packet) & '<th width="'&clip(200)&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseChargeableParts','Quantity',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Quantity')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseChargeableParts','Status',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND par:Part_Number <> 'EXCH') AND  true
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Edit
        do AddPacket
        loc:columns += 1
    End ! Selecting
  End ! Field condition
  If (p_web.GSV('Hide:DeleteButton') = 0) AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  MyDelete
        do AddPacket
        loc:columns += 1
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('par:record_number',lower(Thisview{prop:order}),1,1) = 0 !and PARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'par:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('par:Record_Number'),p_web.GetValue('par:Record_Number'),p_web.GetSessionValue('par:Record_Number'))
      loc:FilterWas = 'par:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseChargeableParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseChargeableParts_Filter')
    p_web.SetSessionValue('BrowseChargeableParts_FirstValue','')
    p_web.SetSessionValue('BrowseChargeableParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,PARTS,par:recordnumberkey,loc:PageRows,'BrowseChargeableParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If PARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(PARTS,loc:firstvalue)
              Reset(ThisView,PARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If PARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(PARTS,loc:lastvalue)
            Reset(ThisView,PARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:PartStatus = getPartStatus('C')!Work out the colour
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(par:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Chargeable Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 0
      if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseChargeableParts')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseChargeableParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseChargeableParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseChargeableParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseChargeableParts.locate(''Locator1BrowseChargeableParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseChargeableParts.cl(''BrowseChargeableParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseChargeableParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseChargeableParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 0
    if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseChargeableParts')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  End
      IF (p_web.GSV('Hide:InsertButton') = 0)
          packet = clip(packet) & p_web.br
          Packet = clip(Packet) & |
              p_web.CreateButton('button','Adjustment','Adjustment','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormChargeableParts?&Insert_btn=Insert&adjustment=1')) & ''','''&clip('_self')&''')',,0,,,,,)
          packet = clip(packet) & p_web.br
          Do SendPacket
      end !IF (p_web.GSV('Hide:InsertButton) = 0)
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = par:Record_Number
    p_web._thisrow = p_web._nocolon('par:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseChargeableParts:LookupField')) = par:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((par:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseChargeableParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If PARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(PARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If PARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(PARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','par:Record_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','par:Record_Number',clip(loc:field),,'checked',,,'onclick="BrowseChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
      !Show Hide Browse Buttons
      error# = 0
      if (par:Status = 'RET' or par:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
      end ! if (wpr:Status = 'RET' or wpr:Status = 'RTS')
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (error# = 0)
      
      !if (p_web.GSV('Hide:DeleteButton') = 0)
      !    if (par:Part_Number = 'EXCH')
      !        ! Can't delete exchange button
      !        p_web.SSV('Hide:DeleteButton',1)
      !    end ! if (wpr:Part_Number = 'EXCH')
      !end ! if (error# = 0)
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          ! Job Complete, don't let delete.
          if (p_web.GSV('job:Date_Completed') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Date_Completed') > 0)
      
          if (p_web.GSV('isJobInvoiced') = 1)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      end ! if (error# = 0)
      
      ! DBH #10544 - Liquid Damage. Don't amend parts
      IF (p_web.GSV('jobe:Booking48HourOption') = 4)
          p_web.SSV('Hide:DeleteButton',1)
      END ! IF (p_web.GSV('jobe:Booking48HourOption') = 4)
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:partStatus = 'Requested'
              packet = clip(packet) & '<td class="'&clip('GreenRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Picked'
              packet = clip(packet) & '<td class="'&clip('BlueRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'On Order'
              packet = clip(packet) & '<td class="'&clip('PurpleRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&clip('PinkRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&clip('OrangeRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:partStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Hide:ChangeButton') <> 1 AND par:Part_Number <> 'EXCH') AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      End ! Field Condition
      If (p_web.GSV('Hide:DeleteButton') = 0) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::MyDelete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseChargeableParts.omv(this);" onMouseOut="BrowseChargeableParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseChargeableParts=new browseTable(''BrowseChargeableParts'','''&clip(loc:formname)&''','''&p_web._jsok('par:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('par:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormChargeableParts'');<13,10>'&|
      'BrowseChargeableParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseChargeableParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseChargeableParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseChargeableParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseChargeableParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseChargeableParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(PARTS)
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  Clear(par:Record)
  NetWebSetSessionPics(p_web,PARTS)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('par:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(PARTS)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('MyDelete')
    do Validate::MyDelete
  End
  p_web._CloseFile(PARTS)
! ----------------------------------------------------------------------------------------
value::par:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Part_Number_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::par:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Description_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::par:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Quantity_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Quantity,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:partStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:partStatus = 'Requested'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'GreenRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Picked'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'BlueRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'On Order'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'PurpleRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Picking'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'PinkRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Return'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'OrangeRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:partStatus,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND par:Part_Number <> 'EXCH')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Edit_'&par:Record_Number,,net:crc)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowseChargeableParts',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::MyDelete  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  par:Record_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(PARTS,par:recordnumberkey)
  p_web.FileToSessionQueue(PARTS)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::MyDelete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:DeleteButton') = 0)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('MyDelete_'&par:Record_Number,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','Delete','Delete','SmallButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormDeletePart?DelType=CHA')  & '&' & p_web._noColon('par:Record_Number')&'='& p_web.escape(par:Record_Number) & '&PressedButton=' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = par:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('par:Record_Number',par:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('par:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('par:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('par:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseEngineerHistory PROCEDURE  (NetWebServerWorker p_web)
joeEngineersName     STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBSENG)
                      Project(joe:RecordNumber)
                      Project(joe:DateAllocated)
                      Project(joe:AllocatedBy)
                      Project(joe:EngSkillLevel)
                      Project(joe:JobSkillLevel)
                      Project(joe:Status)
                      Project(joe:StatusDate)
                      Project(joe:StatusTime)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseEngineerHistory')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseEngineerHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseEngineerHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseEngineerHistory:FormName')
    else
      loc:FormName = 'BrowseEngineerHistory_frm'
    End
    p_web.SSV('BrowseEngineerHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseEngineerHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseEngineerHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseEngineerHistory:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseEngineerHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseEngineerHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSENG,joe:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOEENGINEERSNAME') then p_web.SetValue('BrowseEngineerHistory_sort','8')
    ElsIf (loc:vorder = 'JOEENGINEERSNAME') then p_web.SetValue('BrowseEngineerHistory_sort','8')
    ElsIf (loc:vorder = 'JOE:DATEALLOCATED') then p_web.SetValue('BrowseEngineerHistory_sort','1')
    ElsIf (loc:vorder = 'JOE:ALLOCATEDBY') then p_web.SetValue('BrowseEngineerHistory_sort','2')
    ElsIf (loc:vorder = 'JOE:ENGSKILLLEVEL') then p_web.SetValue('BrowseEngineerHistory_sort','3')
    ElsIf (loc:vorder = 'JOE:JOBSKILLLEVEL') then p_web.SetValue('BrowseEngineerHistory_sort','4')
    ElsIf (loc:vorder = 'JOE:STATUS') then p_web.SetValue('BrowseEngineerHistory_sort','5')
    ElsIf (loc:vorder = 'JOE:STATUSDATE') then p_web.SetValue('BrowseEngineerHistory_sort','6')
    ElsIf (loc:vorder = 'JOE:STATUSTIME') then p_web.SetValue('BrowseEngineerHistory_sort','7')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseEngineerHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseEngineerHistory:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseEngineerHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseEngineerHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseEngineerHistory:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Both
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.closeButton.image = '/images/listback.png'
  p_web.site.closeButton.textValue = 'Back'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseEngineerHistory_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 8
  End
  p_web.SetSessionValue('BrowseEngineerHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'joeEngineersName','joeEngineersName')
    Loc:LocateField = 'joeEngineersName'
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'joe:DateAllocated','-joe:DateAllocated')
    Loc:LocateField = 'joe:DateAllocated'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:AllocatedBy)','-UPPER(joe:AllocatedBy)')
    Loc:LocateField = 'joe:AllocatedBy'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'joe:EngSkillLevel','-joe:EngSkillLevel')
    Loc:LocateField = 'joe:EngSkillLevel'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:JobSkillLevel)','-UPPER(joe:JobSkillLevel)')
    Loc:LocateField = 'joe:JobSkillLevel'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:Status)','-UPPER(joe:Status)')
    Loc:LocateField = 'joe:Status'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'joe:StatusDate','-joe:StatusDate')
    Loc:LocateField = 'joe:StatusDate'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'joe:StatusTime','-joe:StatusTime')
    Loc:LocateField = 'joe:StatusTime'
  end
  if loc:vorder = ''
    loc:vorder = '-joe:RecordNumber'
  end
  If False ! add range fields to sort order
  Else
    If Instring('JOE:JOBNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'joe:JobNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joeEngineersName')
    loc:SortHeader = p_web.Translate('Engineer Name')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s60')
  Of upper('joe:DateAllocated')
    loc:SortHeader = p_web.Translate('Allocated')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@d6')
  Of upper('joe:AllocatedBy')
    loc:SortHeader = p_web.Translate('Allocated By')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s3')
  Of upper('joe:EngSkillLevel')
    loc:SortHeader = p_web.Translate('Eng Skill Level')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s8')
  Of upper('joe:JobSkillLevel')
    loc:SortHeader = p_web.Translate('Job Skill Level')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s30')
  Of upper('joe:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s30')
  Of upper('joe:StatusDate')
    loc:SortHeader = p_web.Translate('Status Date')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@d6')
  Of upper('joe:StatusTime')
    loc:SortHeader = p_web.Translate('Status Time')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@t1b')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseEngineerHistory:LookupFrom')
  End!Else
  loc:formaction = 'BrowseEngineerHistory'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineerHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineerHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseEngineerHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSENG"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joe:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineerHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseEngineerHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseEngineerHistory.locate(''Locator2BrowseEngineerHistory'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineerHistory.cl(''BrowseEngineerHistory'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseEngineerHistory_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseEngineerHistory_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'8','BrowseEngineerHistory','Engineer Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Engineer Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseEngineerHistory','Allocated','Click here to sort by Date Allocated',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Allocated')&'">'&p_web.Translate('Allocated')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseEngineerHistory','Allocated By','Click here to sort by Allocated By',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Allocated By')&'">'&p_web.Translate('Allocated By')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseEngineerHistory','Eng Skill Level','Click here to sort by Engineer Skill Level',,'CenterJustify',,1)
        Else
          packet = clip(packet) & '<th class="CenterJustify" Title="'&p_web.Translate('Click here to sort by Engineer Skill Level')&'">'&p_web.Translate('Eng Skill Level')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseEngineerHistory','Job Skill Level','Click here to sort by Job Skill Level',,'CenterJustify',,1)
        Else
          packet = clip(packet) & '<th class="CenterJustify" Title="'&p_web.Translate('Click here to sort by Job Skill Level')&'">'&p_web.Translate('Job Skill Level')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseEngineerHistory','Status','Click here to sort by Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseEngineerHistory','Status Date','Click here to sort by Status Date',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status Date')&'">'&p_web.Translate('Status Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseEngineerHistory','Status Time','Click here to sort by Status Time',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status Time')&'">'&p_web.Translate('Status Time')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'joe:DateAllocated' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'joe:StatusDate' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('joe:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBSENG{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'joe:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joe:RecordNumber'),p_web.GetValue('joe:RecordNumber'),p_web.GetSessionValue('joe:RecordNumber'))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'joe:JobNumber = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineerHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseEngineerHistory_Filter')
    p_web.SetSessionValue('BrowseEngineerHistory_FirstValue','')
    p_web.SetSessionValue('BrowseEngineerHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSENG,joe:RecordNumberKey,loc:PageRows,'BrowseEngineerHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSENG{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSENG,loc:firstvalue)
              Reset(ThisView,JOBSENG)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSENG{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSENG,loc:lastvalue)
            Reset(ThisView,JOBSENG)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joe:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineerHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineerHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineerHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineerHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineerHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseEngineerHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseEngineerHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseEngineerHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseEngineerHistory.locate(''Locator1BrowseEngineerHistory'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineerHistory.cl(''BrowseEngineerHistory'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseEngineerHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseEngineerHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineerHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineerHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineerHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineerHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code    = joe:UserCode
    if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Found
        joeEngineersName = clip(use:Forename) & ' ' & clip(use:Surname)
    else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Error
        joeEngineersName = ''
    end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    loc:field = joe:RecordNumber
    p_web._thisrow = p_web._nocolon('joe:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseEngineerHistory:LookupField')) = joe:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((joe:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseEngineerHistory.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSENG{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSENG)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSENG{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSENG)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joe:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseEngineerHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joe:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseEngineerHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joeEngineersName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:DateAllocated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:AllocatedBy
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:EngSkillLevel
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:JobSkillLevel
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:StatusDate
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:StatusTime
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseEngineerHistory.omv(this);" onMouseOut="BrowseEngineerHistory.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseEngineerHistory=new browseTable(''BrowseEngineerHistory'','''&clip(loc:formname)&''','''&p_web._jsok('joe:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('joe:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseEngineerHistory.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseEngineerHistory.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineerHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineerHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineerHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineerHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSENG)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSENG)
  Bind(joe:Record)
  Clear(joe:Record)
  NetWebSetSessionPics(p_web,JOBSENG)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('joe:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::joeEngineersName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joeEngineersName_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joeEngineersName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:DateAllocated   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:DateAllocated_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:DateAllocated,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:AllocatedBy   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:AllocatedBy_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:AllocatedBy,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:EngSkillLevel   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:EngSkillLevel_'&joe:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:EngSkillLevel,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:JobSkillLevel   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:JobSkillLevel_'&joe:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:JobSkillLevel,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:Status_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:StatusDate   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:StatusDate_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:StatusDate,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:StatusTime   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:StatusTime_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:StatusTime,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = joe:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('joe:RecordNumber',joe:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('joe:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joe:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joe:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseLocationHistory PROCEDURE  (NetWebServerWorker p_web)
locUserName          STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(LOCATLOG)
                      Project(lot:RecordNumber)
                      Project(lot:TheDate)
                      Project(lot:TheTime)
                      Project(lot:PreviousLocation)
                      Project(lot:NewLocation)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseLocationHistory')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseLocationHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseLocationHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseLocationHistory:FormName')
    else
      loc:FormName = 'BrowseLocationHistory_frm'
    End
    p_web.SSV('BrowseLocationHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseLocationHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseLocationHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseLocationHistory:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseLocationHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseLocationHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(LOCATLOG,lot:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOT:THEDATE') then p_web.SetValue('BrowseLocationHistory_sort','1')
    ElsIf (loc:vorder = 'LOT:THETIME') then p_web.SetValue('BrowseLocationHistory_sort','2')
    ElsIf (loc:vorder = 'LOCUSERNAME') then p_web.SetValue('BrowseLocationHistory_sort','3')
    ElsIf (loc:vorder = 'LOT:PREVIOUSLOCATION') then p_web.SetValue('BrowseLocationHistory_sort','4')
    ElsIf (loc:vorder = 'LOT:NEWLOCATION') then p_web.SetValue('BrowseLocationHistory_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseLocationHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseLocationHistory:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseLocationHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseLocationHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseLocationHistory:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseLocationHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseLocationHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'lot:TheDate','-lot:TheDate')
    Loc:LocateField = 'lot:TheDate'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'lot:TheTime','-lot:TheTime')
    Loc:LocateField = 'lot:TheTime'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'locUserName','-locUserName')
    Loc:LocateField = 'locUserName'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(lot:PreviousLocation)','-UPPER(lot:PreviousLocation)')
    Loc:LocateField = 'lot:PreviousLocation'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(lot:NewLocation)','-UPPER(lot:NewLocation)')
    Loc:LocateField = 'lot:NewLocation'
  end
  if loc:vorder = ''
    loc:vorder = '-lot:RecordNumber'
  end
  If False ! add range fields to sort order
  Else
    If Instring('LOT:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'lot:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('lot:TheDate')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@d6')
  Of upper('lot:TheTime')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@t1b')
  Of upper('locUserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@s60')
  Of upper('lot:PreviousLocation')
    loc:SortHeader = p_web.Translate('Previous Location')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@s30')
  Of upper('lot:NewLocation')
    loc:SortHeader = p_web.Translate('New Location')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseLocationHistory:LookupFrom')
  End!Else
  loc:formaction = 'BrowseLocationHistory'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseLocationHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseLocationHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseLocationHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="LOCATLOG"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="lot:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLocationHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseLocationHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseLocationHistory.locate(''Locator2BrowseLocationHistory'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLocationHistory.cl(''BrowseLocationHistory'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseLocationHistory_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseLocationHistory_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseLocationHistory','Date','Click here to sort by Date',,'RightJustify',60,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(60)&'" Title="'&p_web.Translate('Click here to sort by Date')&'">'&p_web.Translate('Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseLocationHistory','Time','Click here to sort by Time',,'RightJustify',60,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(60)&'" Title="'&p_web.Translate('Click here to sort by Time')&'">'&p_web.Translate('Time')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseLocationHistory','Username','Username',,,120,1)
        Else
          packet = clip(packet) & '<th width="'&clip(120)&'" Title="'&p_web.Translate('Username')&'">'&p_web.Translate('Username')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseLocationHistory','Previous Location','Click here to sort by Previous Location',,,150,1)
        Else
          packet = clip(packet) & '<th width="'&clip(150)&'" Title="'&p_web.Translate('Click here to sort by Previous Location')&'">'&p_web.Translate('Previous Location')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseLocationHistory','New Location','Click here to sort by New Location',,,150,1)
        Else
          packet = clip(packet) & '<th width="'&clip(150)&'" Title="'&p_web.Translate('Click here to sort by New Location')&'">'&p_web.Translate('New Location')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'lot:TheDate' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('lot:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and LOCATLOG{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'lot:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('lot:RecordNumber'),p_web.GetValue('lot:RecordNumber'),p_web.GetSessionValue('lot:RecordNumber'))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'lot:RefNumber = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLocationHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseLocationHistory_Filter')
    p_web.SetSessionValue('BrowseLocationHistory_FirstValue','')
    p_web.SetSessionValue('BrowseLocationHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,LOCATLOG,lot:RecordNumberKey,loc:PageRows,'BrowseLocationHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If LOCATLOG{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(LOCATLOG,loc:firstvalue)
              Reset(ThisView,LOCATLOG)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If LOCATLOG{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(LOCATLOG,loc:lastvalue)
            Reset(ThisView,LOCATLOG)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(lot:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLocationHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLocationHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLocationHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLocationHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLocationHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseLocationHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseLocationHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseLocationHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseLocationHistory.locate(''Locator1BrowseLocationHistory'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLocationHistory.cl(''BrowseLocationHistory'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseLocationHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseLocationHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLocationHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLocationHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLocationHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLocationHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code    = lot:UserCode
    if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Found
        locUserName = clip(use:Forename) & ' ' & clip(use:Surname)
    else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Error
        locUserName = ''
    end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    loc:field = lot:RecordNumber
    p_web._thisrow = p_web._nocolon('lot:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseLocationHistory:LookupField')) = lot:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((lot:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseLocationHistory.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If LOCATLOG{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(LOCATLOG)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If LOCATLOG{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(LOCATLOG)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','lot:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseLocationHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','lot:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseLocationHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(60)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::lot:TheDate
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(60)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::lot:TheTime
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(120)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locUserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(150)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::lot:PreviousLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(150)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::lot:NewLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseLocationHistory.omv(this);" onMouseOut="BrowseLocationHistory.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseLocationHistory=new browseTable(''BrowseLocationHistory'','''&clip(loc:formname)&''','''&p_web._jsok('lot:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('lot:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseLocationHistory.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseLocationHistory.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLocationHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLocationHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLocationHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLocationHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(LOCATLOG)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(LOCATLOG)
  Bind(lot:Record)
  Clear(lot:Record)
  NetWebSetSessionPics(p_web,LOCATLOG)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('lot:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::lot:TheDate   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('lot:TheDate_'&lot:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(lot:TheDate,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::lot:TheTime   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('lot:TheTime_'&lot:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(lot:TheTime,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locUserName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locUserName_'&lot:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locUserName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::lot:PreviousLocation   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('lot:PreviousLocation_'&lot:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(lot:PreviousLocation,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::lot:NewLocation   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('lot:NewLocation_'&lot:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(lot:NewLocation,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = lot:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('lot:RecordNumber',lot:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('lot:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('lot:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('lot:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseStatusChanges  PROCEDURE  (NetWebServerWorker p_web)
tmp:StatusTypeFilter STRING(3)                             !Status Type Filer
tmp:UserName         STRING(60)                            !User Name
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(AUDSTATS)
                      Project(aus:RecordNumber)
                      Project(aus:DateChanged)
                      Project(aus:TimeChanged)
                      Project(aus:OldStatus)
                      Project(aus:NewStatus)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseStatusChanges')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStatusChanges:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStatusChanges:NoForm')
      loc:FormName = p_web.GetValue('BrowseStatusChanges:FormName')
    else
      loc:FormName = 'BrowseStatusChanges_frm'
    End
    p_web.SSV('BrowseStatusChanges:NoForm',loc:NoForm)
    p_web.SSV('BrowseStatusChanges:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStatusChanges:NoForm')
    loc:FormName = p_web.GSV('BrowseStatusChanges:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStatusChanges') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStatusChanges')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(AUDSTATS,aus:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'AUS:DATECHANGED') then p_web.SetValue('BrowseStatusChanges_sort','1')
    ElsIf (loc:vorder = 'AUS:TIMECHANGED') then p_web.SetValue('BrowseStatusChanges_sort','2')
    ElsIf (loc:vorder = 'TMP:USERNAME') then p_web.SetValue('BrowseStatusChanges_sort','4')
    ElsIf (loc:vorder = 'AUS:OLDSTATUS') then p_web.SetValue('BrowseStatusChanges_sort','5')
    ElsIf (loc:vorder = 'AUS:NEWSTATUS') then p_web.SetValue('BrowseStatusChanges_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStatusChanges:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStatusChanges:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseStatusChanges:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStatusChanges:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStatusChanges:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStatusChanges_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStatusChanges_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'aus:DateChanged','-aus:DateChanged')
    Loc:LocateField = 'aus:DateChanged'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'aus:TimeChanged','-aus:TimeChanged')
    Loc:LocateField = 'aus:TimeChanged'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:UserName','-tmp:UserName')
    Loc:LocateField = 'tmp:UserName'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aus:OldStatus)','-UPPER(aus:OldStatus)')
    Loc:LocateField = 'aus:OldStatus'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aus:NewStatus)','-UPPER(aus:NewStatus)')
    Loc:LocateField = 'aus:NewStatus'
  end
  if loc:vorder = ''
    loc:vorder = '-aus:RecordNumber'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
    If Instring('AUS:TYPE',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'UPPER(aus:Type),' & loc:vorder
    End
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') = '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('aus:DateChanged')
    loc:SortHeader = p_web.Translate('Date Changed')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@d6')
  Of upper('aus:TimeChanged')
    loc:SortHeader = p_web.Translate('Time Changed')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@t1b')
  Of upper('tmp:UserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s60')
  Of upper('aus:OldStatus')
    loc:SortHeader = p_web.Translate('Old Status')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s30')
  Of upper('aus:NewStatus')
    loc:SortHeader = p_web.Translate('New Status')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseStatusChanges:LookupFrom')
  End!Else
  loc:formaction = 'BrowseStatusChanges'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStatusChanges:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStatusChanges:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStatusChanges:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="AUDSTATS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="aus:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseStatusChanges',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStatusChanges.locate(''Locator2BrowseStatusChanges'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStatusChanges.cl(''BrowseStatusChanges'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseStatusChanges_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseStatusChanges_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseStatusChanges','Date Changed','Click here to sort by Date Changed',,'RightJustify',100,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Date Changed')&'">'&p_web.Translate('Date Changed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseStatusChanges','Time Changed','Click here to sort by Time Changed',,'RightJustify',100,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Time Changed')&'">'&p_web.Translate('Time Changed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseStatusChanges','Username',,,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'">'&p_web.Translate('Username')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseStatusChanges','Old Status','Click here to sort by Old Status',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Old Status')&'">'&p_web.Translate('Old Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseStatusChanges','New Status','Click here to sort by New Status',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by New Status')&'">'&p_web.Translate('New Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'aus:DateChanged' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('aus:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and AUDSTATS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'aus:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('aus:RecordNumber'),p_web.GetValue('aus:RecordNumber'),p_web.GetSessionValue('aus:RecordNumber'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
      tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
      loc:FilterWas = 'aus:Type = ''' & clip(tmp:StatusTypeFilter) &''''
      If 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number') <> ''
        loc:FilterWas = clip(loc:FilterWas) & ' AND ' & 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number')
      End
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') = '')
      loc:FilterWas = 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number')
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStatusChanges_Filter')
    p_web.SetSessionValue('BrowseStatusChanges_FirstValue','')
    p_web.SetSessionValue('BrowseStatusChanges_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,AUDSTATS,aus:RecordNumberKey,loc:PageRows,'BrowseStatusChanges',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If AUDSTATS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(AUDSTATS,loc:firstvalue)
              Reset(ThisView,AUDSTATS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If AUDSTATS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(AUDSTATS,loc:lastvalue)
            Reset(ThisView,AUDSTATS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(aus:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStatusChanges.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStatusChanges.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStatusChanges.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStatusChanges.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStatusChanges_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStatusChanges_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseStatusChanges',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStatusChanges.locate(''Locator1BrowseStatusChanges'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStatusChanges.cl(''BrowseStatusChanges'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseStatusChanges_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStatusChanges_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStatusChanges.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStatusChanges.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStatusChanges.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStatusChanges.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = aus:UserCode
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        tmp:UserName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
    Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        tmp:UserName = '* Unknown User Code: ' & Clip(aus:UserCode) & ' *'
    End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    
    loc:field = aus:RecordNumber
    p_web._thisrow = p_web._nocolon('aus:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStatusChanges:LookupField')) = aus:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((aus:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseStatusChanges.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If AUDSTATS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(AUDSTATS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If AUDSTATS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(AUDSTATS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aus:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseStatusChanges.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aus:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseStatusChanges.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:DateChanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:TimeChanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:UserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:OldStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:NewStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseStatusChanges.omv(this);" onMouseOut="BrowseStatusChanges.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseStatusChanges=new browseTable(''BrowseStatusChanges'','''&clip(loc:formname)&''','''&p_web._jsok('aus:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('aus:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseStatusChanges.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseStatusChanges.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStatusChanges')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStatusChanges')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStatusChanges')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStatusChanges')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(AUDSTATS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(AUDSTATS)
  Bind(aus:Record)
  Clear(aus:Record)
  NetWebSetSessionPics(p_web,AUDSTATS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('aus:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::aus:DateChanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:DateChanged_'&aus:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:DateChanged,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:TimeChanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:TimeChanged_'&aus:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:TimeChanged,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:UserName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:UserName_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:UserName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:OldStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:OldStatus_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:OldStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:NewStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:NewStatus_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:NewStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = aus:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('aus:RecordNumber',aus:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('aus:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aus:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aus:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseStatusFilter   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:StatusTypeFilter STRING(3)                             !Status Type Filter
tmp:JOB              STRING('JOB')                         !
tmp:EXC              STRING('EXC')                         !
tmp:2NE              STRING('2NE')                         !
tmp:LOA              STRING('LOA')                         !
FilesOpened     Long
AUDSTATS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BrowseStatusFilter')
  loc:formname = 'BrowseStatusFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BrowseStatusFilter','None')
    p_web._DivHeader('BrowseStatusFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BrowseStatusFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BrowseStatusFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('tmp:StatusTypeFilter','JOB')
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  
  p_web.SetValue('BrowseStatusFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:StatusTypeFilter')
    tmp:StatusTypeFilter = p_web.GetValue('tmp:StatusTypeFilter')
    p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BrowseStatusFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BrowseStatusFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BrowseStatusFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('BrowseStatusFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BrowseStatusFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BrowseStatusFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BrowseStatusFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Status Changes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Status Changes',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BrowseStatusFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Filter') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BrowseStatusFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('BrowseStatusFilter_BrowseStatusChanges_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:StatusTypeFilter')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Filter') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StatusTypeFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browseStatusChanges
      do Comment::browseStatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Status Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StatusTypeFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('NewValue'))
    tmp:StatusTypeFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StatusTypeFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('Value'))
    tmp:StatusTypeFilter = p_web.GetValue('Value')
  End
  do Value::tmp:StatusTypeFilter
  do SendAlert
  do Value::browseStatusChanges  !1

Value::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:StatusTypeFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatusfilter_tmp:statustypefilter_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:StatusTypeFilter',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:StatusTypeFilter') = 0
    p_web.SetSessionValue('tmp:StatusTypeFilter','JOB')
  end
    packet = clip(packet) & p_web.CreateOption('Job','JOB',choose('JOB' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Exchange','EXC',choose('EXC' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Loan','LOA',choose('LOA' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('2nd Exchange','2NE',choose('2NE' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('All','',choose('' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value')

Comment::tmp:StatusTypeFilter  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browseStatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browseStatusChanges',p_web.GetValue('NewValue'))
    do Value::browseStatusChanges
  Else
    p_web.StoreValue('aus:RecordNumber')
  End

Value::browseStatusChanges  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseStatusChanges --
  p_web.SetValue('BrowseStatusChanges:NoForm',1)
  p_web.SetValue('BrowseStatusChanges:FormName',loc:formname)
  p_web.SetValue('BrowseStatusChanges:parentIs','Form')
  p_web.SetValue('_parentProc','BrowseStatusFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('BrowseStatusFilter_BrowseStatusChanges_embedded_div')&'"><!-- Net:BrowseStatusChanges --></div><13,10>'
    p_web._DivHeader('BrowseStatusFilter_' & lower('BrowseStatusChanges') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('BrowseStatusFilter_' & lower('BrowseStatusChanges') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseStatusChanges --><13,10>'
  end
  do SendPacket

Comment::browseStatusChanges  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('browseStatusChanges') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BrowseStatusFilter_tmp:StatusTypeFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StatusTypeFilter
      else
        do Value::tmp:StatusTypeFilter
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)

PreCopy  Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BrowseStatusFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BrowseStatusFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)
  p_web.StoreValue('tmp:StatusTypeFilter')
  p_web.StoreValue('')
BrowseWarrantyParts  PROCEDURE  (NetWebServerWorker p_web)
tmp:WARPARTStatus    STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(WARPARTS)
                      Project(wpr:Record_Number)
                      Project(wpr:Part_Number)
                      Project(wpr:Description)
                      Project(wpr:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
STOCK::State  USHORT
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseWarrantyParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseWarrantyParts:FormName')
    else
      loc:FormName = 'BrowseWarrantyParts_frm'
    End
    p_web.SSV('BrowseWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('BrowseWarrantyParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(WARPARTS,wpr:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'WPR:PART_NUMBER') then p_web.SetValue('BrowseWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'WPR:DESCRIPTION') then p_web.SetValue('BrowseWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'WPR:QUANTITY') then p_web.SetValue('BrowseWarrantyParts_sort','3')
    ElsIf (loc:vorder = 'TMP:WARPARTSTATUS') then p_web.SetValue('BrowseWarrantyParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseWarrantyParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseWarrantyParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.SSV('Hide:InsertButton',0)
  p_web.SSV('Hide:ChangeButton',0)
  p_web.SSV('Hide:DeleteButton',0)
  
  if (p_web.GSV('job:Date_Completed') > 0)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:DeleteButton',1)
  end ! if (p_web.GSV('job:Date_Completed') > 0)
  
  if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:DeleteButton',1)
  end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
  
  if (p_web.GSV('Hide:InsertButton') = 0)
      IF (p_web.GSV('locEngineeringOption') = 'Not Set')
          p_web.SSV('Hide:InsertButton',1)
      ELSE
          
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
  
  
              Access:LOCATION.Clearkey(loc:location_Key)
              loc:location    = use:Location
              if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Found
                  if (loc:Active = 0)
                      ! can add parts from an inactive location
                      p_web.SSV('Hide:InsertButton',1)
                  end !if (loc:Active = 0)
              else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Error
              end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:InsertButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      END
      
  end ! if (error# = 0)
  
  ! DBH #10544 - Liquid Damage. Don't amend parts
  IF (p_web.GSV('jobe:Booking48HourOption') = 4) OR (p_web.GSV('Job:ViewOnly') = 1)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:ChangeBUtton',1)
      p_web.SSV('Hide:DeleteButton',1)
  END ! IF (p_web.GSV('jobe:Booking48HourOption') = 4)
  p_web.site.SmallChangeButton.TextValue = p_web.Translate('Edit')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Part_Number)','-UPPER(wpr:Part_Number)')
    Loc:LocateField = 'wpr:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Description)','-UPPER(wpr:Description)')
    Loc:LocateField = 'wpr:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'wpr:Quantity','-wpr:Quantity')
    Loc:LocateField = 'wpr:Quantity'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:WARPARTStatus','-tmp:WARPARTStatus')
    Loc:LocateField = 'tmp:WARPARTStatus'
  of 5
    Loc:LocateField = ''
  of 7
    Loc:LocateField = ''
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('wpr:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s8')
  Of upper('tmp:WARPARTStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseWarrantyParts:LookupFrom')
  End!Else
    loc:formaction = 'FormWarrantyParts'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="WARPARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="wpr:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeadingAlt')&'">'&p_web.Translate('Warranty Parts',0)&'</span>'&CRLF
  End
  If clip('Warranty Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseWarrantyParts.locate(''Locator2BrowseWarrantyParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseWarrantyParts.cl(''BrowseWarrantyParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseWarrantyParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseWarrantyParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseWarrantyParts','Part Number',,,,100,1)
        Else
          packet = clip(packet) & '<th width="'&clip(100)&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseWarrantyParts','Description',,,,200,1)
        Else
          packet = clip(packet) & '<th width="'&clip(200)&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseWarrantyParts','Quantity',,,'CenterJustify',,1)
        Else
          packet = clip(packet) & '<th class="CenterJustify">'&p_web.Translate('Quantity')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseWarrantyParts','Status',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH') AND  true
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Edit
        do AddPacket
        loc:columns += 1
    End ! Selecting
  End ! Field condition
  If (p_web.GSV('Hide:DeleteButton') = 0) AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  MyDelete
        do AddPacket
        loc:columns += 1
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('wpr:record_number',lower(Thisview{prop:order}),1,1) = 0 !and WARPARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'wpr:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('wpr:Record_Number'),p_web.GetValue('wpr:Record_Number'),p_web.GetSessionValue('wpr:Record_Number'))
      loc:FilterWas = 'wpr:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseWarrantyParts_Filter')
    p_web.SetSessionValue('BrowseWarrantyParts_FirstValue','')
    p_web.SetSessionValue('BrowseWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,WARPARTS,wpr:RecordNumberKey,loc:PageRows,'BrowseWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If WARPARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(WARPARTS,loc:firstvalue)
              Reset(ThisView,WARPARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If WARPARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(WARPARTS,loc:lastvalue)
            Reset(ThisView,WARPARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:WARPARTStatus = getPartStatus('W')!Work out the colour
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Warranty Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 0
      if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseWarrantyParts')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseWarrantyParts.locate(''Locator1BrowseWarrantyParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseWarrantyParts.cl(''BrowseWarrantyParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 0
    if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseWarrantyParts')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  End
      IF (p_web.GSV('Hide:InsertButton') = 0)
          packet = clip(packet) & p_web.br
          Packet = clip(Packet) & |
              p_web.CreateButton('button','Adjustment','Adjustment','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyParts?&Insert_btn=Insert&adjustment=1')) & ''','''&clip('_self')&''')',,0,,,,,)
          packet = clip(packet) & p_web.br
          Do SendPacket
      END
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = wpr:Record_Number
    p_web._thisrow = p_web._nocolon('wpr:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseWarrantyParts:LookupField')) = wpr:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((wpr:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseWarrantyParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If WARPARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(WARPARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If WARPARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(WARPARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wpr:Record_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseWarrantyParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wpr:Record_Number',clip(loc:field),,'checked',,,'onclick="BrowseWarrantyParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
      !Show hide Buttons
      error# = 0
      if (wpr:Status = 'RET' or wpr:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
      end ! if (wpr:Status = 'RET' or wpr:Status = 'RTS')
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (error# = 0)
      
      !if (p_web.GSV('Hide:DeleteButton') = 0)
      !    if (wpr:Part_Number = 'EXCH')
      !        ! Can't delete exchange button
      !        p_web.SSV('Hide:DeleteButton',1)
      !    end ! if (wpr:Part_Number = 'EXCH')
      !end ! if (error# = 0)
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          ! Job Complete, don't let delete.
          if (p_web.GSV('job:Date_Completed') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Date_Completed') > 0)
      
          if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      end ! if (error# = 0)
      
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif wpr:Correction = 1
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::wpr:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:WARPARTStatus = 'Requested'
              packet = clip(packet) & '<td class="'&clip('GreenRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:WARPARTStatus = 'Picked'
              packet = clip(packet) & '<td class="'&clip('BlueRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:WARPARTStatus = 'On Order'
              packet = clip(packet) & '<td class="'&clip('PurpleRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:WARPARTStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&clip('PinkRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:WARPARTStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&clip('OrangeRegular')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:WARPARTStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH') AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      End ! Field Condition
      If (p_web.GSV('Hide:DeleteButton') = 0) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::MyDelete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseWarrantyParts.omv(this);" onMouseOut="BrowseWarrantyParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseWarrantyParts=new browseTable(''BrowseWarrantyParts'','''&clip(loc:formname)&''','''&p_web._jsok('wpr:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('wpr:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormWarrantyParts'');<13,10>'&|
      'BrowseWarrantyParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseWarrantyParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(WARPARTS)
  p_web._CloseFile(STOCK)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  Clear(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('wpr:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(WARPARTS)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('MyDelete')
    do Validate::MyDelete
  End
  p_web._CloseFile(WARPARTS)
! ----------------------------------------------------------------------------------------
value::wpr:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Part_Number_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Description_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif wpr:Correction = 1
      packet = clip(packet) & p_web._DivHeader('wpr:Quantity_'&wpr:Record_Number,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('COR',0))
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Quantity_'&wpr:Record_Number,'CenterJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Quantity,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:WARPARTStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:WARPARTStatus = 'Requested'
      packet = clip(packet) & p_web._DivHeader('tmp:WARPARTStatus_'&wpr:Record_Number,'GreenRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:WARPARTStatus,0))
    elsif tmp:WARPARTStatus = 'Picked'
      packet = clip(packet) & p_web._DivHeader('tmp:WARPARTStatus_'&wpr:Record_Number,'BlueRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:WARPARTStatus,0))
    elsif tmp:WARPARTStatus = 'On Order'
      packet = clip(packet) & p_web._DivHeader('tmp:WARPARTStatus_'&wpr:Record_Number,'PurpleRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:WARPARTStatus,0))
    elsif tmp:WARPARTStatus = 'Awaiting Picking'
      packet = clip(packet) & p_web._DivHeader('tmp:WARPARTStatus_'&wpr:Record_Number,'PinkRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:WARPARTStatus,0))
    elsif tmp:WARPARTStatus = 'Awaiting Return'
      packet = clip(packet) & p_web._DivHeader('tmp:WARPARTStatus_'&wpr:Record_Number,'OrangeRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:WARPARTStatus,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:WARPARTStatus,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Edit_'&wpr:Record_Number,,net:crc)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowseWarrantyParts',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::MyDelete  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  wpr:Record_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(WARPARTS,wpr:RecordNumberKey)
  p_web.FileToSessionQueue(WARPARTS)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::MyDelete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:DeleteButton') = 0)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('MyDelete_'&wpr:Record_Number,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','Delete','Delete','SmallButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormDeletePart?DelType=WAR')  & '&' & p_web._noColon('wpr:Record_Number')&'='& p_web.escape(wpr:Record_Number) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = wpr:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('wpr:Record_Number',wpr:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('wpr:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wpr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wpr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
AccountActivate48Hour PROCEDURE  (fAccountNumber)          ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    return# = 0
    do openFiles

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = fAccountNumber
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        if (tra:Activate48Hour)
            return# = 1
        end ! if (tra:Activate48Hour)
    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    do closeFiles

    return return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     FilesOpened = False
  END
FormEngineeringOption PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locUserPassword      STRING(30)                            !
locEngineeringOption STRING(30)                            !
locExchangeManufacturer STRING(30)                         !
locExchangeModelNumber STRING(30)                          !
locExchangeNotes     STRING(255)                           !
locNewEngineeringOption STRING(20)                         !
locSplitJob          BYTE                                  !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
MODELNUM::State  USHORT
JOBS::State  USHORT
MANUFACT::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
STOMODEL::State  USHORT
AUDIT::State  USHORT
STOCKALL::State  USHORT
JOBSE::State  USHORT
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
WEBJOB::State  USHORT
EXCHOR48::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEngineeringOption')
  loc:formname = 'FormEngineeringOption_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEngineeringOption','Change')
    p_web._DivHeader('FormEngineeringOption',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
add:ExchangeOrderPart         Routine
data
local:foundUnit             Byte(0)
code
    !Check to see if a part already exists

    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            local:FoundUnit = True
            If local:FoundUnit
                wpr:Status  = 'ORD'
                Access:WARPARTS.Update()

                Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                stl:PartType    = 'WAR'
                stl:PartRecordNumber    = wpr:Record_Number
                if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Found
                    relate:STOCKALL.delete(0)
                else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Error
                end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                Break
            End !If local:FoundUnit
        End !Loop

    End !If job:Warranty_Job = 'YES'

    If local:FoundUnit = True
        If p_web.GSV('ob:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                local:FoundUnit = True
                If local:FoundUnit
                    par:Status  = 'ORD'
                    Access:PARTS.Update()
                    Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                    stl:PartType    = 'CHA'
                    stl:PartRecordNumber    = par:Record_Number
                    if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Found
                        relate:STOCKALL.delete(0)
                    else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    Break
                End !If local:FoundUnit
            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If FoundEXCHPart# = 0

    If local:FoundUnit = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If job:Engineer = ''

         !Found
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = 'ORD'
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = 0
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = 'ORD'
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = 0
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If FoundEXCHPart# = 0
create:ExchangeOrder        Routine
    if (Access:EXCHOR48.PrimeRecord() = Level:Benign)
        ex4:Location    = p_web.GSV('Default:SiteLocation')
        ex4:Manufacturer    = p_web.GSV('locExchangeManufacturer')
        ex4:ModelNumber    = p_web.GSV('locExchangeModelNumber')
        ex4:Received    = 0
        ex4:Notes    = p_web.GSV('locExchangeNotes')
        ex4:JobNumber    = p_web.GSV('wob:RefNumber')
                
        if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Inserted

            p_web.SSV('jobe:Engineer48HourOption',1)
            p_web.SSV('locEngineeringOption','48 Hour Exchange')

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','48 HOUR EXCHANGE ORDER CREATED')
            p_web.SSV('AddToAudit:Notes','MANUFACTURER: ' & p_web.GSV('locExchangeManufacturer') & |
                '<13,10>MODEL NUMBER: ' & p_web.GSV('locExchangeModelNumber'))
            AddToAudit(p_web)

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE')
            p_web.SSV('AddToAudit:Notes','')
            AddToAudit(p_web)


            p_web.SSV('GetStatus:StatusNumber',360)
            p_web.SSV('GetStatus:Type','EXC')

            GetStatus(p_web)

            p_web.SSV('GetStatus:StatusNumber',355)
            p_web.SSV('GetStatus:Type','JOB')

            GetStatus(p_web)

! #11939 Don't force split (even though original spec states you should) (Bryan: 01/03/2011)
!            !If warranty job, auto change to spilt chargeable - 3876 (DBH: 29-03-2004)
!            if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
!                p_web.SSV('job:Chargeable_Job','YES')
!                if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
!                else ! if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
!                end ! if p_web.GSV('BookingSite') = 'RRC'
!                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
!            end ! if ( p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
            
            CASE (p_web.GSV('locSplitJob'))
            OF 1 ! Chargeable Only
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','NO')
            OF 2 ! Warranty Only
                p_web.SSV('job:Chargeable_Job','NO')
                p_web.SSV('job:Warranty_Job','YES')
            OF 3 ! Split
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','YES')
            END

            IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
                else ! if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                end ! if p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
            END

            do add:ExchangeOrderPart

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber    = p_web.GSV('wob:RefNumber')
            if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.TryUpdate()
            else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Error
            end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = p_web.GSV('wob:RefNumber')
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Found
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Error
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Error
            Access:EXCHOR48.CancelAutoInc()
        end ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
    end ! if (Access:EXCHOR48.PrimeRecord() = Level:Benign)

    p_web.SSV('exchangeOrderCreated',1)
    p_web.SSV('ExchangeOrder:RecordNumber',ex4:RecordNumber)
    p_web.SSV('locWarningMessage','Exchange Order Created')

set:HubRepair      routine
    p_web.SSV('jobe:HubRepairDate',Today())
    p_web.SSV('jobe:HubRepairTime',Clock())

    p_web.SSV('GetStatus:StatusNumber',sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    p_web.SSV('GetStatus:Type','JOB')
    GetStatus(p_web)

    if (p_web.GSV('BookingSite') = 'RRC')
        if (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
            rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
            set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
            loop
                if (Access:REPTYDEF.Next())
                    Break
                end ! if (Access:REPTYDEF.Next())
                if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                    Break
                end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                if (rtd:BER = 11)
                    if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                        p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                        p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    break
                end ! if (rtd:BER = 11)
            end ! loop
        end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
    end ! if (p_web.GSV('BookingSite') = 'RRC')


    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber    = p_web.GSV('job:Ref_Number')
    joe:UserCode    = p_web.GSV('job:Engineer')
    joe:DateAllocated    = Today()
    set(joe:UserCodeKey,joe:UserCodeKey)
    loop
        if (Access:JOBSENG.Previous())
            Break
        end ! if (Access:JOBSENG.Next())
        if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
        if (joe:UserCode    <> p_web.GSV('job:Engineer'))
            Break
        end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
        if (joe:DateAllocated    > Today())
            Break
        end ! if (joe:DateAllocated    <> Today())
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        access:JOBSENG.update()
        break
    end ! loop


    if (p_web.GSV('jobe2:SMSNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
    if (p_web.GSV('jobe2:EmailNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
Validate:locEngineeringOption           Routine
    p_web.SSV('locErrorMessage','')
    case p_web.GSV('locNewEngineeringOption')
    of 'Standard Repair'
        if (p_web.GSV('BookingSite') = 'RRC')
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Found
                if (mod:ExcludedRRCRepair)
                    p_web.SSV('locErrorMessage','Warning! This model should not be repaired by an RRC and should be sent to the Hub.')
                    if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
                        p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
                        p_web.SSV('locValidationFailed',1)
                        p_web.SSV('locNewEngineeringOption','')
                        Exit
                    end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))

                end ! if (mod:ExcludedRRCRepair)
            else ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('BookingSite') = 'RRC')
    of '48 Hour Exchange'
        if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange has already been requested for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange Unit has already been requested and despatched for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
            ! #11939 Job is warranty only, show the "split job" option. (Bryan: 01/03/2011)
            p_web.SSV('locSplitJob',0)
            p_web.SSV('Hide:SplitJob',0)
        ELSE
            ! #11980 Job is chargeable so no need to show split option. (Bryan: 01/03/2011)
            IF (p_web.GSV('job:Warranty_Job') = 'YES')
                p_web.SSV('locSplitJob',3)
            ELSE
                p_web.SSV('locSplitJob',1)
            END
        END
        
    of 'ARC Repair'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    of '7 Day TAT'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    end ! case p_web.GSV('locEngineeringOption')
Validate:locExchangeModelNumber           Routine
    if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! The selected unit is not an "alternative" Model Number for this job!')                        
        else ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! You have selected a different Model Number for this job!')
        end ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
    else ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locWarningMessage','')
    end ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))

OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(EXCHOR48)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(EXCHOR48)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !Init Form
  if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('locValidationFailed',0)
      p_web.SSV('locUserPassword','')
      p_web.SSV('locErrorMessage','')
      p_web.SSV('locWarningMessage','')
      p_web.SSV('Comment:UserPassword','Enter User Password and press [TAB]')
      p_web.SSV('filter:Manufacturer','')
      p_web.SSV('locNewEngineeringOption','')
      p_web.SSV('locExchangeManufacturer',p_web.GSV('job:Manufacturer'))
      p_web.SSV('locExchangeModelNumber',p_web.GSV('job:Model_number'))
      p_web.SSV('locExchangeNotes','')
      p_web.SSV('locSplitJob',0)
      p_web.SSV('Hide:SplitJob',1)
  
      p_web.SSV('exchangeOrderCreated',0)
  
      p_web.SSV('FormEngineeringOption:FirstTime',0)
  
      ! Activate the 48 Hour option and show the booking option (DBH: 24-03-2005)
      p_web.SSV('hide:48HourOption',0)
      If AccountActivate48Hour(p_web.GSV('wob:HeadAccountNumber')) = True
          If Allow48Hour(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),p_web.GSV('wob:HeadAccountNumber')) = True
  
          Else ! If Allow48Hour(job:ESN,job:Model_Number) = True
              p_web.SSV('hide:48HourOption',1)
          End ! If Allow48Hour(job:ESN,job:Model_Number) = True
      Else ! AccountActivate48Hour(wob:HeadAccountNumber) = True
          p_web.SSV('hide:48HourOption',1)
      End ! AccountActivate48Hour(wob:HeadAccountNumber) = True
  
  end ! if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
  p_web.SetValue('FormEngineeringOption_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:DOP',p_web.site.DatePicture)
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locExchangeManufacturer'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      p_web.SSV('filter:ModelNumber','Upper(mod:Manufacturer) = Upper(<39>' & clip(man:Manufacturer) & '<39>)')
      
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
  Of 'locExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
      do Validate:locExchangeModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locUserPassword',locUserPassword)
  p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  p_web.SetSessionValue('job:DOP',job:DOP)
  p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  p_web.SetSessionValue('locSplitJob',locSplitJob)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locUserPassword')
    locUserPassword = p_web.GetValue('locUserPassword')
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  End
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  End
  if p_web.IfExistsValue('locNewEngineeringOption')
    locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:DOP',job:DOP)
  End
  if p_web.IfExistsValue('locExchangeManufacturer')
    locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  End
  if p_web.IfExistsValue('locExchangeModelNumber')
    locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  End
  if p_web.IfExistsValue('locExchangeNotes')
    locExchangeNotes = p_web.GetValue('locExchangeNotes')
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  End
  if p_web.IfExistsValue('locSplitJob')
    locSplitJob = p_web.GetValue('locSplitJob')
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormEngineeringOption_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locUserPassword = p_web.RestoreValue('locUserPassword')
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
 locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
 locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
 locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
 locSplitJob = p_web.RestoreValue('locSplitJob')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEngineeringOption_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEngineeringOption_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEngineeringOption_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormEngineeringOption" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormEngineeringOption" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormEngineeringOption" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Change Engineering Option') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Change Engineering Option',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormEngineeringOption">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormEngineeringOption" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormEngineeringOption')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Engineering Option') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineeringOption')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormEngineeringOption'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locUserPassword')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineeringOption')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm User') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUserPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Engineering Option') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorMessage
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:ExchangeOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:ExchangeOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarningMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarningMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSplitJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSplitJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSplitJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:PrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:PrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locUserPassword  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Enter Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUserPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUserPassword',p_web.GetValue('NewValue'))
    locUserPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUserPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUserPassword',p_web.GetValue('Value'))
    locUserPassword = p_web.GetValue('Value')
  End
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('Comment:UserPassword','User Does Not Have Access')
  
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('locUserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'AMEND ENGINEERING OPTION'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              p_web.SSV('locPasswordValidated',1)
              p_web.SSV('Comment:UserPassword','')
              p_web.SSV('headingExchangeOrder','Exchange Order')
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locUserPassword
  do SendAlert
  do Prompt::locNewEngineeringOption
  do Value::locNewEngineeringOption  !1
  do Comment::locNewEngineeringOption
  do Comment::locUserPassword
  do Prompt::locEngineeringOption
  do Value::locEngineeringOption  !1
  do Comment::locEngineeringOption

Value::locUserPassword  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locUserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locUserPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locUserPassword'',''formengineeringoption_locuserpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locUserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locUserPassword',p_web.GetSessionValueFormat('locUserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value')

Comment::locUserPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment')

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate(p_web.GSV('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Option')
  If p_web.GSV('locPasswordValidated') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt')

Validate::locEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('NewValue'))
    locEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('Value'))
    locEngineeringOption = p_web.GetValue('Value')
  End

Value::locEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value')

Comment::locEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment')

Prompt::locNewEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Engineering Option')
  If p_web.GSV('locPasswordValidated') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt')

Validate::locNewEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewEngineeringOption',p_web.GetValue('NewValue'))
    locNewEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewEngineeringOption',p_web.GetValue('Value'))
    locNewEngineeringOption = p_web.GetValue('Value')
  End
  If locNewEngineeringOption = ''
    loc:Invalid = 'locNewEngineeringOption'
    loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do validate:locEngineeringOption
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<</DIV>',''))
  
  
  do Value::locNewEngineeringOption
  do SendAlert
  do Value::locErrorMessage  !1
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Prompt::locExchangeManufacturer
  do Value::locExchangeManufacturer  !1
  do Comment::locExchangeManufacturer
  do Prompt::locExchangeModelNumber
  do Value::locExchangeModelNumber  !1
  do Comment::locExchangeModelNumber
  do Prompt::locExchangeNotes
  do Value::locExchangeNotes  !1
  do Comment::locExchangeNotes
  do Value::text:ExchangeOrder  !1
  do Value::button:CreateOrder  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

Value::locNewEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewEngineeringOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewEngineeringOption = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewEngineeringOption'',''formengineeringoption_locnewengineeringoption_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewEngineeringOption')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNewEngineeringOption',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'Standard Repair'
    packet = clip(packet) & p_web.CreateOption('Standard Repair','Standard Repair',choose('Standard Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '48 Hour Exchange'  and p_web.GSV('hide:48HourOption') <> 1
    packet = clip(packet) & p_web.CreateOption('48 Hour Exchange','48 Hour Exchange',choose('48 Hour Exchange' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'ARC Repair'
    packet = clip(packet) & p_web.CreateOption('ARC Repair','ARC Repair',choose('ARC Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '7 Day TAT'
    packet = clip(packet) & p_web.CreateOption('7 Day TAT','7 Day TAT',choose('7 Day TAT' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value')

Comment::locNewEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment')

Validate::text:ExchangeOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:ExchangeOrder',p_web.GetValue('NewValue'))
    do Value::text:ExchangeOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:ExchangeOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Exchange Order',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value')

Comment::text:ExchangeOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:DOP  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Of Purchase')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt')

Validate::job:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture))
    job:DOP = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do Value::job:DOP
  do SendAlert

Value::job:DOP  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DATE --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:lookuponly = ''
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.site.datepicture,loc:javascript,,) & '<13,10>'
  if not loc:readonly
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' '&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:DateLookupButton,loc:formname,,,|
    'sd('''&clip(loc:formName)&''','''&p_web._nocolon('job:DOP')&''','''&clip(upper(p_web.site.datepicture))&''','&lower(p_web._nocolon('''FormEngineeringOption_job:DOP_value'''))&');','onfocus="rad();" ')

  End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value')

Comment::job:DOP  Routine
    loc:comment = p_web._DateFormat(p_web.site.DatePicture)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment')

Prompt::locExchangeManufacturer  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manufacturer')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt')

Validate::locExchangeManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeManufacturer',p_web.GetValue('NewValue'))
    locExchangeManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeManufacturer',p_web.GetValue('Value'))
    locExchangeManufacturer = p_web.GetValue('Value')
  End
  If locExchangeManufacturer = ''
    loc:Invalid = 'locExchangeManufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locExchangeManufacturer')
  do AfterLookup
  do Value::locExchangeManufacturer
  do SendAlert
  do Comment::locExchangeManufacturer

Value::locExchangeManufacturer  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('exchangeOrderCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locExchangeManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locExchangeManufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeManufacturer'',''formengineeringoption_locexchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeManufacturer',p_web.GetSessionValueFormat('locExchangeManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=locExchangeManufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=FormEngineeringOption&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value')

Comment::locExchangeManufacturer  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment')

Prompt::locExchangeModelNumber  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt')

Validate::locExchangeModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeModelNumber',p_web.GetValue('NewValue'))
    locExchangeModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeModelNumber',p_web.GetValue('Value'))
    locExchangeModelNumber = p_web.GetValue('Value')
  End
  If locExchangeModelNumber = ''
    loc:Invalid = 'locExchangeModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locExchangeModelNumber')
  do AfterLookup
  do Value::locExchangeModelNumber
  do SendAlert
  do Comment::locExchangeModelNumber
  do Value::locWarningMessage  !1

Value::locExchangeModelNumber  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('exchangeOrderCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locExchangeModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locExchangeModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeModelNumber'',''formengineeringoption_locexchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeModelNumber',p_web.GetSessionValueFormat('locExchangeModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=locExchangeModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort&LookupFrom=FormEngineeringOption&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value')

Comment::locExchangeModelNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment')

Prompt::locExchangeNotes  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Notes')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt')

Validate::locExchangeNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeNotes',p_web.GetValue('NewValue'))
    locExchangeNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeNotes',p_web.GetValue('Value'))
    locExchangeNotes = p_web.GetValue('Value')
  End
    locExchangeNotes = Upper(locExchangeNotes)
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  do Value::locExchangeNotes
  do SendAlert

Value::locExchangeNotes  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- TEXT --- locExchangeNotes
  loc:fieldclass = Choose(sub('TextEntry',1,1) = ' ',clip('FormEntry') & 'TextEntry','TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locExchangeNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeNotes'',''formengineeringoption_locexchangenotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeNotes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locExchangeNotes',p_web.GetSessionValue('locExchangeNotes'),3,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locExchangeNotes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value')

Comment::locExchangeNotes  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment')

Validate::locWarningMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarningMessage',p_web.GetValue('NewValue'))
    do Value::locWarningMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locWarningMessage  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locWarningMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value')

Comment::locWarningMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSplitJob  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Select Job Type')
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt')

Validate::locSplitJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSplitJob',p_web.GetValue('NewValue'))
    locSplitJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSplitJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSplitJob',p_web.GetValue('Value'))
    locSplitJob = p_web.GetValue('Value')
  End
  do Value::locSplitJob
  do SendAlert
  do Value::button:CreateOrder  !1

Value::locSplitJob  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
  ! --- RADIO --- locSplitJob
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSplitJob')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Warranty Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Split Warranty/Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value')

Comment::locSplitJob  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_comment',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:CreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:CreateOrder',p_web.GetValue('NewValue'))
    do Value::button:CreateOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('locExchangeManufacturer') <> '' and p_web.GSV('locExchangeModelNumber') <> '')
      do create:ExchangeOrder
  end ! if (p_web.GSV('locExchangeManufacturer') <> '' and |
  do Value::button:CreateOrder
  do SendAlert
  do Value::button:PrintOrder  !1
  do Value::locExchangeManufacturer  !1
  do Value::locExchangeModelNumber  !1
  do Value::locExchangeNotes  !1
  do Value::locWarningMessage  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

Value::button:CreateOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value',Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:CreateOrder'',''formengineeringoption_button:createorder_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateExchangeOrder','Create Exch. Order','DoubleButton',loc:formname,,,,loc:javascript,0,'images\star.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value')

Comment::button:CreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_comment',Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:PrintOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:PrintOrder',p_web.GetValue('NewValue'))
    do Value::button:PrintOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:PrintOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value',Choose(p_web.GSV('exchangeOrderCreated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintOrder','Print Order','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ExchangeOrder')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images\printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value')

Comment::button:PrintOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_comment',Choose(p_web.GSV('exchangeOrderCreated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('exchangeOrderCreated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormEngineeringOption_locUserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locUserPassword
      else
        do Value::locUserPassword
      end
  of lower('FormEngineeringOption_locNewEngineeringOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewEngineeringOption
      else
        do Value::locNewEngineeringOption
      end
  of lower('FormEngineeringOption_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormEngineeringOption_locExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeManufacturer
      else
        do Value::locExchangeManufacturer
      end
  of lower('FormEngineeringOption_locExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeModelNumber
      else
        do Value::locExchangeModelNumber
      end
  of lower('FormEngineeringOption_locExchangeNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeNotes
      else
        do Value::locExchangeNotes
      end
  of lower('FormEngineeringOption_locSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSplitJob
      else
        do Value::locSplitJob
      end
  of lower('FormEngineeringOption_button:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:CreateOrder
      else
        do Value::button:CreateOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)

PreCopy  Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
      ! Validate Form
      if (p_web.GSV('locPasswordValidated') = 0)
          loc:Invalid = 'locUserPassword'
          loc:Alert = 'Your password has not yet been validated'
          p_web.SSV('locUserPassword','')
          exit
      end ! if (p_web.GSV('local:PasswordValidated') = 0)
  
      if (p_web.GSV('locValidationFailed') = 1)
          loc:Invalid = 'locUserPassword'
          loc:Alert = p_web.GSV('locErrorMessage')
          exit
      end ! if (p_web.GSV('locValidationFailed') = 1)
  
  
  
      if ((p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption')) and p_web.GSV('locNewEngineeringOption') <> '')
          p_web.SSV('locEngineeringOption',p_web.GSV('locNewEngineeringOption'))
  
          case p_web.GSV('locNewEngineeringOption')
          of 'ARC Repair'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: ARC REPAIR')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',2)
  
              
          of '7 Day TAT'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 7 DAY TAT')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',3)
              
          of 'Standard Repair'
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: STANDARD REPAIR')
              p_web.SSV('AddToAudit:Notes','')
              AddToAudit(p_web)
              p_web.SSV('jobe:Engineer48HourOption',4)
          end !case p_web.GSV('locNewEngineeringOption')
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE)
              Access:JOBSE.tryUpdate()
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              Access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber = p_web.GSV('wob:RefNumber')
          if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              Access:WEBJOB.TryUpdate()
          end ! if (Access:WEBJOB.TryFetch(wob:Ref_Number_Key) = Level:Benign)
          
  
          p_web.SSV('FirstTime',1)
  
  !
  !        p_web.SSV('locMessage','Your changes have been saved')
  !        p_web.SSV('loc:Alert','Your changes have been saved')
  
      end ! if (p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption'))
  
  
  
      

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
      If not (p_web.GSV('locPasswordValidated') = 0)
        If locNewEngineeringOption = ''
          loc:Invalid = 'locNewEngineeringOption'
          loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If locExchangeManufacturer = ''
          loc:Invalid = 'locExchangeManufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If locExchangeModelNumber = ''
          loc:Invalid = 'locExchangeModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          locExchangeNotes = Upper(locExchangeNotes)
          p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.StoreValue('locUserPassword')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('locNewEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('locExchangeManufacturer')
  p_web.StoreValue('locExchangeModelNumber')
  p_web.StoreValue('locExchangeNotes')
  p_web.StoreValue('')
  p_web.StoreValue('locSplitJob')
  p_web.StoreValue('')
  p_web.StoreValue('')
