

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A022.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A018.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A026.INC'),ONCE        !Req'd for module callout resolution
                     END


PickContactHistoryNotes PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPickList          STRING(10000)                         !
locFinalList         STRING(10000)                         !
locFinalListField    STRING(10000)                         !
locFoundField        STRING(10000)                         !
FilesOpened     Long
CONTHIST::State  USHORT
NOTESCON::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('PickContactHistoryNotes')
  loc:formname = 'PickContactHistoryNotes_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickContactHistoryNotes','')
    p_web._DivHeader('PickContactHistoryNotes',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
clearVariables      ROUTINE
    p_web.DeleteSessionValue('locPickList')
    p_web.DeleteSessionValue('locFinalList')
    p_web.DeleteSessionValue('locFinalListField')
    p_web.DeleteSessionValue('locFoundField')
OpenFiles  ROUTINE
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(NOTESCON)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(NOTESCON)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickContactHistoryNotes_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do clearVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locPickList',locPickList)
  p_web.SetSessionValue('locFinalList',locFinalList)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPickList')
    locPickList = p_web.GetValue('locPickList')
    p_web.SetSessionValue('locPickList',locPickList)
  End
  if p_web.IfExistsValue('locFinalList')
    locFinalList = p_web.GetValue('locFinalList')
    p_web.SetSessionValue('locFinalList',locFinalList)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickContactHistoryNotes_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locPickList = p_web.RestoreValue('locPickList')
 locFinalList = p_web.RestoreValue('locFinalList')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormContactHistory'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickContactHistoryNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickContactHistoryNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickContactHistoryNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormContactHistory'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickContactHistoryNotes" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickContactHistoryNotes" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickContactHistoryNotes" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Contact History Notes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Contact History Notes',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickContactHistoryNotes">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickContactHistoryNotes" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickContactHistoryNotes')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickContactHistoryNotes')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickContactHistoryNotes'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPickList')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickContactHistoryNotes')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickContactHistoryNotes_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPickList
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPickList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFinalList
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFinalList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::button:AddSelected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AddSelected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::button:RemoveSelected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:RemoveSelected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::gap
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:RemoveAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::title  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('title',p_web.GetValue('NewValue'))
    do Value::title
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::title  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('title') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one entry.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locPickList  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locPickList') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPickList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPickList',p_web.GetValue('NewValue'))
    locPickList = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPickList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPickList',p_web.GetValue('Value'))
    locPickList = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::locFinalList  !1

Value::locPickList  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locPickList') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locPickList')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPickList'',''pickcontacthistorynotes_locpicklist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPickList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locPickList',loc:fieldclass,loc:readonly,30,420,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locPickList') = 0
    p_web.SetSessionValue('locPickList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Select Note -----','',choose('' = p_web.getsessionvalue('locPickList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      access:notescon.clearkey(noc:Notes_Key)
      noc:Reference = ''
      SET(noc:Notes_Key,noc:Notes_Key)
      LOOP
          IF (access:notescon.next())
              BREAK
          END
          If Instring(';' & Clip(noc:notes),p_web.GSV('locFinalListField'),1,1)
              Cycle
          End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('locFinalListField'),1,1)
  
          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
          packet = clip(packet) & p_web.CreateOption(CLIP(noc:Reference) & ' - ' & Clip(noc:Notes),noc:Notes,choose(noc:Notes = p_web.GSV('locPickList')),clip(loc:rowstyle),,)&CRLF
          loc:even = Choose(loc:even=1,2,1)
      END
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickContactHistoryNotes_' & p_web._nocolon('locPickList') & '_value')


Prompt::locFinalList  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locFinalList') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFinalList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFinalList',p_web.GetValue('NewValue'))
    locFinalList = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFinalList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFinalList',p_web.GetValue('Value'))
    locFinalList = p_web.GetValue('Value')
  End
  do Value::locFinalList
  do SendAlert
  do Value::locPickList  !1

Value::locFinalList  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locFinalList') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locFinalList')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFinalList'',''pickcontacthistorynotes_locfinallist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFinalList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locFinalList',loc:fieldclass,loc:readonly,30,350,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Selected Notes -----','',choose('' = p_web.getsessionvalue('locFinalList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('locFinalListField') <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                          packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                          loc:even = Choose(loc:even=1,2,1)
                      End ! If tmp:FoundAccessory <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                  packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                  loc:even = Choose(loc:even=1,2,1)
              End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('locFinalListField') <> ''
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickContactHistoryNotes_' & p_web._nocolon('locFinalList') & '_value')


Prompt::button:AddSelected  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:AddSelected') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::button:AddSelected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AddSelected',p_web.GetValue('NewValue'))
    do Value::button:AddSelected
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      p_web.SSV('locFinalListField',p_web.GSV('locFinalListField') & p_web.GSV('locPickList'))
      p_web.SSV('locPickList','')
  do Value::button:AddSelected
  do SendAlert
  do Value::locFinalList  !1
  do Value::locPickList  !1

Value::button:AddSelected  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:AddSelected') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AddSelected'',''pickcontacthistorynotes_button:addselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddSelected','Add Selected','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickContactHistoryNotes_' & p_web._nocolon('button:AddSelected') & '_value')


Prompt::button:RemoveSelected  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveSelected') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::button:RemoveSelected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:RemoveSelected',p_web.GetValue('NewValue'))
    do Value::button:RemoveSelected
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      locFinalListField = p_web.GSV('locFinalListField') & ';'
      locFinalList = '|;' & Clip(p_web.GSV('locFinalList')) & ';'
      Loop x# = 1 To 10000
          pos# = Instring(Clip(locFinalList),locFinalListField,1,1)
          If pos# > 0
              locFinalListField = Sub(locFinalListField,1,pos# - 1) & Sub(locFinalListField,pos# + Len(Clip(locFinalList)),10000)
              Break
          End ! If pos# > 0#
      End ! Loop x# = 1 To 1000
      p_web.SSV('locFinalListField',Sub(locFinalListField,1,Len(Clip(locFinalListField)) - 1))
      p_web.SSV('locFinalList','')
  do Value::button:RemoveSelected
  do SendAlert
  do Value::locFinalList  !1
  do Value::locPickList  !1

Value::button:RemoveSelected  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveSelected') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveSelected'',''pickcontacthistorynotes_button:removeselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','DeleteSelected','Delete Selected','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveSelected') & '_value')


Validate::gap  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('gap',p_web.GetValue('NewValue'))
    do Value::gap
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::gap
  do SendAlert

Value::gap  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('gap') & '_value',Choose(1,'hdiv','adiv'))
  loc:extra = ''
  If Not (1)
  ! --- STRING --- 
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''gap'',''pickcontacthistorynotes_gap_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','',p_web.GetSessionValueFormat(''),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickContactHistoryNotes_' & p_web._nocolon('gap') & '_value')


Validate::button:RemoveAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:RemoveAll',p_web.GetValue('NewValue'))
    do Value::button:RemoveAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      p_web.SetSessionValue('locFinalListField','')
      p_web.SetSessionValue('locFinalList','')
      p_web.SetSessionValue('locPickList','')
  do Value::button:RemoveAll
  do SendAlert
  do Value::locFinalList  !1
  do Value::locPickList  !1

Value::button:RemoveAll  Routine
  p_web._DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveAll'',''pickcontacthistorynotes_button:removeall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAll','Remove All','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveAll') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickContactHistoryNotes_locPickList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPickList
      else
        do Value::locPickList
      end
  of lower('PickContactHistoryNotes_locFinalList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFinalList
      else
        do Value::locFinalList
      end
  of lower('PickContactHistoryNotes_button:AddSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AddSelected
      else
        do Value::button:AddSelected
      end
  of lower('PickContactHistoryNotes_button:RemoveSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveSelected
      else
        do Value::button:RemoveSelected
      end
  of lower('PickContactHistoryNotes_gap_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::gap
      else
        do Value::gap
      end
  of lower('PickContactHistoryNotes_button:RemoveAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveAll
      else
        do Value::button:RemoveAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)

PreCopy  Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickContactHistoryNotes:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickContactHistoryNotes:Primed',0)
  p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickContactHistoryNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      If Clip(p_web.GSV('locFinalListField')) <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          p_web.SSV('cht:Notes',p_web.GSV('cht:Notes') & '<13,10>' & | 
                              CLIP(locFoundField))
                      End ! If locFoundField <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  p_web.SSV('cht:Notes',p_web.GSV('cht:Notes') & '<13,10>' & | 
                      CLIP(locFoundField))
              End ! If locFoundField <> ''
          End ! If Start# > 0
      End ! If Clip(p_web.GSV('locFinalListField') <> ''
  p_web.DeleteSessionValue('PickContactHistoryNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  do clearVariables
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickContactHistoryNotes:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locPickList')
  p_web.StoreValue('locFinalList')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SendSMS              PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('SendSMS')
  loc:formname = 'SendSMS_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SendSMS','')
    p_web._DivHeader('SendSMS',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SendSMS',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SendSMS',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('SendSMS_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('SendSMS_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BrowseContactHistory'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SendSMS_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SendSMS_ChainTo')
    loc:formaction = p_web.GetSessionValue('SendSMS_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseContactHistory'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="SendSMS" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="SendSMS" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="SendSMS" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Send SMS') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Send SMS',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_SendSMS">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_SendSMS" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_SendSMS')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_SendSMS')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_SendSMS'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_SendSMS')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SendSMS_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::messageText
      do Comment::messageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::messageText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('messageText',p_web.GetValue('NewValue'))
    do Value::messageText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::messageText  Routine
  p_web._DivHeader('SendSMS_' & p_web._nocolon('messageText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Are you sure you want to resend an SMS?',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::messageText  Routine
    loc:comment = ''
  p_web._DivHeader('SendSMS_' & p_web._nocolon('messageText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_SendSMS',0)

PreCopy  Routine
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SendSMS',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('SendSMS:Primed',0)

PreDelete       Routine
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('SendSMS:Primed',0)
  p_web.setsessionvalue('showtab_SendSMS',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SendSMS_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  !    AddEmailSMS(p_web.GSV('job:Ref_Number'), |
  !        p_web.GSV('wob:HeadAccountNumber') |
  !    ,'COMP','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
  
    ! Assumes we are in the jobs record
    SendSMSText('J','N','N',p_web)
  p_web.DeleteSessionValue('SendSMS_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('SendSMS:Primed',0)
  p_web.StoreValue('')
SendAnEmail          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('SendAnEmail')
  loc:formname = 'SendAnEmail_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SendAnEmail','')
    p_web._DivHeader('SendAnEmail',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SendAnEmail',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SendAnEmail',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      p_web.StoreValue('returnURL')
  p_web.SetValue('SendAnEmail_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('SendAnEmail_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('returnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SendAnEmail_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SendAnEmail_ChainTo')
    loc:formaction = p_web.GetSessionValue('SendAnEmail_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('returnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="SendAnEmail" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="SendAnEmail" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="SendAnEmail" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Send Email') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Send Email',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_SendAnEmail">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_SendAnEmail" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_SendAnEmail')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_SendAnEmail')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_SendAnEmail'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_SendAnEmail')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SendAnEmail_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::messageText
      do Comment::messageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::messageText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('messageText',p_web.GetValue('NewValue'))
    do Value::messageText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::messageText  Routine
  p_web._DivHeader('SendAnEmail_' & p_web._nocolon('messageText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Are you sure you want to resend an Email?',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::messageText  Routine
    loc:comment = ''
  p_web._DivHeader('SendAnEmail_' & p_web._nocolon('messageText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_SendAnEmail',0)

PreCopy  Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SendAnEmail',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('SendAnEmail:Primed',0)

PreDelete       Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('SendAnEmail:Primed',0)
  p_web.setsessionvalue('showtab_SendAnEmail',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SendAnEmail_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      AddEmailSMS(p_web.GSV('job:Ref_Number')|
      ,p_web.GSV('wob:HeadAccountNumber'),'COMP','EMAIL','', |
          p_web.GSV('jobe2:EmailAlertAddress'),0,'')
  p_web.DeleteSessionValue('SendAnEmail_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('SendAnEmail:Primed',0)
  p_web.StoreValue('')
CreateInvoice        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('CreateInvoice')
  loc:formname = 'CreateInvoice_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('CreateInvoice','')
    p_web._DivHeader('CreateInvoice',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateInvoice',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CreateInvoice',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      p_web.StoreValue('returnURL')
  p_web.SetValue('CreateInvoice_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CreateInvoice_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'InvoiceCreated'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CreateInvoice_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CreateInvoice_ChainTo')
    loc:formaction = p_web.GetSessionValue('CreateInvoice_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('returnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CreateInvoice" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CreateInvoice" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CreateInvoice" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Create Invoice') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Create Invoice',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CreateInvoice">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CreateInvoice" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CreateInvoice')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CreateInvoice')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CreateInvoice'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CreateInvoice')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateInvoice_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::messageText
      do Comment::messageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::messageText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('messageText',p_web.GetValue('NewValue'))
    do Value::messageText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::messageText  Routine
  p_web._DivHeader('CreateInvoice_' & p_web._nocolon('messageText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Are you sure you want to create an invoice?',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::messageText  Routine
    loc:comment = ''
  p_web._DivHeader('CreateInvoice_' & p_web._nocolon('messageText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CreateInvoice',0)

PreCopy  Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CreateInvoice',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CreateInvoice:Primed',0)

PreDelete       Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CreateInvoice:Primed',0)
  p_web.setsessionvalue('showtab_CreateInvoice',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CreateInvoice_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Process
      CreateTheInvoice(p_web)
  p_web.DeleteSessionValue('CreateInvoice_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CreateInvoice:Primed',0)
  p_web.StoreValue('')
CreateTheInvoice     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locPaid              BYTE                                  !
FilesOpened     BYTE(0)
  CODE
    IF (p_web.GSV('job:Invoice_Number') > 0)
        RETURN
    END

    IF (JobInUse(p_web.GSV('job:Ref_Number')) = 1)
        RETURN
    END

    DO openfiles

    SET(DEFAULTS,0)
    Access:DEFAULTS.Next()


    IF (vod.InvoiceTheSubAccount(p_web.GSV('job:Account_Number')))
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
            DO CloseFiles
            RETURN
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            DO CloseFiles
            RETURN
        END        
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = sub:Labour_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END
        
        locLabourVatRate = vat:VAT_Rate
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = sub:Parts_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END        
            
        locPartsVatRate = vat:VAT_Rate
        
        ! Don't think this is relevant to Vodacom        
!        IF (sub:Despatch_Invoiced_Jobs = 'YES')
!            IF (sub:Despatch_Paid_Jobs = 'YES')
!                IF (job:Paid = 'YES')
!                    ! Print Despatch = 1
!                ELSE
!                    ! Print Despatch = 2
!                END
!            ELSE
!                ! Print Despatch = 1
!            END
!        END
        
        
    ELSE
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
            DO CloseFiles
            RETURN
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            DO CloseFiles
            RETURN
        END  
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = tra:Labour_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END
        
        locLabourVatRate = vat:VAT_Rate
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = tra:Parts_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END        
            
        locPartsVatRate = vat:VAT_Rate       
    END

    JobPricingRoutine(p_web)

    IF (Access:INVOICE.PrimeRecord() = Level:Benign)
        inv:Invoice_Type = 'SIN'
        inv:Job_Number = p_web.GSV('job:Ref_Number')
        inv:Date_Created = TODAY()
        inv:Account_Number = p_web.GSV('job:Account_Number')
        IF (tra:Invoice_Sub_Accounts = 'YES')
            inv:AccountType = 'SUB'
        ELSE
            inv:AccountType = 'MAI'
        END
        inv:Total = job:Sub_Total
        inv:RRCVatRateLabour = locLabourVatRate
        inv:RRCVatRateParts = locPartsVatRate
        inv:RRCVatRateRetail = locLabourVatRate
        inv:Vat_Rate_Labour = locLabourVatRate
        inv:Vat_Rate_Parts = locPartsVatRate
        inv:Vat_Rate_Retail = locLabourVatRate
        inv:VAT_Number = def:Vat_Number
        inv:Courier_Paid = job:Courier_Cost
        inv:Parts_Paid = job:Parts_Cost
        inv:Labour_Paid = job:Labour_Cost
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            ! An RRC Created Invoice
            inv:RRCInvoiceDate = TODAY()
            inv:ExportedRRCOracle = TRUE
        END
        
        IF (Access:INVOICE.TryInsert())
            Access:INVOICE.CancelAutoInc()
            DO CloseFiles
            RETURN
        END
        
        locPaid = FALSE
        IF (p_web.GSV('job:Warranty_Job') = 'YES')
            IF (p_web.GSV('job:EDI') = 'FIN')
                locPaid = TRUE
            END
        END
        IF (p_web.GSV('job:Chargeable_Job') = 'YES')
            locPaid = FALSE
            p_web.SSV('TotalPrice:Type','C')
            TotalPrice(p_web)
            IF (p_web.GSV('TotalPrice:Total') = 0 OR p_web.GSV('TotalPrice:Balance') <= 0)
                locPaid = TRUE
            END
        END
        
        Access:COURIER.ClearKey(cou:Courier_Key)
        cou:Courier = job:Courier
        IF (Access:COURIER.TryFetch(cou:Courier_Key))
        END
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            IF (p_web.GSV('jobe:Despatched') <> 'REA')
                IF (locPaid = TRUE)
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',910) ! Despatched Paid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                    
                ELSE
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',905) ! Despatched UnPaid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                END
            ELSE
                IF (locPaid = true)
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',915) ! Paid Awaiting Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',916) ! Paid Awaiting Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END
                    
                ELSE
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',805) ! Ready To Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',810) ! Ready To Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END                    
                END
            END
        ELSE
            !ARC Invoice
            IF (p_web.GSV('job:Despatched') <> 'REA')
                IF (locPaid = TRUE)
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',910) ! Despatched Paid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                    
                ELSE
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',905) ! Despatched UnPaid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                END
            ELSE
                IF (locPaid = true)
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',915) ! Paid Awaiting Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',916) ! Paid Awaiting Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END
                    
                ELSE
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',805) ! Ready To Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',810) ! Ready To Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END                    
                END
                IF (p_web.GSV('job:Despatch_Type') = 'JOB' AND | 
                    p_web.GSV('jobe:WebJob') = 1)
                    p_web.SSV('GetStatus:StatusNumber',p_web.GSV('Default:StatusSentToRRC'))
                    p_web.SSV('GetSTatus:Type','JOB')
                    GetStatus(p_web)   
                END
                
            END
           
        END
        
        
        p_web.SSV('job:Invoice_Number',inv:Invoice_Number)
        p_web.SSV('job:Invoice_Date',TODAY())
        p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
        p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
        p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Sub_Total'))
        
        p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('jobe:InvRRCCSubTotal', p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
        p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate') )
        p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
     
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('job:Ref_Number')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE)
            Access:JOBSE.TryUpdate()
        END
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('job:Ref_Number')
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            p_web.SessionQueueToFile(JOBS)
            Access:JOBS.TryUpdate()
        END
        
        Access:JOBSE2.CLearkey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('job:Ref_Number')
        If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE2)
            Access:JOBSE2.TryUpdate()
        end
        
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            !todo: Line 500
            Line500_XML('RIV')
        END
        DO CloseFiles
        RETURN
        
    END



!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:COURIER.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:COURIER.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSE2.Close
     Access:VATCODE.Close
     Access:COURIER.Close
     Access:INVOICE.Close
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:JOBSE.Close
     Access:JOBS.Close
     Access:DEFAULTS.Close
     FilesOpened = False
  END
Line500_XML          PROCEDURE  (f:ID)                     ! Declare Procedure
!    INCLUDE('Line500XML.inc','Data')
  CODE
!    INCLUDE('Line500XML.inc','Code')
BrowseSubAddresses   PROCEDURE  (NetWebServerWorker p_web)
locSubRefNumber      LONG                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SUBACCAD)
                      Project(sua:RecordNumber)
                      Project(sua:CompanyName)
                      Project(sua:AccountNumber)
                      Project(sua:Postcode)
                      Project(sua:AddressLine1)
                      Project(sua:AddressLine2)
                      Project(sua:AddressLine3)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
SUBTRACC::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('BrowseSubAddresses')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseSubAddresses:NoForm')
      loc:NoForm = p_web.GetValue('BrowseSubAddresses:NoForm')
      loc:FormName = p_web.GetValue('BrowseSubAddresses:FormName')
    else
      loc:FormName = 'BrowseSubAddresses_frm'
    End
    p_web.SSV('BrowseSubAddresses:NoForm',loc:NoForm)
    p_web.SSV('BrowseSubAddresses:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseSubAddresses:NoForm')
    loc:FormName = p_web.GSV('BrowseSubAddresses:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseSubAddresses') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseSubAddresses')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SUBACCAD,sua:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SUA:COMPANYNAME') then p_web.SetValue('BrowseSubAddresses_sort','1')
    ElsIf (loc:vorder = 'SUA:ACCOUNTNUMBER') then p_web.SetValue('BrowseSubAddresses_sort','2')
    ElsIf (loc:vorder = 'SUA:POSTCODE') then p_web.SetValue('BrowseSubAddresses_sort','3')
    ElsIf (loc:vorder = 'SUA:ADDRESSLINE1') then p_web.SetValue('BrowseSubAddresses_sort','4')
    ElsIf (loc:vorder = 'SUA:ADDRESSLINE2') then p_web.SetValue('BrowseSubAddresses_sort','5')
    ElsIf (loc:vorder = 'SUA:ADDRESSLINE3') then p_web.SetValue('BrowseSubAddresses_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseSubAddresses:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseSubAddresses:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseSubAddresses:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseSubAddresses:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseSubAddresses:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseSubAddresses_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseSubAddresses_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 7
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:CompanyName)','-UPPER(sua:CompanyName)')
    Loc:LocateField = 'sua:CompanyName'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AccountNumber)','-UPPER(sua:AccountNumber)')
    Loc:LocateField = 'sua:AccountNumber'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:Postcode)','-UPPER(sua:Postcode)')
    Loc:LocateField = 'sua:Postcode'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AddressLine1)','-UPPER(sua:AddressLine1)')
    Loc:LocateField = 'sua:AddressLine1'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AddressLine2)','-UPPER(sua:AddressLine2)')
    Loc:LocateField = 'sua:AddressLine2'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AddressLine3)','-UPPER(sua:AddressLine3)')
    Loc:LocateField = 'sua:AddressLine3'
  end
  if loc:vorder = ''
    loc:vorder = '+sua:RefNumber,+UPPER(sua:CompanyName)'
  end
  If False ! add range fields to sort order
  Else
    If Instring('SUA:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'sua:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sua:CompanyName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  Of upper('sua:AccountNumber')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s15')
  Of upper('sua:Postcode')
    loc:SortHeader = p_web.Translate('Postcode')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s15')
  Of upper('sua:AddressLine1')
    loc:SortHeader = p_web.Translate('Address')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  Of upper('sua:AddressLine2')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  Of upper('sua:AddressLine3')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseSubAddresses:LookupFrom')
  End!Else
  loc:formaction = 'BrowseSubAddresses'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseSubAddresses:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseSubAddresses:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseSubAddresses:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SUBACCAD"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sua:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse Accounts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Accounts',0)&'</span>'&CRLF
  End
  If clip('Browse Accounts') <> ''
    packet = clip(packet) & p_web.br
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSubAddresses',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseSubAddresses',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseSubAddresses.locate(''Locator2BrowseSubAddresses'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSubAddresses.cl(''BrowseSubAddresses'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseSubAddresses_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseSubAddresses_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
        p_web.SSV('locSubRefNumber',sub:RecordNumber)
    END
    
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseSubAddresses','Company Name','Click here to sort by Company Name',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Company Name')&'">'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseSubAddresses','Account Number','Click here to sort by Account Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseSubAddresses','Postcode','Click here to sort by Postcode',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Postcode')&'">'&p_web.Translate('Postcode')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseSubAddresses','Address','Click here to sort by Address',,,,3)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Address')&'" colspan="'&clip(3)&'">'&p_web.Translate('Address')&'</th>'&CRLF
        End
        loc:SkipHeader =3 - 1
        do AddPacket
        loc:columns += 3
      If loc:SkipHeader
        loc:SkipHeader -= 1
      Else
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseSubAddresses','','',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('')&'">'&p_web.Translate('')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
      End ! if skipped header
      If loc:SkipHeader
        loc:SkipHeader -= 1
      Else
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseSubAddresses','','',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('')&'">'&p_web.Translate('')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
      End ! if skipped header
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sua:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SUBACCAD{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sua:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sua:RecordNumber'),p_web.GetValue('sua:RecordNumber'),p_web.GetSessionValue('sua:RecordNumber'))
    locSubRefNumber = p_web.RestoreValue('locSubRefNumber')
    loc:FilterWas = 'sua:RefNumber = ' & locSubRefNumber
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSubAddresses',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseSubAddresses_Filter')
    p_web.SetSessionValue('BrowseSubAddresses_FirstValue','')
    p_web.SetSessionValue('BrowseSubAddresses_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SUBACCAD,sua:RecordNumberKey,loc:PageRows,'BrowseSubAddresses',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SUBACCAD{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SUBACCAD,loc:firstvalue)
              Reset(ThisView,SUBACCAD)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SUBACCAD{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SUBACCAD,loc:lastvalue)
            Reset(ThisView,SUBACCAD)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sua:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSubAddresses.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSubAddresses.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSubAddresses.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSubAddresses.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSubAddresses',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseSubAddresses_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseSubAddresses_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseSubAddresses',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseSubAddresses.locate(''Locator1BrowseSubAddresses'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSubAddresses.cl(''BrowseSubAddresses'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseSubAddresses_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseSubAddresses_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSubAddresses.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSubAddresses.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSubAddresses.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSubAddresses.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sua:RecordNumber
    p_web._thisrow = p_web._nocolon('sua:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseSubAddresses:LookupField')) = sua:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sua:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseSubAddresses.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SUBACCAD{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SUBACCAD)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SUBACCAD{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SUBACCAD)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sua:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseSubAddresses.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sua:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseSubAddresses.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:CompanyName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AccountNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:Postcode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td colspan="'&clip(3)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AddressLine1
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AddressLine2
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AddressLine3
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseSubAddresses.omv(this);" onMouseOut="BrowseSubAddresses.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseSubAddresses=new browseTable(''BrowseSubAddresses'','''&clip(loc:formname)&''','''&p_web._jsok('sua:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sua:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseSubAddresses.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseSubAddresses.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSubAddresses')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSubAddresses')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSubAddresses')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSubAddresses')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SUBACCAD)
  p_web._CloseFile(SUBTRACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SUBACCAD)
  Bind(sua:Record)
  Clear(sua:Record)
  NetWebSetSessionPics(p_web,SUBACCAD)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sua:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&sua:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseSubAddresses',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseSubAddresses',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseSubAddresses',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:CompanyName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:CompanyName_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:CompanyName,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AccountNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AccountNumber_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AccountNumber,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:Postcode   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:Postcode_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:Postcode,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AddressLine1   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AddressLine1_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AddressLine1,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AddressLine2   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AddressLine2_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AddressLine2,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AddressLine3   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AddressLine3_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AddressLine3,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(sua:AccountNumberOnlyKey)
    loc:Invalid = 'sua:AccountNumber'
    loc:Alert = clip(p_web.site.DuplicateText) & ' AccountNumberOnlyKey --> '&clip('Account Number')&' = ' & clip(sua:AccountNumber)
  End
PushDefaultSelection  Routine
  loc:default = sua:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sua:RecordNumber',sua:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sua:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sua:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sua:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ClearAddressDetails  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locClearAddressOption BYTE                                 !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('ClearAddressDetails')
  loc:formname = 'ClearAddressDetails_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ClearAddressDetails','')
    p_web._DivHeader('ClearAddressDetails',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ClearAddressDetails',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ClearAddressDetails',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
deleteVariables     ROUTINE
    p_web.DeleteSessionValue('locClearAddressOption')
    p_web.DeleteSessionValue('AddType')
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ClearAddressDetails_form:inited_',1)
  do RestoreMem

CancelForm  Routine
      DO deleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locClearAddressOption',locClearAddressOption)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locClearAddressOption')
    locClearAddressOption = p_web.GetValue('locClearAddressOption')
    p_web.SetSessionValue('locClearAddressOption',locClearAddressOption)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ClearAddressDetails_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.StoreValue('AddType')
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locClearAddressOption = p_web.RestoreValue('locClearAddressOption')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'AmendAddress'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ClearAddressDetails_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ClearAddressDetails_ChainTo')
    loc:formaction = p_web.GetSessionValue('ClearAddressDetails_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'AmendAddress'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ClearAddressDetails" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ClearAddressDetails" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ClearAddressDetails" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Address Options') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Address Options',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ClearAddressDetails">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ClearAddressDetails" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ClearAddressDetails')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Options') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ClearAddressDetails')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ClearAddressDetails'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ClearAddressDetails')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Options') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ClearAddressDetails_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locClearAddressOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locClearAddressOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::locClearAddressOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locClearAddressOption',p_web.GetValue('NewValue'))
    locClearAddressOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locClearAddressOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locClearAddressOption',p_web.GetValue('Value'))
    locClearAddressOption = p_web.GetValue('Value')
  End
  do Value::locClearAddressOption
  do SendAlert

Value::locClearAddressOption  Routine
  p_web._DivHeader('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locClearAddressOption
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('locClearAddressOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locClearAddressOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Clear Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locClearAddressOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Customer Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  if p_web.GSV('Hide:CollectionAddress') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('AddType') = 'C') then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('locClearAddressOption') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Collection Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  if p_web.GSV('Hide:DeliveryAddress') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('AddType') = 'D') then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('locClearAddressOption') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(4)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Delivery Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_value')

Comment::locClearAddressOption  Routine
    loc:comment = ''
  p_web._DivHeader('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ClearAddressDetails_locClearAddressOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locClearAddressOption
      else
        do Value::locClearAddressOption
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)

PreCopy  Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)

PreDelete       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ClearAddressDetails_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      CASE p_web.GSV('locClearAddressOption')
      OF 1 ! Clear Address
          CASE p_web.GSV('AddType')
          OF 'C'
              p_web.SSV('job:Postcode_Collection','')
              p_web.SSV('job:Company_Name_Collection','')
              p_web.SSV('job:Address_Line1_Collection','')
              p_web.SSV('job:Address_Line2_Collection','')
              p_web.SSV('job:Address_Line3_Collection','')
              p_web.SSV('job:Telephone_Collection','')
              p_web.SSV('jobe2:HubCollection','')            
          OF 'D'
              p_web.SSV('job:Postcode_Delivery','')
              p_web.SSV('job:Company_Name_Delivery','')
              p_web.SSV('job:Address_Line1_Delivery','')
              p_web.SSV('job:Address_Line2_Delivery','')
              p_web.SSV('job:Address_Line3_Delivery','')
              p_web.SSV('job:Telephone_Delivery','')
              p_web.SSV('jobe2:HubDelivery','')            
          END
         
      OF 2 ! Copy Customer Address
          CASE p_web.GSV('AddType')
          OF 'C'
              p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
              p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
              p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
              p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
              p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
              p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
              p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))            
          OF 'D'
              p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
              p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
              p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
              p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
              p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
              p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
              p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))            
          END
      OF 3 ! Copy Collection Address
          IF (p_web.GSV('AddType') = 'D')
              p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode_Collection'))
              p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name_Collection'))
              p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1_Collection'))
              p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2_Collection'))
              p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3_Collection'))
              p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number_Collection'))
              p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer_Collection'))            
          END
          
      OF 4 ! Copy Delivery Address
          IF (p_web.GSV('AddType') = 'C')
              p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode_Delivery'))
              p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name_Delivery'))
              p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1_Delivery'))
              p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2_Delivery'))
              p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3_Delivery'))
              p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number_Delivery'))
              p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubDelivery'))  
          END
      ELSE
          loc:alert = 'You must select an option.'
          loc:invalid = 'locClearAddressOption'
  
      END
  
         
         DO deleteVariables
  p_web.DeleteSessionValue('ClearAddressDetails_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)
  p_web.StoreValue('locClearAddressOption')
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
InvoiceNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locDetail            STRING(10000)                         !
locCompanyName       STRING(30)                            !
locAddressLine1      STRING(30)                            !
locAddressLine2      STRING(30)                            !
locAddressLine3      STRING(30)                            !
locPostcode          STRING(30)                            !
locTelephoneNumber   STRING(30)                            !
locCustCompanyName   STRING(30)                            !
locCustAddressLine1  STRING(30)                            !
locCustAddressLine2  STRING(30)                            !
locCustAddressLine3  STRING(30)                            !
locCustPostcode      STRING(30)                            !
locCustTelephoneNumber STRING(30)                          !
locCustVatNumber     STRING(30)                            !
locSubTotal          REAL                                  !
locVat               REAL                                  !
locTotal             REAL                                  !
locInvoiceTime       TIME                                  !
locInvoiceDate       DATE                                  !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(0,0,8430,8230),PRE(RPT),PAPER(PAPER:USER,8430,8230),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
Detail                 DETAIL,AT(0,0,8430,8230),USE(?Detail),ABSOLUTE
                         TEXT,AT(375,-10,7656,7800),USE(locDetail),FONT('Courier New',11)
                       END
ANewPage               DETAIL,AT(0,0,8427,83),USE(?ANewPage),PAGEAFTER(1)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

                    MAP
addToDetail             PROCEDURE(<STRING textString>)
newLine                 PROCEDURE(<LONG number>)
PrintReport             PROCEDURE(BYTE fCredit)
CheckCreateInvoiceRRC   PROCEDURE(BYTE fCredit)
CheckCreateInvoiceARC   PROCEDURE(BYTE fCredit)
                    END

Detail_Queue        Queue,Pre(detail)
Line                    String(56)
Filler1                 String(4)
Amount                  String(20)
                    End ! Detail_Queue        Queue,Pre(detail)

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CreatePartsList     Routine
    Data
local:FaultDescription  String(255)
    Code
        Free(Detail_Queue)
        Clear(Detail_Queue)

        local:FaultDescription = Clip(BHStripReplace(p_web.GSV('jbn:Fault_Description'),'<13,10>','. '))

        If Clip(local:FaultDescription) <> ''
            detail:Line = Clip(Sub(local:FaultDescription,1,55))
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = Clip(Sub(local:FaultDescription,56,55))
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End            
        Else ! If Clip(jbn:Fault_Description) <> ''
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End            
        End ! If Clip(jbn:Fault_Description) <> ''

        detail:Line = ALL('-',55)
        detail:Amount = ''
        Add(Detail_Queue)

    ! Show Parts (DBH: 27/07/2007)
        RowNum# = 0
        Access:PARTS.Clearkey(par:Order_Number_Key)
        par:Ref_Number = p_web.GSV('job:Ref_Number')
        Set(par:Order_Number_Key,par:Order_Number_Key)
        Loop ! Begin Loop
            If Access:PARTS.Next()
                Break
            End ! If Access:PARTS.Next()
            If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                Break
            End ! If par:Ref_Number <> job:Ref_Number
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(par:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

    ! Show Fault Codes (DBH: 27/07/2007)
        Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
        joo:JobNumber = p_web.GSV('job:Ref_Number')
        Set(joo:JobNumberKey,joo:JobNumberKey)
        Loop ! Begin Loop
            If Access:JOBOUTFL.Next()
                Break
            End ! If Access:JOBOUTFL.Next()
            If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                Break
            End ! If joo:JobNumber <> job:Ref_Number
            If Instring('IMEI VALIDATION',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(joo:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

!    ! I guess this is filling in the blank lines (DBH: 27/07/2007)
!        detail:Line = ''
!        detail:Amount = ''
!        Loop
!            If Records(Detail_Queue) % prn:DetailRows = 0
!                Break
!            End ! If Records(Detail_Queue) % prn:DetailRows = 0
!            Add(Detail_Queue)
!        End ! Loop

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Print A5 Invoice
  IF (GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI') <> 1)
      VodacomSingleInvoice(p_web)
      RETURN RequestCompleted
  END
  GlobalErrors.SetProcedureName('InvoiceNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  locJobNumber = p_web.GSV('job:Ref_Number')
  
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('InvoiceNote',ProgressWindow)               ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  IF SELF.Opened
    INIMgr.Update('InvoiceNote',ProgressWindow)            ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue

addToDetail         PROCEDURE(<STRING textString>)
    CODE
        locDetail = CLIP(locDetail) & textString
        
newLine             PROCEDURE(<LONG number>)
    CODE
        IF (number = 0)
            locDetail = CLIP(locDetail) & '<13,10>'
        ELSE
            LOOP xx# = 1 TO number
                locDetail = CLIP(locDetail) & '<13,10>'
            END
        END
        
CheckCreateInvoiceARC       Procedure(BYTE fCredit)
local:AuditNotes        String(255)
    Code
        If inv:ARCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
            inv:ARCInvoiceDate = Today()

            locInvoiceTime = Clock()

            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + p_web.GSV('job:Invoice_Parts_Cost') |
                + p_web.GSV('job:Invoice_Courier_Cost'))
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = p_web.GSV('jobe:RefNumber')
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.tryupdate()
            END
            

            inv:ExportedARCOracle = 1

            Access:INVOICE.Update()

            Line500_XML('AOW')

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
            local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            LoanAttachedToJob(p_web)
            
            If p_web.GSV('LoanAttachedToJob') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If LoanAttachedToJob(p_web.GSV('job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
            If (p_web.GSV('jobe:Engineer48HourOption') = 1)
                If (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                End ! If p_web.GSV('jobe:ExcReplcamentCharge
            End  ! If p_web.GSV('jobe:Engineer48HourOption = 1
            
           
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2) & |
                '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2) & |
                '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2) & |
                '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2) & |
                '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                    

            
            p_web.SSV('AddToAudit:Type','JOB') 
            p_web.SSV('AddToAudit:Action','INVOICE CREATED')
            p_web.SSV('AddToAudit:Notes',Clip(local:AuditNotes))
            AddToAudit(p_web)
        Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
        ! Normal Invoice Reprint (DBH: 27/07/2007)
            p_web.SSV('AddToAudit:Type','JOB') 
            p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06) )
            AddToAudit(p_web)            

            locInvoiceTime = p_web.GSV('job:Time_Booked')
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Type = 'JOB'
            aud:Action = 'INVOICE CREATED'
            aud:Date = inv:ARCInvoiceDate
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                    Break
                End ! If aud:Ref_Number <> p_web.GSV('job:Ref_Number
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type <> 'JOB'
                If aud:Action <> 'INVOICE CREATED'
                    Break
                End ! If aud:Action <> 'INVOICE CREATED'
                If aud:Date <> inv:ARCInvoiceDate
                    Break
                End ! If aud:Date <> inv:RRCInvoiceDate
                locInvoiceTime = aud:Time
                Break
            End ! Loop
        End ! If inv:RRCInvoiceDate = 0

        locVAT = (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
            (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100)
        locSubTotal = p_web.GSV('job:Invoice_Sub_Total')
        locTotal = locSubTotal + locVAT

CheckCreateInvoiceRRC       Procedure(BYTE fCredit)
local:AuditNotes                String(255)
Code
    If inv:RRCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
        inv:RRCInvoiceDate = Today()

        locInvoiceTime = Clock()

        p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
        p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
        p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('jobe:RefNumber')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = level:Benign)
            p_web.SessionQueueToFile(JOBSE)
            Access:JOBSE.TryUpdate()
        END
            
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('jobe2:RefNumber')
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE2)
            Access:JOBSE2.TryUpdate()
        END
            

        inv:ExportedRRCOracle = 1

        Access:INVOICE.Update()

!    If f:Suffix = '' And f:Prefix = ''
!            ! Don't export credit notes (DBH: 02/08/2007)
!        Line500_XML('RIV')
!    End ! If f:Suffix = '' And f:Prefix = ''

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
        local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            
        LoanAttachedToJob(p_web)
        If (p_web.GSV('LoanAttachedToJob') = 1)
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
        End ! If LoanAttachedToJob(job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
        If p_web.GSV('jobe:Engineer48HourOption') = 1
            If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If jobe:ExcReplcamentCharge
        End ! If jobe:Engineer48HourOption = 1

        local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2) & |
            '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2) & |
            '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2) & |
            '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2) & |
            '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
            
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Notes',CLIP(local:AuditNotes))
        p_web.SSV('AddToAudit:Action','INVOICE CREATED')
        AddToAudit(p_web)

    Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
            
        IF (fCredit = 2)
                
            ! Credit Note (DBH: 27/07/2007)
            p_web.SSV('AddToAudit:Notes','CREDIT NOTE: ' & Clip('CN') & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                'AMOUNT: ' & Format(jov:CreditAmount,@n14.2))
            p_web.SSV('AddToAudit:Action','CREDIT NOTE PRINTED')
            p_web.SSV('AddToAudit:Type','JOB')
            AddToAudit(p_web)
        Else ! If f:Prefix <> ''
            If (fCredit = 1)
                ! Credit Invoice (DBH: 27/07/2007)
                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                    'AMOUNT: ' & Format(jov:NewTotalCost,@n14.2))
                p_web.SSV('AddToAudit:Action','CREDITED INVOICE PRINTED')
                p_web.SSV('AddToAudit:Type','JOB')
                AddToAudit(p_web)                    
            Else ! If f:Suffix <> ''
                ! Normal Invoice Reprint (DBH: 27/07/2007)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                    '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
                p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
                AddToAudit(p_web)            

                locInvoiceTime = p_web.GSV('job:Time_Booked')
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:Ref_Number = p_web.GSV('job:Ref_Number')
                aud:Type = 'JOB'
                aud:Action = 'INVOICE CREATED'
                aud:Date = inv:RRCInvoiceDate
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop ! Begin Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If Access:AUDIT.Next()
                    If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                        Break
                    End ! If aud:Ref_Number <> job:Ref_Number
                    If aud:Type <> 'JOB'
                        Break
                    End ! If aud:Type <> 'JOB'
                    If aud:Action <> 'INVOICE CREATED'
                        Break
                    End ! If aud:Action <> 'INVOICE CREATED'
                    If aud:Date <> inv:RRCInvoiceDate
                        Break
                    End ! If aud:Date <> inv:RRCInvoiceDate
                    locInvoiceTime = aud:Time
                    Break
                End ! Loop


            End ! If f:Suffix <> ''
        End ! If f:Prefix <> ''
    End ! If inv:RRCInvoiceDate = 0

    locVAT = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
        (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
        (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
    locSubTotal = p_web.GSV('jobe:InvRRCCLabourCost') + | 
        p_web.GSV('jobe:InvRRCCPartsCost') + | 
        p_web.GSV('job:Invoice_Courier_Cost')
    locTotal = locSubTotal + locVAT

    IF (fCredit = 1)
        locTotal = jov:NewTotalCost
    END
    IF (fCredit = 2)
        locTotal = -jov:CreditAmount
    END
PrintReport         procedure(BYTE fCredit)
CODE
    
    
    newLine(7)

    IF (p_web.GSV('BookingSite') = 'ARC')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('ARC:AccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number
        
        locInvoiceDate = inv:ARCInvoiceDate
        CheckCreateInvoiceARC(fCredit)
    ELSE
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number
        
        locInvoiceDate = inv:RRCInvoiceDate
        CheckCreateInvoiceRRC(fCredit)
    END


    IF (p_web.GSV('jobe:WebJob') = 1)
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        IF (sub:OverrideHeadVATNo = TRUE)
            locCustVatNumber = sub:VAT_Number            
        ELSE
            locCustVatNumber = tra:VAT_Number
        END
        
        IF (sub:Invoice_Customer_Address = 'YES')
            IF (p_web.GSV('job:Company_Name') = '')
                locCustCompanyName = p_web.GSV('job:Initial') & ' ' & p_web.GSV('job:Initial') & | 
                    ' ' & p_web.GSV('job:Surname')
            ELSE
                locCustCompanyName = p_web.GSV('job:Company_Name')
            END
            locCustAddressLine1 = p_web.GSV('job:Address_Line1')
            locCustAddressLine2 = p_web.GSV('job:Address_Line2')
            locCustAddressLine3 = p_web.GSV('job:Address_Line3')
            locCustPostcode = p_web.GSV('job:Postcode')
            locCustTelephoneNumber = p_web.GSV('job:Telephone_Number')
        ELSE
            locCustCompanyName = sub:Company_Name
            locCustAddressLine1 = sub:Address_Line1
            locCustAddressLine2 = sub:Address_Line2
            locCustAddressLine3 = sub:Address_Line3
            locCustPostcode = sub:Postcode
            locCustTelephoneNumber = sub:Telephone_Number
        END
    END

        
    addToDetail(LEFT(locCustCOmpanyName,33) & locCompanyName)
    newLine()
    addToDetail(LEFT(locCustAddressLine1,33) & locAddressLine1)
    newLine()
    addToDetail(LEFT(locCustAddressLine2,33) & locAddressLine2)
    newLine()
    addToDetail(LEFT(locCustAddressLine3,33) & locAddressLine3)
    newLine()
    addToDetail(LEFT(locCustPostcode,33) & locPostcode)
    newLine()
    addToDetail(LEFT(locCustTelephoneNumber,33) & locTelephoneNumber)
    newLine()
    addToDetail('VAT NO.: ' & locCustVatNumber)
    newLine(2)
        
! Job Number
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
    END
    addToDetail(RIGHT('',33) & 'Job Number ' & p_web.GSV('job:Ref_Number') & | 
        '-' & tra:BranchIdentification & p_web.GSV('wob:JobNumber'))
    newLine(5)
        
! Order Line
        
    IF (fCredit = 1)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT(CLIP(inv:Invoice_Number) & p_web.GSV('InvoiceSuffix'),13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSIF (fCredit = 2)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT('CN' & CLIP(inv:Invoice_Number) & p_web.GSV('CreditSuffix'),13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSE
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT(inv:Invoice_Number,13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))

    END
    
        
    newLine(5)
        
! IMEI LIne
    addToDetail(LEFT(p_web.GSV('job:Manufacturer'),16) & | 
        LEFT(p_web.GSV('job:Model_Number'),17) & |
        LEFT(p_web.GSV('job:MSN'),25) & |
        LEFT(p_web.GSV('job:ESN'),20))
        
    newLine(2)
        
    Do CreatePartsList

    Loop x# = 1 To 14
        Get(Detail_Queue,x#)
        IF (ERROR())
            ERROR# = TRUE
        ELSE
            ERROR# = FALSE
        END
        detail:Amount = ''
        Case x#
        Of 1
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCLabourCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Labour_Cost'),@n-14.2))
            END
                
        Of 3
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCPartsCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Parts_Cost'),@n-14.2))
            END
                
        Of 5
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            END
                
        Of 10
            detail:Amount = (Format(locSubTotal,@n-14.2))
        Of 12
            detail:Amount = (Format(locVAT,@n-14.2))
        Of 14
            detail:Amount = (Format(locTotal,@n-14.2))
        End ! Case x#

        IF (ERROR#)
            detail:Line = '-'
            Add(Detail_Queue)
        ELSE
            Put(Detail_Queue)
        END
        
    End ! Loop x# = 1 To Records(Detail_Queue)

    Get(Detail_Queue,1)

    Loop x# = 1 To 15
        Get(Detail_Queue,x#)
        IF ~(ERROR())
            addToDetail(LEFT(detail:Line,68) & | 
                RIGHT(detail:Amount,10))
        END
        newLine()
    End ! Loop x# = 1 To Records(Detail_Queue)        
        
! Two Dates
    addToDetail(LEFT('',10) & |
        LEFT(FORMAT(p_web.GSV('job:Date_Booked'),@d06),32) & | 
        FORMAT(p_web.GSV('job:Time_Booked'),@t01))
    newLine(2)
    
    IF (fCredit <> 0)
        ! If credit use dates
        locInvoiceDate = jov:DateCreated
        locInvoiceTime = jov:TimeCreated
    END
    
    addToDetail(LEFT('',10) & | 
        LEFT(FORMAT(locInvoiceDate,@d06),32) & | 
        LEFT(FORMAT(locInvoiceTime,@t01),20) & | 
        p_web.GSV('job:Engineer'))
              


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Printing Details
      
  Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
  inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
  IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
  END
  
  ! Is this a credit note?
  IF (p_web.GSV('CreditNoteCreated') = 1)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'C'
      jov:Suffix = p_web.GSV('CreditSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(2)
      Print(rpt:Detail)
      Print(rpt:ANewPage)
      locDetail = ''
      
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'I'
      jov:Suffix = p_web.GSV('InvoiceSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(1)
      Print(rpt:Detail)
      
      
  
      
  ELSE
      PrintReport(0)
      Print(rpt:Detail)
  END
  
  
  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase2','InvoiceNote','InvoiceNote','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = False
  SELF.CompressImages = False
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

InvoiceCreated       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('InvoiceCreated')
  loc:formname = 'InvoiceCreated_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('InvoiceCreated','')
    p_web._DivHeader('InvoiceCreated',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_InvoiceCreated',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_InvoiceCreated',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('InvoiceCreated_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('InvoiceCreated_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('InvoiceCreated_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('InvoiceCreated_ChainTo')
    loc:formaction = p_web.GetSessionValue('InvoiceCreated_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="InvoiceCreated" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="InvoiceCreated" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="InvoiceCreated" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Invoice Created') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Invoice Created',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_InvoiceCreated">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_InvoiceCreated" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_InvoiceCreated')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_InvoiceCreated')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_InvoiceCreated'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_InvoiceCreated')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_InvoiceCreated_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::buttonPrintInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintInvoice',p_web.GetValue('NewValue'))
    do Value::buttonPrintInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintInvoice  Routine
  p_web._DivHeader('InvoiceCreated_' & p_web._nocolon('buttonPrintInvoice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintInvoice','Print Invoice','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('InvoiceNote?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images\money.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonPrintInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('InvoiceCreated_' & p_web._nocolon('buttonPrintInvoice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_InvoiceCreated',0)

PreCopy  Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_InvoiceCreated',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('InvoiceCreated:Primed',0)

PreDelete       Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('InvoiceCreated:Primed',0)
  p_web.setsessionvalue('showtab_InvoiceCreated',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('InvoiceCreated_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('InvoiceCreated_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('InvoiceCreated:Primed',0)
  p_web.StoreValue('')
