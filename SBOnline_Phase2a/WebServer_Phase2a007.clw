

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A007.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A026.INC'),ONCE        !Req'd for module callout resolution
                     END


CreateTestRecord     PROCEDURE  (String f:Path)            ! Declare Procedure
tmp:TestRecord       STRING(255),STATIC                    !
TestRecord    File,Driver('ASCII'),Pre(test),Name(tmp:TestRecord),Create,Bindable,Thread
Record                  Record
TestLine                String(1)
                        End
                    End
  CODE
    ! Start - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
    tmp:TestRecord = Clip(f:Path)

    If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
    Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
    End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'

    Remove(TestRecord)
    Create(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Open(TestRecord)
    If Error()
        Return False
    End ! If Error()
    test:TestLine = '1'
    Add(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Close(TestRecord)
    Remove(TestRecord)
    If Error()
        Return False
    End ! If Error()
    ! End   - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005
    Return True
CID_XML              PROCEDURE  (String f:Mobile,String f:Account,Byte f:Type) ! Declare Procedure
seq                  LONG                                  !
RepositoryDir        CSTRING('C:\ServiceBaseMQ\MQ_Repository<0>{224}') !
save_invoice_id      USHORT,AUTO                           !
save_tradeacc_id     USHORT,AUTO                           !
save_subtracc_id     USHORT,AUTO                           !
tmp:STFMessage       STRING(30)                            !STF Message
tmp:AOWMessage       STRING(30)                            !AOW Message
tmp:RIVMessage       STRING(30)                            !RIV Message
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
XmlData group,type
ServiceCode string(3)
CID         group
MSISDN_OR_ICCID         string(20)
CALL_ID                 string(7)
ACCOUNT_ID              string(9)
SOURCE                  string(60)
TOPIC_CODE              string(60)
COMMENT                 string(255)
            end
        end

    map
SendXML         procedure(const *XmlData aXmlData, string aRepositoryDir)
    end

XmlValues GROUP(XmlData)
           END
  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
    ! Check default to see if should create messages - TrkBs: 6141 (DBH: 20-07-2005)
    If GETINI('XML','CreateCID',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        Return
    End ! If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> True

    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    If CreateTestRecord(RepositoryDir) = False
        Return
    End ! If CreateTestRecord(RepositoryDir) = False

    Do CreateCID
CreateCID                  Routine
    Access:TRADEACC_ALIAS.Open()
    Access:TRADEACC_ALIAS.UseFile()
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = f:Account
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign

    clear(XmlValues)
    XmlValues.ServiceCode         = 'CID'
    XmlValues.CID.MSISDN_OR_ICCID = f:Mobile
    XmlValues.CID.CALL_ID         = ''
    XmlValues.CID.ACCOUNT_ID      = ''
    XmlValues.CID.SOURCE          = 'ServiceBase'
    Case f:Type
    Of 1 !Booked In
        XmlValues.CID.TOPIC_CODE      = 'Repair Booked In'
        XmlValues.CID.COMMENT         = 'Repair Booked In At ' & Clip(tra_ali:Company_Name)
    Of 2 !Despatched
        XmlValues.CID.TOPIC_CODE      = 'Repair Collected'
        XmlValues.CID.COMMENT         = 'Repair Collected From ' & Clip(tra_ali:Company_Name)
    End ! Case f:Type

    SendXml(XmlValues, RepositoryDir)

    Access:TRADEACC_ALIAS.Close()
SendXML         procedure(aXmlData, aRepositoryDir)

TheDate date
TheTime time
MsgId   string(24)

    code

    TheDate = today()
    TheTime = clock()
    MsgId = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
    seq += 1
    if seq > 99999 then
        seq = 1
    end
    if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
        objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
            objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
                objXmlExport.WriteTag('SRC_SYSTEM', '')
                objXmlExport.WriteTag('SRC_APPLICATION', '')
                objXmlExport.WriteTag('SERVICING_APPLICATION', '')
                objXmlExport.WriteTag('ACTION_CODE', 'INS')
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
                objXmlExport.WriteTag('MESSAGE_ID', '')
                objXmlExport.OpenTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('VALIDITY','84600000','unit="millisecond"')
                objXmlExport.WriteTag('ACTION','','type="DISCARD"')
                objXmlExport.WriteTag('RESPONSE_REQUIRED','Y')
                objXmlExport.CloseTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('USER_NAME', '')
                objXmlExport.WriteTag('TOKEN', '')
            objXmlExport.CloseTag('MESSAGE_HEADER')
            objXmlExport.OpenTag('MESSAGE_BODY')

            objXmlExport.OpenTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')
                objXmlExport.WriteTag('MSISDN_OR_ICCID', clip(aXmlData.CID.MSISDN_OR_ICCID))
                objXmlExport.WriteTag('CALL_ID', clip(aXmlData.CID.CALL_ID))
                objXmlExport.WriteTag('ACCOUNT_ID', clip(aXmlData.CID.ACCOUNT_ID))
                objXmlExport.WriteTag('SOURCE', clip(aXmlData.CID.SOURCE))
                objXmlExport.WriteTag('TOPIC_CODE', clip(aXmlData.CID.TOPIC_CODE))
                objXmlExport.WriteTag('COMMENT', clip(aXmlData.CID.COMMENT))
             objXmlExport.CloseTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')

            objXmlExport.CloseTag('MESSAGE_BODY')
        objXmlExport.CloseTag('VODACOM_MESSAGE')
        objXmlExport.FClose();
    end





GetStatus            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
SentJob     STRING(1)
  CODE
!region ProcessedCode
        ! GetStatus:StatusNumber
        ! GetStatus:Type
        if (p_web.GSV('GetStatus:StatusNumber') <> '' and p_web.GSV('GetStatus:Type') <> '')
            
            SentJob = p_web.GSV('GetStatus:Type')
            
            relate:DEFAULTS.open()
            relate:STATUS.open()
            relate:AUDSTATS.open()
            relate:STAHEAD.open()
            relate:JOBSTAGE.open()
    
            Access:DEFAULTS.Clearkey(def:RecordNumberKey)
            def:Record_Number = 0
            Set(def:RecordNumberKey,def:RecordNumberKey)
            Loop
                If Access:DEFAULTS.Next()
                    Break
                End ! If Access:DEFAULTS.Next()
                Break
            End ! Loop
    
            Case p_web.GSV('GetStatus:Type')
            Of 'JOB'
                If p_web.GSV('job:Cancelled') <> 'YES'
                    Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
                    sts:Ref_Number = p_web.GSV('GetStatus:StatusNumber')
                    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                        p_web.SSV('job:Current_Status','ERROR (' & Clip(p_web.GSV('GetStatus:StatusNumber')) & ')')
                    Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                        If sts:Status <> p_web.GSV('job:Current_Status')
                            p_web.SSV('job:PreviousStatus',p_web.GSV('job:Current_Status'))
                            p_web.SSV('job:StatusUser',p_web.GSV('BookingUserCode'))
                            p_web.SSV('job:Current_Status',sts:Status)
                            p_web.SSV('wob:Current_Status_Date',Today())
                            p_web.SSV('wob:Current_Status',sts:Status)
                            If Access:AUDSTATS.PrimeRecord() = Level:Benign
                                aus:RefNumber = p_web.GSV('job:Ref_Number')
                                aus:Type = 'JOB'
                                aus:DateChanged = Today()
                                aus:TimeChanged = Clock()
                                aus:OldStatus = p_web.GSV('job:PreviousStatus')
                                aus:NewStatus = p_web.GSV('job:Current_Status')
                                aus:UserCode = p_web.GSV('BookingUserCode')
                                If Access:AUDSTATS.TryInsert() = Level:Benign
    
                                Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                                    Access:AUDSTATS.CancelAutoInc()
                                End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                            End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign
    
                            Access:STAHEAD.Clearkey(sth:Ref_Number_Key)
                            sth:Ref_Number = sts:Heading_Ref_Number
                            If Access:STAHEAD.TryFetch(sth:Ref_Number_Key) = Level:Benign
                                Access:JOBSTAGE.Clearkey(jst:Job_Stage_Key)
                                jst:Ref_Number = p_web.GSV('job:Ref_Number')
                                jst:Job_Stage = sth:Heading
                                If Access:JOBSTAGE.TryFetch(jst:Job_Stage_Key)
                                    If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                                        jst:Ref_Number = p_web.GSV('job:Ref_Number')
                                        jst:Job_Stage = sth:Heading
                                        jst:Time = CLock()
                                        jst:Date = Today()
                                        jst:User = p_web.GSV('BookingUserCode')
                                        If Access:JOBSTAGE.TryInsert() = Level:Benign
    
                                        Else ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                                            Access:JOBSTAGE.CancelAutoInc()
                                        End ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                                    End ! If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                                End ! If Access:JOBSTAGE.TryFetch()
                            End ! If Access:STAHEAD.TryFetch(sth:Ref_Number_Key) = Level:Benign
    
                            If sts:use_turnaround_time  = 'YES'
                                EndDays# = Today()
                                x# = 0
                                count# = 0
                                If sts:Turnaround_Days <> 0
                                    Loop
                                        count# += 1
                                        day_number# = (Today() + count#) % 7
                                        If def:include_saturday <> 'YES'
                                            If day_number# = 6
                                                EndDays# += 1
                                                Cycle
                                            End
                                        End
                                        If def:include_sunday <> 'YES'
                                            If day_number# = 0
                                                EndDays# += 1
                                                Cycle
                                            End
                                        End
                                        EndDays# += 1
                                        x# += 1
                                        If x# >= sts:Turnaround_Days
                                            Break
                                        End!If x# >= sts:turnaround_days
                                    End!Loop
                                End!If f_days <> ''
    
                                EndHours# = Clock()
                                If sts:Turnaround_Hours <> 0
                                    start_time_temp# = Clock()
                                    new_day# = 0
                                    Loop x# = 1 To sts:Turnaround_Hours
                                        EndHours# += 360000
                                        If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
                                            If EndHours# > def:End_Work_Hours or EndHours# < def:Start_Work_Hours
                                                EndHours# = def:Start_Work_Hours
                                                EndDays# += 1
                                            End !If tmp:End_Hours > def:End_Work_Hours
                                        End !If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
    
                                    End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
                                End!If f_hours <> ''
    
                                p_web.SSV('job:Status_End_Time',EndHours#)
                                p_web.SSV('job:status_end_date',EndDays#)
                            Else!If sts:use_turnaround_time = 'YES'
                                p_web.SSV('job:status_end_date',Deformat('1/1/2050',@d6))
                                p_web.SSV('job:status_end_Time',Clock())
                            End!If sts:use_turnaround_time  = 'YES'
                        Else ! If sts:Status <> job:Current_Status
                        End ! If sts:Status <> job:Current_Status
                    End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                End ! If job:Cancelled <> 'YES'
            Of 'EXC'
                Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
                sts:Ref_Number = p_web.GSV('GetStatus:StatusNumber')
                If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                    p_web.SSV('job:Exchange_Status','ERROR (' & Clip(p_web.GSV('GetStatus:StatusNumber')) & ')')
                Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                    If sts:Status <> p_web.GSV('job:Exchange_Status')
                        PreviousStatus" = p_web.GSV('job:Exchange_Status')
                        p_web.SSV('job:Exchange_Status',sts:Status)
                        p_web.SSV('wob:Exchange_Status_Date',Today())
                        p_web.SSV('wob:Exchange_Status',sts:Status)
                        If Access:AUDSTATS.PrimeRecord() = Level:Benign
                            aus:RefNumber = p_web.GSV('job:Ref_Number')
                            aus:Type = 'EXC'
                            aus:DateChanged = Today()
                            aus:TimeChanged = Clock()
                            aus:OldStatus = PreviousStatus"
                            aus:NewStatus = p_web.GSV('job:Exchange_Status')
                            aus:UserCode = p_web.GSV('BookingUserCode')
                            If Access:AUDSTATS.TryInsert() = Level:Benign
    
                            Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                                Access:AUDSTATS.CancelAutoInc()
                            End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                        End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign
    
                    Else ! If sts:Status <> job:Current_Status
                    End ! If sts:Status <> job:Current_Status
                End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
    
            Of 'LOA'
                Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
                sts:Ref_Number = p_web.GSV('GetStatus:StatusNumber')
                If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                    p_web.SSV('job:Loan_Status','ERROR (' & Clip(p_web.GSV('GetStatus:StatusNumber')) & ')')
                Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                    If sts:Status <> p_web.GSV('job:Loan_Status')
                        PreviousStatus" = p_web.GSV('job:Loan_Status')
                        p_web.SSV('job:Loan_Status',sts:Status)
                        p_web.SSV('wob:Loan_Status_Date',Today())
                        p_web.SSV('wob:Loan_Status',sts:Status)
                        If Access:AUDSTATS.PrimeRecord() = Level:Benign
                            aus:RefNumber = p_web.GSV('job:Ref_Number')
                            aus:Type = 'LOA'
                            aus:DateChanged = Today()
                            aus:TimeChanged = Clock()
                            aus:OldStatus = PreviousStatus"
                            aus:NewStatus = p_web.GSV('job:Loan_Status')
                            aus:UserCode = p_web.GSV('BookingUserCode')
                            If Access:AUDSTATS.TryInsert() = Level:Benign
    
                            Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                                Access:AUDSTATS.CancelAutoInc()
                            End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                        End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign
    
                    Else ! If sts:Status <> job:Current_Status
                    End ! If sts:Status <> job:Current_Status
                End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
    
            End ! Case p_web.GSV('GetStatus:Type')
    
        end ! if (p_web.GSV('GetStatus:StatusNumber') <> '' and p_web.GSV('GetStatus:StatusType') <> '')
    
        p_web.deleteSessionValue('GetStatus:StatusNumber')
        p_web.deleteSessionValue('GetStatus:Type')
        
        
        SendSMSText(SentJob,'Y','N',p_web) ! #12477 Call the SMS routine (DBH: 17/05/2012)

!endregion
InvoiceSubAccounts   PROCEDURE  (func:AccountNumber)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                Return Level:Fatal
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign
GetVATRate           PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
rtnValue             REAL                                  !
FilesOpened     BYTE(0)
  CODE
    DO openFiles
    rtnValue = 0
    If InvoiceSubAccounts(func:AccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case func:Type
        Of 'L'
            vat:Vat_Code    = sub:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = sub:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnvalue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    Else !If InvoiceSubAccounts(func:AccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case func:Type
        Of 'L'
            vat:Vat_Code    = tra:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = tra:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnValue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    End !If InvoiceSubAccounts(func:AccountNumber)
    Do CloseFiles
    Return rtnValue
    
    
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
JobCard PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
tmp:JobNumber        LONG                                  !Job Number
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(20)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Courier)
                       PROJECT(job:DOP)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Date_QA_Passed)
                       PROJECT(job:ESN)
                       PROJECT(job:Location)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Time_Completed)
                       PROJECT(job:Time_QA_Passed)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(260,6458,7750,813),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(260,615,7750,8250),USE(?unnamed),FONT('Tahoma',7)
                         STRING(@s50),AT(6094,469),USE(who_booked),FONT(,8,,FONT:bold),TRN
                         STRING('Job Number: '),AT(5104,52),USE(?String25),FONT(,8),TRN
                         STRING(@s16),AT(6010,21),USE(tmp:Ref_number),FONT(,11,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(5104,313),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(6094,313),USE(job:date_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@t1b),AT(6979,313),USE(job:time_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Booked By:'),AT(5104,469),USE(?String158),FONT(,8),TRN
                         STRING(@d6b),AT(6094,781),USE(job:DOP),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Engineer:'),AT(5104,625),USE(?String96),FONT(,8),TRN
                         STRING(@s30),AT(6094,625),USE(engineer_temp),FONT(,8,,FONT:bold),TRN
                         STRING('Date Of Purchase:'),AT(5104,781),USE(?String96:2),FONT(,8),TRN
                         STRING('Model'),AT(177,3500),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Location'),AT(177,2833),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Type'),AT(4708,2833),USE(?warranty_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Repair Type'),AT(6219,2833),USE(?warranty_repair_type),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Chargeable Type'),AT(1688,2833),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(3198,2833),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1688,3500),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3198,3500),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4708,3510),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6219,3510),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(1688,3073),USE(job:Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3198,3073),USE(job:Repair_Type),FONT(,8),TRN
                         STRING(@s20),AT(177,3073),USE(job:Location),FONT(,8),TRN
                         STRING(@s30),AT(177,3698,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1688,3698,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3198,3698,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(4708,3698,1396,156),USE(job:ESN),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6219,3698),USE(job:MSN),FONT(,8),TRN
                         STRING(@s20),AT(6219,4010),USE(exchange_msn_temp),FONT(,8),TRN
                         STRING(@s16),AT(4708,4010,1396,156),USE(exchange_esn_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3198,4010,1396,156),USE(exchange_unit_type_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1688,4010,1323,156),USE(exchange_manufacturer_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(177,4010,1000,156),USE(exchange_model_number),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(177,3854),USE(exchange_unit_title_temp),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s20),AT(1354,3854),USE(exchange_unit_number_temp),LEFT,TRN
                         STRING('REPORTED FAULT'),AT(3562,4229,1062,156),USE(?String64),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(4625,4219,2406,844),USE(jbn:Fault_Description),FONT(,7),TRN
                         STRING('ESTIMATE'),AT(156,4583,1354,156),USE(?Estimate),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING(@s70),AT(1625,5031,5625,208),USE(estimate_value_temp),FONT(,8),TRN
                         STRING('ENGINEERS REPORT'),AT(156,5031,1354,156),USE(?String88),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR NOTES'),AT(156,4229),USE(?String88:3),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(1615,4646,1927,417),USE(tmp:InvoiceText),FONT(,7),TRN
                         TEXT,AT(1615,5208,5417,313),USE(tmp:accessories),FONT(,7),TRN
                         STRING('ACCESSORIES'),AT(156,5208,1354,156),USE(?String100),FONT(,8,,FONT:bold),TRN
                         STRING('Type'),AT(521,5677),USE(?String98),FONT(,7,,FONT:bold),TRN
                         STRING('PARTS REQUIRED'),AT(156,5521,1135,156),USE(?String79),FONT(,8,,FONT:bold),TRN
                         STRING('Qty'),AT(156,5677),USE(?String80),FONT(,7,,FONT:bold),TRN
                         STRING('Part Number'),AT(885,5677),USE(?String81),FONT(,7,,FONT:bold),TRN
                         STRING('Description'),AT(2292,5677),USE(?String82),FONT(,7,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(4063,5677),USE(?String83),FONT(,7,,FONT:bold),TRN
                         STRING('Line Cost'),AT(4948,5677),USE(?String89),FONT(,7,,FONT:bold),TRN
                         LINE,AT(156,5833,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(156,6667,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('BOUNCER'),AT(156,6719,1354,156),USE(?BouncerTitle),FONT('Tahoma',8,,FONT:bold),HIDE, |
  TRN
                         TEXT,AT(1667,6719,5417,208),USE(tmp:bouncers),FONT(,8),HIDE,TRN
                         STRING('LOAN UNIT ISSUED'),AT(156,6927,1354,156),USE(?LoanUnitIssued),FONT('Tahoma',8,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s30),AT(1667,6927),USE(tmp:LoanModel),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  LEFT,HIDE,TRN
                         STRING('IMEI'),AT(3646,6927),USE(?IMEITitle),FONT('Tahoma',9,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         STRING(@s16),AT(4010,6927),USE(tmp:LoanIMEI),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  LEFT,HIDE,TRN
                         STRING('CUSTOMER SIGNATURE'),AT(177,8042,1583,156),USE(?String820),FONT('Tahoma',8,,FONT:bold), |
  TRN
                         STRING('REPLACEMENT VALUE'),AT(5104,6927),USE(?LoanValueText),FONT('Tahoma',9,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@n10.2),AT(6406,6927),USE(tmp:ReplacementValue),FONT(,8),RIGHT,HIDE,TRN
                         LINE,AT(4719,8198,1406,0),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Date: {22}/ {23}/'),AT(4490,8042,2542,156),USE(?String830),FONT(,,,FONT:bold),TRN
                         LINE,AT(1615,8187,2917,0),USE(?Line5:2),COLOR(COLOR:Black)
                         STRING('LOAN ACCESSORIES'),AT(156,7083,1354,156),USE(?LoanAccessoriesTitle),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s255),AT(1667,7083,5417,208),USE(tmp:LoanAccessories),FONT('Tahoma',8,COLOR:Black, |
  FONT:regular,CHARSET:ANSI),LEFT,HIDE,TRN
                         STRING('EXTERNAL DAMAGE CHECK LIST'),AT(146,7240),USE(?ExternalDamageCheckList),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Notes:'),AT(5771,7240),USE(?Notes),FONT(,7),TRN
                         TEXT,AT(6083,7240,1458,469),USE(jobe2:XNotes),FONT(,6),TRN
                         CHECK('None'),AT(5146,7240,625,156),USE(jobe2:XNone),TRN
                         CHECK('Keypad'),AT(146,7396,729,156),USE(jobe2:XKeypad),TRN
                         CHECK('Charger'),AT(2073,7396,781,156),USE(jobe2:XCharger),TRN
                         CHECK('Antenna'),AT(2073,7240,833,156),USE(jobe2:XAntenna),TRN
                         CHECK('Lens'),AT(2917,7240,573,156),USE(jobe2:XLens),TRN
                         CHECK('F/Cover'),AT(3542,7240,781,156),USE(jobe2:XFCover),TRN
                         CHECK('B/Cover'),AT(4427,7240,729,156),USE(jobe2:XBCover),TRN
                         CHECK('Battery'),AT(979,7396,729,156),USE(jobe2:XBattery),TRN
                         CHECK('LCD'),AT(2917,7406,625,156),USE(jobe2:XLCD),TRN
                         CHECK('System Connector'),AT(4427,7406,1250,156),USE(jobe2:XSystemConnector),TRN
                         CHECK('Sim Reader'),AT(3542,7396,885,156),USE(jobe2:XSimReader),TRN
                         STRING(@s30),AT(156,1927),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4167,2083),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4427,2083),USE(Delivery_Telephone_Temp),FONT(,8),TRN
                         STRING(@s50),AT(521,2083,3490,156),USE(invoice_EMail_Address),FONT(,8),LEFT,TRN
                         STRING('Email:'),AT(156,2083),USE(?unnamed:4),FONT(,8),TRN
                         STRING('Tel:'),AT(156,2240),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(521,2240),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(156,2396),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(521,2396),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2760,1302),USE(job:Account_Number),FONT(,8),TRN
                         STRING(@s20),AT(4708,3073),USE(job:Warranty_Charge_Type),FONT(,8),TRN
                         STRING(@s20),AT(6219,3073),USE(job:Repair_Type_Warranty),FONT(,8),TRN
                         STRING('Acc No:'),AT(2188,1302),USE(?String94),FONT(,8,COLOR:Black),TRN
                         STRING(@s30),AT(4167,1302),USE(delivery_Company_Name_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1458,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s20),AT(2760,1458),USE(job:Order_Number),FONT(,8),TRN
                         STRING('Order No:'),AT(2188,1458),USE(?String140),FONT(,8),TRN
                         STRING(@s30),AT(4167,1615),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1771),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1927),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1302),USE(invoice_company_name_temp),FONT(,8,COLOR:Black),TRN
                         STRING(@s30),AT(156,1458),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1771),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1615),USE(invoice_address2_temp),FONT(,8),TRN
                         STRING(@s65),AT(4167,2396,2865,156),USE(clientName),FONT('Tahoma',8,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         TEXT,AT(146,7802,7375,208),USE(stt:Text),TRN
                         TEXT,AT(146,7542,5885,260),USE(locTermsText)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:3)
DETAIL                   DETAIL,AT(0,0,,115),USE(?DetailBand)
                           STRING(@n8b),AT(-104,0),USE(Quantity_temp),FONT(,7),RIGHT,TRN
                           STRING(@s25),AT(885,0),USE(part_number_temp),FONT(,7),TRN
                           STRING(@s25),AT(2292,0),USE(Description_temp),FONT(,7),TRN
                           STRING(@n14.2b),AT(3802,0),USE(Cost_Temp),FONT(,7),RIGHT,TRN
                           STRING(@n14.2b),AT(4740,0),USE(line_cost_temp),FONT(,7),RIGHT,TRN
                           STRING(@s4),AT(521,0),USE(part_type_temp),FONT(,7),TRN
                         END
                         FOOTER,AT(396,8896,,2406),USE(?unnamed:2),ABSOLUTE,TOGETHER
                           STRING('Loan Deposit Paid'),AT(5313,21),USE(?LoanDepositPaidTitle),FONT(,8,,FONT:regular,CHARSET:ANSI), |
  HIDE,TRN
                           STRING(@n14.2b),AT(6396,21),USE(tmp:LoanDepositPaid),FONT(,8,,,CHARSET:ANSI),RIGHT,HIDE,TRN
                           STRING('Completed:'),AT(52,156),USE(?String66),FONT(,8),TRN
                           STRING(@D6b),AT(719,167),USE(job:Date_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING(@t1b),AT(1677,167),USE(job:Time_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('QA Passed:'),AT(52,313),USE(?qa_passed),FONT(,8),HIDE,TRN
                           STRING('Labour:'),AT(5313,167),USE(?labour_string),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,167),USE(labour_temp),FONT(,8),RIGHT,TRN
                           STRING(@s20),AT(2500,260,2552,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('Outgoing Courier:'),AT(52,573),USE(?String66:2),FONT(,8),TRN
                           STRING(@s20),AT(2917,469,1760,198),USE(job_number_temp),FONT('Tahoma',8,,FONT:bold),CENTER, |
  TRN
                           STRING('Carriage:'),AT(5313,458),USE(?carriage_string),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,313),USE(parts_temp),FONT(,8),RIGHT,TRN
                           STRING(@s20),AT(2500,729,2552,208),USE(barcodeIMEINumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('V.A.T.'),AT(5313,604),USE(?vat_String),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,458),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                           STRING(@d6b),AT(719,313),USE(job:Date_QA_Passed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('Total:'),AT(5313,781),USE(?total_string),FONT(,8,,FONT:bold),TRN
                           LINE,AT(6281,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6302,781),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('<128>'),AT(6250,781),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                           STRING('FAULT CODES'),AT(52,1146),USE(?String107),FONT(,8,,FONT:bold),TRN
                           STRING('Booking Option:'),AT(5313,938),USE(?BookingOption),FONT(,8,,FONT:bold),TRN
                           STRING(@s30),AT(6354,938,1406,156),USE(tmp:BookingOption),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                           STRING(@s30),AT(52,1302),USE(fault_code_field_temp[1]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1302,4844,208),USE(tmp:FaultCodeDescription[1]),FONT(,8),TRN
                           STRING(@s30),AT(52,1458),USE(fault_code_field_temp[2]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1458,4844,208),USE(tmp:FaultCodeDescription[2]),FONT(,8)
                           STRING(@s30),AT(52,1615),USE(fault_code_field_temp[3]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1615,4844,208),USE(tmp:FaultCodeDescription[3]),FONT(,8)
                           STRING(@s30),AT(52,1771),USE(fault_code_field_temp[4]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1771,4844,208),USE(tmp:FaultCodeDescription[4]),FONT(,8)
                           STRING(@s30),AT(52,1927),USE(fault_code_field_temp[5]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1927,4844,208),USE(tmp:FaultCodeDescription[5]),FONT(,8)
                           STRING(@s30),AT(52,2083),USE(fault_code_field_temp[6]),FONT(,8),TRN
                           STRING(@s255),AT(2135,2083,4844,208),USE(tmp:FaultCodeDescription[6]),FONT(,8)
                           STRING(@n14.2b),AT(6396,604),USE(vat_temp),FONT(,8),RIGHT,TRN
                           STRING(@s13),AT(990,729,1406,156),USE(jobe2:IDNumber),FONT(,8,,FONT:bold),TRN
                           STRING('Customer ID No:'),AT(52,729),USE(?String66:3),FONT(,8),TRN
                           STRING(@s15),AT(990,573,1563,156),USE(job:Courier),FONT(,8,,FONT:bold),TRN
                           STRING(@t1b),AT(1677,313),USE(job:Time_QA_Passed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING(@s30),AT(2760,938,2031,260),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING('Parts:'),AT(5313,313),USE(?parts_string),FONT(,8),TRN
                         END
                       END
                       FOOTER,AT(396,10271,7521,1156),USE(?Fault_Code9:2)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB CARD'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),FONT(,16,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),FONT(,8,,FONT:bold),TRN
                         STRING('COMPLETION DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobCard')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB CARD'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI') !! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('JobCard',ProgressWindow)                   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobCard',ProgressWindow)                ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
    job_Number_Temp = 'Job No: ' & clip(job:Ref_Number)
    esn_Temp = 'I.M.E.I.: ' & clip(job:ESN)
    barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
    barcodeIMEINumber = '*' & clip(job:ESN) & '*'

!  !Barcode Bit and setup refno
!  code_temp            = 1
!  option_temp          = 0
!  
!  bar_code_string_temp = Clip(job:ref_number)
!  !job_number_temp      = 'Job No: ' & Clip(job:ref_number)
!  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
!  
!  bar_code_string_temp = Clip(job:esn)
!  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
!  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
!  
!  job_number_temp = '*' & job:Ref_Number & '*'
!  
!  Settarget(Report)
!  Draw_JobNo.Init256()
!  Draw_JobNo.RenderWholeStrings = 1
!  Draw_JobNo.Resize(Draw_JobNo.Width * 5, Draw_JobNo.Height * 5)
!  Draw_JobNo.Blank(Color:White)
!  Draw_JobNo.FontName = 'C39 High 12pt LJ3'
!  Draw_JobNo.FontStyle = font:Regular
!  Draw_JobNo.FontSize = 24
!  Draw_JobNo.Show(0,0,Clip(job_Number_temp))
!  Draw_JobNo.Display()
!  Draw_JobNo.Kill256()
!  
!  drawer4.Init256()
!  drawer4.RenderWholeStrings = 1
!  drawer4.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer4.Resize(drawer4.Width * 5, drawer4.Height * 5)
!  drawer4.Blank(Color:White)
!  drawer4.FontName = 'C39 High 12pt LJ3'
!  drawer4.FontStyle = font:Regular
!  drawer4.FontSize = 24
!  drawer4.Show(0,0,Clip(job_Number_temp))
!  drawer4.Display()
!  drawer4.Kill256()
!
!  drawer6.Init256()
!  drawer6.RenderWholeStrings = 1
!  drawer6.Resize(drawer6.Width * 5, drawer6.Height * 5)
!  drawer6.Blank(Color:White)
!  drawer6.FontName = 'C39 High 12pt LJ3'
!  drawer6.FontStyle = font:Regular
!  drawer6.FontSize = 24
!  drawer6.Show(0,0,Bar_Code_Temp)
!  drawer6.Display()
!  drawer6.Kill256()
!!  
!  drawer7.Init256()
!  drawer7.RenderWholeStrings = 1
!  drawer7.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer7.Resize()
!  drawer7.Blank(Color:White)
!  drawer7.FontName = 'C39 High 12pt LJ3'
!  drawer7.FontStyle = font:Regular
!  drawer7.FontSize = 24
!  drawer7.Show(0,0,Clip(job_Number_temp))
!  drawer7.Display()
!  drawer7.Kill256()
!!  
!  drawer8.Init256()
!  drawer8.RenderWholeStrings = 1
!  !drawer8.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer8.Resize()
!  drawer8.Blank(Color:White)
!  drawer8.FontName = 'C39 High 12pt LJ3'
!  drawer8.FontStyle = font:Regular
!  drawer8.FontSize = 24
!  drawer8.Show(0,0,Clip(job_Number_temp))
!  drawer8.Display()
!  drawer8.Kill256()
!!  
!!  drawer9.Init256()
!!  drawer9.RenderWholeStrings = 1
!!  drawer9.Resize(drawer9.Width * 5, drawer9.Height * 5)
!!  drawer9.Blank(Color:White)
!!  drawer9.FontName = 'C39 High 12pt LJ3'
!!  drawer9.FontStyle = font:Regular
!!  drawer9.FontSize = 22
!!  drawer9.Show(2,2,Clip(job_Number_temp))
!!  drawer9.Display()
!!  drawer9.Kill256()
!!
!!  !Draw_IMEI.Init256()
!!  !Draw_IMEI.RenderWholeStrings = 1
!!  !Draw_IMEI.Resize(Draw_IMEI.Width * 5, Draw_IMEI.Height * 5)
!!  !Draw_IMEI.Blank(Color:White)
!!  !Draw_IMEI.FontName = 'C39 High 12pt LJ3'
!!  !Draw_IMEI.FontStyle = font:Regular
!!  !Draw_IMEI.FontSize = 48
!!  !Draw_IMEI.Show(0,0,Bar_Code2_Temp)
!!  !Draw_IMEI.Display()
!!  !Draw_IMEI.Kill256()
!  SetTarget()
!  
!  
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
  
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Engineer
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Engineer_Temp = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Engineer_Temp = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Who_Booked
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Who_Booked = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Who_Booked = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Despatched_User_Temp = ''
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  Settarget(Report)
  
  ! Show Engineering Option If Set - 4285 (DBH: 10-06-2004)
  Case jobe:Engineer48HourOption
  Of 1
      tmp:BookingOption         = '48 Hour Exchange'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 2
      tmp:BookingOption         = 'ARC Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 3
      tmp:BookingOption         = '7 Day TAT'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 4
      tmp:BookingOption         = 'Standard Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Else
      Case jobe:Booking48HourOption
      Of 1
          tmp:BookingOption = '48 Hour Exchange'
      Of 2
          tmp:BookingOption = 'ARC Repair'
      Of 3
          tmp:BookingOption = '7 Day TAT'
      Else
          tmp:BookingOption = 'N/A'
      End ! Case jobe:Booking48HourOption
  End ! Case jobe:Engineer48HourOption
  
  If job:warranty_job <> 'YES'
      Hide(?job:warranty_charge_type)
      Hide(?job:repair_type_warranty)
      Hide(?warranty_type)
      Hide(?Warranty_repair_Type)
  End! If job:warranty_job <> 'YES'
  
  If job:chargeable_job <> 'YES'
      Hide(?job:charge_type)
      Hide(?job:repair_type)
      Hide(?Chargeable_type)
      Hide(?repair_Type)
  End! If job:chargeable_job <> 'YES'
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      ! Found
      If man:UseQA
          UnHide(?qa_passed)
      End! If def:qa_required <> 'YES'
  
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  ! Error
  End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  
  If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
      Hide(?labour_string)
      Hide(?parts_string)
      Hide(?Carriage_string)
      Hide(?vat_string)
      Hide(?total_string)
      Hide(?line)
      Hide(?Euro)
  End! If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
  
  If job:estimate = 'YES' and job:Chargeable_Job = 'YES'
      Unhide(?estimate)
      estimate_value_temp = 'ESTIMATE REQUIRED IF REPAIR COST EXCEEDS ' & Clip(Format(job:estimate_if_over, @n14.2)) & ' (Plus V.A.T.)'
  End ! If
  
  
  
  
  
  !------------------------------------------------------------------
  IF CLIP(job:title) = ''
      customer_name_temp = job:initial
  ELSIF CLIP(job:initial) = ''
      customer_name_temp = job:title
  ELSE
      customer_name_temp = CLIP(job:title) & ' ' & CLIP(job:initial)
  END ! IF
  !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job:surname
  ELSIF CLIP(job:surname) = ''
  ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job:surname)
  END ! IF
  !------------------------------------------------------------------
  Settarget()
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  !**************************************************
  delivery_name_temp         = customer_name_temp
  delivery_company_Name_temp = job:Company_Name_Delivery
  
  delivery_address1_temp = job:address_line1_delivery
  delivery_address2_temp = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_telephone_temp     = job:Telephone_Delivery
  !**************************************************
  
  
  !**************************************************
  
  ! If an RRC job, then put the RRC as the invoice address,
  
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Access:WEBJOB.Clearkey(wob:RefNumberKey)
  !    wob:RefNumber   = job:Ref_Number
  !    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !        ! Found
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = wob:HeadAccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            ! Found
  !            invoice_address1_temp = tra:address_line1
  !            invoice_address2_temp = tra:address_line2
  !            If job:address_line3_delivery   = ''
  !                invoice_address3_temp = tra:postcode
  !                invoice_address4_temp = ''
  !            Else
  !                invoice_address3_temp = tra:address_line3
  !                invoice_address4_temp = tra:postcode
  !            End ! If
  !            invoice_company_name_temp     = tra:company_name
  !            invoice_telephone_number_temp = tra:telephone_number
  !            invoice_fax_number_temp       = tra:fax_number
  !            invoice_EMail_Address         = tra:EmailAddress
  !        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !        ! Error
  !        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !    ! Error
  !    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !
  !Else ! If ~glo:WebJob And jobe:WebJob
  
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      access:subtracc.fetch(sub:account_number_key)
  
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress  ! '' ! tra:EmailAddress
          ! MEssage('case 1')
          Else! If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = sub:address_line1
              invoice_address2_temp = sub:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = sub:address_line3
                  invoice_address4_temp = sub:postcode
              End ! If
              invoice_company_name_temp     = sub:company_name
              invoice_telephone_number_temp = sub:telephone_number
              invoice_fax_number_temp       = sub:fax_number
              invoice_EMail_Address         = sub:EmailAddress
  
          End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress ! '' ! tra:EmailAddress
  
          Else! If tra:invoice_customer_address = 'YES'
              !            UseAlternativeAddress# = 1
              invoice_address1_temp = tra:address_line1
              invoice_address2_temp = tra:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = tra:address_line3
                  invoice_address4_temp = tra:postcode
              End ! If
              invoice_company_name_temp     = tra:company_name
              invoice_telephone_number_temp = tra:telephone_number
              invoice_fax_number_temp       = tra:fax_number
              invoice_EMail_Address         = tra:EmailAddress
  
          End! If tra:invoice_customer_address = 'YES'
      end!!if tra:use_sub_accounts = 'YES'
  !End ! If ~glo:WebJob And jobe:WebJob
  
  !**************************************************
  
  
  !*************************set the display address to the head account if booked on as a web job***********************
  ! but only if this was not from a generic account
  
  
  !*********************************************************************************************************************
  
  
  !**************************************************
  If job:chargeable_job <> 'YES'
      labour_temp       = ''
      parts_temp        = ''
      courier_cost_temp = ''
      vat_temp          = ''
      total_temp        = ''
  Else! If job:chargeable_job <> 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job:invoice_number <> ''
          ! Total_Price('I',vat",total",balance")
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          if glo:webjob then
              If inv:ExportedRRCOracle
                  Labour_Temp = jobe:InvRRCCLabourCost
                  Parts_Temp  = jobe:InvRRCCPartsCost
                  Vat_Temp    = jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100 + |
                  jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100 +                 |
                  job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
                  courier_cost_temp = job:invoice_courier_cost
              Else ! If inv:ExportedRRCOracle
                  Labour_Temp = jobe:RRCCLabourCost
                  Parts_Temp  = jobe:RRCCPartsCost
                  Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
                  jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
                  job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
                  courier_cost_temp = job:courier_cost
              End ! If inv:ExportedRRCOracle
          else
              labour_temp = job:invoice_labour_cost
              Parts_temp  = job:Invoice_Parts_Cost
              Vat_temp    = job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100 + |
              job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100 +                 |
              job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
              courier_cost_temp = job:invoice_courier_cost
          end ! if
  
      Else! If job:invoice_number <> ''
          If glo:Webjob Then
              Labour_Temp = jobe:RRCCLabourCost
              Parts_Temp  = jobe:RRCCPartsCost
              Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
              jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          Else
              labour_temp = job:labour_cost
              parts_temp  = job:parts_cost
              Vat_Temp    = job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100 + |
              job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          End ! If
  
          courier_cost_temp = job:courier_cost
      End! If job:invoice_number <> ''
      Total_Temp  = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End! If job:chargeable_job <> 'YES'
  !**************************************************
  
  setcursor(cursor:wait)
  
  FieldNumber# = 1
  save_maf_id  = access:manfault.savefile()
  access:manfault.clearkey(maf:field_number_key)
  maf:manufacturer = job:manufacturer
  set(maf:field_number_key, maf:field_number_key)
  loop
      if access:manfault.next()
          break
      end ! if
      if maf:manufacturer <> job:manufacturer      |
          then break   ! end if
      end ! if
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
  
      If maf:Compulsory_At_Booking <> 'YES'
          Cycle
      End ! If maf:Compulsory_At_Booking <> 'YES'
  
      fault_code_field_temp[FieldNumber#] = maf:field_name
  
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
  
  
      Case maf:Field_Number
      Of 1
          mfo:Field = job:Fault_Code1
      Of 2
          mfo:Field = job:Fault_Code2
      Of 3
          mfo:Field = job:Fault_Code3
      Of 4
          mfo:Field = job:Fault_Code4
      Of 5
          mfo:Field = job:Fault_Code5
      Of 6
          mfo:Field = job:Fault_Code6
      Of 7
          mfo:Field = job:Fault_Code7
      Of 8
          mfo:Field = job:Fault_Code8
      Of 9
          mfo:Field = job:Fault_Code9
      Of 10
          mfo:Field = job:Fault_Code10
      Of 11
          mfo:Field = job:Fault_Code11
      Of 12
          mfo:Field = job:Fault_Code12
      ! Inserting (DBH 21/04/2006) #7551 - Display the extra fault codes, if comp at booking
      Of 13
          mfo:Field = wob:FaultCode13
      Of 14
          mfo:Field = wob:FaultCode14
      Of 15
          mfo:Field = wob:FaultCode15
      Of 16
          mfo:Field = wob:FaultCode16
      Of 17
          mfo:Field = wob:FaultCode17
      Of 18
          mfo:Field = wob:FaultCode18
      Of 19
          mfo:Field = wob:FaultCode19
      Of 20
          mfo:Field = wob:FaultCode20
      End ! Case maf:Field_Number
      ! End (DBH 21/04/2006) #7551
  
      tmp:FaultCodeDescription[FieldNumber#] = mfo:Field
  
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          ! Found
          tmp:FaultCodeDescription[FieldNumber#] = Clip(tmp:FaultCodeDescription[FieldNumber#]) & ' - ' & Clip(mfo:Description)
      Else! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
          ! Error
          ! Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  
      FieldNumber# += 1
      If FieldNumber# > 6
          Break
      End ! If FieldNumber# > 6
  end ! loop
  access:manfault.restorefile(save_maf_id)
  setcursor()
  
  tmp:accessories = ''
  setcursor(cursor:wait)
  x#          = 0
  Save_jac_ID = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged    = 0
  jac:Pirate     = 0
  Set(jac:DamagedPirateKey, jac:DamagedPirateKey)
  Loop
      If Access:JOBACC.NEXT()
          Break
      End ! If
      If jac:Ref_Number <> job:Ref_Number |
          Then Break
      End ! If
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
      x# += 1
      If jac:Damaged
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged
      If jac:Pirate
          jac:Accessory = 'PIRATE ' & Clip(jac:Accessory)
      End ! If jac:Pirate
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else! If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End! If tmp:accessories = ''
  End ! loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  If job:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end! if access:exchange.fetch(xch:ref_number_key) = Level:Benign
  Else! If job:exchange_unit_number <> ''
      x#          = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job:ref_number
      set(jea:part_number_key, jea:part_number_key)
      loop
          if access:jobexacc.next()
              break
          end ! if
          if jea:job_ref_number <> job:ref_number      |
              then break   ! end if
          end ! if
          x# = 1
          Break
      end ! loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End! If x# = 1
  End! If job:exchange_unit_number <> ''
  
  ! Check For Bouncer
  access:manufact.clearkey(man:manufacturer_key)
  man:manufacturer = job:manufacturer
  access:manufact.tryfetch(man:manufacturer_key)
  
  tmp:bouncers = ''
  If job:ESN <> 'N/A' And job:ESN <> ''
      setcursor(cursor:wait)
      save_job2_id = access:jobs2_alias.savefile()
      access:jobs2_alias.clearkey(job2:esn_key)
      job2:esn = job:esn
      set(job2:esn_key, job2:esn_key)
      loop
          if access:jobs2_alias.next()
              break
          end ! if
          if job2:esn <> job:esn      |
              then break   ! end if
          end ! if
          yldcnt# += 1
          if yldcnt# > 25
              yield()
              yldcnt# = 0
          end ! if
          If job2:Cancelled = 'YES'
              Cycle
          End ! If job2:Cancelled = 'YES'
  
          If job2:ref_number <> job:ref_number
  !            If job2:date_booked + man:warranty_period > job:date_booked And job2:date_booked <= job:date_booked
                  If tmp:bouncers = ''
                      tmp:bouncers = 'Previous Job Number(s): ' & job2:ref_number
                  Else! If tmp:bouncer = ''
                      tmp:bouncers = CLip(tmp:bouncers) & ', ' & job2:ref_number
                  End! If tmp:bouncer = ''
  !            End! If job2:date_booked + man:warranty_period < Today()
          End! If job2:esn <> job2:ref_number
      end ! loop
      access:jobs2_alias.restorefile(save_job2_id)
      setcursor()
  End ! job:ESN <> 'N/A' And job:ESN <> ''
  
  If tmp:bouncers <> ''
      Settarget(report)
      Unhide(?bouncertitle)
      Unhide(?tmp:bouncers)
      Settarget()
  End! If CheckBouncer(job:ref_number)
  
  If man:UseInvTextForFaults
      tmp:InvoiceText = ''
      Save_joo_ID     = Access:JOBOUTFL.SaveFile()
      Access:JOBOUTFL.ClearKey(joo:LevelKey)
      joo:JobNumber = job:Ref_Number
      joo:Level     = 10
      Set(joo:LevelKey, joo:LevelKey)
      Loop
          If Access:JOBOUTFL.PREVIOUS()
              Break
          End ! If
          If joo:JobNumber <> job:Ref_Number      |
              Then Break   ! End If
          End ! If
          If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          Else ! If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          End ! If tmp:InvoiceText = ''
      End ! Loop
      Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  Else ! man:UseInvTextForFaults
      tmp:InvoiceText = jbn:Invoice_Text
  End ! man:UseInvTextForFaults
  
  ! Loan unit information (Added by Gary (7th Aug 2002))
  ! //------------------------------------------------------------------------
  tmp:LoanModel       = ''
  tmp:LoanIMEI        = ''
  tmp:LoanAccessories = ''
  
  if job:Loan_Unit_Number <> 0                                            ! Is a loan unit attached to this job ?
  
      save_loa_id = Access:Loan.SaveFile()
      Access:Loan.Clearkey(loa:Ref_Number_Key)
      loa:Ref_Number  = job:Loan_Unit_Number
      if not Access:Loan.TryFetch(loa:Ref_Number_Key)                         ! Fetch loan unit record
          tmp:LoanModel        = clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI         = loa:ESN
          tmp:ReplacementValue = jobe:LoanReplacementValue
  
          save_lac_id = Access:LoanAcc.SaveFile()
          Access:LoanAcc.ClearKey(lac:ref_number_key)
          lac:ref_number = loa:ref_number
          set(lac:ref_number_key, lac:ref_number_key)
          loop until Access:LoanAcc.Next()                                    ! Fetch loan accessories
              if lac:ref_number <> loa:ref_number then break.
              if lac:Accessory_Status <> 'ISSUE' then cycle.                  ! Only display issued loan accessories
              if tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              else
                  tmp:LoanAccessories = clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              end ! if
  
          end ! loop
          Access:LoanAcc.RestoreFile(save_lac_id)
  
      end ! if
      Access:Loan.RestoreFile(save_loa_id)
  
  end ! if
  
  if tmp:LoanModel <> ''                                                      ! Display loan details on report
      SetTarget(report)
      Unhide(?LoanUnitIssued)
      Unhide(?tmp:LoanModel)
      Unhide(?IMEITitle)
      Unhide(?tmp:LoanIMEI)
      Unhide(?LoanAccessoriesTitle)
      Unhide(?tmp:LoanAccessories)
      Unhide(?tmp:ReplacementValue)
      Unhide(?LoanValueText)
      SetTarget()
  end ! if
  
  
  tmp:LoanDepositPaid = 0
  
  Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number   = job:Ref_Number
  jpt:Loan_Deposit = True
  set(jpt:Loan_Deposit_Key, jpt:Loan_Deposit_Key)
  loop until Access:JobPaymt.Next()                                           ! Loop through payments attached to job
      if jpt:Ref_Number <> job:Ref_Number then break.
      if jpt:Loan_Deposit <> True then break.
      tmp:LoanDepositPaid += jpt:Amount                                       ! Check for deposit value
  end ! loop
  
  if tmp:LoanDepositPaid <> 0
      SetTarget(report)
      Unhide(?LoanDepositPaidTitle)
      Unhide(?tmp:LoanDepositPaid)                                            ! Display fields if a deposit was found
      SetTarget()
  end ! if
  ! //------------------------------------------------------------------------
  
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName(loc:PDFName)
  SELF.SetDocumentInfo('Job Card','ServiceBase OnLine','ServiceBase OnLine','Job Card','ServiceBase OnLine','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = False
  SELF.CompressImages = False
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
JobReceipt PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
tmp:JobNumber        LONG                                  !Job Number
locTermsText         STRING(255)                           !
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(15)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !
VatNumber            STRING(30)                            !
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
Received_By_Temp     STRING(60)                            !Received_By_Temp
Amount_Received_Temp REAL                                  !Amount_Received_Temp
tmp:StandardText     STRING(255)                           !tmp:StandardText
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:Incoming_Consignment_Number)
                       PROJECT(job:Incoming_Courier)
                       PROJECT(job:Incoming_Date)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Transit_Type)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('Job Receipt'),AT(250,4323,7750,5813),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(323,438,7521,4115),USE(?unnamed),FONT('Tahoma')
                         STRING('Job Number: '),AT(5052,365),USE(?String25),FONT(,8),TRN
                         STRING(@s16),AT(5990,365),USE(tmp:Ref_number),FONT(,11,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(5052,677),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(5990,677),USE(job:date_booked),FONT(,8,,FONT:bold),TRN
                         STRING(@t1),AT(6875,677),USE(job:time_booked),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(1604,3240),USE(job:Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4635,3240),USE(job:Warranty_Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3125,3240),USE(job:Repair_Type),FONT(,8),TRN
                         STRING(@s20),AT(6146,3240),USE(job:Repair_Type_Warranty),FONT(,8),TRN
                         STRING('Model'),AT(156,3646),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1604,3646),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3125,3646),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4635,3646),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6146,3646),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4083,2344),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2344),USE(Delivery_Telephone_Temp),FONT(,8)
                         STRING(@s65),AT(4063,2500,2865,156),USE(clientName),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Order Number'),AT(156,3000),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(1604,3000),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(3125,3000),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Type'),AT(4635,3000),USE(?String40:5),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Repair Type'),AT(6146,3000),USE(?String40:6),FONT(,8,,FONT:bold),TRN
                         STRING('Tel:'),AT(2198,2031),USE(?String34:3),FONT(,8),TRN
                         STRING('Acc No:'),AT(2188,1563),USE(?String78),FONT(,8),TRN
                         STRING('Email:'),AT(156,2344),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(2667,2031),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2198,2188),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2667,2188),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2656,1563),USE(job:Account_Number,,?job:Account_Number:2),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(156,3240),USE(job:Order_Number),FONT(,8),TRN
                         STRING(@s30),AT(4083,1563),USE(delivery_Company_Name_temp),FONT(,8),TRN
                         STRING(@s50),AT(469,2344,3125,208),USE(invoice_EMail_Address),FONT(,8),TRN
                         STRING(@s30),AT(4083,1719,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,1875),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,2031),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,2188),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),FONT(,8),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:4)
DETAIL                   DETAIL,AT(0,0,,5646),USE(?DetailBand),FONT('Tahoma',8),TOGETHER
                           STRING(@s30),AT(229,0,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(1677,0,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(3198,0,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(4708,0,1396,156),USE(job:ESN),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(6219,0),USE(job:MSN),FONT(,8),TRN
                           STRING(@s15),AT(6219,365),USE(exchange_msn_temp),FONT(,8),TRN
                           STRING(@s16),AT(4708,365,1396,156),USE(exchange_esn_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(3198,365,1396,156),USE(exchange_unit_type_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(1677,365,1323,156),USE(exchange_manufacturer_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(229,365,1000,156),USE(exchange_model_number),FONT(,8),LEFT,TRN
                           STRING(@s15),AT(229,208),USE(exchange_unit_title_temp),FONT(,8,,FONT:bold),LEFT,TRN
                           STRING(@s20),AT(1354,208),USE(exchange_unit_number_temp),LEFT,TRN
                           STRING('ACCESSORIES'),AT(156,1042),USE(?String63),FONT(,8,,FONT:bold),TRN
                           TEXT,AT(1927,1042,5313,365),USE(tmp:accessories),FONT(,7),TRN
                           STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,1458),USE(?ExternalDamageCheckList),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Notes:'),AT(5990,1458),USE(?Notes),FONT(,7),TRN
                           TEXT,AT(6302,1458,1250,677),USE(jobe2:XNotes),FONT(,6),TRN
                           CHECK('None'),AT(5365,1458,625,156),USE(jobe2:XNone),TRN
                           CHECK('Keypad'),AT(156,1615,729,156),USE(jobe2:XKeypad),TRN
                           CHECK('Charger'),AT(2146,1615,781,156),USE(jobe2:XCharger),TRN
                           CHECK('Antenna'),AT(2146,1448,833,156),USE(jobe2:XAntenna),TRN
                           CHECK('Lens'),AT(2938,1448,573,156),USE(jobe2:XLens),TRN
                           CHECK('F/Cover'),AT(3563,1448,781,156),USE(jobe2:XFCover),TRN
                           CHECK('B/Cover'),AT(4448,1448,729,156),USE(jobe2:XBCover),TRN
                           CHECK('Battery'),AT(990,1615,729,156),USE(jobe2:XBattery),TRN
                           CHECK('LCD'),AT(2938,1615,625,156),USE(jobe2:XLCD),TRN
                           CHECK('System Connector'),AT(4448,1615,1250,156),USE(jobe2:XSystemConnector),TRN
                           CHECK('Sim Reader'),AT(3563,1604,885,156),USE(jobe2:XSimReader),TRN
                           STRING('Initial Transit Type: '),AT(156,4740,990,156),USE(?String66),FONT(,8),TRN
                           STRING(@s20),AT(1198,4740),USE(job:Transit_Type),FONT(,8,,FONT:bold),LEFT,TRN
                           STRING('Courier: '),AT(156,4896),USE(?String67),FONT(,8),TRN
                           STRING(@s15),AT(1198,4896,1302,188),USE(job:Incoming_Courier),FONT(,8,,FONT:bold),TRN
                           STRING('Consignment No:'),AT(156,5052),USE(?String70),FONT(,8),TRN
                           STRING(@s15),AT(1198,5052,1302,188),USE(job:Incoming_Consignment_Number),FONT(,8,,FONT:bold), |
  TRN
                           STRING('Customer ID No:'),AT(5573,5156),USE(?CustomerIDNo),FONT(,8),TRN
                           STRING('Date Received: '),AT(156,5208),USE(?String68),FONT(,8),TRN
                           STRING(@d6b),AT(1198,5208),USE(job:Incoming_Date),FONT(,8,,FONT:bold),TRN
                           STRING('Received By: '),AT(156,5365),USE(?String69),FONT(,8),TRN
                           STRING(@s15),AT(1198,5365),USE(Received_By_Temp),FONT(,8,,FONT:bold),TRN
                           STRING('REPORTED FAULT'),AT(156,573),USE(?String64),FONT(,8,,FONT:bold),TRN
                           TEXT,AT(1927,573,5313,417),USE(jbn:Fault_Description),FONT('Arial',7,,,CHARSET:ANSI),TRN
                           STRING(@s24),AT(3281,5365,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING('Booking Option:'),AT(5573,5313),USE(?String122),FONT(,8,,FONT:bold),TRN
                           STRING(@s30),AT(6563,5313,1563,156),USE(tmp:BookingOption),FONT(,8),TRN
                           STRING(@s13),AT(6563,5156),USE(jobe2:IDNumber),FONT(,8),TRN
                           STRING(@s20),AT(3281,4948,1760,198),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING(@s20),AT(2865,5156,2552,208),USE(barcodeIMEINumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White),TRN
                           STRING(@s20),AT(2865,4740,2552,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('Amount:'),AT(5573,4948),USE(?amount_text),FONT(,8),TRN
                           STRING('<128>'),AT(6510,4948,52,208),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                           STRING(@n-14.2b),AT(6615,4948),USE(Amount_Received_Temp),FONT(,8,,FONT:bold),TRN
                           STRING('Loan Deposit Paid'),AT(5573,4740),USE(?LoanDepositPaidTitle),FONT(,8,,,CHARSET:ANSI), |
  HIDE,TRN
                           STRING(@n-14.2b),AT(6667,4740),USE(tmp:LoanDepositPaid),FONT(,8,,FONT:bold,CHARSET:ANSI),HIDE, |
  TRN
                           TEXT,AT(208,2125,7333,2417),USE(stt:Text),TRN
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156),USE(?unnamed:2)
                         STRING('Estimate Required If Repair Costs Exceed:'),AT(156,63),USE(?estimate_text),FONT(,8), |
  HIDE,TRN
                         STRING(@s70),AT(2344,52,4115,260),USE(estimate_value_temp),FONT(,8),HIDE,TRN
                         TEXT,AT(83,83,10,10),USE(tmp:StandardText),FONT(,8),HIDE
                         STRING('LOAN UNIT ISSUED'),AT(156,271),USE(?LoanUnitIssued),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         STRING(@s30),AT(1906,271),USE(tmp:LoanModel),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  HIDE,TRN
                         STRING('IMEI'),AT(3969,271),USE(?IMEITitle),FONT('Arial',9,,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s30),AT(4292,271),USE(tmp:LoanIMEI),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  HIDE,TRN
                         STRING('LOAN ACCESSORIES'),AT(156,458),USE(?LoanAccessoriesTitle),FONT('Arial',8,,FONT:bold, |
  CHARSET:ANSI),HIDE,TRN
                         STRING(@s255),AT(1906,458,5417,208),USE(tmp:LoanAccessories),FONT('Arial',8,,FONT:regular, |
  CHARSET:ANSI),LEFT,HIDE,TRN
                         STRING('Customer Signature'),AT(156,885),USE(?String82),FONT(,,,FONT:bold),TRN
                         STRING('Date: {18}/ {19}/'),AT(4885,885,2219),USE(?String83),FONT(,,,FONT:bold),TRN
                         LINE,AT(1406,1042,3677,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(5573,1042,1521,0),USE(?Line2),COLOR(COLOR:Black)
                         TEXT,AT(156,615,5885,260),USE(locTermsText),FONT(,7)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB RECEIPT'),AT(5917,0,1521,240),USE(?String20),FONT(,16,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GOODS RECEIVED DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('PAYMENT RECEIVED (INC. V.A.T.)'),AT(5552,8563),USE(?String73:2),FONT(,8,,FONT:bold), |
  TRN
                         STRING('REPAIR DETAILS'),AT(156,3677),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2552,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5521,8698,2135,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobReceipt')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB RECEIPT'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('JobReceipt',ProgressWindow)                ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobReceipt',ProgressWindow)             ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
   Access:STANTEXT.ClearKey(stt:Description_Key)
   stt:Description = 'JOB RECEIPT'
   If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
       !Found
       tmp:StandardText = stt:Text
   Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
       !Error
   End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = job:Despatch_User
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
      Received_By_Temp = CLip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  
  Case jobe:Booking48HourOption
  Of 1
      tmp:BookingOption = '48 Hour Exchange'
  Of 2
      tmp:BookingOption = 'ARC Repair'
  Of 3
      tmp:BookingOption = '7 Day TAT'
  Else
      tmp:BookingOption = 'N/A'
  End ! Case jobe:Booking48HourOption
  
  If Clip(job:Title) = '' And Clip(job:Initial) = ''
      Customer_Name_Temp = job:Surname
  End ! If Clip(job:Title) = '' And Clip(job:Initial) = ''
  If Clip(job:Title) = '' And Clip(job:Initial) <> ''
      CUstomer_Name_Temp = Clip(job:Initial) & ' ' & Clip(job:Surname)
  End ! If Clip(job:Title) = '' And Clip(job:Initial) <> ''
  If Clip(job:Title) <> '' ANd CLip(job:Initial) <> ''
      Customer_Name_Temp = CLip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)
  End ! If Clip(job:Title) <> '' ANd CLip(job:Initial) <> ''
  
  If Customer_Name_Temp <> ''
      ClientName = 'Client: ' & Clip(CUstomer_Name_Temp)
      If jobe:EndUserTelNo <> ''
          CLientName = Clip(ClientName) & '  Tel: ' & jobe:EndUSerTelNo
      End ! If jobe:EndUserTelNo <> ''
  ELse
      If jobe:EndUserTelNo <> ''
          CLientName = 'Tel: ' & jobe:EndUSerTelNo
      End ! If jobe:EndUserTelNo <> ''
  End ! If Customer_Name_Temp <> ''
  
  delivery_Company_Name_temp = job:Company_Name_Delivery
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_Telephone_temp = job:Telephone_Delivery
  
  ! ---- Not relevant ----
  ! Deleting: DBH 29/01/2009 #10661
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Invoice_Address1_Temp = tra:Address_Line1
  !    Invoice_ADdress2_Temp = tra:Address_Line2
  !    Invoice_Address3_Temp = tra:Address_Line3
  !    Invoice_Address4_temp = tra:Postcode
  !    Invoice_Company_Name_Temp = tra:Company_Name
  !    Invoice_Telephone_Number_Temp = tra:Telephone_Number
  !    Invoice_Fax_Number_Temp = tra:Fax_Number
  !    Invoice_EMail_Address = tra:EmailAddress
  !Else ! If jobe:WebJob
  
  ! End: DBH 29/01/2009 #10661
  ! -----------------------------------------
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = job:Account_Number
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = job:Address_Line1
                      Invoice_ADdress2_Temp = job:Address_Line2
                      Invoice_Address3_Temp = job:Address_Line3
                      Invoice_Address4_temp = job:Postcode
                      Invoice_Company_Name_Temp = job:Company_Name
                      Invoice_Telephone_Number_Temp = job:Telephone_Number
                      Invoice_Fax_Number_Temp = job:Fax_Number
                      Invoice_EMail_Address = jobe:EndUserEmailAddress
                  Else ! If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = sub:Address_Line1
                      Invoice_ADdress2_Temp = sub:Address_Line2
                      Invoice_Address3_Temp = sub:Address_Line3
                      Invoice_Address4_temp = sub:Postcode
                      Invoice_Company_Name_Temp = sub:Company_Name
                      Invoice_Telephone_Number_Temp = sub:Telephone_Number
                      Invoice_Fax_Number_Temp = sub:Fax_Number
                      Invoice_EMail_Address = sub:EmailAddress
                  End ! If sub:Invoice_Customer_Address= 'YES'
              Else ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If tra:invoice_Customer_Address = 'YES'
                      Invoice_Address1_Temp = job:Address_Line1
                      Invoice_ADdress2_Temp = job:Address_Line2
                      Invoice_Address3_Temp = job:Address_Line3
                      Invoice_Address4_temp = job:Postcode
                      Invoice_Company_Name_Temp = job:Company_Name
                      Invoice_Telephone_Number_Temp = job:Telephone_Number
                      Invoice_Fax_Number_Temp = job:Fax_Number
                      Invoice_EMail_Address = jobe:EndUserEmailAddress
                  Else ! If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = tra:Address_Line1
                      Invoice_ADdress2_Temp = tra:Address_Line2
                      Invoice_Address3_Temp = tra:Address_Line3
                      Invoice_Address4_temp = tra:Postcode
                      Invoice_Company_Name_Temp = tra:Company_Name
                      Invoice_Telephone_Number_Temp = tra:Telephone_Number
                      Invoice_Fax_Number_Temp = tra:Fax_Number
                      Invoice_EMail_Address = tra:EmailAddress
                  End ! If tra:invoice_Customer_Address = 'YES'
              End ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  !End ! If jobe:WebJob
  
  If job:Exchange_Unit_Number > 0
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          Exchange_Unit_Number_Temp = 'Unit: ' & job:Exchange_Unit_Number
          Exchange_Model_Number    = xch:Model_NUmber
          Exchange_Manufacturer_Temp = xch:Manufacturer
          Exchange_Unit_Type_Temp = 'N/A'
          Exchange_ESN_Temp = xch:ESN
          Exchange_Unit_Title_Temp = 'EXCHANGE UNIT'
          Exchange_MSN_Temp = xch:MSN
      Else ! If Access:EXCHANGE.TryFetch(xch:Ref_NumberKey) = Level:Benign
          !Error
      End ! If Access:EXCHANGE.TryFetch(xch:Ref_NumberKey) = Level:Benign
  End ! If job:Exchange_Unit_Number > 0
  
  tmp:Accessories = ''
  Access:JOBACC.Clearkey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged = 0
  Set(jac:DamagedKey,jac:DamagedKey)
  Loop ! Begin Loop
      If Access:JOBACC.Next()
          Break
      End ! If Access:JOBACC.Next()
      If jac:Ref_Number <> job:Ref_Number
          Break
      End ! If jac:Ref_Number <> job:Ref_Number
  
      If jac:Damaged = 1
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged = 1
  
      If tmp:Accessories = ''
          tmp:Accessories = jac:Accessory
      Else ! If tmp:Accessories = ''
          tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
      End ! If tmp:Accessories = ''
  End ! Loop
  
  If job:Estimate = 'YES'
      Settarget(Report)
      ?Estimate_Text{prop:Hide} = 0
      ?Estimate_Value_Temp{prop:Hide} = 0
      Estimate_Value_Temp = Clip(Format(job:Estimate_If_Over,@n14.2)) & ' (Plus VAT)'
      SetTarget()
  End ! If job:Estimate = 'YES'
  
  If job:Chargeable_Job = 'YES'
      Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
      jpt:Ref_Number = job:Ref_Number
      Set(jpt:All_Date_Key,jpt:All_Date_Key)
      Loop ! Begin Loop
          If Access:JOBPAYMT.Next()
              Break
          End ! If Access:JOBPAYMT.Next()
          If jpt:Ref_Number <> job:Ref_Number
              Break
          End ! If jpt:Ref_Number <> job:Ref_Number
          Amount_Received_Temp += jpt:Amount
      End ! Loop
  Else ! If job:Chargeable_Job = 'YES'
      SetTarget(Report)
      ?Amount_Text{prop:Hide} = 1
      ?job:Charge_Type{prop:Hide} = 1
      ?String40:3{prop:Hide} = 1
      SetTarget()
  End ! If job:Chargeable_Job = 'YES'
  
  tmp:LoanModel = ''
  tmp:LoanIMEI = ''
  tmp:LoanAccessories = ''
  
  If job:Loan_Unit_Number > 0
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = job:Loan_Unit_Number
      If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
          !Found
          tmp:LoanModel = Clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI = loa:ESN
  
          Access:LOANACC.Clearkey(lac:Ref_Number_Key)
          lac:Ref_Number = loa:Ref_Number
          Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
          Loop ! Begin Loop
              If Access:LOANACC.Next()
                  Break
              End ! If Access:LOANACC.Next()
              If lac:Ref_Number <> loa:Ref_Number
                  Break
              End ! If lac:Ref_Number <> loa:Ref_Number
              If lac:Accessory_Status <> 'ISSUE'
                  Cycle
              End ! If lac:Accessory_Status <> 'ISSUE'
              If tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              Else ! If tmp:LoanAccessories = ''
                  tmp:LoanAccessories = Clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              End ! If tmp:LoanAccessories = ''
          End ! Loop
  
          Access:STANTEXT.ClearKey(stt:Description_Key)
          stt:Description = 'LOAN DISCLAIMER'
          If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
              !Found
              tmp:StandardText = Clip(tmp:StandardText) & '<13,10>' & stt:Text
          Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
              !Error
          End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
      Else ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
          !Error
      End ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
  
  End ! If job:Loan_Unit_Number > 0
  
  If tmp:LoanModel <> ''
      SetTarget(Report)
      ?LoanUnitIssued{prop:Hide} = 0
      ?tmp:LoanModel{prop:Hide} = 0
      ?IMEITitle{prop:Hide} = 0
      ?tmp:LoanIMEI{prop:Hide} = 0
      ?LoanAccessoriesTitle{prop:Hide} = 0
      ?tmp:LOanAccessories{prop:Hide} = 0
  End ! If tmp:LoanModel <> ''
  
  tmp:LoanDepositPaid = 0
  
  Access:JOBPAYMT.Clearkey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number = job:Ref_Number
  jpt:Loan_Deposit = 1
  Set(jpt:Loan_Deposit_Key,jpt:Loan_Deposit_Key)
  Loop ! Begin Loop
      If Access:JOBPAYMT.Next()
          Break
      End ! If Access:JOBPAYMT.Next()
      If jpt:Ref_Number <> job:Ref_Number
          Break
      End ! If jpt:Ref_Number <> job:Ref_Number
      If jpt:Loan_Deposit <> 1
          Break
      End ! If jpt:Loan_Deposit <> 1
      tmp:LoanDepositPaid += jpt:Amount
  End ! Loop
  
  If tmp:LoanDepositPaid > 0
      SetTarget(Report)
      ?LoanDepositPaidTitle{prop:Hide} = 0
      ?tmp:LoanDepositPaid{prop:Hide} = 0
      SetTarget()
  End ! If tmp:LoanDepositPaid > 0
  
  job_number_temp      = 'Job No: ' & job:ref_number
  esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
  barcodeIMEINumber = '*' & clip(job:ESN) & '*'
  
  !!Barcode Bit
  !code_temp   = 3
  !option_temp = 0
  !
  !bar_code_string_temp = job:ref_number
  !job_number_temp      = 'Job No: ' & job:ref_number
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  !
  !bar_code_string_temp = Clip(job:esn)
  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  !
  !SetTarget(Report)
  !DR_JobNo.Init256()
  !DR_JobNo.RenderWholeStrings = 1
  !DR_JobNo.Resize(DR_JobNo.Width * 5, DR_JobNo.Height * 5)
  !DR_JobNo.Blank(Color:White)
  !DR_JobNo.FontName = 'C128 High 12pt LJ3'
  !DR_JobNo.FontStyle = font:Regular
  !DR_JobNo.FontSize = 48
  !DR_JobNo.Show(0,0,Bar_Code_Temp)
  !DR_JobNo.Display()
  !DR_JobNo.Kill256()
  !
  !DR_IMEI.Init256()
  !DR_IMEI.RenderWholeStrings = 1
  !DR_IMEI.Resize(DR_IMEI.Width * 5, DR_IMEI.Height * 5)
  !DR_IMEI.Blank(Color:White)
  !DR_IMEI.FontName = 'C128 High 12pt LJ3'
  !DR_IMEI.FontStyle = font:Regular
  !DR_IMEI.FontSize = 48
  !DR_IMEI.Show(0,0,Bar_Code2_Temp)
  !DR_IMEI.Display()
  !DR_IMEI.Kill256()
  !SetTarget()
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','JobCard','JobCard','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

LocationChange       PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Previous_Loc         STRING(30)                            !
FilesOpened     BYTE(0)
  CODE
    do openFiles
    if (p_web.GSV('LocationChange:Location') <> '')
        if (Access:LOCATLOG.PrimeRecord() = Level:Benign)
            lot:refNumber   = p_web.GSV('job:Ref_Number')
            lot:theDate     = today()
            lot:theTime     = clock()
            lot:userCode    = p_web.GSV('BookingUserCode')
            lot:PreviousLocation    = p_web.GSV('job:Location')
            if (lot:PreviousLocation = '')
                lot:PreviousLocation = 'N/A'
            end !if (lot:PreviousLocation = '')
            lot:NewLocation = p_web.GSV('LocationChange:Location')
           
            if (Access:LOCATLOG.TryInsert() = Level:Benign)
                ! Inserted
            else ! if (Access:LOCATLOG.TryInsert() = Level:Benign)
                ! Error
                access:LOCATLOG.cancelautoinc()
            end ! if (Access:LOCATLOG.TryInsert() = Level:Benign)
        end ! if (Access:LOCATLOG.PrimeRecord() = Level:Benign)
    end ! if (p_web.GSV('LocationChange:Location') <> '')

    p_web.SSV('job:Location',p_web.GSV('LocationChange:Location'))

    p_web.deleteSessionValue('LocationChange:Location')

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     FilesOpened = False
  END
LookupProductCodes   PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MODPROD)
                      Project(mop:RecordNumber)
                      Project(mop:ProductCode)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('LookupProductCodes')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupProductCodes:NoForm')
      loc:NoForm = p_web.GetValue('LookupProductCodes:NoForm')
      loc:FormName = p_web.GetValue('LookupProductCodes:FormName')
    else
      loc:FormName = 'LookupProductCodes_frm'
    End
    p_web.SSV('LookupProductCodes:NoForm',loc:NoForm)
    p_web.SSV('LookupProductCodes:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupProductCodes:NoForm')
    loc:FormName = p_web.GSV('LookupProductCodes:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupProductCodes') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupProductCodes')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MODPROD,mop:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MOP:PRODUCTCODE') then p_web.SetValue('LookupProductCodes_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupProductCodes:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupProductCodes:LookupFrom','LookupFrom')
    p_web.StoreValue('LookupProductCodes:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupProductCodes:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupProductCodes:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    If p_web.IfExistsValue('LookupJobType')
      p_web.StoreValue('LookupJobType')
    End
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 22
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupProductCodes_sort',net:DontEvaluate)
  p_web.SetSessionValue('LookupProductCodes_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mop:ProductCode)','-UPPER(mop:ProductCode)')
    Loc:LocateField = 'mop:ProductCode'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mop:ModelNumber),+UPPER(mop:ProductCode)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mop:ProductCode')
    loc:SortHeader = p_web.Translate('Product Code')
    p_web.SetSessionValue('LookupProductCodes_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('LookupProductCodes:LookupFrom')
  End!Else
  loc:formaction = 'LookupProductCodes'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupProductCodes:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupProductCodes:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupProductCodes:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MODPROD"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mop:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Product Code') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Product Code',0)&'</span>'&CRLF
  End
  If clip('Select Product Code') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupProductCodes',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2LookupProductCodes',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupProductCodes.locate(''Locator2LookupProductCodes'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupProductCodes.cl(''LookupProductCodes'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="LookupProductCodes_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="LookupProductCodes_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','LookupProductCodes','Product Code','Click here to sort by Product Code',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Product Code')&'">'&p_web.Translate('Product Code')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,22,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mop:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MODPROD{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mop:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mop:RecordNumber'),p_web.GetValue('mop:RecordNumber'),p_web.GetSessionValue('mop:RecordNumber'))
      loc:FilterWas = 'Upper(mop:ModelNumber) = Upper(<39>' & p_web.GetSessionValue('job:Model_Number') & '<39>)'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupProductCodes',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupProductCodes_Filter')
    p_web.SetSessionValue('LookupProductCodes_FirstValue','')
    p_web.SetSessionValue('LookupProductCodes_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MODPROD,mop:RecordNumberKey,loc:PageRows,'LookupProductCodes',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MODPROD{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MODPROD,loc:firstvalue)
              Reset(ThisView,MODPROD)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MODPROD{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MODPROD,loc:lastvalue)
            Reset(ThisView,MODPROD)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mop:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupProductCodes.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupProductCodes.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupProductCodes.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupProductCodes.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupProductCodes',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupProductCodes_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupProductCodes_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1LookupProductCodes',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupProductCodes.locate(''Locator1LookupProductCodes'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupProductCodes.cl(''LookupProductCodes'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('LookupProductCodes_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupProductCodes_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupProductCodes.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupProductCodes.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupProductCodes.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupProductCodes.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mop:RecordNumber
    p_web._thisrow = p_web._nocolon('mop:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupProductCodes:LookupField')) = mop:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mop:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="LookupProductCodes.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MODPROD{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MODPROD)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MODPROD{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MODPROD)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mop:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="LookupProductCodes.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mop:RecordNumber',clip(loc:field),,'checked',,,'onclick="LookupProductCodes.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mop:ProductCode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="LookupProductCodes.omv(this);" onMouseOut="LookupProductCodes.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var LookupProductCodes=new browseTable(''LookupProductCodes'','''&clip(loc:formname)&''','''&p_web._jsok('mop:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mop:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'LookupProductCodes.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'LookupProductCodes.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupProductCodes')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupProductCodes')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupProductCodes')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupProductCodes')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MODPROD)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MODPROD)
  Bind(mop:Record)
  Clear(mop:Record)
  NetWebSetSessionPics(p_web,MODPROD)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mop:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&mop:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupProductCodes',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupProductCodes',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupProductCodes',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mop:ProductCode   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mop:ProductCode_'&mop:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mop:ProductCode,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mop:ProductCodeKey)
    loc:Invalid = 'mop:ModelNumber'
    loc:Alert = clip(p_web.site.DuplicateText) & ' ProductCodeKey --> mop:ModelNumber, '&clip('Product Code')&''
  End
PushDefaultSelection  Routine
  loc:default = mop:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mop:RecordNumber',mop:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mop:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mop:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mop:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
LookupRRCAccounts    PROCEDURE  (NetWebServerWorker p_web)
BookingAccount       STRING(30)                            !BookingAccount
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SUBTRACC)
                      Project(sub:RecordNumber)
                      Project(sub:Account_Number)
                      Project(sub:Company_Name)
                      Project(sub:Branch)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('LookupRRCAccounts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupRRCAccounts:NoForm')
      loc:NoForm = p_web.GetValue('LookupRRCAccounts:NoForm')
      loc:FormName = p_web.GetValue('LookupRRCAccounts:FormName')
    else
      loc:FormName = 'LookupRRCAccounts_frm'
    End
    p_web.SSV('LookupRRCAccounts:NoForm',loc:NoForm)
    p_web.SSV('LookupRRCAccounts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupRRCAccounts:NoForm')
    loc:FormName = p_web.GSV('LookupRRCAccounts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupRRCAccounts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupRRCAccounts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SUBTRACC,sub:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SUB:ACCOUNT_NUMBER') then p_web.SetValue('LookupRRCAccounts_sort','1')
    ElsIf (loc:vorder = 'SUB:ACCOUNT_NUMBER') then p_web.SetValue('LookupRRCAccounts_sort','1')
    ElsIf (loc:vorder = 'SUB:COMPANY_NAME') then p_web.SetValue('LookupRRCAccounts_sort','2')
    ElsIf (loc:vorder = 'SUB:COMPANY_NAME') then p_web.SetValue('LookupRRCAccounts_sort','2')
    ElsIf (loc:vorder = 'SUB:BRANCH') then p_web.SetValue('LookupRRCAccounts_sort','4')
    ElsIf (loc:vorder = 'SUB:BRANCH') then p_web.SetValue('LookupRRCAccounts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupRRCAccounts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupRRCAccounts:LookupFrom','LookupFrom')
    p_web.StoreValue('LookupRRCAccounts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupRRCAccounts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupRRCAccounts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupRRCAccounts_sort',net:DontEvaluate)
  p_web.SetSessionValue('LookupRRCAccounts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 5
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sub:Account_Number)','-UPPER(sub:Account_Number)')
    Loc:LocateField = 'sub:Account_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sub:Company_Name)','-UPPER(sub:Company_Name)')
    Loc:LocateField = 'sub:Company_Name'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sub:Branch)','-UPPER(sub:Branch)')
    Loc:LocateField = 'sub:Branch'
  end
  if loc:vorder = ''
    loc:nobuffer = 1
    Loc:LocateField = 'sub:Account_Number'
    loc:sortheader = 'Account Number'
    loc:vorder = '+sub:Account_Number,+sub:Company_Name,+sub:Branch'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 1)
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 2)
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sub:Account_Number')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('LookupRRCAccounts_LocatorPic','@s15')
  Of upper('sub:Company_Name')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('LookupRRCAccounts_LocatorPic','@s30')
  Of upper('sub:Branch')
    loc:SortHeader = p_web.Translate('Branch')
    p_web.SetSessionValue('LookupRRCAccounts_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('LookupRRCAccounts:LookupFrom')
  End!Else
  loc:formaction = 'LookupRRCAccounts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupRRCAccounts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupRRCAccounts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupRRCAccounts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SUBTRACC"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sub:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Account') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Account',0)&'</span>'&CRLF
  End
  If clip('Select Account') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupRRCAccounts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2LookupRRCAccounts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupRRCAccounts.locate(''Locator2LookupRRCAccounts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupRRCAccounts.cl(''LookupRRCAccounts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="LookupRRCAccounts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="LookupRRCAccounts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','LookupRRCAccounts','Account Number','Click here to sort by Account Number',,,200,1)
        Else
          packet = clip(packet) & '<th width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','LookupRRCAccounts','Company Name','Click here to sort by Company Name',,,300,1)
        Else
          packet = clip(packet) & '<th width="'&clip(300)&'" Title="'&p_web.Translate('Click here to sort by Company Name')&'">'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','LookupRRCAccounts','Branch','Click here to sort by Branch',,,280,1)
        Else
          packet = clip(packet) & '<th width="'&clip(280)&'" Title="'&p_web.Translate('Click here to sort by Branch')&'">'&p_web.Translate('Branch')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sub:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SUBTRACC{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sub:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sub:RecordNumber'),p_web.GetValue('sub:RecordNumber'),p_web.GetSessionValue('sub:RecordNumber'))
  If False  ! Generate Filter
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 1)
      loc:FilterWas = 'Upper(sub:Main_Account_Number) = Upper(''' & P_web.GetSessionValue('BookingAccount') & ''')'
  ElsIf (p_web.GetSessionValue('FranchiseAccount') = 2)
      loc:FilterWas = 'Upper(sub:Generic_Account) = 1'
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupRRCAccounts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupRRCAccounts_Filter')
    p_web.SetSessionValue('LookupRRCAccounts_FirstValue','')
    p_web.SetSessionValue('LookupRRCAccounts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SUBTRACC,sub:RecordNumberKey,loc:PageRows,'LookupRRCAccounts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SUBTRACC{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SUBTRACC,loc:firstvalue)
              Reset(ThisView,SUBTRACC)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SUBTRACC{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SUBTRACC,loc:lastvalue)
            Reset(ThisView,SUBTRACC)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sub:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupRRCAccounts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupRRCAccounts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupRRCAccounts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupRRCAccounts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupRRCAccounts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupRRCAccounts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupRRCAccounts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1LookupRRCAccounts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupRRCAccounts.locate(''Locator1LookupRRCAccounts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupRRCAccounts.cl(''LookupRRCAccounts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('LookupRRCAccounts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupRRCAccounts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupRRCAccounts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupRRCAccounts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupRRCAccounts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupRRCAccounts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sub:RecordNumber
    p_web._thisrow = p_web._nocolon('sub:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupRRCAccounts:LookupField')) = sub:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sub:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="LookupRRCAccounts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SUBTRACC{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SUBTRACC)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SUBTRACC{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SUBTRACC)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sub:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="LookupRRCAccounts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sub:RecordNumber',clip(loc:field),,'checked',,,'onclick="LookupRRCAccounts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sub:Account_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(300)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sub:Company_Name
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(280)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sub:Branch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="LookupRRCAccounts.omv(this);" onMouseOut="LookupRRCAccounts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var LookupRRCAccounts=new browseTable(''LookupRRCAccounts'','''&clip(loc:formname)&''','''&p_web._jsok('sub:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sub:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'LookupRRCAccounts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'LookupRRCAccounts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupRRCAccounts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupRRCAccounts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupRRCAccounts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupRRCAccounts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SUBTRACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  Clear(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sub:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&sub:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupRRCAccounts',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupRRCAccounts',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'LookupRRCAccounts',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sub:Account_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sub:Account_Number_'&sub:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sub:Account_Number,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sub:Company_Name   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sub:Company_Name_'&sub:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sub:Company_Name,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sub:Branch   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sub:Branch_'&sub:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sub:Branch,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(sub:Main_Account_Key)
    loc:Invalid = 'sub:Main_Account_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Main_Account_Key --> sub:Main_Account_Number, '&clip('Account Number')&''
  End
  If Duplicate(sub:Account_Number_Key)
    loc:Invalid = 'sub:Account_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Account_Number_Key --> '&clip('Account Number')&' = ' & clip(sub:Account_Number)
  End
PushDefaultSelection  Routine
  loc:default = sub:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sub:RecordNumber',sub:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sub:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sub:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sub:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
