  Section('_SendFile')

  Section('_SendFileCase')
  do CaseStart:WebServer_Phase2a
  If loc:Done then Return.

  Section('_SendFileRoutines')
CaseStart:WebServer_Phase2a  routine
  do Case:FinishBatch
  do Case:CreateCreditNote
  do Case:IndividualDespatch
  do Case:JobEstimate
  do Case:ExchangeOrder
  do Case:PreNewJobBooking
  do Case:FormContactHistory
  do Case:FormBrowseLocationHistory
  do Case:SelectEngineers
  do Case:LoginForm
  do Case:FormBrowseContactHistory
  do Case:BrowseContactHistory
  do Case:TagValidateLoanAccessories
  do Case:BrowseJobsInBatch
  do Case:LookupRRCAccounts
  do Case:BrowseLoanUnits
  do Case:LookupSuburbs
  do Case:MultipleBatchDespatch
  do Case:SelectColours
  do Case:VodacomSingleInvoice
  do Case:IndexPage
  do Case:ViewJob
  do Case:BrowseJobCredits
  do Case:BrowseChargeableParts
  do Case:JobAccessories
  do Case:BrowseEstimateParts
  do Case:SelectAccessJobStatus
  do Case:SendSMS
  do Case:DisplayOutFaults
  do Case:BrowseJobOutFaults
  do Case:PickEngineersNotes
  do Case:LookupGenericAccounts
  do Case:BouncerChargeableParts
  do Case:FormExchangeUnitFilter
  do Case:BrowseSubAddresses
  do Case:BrowseWarrantyParts
  do Case:FormDeletePart
  do Case:ProofOfPurchase
  do Case:CreateNewBatch
  do Case:FormWarrantyParts
  do Case:PickPaperwork
  do Case:BannerNewJobBooking
  do Case:DespatchNote
  do Case:AddToBatch
  do Case:BrowseModelNumbers
  do Case:FormRepairNotes
  do Case:LookupProductCodes
  do Case:BrowsePayments
  do Case:Estimate
  do Case:JobFaultCodes
  do Case:SelectCouriers
  do Case:RefreshPage
  do Case:SelectChargeTypes
  do Case:AmendAddress
  do Case:BrowseAuditFilter
  do Case:PickLoanUnit
  do Case:JobSearch
  do Case:ClearAddressDetails
  do Case:OBFValidation
  do Case:SendAnEmail
  do Case:BrowseStockPartFaultCodeLookup
  do Case:FormPayments
  do Case:FormAddToBatch
  do Case:BrowseLocationHistory
  do Case:BrowseStatusChanges
  do Case:FormPickModelStockLocation
  do Case:InsertJob_Finished
  do Case:ViewCosts
  do Case:BrowseEngineerHistory
  do Case:SelectNetworks
  do Case:BannerBlank
  do Case:BrowseOutFaultCodes
  do Case:SelectUnitTypes
  do Case:PickExchangeUnit
  do Case:BannerMainMenu
  do Case:BouncerWarrantyParts
  do Case:BrowseModelStock
  do Case:PageFooter
  do Case:BouncerOutFaults
  do Case:PickAccessory
  do Case:BrowseJobFaultCodeLookup
  do Case:FormBrowseEngineerHistory
  do Case:InvoiceCreated
  do Case:BrowseIMEIHistory
  do Case:JobReceipt
  do Case:BrowseExchangeUnits
  do Case:ViewBouncerJob
  do Case:PrintRoutines
  do Case:BrowsePartFaultCodeLookup
  do Case:BannerEngineeringDetails
  do Case:JobEstimateQuery
  do Case:NewJobBooking
  do Case:ViewChargeableParts
  do Case:LookupJobStatus
  do Case:SelectModelNumbers
  do Case:BrowseStatusChangesFilter
  do Case:SetJobType
  do Case:BannerBrowseJobs
  do Case:BrowseOutFaultsChargeableParts
  do Case:InvoiceNote
  do Case:ViewEstimateParts
  do Case:Waybill
  do Case:BrowseSMSHistory
  do Case:CreateInvoice
  do Case:ViewWarrantyParts
  do Case:BrowseBatchesInProgress
  do Case:SelectSuppliers
  do Case:DespatchConfirmation
  do Case:ReceiptFromPUP
  do Case:FormChargeableParts
  do Case:FormAccessoryNumbers
  do Case:BannerVodacom
  do Case:FormEngineeringOption
  do Case:AllocateEngineer
  do Case:FormEstimateParts
  do Case:formFilterModelNumber
  do Case:BrowseRepairNotes
  do Case:BrowseEngineersOnJob
  do Case:BrowseAccessoryNumber
  do Case:CloseWebPage
  do Case:BrowseOutFaultsWarrantyParts
  do Case:DisplayBrowsePayments
  do Case:BrowseOutFaultsEstimateParts
  do Case:FormJobOutFaults
  do Case:FormChangeDOP
  do Case:BrowseStatusFilter
  do Case:BouncerHistory
  do Case:BillingConfirmation
  do Case:SelectManufacturers
  do Case:SelectTransitTypes
  do Case:BrowseAuditTrail
  do Case:FormLoanUnitFilter
  do Case:PageHeader
  do Case:JobCard
  do Case:SetHubRepair
  do Case:PickContactHistoryNotes
  do Case:SelectFaultCodes
  do Case:FormJobsInBatch

! - -  - - - - - - - - - - - - - -
Case:FinishBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'finishbatch'
  orof 'finish batch'
    Self.MakePage('FinishBatch',Net:Web:Form,Net:Login,'Finish Batch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('finishbatch_locwaybillnumber_value')
    FinishBatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('finishbatch_buttonfinishbatch_value')
    FinishBatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CreateCreditNote  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'createcreditnote'
  orof 'create credit note'
    Self.MakePage('CreateCreditNote',Net:Web:Form,Net:Login,'Create Credit Note',,)
    loc:Done = 1 ; Exit
  of self._nocolon('createcreditnote_loccredittype_value')
    CreateCreditNote(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('createcreditnote_loccreditamount_value')
    CreateCreditNote(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('createcreditnote_buttoncreatecreditnote_value')
    CreateCreditNote(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:IndividualDespatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'individualdespatch'
  orof 'individual despatch'
    Self.MakePage('IndividualDespatch',Net:Web:Form,Net:Login,'Individual Despatch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locjobnumber_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locimeinumber_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonvalidatejobdetails_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_tagvalidateloanaccessories_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonvalidateaccessories_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locaccessorypassword_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonconfirmmismatch_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonfailaccessory_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_cou:courier_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locconsignmentnumber_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locsecuritypackid_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonconfirmdespatch_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobEstimate  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobestimate'
  orof 'estimatedetails'
    Self.MakePage('JobEstimate',Net:Web:Form,0,'Estimate Details',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_if_over_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_ready_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_accepted_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_rejected_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestaccby_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestacccommunicationmethod_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestrejby_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestrejcommunicationmethod_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestrejreason_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ExchangeOrder  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'exchangeorder'
  orof 'exchangeorder' & '.pdf'
    ExchangeOrder(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PreNewJobBooking  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'prenewjobbooking'
  orof 'newjobbooking.htm'
    Self.MakePage('PreNewJobBooking',Net:Web:Form,Net:Login,'New Job Booking','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:colour_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:ordernumber_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:faultcode_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:engineersnotes_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:transittype_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:esn_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_buttonvalidateimei_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:manufacturer_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:modelnumber_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormContactHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formcontacthistory'
  orof 'insert / amend contact history'
    Self.MakePage('FormContactHistory',Net:Web:Form,Net:Login,'Insert / Amend Contact History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formcontacthistory_cht:action_value')
    FormContactHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formcontacthistory_cht:notes_value')
    FormContactHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormBrowseLocationHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formbrowselocationhistory'
  orof 'location history'
    Self.MakePage('FormBrowseLocationHistory',Net:Web:Form,Net:Login,'Location History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formbrowselocationhistory_browselocationhistory_value')
    FormBrowseLocationHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectEngineers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectengineers'
  orof 'selectengineers' & '_' & loc:parent
  orof 'select engineer'
  orof 'select engineer' & '_' & loc:parent
    Self.MakePage('SelectEngineers',Net:Web:Browse,Net:Login,'Select Engineer',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LoginForm  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'loginform'
  orof 'sblogin.htm'
    Self.MakePage('LoginForm',Net:Web:Form,0,'ServiceBase Login','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_loc:branchid_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_loc:password_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_locnewpassword_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_locconfirmnewpassword_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormBrowseContactHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formbrowsecontacthistory'
  orof 'contact history'
    Self.MakePage('FormBrowseContactHistory',Net:Web:Form,Net:Login,'Contact History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formbrowsecontacthistory_browsecontacthistory_value')
    FormBrowseContactHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseContactHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsecontacthistory'
  orof 'browsecontacthistory' & '_' & loc:parent
  orof 'browse contact history'
  orof 'browse contact history' & '_' & loc:parent
    Self.MakePage('BrowseContactHistory',Net:Web:Browse,Net:Login,'Browse Contact History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:TagValidateLoanAccessories  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'tagvalidateloanaccessories'
  orof 'tagvalidateloanaccessories' & '_' & loc:parent
  orof 'tagvalidateloanaccessories' & '_' & loc:parent
    Self.MakePage('TagValidateLoanAccessories',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobsInBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejobsinbatch'
  orof 'browsejobsinbatch' & '_' & loc:parent
  orof 'browse jobs in batch'
  orof 'browse jobs in batch' & '_' & loc:parent
    Self.MakePage('BrowseJobsInBatch',Net:Web:Browse,Net:Login,'Browse Jobs In Batch',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupRRCAccounts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookuprrcaccounts'
  orof 'lookuprrcaccounts' & '_' & loc:parent
  orof 'new job booking'
  orof 'new job booking' & '_' & loc:parent
    Self.MakePage('LookupRRCAccounts',Net:Web:Browse,Net:Login,'New Job Booking',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseLoanUnits  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseloanunits'
  orof 'browseloanunits' & '_' & loc:parent
  orof 'browseloanunits' & '_' & loc:parent
    Self.MakePage('BrowseLoanUnits',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupSuburbs  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupsuburbs'
  orof 'lookupsuburbs' & '_' & loc:parent
  orof 'new job booking'
  orof 'new job booking' & '_' & loc:parent
    Self.MakePage('LookupSuburbs',Net:Web:Browse,Net:Login,'New Job Booking',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:MultipleBatchDespatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'multiplebatchdespatch'
  orof 'multbatchdespatch.htm'
    Self.MakePage('MultipleBatchDespatch',Net:Web:Form,Net:Login,'Multiple Batch Despatch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_browsebatchesinprogress_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locjobnumber_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locimeinumber_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locsecuritypacknumber_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonprocessjob_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_tagvalidateloanaccessories_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonvalidateaccessories_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonconfirmmismatch_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonfailaccessory_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locaccessorypassword_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectColours  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectcolours'
  orof 'selectcolours' & '_' & loc:parent
  orof 'select colour'
  orof 'select colour' & '_' & loc:parent
    Self.MakePage('SelectColours',Net:Web:Browse,Net:Login,'Select Colour',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:VodacomSingleInvoice  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'vodacomsingleinvoice'
  orof 'vodacomsingleinvoice' & '.pdf'
    VodacomSingleInvoice(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:IndexPage  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'indexpage'
  orof 'servicebase 3g'
    Self.MakePage('IndexPage',Net:Web:Form,Net:Login,'ServiceBase 3g','<!-- Net:BannerBlank-->','')
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewJob  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewjob'
  orof 'amend job'
    Self.MakePage('ViewJob',Net:Web:Form,Net:Login,'Amend Job',,)
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_job:current_status_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_jbn:fault_description_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_jbn:engineers_notes_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_jobe:network_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_job:authority_number_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_job:unit_type_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_browseestimateparts_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_browsechargeableparts_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_browsewarrantyparts_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_buttonresendxml_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_loccchargetypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_loccrepairtypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_locwchargetypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_locwrepairtypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_buttonprintestimate_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobCredits  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejobcredits'
  orof 'browsejobcredits' & '_' & loc:parent
  orof 'browsejobcredits' & '_' & loc:parent
    Self.MakePage('BrowseJobCredits',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsechargeableparts'
  orof 'browsechargeableparts' & '_' & loc:parent
  orof 'browsechargeableparts' & '_' & loc:parent
    Self.MakePage('BrowseChargeableParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobAccessories  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobaccessories'
  orof 'jobaccessories.htm'
    Self.MakePage('JobAccessories',Net:Web:Form,Net:Login,'Job Accessories',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_tmp:showaccessory_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_tmp:theaccessory_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_buttonaddaccessories_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_buttonremoveaccessory_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_jobe:accessorynotes_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_browseaccessorynumber_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseestimateparts'
  orof 'browseestimateparts' & '_' & loc:parent
  orof 'browseestimateparts' & '_' & loc:parent
    Self.MakePage('BrowseEstimateParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectAccessJobStatus  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectaccessjobstatus'
  orof 'selectaccessjobstatus' & '_' & loc:parent
  orof 'select status'
  orof 'select status' & '_' & loc:parent
    Self.MakePage('SelectAccessJobStatus',Net:Web:Browse,Net:Login,'Select Status',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SendSMS  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'sendsms'
  orof 'send sms'
    Self.MakePage('SendSMS',Net:Web:Form,0,'Send SMS',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DisplayOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'displayoutfaults'
  orof 'out faults'
    Self.MakePage('DisplayOutFaults',Net:Web:Form,0,'Out Faults',,)
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browsejoboutfaults_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browseoutfaultschargeableparts_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browseoutfaultswarrantyparts_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browseoutfaultsestimateparts_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejoboutfaults'
  orof 'browsejoboutfaults' & '_' & loc:parent
  orof 'out faults'
  orof 'out faults' & '_' & loc:parent
    Self.MakePage('BrowseJobOutFaults',Net:Web:Browse,Net:Login,'Out Faults',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickEngineersNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickengineersnotes'
  orof 'engineers notes'
    Self.MakePage('PickEngineersNotes',Net:Web:Form,0,'Engineers Notes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_locpicklist_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_locfinallist_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_button:addselected_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_button:removeselected_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_gap_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_button:removeall_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupGenericAccounts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupgenericaccounts'
  orof 'lookupgenericaccounts' & '_' & loc:parent
  orof 'new job booking'
  orof 'new job booking' & '_' & loc:parent
    Self.MakePage('LookupGenericAccounts',Net:Web:Browse,Net:Login,'New Job Booking',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bouncerchargeableparts'
  orof 'bouncerchargeableparts' & '_' & loc:parent
  orof 'bouncerchargeableparts' & '_' & loc:parent
    Self.MakePage('BouncerChargeableParts',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormExchangeUnitFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formexchangeunitfilter'
  orof 'browse exchange unit'
    Self.MakePage('FormExchangeUnitFilter',Net:Web:Form,Net:Login,'Browse Exchange Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_tmp:exchangestocktype_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_tmp:exchangemanufacturer_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_tmp:exchangemodelnumber_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_browseexchangeunits_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseSubAddresses  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsesubaddresses'
  orof 'browsesubaddresses' & '_' & loc:parent
  orof 'browse accounts'
  orof 'browse accounts' & '_' & loc:parent
    Self.MakePage('BrowseSubAddresses',Net:Web:Browse,0,'Browse Accounts',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsewarrantyparts'
  orof 'browsewarrantyparts' & '_' & loc:parent
  orof 'browsewarrantyparts' & '_' & loc:parent
    Self.MakePage('BrowseWarrantyParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormDeletePart  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formdeletepart'
  orof 'delete part'
    Self.MakePage('FormDeletePart',Net:Web:Form,Net:Login,'Delete Part',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formdeletepart_locpartnumber_value')
    FormDeletePart(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formdeletepart_locdescription_value')
    FormDeletePart(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formdeletepart_locscraprestock_value')
    FormDeletePart(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ProofOfPurchase  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'proofofpurchase'
  orof 'proof of purchase'
    Self.MakePage('ProofOfPurchase',Net:Web:Form,Net:Login,'Proof Of Purchase',,)
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:dop_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:pop_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_locpoptypepassword_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:poptype_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:warrantyrefno_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CreateNewBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'createnewbatch'
    Self.MakePage('CreateNewBatch',Net:Web:Form,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formwarrantyparts'
  orof 'insert / amend warranty parts'
    Self.MakePage('FormWarrantyParts',Net:Web:Form,Net:Login,'Insert / Amend Warranty Parts',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:correction_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:part_number_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:description_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:despatch_note_number_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:quantity_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:purchasecost_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:inwarrantycost_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:inwarrantymarkup_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:supplier_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:exclude_from_order_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:partallocated_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:unallocatepart_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:createorder_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcodeschecked_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcodes1_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode2_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode3_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode4_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode5_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode6_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode7_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode8_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode9_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode10_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode11_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode12_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickPaperwork  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickpaperwork'
    Self.MakePage('PickPaperwork',Net:Web:Form,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerNewJobBooking  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannernewjobbooking'
    Self.MakePage('BannerNewJobBooking',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DespatchNote  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'despatchnote'
  orof 'despatchnote' & '.pdf'
    DespatchNote(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:AddToBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'addtobatch'
    Self.MakePage('AddToBatch',Net:Web:Form,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseModelNumbers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsemodelnumbers'
  orof 'browsemodelnumbers' & '_' & loc:parent
  orof 'browsemodelnumbers' & '_' & loc:parent
    Self.MakePage('BrowseModelNumbers',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormRepairNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formrepairnotes'
  orof 'insert / amend repair notes'
    Self.MakePage('FormRepairNotes',Net:Web:Form,Net:Login,'Insert / Amend Repair Notes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formrepairnotes_jrn:thedate_value')
    FormRepairNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formrepairnotes_jrn:thetime_value')
    FormRepairNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formrepairnotes_jrn:notes_value')
    FormRepairNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupProductCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupproductcodes'
  orof 'lookupproductcodes' & '_' & loc:parent
  orof 'select product code'
  orof 'select product code' & '_' & loc:parent
    Self.MakePage('LookupProductCodes',Net:Web:Browse,Net:Login,'Select Product Code',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowsePayments  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsepayments'
  orof 'browsepayments' & '_' & loc:parent
  orof 'browse payments'
  orof 'browse payments' & '_' & loc:parent
    Self.MakePage('BrowsePayments',Net:Web:Browse,0,'Browse Payments',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:Estimate  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'estimate'
  orof 'estimate' & '.pdf'
    Estimate(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobFaultCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobfaultcodes'
  orof 'jobfaultcodes.htm'
    Self.MakePage('JobFaultCodes',Net:Web:Form,Net:Login,'Job Fault Codes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:msn_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_buttonverifymsn_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:verifymsn_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_buttonverifymsn2_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:confirmmsnchange_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_job:productcode_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode1_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode2_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode3_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode4_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode5_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode6_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode7_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode8_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode9_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode10_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode11_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode12_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode13_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode14_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode15_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode16_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode17_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode18_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode19_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode20_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:processexchange_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectCouriers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectcouriers'
  orof 'selectcouriers' & '_' & loc:parent
  orof 'select courier'
  orof 'select courier' & '_' & loc:parent
    Self.MakePage('SelectCouriers',Net:Web:Browse,Net:Login,'Select Courier',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:RefreshPage  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'refreshpage'
    RefreshPage(self)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectChargeTypes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectchargetypes'
  orof 'selectchargetypes' & '_' & loc:parent
  orof 'select charge type'
  orof 'select charge type' & '_' & loc:parent
    Self.MakePage('SelectChargeTypes',Net:Web:Browse,Net:Login,'Select Charge Type',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:AmendAddress  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'amendaddress'
  orof 'amend addresses'
    Self.MakePage('AmendAddress',Net:Web:Form,Net:Login,'Amend Addresses',,)
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:company_name_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line1_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line2_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line3_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:hubcustomer_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:postcode_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:telephone_number_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:fax_number_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:enduseremailaddress_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:endusertelno_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:vatnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:idnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:smsnotification_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:smsalertnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:emailnotification_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:emailalertaddress_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:courierwaybillnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:company_name_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:company_name_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line1_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line1_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line2_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line2_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line3_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line3_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:hubdelivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:hubcollection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:postcode_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:postcode_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:telephone_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:telephone_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_button:copyenduseraddress_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_button:copyenduseraddress1_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:delivery_text_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:collection_text_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:delcontactname_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:colcontatname_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:deldepartment_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:coldepartment_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:sub_sub_account_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseAuditFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseauditfilter'
  orof 'browse audit trail'
    Self.MakePage('BrowseAuditFilter',Net:Web:Form,Net:Login,'Browse Audit Trail',,)
    loc:Done = 1 ; Exit
  of self._nocolon('browseauditfilter_tmp:statustypefilter_value')
    BrowseAuditFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('browseauditfilter_browseaudittrail_value')
    BrowseAuditFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickLoanUnit  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickloanunit'
  orof 'pick loan unit'
    Self.MakePage('PickLoanUnit',Net:Web:Form,Net:Login,'Pick Loan Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tmp:loanimeinumber_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonpickloanunit_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_jobe2:loanidnumber_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_locloanidoption_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_button:amenddespatchdetails_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonremoveattachedunit_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_locremovalalertmessage_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tmp:removalreason_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tagvalidateloanaccessories_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonvalidateaccessories_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tmp:lostloanfee_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonconfirmloanremoval_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_link:sendsms_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobSearch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobsearch'
  orof 'jobsearch'
    Self.MakePage('JobSearch',Net:Web:Form,Net:Login,'Job Search',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobsearch_locsearchjobnumber_value')
    JobSearch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobsearch_locjobpassword_value')
    JobSearch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ClearAddressDetails  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'clearaddressdetails'
  orof 'address options'
    Self.MakePage('ClearAddressDetails',Net:Web:Form,0,'Address Options',,)
    loc:Done = 1 ; Exit
  of self._nocolon('clearaddressdetails_locclearaddressoption_value')
    ClearAddressDetails(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:OBFValidation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'obfvalidation'
  orof 'obfvalidation.htm'
    Self.MakePage('OBFValidation',Net:Web:Form,Net:Login,'OBF Validation','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:imeinumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:boximeinumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:network_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:dateofpurchase_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:returndate_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:talktime_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originaldealer_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:branchofreturn_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:storereferencenumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:proofofpurchase_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originalpackaging_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originalaccessories_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originalmanuals_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:physicaldamage_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:replacement_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:laccountnumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:replacementimeinumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_button:failvalidation_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_error:validation_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:transittype_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SendAnEmail  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'sendanemail'
  orof 'send email'
    Self.MakePage('SendAnEmail',Net:Web:Form,0,'Send Email',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStockPartFaultCodeLookup  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestockpartfaultcodelookup'
  orof 'browsestockpartfaultcodelookup' & '_' & loc:parent
  orof 'select part fault codes'
  orof 'select part fault codes' & '_' & loc:parent
    Self.MakePage('BrowseStockPartFaultCodeLookup',Net:Web:Browse,Net:Login,'Select Part Fault Codes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormPayments  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formpayments'
  orof 'job payments'
    Self.MakePage('FormPayments',Net:Web:Form,Net:Login,'Job Payments',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:date_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:payment_type_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:credit_card_number_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:expiry_date_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:issue_number_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:amount_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormAddToBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formaddtobatch'
  orof 'multiple despatch'
    Self.MakePage('FormAddToBatch',Net:Web:Form,Net:Login,'Multiple Despatch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formaddtobatch_locwaybillnumber_value')
    FormAddToBatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseLocationHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browselocationhistory'
  orof 'browselocationhistory' & '_' & loc:parent
  orof 'browse location history'
  orof 'browse location history' & '_' & loc:parent
    Self.MakePage('BrowseLocationHistory',Net:Web:Browse,Net:Login,'Browse Location History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStatusChanges  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestatuschanges'
  orof 'browsestatuschanges' & '_' & loc:parent
  orof 'browsestatuschanges' & '_' & loc:parent
    Self.MakePage('BrowseStatusChanges',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormPickModelStockLocation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formpickmodelstocklocation'
  orof 'browse stock'
    Self.MakePage('FormPickModelStockLocation',Net:Web:Form,0,'Browse Stock',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formpickmodelstocklocation_locpicklocation_value')
    FormPickModelStockLocation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpickmodelstocklocation_browsemodelstock_value')
    FormPickModelStockLocation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:InsertJob_Finished  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'insertjob_finished'
  orof 'jobbooked.htm'
    Self.MakePage('InsertJob_Finished',Net:Web:Form,Net:Login,'Job Booked','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewCosts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewcosts'
  orof 'view costs'
    Self.MakePage('ViewCosts',Net:Web:Form,Net:Login,'View Costs',,)
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arcviewcosttype_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arcignoredefaultcharges_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arcignorereason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttonacceptarcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttoncancelarcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost1_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost1_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost2_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost2_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost3_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost3_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost4_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost4_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost5_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost5_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost6_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost6_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost7_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost8_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrcviewcosttype_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrcignoredefaultcharges_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrcignorereason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttonacceptrrcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttoncancelrrcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost0_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost1_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:originalinvoice_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost2_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost3_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_browsejobcredits_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost4_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost5_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost6_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost7_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost8_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_jobe:excreplcamentcharge_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseEngineerHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseengineerhistory'
  orof 'browseengineerhistory' & '_' & loc:parent
  orof 'browse engineer history'
  orof 'browse engineer history' & '_' & loc:parent
    Self.MakePage('BrowseEngineerHistory',Net:Web:Browse,Net:Login,'Browse Engineer History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectNetworks  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectnetworks'
  orof 'selectnetworks' & '_' & loc:parent
  orof 'select network'
  orof 'select network' & '_' & loc:parent
    Self.MakePage('SelectNetworks',Net:Web:Browse,Net:Login,'Select Network',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerBlank  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannerblank'
    Self.MakePage('BannerBlank',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultcodes'
  orof 'browseoutfaultcodes' & '_' & loc:parent
  orof 'browse out faults'
  orof 'browse out faults' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultCodes',Net:Web:Browse,Net:Login,'Browse Out Faults',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectUnitTypes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectunittypes'
  orof 'selectunittypes' & '_' & loc:parent
  orof 'select unit type'
  orof 'select unit type' & '_' & loc:parent
    Self.MakePage('SelectUnitTypes',Net:Web:Browse,Net:Login,'Select Unit Type',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickExchangeUnit  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickexchangeunit'
  orof 'pick exchange unit'
    Self.MakePage('PickExchangeUnit',Net:Web:Form,Net:Login,'Pick Exchange Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:exchangeimeinumber_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_buttonpickexchangeunit_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:handsetpartnumber_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:handsetreplacementvalue_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_job:exchange_courier_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_button:amenddespatchdetails_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_buttonremoveattachedunit_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_locremovalalertmessage_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:removalreason_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_buttonconfirmexchangeremoval_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerMainMenu  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannermainmenu'
    Self.MakePage('BannerMainMenu',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bouncerwarrantyparts'
  orof 'bouncerwarrantyparts' & '_' & loc:parent
  orof 'bouncerwarrantyparts' & '_' & loc:parent
    Self.MakePage('BouncerWarrantyParts',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseModelStock  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsemodelstock'
  orof 'browsemodelstock' & '_' & loc:parent
  orof 'browse stock'
  orof 'browse stock' & '_' & loc:parent
    Self.MakePage('BrowseModelStock',Net:Web:Browse,Net:Login,'Browse Stock',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PageFooter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pagefooter'
    Self.MakePage('PageFooter',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bounceroutfaults'
  orof 'bounceroutfaults' & '_' & loc:parent
  orof 'bounceroutfaults' & '_' & loc:parent
    Self.MakePage('BouncerOutFaults',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickAccessory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickaccessory'
    Self.MakePage('PickAccessory',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_tmp:showaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_tmp:theaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:addaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:removeaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:addall_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:removeall_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobFaultCodeLookup  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejobfaultcodelookup'
  orof 'browsejobfaultcodelookup' & '_' & loc:parent
  orof 'select fault codes'
  orof 'select fault codes' & '_' & loc:parent
    Self.MakePage('BrowseJobFaultCodeLookup',Net:Web:Browse,Net:Login,'Select Fault Codes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormBrowseEngineerHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formbrowseengineerhistory'
  orof 'engineer history'
    Self.MakePage('FormBrowseEngineerHistory',Net:Web:Form,Net:Login,'Engineer History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formbrowseengineerhistory_browseengineerhistory_value')
    FormBrowseEngineerHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:InvoiceCreated  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'invoicecreated'
  orof 'invoicecreated.htm'
    Self.MakePage('InvoiceCreated',Net:Web:Form,0,'Invoice Created',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseIMEIHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseimeihistory'
  orof 'browseimeihistory' & '_' & loc:parent
  orof 'previous job history'
  orof 'previous job history' & '_' & loc:parent
    Self.MakePage('BrowseIMEIHistory',Net:Web:Browse,Net:Login,'Previous Job History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobReceipt  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobreceipt'
  orof 'jobreceipt' & '.pdf'
    JobReceipt(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseExchangeUnits  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseexchangeunits'
  orof 'browseexchangeunits' & '_' & loc:parent
  orof 'browseexchangeunits' & '_' & loc:parent
    Self.MakePage('BrowseExchangeUnits',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewBouncerJob  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewbouncerjob'
  orof 'view job details'
    Self.MakePage('ViewBouncerJob',Net:Web:Form,0,'View Job Details',,)
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:account_number_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:company_name_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:address_line1_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:address_line2_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:address_line3_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:postcode_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:telephone_number_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:fax_number_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:emailaddress_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PrintRoutines  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'printroutines'
  orof 'printroutines.htm'
    Self.MakePage('PrintRoutines',Net:Web:Form,Net:Login,'Print Routines',,)
    loc:Done = 1 ; Exit
  of self._nocolon('printroutines_locjobnumber_value')
    PrintRoutines(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('printroutines_locwaybillnumber_value')
    PrintRoutines(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowsePartFaultCodeLookup  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsepartfaultcodelookup'
  orof 'browsepartfaultcodelookup' & '_' & loc:parent
  orof 'select part fault codes'
  orof 'select part fault codes' & '_' & loc:parent
    Self.MakePage('BrowsePartFaultCodeLookup',Net:Web:Browse,Net:Login,'Select Part Fault Codes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerEngineeringDetails  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannerengineeringdetails'
    Self.MakePage('BannerEngineeringDetails',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobEstimateQuery  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobestimatequery'
  orof 'estimatequery'
    Self.MakePage('JobEstimateQuery',Net:Web:Form,Net:Login,'Estimate Query',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimatequery_locestimatereadyoption_value')
    JobEstimateQuery(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:NewJobBooking  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'newjobbooking'
  orof 'newjobbooking.htm'
    Self.MakePage('NewJobBooking',Net:Web:Form,Net:Login,'New Job Booking','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:previousaddress_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:usepreviousaddress_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:network_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:mobile_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_buttonmsisdncheck_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:unit_type_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:productcode_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:colour_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:msn_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:dop_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:pop_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:popconfirmed_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_locpoptypepassword_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:poptype_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:warrantyrefno_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:chargeable_job_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:charge_type_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:warranty_job_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:warranty_charge_type_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:authority_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_franchiseaccount_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:account_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:account_number2_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:order_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:title_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:initial_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:surname_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:endusertelno_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:courier_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:courierwaybillnumber_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:amendaccessories_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:showaccessory_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:theaccessory_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_button:addaccessories_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_button:removeaccessory_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_button:removeallaccessories_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:amendfaultcodes_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_1_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_2_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_3_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_4_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_5_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_6_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:engineer_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jbn:fault_description_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jbn:engineers_notes_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:externaldamagechecklist_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xnone_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xantenna_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xlens_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xfcover_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xbcover_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xkeypad_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xbattery_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xcharger_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xlcd_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xsimreader_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xsystemconnector_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xnotes_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:booking48houroption_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:vsacustomer_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:contract_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:prepaid_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewchargeableparts'
  orof 'viewchargeableparts' & '_' & loc:parent
  orof 'viewchargeableparts' & '_' & loc:parent
    Self.MakePage('ViewChargeableParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupJobStatus  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupjobstatus'
  orof 'lookupjobstatus' & '_' & loc:parent
  orof 'lookupjobstatus' & '_' & loc:parent
    Self.MakePage('LookupJobStatus',Net:Web:Browse,0,'Select Job Status',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectModelNumbers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectmodelnumbers'
  orof 'selectmodelnumbers' & '_' & loc:parent
  orof 'select model number'
  orof 'select model number' & '_' & loc:parent
    Self.MakePage('SelectModelNumbers',Net:Web:Browse,Net:Login,'Select Model Number',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStatusChangesFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestatuschangesfilter'
    Self.MakePage('BrowseStatusChangesFilter',Net:Web:Form,Net:Login,'Browse Status Change History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatuschangesfilter_tmp:statustypefilter_value')
    BrowseStatusChangesFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatuschangesfilter_browsestatuschanges_value')
    BrowseStatusChangesFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SetJobType  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'setjobtype'
  orof 'set job type(s)'
    Self.MakePage('SetJobType',Net:Web:Form,Net:Login,'Set Job Type(s)',,)
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locchargeablejob_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_loccchargetype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_loccrepairtype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locwarrantyjob_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locwchargetype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locwrepairtype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_buttonconfirm_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_buttonsplitjob_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_buttoncancel_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerBrowseJobs  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannerbrowsejobs'
    Self.MakePage('BannerBrowseJobs',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultsChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultschargeableparts'
  orof 'browseoutfaultschargeableparts' & '_' & loc:parent
  orof 'browseoutfaultschargeableparts' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultsChargeableParts',Net:Web:Browse,Net:Login,'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''C''',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:InvoiceNote  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'invoicenote'
  orof 'invoicenote' & '.pdf'
    InvoiceNote(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewestimateparts'
  orof 'viewestimateparts' & '_' & loc:parent
  orof 'viewestimateparts' & '_' & loc:parent
    Self.MakePage('ViewEstimateParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:Waybill  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'waybill'
  orof 'waybill' & '.pdf'
    Waybill(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseSMSHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsesmshistory'
  orof 'browsesmshistory' & '_' & loc:parent
  orof 'browse sms history'
  orof 'browse sms history' & '_' & loc:parent
    Self.MakePage('BrowseSMSHistory',Net:Web:Browse,0,'Browse SMS History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CreateInvoice  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'createinvoice'
  orof 'create invoice'
    Self.MakePage('CreateInvoice',Net:Web:Form,0,'Create Invoice',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewwarrantyparts'
  orof 'viewwarrantyparts' & '_' & loc:parent
  orof 'viewwarrantyparts' & '_' & loc:parent
    Self.MakePage('ViewWarrantyParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseBatchesInProgress  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsebatchesinprogress'
  orof 'browsebatchesinprogress' & '_' & loc:parent
  orof 'browsebatchesinprogress' & '_' & loc:parent
    Self.MakePage('BrowseBatchesInProgress',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectSuppliers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectsuppliers'
  orof 'selectsuppliers' & '_' & loc:parent
  orof 'select supplier'
  orof 'select supplier' & '_' & loc:parent
    Self.MakePage('SelectSuppliers',Net:Web:Browse,Net:Login,'Select Supplier',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DespatchConfirmation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'despatchconfirmation'
  orof 'despatch confirmation'
    Self.MakePage('DespatchConfirmation',Net:Web:Form,Net:Login,'Despatch Confirmation',,)
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_job:courier_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_buttoncreateinvoice_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_buttonprintwaybill_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_buttonprintdespatchnote_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ReceiptFromPUP  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'receiptfrompup'
  orof 'pup validation'
    Self.MakePage('ReceiptFromPUP',Net:Web:Form,Net:Login,'PUP Validation',,)
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locimeinumber_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locpassword_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locmsn_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locnetwork_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locinfault_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_tagvalidateloanaccessories_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_buttonvalidateaccessories_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formchargeableparts'
  orof 'insert / amend chargeable parts'
    Self.MakePage('FormChargeableParts',Net:Web:Form,Net:Login,'Insert / Amend Chargeable Parts',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:part_number_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:description_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:despatch_note_number_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:quantity_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:purchasecost_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:outwarrantycost_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:outwarrantymarkup_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:supplier_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:exclude_from_order_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:partallocated_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:unallocatepart_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:createorder_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcodeschecked_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcodes1_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode2_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode3_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode4_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode5_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode6_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode7_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode8_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode9_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode10_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode11_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode12_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormAccessoryNumbers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formaccessorynumbers'
  orof 'accessorynumber'
    Self.MakePage('FormAccessoryNumbers',Net:Web:Form,Net:Login,'Accessory Number',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formaccessorynumbers_joa:accessorynumber_value')
    FormAccessoryNumbers(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerVodacom  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannervodacom'
    Self.MakePage('BannerVodacom',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormEngineeringOption  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formengineeringoption'
  orof 'change engineering option'
    Self.MakePage('FormEngineeringOption',Net:Web:Form,Net:Login,'Change Engineering Option',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locuserpassword_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locnewengineeringoption_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_job:dop_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locexchangemanufacturer_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locexchangemodelnumber_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locexchangenotes_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locsplitjob_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_button:createorder_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:AllocateEngineer  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'allocateengineer'
  orof 'allocate engineer'
    Self.MakePage('AllocateEngineer',Net:Web:Form,Net:Login,'Allocate Engineer',,)
    loc:Done = 1 ; Exit
  of self._nocolon('allocateengineer_tmp:engineerpassword_value')
    AllocateEngineer(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formestimateparts'
  orof 'insert / amend estimate parts'
    Self.MakePage('FormEstimateParts',Net:Web:Form,Net:Login,'Insert / Amend Estimate Parts',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:part_number_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:description_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:despatch_note_number_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:quantity_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:purchasecost_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:outwarrantycost_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:outwarrantymarkup_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:supplier_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:exclude_from_order_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:partallocated_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:unallocatepart_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:createorder_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcodeschecked_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcodes1_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode2_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode3_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode4_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode5_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode6_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode7_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode8_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode9_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode10_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode11_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode12_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:usedonrepair_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:formFilterModelNumber  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formfiltermodelnumber'
  orof 'select model numbers'
    Self.MakePage('formFilterModelNumber',Net:Web:Form,0,'Select Model Numbers',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formfiltermodelnumber_filter:manufacturer_value')
    formFilterModelNumber(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formfiltermodelnumber_browsemodelnumbers_value')
    formFilterModelNumber(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseRepairNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browserepairnotes'
  orof 'browserepairnotes' & '_' & loc:parent
  orof 'job repair notes'
  orof 'job repair notes' & '_' & loc:parent
    Self.MakePage('BrowseRepairNotes',Net:Web:Browse,Net:Login,'Job Repair Notes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseEngineersOnJob  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseengineersonjob'
  orof 'browseengineersonjob' & '_' & loc:parent
  orof 'browseengineersonjob' & '_' & loc:parent
    Self.MakePage('BrowseEngineersOnJob',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseAccessoryNumber  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseaccessorynumber'
  orof 'browseaccessorynumber' & '_' & loc:parent
  orof 'browseaccessorynumber' & '_' & loc:parent
    Self.MakePage('BrowseAccessoryNumber',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CloseWebPage  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'closewebpage'
    CloseWebPage(self)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultsWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultswarrantyparts'
  orof 'browseoutfaultswarrantyparts' & '_' & loc:parent
  orof 'browseoutfaultswarrantyparts' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultsWarrantyParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DisplayBrowsePayments  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'displaybrowsepayments'
  orof 'browsepayments.htm'
    Self.MakePage('DisplayBrowsePayments',Net:Web:Form,Net:Login,'Browse Payments',,)
    loc:Done = 1 ; Exit
  of self._nocolon('displaybrowsepayments_browsepayments_value')
    DisplayBrowsePayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultsEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultsestimateparts'
  orof 'browseoutfaultsestimateparts' & '_' & loc:parent
  orof 'browseoutfaultsestimateparts' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultsEstimateParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormJobOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formjoboutfaults'
  orof 'insert out fault'
    Self.MakePage('FormJobOutFaults',Net:Web:Form,Net:Login,'Insert Out Fault',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_tmp:freetextoutfault_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_joo:faultcode_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_joo:description_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_joo:level_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormChangeDOP  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formchangedop'
  orof 'override dop'
    Self.MakePage('FormChangeDOP',Net:Web:Form,Net:Login,'Override DOP',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_tmp:userpassword_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_job:dop_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_tmp:newdop_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_tmp:changereason_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStatusFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestatusfilter'
  orof 'browse status changes'
    Self.MakePage('BrowseStatusFilter',Net:Web:Form,Net:Login,'Browse Status Changes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatusfilter_tmp:statustypefilter_value')
    BrowseStatusFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatusfilter_browsestatuschanges_value')
    BrowseStatusFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bouncerhistory'
  orof 'bouncerhistory' & '.pdf'
    BouncerHistory(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BillingConfirmation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'billingconfirmation'
  orof 'billingconfirmation'
    Self.MakePage('BillingConfirmation',Net:Web:Form,Net:Login,'Billing Confirmation',,)
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:cchargetype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:crepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:cconfirmrepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:wchargetype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:wrepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:wconfirmrepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectManufacturers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectmanufacturers'
  orof 'selectmanufacturers' & '_' & loc:parent
  orof 'select manufacturer'
  orof 'select manufacturer' & '_' & loc:parent
    Self.MakePage('SelectManufacturers',Net:Web:Browse,0,'Select Manufacturer',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectTransitTypes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selecttransittypes'
  orof 'selecttransittypes' & '_' & loc:parent
  orof 'select transit type'
  orof 'select transit type' & '_' & loc:parent
    Self.MakePage('SelectTransitTypes',Net:Web:Browse,0,'Select Transit Type',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseAuditTrail  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseaudittrail'
  orof 'browseaudittrail' & '_' & loc:parent
  orof 'browseaudittrail' & '_' & loc:parent
    Self.MakePage('BrowseAuditTrail',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormLoanUnitFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formloanunitfilter'
  orof 'browse loan unit'
    Self.MakePage('FormLoanUnitFilter',Net:Web:Form,Net:Login,'Browse Loan Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_tmp:loanstocktype_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_tmp:loanmanufacturer_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_tmp:loanmodelnumber_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_browseloanunits_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PageHeader  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pageheader'
    PageHeader(self)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobCard  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobcard'
  orof 'jobcard' & '.pdf'
    JobCard(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SetHubRepair  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'sethubrepair'
  orof 'sethubrepair' & '_' & loc:parent
      self._requestajaxnow = 1
      SetHubRepair(self)
      self._Sendfooter()
      loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickContactHistoryNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickcontacthistorynotes'
  orof 'contact history notes'
    Self.MakePage('PickContactHistoryNotes',Net:Web:Form,0,'Contact History Notes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_locpicklist_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_locfinallist_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_button:addselected_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_button:removeselected_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_gap_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_button:removeall_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectFaultCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectfaultcodes'
  orof 'selectfaultcodes' & '_' & loc:parent
  orof 'select fault code'
  orof 'select fault code' & '_' & loc:parent
    Self.MakePage('SelectFaultCodes',Net:Web:Browse,Net:Login,'Select Fault Code',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormJobsInBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formjobsinbatch'
    Self.MakePage('FormJobsInBatch',Net:Web:Form,Net:Login,'Update MULDESPJ',,)
    loc:Done = 1 ; Exit
  End ! Case
  Section('ProcessTag')
  Case loc:tag
    of 'finishbatch'
      finishbatch(Self)
    of 'messagequestion'
      messagequestion(Self)
    of 'createcreditnote'
      createcreditnote(Self)
    of 'individualdespatch'
      individualdespatch(Self)
    of 'jobestimate'
      jobestimate(Self)
    of 'prenewjobbooking'
      prenewjobbooking(Self)
    of 'formcontacthistory'
      formcontacthistory(Self)
    of 'formbrowselocationhistory'
      formbrowselocationhistory(Self)
    of 'clearjobvariables'
      clearjobvariables(Self)
    of 'selectengineers'
      selectengineers(Self)
    of 'loginform'
      loginform(Self)
    of 'formbrowsecontacthistory'
      formbrowsecontacthistory(Self)
    of 'messagealert'
      messagealert(Self)
    of 'browsecontacthistory'
      browsecontacthistory(Self)
    of 'tagvalidateloanaccessories'
      tagvalidateloanaccessories(Self)
    of 'browsejobsinbatch'
      browsejobsinbatch(Self)
    of 'lookuprrcaccounts'
      lookuprrcaccounts(Self)
    of 'browseloanunits'
      browseloanunits(Self)
    of 'lookupsuburbs'
      lookupsuburbs(Self)
    of 'multiplebatchdespatch'
      multiplebatchdespatch(Self)
    of 'selectcolours'
      selectcolours(Self)
    of 'indexpage'
      indexpage(Self)
    of 'viewjob'
      viewjob(Self)
    of 'browsejobcredits'
      browsejobcredits(Self)
    of 'browsechargeableparts'
      browsechargeableparts(Self)
    of 'jobaccessories'
      jobaccessories(Self)
    of 'browseestimateparts'
      browseestimateparts(Self)
    of 'clearupdatejobvariables'
      clearupdatejobvariables(Self)
    of 'selectaccessjobstatus'
      selectaccessjobstatus(Self)
    of 'sendsms'
      sendsms(Self)
    of 'displayoutfaults'
      displayoutfaults(Self)
    of 'browsejoboutfaults'
      browsejoboutfaults(Self)
    of 'pickengineersnotes'
      pickengineersnotes(Self)
    of 'lookupgenericaccounts'
      lookupgenericaccounts(Self)
    of 'bouncerchargeableparts'
      bouncerchargeableparts(Self)
    of 'formexchangeunitfilter'
      formexchangeunitfilter(Self)
    of 'browsesubaddresses'
      browsesubaddresses(Self)
    of 'browsewarrantyparts'
      browsewarrantyparts(Self)
    of 'formdeletepart'
      formdeletepart(Self)
    of 'proofofpurchase'
      proofofpurchase(Self)
    of 'createnewbatch'
      createnewbatch(Self)
    of 'formwarrantyparts'
      formwarrantyparts(Self)
    of 'pickpaperwork'
      pickpaperwork(Self)
    of 'bannernewjobbooking'
      bannernewjobbooking(Self)
    of 'addtobatch'
      addtobatch(Self)
    of 'browsemodelnumbers'
      browsemodelnumbers(Self)
    of 'formrepairnotes'
      formrepairnotes(Self)
    of 'lookupproductcodes'
      lookupproductcodes(Self)
    of 'browsepayments'
      browsepayments(Self)
    of 'jobfaultcodes'
      jobfaultcodes(Self)
    of 'selectcouriers'
      selectcouriers(Self)
    of 'selectchargetypes'
      selectchargetypes(Self)
    of 'amendaddress'
      amendaddress(Self)
    of 'browseauditfilter'
      browseauditfilter(Self)
    of 'pickloanunit'
      pickloanunit(Self)
    of 'jobsearch'
      jobsearch(Self)
    of 'clearaddressdetails'
      clearaddressdetails(Self)
    of 'obfvalidation'
      obfvalidation(Self)
    of 'sendanemail'
      sendanemail(Self)
    of 'browsestockpartfaultcodelookup'
      browsestockpartfaultcodelookup(Self)
    of 'formpayments'
      formpayments(Self)
    of 'formaddtobatch'
      formaddtobatch(Self)
    of 'browselocationhistory'
      browselocationhistory(Self)
    of 'browsestatuschanges'
      browsestatuschanges(Self)
    of 'formpickmodelstocklocation'
      formpickmodelstocklocation(Self)
    of 'insertjob_finished'
      insertjob_finished(Self)
    of 'viewcosts'
      viewcosts(Self)
    of 'browseengineerhistory'
      browseengineerhistory(Self)
    of 'selectnetworks'
      selectnetworks(Self)
    of 'bannerblank'
      bannerblank(Self)
    of 'menutools'
      menutools(Self)
    of 'browseoutfaultcodes'
      browseoutfaultcodes(Self)
    of 'selectunittypes'
      selectunittypes(Self)
    of 'pickexchangeunit'
      pickexchangeunit(Self)
    of 'bannermainmenu'
      bannermainmenu(Self)
    of 'bouncerwarrantyparts'
      bouncerwarrantyparts(Self)
    of 'browsemodelstock'
      browsemodelstock(Self)
    of 'pagefooter'
      pagefooter(Self)
    of 'bounceroutfaults'
      bounceroutfaults(Self)
    of 'pickaccessory'
      pickaccessory(Self)
    of 'browsejobfaultcodelookup'
      browsejobfaultcodelookup(Self)
    of 'formbrowseengineerhistory'
      formbrowseengineerhistory(Self)
    of 'invoicecreated'
      invoicecreated(Self)
    of 'browseimeihistory'
      browseimeihistory(Self)
    of 'browseexchangeunits'
      browseexchangeunits(Self)
    of 'viewbouncerjob'
      viewbouncerjob(Self)
    of 'printroutines'
      printroutines(Self)
    of 'browsepartfaultcodelookup'
      browsepartfaultcodelookup(Self)
    of 'gotobottom'
      gotobottom(Self)
    of 'bannerengineeringdetails'
      bannerengineeringdetails(Self)
    of 'jobestimatequery'
      jobestimatequery(Self)
    of 'newjobbooking'
      newjobbooking(Self)
    of 'viewchargeableparts'
      viewchargeableparts(Self)
    of 'lookupjobstatus'
      lookupjobstatus(Self)
    of 'setbottom'
      setbottom(Self)
    of 'selectmodelnumbers'
      selectmodelnumbers(Self)
    of 'browsestatuschangesfilter'
      browsestatuschangesfilter(Self)
    of 'setjobtype'
      setjobtype(Self)
    of 'bannerbrowsejobs'
      bannerbrowsejobs(Self)
    of 'browseoutfaultschargeableparts'
      browseoutfaultschargeableparts(Self)
    of 'viewestimateparts'
      viewestimateparts(Self)
    of 'browsesmshistory'
      browsesmshistory(Self)
    of 'createinvoice'
      createinvoice(Self)
    of 'viewwarrantyparts'
      viewwarrantyparts(Self)
    of 'popupmessage'
      popupmessage(Self)
    of 'browsebatchesinprogress'
      browsebatchesinprogress(Self)
    of 'selectsuppliers'
      selectsuppliers(Self)
    of 'despatchconfirmation'
      despatchconfirmation(Self)
    of 'receiptfrompup'
      receiptfrompup(Self)
    of 'formchargeableparts'
      formchargeableparts(Self)
    of 'formaccessorynumbers'
      formaccessorynumbers(Self)
    of 'bannervodacom'
      bannervodacom(Self)
    of 'formengineeringoption'
      formengineeringoption(Self)
    of 'allocateengineer'
      allocateengineer(Self)
    of 'formestimateparts'
      formestimateparts(Self)
    of 'formfiltermodelnumber'
      formfiltermodelnumber(Self)
    of 'browserepairnotes'
      browserepairnotes(Self)
    of 'browseengineersonjob'
      browseengineersonjob(Self)
    of 'browseaccessorynumber'
      browseaccessorynumber(Self)
    of 'browseoutfaultswarrantyparts'
      browseoutfaultswarrantyparts(Self)
    of 'displaybrowsepayments'
      displaybrowsepayments(Self)
    of 'browseoutfaultsestimateparts'
      browseoutfaultsestimateparts(Self)
    of 'formjoboutfaults'
      formjoboutfaults(Self)
    of 'formchangedop'
      formchangedop(Self)
    of 'browsestatusfilter'
      browsestatusfilter(Self)
    of 'billingconfirmation'
      billingconfirmation(Self)
    of 'selectmanufacturers'
      selectmanufacturers(Self)
    of 'selecttransittypes'
      selecttransittypes(Self)
    of 'browseaudittrail'
      browseaudittrail(Self)
    of 'formloanunitfilter'
      formloanunitfilter(Self)
    of 'sethubrepair'
      sethubrepair(Self)
    of 'pickcontacthistorynotes'
      pickcontacthistorynotes(Self)
    of 'selectfaultcodes'
      selectfaultcodes(Self)
    of 'formjobsinbatch'
      formjobsinbatch(Self)
  End
  Section('CallForm')
    If Band(p_Stage, NET:WEB:StagePost + NET:WEB:StageValidate + NET:WEB:Cancel)
      case lower(SELF.GetValue('FromForm'))
      Of 'finishbatch'
         ReturnValue = FinishBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'createcreditnote'
         ReturnValue = CreateCreditNote(Self,p_stage)
         RETURN ReturnValue
      Of 'individualdespatch'
         ReturnValue = IndividualDespatch(Self,p_stage)
         RETURN ReturnValue
      Of 'jobestimate'
         ReturnValue = JobEstimate(Self,p_stage)
         RETURN ReturnValue
      Of 'prenewjobbooking'
         ReturnValue = PreNewJobBooking(Self,p_stage)
         RETURN ReturnValue
      Of 'formcontacthistory'
         ReturnValue = FormContactHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'formbrowselocationhistory'
         ReturnValue = FormBrowseLocationHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'loginform'
         ReturnValue = LoginForm(Self,p_stage)
         RETURN ReturnValue
      Of 'formbrowsecontacthistory'
         ReturnValue = FormBrowseContactHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'multiplebatchdespatch'
         ReturnValue = MultipleBatchDespatch(Self,p_stage)
         RETURN ReturnValue
      Of 'indexpage'
         ReturnValue = IndexPage(Self,p_stage)
         RETURN ReturnValue
      Of 'viewjob'
         ReturnValue = ViewJob(Self,p_stage)
         RETURN ReturnValue
      Of 'jobaccessories'
         ReturnValue = JobAccessories(Self,p_stage)
         RETURN ReturnValue
      Of 'sendsms'
         ReturnValue = SendSMS(Self,p_stage)
         RETURN ReturnValue
      Of 'displayoutfaults'
         ReturnValue = DisplayOutFaults(Self,p_stage)
         RETURN ReturnValue
      Of 'pickengineersnotes'
         ReturnValue = PickEngineersNotes(Self,p_stage)
         RETURN ReturnValue
      Of 'formexchangeunitfilter'
         ReturnValue = FormExchangeUnitFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'formdeletepart'
         ReturnValue = FormDeletePart(Self,p_stage)
         RETURN ReturnValue
      Of 'proofofpurchase'
         ReturnValue = ProofOfPurchase(Self,p_stage)
         RETURN ReturnValue
      Of 'createnewbatch'
         ReturnValue = CreateNewBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'formwarrantyparts'
         ReturnValue = FormWarrantyParts(Self,p_stage)
         RETURN ReturnValue
      Of 'pickpaperwork'
         ReturnValue = PickPaperwork(Self,p_stage)
         RETURN ReturnValue
      Of 'bannernewjobbooking'
         ReturnValue = BannerNewJobBooking(Self,p_stage)
         RETURN ReturnValue
      Of 'addtobatch'
         ReturnValue = AddToBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'formrepairnotes'
         ReturnValue = FormRepairNotes(Self,p_stage)
         RETURN ReturnValue
      Of 'jobfaultcodes'
         ReturnValue = JobFaultCodes(Self,p_stage)
         RETURN ReturnValue
      Of 'amendaddress'
         ReturnValue = AmendAddress(Self,p_stage)
         RETURN ReturnValue
      Of 'browseauditfilter'
         ReturnValue = BrowseAuditFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'pickloanunit'
         ReturnValue = PickLoanUnit(Self,p_stage)
         RETURN ReturnValue
      Of 'jobsearch'
         ReturnValue = JobSearch(Self,p_stage)
         RETURN ReturnValue
      Of 'clearaddressdetails'
         ReturnValue = ClearAddressDetails(Self,p_stage)
         RETURN ReturnValue
      Of 'obfvalidation'
         ReturnValue = OBFValidation(Self,p_stage)
         RETURN ReturnValue
      Of 'sendanemail'
         ReturnValue = SendAnEmail(Self,p_stage)
         RETURN ReturnValue
      Of 'formpayments'
         ReturnValue = FormPayments(Self,p_stage)
         RETURN ReturnValue
      Of 'formaddtobatch'
         ReturnValue = FormAddToBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'formpickmodelstocklocation'
         ReturnValue = FormPickModelStockLocation(Self,p_stage)
         RETURN ReturnValue
      Of 'insertjob_finished'
         ReturnValue = InsertJob_Finished(Self,p_stage)
         RETURN ReturnValue
      Of 'viewcosts'
         ReturnValue = ViewCosts(Self,p_stage)
         RETURN ReturnValue
      Of 'bannerblank'
         ReturnValue = BannerBlank(Self,p_stage)
         RETURN ReturnValue
      Of 'pickexchangeunit'
         ReturnValue = PickExchangeUnit(Self,p_stage)
         RETURN ReturnValue
      Of 'bannermainmenu'
         ReturnValue = BannerMainMenu(Self,p_stage)
         RETURN ReturnValue
      Of 'pagefooter'
         ReturnValue = PageFooter(Self,p_stage)
         RETURN ReturnValue
      Of 'pickaccessory'
         ReturnValue = PickAccessory(Self,p_stage)
         RETURN ReturnValue
      Of 'formbrowseengineerhistory'
         ReturnValue = FormBrowseEngineerHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'invoicecreated'
         ReturnValue = InvoiceCreated(Self,p_stage)
         RETURN ReturnValue
      Of 'viewbouncerjob'
         ReturnValue = ViewBouncerJob(Self,p_stage)
         RETURN ReturnValue
      Of 'printroutines'
         ReturnValue = PrintRoutines(Self,p_stage)
         RETURN ReturnValue
      Of 'bannerengineeringdetails'
         ReturnValue = BannerEngineeringDetails(Self,p_stage)
         RETURN ReturnValue
      Of 'jobestimatequery'
         ReturnValue = JobEstimateQuery(Self,p_stage)
         RETURN ReturnValue
      Of 'newjobbooking'
         ReturnValue = NewJobBooking(Self,p_stage)
         RETURN ReturnValue
      Of 'browsestatuschangesfilter'
         ReturnValue = BrowseStatusChangesFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'setjobtype'
         ReturnValue = SetJobType(Self,p_stage)
         RETURN ReturnValue
      Of 'bannerbrowsejobs'
         ReturnValue = BannerBrowseJobs(Self,p_stage)
         RETURN ReturnValue
      Of 'createinvoice'
         ReturnValue = CreateInvoice(Self,p_stage)
         RETURN ReturnValue
      Of 'despatchconfirmation'
         ReturnValue = DespatchConfirmation(Self,p_stage)
         RETURN ReturnValue
      Of 'receiptfrompup'
         ReturnValue = ReceiptFromPUP(Self,p_stage)
         RETURN ReturnValue
      Of 'formchargeableparts'
         ReturnValue = FormChargeableParts(Self,p_stage)
         RETURN ReturnValue
      Of 'formaccessorynumbers'
         ReturnValue = FormAccessoryNumbers(Self,p_stage)
         RETURN ReturnValue
      Of 'bannervodacom'
         ReturnValue = BannerVodacom(Self,p_stage)
         RETURN ReturnValue
      Of 'formengineeringoption'
         ReturnValue = FormEngineeringOption(Self,p_stage)
         RETURN ReturnValue
      Of 'allocateengineer'
         ReturnValue = AllocateEngineer(Self,p_stage)
         RETURN ReturnValue
      Of 'formestimateparts'
         ReturnValue = FormEstimateParts(Self,p_stage)
         RETURN ReturnValue
      Of 'formfiltermodelnumber'
         ReturnValue = formFilterModelNumber(Self,p_stage)
         RETURN ReturnValue
      Of 'displaybrowsepayments'
         ReturnValue = DisplayBrowsePayments(Self,p_stage)
         RETURN ReturnValue
      Of 'formjoboutfaults'
         ReturnValue = FormJobOutFaults(Self,p_stage)
         RETURN ReturnValue
      Of 'formchangedop'
         ReturnValue = FormChangeDOP(Self,p_stage)
         RETURN ReturnValue
      Of 'browsestatusfilter'
         ReturnValue = BrowseStatusFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'billingconfirmation'
         ReturnValue = BillingConfirmation(Self,p_stage)
         RETURN ReturnValue
      Of 'formloanunitfilter'
         ReturnValue = FormLoanUnitFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'pickcontacthistorynotes'
         ReturnValue = PickContactHistoryNotes(Self,p_stage)
         RETURN ReturnValue
      Of 'formjobsinbatch'
         ReturnValue = FormJobsInBatch(Self,p_stage)
         RETURN ReturnValue
      End
    Else
      case lower(SELF.PageName)
        Of 'finishbatch'
             ReturnValue = FinishBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'createcreditnote'
             ReturnValue = CreateCreditNote(Self,p_stage)
           RETURN ReturnValue
        Of 'individualdespatch'
             ReturnValue = IndividualDespatch(Self,p_stage)
           RETURN ReturnValue
        Of 'jobestimate'
             ReturnValue = JobEstimate(Self,p_stage)
           RETURN ReturnValue
        Of 'prenewjobbooking'
             ReturnValue = PreNewJobBooking(Self,p_stage)
           RETURN ReturnValue
        Of 'formcontacthistory'
             ReturnValue = FormContactHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'formbrowselocationhistory'
             ReturnValue = FormBrowseLocationHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'loginform'
             ReturnValue = LoginForm(Self,p_stage)
           RETURN ReturnValue
        Of 'formbrowsecontacthistory'
             ReturnValue = FormBrowseContactHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'multiplebatchdespatch'
             ReturnValue = MultipleBatchDespatch(Self,p_stage)
           RETURN ReturnValue
        Of 'indexpage'
             ReturnValue = IndexPage(Self,p_stage)
           RETURN ReturnValue
        Of 'viewjob'
             ReturnValue = ViewJob(Self,p_stage)
           RETURN ReturnValue
        Of 'jobaccessories'
             ReturnValue = JobAccessories(Self,p_stage)
           RETURN ReturnValue
        Of 'sendsms'
             ReturnValue = SendSMS(Self,p_stage)
           RETURN ReturnValue
        Of 'displayoutfaults'
             ReturnValue = DisplayOutFaults(Self,p_stage)
           RETURN ReturnValue
        Of 'pickengineersnotes'
             ReturnValue = PickEngineersNotes(Self,p_stage)
           RETURN ReturnValue
        Of 'formexchangeunitfilter'
             ReturnValue = FormExchangeUnitFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'formdeletepart'
             ReturnValue = FormDeletePart(Self,p_stage)
           RETURN ReturnValue
        Of 'proofofpurchase'
             ReturnValue = ProofOfPurchase(Self,p_stage)
           RETURN ReturnValue
        Of 'createnewbatch'
             ReturnValue = CreateNewBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'formwarrantyparts'
             ReturnValue = FormWarrantyParts(Self,p_stage)
           RETURN ReturnValue
        Of 'pickpaperwork'
             ReturnValue = PickPaperwork(Self,p_stage)
           RETURN ReturnValue
        Of 'bannernewjobbooking'
             ReturnValue = BannerNewJobBooking(Self,p_stage)
           RETURN ReturnValue
        Of 'addtobatch'
             ReturnValue = AddToBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'formrepairnotes'
             ReturnValue = FormRepairNotes(Self,p_stage)
           RETURN ReturnValue
        Of 'jobfaultcodes'
             ReturnValue = JobFaultCodes(Self,p_stage)
           RETURN ReturnValue
        Of 'amendaddress'
             ReturnValue = AmendAddress(Self,p_stage)
           RETURN ReturnValue
        Of 'browseauditfilter'
             ReturnValue = BrowseAuditFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'pickloanunit'
             ReturnValue = PickLoanUnit(Self,p_stage)
           RETURN ReturnValue
        Of 'jobsearch'
             ReturnValue = JobSearch(Self,p_stage)
           RETURN ReturnValue
        Of 'clearaddressdetails'
             ReturnValue = ClearAddressDetails(Self,p_stage)
           RETURN ReturnValue
        Of 'obfvalidation'
             ReturnValue = OBFValidation(Self,p_stage)
           RETURN ReturnValue
        Of 'sendanemail'
             ReturnValue = SendAnEmail(Self,p_stage)
           RETURN ReturnValue
        Of 'formpayments'
             ReturnValue = FormPayments(Self,p_stage)
           RETURN ReturnValue
        Of 'formaddtobatch'
             ReturnValue = FormAddToBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'formpickmodelstocklocation'
             ReturnValue = FormPickModelStockLocation(Self,p_stage)
           RETURN ReturnValue
        Of 'insertjob_finished'
             ReturnValue = InsertJob_Finished(Self,p_stage)
           RETURN ReturnValue
        Of 'viewcosts'
             ReturnValue = ViewCosts(Self,p_stage)
           RETURN ReturnValue
        Of 'bannerblank'
             ReturnValue = BannerBlank(Self,p_stage)
           RETURN ReturnValue
        Of 'pickexchangeunit'
             ReturnValue = PickExchangeUnit(Self,p_stage)
           RETURN ReturnValue
        Of 'bannermainmenu'
             ReturnValue = BannerMainMenu(Self,p_stage)
           RETURN ReturnValue
        Of 'pagefooter'
             ReturnValue = PageFooter(Self,p_stage)
           RETURN ReturnValue
        Of 'pickaccessory'
             ReturnValue = PickAccessory(Self,p_stage)
           RETURN ReturnValue
        Of 'formbrowseengineerhistory'
             ReturnValue = FormBrowseEngineerHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'invoicecreated'
             ReturnValue = InvoiceCreated(Self,p_stage)
           RETURN ReturnValue
        Of 'viewbouncerjob'
             ReturnValue = ViewBouncerJob(Self,p_stage)
           RETURN ReturnValue
        Of 'printroutines'
             ReturnValue = PrintRoutines(Self,p_stage)
           RETURN ReturnValue
        Of 'bannerengineeringdetails'
             ReturnValue = BannerEngineeringDetails(Self,p_stage)
           RETURN ReturnValue
        Of 'jobestimatequery'
             ReturnValue = JobEstimateQuery(Self,p_stage)
           RETURN ReturnValue
        Of 'newjobbooking'
             ReturnValue = NewJobBooking(Self,p_stage)
           RETURN ReturnValue
        Of 'browsestatuschangesfilter'
             ReturnValue = BrowseStatusChangesFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'setjobtype'
             ReturnValue = SetJobType(Self,p_stage)
           RETURN ReturnValue
        Of 'bannerbrowsejobs'
             ReturnValue = BannerBrowseJobs(Self,p_stage)
           RETURN ReturnValue
        Of 'createinvoice'
             ReturnValue = CreateInvoice(Self,p_stage)
           RETURN ReturnValue
        Of 'despatchconfirmation'
             ReturnValue = DespatchConfirmation(Self,p_stage)
           RETURN ReturnValue
        Of 'receiptfrompup'
             ReturnValue = ReceiptFromPUP(Self,p_stage)
           RETURN ReturnValue
        Of 'formchargeableparts'
             ReturnValue = FormChargeableParts(Self,p_stage)
           RETURN ReturnValue
        Of 'formaccessorynumbers'
             ReturnValue = FormAccessoryNumbers(Self,p_stage)
           RETURN ReturnValue
        Of 'bannervodacom'
             ReturnValue = BannerVodacom(Self,p_stage)
           RETURN ReturnValue
        Of 'formengineeringoption'
             ReturnValue = FormEngineeringOption(Self,p_stage)
           RETURN ReturnValue
        Of 'allocateengineer'
             ReturnValue = AllocateEngineer(Self,p_stage)
           RETURN ReturnValue
        Of 'formestimateparts'
             ReturnValue = FormEstimateParts(Self,p_stage)
           RETURN ReturnValue
        Of 'formfiltermodelnumber'
             ReturnValue = formFilterModelNumber(Self,p_stage)
           RETURN ReturnValue
        Of 'displaybrowsepayments'
             ReturnValue = DisplayBrowsePayments(Self,p_stage)
           RETURN ReturnValue
        Of 'formjoboutfaults'
             ReturnValue = FormJobOutFaults(Self,p_stage)
           RETURN ReturnValue
        Of 'formchangedop'
             ReturnValue = FormChangeDOP(Self,p_stage)
           RETURN ReturnValue
        Of 'browsestatusfilter'
             ReturnValue = BrowseStatusFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'billingconfirmation'
             ReturnValue = BillingConfirmation(Self,p_stage)
           RETURN ReturnValue
        Of 'formloanunitfilter'
             ReturnValue = FormLoanUnitFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'pickcontacthistorynotes'
             ReturnValue = PickContactHistoryNotes(Self,p_stage)
           RETURN ReturnValue
        Of 'formjobsinbatch'
             ReturnValue = FormJobsInBatch(Self,p_stage)
           RETURN ReturnValue
      End
    End
    If p_File &= conthist
       ReturnValue = FormContactHistory(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= webjob
       ReturnValue = ViewJob(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= warparts
       ReturnValue = FormWarrantyParts(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobrpnot
       ReturnValue = FormRepairNotes(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobpaymt
       ReturnValue = FormPayments(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobs_alias
       ReturnValue = ViewBouncerJob(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobs
       ReturnValue = NewJobBooking(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= parts
       ReturnValue = FormChargeableParts(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobaccno
       ReturnValue = FormAccessoryNumbers(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= estparts
       ReturnValue = FormEstimateParts(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= joboutfl
       ReturnValue = FormJobOutFaults(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= muldespj
       ReturnValue = FormJobsInBatch(Self,p_stage)
       RETURN ReturnValue
    End
    Case Lower(Self.GetValue('Retry'))
      Of 'finishbatch'
        FinishBatch(Self,p_Stage)
      Of 'createcreditnote'
        CreateCreditNote(Self,p_Stage)
      Of 'individualdespatch'
        IndividualDespatch(Self,p_Stage)
      Of 'jobestimate'
        JobEstimate(Self,p_Stage)
      Of 'prenewjobbooking'
        PreNewJobBooking(Self,p_Stage)
      Of 'formbrowselocationhistory'
        FormBrowseLocationHistory(Self,p_Stage)
      Of 'loginform'
        LoginForm(Self,p_Stage)
      Of 'formbrowsecontacthistory'
        FormBrowseContactHistory(Self,p_Stage)
      Of 'multiplebatchdespatch'
        MultipleBatchDespatch(Self,p_Stage)
      Of 'indexpage'
        IndexPage(Self,p_Stage)
      Of 'jobaccessories'
        JobAccessories(Self,p_Stage)
      Of 'sendsms'
        SendSMS(Self,p_Stage)
      Of 'displayoutfaults'
        DisplayOutFaults(Self,p_Stage)
      Of 'pickengineersnotes'
        PickEngineersNotes(Self,p_Stage)
      Of 'formexchangeunitfilter'
        FormExchangeUnitFilter(Self,p_Stage)
      Of 'formdeletepart'
        FormDeletePart(Self,p_Stage)
      Of 'proofofpurchase'
        ProofOfPurchase(Self,p_Stage)
      Of 'createnewbatch'
        CreateNewBatch(Self,p_Stage)
      Of 'pickpaperwork'
        PickPaperwork(Self,p_Stage)
      Of 'bannernewjobbooking'
        BannerNewJobBooking(Self,p_Stage)
      Of 'addtobatch'
        AddToBatch(Self,p_Stage)
      Of 'jobfaultcodes'
        JobFaultCodes(Self,p_Stage)
      Of 'amendaddress'
        AmendAddress(Self,p_Stage)
      Of 'browseauditfilter'
        BrowseAuditFilter(Self,p_Stage)
      Of 'pickloanunit'
        PickLoanUnit(Self,p_Stage)
      Of 'jobsearch'
        JobSearch(Self,p_Stage)
      Of 'clearaddressdetails'
        ClearAddressDetails(Self,p_Stage)
      Of 'obfvalidation'
        OBFValidation(Self,p_Stage)
      Of 'sendanemail'
        SendAnEmail(Self,p_Stage)
      Of 'formaddtobatch'
        FormAddToBatch(Self,p_Stage)
      Of 'formpickmodelstocklocation'
        FormPickModelStockLocation(Self,p_Stage)
      Of 'insertjob_finished'
        InsertJob_Finished(Self,p_Stage)
      Of 'viewcosts'
        ViewCosts(Self,p_Stage)
      Of 'bannerblank'
        BannerBlank(Self,p_Stage)
      Of 'pickexchangeunit'
        PickExchangeUnit(Self,p_Stage)
      Of 'bannermainmenu'
        BannerMainMenu(Self,p_Stage)
      Of 'pagefooter'
        PageFooter(Self,p_Stage)
      Of 'pickaccessory'
        PickAccessory(Self,p_Stage)
      Of 'formbrowseengineerhistory'
        FormBrowseEngineerHistory(Self,p_Stage)
      Of 'invoicecreated'
        InvoiceCreated(Self,p_Stage)
      Of 'printroutines'
        PrintRoutines(Self,p_Stage)
      Of 'bannerengineeringdetails'
        BannerEngineeringDetails(Self,p_Stage)
      Of 'jobestimatequery'
        JobEstimateQuery(Self,p_Stage)
      Of 'browsestatuschangesfilter'
        BrowseStatusChangesFilter(Self,p_Stage)
      Of 'setjobtype'
        SetJobType(Self,p_Stage)
      Of 'bannerbrowsejobs'
        BannerBrowseJobs(Self,p_Stage)
      Of 'createinvoice'
        CreateInvoice(Self,p_Stage)
      Of 'despatchconfirmation'
        DespatchConfirmation(Self,p_Stage)
      Of 'receiptfrompup'
        ReceiptFromPUP(Self,p_Stage)
      Of 'bannervodacom'
        BannerVodacom(Self,p_Stage)
      Of 'formengineeringoption'
        FormEngineeringOption(Self,p_Stage)
      Of 'allocateengineer'
        AllocateEngineer(Self,p_Stage)
      Of 'formfiltermodelnumber'
        formFilterModelNumber(Self,p_Stage)
      Of 'displaybrowsepayments'
        DisplayBrowsePayments(Self,p_Stage)
      Of 'formchangedop'
        FormChangeDOP(Self,p_Stage)
      Of 'browsestatusfilter'
        BrowseStatusFilter(Self,p_Stage)
      Of 'billingconfirmation'
        BillingConfirmation(Self,p_Stage)
      Of 'formloanunitfilter'
        FormLoanUnitFilter(Self,p_Stage)
      Of 'pickcontacthistorynotes'
        PickContactHistoryNotes(Self,p_Stage)
    End
  Section('ProcessYear')
