

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A011.INC'),ONCE        !Local module procedure declarations
                     END


BannerMainMenu       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('BannerMainMenu')
  loc:formname = 'BannerMainMenu_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerMainMenu','')
    p_web._DivHeader('BannerMainMenu',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerMainMenu',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerMainMenu',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerMainMenu_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BannerMainMenu_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerMainMenu')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerMainMenu_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerMainMenu_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerMainMenu_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BannerMainMenu" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BannerMainMenu" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BannerMainMenu" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BannerMainMenu">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BannerMainMenu">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BannerMainMenu')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BannerMainMenu')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BannerMainMenu'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BannerMainMenu')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BannerMainMenu',0)

PreCopy  Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerMainMenu',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BannerMainMenu:Primed',0)

PreDelete       Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BannerMainMenu:Primed',0)
  p_web.setsessionvalue('showtab_BannerMainMenu',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerMainMenu_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerMainMenu_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BannerMainMenu:Primed',0)
heading  Routine
  packet = clip(packet) & |
    '<<table class="TopBanner"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">Main Menu<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    ''
SetHubRepair         PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:SetHubRepair -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
FilesOpened     Long
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('SetHubRepair')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'SetHubRepair' & '_' & loc:parent
  Else
    loc:divname = 'SetHubRepair'
  End
    Do OpenFiles
    Do SaveFiles
    p_web.SetSessionValue('jobe:HubRepair',1)
    p_web.SetSessionValue('jobe:HubRepairDate',Today())
    p_web.SetSessionValue('jobe:HubRepairTime',Clock())
    Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
    rtd:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
    Set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
    Loop
        If Access:REPTYDEF.Next()
            Break
        End ! If Access:REPTYDEF.Next()
        If rtd:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
            Break
        End ! If rtd:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
        If rtd:BER = 11
            If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
                p_web.SetSessionValue('job:Repair_Type',rtd:Repair_Type)
            End ! If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
            If p_web.GetSessionValue('job:Warranty_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type_Warranty') = ''
                p_web.SetSessionValue('job:Repair_Type_Warranty',rtd:Repair_Type)
            End ! If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
            Break
        End ! If rtd:BER = 11
    End ! Loop

    ! Lookup up current engineer and mark as "HUB" (DBH: 18/01/2008)
    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber = p_web.GetSessionValue('job:Ref_Number')
    joe:UserCode = p_web.GetSessionValue('job:Engineer')
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.Next()
            Break
        End ! If Access:JOBSENG.Next()
        If joe:JobNumber <> p_web.GetSessionValue('job:Ref_Number')
            Break
        End ! If joe:JobNumber <> p_web.GetSessionValue('job:Ref_Number')
        If joe:UserCode <> p_web.GetSessionValue('job:Engineer')
            Break
        End ! If joe:UserCode <> p_web.GetSessionValue('job:Engineer')
        If joe:DateAllocated > Today()
            Break
        End ! If joe:DateAllocated > Today()
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        Access:JOBSENG.TryUPdate()
        Break
    End ! Loop

    ! Check for SMS/Email Alerts (DBH: 18/01/2008)
    If p_web.GetSessionValue('jobe2:SMSNotification')
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('job:account_Number'),|
            '2ARC','SMS',p_web.GetSessionValue('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('wob:HeadAccountNumber'),|
            '2ARC','SMS',p_web.GetSessionValue('jobe2:SMSAlertNumber'),'',0,'')
        end !if (p_web.GSV('job:Who_Booked') = 'WEB')
    End ! If p_web.GetSessionValue('jobe2:SMSNotification')
    If p_web.GetSessionValue('jobe2:EmailNotification')
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('job:account_Number'),|
            '2ARC','EMAIL',p_web.GetSessionValue('jobe2:EMailAlertAddress'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('wob:HeadAccountNumber'),|
            '2ARC','EMAIL',p_web.GetSessionValue('jobe2:EMailAlertAddress'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    End ! If p_web.GetSessionValue('jobe2:SMSNotification')

    Do RestoreFiles
    Do CloseFiles
  p_web._DivHeader(loc:divname,'adiv')
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web._DivFooter()
  if loc:parent
    p_web._RegisterDivEx(loc:divname,timer,'''_parentProc='&clip(loc:parent)&'''')
  else
    p_web._RegisterDivEx(loc:divname,timer)
  End
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
SaveFiles  ROUTINE
  JOBSENG::State = Access:JOBSENG.SaveFile()               ! Save File referenced in 'Other Files' so need to inform it's FileManager
  REPTYDEF::State = Access:REPTYDEF.SaveFile()             ! Save File referenced in 'Other Files' so need to inform it's FileManager
!--------------------------------------
RestoreFiles  ROUTINE
  IF JOBSENG::State <> 0
    Access:JOBSENG.RestoreFile(JOBSENG::State)             ! Restore File referenced in 'Other Files' so need to inform it's FileManager
  END
  IF REPTYDEF::State <> 0
    Access:REPTYDEF.RestoreFile(REPTYDEF::State)           ! Restore File referenced in 'Other Files' so need to inform it's FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
     FilesOpened = False
  END
IsThisModelAlternative PROCEDURE  (func:OriginalModel,func:NewModel) ! Declare Procedure
ESNMODEL::State  USHORT
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
    esn:Model_Number = func:OriginalModel
    Set(esn:Model_Number_Key,esn:Model_Number_Key)
    Loop
        If Access:ESNMODEL.NEXT()
           Break
        End !If
        If esn:Model_Number <> func:OriginalModel      |
            Then Break.  ! End If
        !Now check the alternative models to see if the
        !scanned model matches -  (DBH: 29-10-2003)
        Access:ESNMODAL.ClearKey(esa:RefModelNumberKey)
        esa:RefNumber   = esn:Record_Number
        esa:ModelNumber = func:NewModel
        If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Found
            Return# = 1
            Break
        Else !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Error
        End !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign

    End !Loop

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  ESNMODEL::State = Access:ESNMODEL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODEL::State <> 0
    Access:ESNMODEL.RestoreFile(ESNMODEL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODEL.Close
     Access:ESNMODAL.Close
     FilesOpened = False
  END
IsDateFormatInvalid  PROCEDURE  (f:Date)                   ! Declare Procedure
  CODE
    If Len(Clip(f:Date)) <> 10
        Return 1
    End ! If Len(Clip(f:Date)) <> 10
    If Sub(f:Date,3,1) <> '/'
        Return 1
    End ! If Sub(f:Date,3,1) <> '/'

    If Sub(f:Date,6,1) <> '/'
        Return 1
    End ! If Sub(f:date,6,1) <> '/'

    Return 0
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
BouncerHistory PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:Parts            STRING(25),DIM(15)                    !
tmp:IMEINumber       STRING(30)                            !IMEI Number
code_temp            BYTE                                  !
option_temp          BYTE                                  !
bar_code_string_temp CSTRING(21)                           !
bar_code_temp        CSTRING(21)                           !
barcodeJobNumber     STRING(20)                            !
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_Despatched)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                         PROJECT(jbn_ali:Invoice_Text)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(438,1135,7521,9740),PRE(rpt),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular),THOUS
                       HEADER,AT(396,469,7521,1000),USE(?unnamed)
                         STRING('Page:'),AT(6406,208),USE(?String35),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(6823,208),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
detail                 DETAIL,AT(,,,3042),USE(?detailband)
                         STRING('Job Number:'),AT(208,52,1094,313),USE(?String2),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s8),AT(1823,260),USE(job_ali:Ref_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Unit Details:'),AT(208,521,1094,313),USE(?String2:2),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Model Number:'),AT(1823,677),USE(?String6:3),FONT(,8,,FONT:bold),TRN
                         STRING('Booked:'),AT(5156,52),FONT(,8,,FONT:bold),TRN
                         STRING(@d6b),AT(6302,52),USE(job_ali:date_booked),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Date:'),AT(4531,52,1094,313),USE(?String2:4),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@D6b),AT(6302,208),USE(job_ali:Date_Completed),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(1823,52,2604,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI), |
  LEFT,COLOR(COLOR:White)
                         STRING(@D6b),AT(6302,365),USE(job_ali:Date_Despatched),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Despatched:'),AT(5156,365),USE(?String6:8),FONT(,8,,FONT:bold),TRN
                         STRING('Completed:'),AT(5156,208),USE(?String6:7),FONT(,8,,FONT:bold),TRN
                         STRING('Reported Fault:'),AT(208,1042,1094,313),USE(?String2:5),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         TEXT,AT(1823,1042,5521,417),USE(jbn_ali:Fault_Description),FONT(,8,,,CHARSET:ANSI),TRN
                         TEXT,AT(1823,1510,5521,417),USE(jbn_ali:Invoice_Text),FONT(,8,,,CHARSET:ANSI),TRN
                         STRING(@s16),AT(6250,521),USE(job_ali:ESN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Mobile Number:'),AT(5156,833),USE(?String6:6),FONT(,8,,FONT:bold),TRN
                         STRING('Repair Details:'),AT(208,1510,1094,313),USE(?String2:6),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s16),AT(6250,677),USE(job_ali:MSN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Parts Used:'),AT(208,1979,1094,313),USE(?String2:3),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Chargeable Parts'),AT(208,2240),USE(?String37),FONT(,8,,FONT:underline),TRN
                         STRING('Warranty Parts'),AT(3594,2240),USE(?String37:2),FONT(,8,,FONT:underline),TRN
                         STRING(@s25),AT(1927,2448),USE(tmp:Parts[2]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2604),USE(tmp:Parts[3]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(1927,2604),USE(tmp:Parts[4]),FONT(,8)
                         STRING(@s25),AT(1927,2760),USE(tmp:Parts[6]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2448),USE(tmp:Parts[7]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2448),USE(tmp:Parts[8]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2760),USE(tmp:Parts[5]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2604),USE(tmp:Parts[10]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2604),USE(tmp:Parts[9]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2760),USE(tmp:Parts[12]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2760),USE(tmp:Parts[11]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2448),USE(tmp:Parts[1]),FONT(,8,,,CHARSET:ANSI)
                         LINE,AT(208,2969,7031,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Unit Type:'),AT(1823,833),USE(?String6:4),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,677),USE(job_ali:Model_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('I.M.E.I. Number:'),AT(5156,521),USE(?String6:5),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer:'),AT(1823,521),USE(?String6:2),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,521),USE(job_ali:Manufacturer),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('M.S.N.:'),AT(5156,677),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,833),USE(job_ali:Unit_Type),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s16),AT(6250,833),USE(job_ali:Mobile_Number),FONT(,8),TRN
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('BOUNCER HISTORY REPORT'),AT(1260,52,5000,417),USE(?string20),FONT(,24,,FONT:bold), |
  CENTER,TRN
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BouncerHistory')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:IMEINumber = p_web.GSV('job:ESN')
  Relate:JOBS_ALIAS.SetOpenRelated()
  Relate:JOBS_ALIAS.Open                                   ! File JOBS_ALIAS used by this procedure, so make sure it's RelationManager is open
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('BouncerHistory',ProgressWindow)            ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:ESN)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(job_ali:ESN_Key)
  ThisReport.AddRange(job_ali:ESN,tmp:IMEINumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
  END
  IF SELF.Opened
    INIMgr.Update('BouncerHistory',ProgressWindow)         ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Clear(tmp:Parts)
  PartNumber# = 0
  Access:PARTS.Clearkey(par:Part_Number_Key)
  par:Ref_Number = job_ali:Ref_Number
  Set(par:Part_Number_Key,par:Part_Number_Key)
  Loop
      If Access:PARTS.Next()
          Break
      End ! If Access:PARTS.Next()
      If par:Ref_Number <> job_ali:Ref_Number
          Break
      End ! If par:Ref_Number <> job_ali:Ref_Number
      PartNumber# += 1
      If PartNumber# > 6
          Break
      End ! If PartNumber# > 6
      tmp:Parts[PartNumber#] = par:Description
  End ! Loop
  
  PartNumber# = 6
  Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
  wpr:Ref_Number = job_ali:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.Next()
          Break
      End ! If Access:WARPARTS.Next()
      If wpr:Ref_Number <> job_ali:Ref_Number
          Break
      End ! If wpr:Ref_Number <> job_ali:Ref_Number
      PartNumber# += 1
      If PartNumber# > 12
          Break
      End ! If PartNumber# > 12
      tmp:Parts[PartNumber#] = wpr:Description
  End ! Loop
  
  barcodeJobNumber = '*' & clip(job_ali:Ref_Number) & '*'
  
  !!Barcode Bit and setup refno
  !code_temp            = 3
  !option_temp          = 0
  !
  !bar_code_string_temp = Clip(job_ali:Ref_Number)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  !
  !Settarget(Report)
  !
  !Draw_JobNo.Blank(Color:White)
  !Draw_JobNo.FontName = 'C128 High 12pt LJ3'
  !Draw_JobNo.FontStyle = font:Regular
  !Draw_JobNo.FontSize = 12
  !Draw_JobNo.Show(0,0,Bar_Code_Temp)
  !Draw_JobNo.Display()
  !
  !
  !SetTarget()
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebApp01','BouncerHistory','BouncerHistory','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

PushPack             PROCEDURE  (NetWebServerWorker p_web, string p_packet) ! Declare Procedure
packetlen   long
  CODE
  packetlen = len(clip(p_packet))
  if packetlen > 0
    p_web.ParseHTML(p_packet, 1, packetlen, NET:NoHeader)
    p_packet = ''
    packetlen = 0
  end
RefreshPage          PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('RefreshPage')
  p_web.SetValue('_parentPage','RefreshPage')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody','window.open(NewJobBooking)','PageBodyDiv')
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TimeOut') > 0
    TempTimeOut = ThisMessageBox.GetGlobalSetting('TimeOut')
    ThisMessageBox.SetGlobalSetting('TimeOut', 0)
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
  if TempTimeOut
    ThisMessageBox.SetGlobalSetting('TimeOut',TempTimeOut)
  end
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('MS Sans Serif',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !  ThisMessageBox.DontShowWindow = 1
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
        ThisMessageBox.ReturnValue = ThisMessageBox.PreOpen(LMBD:MessageText,LMBD:HeadingText,LMBD:Defaults)
        if ThisMessageBox.ReturnValue then
          self.Response = RequestCompleted
          return level:notify
        end
        ThisMessageBox.DontShowThisAgain = ?DontShowThisAgain
        ThisMessageBox.TimeOutPrompt = ?TimerCounter
        ThisMessageBox.HAControl = ?HALink
  SELF.Open(window)                                        ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    ThisMessageBox.Close()
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

