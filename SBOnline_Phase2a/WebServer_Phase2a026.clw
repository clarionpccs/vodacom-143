

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ABWMFPAR.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A026.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A018.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A021.INC'),ONCE        !Req'd for module callout resolution
                     END


Despatch:Exc         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',468)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
        p_web.SSV('GetStatus:Type','EXC')
        GetStatus(p_web)
    
        p_web.SSV('wob:ReadyToDespatch',0)
    
    
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Exchange_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    
        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)
        END
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Exchange_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Exchange_Despatched',TOday())
        p_web.SSV('job:Despatched','')
        p_web.SSV('job:Exchange_Despatched_User',p_web.GSV('BookingUserCode'))
    
        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('wob:ExcWaybillNumber',p_web.GSV('locWaybillNumber'))
    
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! if (p_web.GSV('jobe:WebJob') = 1)
    
        p_web.SSV('AddToAudit:Action','EXCHANGE UNIT DESPATCH VIA ' & p_web.GSV('job:Exchange_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))
    
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            xch:Available = 'DES'
            xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
            if (Access:EXCHANGE.TryUpdate() = Level:Benign)
    
                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = Today()
                    exh:Time = Clock()
                    exh:User = p_web.GSV('BookingUserCode')
                    exh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:ref_Number')
                    Access:EXCHHIST.TryInsert()
                END
            END
        END
    
    END ! if (p_web.GSV('BookingSite') = 'RRC')
    
    p_web.SSV('jobe:ExcSecurityPackNo',p_web.GSV('locSecurityPackID'))
    
    p_web.SSV('GetStatus:Type','EXC')
    GetStatus(p_web)
    
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','EXC')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHHIST.Close
     Access:JOBSCONS.Close
     Access:EXCHANGE.Close
     FilesOpened = False
  END
Despatch:Loa         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
    
    
        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'RRC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END
    
    
        p_web.SSV('wob:ReadyToDespatch',0)
    
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Loan_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    
    
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Loan_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Loan_Despatched',Today())
    
        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'ARC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END
    
        p_web.SSV('AddToAudit:Action','LOAN UNIT DESPATCHED VIA ' & p_web.GSV('job:Loan_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))
    
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            loa:Available = 'DES'
            if (Access:LOAN.TryUpdate() = Level:Benign)
                If (Access:LOANHIST.PrimeRecord() = Level:Benign)
                    loh:Ref_Number = loa:Ref_Number
                    loh:Date = Today()
                    loh:Time = Clock()
                    loh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:Ref_Number')
                    Access:LOANHIST.TryInsert()
                END
            END
        END
    
    
    END ! if (p_web.GSV('BookingSite') = 'RRC')
    
    p_web.SSV('GetStatus:Type','LOA')
    p_web.SSV('GetStatus:StatusNumber',901)
    GetStatus(p_web)
    
    p_web.SSV('jobe:LoaSecurityPackNo',p_web.GSV('locSecurityPackID'))
    
    
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','LOA')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOANHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOANHIST.Close
     Access:LOAN.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
FormBrowseLocationHistory PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseLocationHistory')
  loc:formname = 'FormBrowseLocationHistory_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormBrowseLocationHistory','')
    p_web._DivHeader('FormBrowseLocationHistory',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseLocationHistory_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseLocationHistory_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseLocationHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseLocationHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseLocationHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseLocationHistory" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseLocationHistory" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseLocationHistory" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Location History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Location History',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseLocationHistory">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseLocationHistory" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseLocationHistory')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseLocationHistory')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseLocationHistory'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseLocationHistory_BrowseLocationHistory_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseLocationHistory')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseLocationHistory_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseLocationHistory
      do Comment::BrowseLocationHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseLocationHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseLocationHistory',p_web.GetValue('NewValue'))
    do Value::BrowseLocationHistory
  Else
    p_web.StoreValue('lot:RecordNumber')
  End

Value::BrowseLocationHistory  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseLocationHistory --
  p_web.SetValue('BrowseLocationHistory:NoForm',1)
  p_web.SetValue('BrowseLocationHistory:FormName',loc:formname)
  p_web.SetValue('BrowseLocationHistory:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseLocationHistory')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseLocationHistory_BrowseLocationHistory_embedded_div')&'"><!-- Net:BrowseLocationHistory --></div><13,10>'
    p_web._DivHeader('FormBrowseLocationHistory_' & lower('BrowseLocationHistory') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseLocationHistory_' & lower('BrowseLocationHistory') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseLocationHistory --><13,10>'
  end
  do SendPacket

Comment::BrowseLocationHistory  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseLocationHistory_' & p_web._nocolon('BrowseLocationHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseLocationHistory:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseLocationHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseLocationHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseLocationHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseLocationHistory:Primed',0)
  p_web.StoreValue('')
FormBrowseEngineerHistory PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseEngineerHistory')
  loc:formname = 'FormBrowseEngineerHistory_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormBrowseEngineerHistory','')
    p_web._DivHeader('FormBrowseEngineerHistory',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseEngineerHistory_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseEngineerHistory_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseEngineerHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseEngineerHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseEngineerHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseEngineerHistory" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseEngineerHistory" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseEngineerHistory" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Engineer History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Engineer History',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseEngineerHistory">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseEngineerHistory" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseEngineerHistory')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseEngineerHistory')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseEngineerHistory'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseEngineerHistory_BrowseEngineerHistory_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseEngineerHistory')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseEngineerHistory_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseEngineerHistory
      do Comment::BrowseEngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseEngineerHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseEngineerHistory',p_web.GetValue('NewValue'))
    do Value::BrowseEngineerHistory
  Else
    p_web.StoreValue('joe:RecordNumber')
  End

Value::BrowseEngineerHistory  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseEngineerHistory --
  p_web.SetValue('BrowseEngineerHistory:NoForm',1)
  p_web.SetValue('BrowseEngineerHistory:FormName',loc:formname)
  p_web.SetValue('BrowseEngineerHistory:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseEngineerHistory')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseEngineerHistory_BrowseEngineerHistory_embedded_div')&'"><!-- Net:BrowseEngineerHistory --></div><13,10>'
    p_web._DivHeader('FormBrowseEngineerHistory_' & lower('BrowseEngineerHistory') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseEngineerHistory_' & lower('BrowseEngineerHistory') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseEngineerHistory --><13,10>'
  end
  do SendPacket

Comment::BrowseEngineerHistory  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseEngineerHistory_' & p_web._nocolon('BrowseEngineerHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseEngineerHistory:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseEngineerHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseEngineerHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseEngineerHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseEngineerHistory:Primed',0)
  p_web.StoreValue('')
FormBrowseContactHistory PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseContactHistory')
  loc:formname = 'FormBrowseContactHistory_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormBrowseContactHistory','')
    p_web._DivHeader('FormBrowseContactHistory',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseContactHistory',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseContactHistory_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseContactHistory_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseContactHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseContactHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseContactHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseContactHistory" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseContactHistory" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseContactHistory" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Contact History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Contact History',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseContactHistory">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseContactHistory" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseContactHistory')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseContactHistory')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseContactHistory'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseContactHistory_BrowseContactHistory_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseContactHistory')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseContactHistory_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseContactHistory
      do Comment::BrowseContactHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseContactHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseContactHistory',p_web.GetValue('NewValue'))
    do Value::BrowseContactHistory
  Else
    p_web.StoreValue('cht:Record_Number')
  End

Value::BrowseContactHistory  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseContactHistory --
  p_web.SetValue('BrowseContactHistory:NoForm',1)
  p_web.SetValue('BrowseContactHistory:FormName',loc:formname)
  p_web.SetValue('BrowseContactHistory:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseContactHistory')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseContactHistory_BrowseContactHistory_embedded_div')&'"><!-- Net:BrowseContactHistory --></div><13,10>'
    p_web._DivHeader('FormBrowseContactHistory_' & lower('BrowseContactHistory') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseContactHistory_' & lower('BrowseContactHistory') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseContactHistory --><13,10>'
  end
  do SendPacket

Comment::BrowseContactHistory  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseContactHistory_' & p_web._nocolon('BrowseContactHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseContactHistory:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseContactHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseContactHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseContactHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseContactHistory:Primed',0)
  p_web.StoreValue('')
CreateCreditNote     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCreditType        BYTE                                  !
locTotalValue        REAL                                  !
locCreditAmount      REAL                                  !
locCreditMessage     STRING(100)                           !
FilesOpened     Long
JOBPAYMT::State  USHORT
CHARTYPE::State  USHORT
STOCK::State  USHORT
PARTS::State  USHORT
JOBSINV::State  USHORT
INVOICE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('CreateCreditNote')
  loc:formname = 'CreateCreditNote_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('CreateCreditNote','')
    p_web._DivHeader('CreateCreditNote',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateCreditNote',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CreateCreditNote',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
CheckCreditAmount   ROUTINE
    IF (p_web.GSV('locCreditType') = 0)
        p_web.SSV('Hide:CreateCreditNoteButton',0)
    ELSE
        IF (p_web.GSV('locCreditAmount') > p_web.GSV('locTotalValue') OR p_web.GSV('locTotalValue') = 0 OR p_web.GSV('locCreditAmount') = 0)
            p_web.SSV('Comment:CreditAmount','Invalid Amount')
            p_web.SSV('Hide:CreateCreditNoteButton',1)
        ELSE
            p_web.SSV('Hide:CreateCreditNoteButton',0)
            p_web.SSV('Comment:CreditAmount','Required')
        END
        
    END
    
    
    
CreateCreditNote    ROUTINE
    ! Find the last credit note
    p_web.SSV('CreditNoteCreated',0)
    p_web.SSV('LastSuffix','')
    p_web.SSV('locCreditMessage','')
    found# = 0
    Access:JOBSINV.ClearKey(jov:TypeRecordKey)
    jov:RefNumber = p_web.GSV('job:Ref_Number')
    jov:Type = 'C'
    jov:RecordNumber = 0
    SET(jov:TypeRecordKey,jov:TypeRecordKey)
    LOOP UNTIL Access:JOBSINV.Next()
        IF (jov:RefNumber <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        IF (jov:Type <> 'C')
            BREAK
        END
        p_web.SSV('LastSuffix',jov:Suffix)
        found# = 1    
    END
    IF (p_web.GSV('LastSuffix') = '')
        IF (found# = 0)
            p_web.SSV('NextSuffix','')
        ELSE
            p_web.SSV('NextSuffix','A')
        END
    ELSE
        p_web.SSV('LastSuffixNumber',Val(p_web.GSV('LastSuffix')))
        p_web.SSV('NextSuffix',chr(p_web.GSV('LastSuffixNumber') + 1))
    END
    
    IF (Access:JOBSINV.PrimeRecord() = Level:Benign)
        jov:BookingAccount      = p_web.GSV('wob:HeadAccountNumber')
        jov:UserCode            = p_web.GSV('BookingUserCode')
        jov:Type                = 'C'
        jov:RefNumber           = p_web.GSV('job:Ref_Number')
        jov:InvoiceNumber       = p_web.GSV('job:Invoice_Number')
        jov:CreditAmount        = p_web.GSV('locCreditAmount')
        jov:NewTotalCost        = p_web.GSV('locTotalValue') - p_web.GSV('locCreditAmount')
        jov:Suffix              = p_web.GSV('NextSuffix')
        jov:OriginalTotalCost   = p_web.GSV('JobTotal')
        jov:NewInvoiceNumber    = ''
        jov:ChargeType          = p_web.GSV('job:Charge_Type')
        jov:RepairType          = p_web.GSV('job:Repair_Type')
        jov:HandlingFee         = p_web.GSV('HandlingFee')
        jov:ExchangeRate        = p_web.GSV('ExchangeRate')
        jov:ARCCharge           = p_web.GSV('ARCCharge')
        jov:RRCLostLoanCost     = p_web.GSV('RRCLostLoanCost')
        jov:RRCPartsCost        = p_web.GSV('PartsCost')
        jov:RRCPartsSelling     = p_web.GSV('jobe:InvRRCCPartsCost')
        jov:RRCLabour           = p_web.GSV('jobe:InvRRCCLabourCost')
        jov:ARCMarkUp           = p_web.GSV('ARCMarkup')
        jov:RRCVAT              = p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
            p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100)
        jov:Paid                = p_web.GSV('Paid')
        jov:Outstanding         = p_web.GSV('JobTotal') - p_web.GSV('Paid')
        jov:Refund              = p_web.GSV('Refund')
        IF (Access:JOBSINV.TryInsert() = Level:Benign)
            p_web.SSV('CreditSuffix',jov:Suffix)
            p_web.SSV('CreditRecordNumber',jov:RecordNumber)
        ELSE
            Access:JOBSINV.CancelAutoInc()
        END
        
    END

    ! Find the last invoice number
    p_web.SSV('LastSuffix','')
    Access:JOBSINV.ClearKey(jov:TypeRecordKey)
    jov:RefNumber = p_web.GSV('job:Ref_Number')
    jov:Type = 'I'
    jov:RecordNumber = 0
    SET(jov:TypeRecordKey,jov:TypeRecordKey)
    LOOP UNTIL Access:JOBSINV.Next()
        IF (jov:RefNumber <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        IF (jov:Type <> 'I')
            BREAK
        END
        p_web.SSV('LastSuffix',jov:Suffix)
    END
    
    IF (p_web.GSV('LastSuffix') = '')
        p_web.SSV('LastSuffix','A')
    ELSE
        p_web.SSV('LastSuffixNumber',Val(p_web.GSV('LastSuffix')))
        p_web.SSV('NextSuffix',chr(p_web.GSV('LastSuffixNumber') + 1))
    END
    
    IF (Access:JOBSINV.PrimeRecord() = Level:Benign)
        jov:BookingAccount      = p_web.GSV('wob:HeadAccountNumber')
        jov:UserCode            = p_web.GSV('BookingUserCode')
        jov:Type                = 'I'
        jov:RefNumber           = p_web.GSV('job:Ref_Number')
        jov:InvoiceNumber       = p_web.GSV('job:Invoice_Number')
        jov:CreditAmount        = p_web.GSV('locCreditAmount')
        jov:NewTotalCost        = p_web.GSV('locTotalValue') - p_web.GSV('locCreditAmount')
        jov:Suffix              = p_web.GSV('NextSuffix')
        jov:OriginalTotalCost   = p_web.GSV('JobTotal')
        jov:NewInvoiceNumber    = ''
        jov:ChargeType          = p_web.GSV('job:Charge_Type')
        jov:RepairType          = p_web.GSV('job:Repair_Type')
        jov:HandlingFee         = p_web.GSV('HandlingFee')
        jov:ExchangeRate        = p_web.GSV('ExchangeRate')
        jov:ARCCharge           = p_web.GSV('ARCCharge')
        jov:RRCLostLoanCost     = p_web.GSV('RRCLostLoanCharge')
        jov:RRCPartsCost        = p_web.GSV('PartsCost')
        jov:RRCPartsSelling     = p_web.GSV('jobe:InvRRCCPartsCost')
        jov:RRCLabour           = p_web.GSV('jobe:InvRRCCLabourCost')
        jov:ARCMarkup           = p_web.GSV('ARCMarkup')
        jov:RRCVat              = p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                            p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100)
        jov:Paid                = p_web.GSV('Paid')
        jov:Outstanding         = p_web.GSV('JobTotal') - p_web.GSV('Paid')
        jov:Refund              = p_web.GSV('Refund')
        IF (Access:JOBSINV.TryInsert() = Level:Benign)
            p_web.SSV('InvoiceSuffix',jov:Suffix)
            p_web.SSV('NewInvoiceNumber',CLIP(jov:InvoiceNumber) & '-' & | 
                Clip(tra:BranchIdentification) & Clip(jov:Suffix))
                    
            ! Record the associated invoice number on the credit note
            Access:JOBSINV.ClearKey(jov:RecordNumberKey)
            jov:RecordNumber = p_web.GSV('CreditRecordNumber')
            IF (Access:JOBSINV.TryFetch(jov:RecordNumberKey) = Level:Benign)
                jov:NewInvoiceNumber = p_web.GSV('NewInvoiceNumber')
                Access:JOBSINV.TryUpdate()
                
            END
            p_web.SSV('CreditNoteCreated',1)
        ELSE
            Access:JOBSINV.CancelAutoInc()
        END
        
    END
    
    p_web.SSV('locCreditMessage','Credit Note Created!')
    IF (p_web.GSV('JobTotal') - p_web.GSV('Paid') > 0) AND (p_web.GSV('Paid') > 0 AND | 
        p_web.GSV('Paid') <> p_web.GSV('Refund'))
        p_web.SSV('locCreditMessage','Credit Note Created! <br/>There is payment allocated to this job. If necessary, a' & | 
            ' refund should be issued to the customer.')
        
    END
    
    
        
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('Hide:CreateCreditNoteButton')
    p_web.DeleteSessionValue('Comment:CreditAmount')
    p_web.DeleteSessionValue('CreditCreditNote')
    p_web.DeleteSessionValue('locCreditMessage')
    p_web.DeleteSessionValue('locCreditType')
    p_web.DeleteSessionValue('locTotalValue')
    p_web.DeleteSessionValue('locCreditAmount')
    
OpenFiles  ROUTINE
  p_web._OpenFile(JOBPAYMT)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(INVOICE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBPAYMT)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(INVOICE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('CreateCreditNote_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locTotalValue')
    p_web.SetPicture('locTotalValue','@n-14.2')
  End
  p_web.SetSessionPicture('locTotalValue','@n-14.2')
  If p_web.IfExistsValue('locCreditAmount')
    p_web.SetPicture('locCreditAmount','@n-14.2')
  End
  p_web.SetSessionPicture('locCreditAmount','@n-14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locTotalValue',locTotalValue)
  p_web.SetSessionValue('locCreditType',locCreditType)
  p_web.SetSessionValue('locCreditAmount',locCreditAmount)
  p_web.SetSessionValue('locCreditMessage',locCreditMessage)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locTotalValue')
    locTotalValue = p_web.dformat(clip(p_web.GetValue('locTotalValue')),'@n-14.2')
    p_web.SetSessionValue('locTotalValue',locTotalValue)
  End
  if p_web.IfExistsValue('locCreditType')
    locCreditType = p_web.GetValue('locCreditType')
    p_web.SetSessionValue('locCreditType',locCreditType)
  End
  if p_web.IfExistsValue('locCreditAmount')
    locCreditAmount = p_web.dformat(clip(p_web.GetValue('locCreditAmount')),'@n-14.2')
    p_web.SetSessionValue('locCreditAmount',locCreditAmount)
  End
  if p_web.IfExistsValue('locCreditMessage')
    locCreditMessage = p_web.GetValue('locCreditMessage')
    p_web.SetSessionValue('locCreditMessage',locCreditMessage)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CreateCreditNote_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SSV('Hide:CreateCreditNoteButton',0)
  p_web.SSV('Comment:CreditAmount','Required')
  
  ! Is there anything left to credit?
  Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
  inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
  If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
      !Found
  Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
      !Error
  End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
  
  p_web.SSV('JobTotal',p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
      p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
      p_web.GSV('jobe:InvRRCCPartsCOst') * (inv:Vat_Rate_Labour/100)))
  
  p_web.SSV('CreditAmount',0)
  Access:JOBSINV.ClearKey(jov:TypeRecordKey)
  jov:RefNumber = p_web.GSV('job:Ref_Number')
  jov:Type = 'C'
  SET(jov:TypeRecordKey,jov:TypeRecordKey)
  LOOP UNTIL Access:JOBSINV.Next()
      IF (jov:RefNumber <> p_web.GSV('job:Ref_Number'))
          BREAK
      END
      IF (jov:Type <> 'C')
          BREAK
      END
      p_web.SSV('CreditAmount',p_web.GSV('CreditAmount') + jov:CreditAmount)
  END
  
  IF (p_web.GSV('CreditAmount') > p_web.GSV('JobTotal'))
      p_web.SSV('Message:Text','There is nothing remaining to credit on the selected job.')
      p_web.SSV('Message:URL','DisplayBrowsePayments')
      MessageAlert(p_web)
      EXIT
  END
  
  SentToHub(p_web)
  p_web.SSV('ExchangeRate',0)
  p_web.SSV('HandlingFee',0)
  IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
      IF (p_web.GSV('jobe:ExchangedAtRRC') = 1)
          p_web.SSV('ExchangeRate',p_web.GSV('jobe:InvoiceExchangeRate'))
      ELSE
          p_web.SSV('HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
      END
  ELSE
      IF (p_web.GSV('SentToHub') = 1)
          p_web.SSV('HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
      END
  END
  
  ! Get ARC Charges
  p_web.SSV('ARCCharge',0)
  p_web.SSV('ARCPartsCost',0)
  IF (p_web.GSV('SentToHub') = 1)
      IF (inv:ARCInvoiceDate > 0)
          p_web.SSV('ARCCharge',p_web.GSV('job:Invoice_Courier_Cost') + p_web.GSV('job:Invoice_Parts_Cost') + p_web.GSV('job:Invoice_Labour_Cost') + |
              p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
              p_web.GSV('job:Invoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
              p_web.GSV('job:Invoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
          p_web.SSV('ARCPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
      ELSE
          p_web.SSV('ARCCharge',p_web.GSV('job:Courier_Cost') + p_web.GSV('job:Labour_Cost') + p_web.GSV('job:Parts_Cost') + |
              (p_web.GSV('job:Courier_Cost') + (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
              (p_web.GSV('job:Parts_Cost') + (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
              (p_web.GSV('job:Labour_Cost') + (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
          p_web.SSV('ARCPartsCost',p_web.GSV('job:Parts_Cost'))
                  
      END
  END
  
  ! Get Loan Charges
  p_web.SSV('RRCLostLoanCharge',0)
  LoanAttachedToJob(p_web)
  IF (p_web.GSV('LoanAttAchedToJob') = 1)
      IF (p_web.GSV('SentToHub') <> 1)
          p_web.SSV('RRCLostLoanCharge',p_web.GSV('job:Invoice_Courier_Cost'))
      END
  END
  
  ! Get Parts Prices
  p_web.SSV('PartsCost',0)
  Access:PARTS.ClearKey(par:Part_Number_Key)
  par:Ref_Number = p_web.GSV('job:Ref_Number')
  SET(par:Part_Number_Key,par:Part_Number_Key)
  LOOP UNTIL Access:PARTS.Next()
      IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
          BREAK
      END
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
              p_web.SSV('PartsCost',p_web.GSV('PartsCost') + par:RRCAveragePurchaseCost)
          END
      END
  END
  
  ! Get ARC Markup
  p_web.SSV('ARCMarkup',0)
  Access:CHARTYPE.ClearKey(cha:Warranty_Key)
  cha:Warranty = 'NO'
  cha:Charge_Type = p_web.GSV('job:Charge_Type')
  IF (Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign)
      IF (cha:Zero_Parts_ARC OR cha:Zero_Parts = 'YES')
          p_web.SSV('ARCMarkup',p_web.GSV('jobe:InvRRCCPartsCost') - p_web.GSV('ARCPartsCost'))
          IF (p_web.GSV('ARCMarkup') < 0)
              p_web.SSV('ARCMarkup',0)
          END
      END
  END
  
  ! How much has been paid?
  p_web.SSV('Paid',0)
  p_web.SSV('Refund',0)
  Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
  jpt:Ref_Number = p_web.GSV('job:ref_Number')
  SET(jpt:All_Date_Key,jpt:All_Date_Key)
  LOOP UNTIL Access:JOBPAYMT.Next()
      IF (jpt:Ref_Number <> p_web.GSV('job:Ref_Number'))
          BREAK
      END
      IF (jpt:Amount > 0)
          p_web.SSV('Paid',p_web.GSV('Paid') + jpt:Amount)
      ELSE
          p_web.SSV('Refund',p_web.GSV('Refund') + (-jpt:Amount))
      END
  END
  
  !!!
  p_web.SSV('locTotalValue',p_web.GSV('JobTotal') - p_web.GSV('CreditAmount'))
  
  
      
  
  
  
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locTotalValue = p_web.RestoreValue('locTotalValue')
 locCreditType = p_web.RestoreValue('locCreditType')
 locCreditAmount = p_web.RestoreValue('locCreditAmount')
 locCreditMessage = p_web.RestoreValue('locCreditMessage')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'DisplayBrowsePayments'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CreateCreditNote_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CreateCreditNote_ChainTo')
    loc:formaction = p_web.GetSessionValue('CreateCreditNote_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'DisplayBrowsePayments'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CreateCreditNote" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CreateCreditNote" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CreateCreditNote" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Create Credit Note') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Create Credit Note',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CreateCreditNote">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CreateCreditNote" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CreateCreditNote')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CreateCreditNote')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CreateCreditNote'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CreateCreditNote')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateCreditNote_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTotalValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTotalValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTotalValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateCreditNote_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCreditType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCreditType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCreditType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCreditAmount
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCreditAmount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCreditAmount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCreateCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCreditMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCreditMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locTotalValue  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locTotalValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Total Value Of Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTotalValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTotalValue',p_web.GetValue('NewValue'))
    locTotalValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTotalValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTotalValue',p_web.dFormat(p_web.GetValue('Value'),'@n-14.2'))
    locTotalValue = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2') !
  End

Value::locTotalValue  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locTotalValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locTotalValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBoldLarge')&'">' & p_web._jsok(format(p_web.GetSessionValue('locTotalValue'),'@n-14.2')) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locTotalValue  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locTotalValue') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCreditType  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Credit Amount?')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCreditType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCreditType',p_web.GetValue('NewValue'))
    locCreditType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCreditType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCreditType',p_web.GetValue('Value'))
    locCreditType = p_web.GetValue('Value')
  End
  DO CheckCreditAmount
  do Value::locCreditType
  do SendAlert
  do Prompt::locCreditAmount
  do Value::locCreditAmount  !1
  do Comment::locCreditAmount
  do Value::buttonCreateCreditNote  !1

Value::locCreditType  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locCreditType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCreditType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('CreditNoteCreated') = 1,'disabled','')
    if p_web.GetSessionValue('locCreditType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locCreditType'',''createcreditnote_loccredittype_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCreditType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locCreditType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locCreditType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Credit Full Amount') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(p_web.GSV('CreditNoteCreated') = 1,'disabled','')
    if p_web.GetSessionValue('locCreditType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locCreditType'',''createcreditnote_loccredittype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCreditType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locCreditType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locCreditType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Credit Specific Amount') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_value')

Comment::locCreditType  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCreditAmount  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_prompt',Choose(p_web.GSV('locCreditType') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Credit Amount')
  If p_web.GSV('locCreditType') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_prompt')

Validate::locCreditAmount  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCreditAmount',p_web.GetValue('NewValue'))
    locCreditAmount = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCreditAmount
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCreditAmount',p_web.dFormat(p_web.GetValue('Value'),'@n-14.2'))
    locCreditAmount = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2') !
  End
  If locCreditAmount = ''
    loc:Invalid = 'locCreditAmount'
    loc:alert = p_web.translate('Credit Amount') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  DO CheckCreditAmount
  do Value::locCreditAmount
  do SendAlert
  do Comment::locCreditAmount
  do Value::buttonCreateCreditNote  !1

Value::locCreditAmount  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_value',Choose(p_web.GSV('locCreditType') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCreditType') <> 1)
  ! --- STRING --- locCreditAmount
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('CreditNoteCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('CreditNoteCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locCreditAmount')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locCreditAmount = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCreditAmount'',''createcreditnote_loccreditamount_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCreditAmount')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCreditAmount',p_web.GetSessionValue('locCreditAmount'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n-14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_value')

Comment::locCreditAmount  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:CreditAmount'))
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_comment',Choose(p_web.GSV('locCreditType') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCreditType') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_comment')

Validate::buttonCreateCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateCreditNote',p_web.GetValue('NewValue'))
    do Value::buttonCreateCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  DO CreateCreditNote
  do Value::buttonCreateCreditNote
  do SendAlert
  do Value::buttonPrintCreditNote  !1
  do Value::locCreditAmount  !1
  do Value::locCreditMessage  !1
  do Value::locCreditType  !1

Value::buttonCreateCreditNote  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonCreateCreditNote') & '_value',Choose(p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateCreditNote'',''createcreditnote_buttoncreatecreditnote_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateCreditNote','Create Credit Note','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('buttonCreateCreditNote') & '_value')

Comment::buttonCreateCreditNote  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonCreateCreditNote') & '_comment',Choose(p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locCreditMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCreditMessage',p_web.GetValue('NewValue'))
    locCreditMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCreditMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCreditMessage',p_web.GetValue('Value'))
    locCreditMessage = p_web.GetValue('Value')
  End

Value::locCreditMessage  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditMessage') & '_value',Choose(p_web.GSV('locCreditMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCreditMessage') = '')
  ! --- DISPLAY --- locCreditMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCreditMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditMessage') & '_value')

Comment::locCreditMessage  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditMessage') & '_comment',Choose(p_web.GSV('locCreditMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCreditMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintCreditNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintCreditNote  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonPrintCreditNote') & '_value',Choose(p_web.GSV('CreditNoteCreated') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('CreditNoteCreated') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintCreditNote','Print Credit Note','SmallButtonFixedIcon',loc:formname,,,'window.open('''& p_web._MakeURL(clip('InvoiceNote')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('buttonPrintCreditNote') & '_value')

Comment::buttonPrintCreditNote  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonPrintCreditNote') & '_comment',Choose(p_web.GSV('CreditNoteCreated') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('CreditNoteCreated') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('CreateCreditNote_locCreditType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCreditType
      else
        do Value::locCreditType
      end
  of lower('CreateCreditNote_locCreditAmount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCreditAmount
      else
        do Value::locCreditAmount
      end
  of lower('CreateCreditNote_buttonCreateCreditNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateCreditNote
      else
        do Value::buttonCreateCreditNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CreateCreditNote',0)
  locCreditType = 0
  p_web.SetSessionValue('locCreditType',locCreditType)

PreCopy  Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CreateCreditNote',0)
  ! here we need to copy the non-unique fields across
  locCreditType = 0
  p_web.SetSessionValue('locCreditType',0)

PreUpdate       Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CreateCreditNote:Primed',0)

PreDelete       Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CreateCreditNote:Primed',0)
  p_web.setsessionvalue('showtab_CreateCreditNote',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CreateCreditNote_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('CreateCreditNote_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('locCreditType') <> 1)
        If locCreditAmount = ''
          loc:Invalid = 'locCreditAmount'
          loc:alert = p_web.translate('Credit Amount') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CreateCreditNote:Primed',0)
  p_web.StoreValue('locTotalValue')
  p_web.StoreValue('locCreditType')
  p_web.StoreValue('locCreditAmount')
  p_web.StoreValue('')
  p_web.StoreValue('locCreditMessage')
  p_web.StoreValue('')
MessageAlert         PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MessageAlert -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('MessageAlert')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript"><13,10>' & | 
        'alert("' & p_web.GSV('Message:Text') & '")<13,10>' & | 
        'document.write(window.location.href = "' & p_web.GSV('Message:URL') & '")<13,10>' & |
        '</script>'
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end

VodacomSingleInvoice PROCEDURE(<NetWebServerWorker p_web>)
Default_Invoice_Company_Name_Temp STRING(30)
locInvoiceNumber     LONG
locInvoiceDate       DATE
save_tradeacc_id     USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_audit_id        USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:PrintA5Invoice   BYTE(0)
tmp:Ref_Number       STRING(20)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
RejectRecord         LONG,AUTO
save_par_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
customer_name_temp   STRING(60)
sub_total_temp       REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(30)
Name2                STRING(40)
Location             STRING(40)
RegistrationNo       STRING(30)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:CourierCost      REAL
tmp:InvoiceNumber    STRING(30)
InvoiceAddressGroup  GROUP,PRE(ia)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
DeliveryAddressGroup GROUP,PRE(da)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
                     END
locFranchiseAccountNumber STRING(20)
locDateIn            DATE
locDateOut           DATE
locTimeIn            TIME
locTimeOut           TIME
locMainOutFault      STRING(1000)
locAuditNotes        STRING(255)
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
Report               REPORT,AT(385,6646,7521,2260),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,2240,7521,6687),USE(?unnamed)
                         STRING(@s16),AT(6354,1615),USE(tmp:Ref_Number),FONT(,8),LEFT,TRN
                         STRING(@s10),AT(3906,1615),USE(wob:HeadAccountNumber),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2917,1615),USE(tmp:InvoiceNumber),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(5000,1615),USE(job:Order_Number),FONT(,8),LEFT,TRN
                         STRING(@s60),AT(156,52),USE(ia:CustomerName),FONT(,8),TRN
                         STRING(@s60),AT(4115,52),USE(da:CustomerName),FONT(,8),TRN
                         STRING(@d6b),AT(156,1615),USE(locInvoiceDate),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1458,2135,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(156,2135,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s16),AT(5000,2135,990,156),USE(job:ESN),FONT(,8),LEFT,TRN
                         STRING(@s16),AT(2917,2135),USE(job:MSN),FONT(,8),TRN
                         STRING('REPORTED FAULT'),AT(104,2552,1187),USE(?String64),FONT(,9,,FONT:bold),TRN
                         TEXT,AT(1625,2542,5313,656),USE(jbn:Fault_Description),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('ENGINEERS REPORT'),AT(83,3208,1354),USE(?String88),FONT(,9,,FONT:bold),TRN
                         STRING('PARTS USED'),AT(104,4219),USE(?String79),FONT(,9,,FONT:bold),TRN
                         STRING('Qty'),AT(1521,4219),USE(?String80),FONT(,8,,FONT:bold),TRN
                         STRING('Part Number'),AT(1917,4219),USE(?String81),FONT(,8,,FONT:bold),TRN
                         STRING('Description'),AT(3677,4219),USE(?String82),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(5719,4219),USE(?String83),FONT(,8,,FONT:bold),TRN
                         STRING('Line Cost'),AT(6677,4219),USE(?String89),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1354,4375,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,833),USE(ia:AddressLine4),FONT(,8),TRN
                         STRING('Tel: '),AT(4115,990),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,990),USE(da:TelephoneNumber),FONT(,8)
                         STRING('Vat No:'),AT(156,990),USE(?String34:3),FONT(,8),TRN
                         STRING(@s30),AT(625,990,1406,208),USE(ia:VatNumber),FONT(,8),LEFT,TRN
                         STRING('Tel:'),AT(2083,677),USE(?String34:2),FONT(,8),TRN
                         STRING(@s30),AT(2344,677,1406,208),USE(ia:TelephoneNumber),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2083,833),USE(?String35:2),FONT(,8),TRN
                         STRING(@s30),AT(2344,833,1094,208),USE(ia:FaxNumber),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(4115,208),USE(da:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(4115,365,1917,156),USE(da:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(4115,521),USE(da:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(4115,677),USE(da:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(4115,833),USE(da:AddressLine4),FONT(,8),TRN
                         STRING(@s30),AT(156,208),USE(ia:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(156,365),USE(ia:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(156,677,1885),USE(ia:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(156,521),USE(ia:AddressLine2),FONT(,8),TRN
                         TEXT,AT(1625,3208,5312,875),USE(locMainOutFault),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?BREAK1)
DETAIL                   DETAIL,AT(0,0,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(par:Quantity),FONT(,8),RIGHT,TRN
                           STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,8),TRN
                           STRING(@s25b),AT(3677,0),USE(par:Description),FONT(,8),TRN
                           STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),FONT(,8),RIGHT,TRN
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),FONT(,8),RIGHT,TRN
                         END
                       END
ANewPage               DETAIL,AT(0,0,7521,83),USE(?detailANewPage),PAGEAFTER(1)
                       END
                       FOOTER,AT(385,9104,7521,2406),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,104),USE(?String90),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,104),USE(tmp:LabourCost),FONT(,8),RIGHT,TRN
                         STRING('Time In:'),AT(1615,156),USE(?String90:3),FONT(,8),TRN
                         STRING(@d17b),AT(781,573),USE(locDateOut),FONT(,8),TRN
                         STRING('Other:'),AT(4844,417),USE(?String93),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,260),USE(tmp:PartsCost),FONT(,8),RIGHT,TRN
                         STRING(@t7b),AT(2240,573),USE(locTimeOut),FONT(,8),TRN
                         STRING(@t7b),AT(2240,156),USE(locTimeIn),FONT(,8),TRN
                         STRING('Date Out:'),AT(156,573),USE(?String90:4),FONT(,8),TRN
                         STRING('V.A.T.'),AT(4844,729),USE(?String94),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,417),USE(tmp:CourierCost),FONT(,8),RIGHT,TRN
                         STRING('Technician:'),AT(156,990),USE(?String90:6),FONT(,8),TRN
                         STRING(@s30),AT(833,990),USE(job:Engineer),FONT(,8),TRN
                         STRING('Time Out:'),AT(1615,573),USE(?String90:5),FONT(,8),TRN
                         STRING('Date In:'),AT(156,156),USE(?String90:2),FONT(,8),TRN
                         STRING('Total:'),AT(4844,990),USE(?String95),FONT(,8,,FONT:bold),TRN
                         LINE,AT(6302,937,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(6417,990),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Received By: '),AT(156,1510),USE(?String90:7),FONT(,10),TRN
                         STRING('Signature: '),AT(3021,1510),USE(?String90:8),FONT(,10),TRN
                         STRING('Date:'),AT(5885,1510),USE(?String90:9),FONT(,10),TRN
                         LINE,AT(990,1719,2031,0),USE(?Line9),COLOR(COLOR:Black)
                         LINE,AT(3646,1719,2240,0),USE(?Line9:2),COLOR(COLOR:Black)
                         LINE,AT(6198,1719,1250,0),USE(?Line9:3),COLOR(COLOR:Black)
                         STRING(@d17b),AT(781,156),USE(locDateIn),FONT(,8),TRN
                         LINE,AT(6302,573,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6396,573),USE(sub_total_temp),FONT(,8),RIGHT,TRN
                         STRING('Sub Total:'),AT(4844,573),USE(?String106),FONT(,8),TRN
                         STRING(@n14.2),AT(6396,729),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING('<128>'),AT(6344,990),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Parts:'),AT(4844,260),USE(?String92),FONT(,8),TRN
                       END
                       FORM,AT(385,406,7521,11198),USE(?unnamed:3),COLOR(COLOR:White)
                         BOX,AT(52,3125,7396,260),USE(?Box2),COLOR(COLOR:Black),FILL(00ECECECh),LINEWIDTH(1),ROUND
                         BOX,AT(52,3646,7396,260),USE(?Box2:3),COLOR(COLOR:Black),FILL(00ECECECh),LINEWIDTH(1),ROUND
                         STRING('TAX INVOICE'),AT(4167,0,3281),USE(?Text:Invoice),FONT(,18,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,417,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1250),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1406),USE(?String16),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(521,1406,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,260,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,573,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(625,729,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1406,313,208),USE(?String19),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1406,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1510,417,208),USE(?String19:2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(521,1510,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,833,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('INVOICE ADDRESS'),AT(104,1667,1229),USE(?String119),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(156,3698),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Date'),AT(156,3177),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1354,3125,0,1042),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Our Ref'),AT(1458,3177),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Invoice No'),AT(2917,3177),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Your Order No'),AT(5000,3177),USE(?String40:5),FONT(,8,,FONT:bold),TRN
                         STRING('Job No'),AT(6354,3177),USE(?String40:6),FONT(,8,,FONT:bold),TRN
                         STRING('Account No'),AT(3906,3177),USE(?String40:7),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,3385,7396,260),USE(?Box2:2),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(6250,3125,0,521),USE(?Line5:4),COLOR(COLOR:Black)
                         LINE,AT(2812,3125,0,1042),USE(?Line5:2),COLOR(COLOR:Black)
                         LINE,AT(3802,3125,0,521),USE(?Line5:8),COLOR(COLOR:Black)
                         BOX,AT(52,3906,7396,260),USE(?Box2:4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('Model'),AT(1458,3698),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Serial Number'),AT(2917,3698),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(5000,3698),USE(?String43),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,8698,2760,1302),USE(?Box4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(4688,8698,2760,1302),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(52,4219,7396,4323),USE(?Box3),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(4896,3125,0,1042),USE(?Line5:3),COLOR(COLOR:Black)
                         BOX,AT(52,1823,3438,1250),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(4010,1823,3438,1250),USE(?Box6),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('DELIVERY ADDRESS'),AT(4063,1667,1396),USE(?String119:2),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(4542,292,2917,1333),USE(de2:GlobalPrintText),FONT(,8),RIGHT,TRN
                         STRING('REG NO:'),AT(104,729,542),USE(?String16:2),FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White), |
  TRN
                         STRING('VAT NO:'),AT(104,833,542,156),USE(?String16:3),FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White), |
  TRN
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v7.30
! CW Template version    v7.3
! CW Version             7300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('VodacomSingleInvoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  locInvoiceNumber = p_web.GSV('job:Invoice_Number')
  
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBOUTFL.UseFile
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:AUDIT.UseFile
  Access:JOBSINV.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATLOG.UseFile
  DO BindFileFields
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    LocalReportTarget &= PDFReporter.IReportGenerator
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = locInvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
      
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Printing Details
        
        ! Is this a credit note?
        IF (p_web.GSV('CreditNoteCreated') = 1)
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'C'
            jov:Suffix = p_web.GSV('CreditSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
        !    PrintReport(2)
        !    Print(rpt:Detail)
            tmp:InvoiceNumber = 'CN' & Clip(tmp:InvoiceNumber) & p_web.GSV('CreditSuffix')
            Total_Temp = -jov:CreditAmount
            SetTarget(Report)
            ?Text:Invoice{prop:Text} = 'CREDIT NOTE'
            SetTarget()
            DO PrintParts
            Print(rpt:ANewPage)
        !    locDetail = ''
        
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'I'
            jov:Suffix = p_web.GSV('InvoiceSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
            tmp:InvoiceNumber = Clip(tmp:InvoiceNumber) & p_web.GSV('InvoiceSuffix')
            Total_Temp = jov:NewTotalCost
        !    PrintReport(1)
            DO PrintParts
        
        
        
        
        ELSE
        !    PrintReport(0)
            DO PrintParts
        END
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
    If Not p_web &= Null
      p_web.NoOp()
    End
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  SYSTEM{PROP:PrintMode} = 3
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Get Records
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('wob:RefNumber')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('jobe:RefNumber')
  IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
  END
  
  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
  jbn:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  SET(DEFAULT2,0) ! #12079 Pick up new "Global Text" (Bryan: 13/04/2011)
  Access:DEFAULT2.Next()
  ! Set Default Address
  ! Set Job Number / Invoice Number
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      tmp:Ref_Number = p_web.GSV('job:Ref_Number') & '-' & Clip(tra:BranchIdentification) & p_web.GSV('wob:JobNumber')
      tmp:InvoiceNumber = CLIP(p_web.GSV('job:Invoice_Number')) & '-' & Clip(tra:BranchIdentification)
  END
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  
      foundPrinted# = 0
      Access:AUDIT.Clearkey(aud:Action_Key)
      aud:Ref_Number = p_web.GSV('job:Ref_Number')
      aud:Action = 'INVOICE PRINTED'
      SET(aud:Action_Key,aud:Action_Key)
      LOOP UNTIL Access:AUDIT.Next()
          IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
              aud:Action <> 'INVOICE PRINTED')
              BREAK
          END
          IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & | 
              CLIP(tra:BranchIdentification),aud:Notes,1,1))
              foundPrinted# = 1
              BREAK
          END
      END
  
      If (foundPrinted# = 1)
          REPORT $ ?Text:Invoice{prop:Text} = 'COPY TAX INVOICE'
      END
          
      Do CreateARCRRCInvoices
  END
  
  ! Set Invoice  / Delivery Address
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
      END
  END
  CLEAR(InvoiceAddressGroup)
  
  IF (sub:OverrideHeadVATNo = 1)
      ia:VatNumber = sub:Vat_Number
  ELSE
      ia:VatNumber = tra:Vat_Number
  END
  
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = sub:Company_Name
          ia:AddressLine1 = sub:Address_Line1
          ia:AddressLine2 = sub:Address_Line2
          ia:AddressLine3 = sub:Address_Line3
          ia:AddressLine4 = sub:Postcode
          ia:TelephoneNumber = sub:Telephone_Number
          ia:FaxNumber = sub:Fax_Number
          ia:CustomerName = sub:Contact_Name
      END
      IF (ia:VatNumber = '')
          ia:VatNumber = p_web.GSV('jobe:VatNumber')
      END
  ELSE
      IF (tra:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = tra:Company_Name
          ia:AddressLine1 = tra:Address_Line1
          ia:AddressLine2 = tra:Address_Line2
          ia:AddressLine3 = tra:Address_Line3
          ia:AddressLine4 = tra:Postcode
          ia:TelephoneNumber = tra:Telephone_Number
          ia:FaxNumber = tra:Fax_Number
          ia:CustomerName = tra:Contact_Name
      END
  
  END
  IF (ia:CompanyName) = ''
      IF (p_web.GSV('job:Surname') <> '')
          IF (p_web.GSV('job:Title') = '')
              ia:CustomerName = p_web.GSV('job:Surname')
          ELSE
              IF (p_web.GSV('job:Initial') <> '')
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Initial')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              ELSE
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              END
          END
      END
  
      ia:CompanyName = p_web.GSV('job:Company_Name')
      ia:AddressLine1 = p_web.GSV('job:Address_Line1')
      ia:AddressLine2 = p_web.GSV('job:Address_Line2')
      ia:AddressLine3 = p_web.GSV('job:Address_Line3')
      ia:AddressLine4 = p_web.GSV('job:Postcode')
      ia:TelephoneNumber = p_web.GSV('job:Telephone_Number')
      ia:FaxNumber = p_web.GSV('job:Fax_Number')
  END
  
  da:CompanyName = p_web.GSV('job:Company_Name_Delivery')
  da:AddressLine1 = p_web.GSV('job:Address_Line1_Delivery')
  da:AddressLine2 = p_web.GSV('job:Address_Line2_Delivery')
  da:AddressLine3 = p_web.GSV('job:Address_Line3_Delivery')
  da:AddressLine4 = p_web.GSV('job:Postcode_Delivery')
  da:TelephoneNumber = p_web.GSV('job:Telephone_Delivery')
  
  IF (glo:WebJob <> 1)
      IF (p_web.GSV('jobe:WebJob') = 1)
          ! RRC to ARC job.
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
          IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
              da:CompanyName  = tra:Company_Name
              da:AddressLine1 = tra:Address_Line1
              da:AddressLine2 = tra:Address_Line2
              da:AddressLine3 = tra:Address_Line3
              da:AddressLine4 = tra:Postcode
              da:TelephoneNumber = tra:Telephone_Number
  
              ia:CompanyName  = tra:Company_Name
              ia:AddressLine1 = tra:Address_Line1
              ia:AddressLine2 = tra:Address_Line2
              ia:AddressLine3 = tra:Address_Line3
              ia:AddressLine4 = tra:Postcode
              ia:TelephoneNumber = tra:Telephone_Number
              ia:VatNumber = tra:Vat_Number
  
          END
      ELSE ! ! IF (p_web.GSV('jobe:WebJob = 1)
  
  
  
  
      END ! IF (p_web.GSV('jobe:WebJob = 1)
  
  END ! IF (glo:WebJob = 1)
  
  
  ! Lookups
  ! OutFault
  locMainOutFault = ''
  ! #12091 Display outfaults list like the old A5 invoice. (Bryan: 09/05/2011)
  line# = 0
  Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
  joo:JobNumber = job:Ref_Number
  Set(joo:JobNumberKey,joo:JobNumberKey)
  Loop ! Begin Loop
      If Access:JOBOUTFL.Next()
          Break
      End ! If Access:JOBOUTFL.Next()
      If joo:JobNumber <> job:Ref_Number
          Break
      End ! If joo:JobNumber <> job:Ref_Number
      If Instring('IMEI VALIDATION',joo:Description,1,1)
          Cycle
      End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
      line# += 1
      locMainOutFault = Clip(locMainoutfault) & '<13,10>' & Format(line#,@n2) & ' - ' & Clip(joo:Description)
  End ! Loop
  locMainOutFault = CLIP(Sub(locMainOutFault,3,1000))
  
  
  !Costs
  
  
  If (p_Web.GSV('BookingSite') = 'RRC')
      tmp:LabourCost = p_web.GSV('jobe:InvRRCCLabourCost')
      tmp:PartsCost = p_web.GSV('jobe:InvRRCCPartsCost')
      tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
  
      vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
          (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
          (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
      total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
          p_web.GSV('jobe:InvRRCCPartsCost') + |
          p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
  ELSE
      tmp:LabourCost = p_web.GSV('job:Invoice_Labour_Cost')
      tmp:PartsCost = p_web.GSV('job:Invoice_Parts_Cost')
      tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
      vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
          (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
          (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
      total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
          p_web.GSV('jobe:InvRRCCPartsCost') + |
          p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
  END
  Sub_Total_Temp = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
  
  
  ! Find Date In/Out
  
      locDateIn = job:Date_Booked
      locTimeIn = job:Time_Booked
      ! #11881 Use booking date unless RRC job AT ARC. Then use the "date received at ARC".
      ! (Note: Vodacom didn't specifically mention this, but i've assumed
      ! for VCP jobs to use the "received at RRC" date, rather than the booking date (Bryan: 16/03/2011)
      If (p_web.GSV('BookingSite') = 'RRC')
          IF (p_web.GSV('job:Who_Booked') = 'WEB')
              Access:AUDIT.Clearkey(aud:TypeActionKey)
              aud:Ref_Number = p_web.GSV('job:Ref_Number')
              aud:Type = 'JOB'
              aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
              SET(aud:TypeActionKey,aud:TypeActionKey)
              LOOP UNTIL Access:AUDIT.Next()
                  IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                      aud:Type <> 'JOB' OR |
                      aud:Action <> 'UNIT RECEIVED AT RRC FROM PUP')
                      BREAK
                  END
                  locDateIn = aud:Date
                  locTimeIn = aud:Time
                  BREAK
              END
  
          END ! IF (job:Who_Booked = 'WEB')
  
      ELSE ! If (glo:WebJob = 1)
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              foundDateIn# = 0
              Access:AUDSTATS.Clearkey(aus:DateChangedKey)
              aus:RefNumber = p_web.GSV('job:Ref_Number')
              aus:Type = 'JOB'
              SET(aus:DateChangedKey,aus:DateChangedKey)
              LOOP UNTIL Access:AUDSTATS.Next()
                  IF (aus:RefNumber <> p_web.GSV('job:Ref_Number') OR |
                      aus:TYPE <> 'JOB')
                      BREAK
                  END
                  IF (SUB(UPPER(aus:NewStatus),1,3) = '452')
                      locDateIn = aus:DateChanged
                      locTimeIn = aus:TimeChanged
                      foundDateIn# = 1
                      BREAK
                  END
              END
              IF (foundDateIn# = 0)
                  Access:LOCATLOG.Clearkey(lot:DateKey)
                  lot:RefNumber = p_web.GSV('job:Ref_Number')
                  SET(lot:DateKey,lot:DateKey)
                  LOOP UNTIL Access:LOCATLOG.Next()
                      IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                          BREAK
                      END
                      IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                          locDateIn = lot:TheDate
                          locTimeIn = lot:TheTime
                          BREAK
                      END
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  
  
      ! Use despatch to customer date (use last occurance), unless ARC to RRC job
      locDateOut = 0
      locTimeOut = 0
      Access:LOCATLOG.Clearkey(lot:DateKey)
      lot:RefNumber = p_web.GSV('job:Ref_Number')
      SET(lot:DateKey,lot:DateKey)
      LOOP UNTIL Access:LOCATLOG.Next()
          IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
              BREAK
          END
          IF (lot:NewLocation = Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
              locDateOut = lot:TheDate
              locTimeOut = lot:TheTime
  !            BREAK
          END
      END
  
      If (p_web.GSV('BookingSite') <> 'RRC')
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              Access:LOCATLOG.Clearkey(lot:DateKey)
              lot:RefNumber = p_web.GSV('job:Ref_Number')
              SET(lot:DateKey,lot:DateKey)
              LOOP UNTIL Access:LOCATLOG.Next()
                  IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                      BREAK
                  END
                  IF (lot:NewLocation = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')))
                      locDateOut = lot:TheDate
                      locTimeOut = lot:TheTime
                      BREAK
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  Report{PROPPRINT:Extend}=True
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='VodacomSingleInvoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  END
  LocalAttribute.Init(Report)
  Do SetStaticControlsAttributes
  



CreateARCRRCInvoices        Routine
    If (p_web.GSV('BookingSite') = 'RRC')
        IF (inv:RRCInvoiceDate = 0)
            ! RRC Invoice has not been created
            inv:RRCInvoiceDate = TODAY()
            IF (inv:ExportedRRCOracle = 0)
                p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
                p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
                p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
                p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
                p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                    
                END
                
                inv:ExportedRRCOracle = 1
                Access:INVOICE.TryUpdate()

                Line500_XML('RIV')
            END

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |                   
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                IF (p_web.GSV('LoanAttachedToJob') = 1)
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                END
                IF (p_web.GSV('jobe:Engineer48HourOption') = 1)
                    IF (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    END
                END
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)

                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web)
            END
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
            AddToAudit(p_web)
        ELSE


        END
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
        AddToAudit(p_web)
        locInvoiceDate = inv:RRCInvoiceDate
    ELSE ! If (glo:WebJob = 1)
        IF (inv:ARCInvoiceDate = 0)
            inv:ARCInvoiceDate = TODAY()
            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + |
                p_web.GSV('job:Invoice_Parts_Cost') + |
                p_web.GSV('job:Invoice_Courier_Cost'))                 
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            END
            
            

            inv:ExportedARCOracle = 1
            Access:INVOICe.TryUpdate()
            

            Line500_XML('AOW')

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                If (p_web.GSV('LoanAttachedToJob') = 1)
                    ! Changing (DBH 27/06/2006) #7584 - If loan attached, then invoice number is not being written to the audit trail
                    ! locAuditNotes   = '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost,@n14.2)
                    ! to (DBH 27/06/2006) #7584
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    ! End (DBH 27/06/2006) #7584

                End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
                If p_web.GSV('jobe:Engineer48HourOption') = 1
                    If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    End !If p_web.GSV('jobe:ExcReplcamentCharge
                End !If p_web.GSV('jobe:Engineer48HourOption = 1
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web)
            END
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
            AddToAudit(p_web)

        ELSE ! IF (inv:ARCInvoiceDate = 0)

        END ! IF (inv:ARCInvoiceDate = 0)
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
        AddToAudit(p_web)        
        locInvoiceDate = inv:ARCInvoiceDate
    END !If (glo:WebJob = 1)
PrintParts          ROUTINE
DATA
PartsAttached       BYTE(0)
CODE
    
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP UNTIL Access:PARTS.Next()
        IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        
        part_number_temp = par:Part_Number
        line_cost_temp = par:Quantity * par:Sale_Cost
        Print(rpt:Detail)
        PartsAttached = 1
    END
    IF (PartsAttached = 0)
        part_number_temp = 'No Parts Attached'
        Print(rpt:Detail)
    END
GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('locInvoiceNumber',locInvoiceNumber)
  BIND(aud:RECORD)
  BIND(aus:RECORD)
  BIND(de2:RECORD)
  BIND(def:RECORD)
  BIND(dep:RECORD)
  BIND(inv:RECORD)
  BIND(jac:RECORD)
  BIND(jbn:RECORD)
  BIND(joo:RECORD)
  BIND(jpt:RECORD)
  BIND(job:RECORD)
  BIND(jobe:RECORD)
  BIND(jov:RECORD)
  BIND(lot:RECORD)
  BIND(mfo:RECORD)
  BIND(maf:RECORD)
  BIND(par:RECORD)
  BIND(stt:RECORD)
  BIND(sub:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
  BIND(wob:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','SBOnline','VodacomSingleInvoice','VodacomSingleInvoice','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = False
  SELF.CompressImages = False
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
XFiles PROCEDURE 

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT),RESIZE, |
  CENTER,GRAY,IMM,HLP('XFiles'),SYSTEM
                       BUTTON('&OK'),AT(140,138,56,18),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,138,56,18),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:RTNORDER.Open                                     ! File RTNORDER used by this procedure, so make sure it's RelationManager is open
  Relate:SMSText.Open                                      ! File SMSText used by this procedure, so make sure it's RelationManager is open
  Relate:STOCKALX.Open                                     ! File STOCKALX used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK_ALIAS.Open                                  ! File STOCK_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEAC2.Open                                     ! File TRADEAC2 used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles',QuickWindow)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RTNORDER.Close
    Relate:SMSText.Close
    Relate:STOCKALX.Close
    Relate:STOCK_ALIAS.Close
    Relate:TRADEAC2.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles',QuickWindow)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

SendSMSText          PROCEDURE  (STRING SentJob,STRING SentAuto,STRING SentSpecial,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
       VodacomClass.SendSMS(SentJob,SentAuto,SentSpecial,p_web)
