type scripts\prototype.js   > scripts\all.js
type scripts\rico.js        >> scripts\all.js
type scripts\chrome.js      >> scripts\all.js
type scripts\netweb.js      >> scripts\all.js
type scripts\tabs.js        >> scripts\all.js
type scripts\tabsxp.js      >> scripts\all.js
type scripts\xptaskpanel.js >> scripts\all.js
type scripts\calendar2.js   >> scripts\all.js
type scripts\calendar6.js   >> scripts\all.js
type scripts\sorttable.js   >> scripts\all.js
rem keep tinymce separate
rem type scripts\tiny_mce.js    >> scripts\all.js
rem type scripts\tiny_mce_popup.js >> scripts\all.js

gzip -9 -n -f -c scripts\prototype.js   > "scripts\prototype.js.gz"
gzip -9 -n -f -c scripts\rico.js        > "scripts\rico.js.gz"
gzip -9 -n -f -c scripts\chrome.js      > "scripts\chrome.js.gz"
gzip -9 -n -f -c scripts\netweb.js      > "scripts\netweb.js.gz"
gzip -9 -n -f -c scripts\tabs.js        > "scripts\tabs.js.gz"
gzip -9 -n -f -c scripts\tabsxp.js      > "scripts\tabsxp.js.gz"
gzip -9 -n -f -c scripts\xptaskpanel.js > "scripts\xptaskpanel.js.gz"
gzip -9 -n -f -c scripts\calendar2.js   > "scripts\calendar2.js.gz"
gzip -9 -n -f -c scripts\calendar6.js   > "scripts\calendar6.js.gz"
gzip -9 -n -f -c scripts\sorttable.js   > "scripts\sorttable.js.gz"
gzip -9 -n -f -c scripts\msie.js        > "scripts\msie.js.gz"
gzip -9 -n -f -c scripts\msie6.js       > "scripts\msie6.js.gz"
gzip -9 -n -f -c scripts\tiny_mce.js       > "scripts\tiny_mce.js.gz"
gzip -9 -n -f -c scripts\tiny_mce_popup.js > "scripts\tiny_mce_popup.js.gz"
gzip -9 -n -f -c scripts\all.js       > "scripts\all.js.gz"

type styles\accordion.css   >  styles\all.css
type styles\chromemenu.css  >> styles\all.css
type styles\netweb.css      >> styles\all.css
type styles\sorttable.css   >> styles\all.css
type styles\tabs.css        >> styles\all.css
type styles\xptabs.css      >> styles\all.css
type styles\xptaskpanel.css >> styles\all.css
type styles\pccs.css            >> styles\all.css

gzip -9 -n -f -c styles\accordion.css   > "styles\accordion.css.gz"
gzip -9 -n -f -c styles\chromemenu.css  > "styles\chromemenu.css.gz"
gzip -9 -n -f -c styles\firefox.css     > "styles\firefox.css.gz"
gzip -9 -n -f -c styles\msie6.css       > "styles\msie6.css.gz"
gzip -9 -n -f -c styles\msie.css        > "styles\msie.css.gz"
gzip -9 -n -f -c styles\netweb.css      > "styles\netweb.css.gz"
gzip -9 -n -f -c styles\sorttable.css   > "styles\sorttable.css.gz"
gzip -9 -n -f -c styles\tabs.css        > "styles\tabs.css.gz"
gzip -9 -n -f -c styles\xptabs.css      > "styles\xptabs.css.gz"
gzip -9 -n -f -c styles\xptaskpanel.css > "styles\xptaskpanel.css.gz"
gzip -9 -n -f -c styles\pccsfirefox.css     > "styles\pccsfirefox.css.gz"
gzip -9 -n -f -c styles\pccs.css            > "styles\pccs.css.gz"
gzip -9 -n -f -c styles\all.css         > "styles\all.css.gz"
gzip -9 -n -f -c styles\error.css       > "styles\error.css.gz"

rem addBlob -r
rem copy blobfile.tps "Q:\NetTalk\Examples\55\Web Server\FileDownload (40)\blobfile.tps"
