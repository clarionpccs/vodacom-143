

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A018.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE2A019.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE2A024.INC'),ONCE        !Req'd for module callout resolution
                     END


ProofOfPurchase      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:DOP              DATE                                  !Date Of Purchase
tmp:JobType          STRING(1)                             !
tmp:POPType          STRING(20)                            !
tmp:WarrantyRefNo    STRING(30)                            !
tmp:POP              STRING(1)                             !
locPOPTypePassword   STRING(30)                            !
FilesOpened     Long
ACCAREAS::State  USHORT
USERS::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ProofOfPurchase')
  loc:formname = 'ProofOfPurchase_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ProofOfPurchase','')
    p_web._DivHeader('ProofOfPurchase',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ProofOfPurchase',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(USERS)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Reset Variables
  p_web.SSV('tmp:DOP','')
  p_web.SSV('tmp:JobType','')
  p_web.SSV('tmp:POPType','')
  p_web.SSV('tmp:WarrantyRefNo','')
  p_web.SSV('Comment:DOP','')
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('locPOPTypePassword','')
  p_web.SetValue('ProofOfPurchase_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:DOP')
    p_web.SetPicture('tmp:DOP','@d06b')
  End
  p_web.SetSessionPicture('tmp:DOP','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  p_web.SetSessionValue('tmp:POP',tmp:POP)
  p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:DOP')
    tmp:DOP = p_web.dformat(clip(p_web.GetValue('tmp:DOP')),'@d06b')
    p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  End
  if p_web.IfExistsValue('tmp:POP')
    tmp:POP = p_web.GetValue('tmp:POP')
    p_web.SetSessionValue('tmp:POP',tmp:POP)
  End
  if p_web.IfExistsValue('locPOPTypePassword')
    locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  End
  if p_web.IfExistsValue('tmp:POPType')
    tmp:POPType = p_web.GetValue('tmp:POPType')
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  End
  if p_web.IfExistsValue('tmp:WarrantyRefNo')
    tmp:WarrantyRefNo = p_web.GetValue('tmp:WarrantyRefNo')
    p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ProofOfPurchase_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('tmp:WarrantyRefNo',p_web.GSV('jobe2:WarrantyRefNo'))
  
  p_web.SSV('tmp:DOP',p_web.GSV('job:DOP'))
  p_web.SSV('tmp:POP',p_web.GSV('job:POP'))
  p_web.SSV('tmp:POPType',p_web.GSV('jobe:POPType'))
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:DOP = p_web.RestoreValue('tmp:DOP')
 tmp:POP = p_web.RestoreValue('tmp:POP')
 locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
 tmp:POPType = p_web.RestoreValue('tmp:POPType')
 tmp:WarrantyRefNo = p_web.RestoreValue('tmp:WarrantyRefNo')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BillingConfirmation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ProofOfPurchase_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ProofOfPurchase_ChainTo')
    loc:formaction = p_web.GetSessionValue('ProofOfPurchase_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BillingConfirmation'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ProofOfPurchase" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ProofOfPurchase" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ProofOfPurchase" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Proof Of Purchase') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Proof Of Purchase',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ProofOfPurchase">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ProofOfPurchase" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ProofOfPurchase')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ProofOfPurchase')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ProofOfPurchase'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:DOP')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ProofOfPurchase')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ProofOfPurchase_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:POP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:POP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:POP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPOPTypePassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPOPTypePassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPOPTypePassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:POPType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:POPType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:POPType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WarrantyRefNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WarrantyRefNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WarrantyRefNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:DOP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Activation / D.O.P.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:DOP',p_web.GetValue('NewValue'))
    tmp:DOP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:DOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    tmp:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  if (p_web.GSV('tmp:DOP') > today())
      p_web.SSV('comment:DOP','Invalid Date')
      p_web.SSV('tmp:DOP','')
  else !
      p_web.SSV('comment:DOP','')
  end ! if (p_web.GSV('tmp:DOP') > today())
  
  if (p_web.GSV('tmp:POP') = '' and p_web.GSV('tmp:DOP') <> '')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Found
      else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period))
          if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','C')
          else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','S')
              ! DBH #11344 - Force Chargeable If Man/Model is One Year Warranty Only
              IF (IsOneYearWarranty(p_web.GSV('job:Manufacturer'),p_web.GSV('job:Model_Number')))
                  p_web.SSV('tmp:POP','C')
              END
          end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
      else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
          p_web.SSV('tmp:POP','F')
      end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
  end ! if (p_web.GSV('tmp:POP') = '')
  do Value::tmp:DOP
  do SendAlert
  do Comment::tmp:DOP
  do Value::tmp:POP  !1
  do Comment::tmp:POP

Value::tmp:DOP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:DOP'',''proofofpurchase_tmp:dop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:DOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:DOP',p_web.GetSessionValue('tmp:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_value')

Comment::tmp:DOP  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:DOP'))
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_comment')

Prompt::tmp:POP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:POP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:POP',p_web.GetValue('NewValue'))
    tmp:POP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:POP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:POP',p_web.GetValue('Value'))
    tmp:POP = p_web.GetValue('Value')
  End
  do Value::tmp:POP
  do SendAlert

Value::tmp:POP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:POP
  loc:fieldclass = Choose(sub(' noButton',1,1) = ' ',clip('FormEntry') & ' noButton',' noButton')
  If lower(loc:invalid) = lower('tmp:POP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'F'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('F')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('F'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('First Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'S'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('S')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('S'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Second Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'O'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('O')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('O'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Out Of Box Failure') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'C'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('C')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('C'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_value')

Comment::tmp:POP  Routine
    loc:comment = ''
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_comment')

Prompt::locPOPTypePassword  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Enter Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPOPTypePassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPOPTypePassword',p_web.GetValue('NewValue'))
    locPOPTypePassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPOPTypePassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPOPTypePassword',p_web.GetValue('Value'))
    locPOPTypePassword = p_web.GetValue('Value')
  End
    locPOPTypePassword = Upper(locPOPTypePassword)
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  Access:USERS.ClearKey(use:password_key)
  use:Password = p_web.GSV('locPOPTypePassword')
  IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'AMEND POP TYPE'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('ReadOnly:POPType',0)
  
      ELSE
          p_web.SSV('Comment:POPTypePassword','User Does Not Have Access')
      END
  ELSE
      p_web.SSV('Comment:POPTypePassword','Invalid Password')
  END
  do Value::locPOPTypePassword
  do SendAlert
  do Value::tmp:POPType  !1
  do Comment::locPOPTypePassword

Value::locPOPTypePassword  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPOPTypePassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locPOPTypePassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPOPTypePassword'',''proofofpurchase_locpoptypepassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPOPTypePassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPOPTypePassword',p_web.GetSessionValueFormat('locPOPTypePassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Password And Press [TAB]') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_value')

Comment::locPOPTypePassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:POPTypePassword'))
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_comment')

Prompt::tmp:POPType  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('POP Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:POPType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:POPType',p_web.GetValue('NewValue'))
    tmp:POPType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:POPType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:POPType',p_web.GetValue('Value'))
    tmp:POPType = p_web.GetValue('Value')
  End
  do Value::tmp:POPType
  do SendAlert

Value::tmp:POPType  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:POPType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:POPType'',''proofofpurchase_tmp:poptype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:POPType'),'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:POPType',loc:fieldclass,loc:readonly,,,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType','')
  end
    packet = clip(packet) & p_web.CreateOption('------------------','',choose('' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('POP','POP',choose('POP' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('BASTION','BASTION',choose('BASTION' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('NONE','NONE',choose('NONE' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_value')

Comment::tmp:POPType  Routine
    loc:comment = ''
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WarrantyRefNo  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Ref No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WarrantyRefNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WarrantyRefNo',p_web.GetValue('NewValue'))
    tmp:WarrantyRefNo = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WarrantyRefNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:WarrantyRefNo',p_web.GetValue('Value'))
    tmp:WarrantyRefNo = p_web.GetValue('Value')
  End
  do Value::tmp:WarrantyRefNo
  do SendAlert

Value::tmp:WarrantyRefNo  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:WarrantyRefNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:WarrantyRefNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WarrantyRefNo'',''proofofpurchase_tmp:warrantyrefno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:WarrantyRefNo',p_web.GetSessionValueFormat('tmp:WarrantyRefNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_value')

Comment::tmp:WarrantyRefNo  Routine
      loc:comment = ''
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ProofOfPurchase_tmp:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:DOP
      else
        do Value::tmp:DOP
      end
  of lower('ProofOfPurchase_tmp:POP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POP
      else
        do Value::tmp:POP
      end
  of lower('ProofOfPurchase_locPOPTypePassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPOPTypePassword
      else
        do Value::locPOPTypePassword
      end
  of lower('ProofOfPurchase_tmp:POPType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POPType
      else
        do Value::tmp:POPType
      end
  of lower('ProofOfPurchase_tmp:WarrantyRefNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WarrantyRefNo
      else
        do Value::tmp:WarrantyRefNo
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)

PreCopy  Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)

PreDelete       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validate
  if (p_web.GSV('tmp:POP') = '')
      loc:alert = 'You have not selected an option'
      loc:invalid = 'tmp:POP'
      exit
  end !if (p_web.GSV('tmp:POP') = '')
  
  case (p_web.GSV('tmp:POP'))
  of 'F' orof 'S' orof 'O'
      if (p_web.GSV('tmp:DOP') = '')
          loc:alert = 'Date Of Purchase Required'
          loc:invalid = 'tmp:DOP'
          exit
      end ! if (p_web.GSV('tmp:DOP') = '')
  end ! case (p_web.GSV('tmp:POP'))
  
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = p_web.GSV('job:Manufacturer')
  if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Found
  else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Error
  end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
  
  if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
      if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
          if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              ! Out Of 2nd Year
          else ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              if (p_web.GSV('tmp:POP') <> 'O')
                  p_web.SSV('tmp:POP','S')
              end ! if (p_web.GSV('tmp:POP') <> 'O')
          end ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
      end ! if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
  end ! if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
  
  p_web.SSV('job:POP',p_web.GSV('tmp:POP'))
  p_web.SSV('job:DOP',p_web.GSV('tmp:DOP'))
  
  if (p_web.GSV('job:DOP') > 0)
      if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
          if (p_web.GSV('job:Engineer') = '')
              p_web.SSV('GetStatus:StatusNumber',305)
              p_web.SSV('GetStatus:Type','JOB')
          else ! if (p_web.GSV('job:Engineer') = ''
              p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('job:previousStatus'),1,3))
              p_web.SSV('GetStatus:Type','JOB')
          end ! if (p_web.GSV('job:Engineer') = ''
          getStatus(p_web)
      end ! if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
  
      case p_web.GSV('job:POP')
      of 'F'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (MFTR)')
      of 'S'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (2ND YR)')
      of 'O'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (OBF)')
      of 'C'
          p_web.SSV('job:Warranty_job','NO')
          p_web.SSV('job:Chargeable_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Charge_Type','NON-WARRANTY')
      end ! case p_web.GSV('job:POP')
  end ! if (p_web.GSV('job:DOP') > 0)
  
  p_web.SSV('jobe:POPType',p_web.GSV('tmp:POPType')) ! #11912 Never saved this field. (Bryan: 14/02/2011)
  
  
  
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          locPOPTypePassword = Upper(locPOPTypePassword)
          p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.StoreValue('tmp:DOP')
  p_web.StoreValue('tmp:POP')
  p_web.StoreValue('locPOPTypePassword')
  p_web.StoreValue('tmp:POPType')
  p_web.StoreValue('tmp:WarrantyRefNo')
CalculateBilling     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
local:CFaultCode        String(255)
local:CIndex            Byte(0)
local:CRepairType       String(30)
local:WFaultCOde        String(255)
local:WIndex            Byte(0)
local:WRepairType       String(30)
local:KeyRepair         Long()
local:FaultCode         String(255)
FilesOpened     BYTE(0)
  CODE
    do openfiles

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseInvTextForFaults = True
            If man:AutoRepairType = True
                ! Get the main out fault (DBH: 23/11/2007)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer = p_web.GSV('job:Manufacturer')
                maf:MainFault = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                    ! Check the out faults list (DBH: 23/11/2007)
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber = p_web.GSV('job:Ref_Number')
                    Set(joo:JobNumberKey,joo:JobNumberKey)
                    Loop
                        If Access:JOBOUTFL.Next()
                            Break
                        End ! If Access:JOBOUTFL.Next()
                        If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                            Break
                        End ! If joo:JobNumber <> job:Ref_Number

                        ! Get the lookup details (e.g. index and repair types) (DBH: 23/11/2007)
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field = joo:FaultCode
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            ! Is there a repair type? (DBH: 23/11/2007)
                            If mfo:RepairType <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:CIndex
                                    local:CFaultCode = mfo:Field
                                    local:CIndex = mfo:ImportanceLevel
                                    local:CRepairType = mfo:RepairType
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairType <> ''
                            If mfo:RepairTypeWarranty <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:WIndex
                                    local:WFaultCode = mfo:Field
                                    local:WIndex = mfo:ImportanceLevel
                                    local:WRepairType = mfo:RepairTypeWarranty
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairTypeWarranty <> ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End ! Loop

                    ! Inserting (DBH 30/04/2008) # 9723 - Is there a "Key Repair" fault code? If so, that is counted as the OUT Fault
                    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:KeyRepair = 1
                    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        ! Found
                        local:KeyRepair = map:Field_Number
                    Else ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        local:KeyRepair = 0
                    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                    ! End (DBH 30/04/2008) #9723

! Changing (DBH 21/05/2008) # 9723 - Can be more than one part out fault
!                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
!                    map:Manufacturer = job:Manufacturer
!                    map:MainFault = 1
!                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
! to (DBH 21/05/2008) # 9723
                    ! Is the out fault held on the parts. If so, check for the highest repair index there too (DBH: 23/11/2007)
                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:MainFault = 1
                    Set(map:MainFaultKey,map:MainFaultKey)
                    Loop ! Begin Loop
                        If Access:MANFAUPA.Next()
                            Break
                        End ! If Access:MANFAUPA.Next()
                        If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                            Break
                        End ! If map:Manufacturer <> job:Manufacturer
                        If map:MainFault <> 1
                            Break
                        End ! If map:MainFault <> 1

! End (DBH 21/05/2008) #9723
                        ! Check the Warranty Parts (DBH: 23/11/2007)
                        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = p_web.GSV('job:Ref_Number')
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.Next()
                                Break
                            End ! If Access:WARPARTS.Next()
                            If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                Break
                            End ! If wpr:Ref_Number <> job:Ref_Number

                            ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                            Case local:KeyRepair
                            Of 1
                                If wpr:Fault_Code1 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code1 <> 1
                            Of 2
                                If wpr:Fault_Code2 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code2 <> 1
                            Of 3
                                If wpr:Fault_Code3 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 4
                                If wpr:Fault_Code4 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 5
                                If wpr:Fault_Code5 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 6
                                If wpr:Fault_Code6 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 7
                                If wpr:Fault_Code7 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 8
                                If wpr:Fault_Code8 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 9
                                If wpr:Fault_Code9 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 10
                                If wpr:Fault_Code10 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 11
                                If wpr:Fault_Code11 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 12
                                If wpr:Fault_Code12 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            End ! Case local:KeyRepair
                            ! End (DBH 30/04/2008) #9723

                            ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                            mfo:Field_Number = maf:Field_NUmber
                            Case map:Field_Number
                            Of 1
                                mfo:Field = wpr:Fault_Code1
                            Of 2
                                mfo:Field = wpr:Fault_Code2
                            Of 3
                                mfo:Field = wpr:Fault_Code3
                            Of 4
                                mfo:Field = wpr:Fault_Code4
                            Of 5
                                mfo:Field = wpr:Fault_Code5
                            Of 6
                                mfo:Field = wpr:Fault_Code6
                            Of 7
                                mfo:Field = wpr:Fault_Code7
                            Of 8
                                mfo:Field = wpr:Fault_Code8
                            Of 9
                                mfo:Field = wpr:Fault_Code9
                            Of 10
                                mfo:Field = wpr:Fault_Code10
                            Of 11
                                mfo:Field = wpr:Fault_Code11
                            Of 12
                                mfo:Field = wpr:Fault_Code12
                            End ! Case map:Field_Number
                            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                ! Is there a repair type? (DBH: 23/11/2007)
                                If mfo:RepairType <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:CIndex
                                        local:CFaultCode = mfo:Field
                                        local:CIndex = mfo:ImportanceLevel
                                        local:CRepairType = mfo:RepairType
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairType <> ''
                                If mfo:RepairTypeWarranty <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:WIndex
                                        local:WFaultCode = mfo:Field
                                        local:WIndex = mfo:ImportanceLevel
                                        local:WRepairType = mfo:RepairTypeWarranty
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairTypeWarranty <> ''
                            End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                        End ! Loop

                        ! Check the estimate parts (DBH: 23/11/2007)
                        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Estimate_Accepted') <> 'YES' And p_web.GSV('job:Estimate_Rejected') <> 'YES'
                            Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                            epr:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(epr:Part_Number_Key,epr:Part_Number_Key)
                            Loop
                                If Access:ESTPARTS.Next()
                                    Break
                                End ! If Access:ESTPARTS.Next()
                                If epr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If epr:Ref_Number <> job:Ref_Number

                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If epr:Fault_Code1 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code1 <> 1
                                Of 2
                                    If epr:Fault_Code2 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code2 <> 1
                                Of 3
                                    If epr:Fault_Code3 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 4
                                    If epr:Fault_Code4 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 5
                                    If epr:Fault_Code5 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 6
                                    If epr:Fault_Code6 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 7
                                    If epr:Fault_Code7 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 8
                                    If epr:Fault_Code8 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 9
                                    If epr:Fault_Code9 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 10
                                    If epr:Fault_Code10 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 11
                                    If epr:Fault_Code11 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 12
                                    If epr:Fault_Code12 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = epr:Fault_Code1
                                Of 2
                                    mfo:Field = epr:Fault_Code2
                                Of 3
                                    mfo:Field = epr:Fault_Code3
                                Of 4
                                    mfo:Field = epr:Fault_Code4
                                Of 5
                                    mfo:Field = epr:Fault_Code5
                                Of 6
                                    mfo:Field = epr:Fault_Code6
                                Of 7
                                    mfo:Field = epr:Fault_Code7
                                Of 8
                                    mfo:Field = epr:Fault_Code8
                                Of 9
                                    mfo:Field = epr:Fault_Code9
                                Of 10
                                    mfo:Field = epr:Fault_Code10
                                Of 11
                                    mfo:Field = epr:Fault_Code11
                                Of 12
                                    mfo:Field = epr:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                            End ! Loop
                        Else ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:PARTS.Clearkey(par:Part_Number_Key)
                            par:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.Next()
                                    Break
                                End ! If Access:PARTS.Next()
                                If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If par:Ref_Number <> job:Ref_Number
                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If par:Fault_Code1 <> 1
                                        Cycle
                                    End ! If par:Fault_Code1 <> 1
                                Of 2
                                    If par:Fault_Code2 <> 1
                                        Cycle
                                    End ! If par:Fault_Code2 <> 1
                                Of 3
                                    If par:Fault_Code3 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 4
                                    If par:Fault_Code4 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 5
                                    If par:Fault_Code5 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 6
                                    If par:Fault_Code6 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 7
                                    If par:Fault_Code7 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 8
                                    If par:Fault_Code8 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 9
                                    If par:Fault_Code9 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 10
                                    If par:Fault_Code10 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 11
                                    If par:Fault_Code11 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 12
                                    If par:Fault_Code12 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = par:Fault_Code1
                                Of 2
                                    mfo:Field = par:Fault_Code2
                                Of 3
                                    mfo:Field = par:Fault_Code3
                                Of 4
                                    mfo:Field = par:Fault_Code4
                                Of 5
                                    mfo:Field = par:Fault_Code5
                                Of 6
                                    mfo:Field = par:Fault_Code6
                                Of 7
                                    mfo:Field = par:Fault_Code7
                                Of 8
                                    mfo:Field = par:Fault_Code8
                                Of 9
                                    mfo:Field = par:Fault_Code9
                                Of 10
                                    mfo:Field = par:Fault_Code10
                                Of 11
                                    mfo:Field = par:Fault_Code11
                                Of 12
                                    mfo:Field = par:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign

                            End ! Loop
                        End ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            End ! If man:AutoRepairType = True
        End ! If man:UseInvTextForFaults = True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    ! If the charge types are already filled then assume "overwrite" box has been ticked.
!    ! So don't change. (DBH: 23/11/2007)
!    if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
!        p_web.SSV('job:Repair_Type',local:CRepairType)
!    end ! if (p_web.GSV('jobe:COverwriteRepairType') = 0)
!
!    if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
!        p_web.SSV('job:Warranty_Repair_Type',local:WRepairType)
!    end ! if (p_web.GSV('jobe:WOverwriteRepairType') = 0)

    if (local:CRepairType <> '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType = '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType <> '' and local:WRepairType = '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:Warranty_Job','NO')
            end
            transferParts_C(p_web)
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:warranty_Job','NO')
            end
            p_web.SSV('job:chargeable_Job','YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')


    ! Inserting (DBH 13/06/2006) #6733 - Work out if to use Warranty or Chargeable Fault Code
    local:FaultCode = ''
    if (p_web.GSV('job:Chargeable_Job') <> 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        local:FaultCode = local:WFaultCode
    End ! If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') <> 'YES')
        local:FaultCode = local:CFaultCode
    End ! If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        If local:WFaultCode <> ''
            local:FaultCode = local:WFaultCode
        Else
            local:FaultCode = local:CFaultCode
        End ! If tmp:FaultCodeW_T <> ''
    End ! If job:Chargeable_Job = 'YES' ANd job:Warranty_Job = 'YES'
    ! End (DBH 13/06/2006) #6733



    Access:MANFAULT.Clearkey(maf:mainFaultKey)
    maf:manufacturer    = p_web.GSV('job:manufacturer')
    maf:mainFault    = 1
    if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Found
        if (maf:field_Number < 13)
            p_web.SSV('job:fault_Code' & maf:field_Number,local:faultCode)
        else ! if (maf:field_Number < 13)
            p_web.SSV('wob:faultCode' & maf:field_Number,local:faultCode)
        end ! if (maf:field_Number < 13)
    else ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)

    do closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:MANFAULO.Close
     Access:MANFAUPA.Close
     Access:WARPARTS.Close
     Access:ESTPARTS.Close
     Access:PARTS.Close
     FilesOpened = False
  END
TransferParts_W      PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
  CODE
    count# = 0

    access:parts.clearkey(par:part_number_key)
    par:ref_number  = p_web.GSV('job:ref_number')
    set(par:part_number_key,par:part_number_key)
    loop
        if access:parts.next()
           break
        end !if
        if par:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        If par:pending_ref_number <> ''
            access:ordpend.clearkey(ope:ref_number_key)
            ope:ref_number = par:pending_ref_number
            if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                ope:part_type = 'JOB'
                access:ordpend.update()
            End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
        End!If wpr:pending_ref_number <> ''
        If par:order_number <> ''
            access:ordparts.clearkey(orp:record_number_key)
            orp:record_number   = par:order_number
            If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                orp:part_type = 'JOB'
                access:ordparts.update()
            End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
        End!If wpr:order_number <> ''
        get(warparts,0)
        if access:warparts.primerecord() = Level:Benign
            record_number$  = wpr:record_number
            wpr:record      :=: par:record
            wpr:record_number   = record_number$
            if access:warparts.insert()
                access:warparts.cancelautoinc()
            End!if access:parts.insert()
        End!if access:parts.primerecord() = Level:Benign
        Delete(parts)
        count# += 1
    end !loop

    if count# <> 0

        get(audit,0)
        if access:audit.primerecord() = level:benign
            aud:ref_number    = p_web.GSV('job:ref_number')
            aud:date          = today()
            aud:time          = clock()
            aud:type          = 'JOB'
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign
    End!if count# <> 0
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = p_web.GSV('job:Model_NUmber')
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        pendingJob(p_web)
        p_web.SSV('job:Manufacturer',mod:Manufacturer)
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
TransferParts_C      PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
  CODE
    count# = 0

    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = p_web.GSV('job:ref_number')
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        If wpr:pending_ref_number <> ''
            access:ordpend.clearkey(ope:ref_number_key)
            ope:ref_number = wpr:pending_ref_number
            if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                ope:part_type = 'JOB'
                access:ordpend.update()
            End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
        End!If wpr:pending_ref_number <> ''
        If wpr:order_number <> ''
            access:ordparts.clearkey(orp:record_number_key)
            orp:record_number   = wpr:order_number
            If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                orp:part_type = 'JOB'
                access:ordparts.update()
            End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
        End!If wpr:order_number <> ''
        get(parts,0)
        if access:parts.primerecord() = Level:Benign
            record_number$  = par:record_number
            par:record      :=: wpr:record
            par:record_number   = record_number$
            if access:parts.insert()
                access:parts.cancelautoinc()
            End!if access:parts.insert()
        End!if access:parts.primerecord() = Level:Benign
        Delete(warparts)
        count# += 1
    end !loop

    if count# <> 0
        get(audit,0)
        if access:audit.primerecord() = level:benign
            aud:ref_number    = p_web.GSV('job:ref_number')
            aud:date          = today()
            aud:time          = clock()
            aud:type          = 'JOB'
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'WARRANTY PARTS TRANSFERRED TO CHARGEABLE'
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign

    End!if count# <> 0
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = p_web.GSV('job:Model_NUmber')
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        pendingJob(p_web)
        p_web.SSV('job:Manufacturer',mod:Manufacturer)
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
BillingConfirmation  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CChargeType      STRING(30)                            !
tmp:CRepairType      STRING(30)                            !
tmp:CConfirmRepairType STRING(30)                          !
tmp:WChargeType      STRING(30)                            !
tmp:WRepairType      STRING(30)                            !
tmp:WConfirmRepairType BYTE                                !
tmp:Password         STRING(30)                            !
FilesOpened     Long
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
tmp:CChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:CRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
tmp:WChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:WRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BillingConfirmation')
  loc:formname = 'BillingConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BillingConfirmation','')
    p_web._DivHeader('BillingConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BillingConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:CChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CRepairType')
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:WChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WRepairType')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:CChargeType')
    tmp:CChargeType = p_web.GetValue('tmp:CChargeType')
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  End
  if p_web.IfExistsValue('tmp:CRepairType')
    tmp:CRepairType = p_web.GetValue('tmp:CRepairType')
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  End
  if p_web.IfExistsValue('tmp:CConfirmRepairType')
    tmp:CConfirmRepairType = p_web.GetValue('tmp:CConfirmRepairType')
    p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  End
  if p_web.IfExistsValue('tmp:WChargeType')
    tmp:WChargeType = p_web.GetValue('tmp:WChargeType')
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  End
  if p_web.IfExistsValue('tmp:WRepairType')
    tmp:WRepairType = p_web.GetValue('tmp:WRepairType')
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  End
  if p_web.IfExistsValue('tmp:WConfirmRepairType')
    tmp:WConfirmRepairType = p_web.GetValue('tmp:WConfirmRepairType')
    p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BillingConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      CalculateBilling(p_web)
  
      p_web.SSV('tmp:CChargeType',p_web.GSV('job:Charge_Type'))
      p_web.SSV('tmp:WChargeType',p_web.GSV('job:Warranty_Charge_Type'))
      p_web.SSV('tmp:CConfirmRepairType',p_web.GSV('jobe:COverwriteRepairType'))
      p_web.SSV('tmp:WConfirmRepairType',p_web.GSV('jobe:WOverwriteRepairType'))
      p_web.SSV('tmp:CRepairType',p_web.GSV('job:Repair_Type'))
      p_web.SSV('tmp:WRepairType',p_web.GSV('job:Repair_Type_Warranty'))
  
      if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND REPAIR TYPES'))
          p_web.SSV('ReadOnly:RepairType',1)
      else!if (securityCheckFailed('JOBS - AMEND REPAIR TYPES',p_web.GSV('BookingPassword'))
          p_web.SSV('ReadOnly:RepairType',0)
      end !if (securityCheckFailed('JOBS - AMEND REPAIR TYPES',p_web.GSV('BookingPassword'))
  
      p_web.SSV('locEngineerSkillLevel',0)
      if (p_web.GSV('job:Engineer') <> '')
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_Code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              p_web.SSV('locEngineerSkillLevel',use:SkillLevel)
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (p_web.GSV('job:Engineer') <> '')
  
      p_web.storeValue('JobCompleteProcess')
  
      if (p_web.GSV('JobCompleteProcess') = 1)
          if (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Chargeable_Job') = 'YES')
              if (p_web.GSV('job:Estimate_Accepted') <> 'YES' and |
                  p_web.GSV('job:Estimate_Rejected') <> 'YES' and |
                  p_web.GSV('job:Estimate_Ready') <> 'YES')
  
                  jobPricingRoutine(p_web)
  
                  p_web.SSV('TotalPrice:Type','E')
  
                  totalPrice(p_web)
  
                  if (p_web.GSV('TotalPrice:Total') < p_web.GSV('job:Estimate_If_Over'))
                      p_web.SSV('nextURL','EstimateQuery')
                      p_web.SSV('JobCompleteProcess',0)
                  else ! if (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_If_Over'))
                      p_web.SSV('nextURL','ViewJob')
                      p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
                      p_web.SSV('job:Estimate_Ready','YES')
  
                      p_web.SSV('JobCompleteProcess',0)
                  
                      p_web.SSV('Hide:EstimateReady',0)
                      p_web.SSV('GetStatus:Type','JOB')
                      p_web.SSV('GetStatus:StatusNumber',510)
                      getStatus(p_web)
  
                  end ! if (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_If_Over'))
              end ! if (p_web.GSV('job:Estimate_Accepted') <> 'YES' and |
          else !
              p_web.SSV('nextURL','ViewJob')
          end ! if (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Chargeable_Job') = 'YES')
      else !
          p_web.StoreValue('passedURL')
          IF (p_web.GSV('passedURL') <> '')
              p_web.SSV('nextURL',p_web.GSV('passedURL'))
          ELSE
              p_web.SSV('nextURL','ViewCosts')
          END
                 
      end ! if (p_web.GSV('Job:CompleteProcess') = 1)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:CChargeType = p_web.RestoreValue('tmp:CChargeType')
 tmp:CRepairType = p_web.RestoreValue('tmp:CRepairType')
 tmp:CConfirmRepairType = p_web.RestoreValue('tmp:CConfirmRepairType')
 tmp:WChargeType = p_web.RestoreValue('tmp:WChargeType')
 tmp:WRepairType = p_web.RestoreValue('tmp:WRepairType')
 tmp:WConfirmRepairType = p_web.RestoreValue('tmp:WConfirmRepairType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('NextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BillingConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BillingConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('BillingConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BillingConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BillingConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BillingConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Billing Confirmation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Billing Confirmation',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BillingConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BillingConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BillingConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('job:Chargeable_Job') = 'YES'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable') & ''''
        End
        If p_web.GSV('job:Warranty_job') = 'YES'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_BillingConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BillingConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('job:Chargeable_Job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CChargeType')
    ElsIf p_web.GSV('job:Warranty_job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WChargeType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('job:Chargeable_Job') = 'YES'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('job:Warranty_job') = 'YES'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_BillingConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('job:Chargeable_Job') = 'YES'
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('job:Warranty_job') = 'YES'
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Chargeable') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BillingConfirmation_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CConfirmRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('job:Warranty_job') = 'YES'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Warranty') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BillingConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WConfirmRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::tmp:CChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CChargeType',p_web.GetValue('NewValue'))
    tmp:CChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:CChargeType',p_web.GetValue('Value'))
    tmp:CChargeType = p_web.GetValue('Value')
  End
    tmp:CChargeType = Upper(tmp:CChargeType)
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  do Value::tmp:CChargeType
  do SendAlert
  do Value::tmp:CRepairType  !1

Value::tmp:CChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:CChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CChargeType'',''billingconfirmation_tmp:cchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:CChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
    p_web.SetSessionValue('tmp:CChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CChargeType_OptionView)
  tmp:CChargeType_OptionView{prop:filter} = 'upper(cha:Warranty ) <> <39>YES<39>'
  tmp:CChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(tmp:CChargeType_OptionView)
  Loop
    Next(tmp:CChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
      p_web.SetSessionValue('tmp:CChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_value')

Comment::tmp:CChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CRepairType',p_web.GetValue('NewValue'))
    tmp:CRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:CRepairType',p_web.GetValue('Value'))
    tmp:CRepairType = p_web.GetValue('Value')
  End
    tmp:CRepairType = Upper(tmp:CRepairType)
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  do Value::tmp:CRepairType
  do SendAlert

Value::tmp:CRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:CRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CRepairType'',''billingconfirmation_tmp:crepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
    p_web.SetSessionValue('tmp:CRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CRepairType_OptionView)
  tmp:CRepairType_OptionView{prop:filter} = 'upper(rtd:manufacturer) = upper(<39>' & p_web.GSV('job:manufacturer') & '<39>) and upper(rtd:chargeable) = <39>YES<39> and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel')
  tmp:CRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(tmp:CRepairType_OptionView)
  Loop
    Next(tmp:CRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
      p_web.SetSessionValue('tmp:CRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_value')

Comment::tmp:CRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CConfirmRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CConfirmRepairType',p_web.GetValue('NewValue'))
    tmp:CConfirmRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CConfirmRepairType
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:CConfirmRepairType',p_web.GetValue('Value'))
    tmp:CConfirmRepairType = p_web.GetValue('Value')
  End
  do Value::tmp:CConfirmRepairType
  do SendAlert

Value::tmp:CConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:CConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CConfirmRepairType'',''billingconfirmation_tmp:cconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:CConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_value')

Comment::tmp:CConfirmRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WChargeType',p_web.GetValue('NewValue'))
    tmp:WChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:WChargeType',p_web.GetValue('Value'))
    tmp:WChargeType = p_web.GetValue('Value')
  End
    tmp:WChargeType = Upper(tmp:WChargeType)
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  do Value::tmp:WChargeType
  do SendAlert
  do Value::tmp:WRepairType  !1

Value::tmp:WChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:WChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WChargeType'',''billingconfirmation_tmp:wchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:WChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
    p_web.SetSessionValue('tmp:WChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WChargeType_OptionView)
  tmp:WChargeType_OptionView{prop:filter} = 'cha:Warranty = <39>YES<39>'
  tmp:WChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(tmp:WChargeType_OptionView)
  Loop
    Next(tmp:WChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
      p_web.SetSessionValue('tmp:WChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_value')

Comment::tmp:WChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WRepairType',p_web.GetValue('NewValue'))
    tmp:WRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:WRepairType',p_web.GetValue('Value'))
    tmp:WRepairType = p_web.GetValue('Value')
  End
    tmp:WRepairType = Upper(tmp:WRepairType)
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  do Value::tmp:WRepairType
  do SendAlert

Value::tmp:WRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:WRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WRepairType'',''billingconfirmation_tmp:wrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
    p_web.SetSessionValue('tmp:WRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WRepairType_OptionView)
  tmp:WRepairType_OptionView{prop:filter} = 'upper(rtd:manufacturer) = upper(<39>' & p_web.GSV('job:manufacturer') & '<39>) and upper(rtd:warranty) = <39>YES<39> and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel')
  tmp:WRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(tmp:WRepairType_OptionView)
  Loop
    Next(tmp:WRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
      p_web.SetSessionValue('tmp:WRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_value')

Comment::tmp:WRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WConfirmRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WConfirmRepairType',p_web.GetValue('NewValue'))
    tmp:WConfirmRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WConfirmRepairType
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:WConfirmRepairType',p_web.GetValue('Value'))
    tmp:WConfirmRepairType = p_web.GetValue('Value')
  End
  do Value::tmp:WConfirmRepairType
  do SendAlert

Value::tmp:WConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:WConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:WConfirmRepairType'',''billingconfirmation_tmp:wconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:WConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:WConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_value')

Comment::tmp:WConfirmRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BillingConfirmation_tmp:CChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CChargeType
      else
        do Value::tmp:CChargeType
      end
  of lower('BillingConfirmation_tmp:CRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CRepairType
      else
        do Value::tmp:CRepairType
      end
  of lower('BillingConfirmation_tmp:CConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CConfirmRepairType
      else
        do Value::tmp:CConfirmRepairType
      end
  of lower('BillingConfirmation_tmp:WChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WChargeType
      else
        do Value::tmp:WChargeType
      end
  of lower('BillingConfirmation_tmp:WRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WRepairType
      else
        do Value::tmp:WRepairType
      end
  of lower('BillingConfirmation_tmp:WConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WConfirmRepairType
      else
        do Value::tmp:WConfirmRepairType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)

PreCopy  Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
        If (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CConfirmRepairType') = 0
            p_web.SetValue('tmp:CConfirmRepairType',0)
            tmp:CConfirmRepairType = 0
          End
        End
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
        If (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WConfirmRepairType') = 0
            p_web.SetValue('tmp:WConfirmRepairType',0)
            tmp:WConfirmRepairType = 0
          End
        End
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('JobCompleteProcess') = 1)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('tmp:CConfirmRepairType') = 0)
              loc:alert = 'You must confirm the chargeable repair type.'
              loc:invalid = 'tmp:CConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
          if (p_web.GSV('tmp:WConfirmRepairType') = 0)
              loc:alert = 'You must confirm the warranty repair type.'
              loc:invalid = 'tmp:WConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('JobCompleteProcess') = 1)
  
  IF (vod.RepairTypesNoParts(p_web.GSV('job:Chargeable_Job'), |
      p_web.GSV('job:Warranty_Job'), |
      p_web.GSV('job:Manufacturer'), |
      p_web.GSV('tmp:CRepairType'), |
      p_web.GSV('tmp:WRepairType')))
      ! #11762 Repair Type does not allow parts. 
      ! Check if there are any (Bryan: 24/11/2010)
      
      IF (vod.JobHasSparesAttached(p_web.GSV('job:Ref_Number',), |
          p_web.GSV('job:Estimate'), |
          p_web.GSV('job:Chargeable_Job'), |
          p_web.GSV('job:Warranty_Job')))
          loc:Invalid = 'tmp:ConfirmRepairType'
          loc:Alert = 'The selected Repair Type(s) require that there are no parts attached to the job.'
          EXIT
      END
  END
  
  
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:InvalidTab += 1
          tmp:CChargeType = Upper(tmp:CChargeType)
          p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
        If loc:Invalid <> '' then exit.
          tmp:CRepairType = Upper(tmp:CRepairType)
          p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:InvalidTab += 1
          tmp:WChargeType = Upper(tmp:WChargeType)
          p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
        If loc:Invalid <> '' then exit.
          tmp:WRepairType = Upper(tmp:WRepairType)
          p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
        If loc:Invalid <> '' then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
  if (p_web.GSV('job:Chargeable_Job') = 'YES') 
      p_web.SSV('job:Charge_Type',p_web.GSV('tmp:CChargeType'))
      p_web.SSV('job:Repair_Type',p_web.GSV('tmp:CRepairType'))
          
      if (p_web.GSV('job:Estimate') <> 'YES') ! #11659 No need to set to estimate. If it already is. (Bryan: 31/08/2010)
          CASE vod.EstimateRequired(p_web.GSV('job:Charge_Type'),p_web.GSV('job:Account_Number'))
          OF 1 ! Make Esimate
              p_web.SSV('job:Estimate','YES')
          OF 2 ! Don't make esimate
              p_web.SSV('job:Estimate','NO')
          ELSE
          END        
      END
          
  end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  if (p_web.GSV('job:Warranty_job') = 'YES')
      if (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('tmp:WChargeType') Or |
          p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('tmp:WRepairType'))
  
          p_web.SSV('job:Warranty_Charge_Type',p_web.GSV('tmp:WChargeType'))
          p_web.SSV('job:Repair_Type_Warranty',p_web.GSV('tmp:WRepairType'))
          p_web.SSV('JobPricingRoutine:ForceWarranty',1)
          JobPricingRoutine(p_web)
      end !
  end ! if (p_web.GSV('job:Warranty_job') = 'YES')
  
  p_web.SSV('jobe:COverwriteRepairType',p_web.GSV('tmp:CConfirmRepairType'))
  p_web.SSV('jobe:WOverwriteRepairType',p_web.GSV('tmp:WConfirmRepairType'))
  
  
  CalculateBilling(p_web)
  
  p_web.DeleteSessionValue('passedURL') 
  
  
      
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.StoreValue('tmp:CChargeType')
  p_web.StoreValue('tmp:CRepairType')
  p_web.StoreValue('tmp:CConfirmRepairType')
  p_web.StoreValue('tmp:WChargeType')
  p_web.StoreValue('tmp:WRepairType')
  p_web.StoreValue('tmp:WConfirmRepairType')
JobPricingRoutine    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_epr_id          USHORT,AUTO                           !
tmp:ARCCLabourCost   REAL                                  !ARC Labour Cost
tmp:RRCCLabourCost   REAL                                  !RRC Labour Cost
tmp:ARCCPartsCost    REAL                                  !ARCC Parts Cost
tmp:RRCCPartsCost    REAL                                  !RRCC Parts Cost
tmp:ARCWLabourCost   REAL                                  !ARCW Labour Cost
tmp:RRCWLabourCost   REAL                                  !RRCW Labour Cost
tmp:ARCWPartsCost    REAL                                  !ARCW Parts Cost
tmp:RRCWPartsCost    REAL                                  !RRCW Parts Cost
tmp:ARCELabourCost   REAL                                  !ARCE Labour Cost
tmp:RRCELabourCost   REAL                                  !ARCELabour Cost
tmp:ARCEPartsCost    REAL                                  !ARCE Parts Cost
tmp:RRCEPartsCost    REAL                                  !RRCE Parts Cost
tmp:ClaimPartsCost   REAL                                  !Claim PartsCost
tmp:UseStandardRate  BYTE(0)                               !Use Standard Rate
tmp:ClaimValue       REAL                                  !Claim Value
tmp:FixedCharge      BYTE(0)                               !Fixed Charge
tmp:ExcludeHandlingFee BYTE(0)                             !Exclude Handling Fee
tmp:ManufacturerExchangeFee REAL                           !Manufacturer Exchange Fee
tmp:FixedChargeARC   BYTE(0)                               !Fixed Charge ARC
tmp:FixedChargeRRC   BYTE(0)                               !Fixed Charge RRC
tmp:ARCLocation      STRING(30)                            !ARC Location
tmp:RRCLocation      STRING(30)                            !RRCLocation
tmp:SecondYearWarranty BYTE(0)                             !Second Year Warranty (True/False)
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    sentToHub(p_web)

    tmp:FixedCharge = 0
    tmp:ExcludeHandlingFee = 0
    tmp:ManufacturerExchangeFee = 0
    tmp:FixedChargeARC = 0
    tmp:FixedChargeRRC = 0
    tmp:ClaimPartsCost = 0
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        tmp:ARCLocation = tra:SiteLocation
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber  = p_web.GSV('job:Ref_Number')
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            tmp:RRCLocation = tra:SiteLocation
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign

    If p_web.GSV('job:Chargeable_Job') <> 'YES'
        !If not chargeable job, blank chargeable costs - L874 (DBH: 16-07-2003)
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        p_web.SSV('job:Ignore_Chargeable_Charges','NO')
        p_web.SSV('jobe:IgnoreRRCChaCosts',0)

    Else !If p_web.GSV('job:Chargeable_Job <> 'YES'
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        p_web.SSV('jobe:HandlingFee',0)
        p_web.SSV('jobe:ExchangeRate',0)

        If ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))
            tmp:ExcludeHandlingFee = 1
        End !If ExcludeHandingFee('C',p_web.GSV('job:Manfacturer,p_web.GSV('job:Repair_Type)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type = p_web.GSV('job:Charge_Type')
        If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Found
            If cha:Zero_Parts_ARC
                tmp:FixedChargeARC = 1
            End !If cha:Zero_Parts_ARC = 'YES'
            If cha:Zero_Parts = 'YES'
                tmp:FixedChargeRRC = 1
            End !If cha:Zero_Parts = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If


                If ~cha:Zero_Parts_ARC
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            If p_web.GSV('job:Estimate') = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:ARCCPartsCost += epr:Sale_Cost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If p_web.GSV('job:Estimate = 'YES'
                                tmp:ARCCPartsCost += par:Sale_Cost * par:Quantity
                            End !If p_web.GSV('job:Estimate = 'YES'
                        End !If sto:Location = tmp:ARCLocatin
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts_ARC <> 'YES'
                    If p_web.GSV('job:Invoice_Number') = 0
                        par:Sale_Cost = 0
                        Access:PARTS.Update()
                    End !If p_web.GSV('job:Invoice_Number = 0
                End !If cha:Zero_Parts_ARC <> 'YES'
                If cha:Zero_Parts <> 'YES'
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            If p_web.GSV('job:Estimate') = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:RRCCPartsCost += epr:RRCSaleCost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If p_web.GSV('job:Estimate = 'YES'
                                tmp:RRCCPartsCost += par:RRCSaleCost * par:Quantity
                            End !If p_web.GSV('job:Estimate = 'YES'

                        End !If sto:Location <> tmp:RRCLocation
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts <> 'YES'
                    If p_web.GSV('job:Invoice_Number') = 0
                        par:RRCSaleCost = 0
                        Access:PARTS.Update()
                    End !If p_web.GSV('job:Invoice_Number = 0
                End !If cha:Zero_Parts <> 'YES'
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = p_web.GSV('job:account_number')
                suc:model_number   = p_web.GSV('job:model_number')
                suc:charge_type    = p_web.GSV('job:charge_type')
                suc:unit_type      = p_web.GSV('job:unit_type')
                suc:repair_type    = p_web.GSV('job:repair_type')
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCCLabourCost      = trc:Cost
                        tmp:RRCCLabourCost      = trc:RRCRate
                        p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCCLabourCost      = suc:Cost
                    tmp:RRCCLabourCost      = suc:RRCRate
                    p_web.SSV('jobe:HandlingFee',suc:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',suc:Exchange)

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = p_web.GSV('job:model_number')
                trc:charge_type    = p_web.GSV('job:charge_type')
                trc:unit_type      = p_web.GSV('job:unit_type')
                trc:repair_type    = p_web.GSV('job:repair_type')
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCCLabourCost      = trc:Cost
                    tmp:RRCCLabourCost      = trc:RRCRate
                    p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = p_web.GSV('job:model_number')
                sta:charge_type  = p_web.GSV('job:charge_type')
                sta:unit_type    = p_web.GSV('job:unit_type')
                sta:repair_type  = p_web.GSV('job:repair_type')
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCCLabourCost      = sta:Cost
                    tmp:RRCCLabourCost      = sta:RRCRate
                    p_web.SSV('jobe:HandlingFee',sta:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',sta:Exchange)
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Error
        End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Dont blank the cost because of their cock up .
        !They used this field for other costs, and now want to see it
        !even if it's not a loan
!        If ~LoanAttachedToJob(p_web.GSV('job:Ref_Number)
!            p_web.GSV('job:Courier_Cost = 0
!            p_web.GSV('job:Courier_Cost_Estimate = 0
!        End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
    End !p_web.GSV('job:Chargeable_Job = 'YES'

    tmp:SecondYearWarranty = 0
    If p_web.GSV('job:Warranty_Job') <> 'YES'
        !Blank costs if not a Warranty Job - L874 (DBH: 16-07-2003)
        tmp:ARCWLabourCost  = 0
        tmp:ARCWPartsCost   = 0
        tmp:RRCWLabourCost  = 0
        tmp:RRCWPartsCost   = 0
        tmp:ClaimValue      = 0
        p_web.SSV('job:Ignore_Warranty_Charges','NO')
        p_web.SSV('jobe:IgnoreRRCWarCosts',0)
        p_web.SSV('jobe:IgnoreClaimCosts',0)
    Else !If p_web.GSV('job:Warranty_Job = 'YES'
        !Do not reprice Warranty Completed Jobs - L945  (DBH: 03-09-2003)
        !Allow to force the reprice if the job conditions change - L945 (DBH: 04-09-2003)
        If p_web.GSV('job:Date_Completed') = 0 Or p_web.GSV('JobPricingRoutine:ForceWarranty') = 1
            tmp:ARCWLabourCost  = 0
            tmp:ARCWPartsCost   = 0
            tmp:RRCWLabourCost  = 0
            tmp:RRCWPartsCost   = 0
            p_web.SSV('jobe:HandlingFee',0)
            p_web.SSV('jobe:ExchangeRate',0)

            If ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))
                tmp:ExcludeHandlingFee = 1
            End !If ExcludeHandingFee('C',p_web.GSV('job:Manfacturer,p_web.GSV('job:Repair_Type)

            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                tmp:SecondYearWarranty = cha:SecondYearWarranty


                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                        Then Break.  ! End If

                    tmp:ClaimPartsCost += wpr:Purchase_Cost * wpr:Quantity

                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            tmp:ARCWPartsCost += wpr:Purchase_Cost * wpr:Quantity
                        End !If sto:Location = tmp:ARCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            tmp:RRCWPartsCost += wpr:RRCPurchaseCost * wpr:Quantity
                        End !If sto:Location = tmp:RRCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)

                tmp:UseStandardRate = 1
                If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                    access:subchrge.clearkey(suc:model_repair_type_key)
                    suc:account_number = p_web.GSV('job:account_number')
                    suc:model_number   = p_web.GSV('job:model_number')
                    suc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                    suc:unit_type      = p_web.GSV('job:unit_type')
                    suc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                    if access:subchrge.fetch(suc:model_repair_type_key)
                        access:trachrge.clearkey(trc:account_charge_key)
                        trc:account_number = sub:main_account_number
                        trc:model_number   = p_web.GSV('job:model_number')
                        trc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                        trc:unit_type      = p_web.GSV('job:unit_type')
                        trc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                            tmp:ARCWLabourCost      = trc:Cost
                            tmp:RRCWLabourCost      = trc:RRCRate
                            tmp:ClaimValue          = trc:WarrantyClaimRate
                            p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                            p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                            tmp:useStandardRate = 0
                        End!if access:trachrge.fetch(trc:account_charge_key)
                    Else
                        tmp:ARCWLabourCost      = suc:Cost
                        tmp:RRCWLabourCost      = suc:RRCRate
                        tmp:ClaimValue          = suc:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',suc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',suc:Exchange)

                        tmp:useStandardRate = 0
                    End!if access:subchrge.fetch(suc:model_repair_type_key)
                Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCWLabourCost      = trc:Cost
                        tmp:RRCWLabourCost      = trc:RRCRate
                        tmp:ClaimValue          = trc:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                        tmp:useStandardRate = 0

                    End!if access:trachrge.fetch(trc:account_charge_key)

                End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

                If tmp:useStandardRate
                    access:stdchrge.clearkey(sta:model_number_charge_key)
                    sta:model_number = p_web.GSV('job:model_number')
                    sta:charge_type  = p_web.GSV('job:Warranty_charge_type')
                    sta:unit_type    = p_web.GSV('job:unit_type')
                    sta:repair_type  = p_web.GSV('job:repair_type_Warranty')
                    if access:stdchrge.fetch(sta:model_number_charge_key)
                    Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                        tmp:ARCWLabourCost      = sta:Cost
                        tmp:RRCWLabourCost      = sta:RRCRate
                        tmp:ClaimValue          = sta:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',sta:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',sta:Exchange)
                    end !if access:stdchrge.fetch(sta:model_number_charge_key)
                End !If tmp:useStandardRate
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Found
                If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:SecondYrExchangeFee
                Else !If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:ExchangeFee
                End !If tmp:SecondYearWarranty

            Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Error
            End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        End !If p_web.GSV('job:Date_Completed = 0
    End !p_web.GSV('job:Warranty_Job = 'YES'

    tmp:ARCELabourCost = 0
    tmp:ARCEPartsCost  = 0
    tmp:RRCELabourCost = 0
    tmp:RRCEPartsCost   = 0
    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type = p_web.GSV('job:Charge_Type')
    If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Found
        Save_epr_ID = Access:ESTPARTS.SaveFile()
        Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
        epr:Ref_Number  = p_web.GSV('job:Ref_Number')
        Set(epr:Part_Number_Key,epr:Part_Number_Key)
        Loop
            If Access:ESTPARTS.NEXT()
               Break
            End !If
            If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Then Break.  ! End If
            If ~cha:Zero_Parts_ARC
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:ARCLocation
                        tmp:ARCEPartsCost += epr:Sale_Cost * epr:Quantity
                    End !If sto:Location = tmp:ARCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts_ARC <> 'YES'
                If p_web.GSV('job:Invoice_Number') = 0
                    epr:Sale_Cost = 0
                    Access:ESTPARTS.Update()
                End !If p_web.GSV('job:Invoice_Number = 0
            End !If cha:Zero_Parts_ARC <> 'YES'

            If cha:Zero_Parts <> 'YES'
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:RRCLocation
                        tmp:RRCEPartsCost += epr:RRCSaleCost * epr:Quantity
                    End !If sto:Location = tmp:RRCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts <> 'YES'
                If p_web.GSV('job:Invoice_Number') = 0
                    epr:RRCSaleCost = 0
                    Access:ESTPARTS.Update()
                End !If p_web.GSV('job:Invoice_Number = 0
            End !If cha:Zero_Parts <> 'YES'
        End !Loop
        Access:ESTPARTS.RestoreFile(Save_epr_ID)
        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Estimate_Accepted') = 'YES'
                !If estimate, then use the estimate cost.
                !it doesn't matter what the current costs are
                tmp:ARCELabourCost  = p_web.GSV('job:Labour_Cost_Estimate')
                tmp:RRCELabourCost  = p_web.GSV('jobe:RRCELabourCost')
                tmp:ARCCLabourCost  = p_web.GSV('job:Labour_Cost_Estimate')
                tmp:RRCCLabourCost  = p_web.GSV('jobe:RRCELabourCost')

                ! Inserting (DBH 03/12/2007) # 8218 - For RRC-ARC estimate, the RRC costs must mirror the ARC costs
                
                if (p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob'))
                    tmp:RRCCLabourCost = tmp:ARCCLabourCost
                    tmp:RRCCPartsCost = tmp:ARCCPartsCost
                End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
                ! End (DBH 03/12/2007) #8218
        Else !If p_web.GSV('job:Estimate = 'YES' And p_web.GSV('job:Estimate_Accepted = 'YES'

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = p_web.GSV('job:account_number')
                suc:model_number   = p_web.GSV('job:model_number')
                suc:charge_type    = p_web.GSV('job:charge_type')
                suc:unit_type      = p_web.GSV('job:unit_type')
                suc:repair_type    = p_web.GSV('job:repair_type')
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCELabourCost = trc:Cost
                        tmp:RRCELabourCost  = trc:RRCRate
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCELabourCost = suc:Cost
                    tmp:RRCELabourCost = suc:RRCRate

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = p_web.GSV('job:model_number')
                trc:charge_type    = p_web.GSV('job:charge_type')
                trc:unit_type      = p_web.GSV('job:unit_type')
                trc:repair_type    = p_web.GSV('job:repair_type')
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCELabourCost     = trc:Cost
                    tmp:RRCELabourCost  = trc:RRCRate
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = p_web.GSV('job:model_number')
                sta:charge_type  = p_web.GSV('job:charge_type')
                sta:unit_type    = p_web.GSV('job:unit_type')
                sta:repair_type  = p_web.GSV('job:repair_type')
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCELabourCost = sta:Cost
                    tmp:RRCELabourCost = sta:RRCRate
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        End !If p_web.GSV('job:Estimate = 'YES' And p_web.GSV('job:Estimate_Accepted = 'YES'
    Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Error
    End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

    If p_web.GSV('job:Exchange_Unit_Number') <> 0
        If p_web.GSV('jobe:ExchangedAtRRC')
            p_web.SSV('jobe:HandlingFee',0)
        Else !If p_web.GSV('jobe:ExchangedAtRRC
            p_web.SSV('jobe:ExchangeRate',0)
        End !If p_web.GSV('jobe:ExchangedAtRRC
    Else !p_web.GSV('job:Exchange_Unit_Number <> 0
        If (p_web.GSV('SentToHub') = 0)
            p_web.SSV('jobe:HandlingFee',0)
        End !If ~SentToHub(p_web.GSV('job:Ref_Number)
        p_web.SSV('jobe:ExchangeRate',0)
    End !p_web.GSV('job:Exchange_Unit_Number <> 0

    If tmp:ExcludeHandlingFee
        p_web.SSV('jobe:HandlingFee',0)
    End !If tmp:ExcludeHandlingFee

    If p_web.GSV('job:Ignore_Chargeable_Charges') <> 'YES'
        p_web.SSV('job:Labour_Cost',tmp:ARCCLabourCost)
        p_web.SSV('job:Parts_Cost', tmp:ARCCPartsCost)
        If p_web.GSV('job:Third_Party_Site') <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = p_web.GSV('job:Third_Party_Site')
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If p_web.GSV('jobe:Ignore3rdPartyCosts') <> 1
                    If p_web.GSV('jobe:ARC3rdPartyMarkup') = 0
                        p_web.SSV('jobe:ARC3rdPartyMarkup',trd:Markup)
                    End !If p_web.GSV('jobe:ARC3rdPartyMarkup = 0
                End !If ~p_web.GSV('jobe:Ignore3rdPartyCosts
                p_web.SSV('jobe:ARC3rdPartyVAT',p_web.GSV('jobe:ARC3rdPartyCost') * (trd:VatRate/100))
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            If tmp:FixedChargeARC = 0
                p_web.SSV('job:Labour_Cost',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('jobe:ARC3rdPartyMarkup'))
            End !If tmp:FixedChargeARC = 0
        End !If p_web.GSV('job:Third_Party_Site <> ''
    End !p_web.GSV('job:Ignore_Chargeable_Charges <> 'YES'

    If p_web.GSV('jobe:IgnoreRRCChaCosts') <> 1
        p_web.SSV('jobe:RRCCLabourCost',tmp:RRCCLabourCost)

        If p_web.GSV('job:Third_Party_Site') <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = p_web.GSV('job:Third_Party_Site')
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If p_web.GSV('jobe:Ignore3rdPartyCosts') <> 1
                    If p_web.GSV('jobe:ARC3rdPartyMarkup') = 0
                        p_web.SSV('jobe:ARC3rdPartyMarkup',trd:Markup)
                    End !If p_web.GSV('jobe:ARC3rdPartyMarkup = 0
                End !If ~p_web.GSV('jobe:Ignore3rdPartyCosts
                p_web.SSV('jobe:ARC3rdPartyVAT',p_web.GSV('jobe:ARC3rdPartyCost') * (trd:VatRate/100))
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

            If tmp:FixedChargeRRC = 0
                p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('jobe:ARC3rdPartyMarkup'))
            End !If tmp:FixedCharge = 0
        End !If p_web.GSV('job:Third_Party_Site <> ''
    End !~p_web.GSV('jobe:IgnoreRRCChaCosts
    p_web.SSV('jobe:RRCCPartsCost',tmp:RRCCPartsCost)

    !Only update prices on incomplete jobs - L945 (DBH: 03-09-2003)
    !Allow to reprice if the job conditions change - L945 (DBH: 04-09-2003)
    If p_web.GSV('job:Date_Completed') = 0 Or p_web.GSV('JobPricingRoutine:ForceWarranty') = 1
        If p_web.GSV('job:Ignore_Warranty_Charges') <> 'YES'
            p_web.SSV('job:Labour_Cost_Warranty',tmp:ARCWLabourCost)
            p_web.SSV('job:Courier_Cost_Warranty',0)

            If p_web.GSV('jobe:ExchangedAtRRC') = True
                If p_web.GSV('jobe:SecondExchangeNumber') <> 0
                    !Exchange attached at RRC
                    !Don't claim, unless the ARC has added a second exchange - 3788 (DBH: 07-04-2004)
                    p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                Else !If p_web.GSV('jobe:SecondExchangeNumber <> 0
                    !Exchange attached at RRC.
                    !Do not claim, unless sent to ARC, and job is RTM - 3788 (DBH: 07-04-2004)
                    If p_web.GSV('jobe:Engineer48HourOption') <> 1
                        If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.'
                            p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                        End !If p_web.GSV('job:Repair_Type_Warranty = 'R.T.M.'
                    End !If p_web.GSV('jobe:Engineer48HourOption = 1
                End !If p_web.GSV('jobe:SecondExchangeNumber <> 0
            Else !If p_web.GSV('jobe:ExchangedAtRRC = True
                !Exchange attached at ARC. Claim for it - 3788 (DBH: 07-04-2004)
                If p_web.GSV('job:Exchange_Unit_Number') <> 0
                    p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                End !If p_web.GSV('job:Exchange_Unit_Number <> 0
            End !If p_web.GSV('jobe:ExchangedAtRRC = True
        End !p_web.GSV('job:Invoice_Warranty_Charges <> 'YES'

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If p_web.GSV('job:Exchange_Unit_Number') > 0
            If p_web.GSV('jobe:HandsetReplacmentValue') > 0
                tmp:ARCWPartsCost += p_web.GSV('jobe:HandsetReplacmentValue')
            End ! If p_web.GSV('jobe:HandsetReplacmentValue > 0
        End ! If p_web.GSV('job:Exchange_Unit_Number > 0
        If p_web.GSV('jobe:SecondExchangeNumber') > 0
            If p_web.GSV('jobe:SecondHandsetRepValue') > 0
                tmp:ARCWPartsCost += p_web.GSV('jobe:SecondHandsetRepValue')
            End ! If p_web.GSV('jobe:SecondHandsetReplacementValue > 0
        End ! If p_web.GSV('jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        p_web.SSV('job:Parts_Cost_Warranty',tmp:ARCWPartsCost)

        If p_web.GSV('jobe:IgnoreRRCWarCosts') <> 1
            p_web.SSV('jobe:RRCWLabourCost',tmp:RRCWLabourCost)
        End !~p_web.GSV('jobe:IgnoreRRCWarCosts

        p_web.SSV('jobe:RRCWPartsCost',tmp:RRCWPartsCost)

        If p_web.GSV('jobe:IgnoreClaimCosts') <> 1
            p_web.SSV('jobe:ClaimValue',tmp:ClaimValue)
        End !If ~p_web.GSV('jobe:IgnoreClaimCosts

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If p_web.GSV('job:Exchange_Unit_Number') > 0
            If p_web.GSV('jobe:HandsetReplacmentValue') > 0
                tmp:ClaimPartsCost += p_web.GSV('jobe:HandsetReplacmentValue')
            End ! If p_web.GSV('jobe:HandsetReplacmentValue > 0
        End ! If p_web.GSV('job:Exchange_Unit_Number > 0
        If p_web.GSV('jobe:SecondExchangeNumber') > 0
            If p_web.GSV('jobe:SecondHandsetRepValue') > 0
                tmp:ClaimPartsCost += p_web.GSV('jobe:SecondHandsetRepValue')
            End ! If p_web.GSV('jobe:SecondHandsetReplacementValue > 0
        End ! If p_web.GSV('jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        p_web.SSV('jobe:ClaimPartsCost',tmp:ClaimPartsCost)
    End !If p_web.GSV('job:Date_Completed = 0

    If p_web.GSV('job:Ignore_Estimate_Charges') <> 'YES'
        p_web.SSV('job:Labour_Cost_Estimate',tmp:ARCELabourCost)
    End !p_web.GSV('job:Ignore_Estimate_Charges <> 'YES'
    p_web.SSV('job:Parts_Cost_Estimate',tmp:ARCEPartsCost)

    If p_web.GSV('jobe:IgnoreRRCEstCosts') <> 1
        p_web.SSV('jobe:RRCELabourCost',tmp:RRCELabourCost)
    End !~p_web.GSV('jobe:IgnoreRRCEstCosts

    p_web.SSV('jobe:RRCEPartsCost',tmp:RRCEPartsCost)

    !Fixed Pricing

    If (p_web.GSV('SentToHub') = 0)
        !Not a hub job
        If tmp:FixedChargeRRC
            p_web.SSV('jobe:RRCCPartsCost',0)
            p_web.SSV('jobe:RRCEPartsCost',0)
        End !If tmp:FixedChargeRRC
    Else !If ~SentToHub(p_web.GSV('job:Ref_Number)
        If p_web.GSV('jobe:WebJob') = 1
            If tmp:FixedChargeARC
                p_web.SSV('job:Parts_Cost',0)
                p_web.SSV('job:Parts_Cost_Estimate',0)
            End !If tmp:FixedChargeRRC

            If tmp:FixedChargeRRC
                p_web.SSV('jobe:RRCCPartsCost',0)
                p_web.SSV('jobe:RRCEPartsCost',0)
            Else !If tmp:FixedChargeRRC
                If p_web.GSV('jobe:IgnoreRRCChaCosts') <> 1
                    p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('job:Labour_Cost'))
                End !If ~p_web.GSV('jobe:IgnoreRRCChaCosts
            End !If tmp:FixedChargeRRC
        Else !If p_web.GSV('jobe:WebJob = 1
            If tmp:FixedChargeARC
                p_web.SSV('job:Parts_Cost',0)
                p_web.SSV('job:Parts_Cost_Estimate',0)
            End !If tmp:FixedChargeARC
        End !If p_web.GSV('jobe:WebJob = 1
    End !If ~SentToHub(p_web.GSV('job:Ref_Number)

    !Totals
! Inserting (DBH 03/12/2007) # 8218 - RRC has the same costs as the ARC for an estimate
    If (p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob'))
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('job:Parts_Cost_Estimate'))
    End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
! End (DBH 03/12/2007) #8218

    p_web.SSV('job:Sub_Total',p_web.GSV('job:Courier_Cost') + p_web.GSV('job:Labour_Cost') + p_web.GSV('job:Parts_Cost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('jobe:RRCCLabourCost') + p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:Courier_Cost'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('job:Labour_Cost_Warranty') + p_web.GSV('job:Parts_Cost_Warranty') + p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('jobe:RRCWLabourCost') + p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('job:Labour_Cost_Estimate') + p_web.GSV('job:Parts_Cost_Estimate') + p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate'))


    do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:WARPARTS.Close
     Access:CHARTYPE.Close
     Access:SUBCHRGE.Close
     Access:TRACHRGE.Close
     Access:STDCHRGE.Close
     Access:MANUFACT.Close
     Access:TRDPARTY.Close
     Access:WEBJOB.Close
     Access:JOBS.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
ExcludeHandlingFee   PROCEDURE  (func:Type,func:Manufacturer,func:RepairType) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    rtnValue# = 0
    !Return Fatal if the Handling Fee should be excluded
    Case func:Type
        Of 'C'
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Chargeable   = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Found
                IF rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !IF rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
        Of 'W'
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Warranty     = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Found
                If rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !If rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
    End !Case func:Type

    do closeFiles
    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:REPTYDEF.Close
     FilesOpened = False
  END
ViewCosts            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ARCCost1         REAL                                  !
tmp:ARCCost2         REAL                                  !
tmp:ARCCost3         REAL                                  !
tmp:ARCCost4         REAL                                  !
tmp:ARCCost5         REAL                                  !
tmp:ARCCost6         REAL                                  !
tmp:ARCCost7         REAL                                  !
tmp:ARCCost8         REAL                                  !
tmp:RRCCost0         REAL                                  !
tmp:RRCCost1         REAL                                  !
tmp:RRCCost2         REAL                                  !
tmp:RRCCost3         REAL                                  !
tmp:RRCCost4         REAL                                  !
tmp:RRCCost5         REAL                                  !
tmp:RRCCost6         REAL                                  !
tmp:RRCCost7         REAL                                  !
tmp:RRCCost8         REAL                                  !
tmp:ARCIgnoreDefaultCharges BYTE                           !
tmp:RRCIgnoreDefaultCharges BYTE                           !
tmp:ARCViewCostType  STRING(30)                            !
tmp:AdjustmentCost1  REAL                                  !
tmp:AdjustmentCost2  REAL                                  !
tmp:AdjustmentCost3  REAL                                  !
tmp:AdjustmentCost4  REAL                                  !
tmp:AdjustmentCost5  REAL                                  !
tmp:AdjustmentCost6  REAL                                  !
tmp:RRCViewCostType  STRING(30)                            !
tmp:ARCIgnoreReason  STRING(255)                           !
tmp:RRCIgnoreReason  STRING(255)                           !
tmp:originalInvoice  STRING(30)                            !
tmp:ARCInvoiceNumber STRING(60)                            !
FilesOpened     Long
JOBSE2::State  USHORT
SUBCHRGE::State  USHORT
TRACHRGE::State  USHORT
STDCHRGE::State  USHORT
USERS::State  USHORT
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
INVOICE::State  USHORT
JOBSINV::State  USHORT
TRADEACC::State  USHORT
TRDPARTY::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       class
AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ViewCosts')
  loc:formname = 'ViewCosts_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ViewCosts','')
    p_web._DivHeader('ViewCosts',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewCosts',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ViewCostsReturnURL')
addToAudit    routine
    data
locNotes        String(255)
    code
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        if (error())
            break
        end ! if (error())
        if (tmpaud:sessionID <> p_web.sessionID)
            break
        end ! if (tmpaud:sessionID <> p_web.sessionID)
        locNotes = 'REASON: ' & clip(tmpaud:Reason)
        if (tmpaud:SaveCost)
            locNotes = clip(locNotes) & '<13,10>PREVIOUS CHARGE: ' & format(tmpaud:Cost,@n14.2)
        end !if (tmpaud:SaveCost)
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action',tmpaud:Field)
        p_web.SSV('AddToAudit:Notes',locNotes)
        addToAudit(p_web)
    end

    !Empty Audit Queue
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        if (error())
            break
        end ! if (error())
        if (tmpaud:sessionID <> p_web.sessionID)
            break
        end ! if (tmpaud:sessionID <> p_web.sessionID)
        delete(tempAuditQueue)
    end



DefaultLabourCost   ROUTINE
    DefaultLabourCost(p_web)
    
    
displayARCCostFields      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCTotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))


    of 'Warranty'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Warranty'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:ClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:ClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of '3rd Party'
        p_web.SSV('Prompt:Cost1','Cost')
        p_web.SSV('Prompt:Cost2','')
        p_web.SSV('Prompt:Cost3','')
        p_web.SSV('Prompt:Cost4','')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','Markup')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe:ARC3rdPartyCost'))
        p_web.SSV('tmp:ARCCost2',0)
        p_web.SSV('tmp:ARCCost3',0)
        p_web.SSV('tmp:ARCCost4',0)
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARC3rdPartyVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARC3rdPartyTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARC3rdPartyMarkup'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:Ignore3rdPartyCosts'))

    of 'Estimate'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Estimate'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCEVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCETotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Estimate_Charges'))

    of 'Chargeable - Invoiced'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Invoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Invoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Invoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCITotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCIPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCIOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:WInvoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:WInvoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:WInvoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:InvoiceClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:InvClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimISubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:AdjustmentCost1',p_web.GSV('jobe:ExchangeAdjustment'))
        p_web.SSV('tmp:AdjustmentCost2',p_web.GSV('jobe:LabourAdjustment'))
        p_web.SSV('tmp:AdjustmentCost3',p_web.GSV('jobe:PartsAdjustment'))
        p_web.SSV('tmp:AdjustmentCost4',p_web.GSV('tmp:AdjustmentSubTotal'))
        p_web.SSV('tmp:AdjustmentCost5',p_web.GSV('tmp:AdjustmentVAT'))
        p_web.SSV('tmp:AdjustmentCost6',p_web.GSV('tmp:AdjustmentTotal'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of 'Manufacturer Payment'
        p_web.SSV('Prompt:Cost1','Labour Paid')
        p_web.SSV('Prompt:Cost2','Parts Paid')
        p_web.SSV('Prompt:Cost3','Other Costs')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Paid')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe2:WLabourPaid'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe2:WPartsPaid'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe2:WOtherCosts'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('jobe2:WSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('jobe2:WVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('jobe2:WTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)
    end ! case (p_web.GSV('tmp:ARCViewCostType'))


displayRRCCostFields      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'
        
        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END
        
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCost0',p_web.GSV('jobe2:JobDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCTotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCOutstanding'))


        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))
        
        if (p_web.GSV('jobe2:JobDiscountAmnt') > 0 or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        else
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        end
        
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end

    of 'Warranty'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWTotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end
        

    of 'Handling'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Handling Fee')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:HandlingFee'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        

    of 'Exchange'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Exchange Fee')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:ExchangeFee'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

    of 'Estimate'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCELabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCEPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCESubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCEVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCETotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCEstCosts'))
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end        

    of 'Chargeable - Invoiced'
        
        if (p_web.GSV('jobe2:InvDiscountAmnt') > 0  or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        else
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        end

        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCOst0',p_web.GSV('jobe2:InvDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCITotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCIPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCIOutstanding'))

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWITotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        
        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))

    of 'Credits'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('tmp:CreditTotal',p_web.GSV('tmp:RRCCITotal'))
        Access:JOBSINV.Clearkey(jov:typeRecordKey)
        jov:refNumber    = p_web.GSV('job:Ref_Number')
        jov:type    = 'C'
        set(jov:typeRecordKey,jov:typeRecordKey)
        loop
            if (Access:JOBSINV.Next())
                Break
            end ! if (Access:JOBSINV.Next())
            if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
            if (jov:type    <> 'C')
                Break
            end ! if (jov:type    <> 'C')
            p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCITotal') - jov:creditAmount)
            p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCIOutstanding') - jov:creditAmount)
        end ! loop

        access:INVOICE.clearKey(inv:invoice_Number_Key)
        inv:invoice_Number = p_web.GSV('job:invoice_number')
        if (access:INVOICE.tryfetch(inv:invoice_Number_Key) = level:Benign)
        end !

        access:TRADEACC.clearKey(tra:account_Number_Key)
        tra:account_Number = p_web.GSV('wob:HeadAccountNumber')
        if (access:TRADEACC.tryfetch(tra:account_Number_Key) = level:Benign)
        end

        p_web.SSV('tmp:OriginalInvoice',clip(inv:invoice_number) & '-' & clip(tra:BranchIdentification))


        p_web.SSV('Prompt:RCost1','Original Total')
        p_web.SSV('Prompt:RCost2','Original Invoice')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','Current Total')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:CreditTotal'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCCITotal'))

    end !case (p_web.GSV('tmp:RRCViewCostType'))
displayOtherFields      routine
    p_web.SSV('Hide:ExchangeReplacement',1)

    p_web.SSV('Prompt:RCost1','')
    p_web.SSV('Prompt:ACost1','')


    if (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')
        if (p_web.GSV('job:exchange_unit_number') > 0 and |
            (p_web.GSV('jobe:exchangedAtRRC') = 0 or |
                p_web.GSV('jobe:exchangedAtRRC') = 1 and p_web.GSV('job:repair_Type_Warranty') = 'R.T.M.'))
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        else !job:Courier_Cost
            p_web.SSV('Prompt:RCost1','')
        end ! job:Courier_Cost
    end ! if (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')

    LoanAttachedToJob(p_web)
    if (p_web.GSV('LoanAttachedToJob') = 1)
        if (p_web.GSV('jobe:HubRepair') = 0)
            if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:RCost1','Lost Loan Charge')
            end
        else ! if (p_web.GSV('jobe:HubRepair') = 0)
            if (p_web.GSV('tmp:ARCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:ACost1','Lost Loan Charge')
            end

        end ! if (p_web.GSV('jobe:HubRepair') = 0)
    else ! if (p_web.GSV('LoanAttachedToJob') = 1)
        p_web.SSV('Prompt:RCost1','')
        if (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
            if (p_web.GSV('jobe:WebJob') = 1)
                p_web.SSV('Hide:ExchangeReplacement',0)

                if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'EXCHANGE REPLACEMENT CHARGE'))
                else ! if (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE')
                end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE'))

                if (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    p_web.SSV('Prompt:RCost1','Exchange Replacement')

                    p_web.SSV('Prompt:ACost1','Exchange Replacement')

                end ! if (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
            end ! if (p_web.GSV('jobe:WebJob') = 1)
        end !if (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
    end ! if (p_web.GSV('LoanAttachedToJob') = 1)
pricingRoutine      Routine
data
locARCPaid    Real()
locRRCPaid    Real()
locTotalPaid    Real()
code
    jobPricingRoutine(p_web)
    if (p_web.GSV('job:warranty_job') = 'YES')
        if (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:invoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:invoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_Number_warranty')
            if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found
                p_web.SSV('tmp:ARCInvoiceNumber',inv:invoice_number)

                p_web.SSV('tmp:ARCWIVat',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:WInvoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:WInvoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCWITotal',p_web.GSV('tmp:ARCWIVat') + p_web.GSV('job:WInvoice_Sub_Total'))


                p_web.SSV('tmp:ARCClaimIVAT',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('jobe:InvClaimPartsCost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('jobe:InvoiceClaimValue') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCClaimISubTotal',p_web.GSV('job:WInvoice_Courier_Cost') + p_web.GSV('jobe:InvClaimPartsCost') + |
                                    p_web.GSV('jobe:InvoiceClaimValue'))
                p_web.SSV('tmp:ARCClaimITotal',p_web.GSV('tmp:ARCClaimIVat') + p_web.GSV('tmp:ARCClaimISubTotal'))

                p_web.SSV('tmp:AdjustmentSubTotal',p_web.GSV('jobe:LabourAdjustment') + p_web.GSV('jobe:ExchangeAdjustment') + |
                                        p_web.GSV('jobe:PartsAdjustment'))
                p_web.SSV('tmp:AdjustmentVAT',p_web.GSV('jobe:PartsAdjustment') * (inv:Vat_Rate_Parts/100) + |
                                    p_web.GSV('jobe:LabourAdjustment') * (inv:Vat_Rate_Labour/100) +|
                                    p_web.GSV('jobe:ExchangeAdjustment') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:AdjustmentTotal',p_web.GSV('tmp:AdjustmentVAT') + p_web.GSV('tmp:AdjustmentSubTotal'))

            else ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            end ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        else ! if (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        end ! if (p_web.GSV('job:invoice_number_warranty') > 0)
    end ! if (p_web.GSV('job:warranty_job') = 'YES')

    if (p_web.GSV('job:Chargeable_Job') = 'YES')
        if (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:InvoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_number')
            if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found

                Access:TRADEACC.Clearkey(tra:account_number_key)
                tra:account_number    = p_web.GSV('Default:ARCLocation')
                if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Found
                    p_web.SSV('tmp:ARCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:Date_Created,@d6))& ')')
                else ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Error
                end ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)

                if (inv:exportedRRCOracle)
                    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('job:Invoice_Courier_Cost') + |
                                            p_web.GSV('jobe:InvRRCCLabourCost') + |
                                            p_web.GSV('jobe:InvRRCCPartsCost'))

                    p_web.SSV('tmp:RRCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100))
                    p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCIVat') + p_web.GSV('jobe:InvRRCCSubTotal'))

                    Access:TRADEACC.Clearkey(tra:account_number_key)
                    tra:account_number    = p_web.GSV('wob:HeadAccountNumber')
                    if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Found
                        p_web.SSV('tmp:RRCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:RRCInvoiceDate,@d6))& ')')
                        if (p_web.GSV('job:exchange_unit_Number') > 0)
                            p_web.SSV('tmp:RRCInvoiceNumber',p_web.GSV('tmp:RRCInvoiceNumber') & ' Exchange')
                        end ! if (p_web.GSV('job:exchange_unit_Number') > 0)
                    else ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Error
                    end ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                end ! if (inv:exportedRRCOracle)

                p_web.SSV('tmp:ARCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:Invoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:Invoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Courier_Cost') + |
                                        p_web.GSV('job:Invoice_Labour_Cost') + |
                                        p_web.GSV('job:Invoice_Parts_Cost'))
                p_web.SSV('tmp:ARCCITotal',p_web.GSV('tmp:ARCCIVat') + p_web.GSV('job:invoice_Sub_Total'))

            else ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            end ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        else ! if (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        end ! if (p_web.GSV('job:Invoice_Number') > 0)
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')


    ! Paid ?

    Access:JOBPAYMT.Clearkey(jpt:all_Date_Key)
    jpt:ref_Number    = p_web.GSV('job:Ref_Number')
    set(jpt:all_Date_Key,jpt:all_Date_Key)
    loop
        if (Access:JOBPAYMT.Next())
            Break
        end ! if (Access:JOBPAYMT.Next())
        if (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))

        Access:USERS.Clearkey(use:user_code_Key)
        use:user_code    = jpt:user_code
        if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Found
            if (use:location = p_web.GSV('ARC:SiteLocation'))
                locARCPaid += jpt:Amount
            else
                locRRCPaid += jpt:Amount
            end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
        else ! if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
        locTotalPaid += jpt:amount
    end ! loop

    p_web.SSV('tmp:RRCCPaid',locRRCPaid)
    p_web.SSV('tmp:RRCCIPaid',locRRCPaid)
    p_web.SSV('tmp:ARCCPaid',locARCPaid)
    p_web.SSV('tmp:ARCCIPaid',locARCPaid)


    Access:TRDPARTY.Clearkey(trd:company_Name_Key)
    trd:company_Name    = p_web.GSV('job:Third_Party_Site')
    if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
        ! Found
        p_web.SSV('tmp:ARC3rdPartyVAT',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyTotal',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyMarkup',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('tmp:ARC3rdPartyTotal'))

    else ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)
        ! Error
    end ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)


    p_web.SSV('tmp:ARCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                               (p_web.GSV('job:Parts_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                                (p_web.GSV('job:Labour_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCCTotal',p_web.GSV('tmp:ARCCVat') + p_web.GSV('job:sub_total'))

    p_web.SSV('tmp:RRCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCCPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCCLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:RRCCTotal',p_web.GSV('tmp:RRCCVat') + p_web.GSV('jobe:RRCCSubTotal'))

    p_web.SSV('tmp:ARCWVAT',(p_web.GSV('job:Parts_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('job:Labour_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)) + |
                    (p_web.GSV('job:Courier_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)))
    p_web.SSV('tmp:ARCWTotal',p_web.GSV('tmp:ARCWVat') + p_web.GSV('job:sub_total_warranty'))

    p_web.SSV('tmp:RRCWVAT',(p_web.GSV('jobe:RRCWPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('jobe:RRCWLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCWTotal',p_web.GSV('tmp:RRCWVAT') + p_web.GSV('jobe:RRCWSubTotal'))

    p_web.SSV('tmp:ARCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('job:Parts_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('job:Labour_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCETotal',p_web.GSV('tmp:ARCEVat') + p_web.GSV('job:Sub_Total_Estimate'))

    p_web.SSV('tmp:RRCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCEPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCELabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCETotal',p_web.GSV('tmp:RRCEVAT') + p_web.GSV('jobe:RRCESubTotal'))

    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWPartsCost') + p_web.GSV('jobe:InvRRCWLabourCost'))

    p_web.SSV('tmp:RRCWIVAT',p_web.GSV('jobe:InvRRCWPartsCost') * (inv:Vat_Rate_Parts/100) + |
                    p_web.GSV('jobe:InvRRCWLabourCost') * (inv:Vat_Rate_Labour/100))
    p_web.SSV('tmp:RRCWITotal',p_web.GSV('tmp:RRCWIVAT') + p_web.GSV('jobe:InvRRCWSubTotal'))


    p_web.SSV('tmp:RRCOutstanding',p_web.GSV('tmp:RRCCTotal') - p_web.GSV('tmp:RRCCPaid'))
    p_web.SSV('tmp:ARCOutstanding',p_web.GSV('tmp:ARCCTotal') - p_web.GSV('tmp:ARCCPaid'))
    p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCCITotal') - p_web.GSV('tmp:RRCCIPaid'))
    p_web.SSV('tmp:ARCIOutstanding',p_web.GSV('tmp:ARCCITotal') - p_web.GSV('tmp:ARCCIPaid'))
restoreFields    routine
    p_web.SSV('job:Courier_Cost',p_web.GSV('save:Courier_Cost'))
    p_web.SSV('job:Labour_Cost',p_web.GSV('save:Labour_Cost'))
    p_web.SSV('job:Parts_Cot',p_web.GSV('save:Parts_Cost'))
    p_web.SSV('job:Sub_Total',p_web.GSV('save:Sub_Total'))
    p_web.SSV('job:Courier_Cost_Warranty',p_web.GSV('save:Courier_Cost_Warranty'))
    p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('save:Labour_Cost_Warranty'))
    p_web.SSV('job:Parts_Cost_Warranty',p_web.GSV('save:Parts_Cost_Warranty'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('save:Sub_Total_Warranty'))
    p_web.SSV('jobe:ClaimValue',p_web.GSV('save:ClaimValue'))
    p_web.SSV('jobe:ClaimPartsCost',p_web.GSV('save:ClaimPartsCost'))
    p_web.SSV('jobe:ARC3rdPartyCost',p_web.GSV('save:ARC3rdPartyCost'))
    p_web.SSV('job:Courier_Cost_Estimate',p_web.GSV('save:Courier_Cost_Estimate'))
    p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('save:Labour_Cost_Estimate'))
    p_web.SSV('job:Parts_Cost_Estimate',p_web.GSV('save:Parts_Cost_Estimate'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('save:Sub_Total_Estimate'))
    p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('save:Invoice_Courier_Cost'))
    p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('save:Invoice_Labour_Cost'))
    p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('save:Invoice_Parts_Cost'))
    p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('save:Invoice_Sub_Total'))
    p_web.SSV('job:WInvoice_Courier_Cost',p_web.GSV('save:WInvoice_Courier_Cost'))
    p_web.SSV('job:WInvoice_Labour_Cost',p_web.GSV('save:WInvoice_Labour_Cost'))
    p_web.SSV('job:WInvoice_Parts_Cost',p_web.GSV('save:WInvoice_Parts_Cost'))
    p_web.SSV('job:WInvoice_Sub_Total',p_web.GSV('save:WInvoice_Sub_Total'))
    p_web.SSV('jobe:ExchangAdjustment',p_web.GSV('save:ExchangeAdjustment'))
    p_web.SSV('jobe:LabourAdjustment',p_web.GSV('save:LabourAdjustment'))
    p_web.SSV('jobe:PartsAdjustment',p_web.GSV('save:PartsAdjustment'))
    p_web.SSV('jobe2:WLabourPaid',p_web.GSV('save:WLabourPaid'))
    p_web.SSV('jobe2:WPartsPaid',p_web.GSV('save:WPartsPaid'))
    p_web.SSV('jobe2:WOtherCosts',p_web.GSV('save:WOtherCosts'))
    p_web.SSV('jobe2:WVat',p_web.GSV('save:WVat'))
    p_web.SSV('jobe2:WTotal',p_web.GSV('save:WTotal'))
    p_web.SSV('jobe:IgnoreClaimCosts',p_web.GSV('save:IgnoreClaimCosts'))
    p_web.SSV('job:Ignore_Warranty_Charges',p_web.GSV('save:Ignore_Warranty_Charges'))
    p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('save:Ignore_Chargeable_Charges'))
    p_web.SSV('job:Ignore_Estimate_Charges',p_web.GSV('save:Ignore_Estimate_Charges'))
    p_web.SSV('jobe:RRCClabourCost',p_web.GSV('save:RRCCLabourCost'))
    p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('save:RRCCPartsCost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('save:RRCCSubTotal'))
    p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('save:RRCWLabourCost'))
    p_web.SSV('jobe:RRCWPartsCost',p_web.GSV('save:RRCWPartsCost'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('save:RRCWSubTotal'))
    p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('save:IgnoreRRCChaCosts'))
    p_web.SSV('jobe:IgnoreRRCWarCosts',p_web.GSV('save:IgnoreRRCWarCosts'))
    p_web.SSV('jobe:RRCELabourCost',p_web.GSV('save:RRCELabourCost'))
    p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('save:RRCEPartsCost'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('save:RRCESubTotal'))
    p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('save:InvRRCCLabourCost'))
    p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('save:InvRRCCPartsCost'))
    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('save:InvRRCCSubTotal'))
    p_web.SSV('jobe:IgnoreRRCEstCosts',p_web.GSV('save:IgnoreRRCEstCosts'))
    p_web.SSV('jobe:InvRRCWLabourCost',p_web.GSV('save:InvRRCWLabourCost'))
    p_web.SSV('jobe:InvRRCWPartsCost',p_web.GSV('save:InvRRCWPartsCost'))
    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('save:InvRRCWSubTotal'))
    p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('save:JobDiscountAmnt'))
    p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('save:InvDiscountAmnt'))
saveFields    routine
    p_web.SSV('save:Courier_Cost',p_web.GSV('job:Courier_Cost'))
    p_web.SSV('save:Labour_Cost',p_web.GSV('job:Labour_Cost'))
    p_web.SSV('save:Parts_Cot',p_web.GSV('job:Parts_Cost'))
    p_web.SSV('save:Sub_Total',p_web.GSV('job:Sub_Total'))
    p_web.SSV('save:Courier_Cost_Warranty',p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('save:Labour_Cost_Warranty',p_web.GSV('job:Labour_Cost_Warranty'))
    p_web.SSV('save:Parts_Cost_Warranty',p_web.GSV('job:Parts_Cost_Warranty'))
    p_web.SSV('save:Sub_Total_Warranty',p_web.GSV('job:Sub_Total_Warranty'))
    p_web.SSV('save:ClaimValue',p_web.GSV('jobe:ClaimValue'))
    p_web.SSV('save:ClaimPartsCost',p_web.GSV('jobe:ClaimPartsCost'))
    p_web.SSV('save:ARC3rdPartyCost',p_web.GSV('jobe:ARC3rdPartyCost'))
    p_web.SSV('save:Courier_Cost_Estimate',p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('save:Labour_Cost_Estimate',p_web.GSV('job:Labour_Cost_Estimate'))
    p_web.SSV('save:Parts_Cost_Estimate',p_web.GSV('job:Parts_Cost_Estimate'))
    p_web.SSV('save:Sub_Total_Estimate',p_web.GSV('job:Sub_Total_Estimate'))
    p_web.SSV('save:Invoice_Courier_Cost',p_web.GSV('job:Invoice_Courier_Cost'))
    p_web.SSV('save:Invoice_Labour_Cost',p_web.GSV('job:Invoice_Labour_Cost'))
    p_web.SSV('save:Invoice_Parts_Cost',p_web.GSV('job:Invoice_Parts_Cost'))
    p_web.SSV('save:Invoice_Sub_Total',p_web.GSV('job:Invoice_Sub_Total'))
    p_web.SSV('save:WInvoice_Courier_Cost',p_web.GSV('job:WInvoice_Courier_Cost'))
    p_web.SSV('save:WInvoice_Labour_Cost',p_web.GSV('job:WInvoice_Labour_Cost'))
    p_web.SSV('save:WInvoice_Parts_Cost',p_web.GSV('job:WInvoice_Parts_Cost'))
    p_web.SSV('save:WInvoice_Sub_Total',p_web.GSV('job:WInvoice_Sub_Total'))
    p_web.SSV('save:ExchangAdjustment',p_web.GSV('jobe:ExchangeAdjustment'))
    p_web.SSV('save:LabourAdjustment',p_web.GSV('jobe:LabourAdjustment'))
    p_web.SSV('save:PartsAdjustment',p_web.GSV('jobe:PartsAdjustment'))
    p_web.SSV('save:WLabourPaid',p_web.GSV('jobe2:WLabourPaid'))
    p_web.SSV('save:WPartsPaid',p_web.GSV('jobe2:WPartsPaid'))
    p_web.SSV('save:WOtherCosts',p_web.GSV('jobe2:WOtherCosts'))
    p_web.SSV('save:WVat',p_web.GSV('jobe2:WVat'))
    p_web.SSV('save:WTotal',p_web.GSV('jobe2:WTotal'))
    p_web.SSV('save:IgnoreClaimCosts',p_web.GSV('jobe:IgnoreClaimCosts'))
    p_web.SSV('save:Ignore_Warranty_Charges',p_web.GSV('job:Ignore_Warranty_Charges'))
    p_web.SSV('save:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Chargeable_Charges'))
    p_web.SSV('save:Ignore_Estimate_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
    p_web.SSV('save:RRCClabourCost',p_web.GSV('jobe:RRCCLabourCost'))
    p_web.SSV('save:RRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
    p_web.SSV('save:RRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
    p_web.SSV('save:RRCWLabourCost',p_web.GSV('jobe:RRCWLabourCost'))
    p_web.SSV('save:RRCWPartsCost',p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('save:RRCWSubTotal',p_web.GSV('jobe:RRCWSubTotal'))
    p_web.SSV('save:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCChaCosts'))
    p_web.SSV('save:IgnoreRRCWarCosts',p_web.GSV('jobe:IgnoreRRCWarCosts'))
    p_web.SSV('save:RRCELabourCost',p_web.GSV('jobe:RRCELabourCost'))
    p_web.SSV('save:RRCEPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
    p_web.SSV('save:RRCESubTotal',p_web.GSV('jobe:RRCESubTotal'))
    p_web.SSV('save:InvRRCCLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
    p_web.SSV('save:InvRRCCPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
    p_web.SSV('save:InvRRCCSubTotal',p_web.GSV('jobe:InvRRCCSubTotal'))
    p_web.SSV('save:IgnoreRRCEstCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))
    p_web.SSV('save:InvRRCWLabourCost',p_web.GSV('jobe:InvRRCWLabourCost'))
    p_web.SSV('save:InvRRCWPartsCost',p_web.GSV('jobe:InvRRCWPartsCost'))
    p_web.SSV('save:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWSubTotal'))
    p_web.SSV('save:JobDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
    p_web.SSV('save:InvDiscountAmnt',p_web.GSV('jobe2:InvDiscountAmnt'))
    
updateARCCost      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('job:Labour_Cost',p_web.GSV('tmp:ARCCost2'))
    of 'Warranty'
        p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('tmp:ARCCost2'))
    of 'Estimate'
        p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('tmp:ARCCost2'))
    end ! case (p_web.GSV('tmp:RRCViewCostType'))
updateRRCCost      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'
        p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('tmp:RRCCost2'))
        p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('DefaultLabourCost') - p_web.GSV('jobe:RRCCLabourCost'))
        if (p_web.GSV('jobe2:JobDiscountAmnt') < 0)
            p_web.SSV('jobe2:JobDiscountAmnt',0)
        end
            
    of 'Warranty'
        p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('tmp:RRCCost2'))
    of 'Estimate'
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('tmp:RRCCost2'))
    end ! case (p_web.GSV('tmp:RRCViewCostType'))

OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(SUBCHRGE)
  p_web._OpenFile(TRACHRGE)
  p_web._OpenFile(STDCHRGE)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBPAYMT)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TRDPARTY)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBPAYMT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TRDPARTY)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Return URL
  IF (p_web.IfExistsValue('ViewCostsReturnURL'))
      p_web.StoreValue('ViewCostsReturnURL')
  ELSE
      p_web.SSV('ViewCostsReturnURL','ViewJob')
  END
  p_web.SSV('Prompt:RCost0','')
  p_web.SSV('Prompt:RCost1','')
  p_web.SSV('Prompt:RCost2','')
  p_web.SSV('Prompt:RCost3','')
  p_web.SSV('Prompt:RCost4','')
  p_web.SSV('Prompt:RCost5','')
  p_web.SSV('Prompt:RCost6','')
  p_web.SSV('Prompt:RCost7','')
  p_web.SSV('Prompt:RCost8','')
  p_web.SSV('Prompt:ACost1','')
  p_web.SSV('Prompt:ACost2','')
  p_web.SSV('Prompt:ACost3','')
  p_web.SSV('Prompt:ACost4','')
  p_web.SSV('Prompt:ACost5','')
  p_web.SSV('Prompt:ACost6','')
  p_web.SSV('Prompt:ACost7','')
  p_web.SSV('Prompt:ACost8','')
  p_web.SSV('tmp:ARCIgnoreReason','')
  p_web.SSV('tmp:RRCIgnoreReason','')
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ValidateIgnoreTickBox',0)
  
  !Empty Audit Queue
  clear(tmpaud:Record)
  tmpaud:sessionID = p_web.SessionID
  set(tmpaud:keySessionID,tmpaud:keySessionID)
  loop
      next(tempAuditQueue)
      if (error())
          break
      end ! if (error())
      if (tmpaud:sessionID <> p_web.sessionID)
          break
      end ! if (tmpaud:sessionID <> p_web.sessionID)
      delete(tempAuditQueue)
  end
  
  p_web.SSV('JobPricingRoutine:ForceWarranty',0)
  JobPricingRoutine(p_web)
  
  do pricingRoutine
  
  p_web.SSV('Hide:ARCCosts',1)
  p_web.SSV('Hide:RRCCosts',1)
  
  p_web.SSV('Hide:ARCChargeable',1)
  p_web.SSV('Hide:ARCWarranty',1)
  p_web.SSV('Hide:ARCClaim',1)
  p_web.SSV('Hide:ARC3rdParty',1)
  p_web.SSV('Hide:ARCEstimate',1)
  p_web.SSV('Hide:ARCCInvoice',1)
  p_web.SSV('Hide:ARCWInvoice',1)
  p_web.SSV('Hide:ARCClaimInvoice',1)
  p_web.SSV('Hide:ManufacturerPaid',1)
  p_web.SSV('Hide:RRCChargeable',1)
  p_web.SSV('Hide:RRCWarranty',1)
  p_web.SSV('Hide:RRCEstimate',1)
  p_web.SSV('Hide:RRCHandling',1)
  p_web.SSV('Hide:RRCExchange',1)
  p_web.SSV('Hide:RRCCInvoice',1)
  p_web.SSV('Hide:RRCWInvoice',1)
  p_web.SSV('Hide:Credit',1)
  
  p_web.SSV('ReadOnly:ARCCost1',1)
  p_web.SSV('ReadOnly:ARCCost2',1)
  p_web.SSV('ReadOnly:ARCCost3',1)
  p_web.SSV('ReadOnly:RRCCost1',1)
  p_web.SSV('ReadOnly:RRCCost2',1)
  p_web.SSV('ReadOnly:RRCCost3',1)
  
  
  sentToHub(p_web)
  
  if (p_web.GSV('jobe:WebJob') = 1)
      if (p_web.GSV('job:Chargeable_job') = 'YES')
          if (p_web.GSV('BookingSite') = 'RRC')
              p_web.SSV('Hide:RRCChargeable',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCChargeable',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  sentToHub(p_web)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:ARCChargeable',0)
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          else ! if (p_web.GSV('BookingSite') = 'RRC')
  
              p_web.SSV('Hide:RRCChargeable',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
  
              if (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCChargeable',0)
              end ! if (p_web.GSV('SentToHub') = 1)
          end ! if (p_web.GSV('BookingSite') = 'RRC')
      end ! if (p_web.GSV('job:Chargeable_job') = 'YES')
  
      if (p_web.GSV('job:Warranty_job') = 'YES')
          if (p_web.GSV('BookingSite') = 'RRC')
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  else
                      p_web.SSV('Hide:RRCWarranty',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          else ! if (p_web.GSV('BookingSite') = 'RRC'           )
              p_web.SSV('Hide:ARCClaim',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  else
                      p_web.SSV('Hide:RRCWarranty',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          end !
      end  !if (p_web.GSV('job:Warranty_job') <> 'YES')
  else ! if (p_web.GSV('jobe:WebJob'))
      if (p_web.GSV('BookingSite') <> 'RRC')
          if (p_web.GSV('job:Chargeable_Job') = 'YES')
              p_web.SSV('Hide:ARCChargeable',0)
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('job:Warranty_job') = 'YES')
              p_web.SSV('Hide:ARCWarranty',0)
              p_web.SSV('Hide:ARCClaim',0)
          end ! if (p_web.GSV('job:Warranty_job') = 'YES')
      end ! if (p_web.GSV('BookingSite') <> 'RRC')
  end ! if (p_web.GSV('jobe:WebJob'))
  
  
  Access:INVOICE.Clearkey(inv:invoice_Number_Key)
  inv:Invoice_Number    = p_web.GSV('job:Invoice_Number')
  if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Found
  else ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Error
  end ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
  
  if (p_web.GSV('job:Invoice_Number') > 0)
      if (p_web.GSV('Hide:ARCChargeable') = 0 and inv:ARCInvoiceDate > 0)
          p_web.SSV('Hide:ARCChargeable',1)
          p_web.SSV('Hide:ARCCInvoice',0)
      end ! if (p_web.GSV('Hide:ARCChargeable') = 0)
  
      if (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
          p_web.SSV('Hide:RRCChargeable',1)
          p_web.SSV('Hide:RRCCInvoice',0)
      end ! if (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
  end ! if (p_web.GSV('job:Invoice_Number') > 0)
  
  if (p_web.GSV('Hide:RRCWarranty') = 0 and p_web.GSV('wob:RRCWInvoiceNumber') > 0)
      p_web.SSV('Hide:RRCWarranty',1)
      p_web.SSV('Hide:RRCWInvoice',0)
  end ! if (p_web.GSV('Hide:RRCWarranty',0))
  
  if (p_web.GSV('job:Invoice_Number_Waranty') > 0)
      if (p_web.GSV('Hide:ARCWarranty') = 0)
          p_web.SSV('Hide:ARCWarranty',1)
          p_web.SSV('Hide:ARCWInvoice',0)
      end ! if (p_web.GSV('Hide:RRCWarranty',0))
  
      if (p_web.GSV('Hide:ARCClaim') = 0)
          p_web.SSV('Hide:ARCClaim',1)
          p_web.SSV('Hide:ARCClaimInvoice',0)
          p_web.SSV('Hide:ManufacturerPaid',0)
      end ! if (p_web.GSV('Hide:ARCClaim',0))
  end ! if (p_web.GSV('job:Invoice_Number_Waranty') > 0)
  
  if (p_web.GSV('Hide:RRCHandling') = 0)
      if ((p_web.GSV('job:Chargeable_Job') = 'YES' And |
          ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))) Or |
          (p_web.GSV('job:Warranty_job') = 'YES' And |
          ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))))
  
          p_web.SSV('Hide:RRCHandling',1)
      end
  end ! if (p_web.GSV('Hide:RRCHandling',0))
  
  if (p_web.GSV('job:Chargeable_Job') = 'YES')
      if (p_web.GSV('job:Estimate') = 'YES')
          if (p_web.GSV('job:Estimate_Accepted') <> 'YES' And |
              p_web.GSV('job:Estimate_Rejected'))
              p_web.SSV('Hide:RRCChargeable',1)
              p_web.SSV('Hide:ARCChargeable',1)
          end
  
          if (p_web.GSV('jobe:WebJob') = 1)
              p_web.SSV('Hide:RRCEstimate',0)
              if (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCEstimate',0)
              end ! if (p_web.GSV('SentToHub') = 1)
          else ! if (p_web.GSV('jobe:WebJob'))
              p_web.SSV('Hide:ARCEstimate',0)
          end ! if (p_web.GSV('jobe:WebJob'))
  
      end ! if (p_web.GSV('job:Estimate') = 'YES')
  
  end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  if (p_web.GSV('BookingSite') <> 'RRC')
      Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
      jot:RefNumber    = p_web.GSV('job:ref_Number')
      if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SSV('Hide:ARC3rdParty',0)
      else ! if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
  end ! if (p_web.GSV('BookingSite') <> 'RRC')
  
  if (p_web.GSV('Hide:RRCHandling') = 0)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:Chargeable    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type')
          if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Found
              if (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              end ! if (rtd:BER = 4)
          else ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          end ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:warranty    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type_warranty')
          if (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
              ! Found
              if (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              end ! if (rtd:BER = 4)
          else ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          end ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('Hide:RRCHandling') = 0)
  
  if (p_web.GSV('Hide:RRCCInvoice') = 0)
  
      Access:JOBSINV.Clearkey(jov:typeRecordKey)
      jov:refNumber    = p_web.GSV('job:Ref_Number')
      jov:type    = 'C'
      set(jov:typeRecordKey,jov:typeRecordKey)
      loop
          if (Access:JOBSINV.Next())
              Break
          end ! if (Access:JOBSINV.Next())
          if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
          if (jov:type    <> 'C')
              Break
          end ! if (jov:type    <> 'C')
          p_web.SSV('Hide:Credit',0)
          break
      end ! loop
  end ! if (p_web.GSV('Hide:RRCCInvoice') = 0)
  
  
  if (p_web.GSV('Hide:ARCChargeable') = 0 or |
      p_web.GSV('Hide:ARCWarranty') = 0 or |
      p_web.GSV('Hide:ARCClaim') = 0 or |
      p_web.GSV('Hide:ARC3rdParty') = 0 or |
      p_web.GSV('Hide:ARCEstimate') = 0 or |
      p_web.GSV('Hide:ARCCInvoice') = 0 or |
      p_web.GSV('Hide:ARCWInvoice') = 0 or |
      p_web.GSV('Hide:ARCClaimInvoice') = 0 or |
      p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('Hide:ARCCosts',0)
  end
  
  if (p_web.GSV('Hide:RRCChargeable') = 0 or |
      p_web.GSV('Hide:RRCWarranty') = 0 or |
      p_web.GSV('Hide:RRCEstimate') = 0 or |
      p_web.GSV('Hide:RRCHandling') = 0 or |
      p_web.GSV('Hide:RRCExchange') = 0 or |
      p_web.GSV('Hide:RRCCInvoice') = 0 or |
      p_web.GSV('Hide:RRCWInvoice') = 0 or |
      p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('Hide:RRCCosts',0)
  end
  
  if (p_web.GSV('Hide:RRCChargeable') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable')
  elsif (p_web.GSV('Hide:RRCWarranty') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty')
  elsif (p_web.GSV('Hide:RRCEstimate') = 0)
      p_web.SSV('tmp:RRCViewCostType','Estimate')
  elsif (p_web.GSV('Hide:RRCHandling') = 0)
      p_web.SSV('tmp:RRCViewCostType','Handling')
  elsif (p_web.GSV('Hide:RRCExchange') = 0)
      p_web.SSV('tmp:RRCViewCostType','Exchange')
  elsif (p_web.GSV('Hide:RRCCInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable - Invoiced')
  elsif (p_web.GSV('Hide:RRCWInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty - Invoiced')
  elsif (p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('tmp:RRCViewCostType','Credits')
  else
      p_web.SSV('tmp:RRCVIewCostType','')
  end !if (p_web.GSV('Hide:RRCChargeable') = 0)
  
  if (p_web.GSV('Hide:ARCChargeable') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeable')
  elsif (p_web.GSV('Hide:ARCWarranty') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty')
  elsif (p_web.GSV('Hide:ARCClaim') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim')
  elsif (p_web.GSV('Hide:ARC3rdParty') = 0)
      p_web.SSV('tmp:ARCViewCostType','3rd Party')
  elsif (p_web.GSV('Hide:ARCEstimate') = 0)
      p_web.SSV('tmp:ARCViewCostType','Estimate')
  elsif (p_web.GSV('Hide:ARCCInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeble - Invoiced')
  elsif (p_web.GSV('Hide:ARCWInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty - Invoiced')
  elsif (p_web.GSV('Hide:ARCClaimInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim - Invoiced')
  elsif (p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('tmp:ARCViewCostType','Manufacturer Payment')
  else
      p_web.SSV('tmp:ARCVIewCostType','')
  end !if (p_web.GSV('Hide:RRCChargeable') = 0)
  
  
  do displayARCCostFields
  do displayRRCCostFields
  do displayOtherFields
  
  do saveFields
  
  do DefaultLabourCost
  
  LoanAttachedToJob(p_web) ! Has a loan unit been, or is, attached?
  p_web.SetValue('ViewCosts_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do restoreFields
  Do DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ARCCost1')
    p_web.SetPicture('tmp:ARCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost1')
    p_web.SetPicture('tmp:AdjustmentCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost1','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost2')
    p_web.SetPicture('tmp:ARCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost2')
    p_web.SetPicture('tmp:AdjustmentCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost2','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost3')
    p_web.SetPicture('tmp:ARCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost3')
    p_web.SetPicture('tmp:AdjustmentCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost3','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost4')
    p_web.SetPicture('tmp:ARCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost4')
    p_web.SetPicture('tmp:AdjustmentCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost4','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost5')
    p_web.SetPicture('tmp:ARCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost5')
    p_web.SetPicture('tmp:AdjustmentCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost5','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost6')
    p_web.SetPicture('tmp:ARCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost6')
    p_web.SetPicture('tmp:AdjustmentCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost6','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost7')
    p_web.SetPicture('tmp:ARCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost8')
    p_web.SetPicture('tmp:ARCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost8','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost0')
    p_web.SetPicture('tmp:RRCCost0','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost0','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost1')
    p_web.SetPicture('tmp:RRCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost2')
    p_web.SetPicture('tmp:RRCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost3')
    p_web.SetPicture('tmp:RRCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost4')
    p_web.SetPicture('tmp:RRCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost5')
    p_web.SetPicture('tmp:RRCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost6')
    p_web.SetPicture('tmp:RRCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost7')
    p_web.SetPicture('tmp:RRCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost8')
    p_web.SetPicture('tmp:RRCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost8','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ARCViewCostType')
    tmp:ARCViewCostType = p_web.GetValue('tmp:ARCViewCostType')
    p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges')
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('tmp:ARCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreReason')
    tmp:ARCIgnoreReason = p_web.GetValue('tmp:ARCIgnoreReason')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  End
  if p_web.IfExistsValue('tmp:ARCCost1')
    tmp:ARCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost1')
    tmp:AdjustmentCost1 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  End
  if p_web.IfExistsValue('tmp:ARCCost2')
    tmp:ARCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost2')
    tmp:AdjustmentCost2 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  End
  if p_web.IfExistsValue('tmp:ARCCost3')
    tmp:ARCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost3')
    tmp:AdjustmentCost3 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  End
  if p_web.IfExistsValue('tmp:ARCCost4')
    tmp:ARCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost4')
    tmp:AdjustmentCost4 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  End
  if p_web.IfExistsValue('tmp:ARCCost5')
    tmp:ARCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost5')
    tmp:AdjustmentCost5 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  End
  if p_web.IfExistsValue('tmp:ARCCost6')
    tmp:ARCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost6')
    tmp:AdjustmentCost6 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  End
  if p_web.IfExistsValue('tmp:ARCCost7')
    tmp:ARCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  End
  if p_web.IfExistsValue('tmp:ARCCost8')
    tmp:ARCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  End
  if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber')
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('jobe:ARC3rdPartyInvoiceNumber')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  End
  if p_web.IfExistsValue('tmp:ARCInvoiceNumber')
    tmp:ARCInvoiceNumber = p_web.GetValue('tmp:ARCInvoiceNumber')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  End
  if p_web.IfExistsValue('tmp:RRCViewCostType')
    tmp:RRCViewCostType = p_web.GetValue('tmp:RRCViewCostType')
    p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges')
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('tmp:RRCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreReason')
    tmp:RRCIgnoreReason = p_web.GetValue('tmp:RRCIgnoreReason')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  End
  if p_web.IfExistsValue('tmp:RRCCost0')
    tmp:RRCCost0 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost0')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  End
  if p_web.IfExistsValue('tmp:RRCCost1')
    tmp:RRCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  End
  if p_web.IfExistsValue('tmp:originalInvoice')
    tmp:originalInvoice = p_web.GetValue('tmp:originalInvoice')
    p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  End
  if p_web.IfExistsValue('tmp:RRCCost2')
    tmp:RRCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  End
  if p_web.IfExistsValue('tmp:RRCCost3')
    tmp:RRCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  End
  if p_web.IfExistsValue('tmp:RRCCost4')
    tmp:RRCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  End
  if p_web.IfExistsValue('tmp:RRCCost5')
    tmp:RRCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  End
  if p_web.IfExistsValue('tmp:RRCCost6')
    tmp:RRCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  End
  if p_web.IfExistsValue('tmp:RRCCost7')
    tmp:RRCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  End
  if p_web.IfExistsValue('tmp:RRCCost8')
    tmp:RRCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  End
  if p_web.IfExistsValue('jobe:ExcReplcamentCharge')
    jobe:ExcReplcamentCharge = p_web.GetValue('jobe:ExcReplcamentCharge')
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ViewCosts_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ARCViewCostType = p_web.RestoreValue('tmp:ARCViewCostType')
 tmp:ARCIgnoreDefaultCharges = p_web.RestoreValue('tmp:ARCIgnoreDefaultCharges')
 tmp:ARCIgnoreReason = p_web.RestoreValue('tmp:ARCIgnoreReason')
 tmp:ARCCost1 = p_web.RestoreValue('tmp:ARCCost1')
 tmp:AdjustmentCost1 = p_web.RestoreValue('tmp:AdjustmentCost1')
 tmp:ARCCost2 = p_web.RestoreValue('tmp:ARCCost2')
 tmp:AdjustmentCost2 = p_web.RestoreValue('tmp:AdjustmentCost2')
 tmp:ARCCost3 = p_web.RestoreValue('tmp:ARCCost3')
 tmp:AdjustmentCost3 = p_web.RestoreValue('tmp:AdjustmentCost3')
 tmp:ARCCost4 = p_web.RestoreValue('tmp:ARCCost4')
 tmp:AdjustmentCost4 = p_web.RestoreValue('tmp:AdjustmentCost4')
 tmp:ARCCost5 = p_web.RestoreValue('tmp:ARCCost5')
 tmp:AdjustmentCost5 = p_web.RestoreValue('tmp:AdjustmentCost5')
 tmp:ARCCost6 = p_web.RestoreValue('tmp:ARCCost6')
 tmp:AdjustmentCost6 = p_web.RestoreValue('tmp:AdjustmentCost6')
 tmp:ARCCost7 = p_web.RestoreValue('tmp:ARCCost7')
 tmp:ARCCost8 = p_web.RestoreValue('tmp:ARCCost8')
 tmp:ARCInvoiceNumber = p_web.RestoreValue('tmp:ARCInvoiceNumber')
 tmp:RRCViewCostType = p_web.RestoreValue('tmp:RRCViewCostType')
 tmp:RRCIgnoreDefaultCharges = p_web.RestoreValue('tmp:RRCIgnoreDefaultCharges')
 tmp:RRCIgnoreReason = p_web.RestoreValue('tmp:RRCIgnoreReason')
 tmp:RRCCost0 = p_web.RestoreValue('tmp:RRCCost0')
 tmp:RRCCost1 = p_web.RestoreValue('tmp:RRCCost1')
 tmp:originalInvoice = p_web.RestoreValue('tmp:originalInvoice')
 tmp:RRCCost2 = p_web.RestoreValue('tmp:RRCCost2')
 tmp:RRCCost3 = p_web.RestoreValue('tmp:RRCCost3')
 tmp:RRCCost4 = p_web.RestoreValue('tmp:RRCCost4')
 tmp:RRCCost5 = p_web.RestoreValue('tmp:RRCCost5')
 tmp:RRCCost6 = p_web.RestoreValue('tmp:RRCCost6')
 tmp:RRCCost7 = p_web.RestoreValue('tmp:RRCCost7')
 tmp:RRCCost8 = p_web.RestoreValue('tmp:RRCCost8')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ViewCostsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewCosts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewCosts_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewCosts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ViewCostsReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ViewCosts" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ViewCosts" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ViewCosts" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('View Costs') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('View Costs',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ViewCosts">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ViewCosts" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ViewCosts')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Hide:ARCCosts') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ARC Costs') & ''''
        End
        If p_web.GSV('Hide:RRCCosts') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('RRC Costs') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ViewCosts')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ViewCosts'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Hide:ARCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ARCViewCostType')
    ElsIf p_web.GSV('Hide:RRCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:RRCViewCostType')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:ExcReplcamentCharge')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Hide:ARCCosts') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('Hide:RRCCosts') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ViewCosts')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Hide:ARCCosts') <> 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('Hide:RRCCosts') <> 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Hide:ARCCosts') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('ARC Costs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewCosts_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCViewCostType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line1
      do Comment::__line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCIgnoreDefaultCharges
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCIgnoreReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAcceptARCReason
      do Comment::buttonAcceptARCReason
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCancelARCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCancelARCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line2
      do Comment::__line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:ARC3rdPartyInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:ARC3rdPartyInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:ARC3rdPartyInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('Hide:RRCCosts') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('RRC Costs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewCosts_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCViewCostType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line3
      do Comment::__line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCIgnoreDefaultCharges
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCIgnoreReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAcceptRRCReason
      do Comment::buttonAcceptRRCReason
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCancelRRCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCancelRRCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost0
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost0
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost0
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:originalInvoice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:originalInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:originalInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseJobCredits
      do Comment::BrowseJobCredits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line4
      do Comment::__line4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewCosts_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:ExcReplcamentCharge
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:ExcReplcamentCharge
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:ExcReplcamentCharge
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:ARCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('View Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_prompt')

Validate::tmp:ARCViewCostType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCViewCostType',p_web.GetValue('NewValue'))
    tmp:ARCViewCostType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCViewCostType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCViewCostType',p_web.GetValue('Value'))
    tmp:ARCViewCostType = p_web.GetValue('Value')
  End
  do displayARCCostFields
  do displayOtherFields
  do Value::tmp:ARCViewCostType
  do SendAlert
  do Prompt::tmp:ARCCost1
  do Value::tmp:ARCCost1  !1
  do Comment::tmp:ARCCost1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Comment::tmp:ARCCost2
  do Prompt::tmp:ARCCost3
  do Value::tmp:ARCCost3  !1
  do Comment::tmp:ARCCost3
  do Prompt::tmp:ARCCost4
  do Value::tmp:ARCCost4  !1
  do Comment::tmp:ARCCost4
  do Prompt::tmp:ARCCost5
  do Value::tmp:ARCCost5  !1
  do Comment::tmp:ARCCost5
  do Prompt::tmp:ARCCost6
  do Value::tmp:ARCCost6  !1
  do Comment::tmp:ARCCost6
  do Prompt::tmp:ARCCost7
  do Value::tmp:ARCCost7  !1
  do Comment::tmp:ARCCost7
  do Prompt::tmp:ARCCost8
  do Value::tmp:ARCCost8  !1
  do Comment::tmp:ARCCost8
  do Value::tmp:AdjustmentCost1  !1
  do Value::tmp:AdjustmentCost2  !1
  do Value::tmp:AdjustmentCost3  !1
  do Value::tmp:AdjustmentCost4  !1
  do Value::tmp:AdjustmentCost5  !1
  do Value::tmp:AdjustmentCost6  !1
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::jobe:ARC3rdPartyInvoiceNumber
  do Value::jobe:ARC3rdPartyInvoiceNumber  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1

Value::tmp:ARCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ARCViewCostType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCViewCostType'',''viewcosts_tmp:arcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ARCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ARCViewCostType') = 0
    p_web.SetSessionValue('tmp:ARCViewCostType','Chargeable')
  end
  If p_web.GSV('Hide:ARCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaim') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim','Claim',choose('Claim' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARC3rdParty') <> 1
    packet = clip(packet) & p_web.CreateOption('3rd Party','3rd Party',choose('3rd Party' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaimInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim - Invoiced','Claim - Invoiced',choose('Claim - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ManufacturerPaid') <> 1
    packet = clip(packet) & p_web.CreateOption('Manufacturer Payment','Manufacturer Payment',choose('Manufacturer Payment' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_value')

Comment::tmp:ARCViewCostType  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line1',p_web.GetValue('NewValue'))
    do Value::__line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line1  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Ignore Default Charges')
  If p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_prompt')

Validate::tmp:ARCIgnoreDefaultCharges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',p_web.GetValue('NewValue'))
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCIgnoreDefaultCharges
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',p_web.GetValue('Value'))
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  if (p_web.GSV('tmp:ARCIgnoreDefaultCharges') = 1)
      p_web.SSV('ValidateIgnoreTickBoxARC',1)
      p_web.SSV('tmp:ARCIgnoreReason','')
  else  ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','NO')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','NO')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','NO')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      do pricingRoutine
  end ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:ARCIgnoreDefaultCharges
  do SendAlert
  do Value::buttonAcceptARCReason  !1
  do Value::buttonCancelARCReason  !1
  do Value::tmp:ARCViewCostType  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCCost3  !1
  do Value::tmp:ARCCost4  !1
  do Value::tmp:ARCCost5  !1
  do Value::tmp:ARCCost6  !1
  do Value::tmp:ARCCost7  !1
  do Value::tmp:ARCCost8  !1

Value::tmp:ARCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
  ! --- CHECKBOX --- tmp:ARCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ARCIgnoreDefaultCharges'',''viewcosts_tmp:arcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreDefaultCharges')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:ARCIgnoreDefaultCharges',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_value')

Comment::tmp:ARCIgnoreDefaultCharges  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Reason For Ignoring Standard Charges')
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_prompt')

Validate::tmp:ARCIgnoreReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',p_web.GetValue('NewValue'))
    tmp:ARCIgnoreReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCIgnoreReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',p_web.GetValue('Value'))
    tmp:ARCIgnoreReason = p_web.GetValue('Value')
  End
  p_web.SSV('tmp:ARCIgnoreReason',BHStripReplace(p_web.GSV('tmp:ARCIgnoreReason'),'<9>',''))
  do Value::tmp:ARCIgnoreReason
  do SendAlert

Value::tmp:ARCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- TEXT --- tmp:ARCIgnoreReason
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ARCIgnoreReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCIgnoreReason'',''viewcosts_tmp:arcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('tmp:ARCIgnoreReason',p_web.GetSessionValue('tmp:ARCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:ARCIgnoreReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_value')

Comment::tmp:ARCIgnoreReason  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAcceptARCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAcceptARCReason',p_web.GetValue('NewValue'))
    do Value::buttonAcceptARCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('tmp:ARCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBoxARC',0)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT ARC CHARGEABLE COSTS',|
                          p_web.GSV('job:Ignore_Chargeable_Charges'), |
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV(job:Ignore_Chargeable_Charges,'YES')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT ARC WARRANTY COSTS',|
                          p_web.GSV('job:Ignore_Warranty_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('job:Ignore_Estimate_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:ARCCost2',0)
  else ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','YES')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  end ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptARCReason
  do SendAlert
  do Value::buttonCancelARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCViewCostType  !1

Value::buttonAcceptARCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptARCReason'',''viewcosts_buttonacceptarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AcceptARCReason','Accept','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_value')

Comment::buttonAcceptARCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCancelARCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCancelARCReason',p_web.GetValue('NewValue'))
    do Value::buttonCancelARCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ReadOnly:ARCCost2',1)
  case p_web.GSV('tmp:ARCViewCostType')
  of 'Chargeable'
      p_web.SSV('job:Ignore_Chargeable_Charges','NO')
  of 'Warranty'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  of 'Estimate'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  end ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  do Value::buttonCancelARCReason
  do SendAlert
  do Value::buttonAcceptARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Prompt::tmp:ARCViewCostType
  do Value::tmp:ARCViewCostType  !1

Value::buttonCancelARCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelARCReason'',''viewcosts_buttoncancelarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CancelARCReason','Cancel','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_value')

Comment::buttonCancelARCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_prompt',Choose(p_web.GSV('Prompt:Cost1') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost1'))
  If p_web.GSV('Prompt:Cost1') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_prompt')

Validate::tmp:ARCCost1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost1',p_web.GetValue('NewValue'))
    tmp:ARCCost1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost1',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost1
  do SendAlert

Value::tmp:ARCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_value',Choose(p_web.GSV('Prompt:Cost1') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost1') = '')
  ! --- STRING --- tmp:ARCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ARCCost1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost1'',''viewcosts_tmp:arccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost1',p_web.GetSessionValue('tmp:ARCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_value')

Comment::tmp:ARCCost1  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_comment',Choose(p_web.GSV('Prompt:Cost1') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_comment')

Validate::tmp:AdjustmentCost1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost1',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost1',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost1
  do SendAlert

Value::tmp:AdjustmentCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost1'',''viewcosts_tmp:adjustmentcost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost1',p_web.GetSessionValue('tmp:AdjustmentCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_value')

Comment::tmp:AdjustmentCost1  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_prompt',Choose(p_web.GSV('Prompt:Cost2') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost2'))
  If p_web.GSV('Prompt:Cost2') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_prompt')

Validate::tmp:ARCCost2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost2',p_web.GetValue('NewValue'))
    tmp:ARCCost2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost2',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost2
  do SendAlert

Value::tmp:ARCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_value',Choose(p_web.GSV('Prompt:Cost2') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost2') = '')
  ! --- STRING --- tmp:ARCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ARCCost2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost2'',''viewcosts_tmp:arccost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost2',p_web.GetSessionValue('tmp:ARCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_value')

Comment::tmp:ARCCost2  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_comment',Choose(p_web.GSV('Prompt:Cost2') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_comment')

Validate::tmp:AdjustmentCost2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost2',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost2',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost2
  do SendAlert

Value::tmp:AdjustmentCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost2'',''viewcosts_tmp:adjustmentcost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost2',p_web.GetSessionValue('tmp:AdjustmentCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_value')

Comment::tmp:AdjustmentCost2  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_prompt',Choose(p_web.GSV('Prompt:Cost3') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost3'))
  If p_web.GSV('Prompt:Cost3') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_prompt')

Validate::tmp:ARCCost3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost3',p_web.GetValue('NewValue'))
    tmp:ARCCost3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost3',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost3
  do SendAlert

Value::tmp:ARCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_value',Choose(p_web.GSV('Prompt:Cost3') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost3') = '')
  ! --- STRING --- tmp:ARCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ARCCost3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost3'',''viewcosts_tmp:arccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost3',p_web.GetSessionValue('tmp:ARCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_value')

Comment::tmp:ARCCost3  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_comment',Choose(p_web.GSV('Prompt:Cost3') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_comment')

Validate::tmp:AdjustmentCost3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost3',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost3',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost3
  do SendAlert

Value::tmp:AdjustmentCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost3'',''viewcosts_tmp:adjustmentcost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost3',p_web.GetSessionValue('tmp:AdjustmentCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_value')

Comment::tmp:AdjustmentCost3  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line2',p_web.GetValue('NewValue'))
    do Value::__line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line2  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_prompt',Choose(p_web.GSV('Prompt:Cost4') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost4'))
  If p_web.GSV('Prompt:Cost4') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_prompt')

Validate::tmp:ARCCost4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost4',p_web.GetValue('NewValue'))
    tmp:ARCCost4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost4',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost4
  do SendAlert

Value::tmp:ARCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_value',Choose(p_web.GSV('Prompt:Cost4') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost4') = '')
  ! --- STRING --- tmp:ARCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost4'',''viewcosts_tmp:arccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost4',p_web.GetSessionValue('tmp:ARCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_value')

Comment::tmp:ARCCost4  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_comment',Choose(p_web.GSV('Prompt:Cost4') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_comment')

Validate::tmp:AdjustmentCost4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost4',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost4',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost4
  do SendAlert

Value::tmp:AdjustmentCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost4'',''viewcosts_tmp:adjustmentcost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost4',p_web.GetSessionValue('tmp:AdjustmentCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_value')

Comment::tmp:AdjustmentCost4  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_prompt',Choose(p_web.GSV('Prompt:Cost5') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost5'))
  If p_web.GSV('Prompt:Cost5') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_prompt')

Validate::tmp:ARCCost5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost5',p_web.GetValue('NewValue'))
    tmp:ARCCost5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost5',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost5
  do SendAlert

Value::tmp:ARCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_value',Choose(p_web.GSV('Prompt:Cost5') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost5') = '')
  ! --- STRING --- tmp:ARCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost5'',''viewcosts_tmp:arccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost5',p_web.GetSessionValue('tmp:ARCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_value')

Comment::tmp:ARCCost5  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_comment',Choose(p_web.GSV('Prompt:Cost5') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_comment')

Validate::tmp:AdjustmentCost5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost5',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost5',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost5
  do SendAlert

Value::tmp:AdjustmentCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost5'',''viewcosts_tmp:adjustmentcost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost5',p_web.GetSessionValue('tmp:AdjustmentCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_value')

Comment::tmp:AdjustmentCost5  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_prompt',Choose(p_web.GSV('Prompt:Cost6') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost6'))
  If p_web.GSV('Prompt:Cost6') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_prompt')

Validate::tmp:ARCCost6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost6',p_web.GetValue('NewValue'))
    tmp:ARCCost6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost6',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost6
  do SendAlert

Value::tmp:ARCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_value',Choose(p_web.GSV('Prompt:Cost6') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost6') = '')
  ! --- STRING --- tmp:ARCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost6'',''viewcosts_tmp:arccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost6',p_web.GetSessionValue('tmp:ARCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_value')

Comment::tmp:ARCCost6  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_comment',Choose(p_web.GSV('Prompt:Cost6') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_comment')

Validate::tmp:AdjustmentCost6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost6',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost6',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost6
  do SendAlert

Value::tmp:AdjustmentCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost6'',''viewcosts_tmp:adjustmentcost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost6',p_web.GetSessionValue('tmp:AdjustmentCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_value')

Comment::tmp:AdjustmentCost6  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_prompt',Choose(p_web.GSV('Prompt:Cost7') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost7'))
  If p_web.GSV('Prompt:Cost7') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_prompt')

Validate::tmp:ARCCost7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost7',p_web.GetValue('NewValue'))
    tmp:ARCCost7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost7',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost7
  do SendAlert

Value::tmp:ARCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_value',Choose(p_web.GSV('Prompt:Cost7') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost7') = '')
  ! --- STRING --- tmp:ARCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost7'',''viewcosts_tmp:arccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost7',p_web.GetSessionValue('tmp:ARCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_value')

Comment::tmp:ARCCost7  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_comment',Choose(p_web.GSV('Prompt:Cost7') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_comment')

Prompt::tmp:ARCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_prompt',Choose(p_web.GSV('Prompt:Cost8') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost8'))
  If p_web.GSV('Prompt:Cost8') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_prompt')

Validate::tmp:ARCCost8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost8',p_web.GetValue('NewValue'))
    tmp:ARCCost8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost8',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost8
  do SendAlert

Value::tmp:ARCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_value',Choose(p_web.GSV('Prompt:Cost8') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost8') = '')
  ! --- STRING --- tmp:ARCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost8'',''viewcosts_tmp:arccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost8',p_web.GetSessionValue('tmp:ARCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_value')

Comment::tmp:ARCCost8  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_comment',Choose(p_web.GSV('Prompt:Cost8') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_comment')

Prompt::jobe:ARC3rdPartyInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_prompt',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_prompt')

Validate::jobe:ARC3rdPartyInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',p_web.GetValue('NewValue'))
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:ARC3rdPartyInvoiceNumber
    do Value::jobe:ARC3rdPartyInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('Value')
  End

Value::jobe:ARC3rdPartyInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_value',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party')
  ! --- DISPLAY --- jobe:ARC3rdPartyInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('jobe:ARC3rdPartyInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_value')

Comment::jobe:ARC3rdPartyInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_comment',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_prompt',Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If ~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType'))
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ARCInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',p_web.GetValue('NewValue'))
    tmp:ARCInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',p_web.GetValue('Value'))
    tmp:ARCInvoiceNumber = p_web.GetValue('Value')
  End

Value::tmp:ARCInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_value',Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'hdiv','adiv'))
  loc:extra = ''
  If Not (~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')))
  ! --- DISPLAY --- tmp:ARCInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ARCInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::tmp:ARCInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_comment',Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If ~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType'))
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('View Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:RRCViewCostType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCViewCostType',p_web.GetValue('NewValue'))
    tmp:RRCViewCostType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCViewCostType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCViewCostType',p_web.GetValue('Value'))
    tmp:RRCViewCostType = p_web.GetValue('Value')
  End
  do displayRRCCostFields
  do displayOtherFields
  do Value::tmp:RRCViewCostType
  do SendAlert
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Prompt::tmp:RRCCost1
  do Value::tmp:RRCCost1  !1
  do Comment::tmp:RRCCost1
  do Prompt::tmp:RRCCost2
  do Value::tmp:RRCCost2  !1
  do Comment::tmp:RRCCost2
  do Prompt::tmp:RRCCost3
  do Value::tmp:RRCCost3  !1
  do Comment::tmp:RRCCost3
  do Prompt::tmp:RRCCost4
  do Value::tmp:RRCCost4  !1
  do Comment::tmp:RRCCost4
  do Prompt::tmp:RRCCost5
  do Value::tmp:RRCCost5  !1
  do Comment::tmp:RRCCost5
  do Prompt::tmp:RRCCost6
  do Value::tmp:RRCCost6  !1
  do Comment::tmp:RRCCost6
  do Prompt::tmp:RRCCost7
  do Value::tmp:RRCCost7  !1
  do Comment::tmp:RRCCost7
  do Prompt::tmp:RRCCost8
  do Value::tmp:RRCCost8  !1
  do Comment::tmp:RRCCost8
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::tmp:RRCIgnoreDefaultCharges
  do Value::tmp:RRCIgnoreDefaultCharges  !1

Value::tmp:RRCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:RRCViewCostType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCViewCostType'',''viewcosts_tmp:rrcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:RRCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:RRCViewCostType') = 0
    p_web.SetSessionValue('tmp:RRCViewCostType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCHandling') <> 1
    packet = clip(packet) & p_web.CreateOption('Handling','Handling',choose('Handling' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCExchange') <> 1
    packet = clip(packet) & p_web.CreateOption('Exchange','Exchange',choose('Exchange' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:Credit') <> 1
    packet = clip(packet) & p_web.CreateOption('Credits','Credits',choose('Credits' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_value')

Comment::tmp:RRCViewCostType  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line3',p_web.GetValue('NewValue'))
    do Value::__line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line3  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Ignore Default Charges')
  If p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_prompt')

Validate::tmp:RRCIgnoreDefaultCharges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',p_web.GetValue('NewValue'))
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCIgnoreDefaultCharges
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',p_web.GetValue('Value'))
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('tmp:RRCIgnoreReason','')
      p_web.SSV('ValidateIgnoreTickBox',1)
  else  ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('jobe2:JobDiscountAmnt',0)
      do pricingRoutine
      do displayRRCCostFields
  end ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:RRCIgnoreDefaultCharges
  do SendAlert
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::buttonAcceptRRCReason  !1
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

Value::tmp:RRCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_value',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
  ! --- CHECKBOX --- tmp:RRCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RRCIgnoreDefaultCharges'',''viewcosts_tmp:rrcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(tmp:RRCIgnoreReason)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1),'disabled','')
  If p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:RRCIgnoreDefaultCharges',clip(true),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_value')

Comment::tmp:RRCIgnoreDefaultCharges  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Reason For Ignoring Standard Charges')
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_prompt')

Validate::tmp:RRCIgnoreReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',p_web.GetValue('NewValue'))
    tmp:RRCIgnoreReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCIgnoreReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',p_web.GetValue('Value'))
    tmp:RRCIgnoreReason = p_web.GetValue('Value')
  End
  If tmp:RRCIgnoreReason = ''
    loc:Invalid = 'tmp:RRCIgnoreReason'
    loc:alert = p_web.translate('Enter Reason For Ignoring Standard Charges') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:RRCIgnoreReason = Upper(tmp:RRCIgnoreReason)
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  p_web.SSV('tmp:RRCIgnoreReason',BHStripReplace(p_web.GSV('tmp:RRCIgnoreReason'),'<9>',''))
  do Value::tmp:RRCIgnoreReason
  do SendAlert

Value::tmp:RRCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- TEXT --- tmp:RRCIgnoreReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:RRCIgnoreReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:RRCIgnoreReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCIgnoreReason'',''viewcosts_tmp:rrcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('tmp:RRCIgnoreReason',p_web.GetSessionValue('tmp:RRCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:RRCIgnoreReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_value')

Comment::tmp:RRCIgnoreReason  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAcceptRRCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAcceptRRCReason',p_web.GetValue('NewValue'))
    do Value::buttonAcceptRRCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBox',0)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT RRC CHARGEABLE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCChaCosts'), |
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCChaCosts',1)
          p_web.SSV('Prompt:RCost0','Discount')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT RRC WARRANTY COSTS',|
                          p_web.GSV('jobe:IgnoreRRCWarCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCWarCosts',1)
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCEstCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'),|
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCEstCosts',1)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:RRCCost2',0)
  else ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
      p_web.SSV('Prompt:RCost0','')
  end ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptRRCReason
  do SendAlert
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCViewCostType  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

Value::buttonAcceptRRCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptRRCReason'',''viewcosts_buttonacceptrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AcceptRRCReason','Accept','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_value')

Comment::buttonAcceptRRCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCancelRRCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCancelRRCReason',p_web.GetValue('NewValue'))
    do Value::buttonCancelRRCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('ValidateIgnoreTickBox',0)
  p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
  do Value::buttonCancelRRCReason
  do SendAlert
  do Value::buttonAcceptRRCReason  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Value::tmp:RRCCost2  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

Value::buttonCancelRRCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelRRCReason'',''viewcosts_buttoncancelrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CancelRRCReason','Cancel','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_value')

Comment::buttonCancelRRCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCCost0  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_prompt',Choose(p_web.GSV('Prompt:RCost0') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost0'))
  If p_web.GSV('Prompt:RCost0') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_prompt')

Validate::tmp:RRCCost0  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost0',p_web.GetValue('NewValue'))
    tmp:RRCCost0 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost0
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost0',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost0 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost0
  do SendAlert

Value::tmp:RRCCost0  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_value',Choose(p_web.GSV('Prompt:RCost0') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost0') = '')
  ! --- STRING --- tmp:RRCCost0
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost0')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost0'',''viewcosts_tmp:rrccost0_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost0',p_web.GetSessionValue('tmp:RRCCost0'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_value')

Comment::tmp:RRCCost0  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_comment',Choose(p_web.GSV('Prompt:RCost0') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost0') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_comment')

Prompt::tmp:RRCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_prompt',Choose(p_web.GSV('Prompt:RCost1') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost1'))
  If p_web.GSV('Prompt:RCost1') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_prompt')

Validate::tmp:RRCCost1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost1',p_web.GetValue('NewValue'))
    tmp:RRCCost1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost1',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost1
  do SendAlert

Value::tmp:RRCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_value',Choose(p_web.GSV('Prompt:RCost1') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost1') = '')
  ! --- STRING --- tmp:RRCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:RRCCost1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:RRCCost1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost1'',''viewcosts_tmp:rrccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost1',p_web.GetSessionValue('tmp:RRCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_value')

Comment::tmp:RRCCost1  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_comment',Choose(p_web.GSV('Prompt:RCost1') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_comment')

Prompt::tmp:originalInvoice  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Original Invoice No')
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:originalInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:originalInvoice',p_web.GetValue('NewValue'))
    tmp:originalInvoice = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:originalInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:originalInvoice',p_web.GetValue('Value'))
    tmp:originalInvoice = p_web.GetValue('Value')
  End
  do Value::tmp:originalInvoice
  do SendAlert

Value::tmp:originalInvoice  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_value',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
  ! --- STRING --- tmp:originalInvoice
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:originalInvoice')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:originalInvoice'',''viewcosts_tmp:originalinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:originalInvoice',p_web.GetSessionValueFormat('tmp:originalInvoice'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_value')

Comment::tmp:originalInvoice  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_prompt',Choose(p_web.GSV('Prompt:RCost2') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost2'))
  If p_web.GSV('Prompt:RCost2') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_prompt')

Validate::tmp:RRCCost2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost2',p_web.GetValue('NewValue'))
    tmp:RRCCost2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost2',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      if (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          p_web.SSV('Comment:RCost2','Error! Cost is higher than Default Cost')
          p_web.SSV('Prompt:RCost0','') ! Blank the discount field.
      else
          p_web.SSV('Comment:RCost2','')
      end
  end ! 'tmp:RRCViewCostType'
  do updateRRCCost
  do pricingRoutine
  do displayRRCCostFields
  do Value::tmp:RRCCost2
  do SendAlert
  do Prompt::tmp:RRCCost2
  do Comment::tmp:RRCCost2
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

Value::tmp:RRCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_value',Choose(p_web.GSV('Prompt:RCost2') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost2') = '')
  ! --- STRING --- tmp:RRCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:RRCCost2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:RRCCost2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost2'',''viewcosts_tmp:rrccost2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCCost2')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost2',p_web.GetSessionValue('tmp:RRCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_value')

Comment::tmp:RRCCost2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:RCost2'))
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_comment',Choose(p_web.GSV('Prompt:RCost2') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_comment')

Prompt::tmp:RRCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_prompt',Choose(p_web.GSV('Prompt:RCost3') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost3'))
  If p_web.GSV('Prompt:RCost3') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_prompt')

Validate::tmp:RRCCost3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost3',p_web.GetValue('NewValue'))
    tmp:RRCCost3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost3',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost3
  do SendAlert

Value::tmp:RRCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_value',Choose(p_web.GSV('Prompt:RCost3') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost3') = '')
  ! --- STRING --- tmp:RRCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:RRCCost3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:RRCCost3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost3'',''viewcosts_tmp:rrccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost3',p_web.GetSessionValue('tmp:RRCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_value')

Comment::tmp:RRCCost3  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_comment',Choose(p_web.GSV('Prompt:RCost3') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_comment')

Validate::BrowseJobCredits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseJobCredits',p_web.GetValue('NewValue'))
    do Value::BrowseJobCredits
  Else
    p_web.StoreValue('jov:RecordNumber')
  End

Value::BrowseJobCredits  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits',1,0))
  ! --- BROWSE ---  BrowseJobCredits --
  p_web.SetValue('BrowseJobCredits:NoForm',1)
  p_web.SetValue('BrowseJobCredits:FormName',loc:formname)
  p_web.SetValue('BrowseJobCredits:parentIs','Form')
  p_web.SetValue('_parentProc','ViewCosts')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ViewCosts_BrowseJobCredits_embedded_div')&'"><!-- Net:BrowseJobCredits --></div><13,10>'
    p_web._DivHeader('ViewCosts_' & lower('BrowseJobCredits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ViewCosts_' & lower('BrowseJobCredits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseJobCredits --><13,10>'
  end
  do SendPacket

Comment::BrowseJobCredits  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('BrowseJobCredits') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line4',p_web.GetValue('NewValue'))
    do Value::__line4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line4  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_prompt',Choose(p_web.GSV('Prompt:RCost4') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost4'))
  If p_web.GSV('Prompt:RCost4') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_prompt')

Validate::tmp:RRCCost4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost4',p_web.GetValue('NewValue'))
    tmp:RRCCost4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost4',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost4
  do SendAlert

Value::tmp:RRCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_value',Choose(p_web.GSV('Prompt:RCost4') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost4') = '')
  ! --- STRING --- tmp:RRCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost4'',''viewcosts_tmp:rrccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost4',p_web.GetSessionValue('tmp:RRCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_value')

Comment::tmp:RRCCost4  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_comment',Choose(p_web.GSV('Prompt:RCost4') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_comment')

Prompt::tmp:RRCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_prompt',Choose(p_web.GSV('Prompt:RCost5') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost5'))
  If p_web.GSV('Prompt:RCost5') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_prompt')

Validate::tmp:RRCCost5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost5',p_web.GetValue('NewValue'))
    tmp:RRCCost5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost5',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost5
  do SendAlert

Value::tmp:RRCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_value',Choose(p_web.GSV('Prompt:RCost5') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost5') = '')
  ! --- STRING --- tmp:RRCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost5'',''viewcosts_tmp:rrccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost5',p_web.GetSessionValue('tmp:RRCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_value')

Comment::tmp:RRCCost5  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_comment',Choose(p_web.GSV('Prompt:RCost5') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_comment')

Prompt::tmp:RRCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_prompt',Choose(p_web.GSV('Prompt:RCost6') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost6'))
  If p_web.GSV('Prompt:RCost6') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_prompt')

Validate::tmp:RRCCost6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost6',p_web.GetValue('NewValue'))
    tmp:RRCCost6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost6',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost6
  do SendAlert

Value::tmp:RRCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_value',Choose(p_web.GSV('Prompt:RCost6') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost6') = '')
  ! --- STRING --- tmp:RRCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost6'',''viewcosts_tmp:rrccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost6',p_web.GetSessionValue('tmp:RRCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_value')

Comment::tmp:RRCCost6  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_comment',Choose(p_web.GSV('Prompt:RCost6') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_comment')

Prompt::tmp:RRCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_prompt',Choose(p_web.GSV('Prompt:RCost7') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost7'))
  If p_web.GSV('Prompt:RCost7') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_prompt')

Validate::tmp:RRCCost7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost7',p_web.GetValue('NewValue'))
    tmp:RRCCost7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost7',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost7
  do SendAlert

Value::tmp:RRCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_value',Choose(p_web.GSV('Prompt:RCost7') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost7') = '')
  ! --- STRING --- tmp:RRCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost7'',''viewcosts_tmp:rrccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost7',p_web.GetSessionValue('tmp:RRCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_value')

Comment::tmp:RRCCost7  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_comment',Choose(p_web.GSV('Prompt:RCost7') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_comment')

Prompt::tmp:RRCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_prompt',Choose(p_web.GSV('Prompt:RCost8') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost8'))
  If p_web.GSV('Prompt:RCost8') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_prompt')

Validate::tmp:RRCCost8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost8',p_web.GetValue('NewValue'))
    tmp:RRCCost8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost8',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost8
  do SendAlert

Value::tmp:RRCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_value',Choose(p_web.GSV('Prompt:RCost8') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost8') = '')
  ! --- STRING --- tmp:RRCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost8'',''viewcosts_tmp:rrccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost8',p_web.GetSessionValue('tmp:RRCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_value')

Comment::tmp:RRCCost8  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_comment',Choose(p_web.GSV('Prompt:RCost8') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_comment')

Prompt::jobe:ExcReplcamentCharge  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_prompt',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Replacement')
  If p_web.GSV('Hide:ExchangeReplacement') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_prompt')

Validate::jobe:ExcReplcamentCharge  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',p_web.GetValue('NewValue'))
    jobe:ExcReplcamentCharge = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:ExcReplcamentCharge
    do Value::jobe:ExcReplcamentCharge
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',p_web.GetValue('Value'))
    jobe:ExcReplcamentCharge = p_web.GetValue('Value')
  End
  do Value::jobe:ExcReplcamentCharge
  do SendAlert

Value::jobe:ExcReplcamentCharge  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_value',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangeReplacement') = 1)
  ! --- CHECKBOX --- jobe:ExcReplcamentCharge
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:ExcReplcamentCharge'',''viewcosts_jobe:excreplcamentcharge_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe:ExcReplcamentCharge') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:ExcReplcamentCharge',clip(1),,loc:readonly,,,loc:javascript,,'Exchange Replacement Charge') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_value')

Comment::jobe:ExcReplcamentCharge  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_comment',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangeReplacement') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ViewCosts_tmp:ARCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCViewCostType
      else
        do Value::tmp:ARCViewCostType
      end
  of lower('ViewCosts_tmp:ARCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreDefaultCharges
      else
        do Value::tmp:ARCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:ARCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreReason
      else
        do Value::tmp:ARCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptARCReason
      else
        do Value::buttonAcceptARCReason
      end
  of lower('ViewCosts_buttonCancelARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelARCReason
      else
        do Value::buttonCancelARCReason
      end
  of lower('ViewCosts_tmp:ARCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost1
      else
        do Value::tmp:ARCCost1
      end
  of lower('ViewCosts_tmp:AdjustmentCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost1
      else
        do Value::tmp:AdjustmentCost1
      end
  of lower('ViewCosts_tmp:ARCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost2
      else
        do Value::tmp:ARCCost2
      end
  of lower('ViewCosts_tmp:AdjustmentCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost2
      else
        do Value::tmp:AdjustmentCost2
      end
  of lower('ViewCosts_tmp:ARCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost3
      else
        do Value::tmp:ARCCost3
      end
  of lower('ViewCosts_tmp:AdjustmentCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost3
      else
        do Value::tmp:AdjustmentCost3
      end
  of lower('ViewCosts_tmp:ARCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost4
      else
        do Value::tmp:ARCCost4
      end
  of lower('ViewCosts_tmp:AdjustmentCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost4
      else
        do Value::tmp:AdjustmentCost4
      end
  of lower('ViewCosts_tmp:ARCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost5
      else
        do Value::tmp:ARCCost5
      end
  of lower('ViewCosts_tmp:AdjustmentCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost5
      else
        do Value::tmp:AdjustmentCost5
      end
  of lower('ViewCosts_tmp:ARCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost6
      else
        do Value::tmp:ARCCost6
      end
  of lower('ViewCosts_tmp:AdjustmentCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost6
      else
        do Value::tmp:AdjustmentCost6
      end
  of lower('ViewCosts_tmp:ARCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost7
      else
        do Value::tmp:ARCCost7
      end
  of lower('ViewCosts_tmp:ARCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost8
      else
        do Value::tmp:ARCCost8
      end
  of lower('ViewCosts_tmp:RRCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCViewCostType
      else
        do Value::tmp:RRCViewCostType
      end
  of lower('ViewCosts_tmp:RRCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreDefaultCharges
      else
        do Value::tmp:RRCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:RRCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreReason
      else
        do Value::tmp:RRCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptRRCReason
      else
        do Value::buttonAcceptRRCReason
      end
  of lower('ViewCosts_buttonCancelRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelRRCReason
      else
        do Value::buttonCancelRRCReason
      end
  of lower('ViewCosts_tmp:RRCCost0_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost0
      else
        do Value::tmp:RRCCost0
      end
  of lower('ViewCosts_tmp:RRCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost1
      else
        do Value::tmp:RRCCost1
      end
  of lower('ViewCosts_tmp:originalInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:originalInvoice
      else
        do Value::tmp:originalInvoice
      end
  of lower('ViewCosts_tmp:RRCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost2
      else
        do Value::tmp:RRCCost2
      end
  of lower('ViewCosts_tmp:RRCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost3
      else
        do Value::tmp:RRCCost3
      end
  of lower('ViewCosts_tmp:RRCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost4
      else
        do Value::tmp:RRCCost4
      end
  of lower('ViewCosts_tmp:RRCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost5
      else
        do Value::tmp:RRCCost5
      end
  of lower('ViewCosts_tmp:RRCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost6
      else
        do Value::tmp:RRCCost6
      end
  of lower('ViewCosts_tmp:RRCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost7
      else
        do Value::tmp:RRCCost7
      end
  of lower('ViewCosts_tmp:RRCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost8
      else
        do Value::tmp:RRCCost8
      end
  of lower('ViewCosts_jobe:ExcReplcamentCharge_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:ExcReplcamentCharge
      else
        do Value::jobe:ExcReplcamentCharge
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)

PreCopy  Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)

PreDelete       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.setsessionvalue('showtab_ViewCosts',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Hide:ARCCosts') <> 1
      If(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
        If (p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1)
          If p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:ARCIgnoreDefaultCharges',0)
            tmp:ARCIgnoreDefaultCharges = 0
          End
        End
      End
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
      If(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
        If (p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1))
          If p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:RRCIgnoreDefaultCharges',false)
            tmp:RRCIgnoreDefaultCharges = false
          End
        End
      End
  End
      If(p_web.GSV('Hide:ExchangeReplacement') = 1)
          If p_web.IfExistsValue('jobe:ExcReplcamentCharge') = 0
            p_web.SetValue('jobe:ExcReplcamentCharge',0)
            jobe:ExcReplcamentCharge = 0
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      if (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          loc:Alert = 'Error! Labour Cost is higher than Default Cost'
          loc:Invalid = 'tmp:RRCCost2'
          exit
      end
  end ! 'tmp:RRCViewCostType'
  do addToAudit
  Do DeleteSessionValues
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:InvalidTab += 1
  End
  ! tab = 2
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:InvalidTab += 1
      If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
        If tmp:RRCIgnoreReason = ''
          loc:Invalid = 'tmp:RRCIgnoreReason'
          loc:alert = p_web.translate('Enter Reason For Ignoring Standard Charges') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:RRCIgnoreReason = Upper(tmp:RRCIgnoreReason)
          p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
        If loc:Invalid <> '' then exit.
      End
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.StoreValue('tmp:ARCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:ARCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost1')
  p_web.StoreValue('tmp:AdjustmentCost1')
  p_web.StoreValue('tmp:ARCCost2')
  p_web.StoreValue('tmp:AdjustmentCost2')
  p_web.StoreValue('tmp:ARCCost3')
  p_web.StoreValue('tmp:AdjustmentCost3')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost4')
  p_web.StoreValue('tmp:AdjustmentCost4')
  p_web.StoreValue('tmp:ARCCost5')
  p_web.StoreValue('tmp:AdjustmentCost5')
  p_web.StoreValue('tmp:ARCCost6')
  p_web.StoreValue('tmp:AdjustmentCost6')
  p_web.StoreValue('tmp:ARCCost7')
  p_web.StoreValue('tmp:ARCCost8')
  p_web.StoreValue('tmp:ARCInvoiceNumber')
  p_web.StoreValue('tmp:RRCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:RRCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost0')
  p_web.StoreValue('tmp:RRCCost1')
  p_web.StoreValue('tmp:originalInvoice')
  p_web.StoreValue('tmp:RRCCost2')
  p_web.StoreValue('tmp:RRCCost3')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost4')
  p_web.StoreValue('tmp:RRCCost5')
  p_web.StoreValue('tmp:RRCCost6')
  p_web.StoreValue('tmp:RRCCost7')
  p_web.StoreValue('tmp:RRCCost8')
local.AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
    Code
        clear(tmpaud:Record)
        tmpaud:sessionID = p_web.SessionID
        tmpaud:field = fField
        get(TempAuditQueue,tmpaud:keyField)
        if (error())
            tmpaud:Reason = fReason
            if (fSaveCost = 1)
                tmpaud:SaveCost = 1
                tmpaud:Cost = fCost
            end !if (fSaveCost = 1)
            add(tempAuditQueue)
        else ! if (error())
            tmpaud:Reason = fReason
            put(tempAuditQueue)
        end ! if (error())
LoanAttachedToJob    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    p_web.SSV('LoanAttachedToJob',0)            
    if ((p_web.GSV('job:Courier_Cost') <> 0 or p_web.GSV('job:Courier_Cost_Estimate') <> 0 or |
            p_web.GSV('job:invoice_Courier_Cost') <> 0) and p_web.GSV('jobe:Engineer48HourOtion') <> 1)
        p_web.SSV('LoanAttachedToJob',1)
    else
        if (p_web.GSV('job:loan_unit_Number') <> 0)
            p_web.SSV('LoanAttachedToJob',1)
        else ! if (p_web.GSV('job:loan_unit_Number') <> 0)
            found# = 0
            Access:AUDIT.Clearkey(aud:typeActionKey)
            aud:ref_Number    = p_web.GSV('job:Ref_Number')
            aud:type    = 'LOA'
            aud:action    = 'LOAN UNIT ATTACHED TO JOB'
            set(aud:typeActionKey,aud:typeActionKey)
            loop
                if (Access:AUDIT.Next())
                    Break
                end ! if (Access:AUDIT.Next())
                if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                    Break
                end ! if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                if (aud:type    <> 'LOA')
                    Break
                end ! if (aud:type    <> 'LOA')
                if (aud:action    <> 'LOAN UNIT ATTACHED TO JOB')
                    Break
                end ! if (aud:action    <> 'LOAN UNIT ATTACHED TO JOB')
                p_web.SSV('LoanAttachedToJob',1)
                break
            end ! loop
        end ! if (p_web.GSV('job:loan_unit_Number') <> 0)
    end
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT.Close
     FilesOpened = False
  END
BrowseJobCredits     PROCEDURE  (NetWebServerWorker p_web)
tmp:CreditNumber     STRING(30)                            !
tmp:CreditType       STRING(4)                             !
tmp:Amount           REAL                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBSINV)
                      Project(jov:RecordNumber)
                      Project(jov:DateCreated)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseJobCredits')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobCredits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobCredits:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobCredits:FormName')
    else
      loc:FormName = 'BrowseJobCredits_frm'
    End
    p_web.SSV('BrowseJobCredits:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobCredits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobCredits:NoForm')
    loc:FormName = p_web.GSV('BrowseJobCredits:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobCredits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobCredits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSINV,jov:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TMP:CREDITNUMBER') then p_web.SetValue('BrowseJobCredits_sort','1')
    ElsIf (loc:vorder = 'TMP:CREDITTYPE') then p_web.SetValue('BrowseJobCredits_sort','2')
    ElsIf (loc:vorder = 'JOV:DATECREATED') then p_web.SetValue('BrowseJobCredits_sort','3')
    ElsIf (loc:vorder = 'TMP:AMOUNT') then p_web.SetValue('BrowseJobCredits_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobCredits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobCredits:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseJobCredits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobCredits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobCredits:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobCredits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobCredits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:CreditNumber','-tmp:CreditNumber')
    Loc:LocateField = 'tmp:CreditNumber'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:CreditType','-tmp:CreditType')
    Loc:LocateField = 'tmp:CreditType'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'jov:DateCreated','-jov:DateCreated')
    Loc:LocateField = 'jov:DateCreated'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:Amount','-tmp:Amount')
    Loc:LocateField = 'tmp:Amount'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tmp:CreditNumber')
    loc:SortHeader = p_web.Translate('Credit Number')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@s30')
  Of upper('tmp:CreditType')
    loc:SortHeader = p_web.Translate('Type')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@s4')
  Of upper('jov:DateCreated')
    loc:SortHeader = p_web.Translate('Date Created')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@d6')
  Of upper('tmp:Amount')
    loc:SortHeader = p_web.Translate('Amount')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@n-14.2')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseJobCredits:LookupFrom')
  End!Else
  loc:formaction = 'BrowseJobCredits'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobCredits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobCredits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobCredits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSINV"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jov:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseJobCredits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobCredits.locate(''Locator2BrowseJobCredits'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobCredits.cl(''BrowseJobCredits'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseJobCredits_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseJobCredits_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseJobCredits','Credit Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Credit Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseJobCredits','Type',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseJobCredits','Date Created','Click here to sort by Date Created',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Created')&'">'&p_web.Translate('Date Created')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseJobCredits','Amount',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Amount')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jov:DateCreated' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('jov:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBSINV{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'jov:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jov:RecordNumber'),p_web.GetValue('jov:RecordNumber'),p_web.GetSessionValue('jov:RecordNumber'))
      loc:FilterWas = 'jov:RefNumber = ' & p_web.GSV('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobCredits_Filter')
    p_web.SetSessionValue('BrowseJobCredits_FirstValue','')
    p_web.SetSessionValue('BrowseJobCredits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSINV,jov:RecordNumberKey,loc:PageRows,'BrowseJobCredits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSINV{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSINV,loc:firstvalue)
              Reset(ThisView,JOBSINV)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSINV{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSINV,loc:lastvalue)
            Reset(ThisView,JOBSINV)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (jov:Type = 'C')
          tmp:CreditType = 'Cred'
          tmp:CreditNumber = 'CN' & CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
          tmp:Amount = jov:CreditAmount * -1
      else ! if (job:Type = 'C')
          tmp:CreditType = 'Inv'
          tmp:CreditNumber = CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
          tmp:Amount = jov:NewTOtalCost
      end !if (job:Type = 'C')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jov:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobCredits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobCredits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobCredits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobCredits.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobCredits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobCredits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseJobCredits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobCredits.locate(''Locator1BrowseJobCredits'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobCredits.cl(''BrowseJobCredits'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseJobCredits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobCredits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobCredits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobCredits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobCredits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobCredits.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = jov:RecordNumber
    p_web._thisrow = p_web._nocolon('jov:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobCredits:LookupField')) = jov:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((jov:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseJobCredits.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSINV{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSINV)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSINV{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSINV)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','jov:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseJobCredits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','jov:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseJobCredits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:CreditNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:CreditType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jov:DateCreated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:Amount
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseJobCredits.omv(this);" onMouseOut="BrowseJobCredits.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseJobCredits=new browseTable(''BrowseJobCredits'','''&clip(loc:formname)&''','''&p_web._jsok('jov:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('jov:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseJobCredits.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobCredits.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobCredits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobCredits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobCredits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobCredits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSINV)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSINV)
  Bind(jov:Record)
  Clear(jov:Record)
  NetWebSetSessionPics(p_web,JOBSINV)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('jov:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::tmp:CreditNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:CreditNumber_'&jov:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:CreditNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:CreditType   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:CreditType_'&jov:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:CreditType,'@s4')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jov:DateCreated   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jov:DateCreated_'&jov:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jov:DateCreated,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:Amount   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:Amount_'&jov:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:Amount,'@n-14.2')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = jov:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('jov:RecordNumber',jov:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('jov:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('jov:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('jov:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
