

   MEMBER('WebServer_Phase2a.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE2A004.INC'),ONCE        !Local module procedure declarations
                     END


UseReplenishmentProcess PROCEDURE  (fModelNumber,fAccountNumber) ! Declare Procedure
MANUFACT::State  USHORT
MODELNUM::State  USHORT
TRADEACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles


    Return# = 0
    ! Inserting (DBH 21/02/2008) # 9717 - If model/manufacturer is used in Replenishment, then it can't be 48 Hour
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = fAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:IgnoreReplenishmentProcess = 0
            ! This account CAN use the replenishment process (DBH: 21/02/2008)
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number = fModelNumber
            If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                If mod:UseReplenishmentProcess = 1
                    Return# = 1
                Else ! If mod:UseReplenishmentProcess = 1
                    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        If man:UseReplenishmentProcess = 1
                            Return# = 1
                        End ! If man:UseReplenishmentProcess = 1
                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End ! If mod:UseReplenishmentProcess = 1
            End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        End ! If tra:IgnoreReplenishmentProcess = 0
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MODELNUM.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
Allow48Hour          PROCEDURE  (fIMEINumber,fModelNumber,fAccountNumber) ! Declare Procedure
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(fIMEINumber,1,6)
    esn:Model_Number = fModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(fIMEINumber,1,8)
    esn:Model_Number = fModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    ! Inserting (DBH 21/02/2008) # 9717 - If Man/Model is setup for replenishment, then don't use 48 Hour
    If UseReplenishmentProcess(fModelNumber,fAccountNumber) = 1
        Return# = 0
    End ! If UseReplenishmentProcess(f:ModelNumber,fAccountNumber) = 1
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#

SaveFiles  ROUTINE
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODAL.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Report
!!! Report the EXCHOR48 File
!!! </summary>
ExchangeOrder PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locRecordNumber      LONG                                  !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
address:Telephone_Number STRING(20)                        !
address:Fax_No       STRING(20)                            !
address:Email        STRING(50)                            !
tmp:DOP              DATE                                  !
Process:View         VIEW(EXCHOR48)
                       PROJECT(ex4:JobNumber)
                       PROJECT(ex4:Manufacturer)
                       PROJECT(ex4:ModelNumber)
                       PROJECT(ex4:RecordNumber)
                     END
ProgressWindow       WINDOW('Report EXCHOR48'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(385,2865,7521,7344),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(52,833),USE(address:Post_Code),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Date Printed:'),AT(4938,948),USE(?String16),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Email:'),AT(52,1302),USE(?String33),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s50),AT(365,1302),USE(address:Email),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(5677,938),USE(?ReportDateStamp),FONT('Tahoma',8,,FONT:bold), |
  TRN
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Fax:'),AT(52,1146),USE(?String34),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Tel:'),AT(52,990),USE(?String32),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,0),USE(Address:User_Name),FONT('Tahoma',14,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                       END
detail1                DETAIL,AT(,,,167),USE(?unnamed:4)
                         STRING(@s30),AT(104,0),USE(ex4:Manufacturer),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2135,0,,208),USE(ex4:ModelNumber),FONT('Tahoma',8),TRN
                         STRING('1'),AT(4427,0),USE(?String30),FONT(,8),TRN
                         STRING(@s15),AT(4844,0),USE(ex4:JobNumber),FONT(,8),TRN
                         STRING(@d17b),AT(6458,0),USE(tmp:DOP),FONT(,8),RIGHT,TRN
                       END
                       FOOTER,AT(396,10833,7521,302),USE(?unnamed:3)
                         LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Quantity Ordered:  1'),AT(104,104),USE(?String29),FONT('Tahoma',8,,FONT:bold, |
  CHARSET:ANSI),TRN
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         BOX,AT(104,2292,7344,7448),USE(?Box2),COLOR(COLOR:Black),ROUND
                         BOX,AT(104,1875,7344,365),USE(?Box1),COLOR(COLOR:Black),FILL(00C2E3F3h),ROUND
                         BOX,AT(4844,365,2552,1406),USE(?Box3),COLOR(COLOR:Black),ROUND
                         STRING('EXCHANGES ORDERED'),AT(4740,52),USE(?String3),FONT('Tahoma',16,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Quantity'),AT(4198,1979),USE(?String5),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Job Number'),AT(4875,1979),USE(?JobNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Activation Date'),AT(6458,1979),USE(?ActivationDate),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer'),AT(156,1979),USE(?String6),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Model Number'),AT(2167,1979),USE(?String31),FONT('Tahoma',8,,FONT:bold),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeOrder')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:EXCHOR48.Open                                     ! File EXCHOR48 used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = p_web.GSV('BookingAccount')
  IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
     !Found
     address:User_Name = tra:company_name
     address:Address_Line1 = tra:address_line1
     address:Address_Line2 = tra:address_line2
     address:Address_Line3 = tra:address_line3
     address:Post_code = tra:postcode
     address:Telephone_Number = tra:telephone_number
     address:Fax_No = tra:Fax_Number
     address:Email = tra:EmailAddress
  ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locRecordNumber = p_web.GSV('ExchangeOrder:RecordNumber')
  tmp:DOP = p_web.GSV('job:DOP')
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('ExchangeOrder',ProgressWindow)             ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:EXCHOR48, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ex4:RecordNumber)
  ThisReport.AddSortOrder(ex4:RecordNumberKey)
  ThisReport.AddRange(ex4:RecordNumber,locRecordNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:EXCHOR48.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHOR48.Close
    Relate:JOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('ExchangeOrder',ProgressWindow)          ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:detail1)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','ExchangeOrder','ExchangeOrder','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

Is48HourOrderCreated PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 0
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 0      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#

    
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
Is48HourOrderProcessed PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 1
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 1      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
IsUnitLiquidDamaged  PROCEDURE  (String f:ESN, Long f:JobNumber,<Byte ignoreARCCheck>) ! Declare Procedure
tmp:JobDate          DATE                                  !Job Date
  CODE
    Return vod.IsUnitLiquidDamage(f:ESN,f:JobNumber,ignoreARCCheck)
SelectManufacturers  PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MANUFACT)
                      Project(man:RecordNumber)
                      Project(man:Manufacturer)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('SelectManufacturers')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectManufacturers:NoForm')
      loc:NoForm = p_web.GetValue('SelectManufacturers:NoForm')
      loc:FormName = p_web.GetValue('SelectManufacturers:FormName')
    else
      loc:FormName = 'SelectManufacturers_frm'
    End
    p_web.SSV('SelectManufacturers:NoForm',loc:NoForm)
    p_web.SSV('SelectManufacturers:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectManufacturers:NoForm')
    loc:FormName = p_web.GSV('SelectManufacturers:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectManufacturers') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectManufacturers')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANUFACT,man:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MAN:MANUFACTURER') then p_web.SetValue('SelectManufacturers_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectManufacturers:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectManufacturers:LookupFrom','LookupFrom')
    p_web.StoreValue('SelectManufacturers:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectManufacturers:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectManufacturers:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectManufacturers_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectManufacturers_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(man:Manufacturer)','-UPPER(man:Manufacturer)')
    Loc:LocateField = 'man:Manufacturer'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(man:Manufacturer)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('man:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('SelectManufacturers_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('SelectManufacturers:LookupFrom')
  End!Else
  loc:formaction = 'SelectManufacturers'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectManufacturers:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectManufacturers:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectManufacturers:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANUFACT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="man:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Manufacturer') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Manufacturer',0)&'</span>'&CRLF
  End
  If clip('Select Manufacturer') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectManufacturers',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2SelectManufacturers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectManufacturers.locate(''Locator2SelectManufacturers'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectManufacturers.cl(''SelectManufacturers'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="SelectManufacturers_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="SelectManufacturers_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','SelectManufacturers','Manufacturer','Click here to sort by Manufacturer',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Manufacturer')&'">'&p_web.Translate('Manufacturer')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('man:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MANUFACT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'man:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('man:RecordNumber'),p_web.GetValue('man:RecordNumber'),p_web.GetSessionValue('man:RecordNumber'))
      loc:FilterWas = p_web.GetSessionValue('Filter:Manufacturer')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectManufacturers',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectManufacturers_Filter')
    p_web.SetSessionValue('SelectManufacturers_FirstValue','')
    p_web.SetSessionValue('SelectManufacturers_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANUFACT,man:RecordNumberKey,loc:PageRows,'SelectManufacturers',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANUFACT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANUFACT,loc:firstvalue)
              Reset(ThisView,MANUFACT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANUFACT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANUFACT,loc:lastvalue)
            Reset(ThisView,MANUFACT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(man:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectManufacturers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectManufacturers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectManufacturers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectManufacturers.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectManufacturers',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectManufacturers_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectManufacturers_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1SelectManufacturers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectManufacturers.locate(''Locator1SelectManufacturers'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectManufacturers.cl(''SelectManufacturers'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('SelectManufacturers_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectManufacturers_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectManufacturers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectManufacturers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectManufacturers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectManufacturers.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = man:RecordNumber
    p_web._thisrow = p_web._nocolon('man:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectManufacturers:LookupField')) = man:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((man:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="SelectManufacturers.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANUFACT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANUFACT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANUFACT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANUFACT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','man:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="SelectManufacturers.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','man:RecordNumber',clip(loc:field),,'checked',,,'onclick="SelectManufacturers.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::man:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="SelectManufacturers.omv(this);" onMouseOut="SelectManufacturers.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var SelectManufacturers=new browseTable(''SelectManufacturers'','''&clip(loc:formname)&''','''&p_web._jsok('man:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('man:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'SelectManufacturers.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'SelectManufacturers.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectManufacturers')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectManufacturers')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectManufacturers')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectManufacturers')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANUFACT)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANUFACT)
  Bind(man:Record)
  Clear(man:Record)
  NetWebSetSessionPics(p_web,MANUFACT)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('man:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&man:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectManufacturers',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectManufacturers',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectManufacturers',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::man:Manufacturer   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('man:Manufacturer_'&man:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(man:Manufacturer,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(man:Manufacturer_Key)
    loc:Invalid = 'man:Manufacturer'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Manufacturer_Key --> '&clip('Manufacturer')&' = ' & clip(man:Manufacturer)
  End
PushDefaultSelection  Routine
  loc:default = man:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('man:RecordNumber',man:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('man:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('man:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('man:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SelectModelNumbers   PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MODELNUM)
                      Project(mod:Model_Number)
                      Project(mod:Model_Number)
                      Project(mod:Manufacturer)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('SelectModelNumbers')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectModelNumbers:NoForm')
      loc:NoForm = p_web.GetValue('SelectModelNumbers:NoForm')
      loc:FormName = p_web.GetValue('SelectModelNumbers:FormName')
    else
      loc:FormName = 'SelectModelNumbers_frm'
    End
    p_web.SSV('SelectModelNumbers:NoForm',loc:NoForm)
    p_web.SSV('SelectModelNumbers:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectModelNumbers:NoForm')
    loc:FormName = p_web.GSV('SelectModelNumbers:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectModelNumbers') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectModelNumbers')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MODELNUM,mod:Model_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MOD:MODEL_NUMBER') then p_web.SetValue('SelectModelNumbers_sort','1')
    ElsIf (loc:vorder = 'MOD:MANUFACTURER') then p_web.SetValue('SelectModelNumbers_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectModelNumbers:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectModelNumbers:LookupFrom','LookupFrom')
    p_web.StoreValue('SelectModelNumbers:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectModelNumbers:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectModelNumbers:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectModelNumbers_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectModelNumbers_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mod:Model_Number)','-UPPER(mod:Model_Number)')
    Loc:LocateField = 'mod:Model_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mod:Manufacturer)','-UPPER(mod:Manufacturer)')
    Loc:LocateField = 'mod:Manufacturer'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mod:Manufacturer),+UPPER(mod:Model_Number)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mod:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('SelectModelNumbers_LocatorPic','@s30')
  Of upper('mod:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('SelectModelNumbers_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('SelectModelNumbers:LookupFrom')
  End!Else
  loc:formaction = 'SelectModelNumbers'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectModelNumbers:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectModelNumbers:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectModelNumbers:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MODELNUM"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mod:Model_Number_Key"></input><13,10>'
  end
  If p_web.Translate('Select Model Number') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Model Number',0)&'</span>'&CRLF
  End
  If clip('Select Model Number') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectModelNumbers',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2SelectModelNumbers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectModelNumbers.locate(''Locator2SelectModelNumbers'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectModelNumbers.cl(''SelectModelNumbers'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="SelectModelNumbers_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="SelectModelNumbers_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','SelectModelNumbers','Model Number','Click here to sort by Model Number',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Model Number')&'">'&p_web.Translate('Model Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','SelectModelNumbers','Manufacturer','Click here to sort by Manufacturer',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Manufacturer')&'">'&p_web.Translate('Manufacturer')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mod:model_number',lower(Thisview{prop:order}),1,1) = 0 !and MODELNUM{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mod:Model_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mod:Model_Number'),p_web.GetValue('mod:Model_Number'),p_web.GetSessionValue('mod:Model_Number'))
      loc:FilterWas = p_web.GSV('filter:ModelNumber') & ' AND UPPER(mod:Active) = <39>Y<39>'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectModelNumbers',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectModelNumbers_Filter')
    p_web.SetSessionValue('SelectModelNumbers_FirstValue','')
    p_web.SetSessionValue('SelectModelNumbers_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MODELNUM,mod:Model_Number_Key,loc:PageRows,'SelectModelNumbers',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MODELNUM{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MODELNUM,loc:firstvalue)
              Reset(ThisView,MODELNUM)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MODELNUM{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MODELNUM,loc:lastvalue)
            Reset(ThisView,MODELNUM)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mod:Model_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectModelNumbers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectModelNumbers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectModelNumbers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectModelNumbers.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectModelNumbers',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectModelNumbers_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectModelNumbers_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1SelectModelNumbers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectModelNumbers.locate(''Locator1SelectModelNumbers'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectModelNumbers.cl(''SelectModelNumbers'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('SelectModelNumbers_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectModelNumbers_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectModelNumbers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectModelNumbers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectModelNumbers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectModelNumbers.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mod:Model_Number
    p_web._thisrow = p_web._nocolon('mod:Model_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectModelNumbers:LookupField')) = mod:Model_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mod:Model_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="SelectModelNumbers.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MODELNUM{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MODELNUM)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MODELNUM{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MODELNUM)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mod:Model_Number',clip(loc:field),,loc:checked,,,'onclick="SelectModelNumbers.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mod:Model_Number',clip(loc:field),,'checked',,,'onclick="SelectModelNumbers.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mod:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mod:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="SelectModelNumbers.omv(this);" onMouseOut="SelectModelNumbers.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var SelectModelNumbers=new browseTable(''SelectModelNumbers'','''&clip(loc:formname)&''','''&p_web._jsok('mod:Model_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mod:Model_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'SelectModelNumbers.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'SelectModelNumbers.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectModelNumbers')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectModelNumbers')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectModelNumbers')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectModelNumbers')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MODELNUM)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MODELNUM)
  Bind(mod:Record)
  Clear(mod:Record)
  NetWebSetSessionPics(p_web,MODELNUM)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mod:Model_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&mod:Model_Number,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectModelNumbers',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectModelNumbers',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectModelNumbers',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mod:Model_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mod:Model_Number_'&mod:Model_Number,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mod:Model_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mod:Manufacturer   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mod:Manufacturer_'&mod:Model_Number,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mod:Manufacturer,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mod:Model_Number_Key)
    loc:Invalid = 'mod:Model_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> '&clip('Model Number')&' = ' & clip(mod:Model_Number)
  End
PushDefaultSelection  Routine
  loc:default = mod:Model_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mod:Model_Number',mod:Model_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mod:Model_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mod:Model_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mod:Model_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
JobInUse             PROCEDURE  (fSessionValue)            ! Declare Procedure
  CODE
    rtnValue# = 0
    pointer# = Pointer(JOBS)
    Hold(JOBS,1)
    Get(JOBS,pointer#)
    if (errorcode()  = 43)
        rtnValue# = 1
    else
        relate:JOBSLOCK.open()
        
        ! First step. Remove any blanks from the file
        Access:JOBSLOCK.ClearKey(lock:JobNumberKey)
        lock:JobNumber = 0
        SET(lock:JobNumberKey,lock:JobNumberKey)
        LOOP
            IF (Access:JOBSLOCK.Next())
                BREAK
            END
            IF (lock:JobNumber <> 0)
                BREAK
            END
            Access:JOBSLOCK.DeleteRecord(0)
        END
        
        access:JOBSLOCK.clearkey(lock:jobNumberKey)
        lock:jobNUmber = job:ref_number
        if (access:JOBSLOCK.tryfetch(lock:jobNumberKey) = level:benign)
            if (lock:SessionValue <> fSessionValue)
                if (TODAY() = lock:DateLocked and clock() < (lock:timelocked + 60000)) ! 10 Minutes Time Out
                ! If it's the same user then there's no need to give the locked error?!
                    rtnValue# = 1
                end
                
            end
        end
        relate:JOBSLOCK.close()
        

    end ! if (errorcode()  = 43)
    return rtnValue#
jobInvoiced          PROCEDURE  (fRefNumber,fSite)         ! Declare Procedure
returnValue          BYTE                                  !
JOBS_ALIAS::State  USHORT
INVOICE::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles

    returnValue = 0
    Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number    = fRefNumber
    if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
        ! Found
        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        if (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            ! Found
            if (fSite = 'RRC')
                if (inv:Invoice_Number > 0 and inv:RRCInvoiceDate > 0)
                    returnValue = 1
                end ! if (inv:Invoice_Number > 0 and inv:RRCInvoiceDate > 0)
            else ! if fSite = 'RRC'
                if (inv:Invoice_Number > 0 and inv:ARCInvoiceDate > 0)
                    returnValue = 1
                end ! if (inv:Invoice_Number > 0 and inv:ARCInvoiceDate > 0)
            end ! if fSite = 'RRC'
        else ! if (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
    else ! if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)

    do restoreFiles
    do closeFiles

    return returnValue
SaveFiles  ROUTINE
  JOBS_ALIAS::State = Access:JOBS_ALIAS.SaveFile()         ! Save File referenced in 'Other Files' so need to inform its FileManager
  INVOICE::State = Access:INVOICE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBS_ALIAS::State <> 0
    Access:JOBS_ALIAS.RestoreFile(JOBS_ALIAS::State)       ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF INVOICE::State <> 0
    Access:INVOICE.RestoreFile(INVOICE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS_ALIAS.Open                                   ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.UseFile                                ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS_ALIAS.Close
     Access:INVOICE.Close
     FilesOpened = False
  END
