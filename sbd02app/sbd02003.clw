

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02003.INC'),ONCE        !Local module procedure declarations
                     END


QA_Failed_Label_Exchange_9X PROCEDURE                 !Generated from procedure template - Report

RejectRecord         LONG,AUTO
qa_reason_temp       STRING(255)
save_aud_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
engineer_temp        STRING(60)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(EXCHANGE_ALIAS)
                       PROJECT(xch_ali:ESN)
                       PROJECT(xch_ali:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(125,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,3000,2000),USE(?Label)
                           STRING(@s30),AT(365,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('FAILED QA (Exchange)'),AT(750,208),USE(?String8),TRN,FONT(,10,,FONT:bold)
                           STRING('Date:'),AT(104,417),USE(?String3),TRN,FONT(,7,,)
                           STRING('xx/xx/xxxx'),AT(885,417),USE(?ReportDateStamp),TRN,FONT(,7,,FONT:bold)
                           STRING('Unit Details:'),AT(104,573),USE(?String3:2),TRN,FONT(,7,,)
                           STRING(@s255),AT(885,573,2083,156),USE(unit_details_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('hh:mm'),AT(1458,417),USE(?ReportTimeStamp),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. / E.S.N.'),AT(104,781),USE(?String5),TRN,FONT(,7,,)
                           STRING(@s30),AT(885,781),USE(xch_ali:ESN),FONT(,7,,FONT:bold)
                           STRING('Inspector:'),AT(104,1146),USE(?String5:4),TRN,FONT(,7,,)
                           STRING(@s60),AT(885,1146),USE(tmp:PrintedBy),FONT(,7,,FONT:bold)
                           STRING('Failure Reason:'),AT(104,1406),USE(?String5:5),TRN,FONT(,7,,)
                           TEXT,AT(885,1406,2031,365),USE(glo:Notes_Global),FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('QA_Failed_Label_Exchange_9X')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE_ALIAS.Open
  Access:USERS.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:EXCHANGE_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, xch_ali:Ref_Number)
  ThisReport.AddSortOrder(xch_ali:Ref_Number_Key)
  ThisReport.AddRange(xch_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:EXCHANGE_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE_ALIAS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL - QA'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  setcursor(cursor:wait)
  save_aud_id = access:audit.savefile()
  access:audit.clearkey(aud:action_key)
  aud:ref_number = glo:select1
  aud:action     = 'QA REJECTION'
  set(aud:action_key,aud:action_key)
  loop
      !omemo(audit)
      if access:audit.next()
         break
      end !if
      if aud:ref_number <> glo:select1      |
      or aud:action     <> 'QA REJECTION'      |
          then break.  ! end if
      Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
      aud2:AUDRecordNumber = aud:Record_Number
      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
          qa_reason_temp = aud2:notes
          Break
      END ! IF
  end !loop
  access:audit.restorefile(save_aud_id)
  setcursor()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportTimeStamp{PROP:Text}=FORMAT(CLOCK(),@T7)
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  unit_details_temp = Clip(xch_ali:model_number) & ' ' & Clip(xch_ali:manufacturer)
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

QA_Failed_Label_Loan_9X PROCEDURE                     !Generated from procedure template - Report

RejectRecord         LONG,AUTO
qa_reason_temp       STRING(255)
save_aud_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
engineer_temp        STRING(60)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(LOAN_ALIAS)
                       PROJECT(loa_ali:ESN)
                       PROJECT(loa_ali:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(125,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(10,,3000,2000),USE(?Label)
                           STRING(@s30),AT(365,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('FAILED QA (Loan)'),AT(917,208),USE(?String8),TRN,FONT(,10,,FONT:bold)
                           STRING('Date:'),AT(104,417),USE(?String3),TRN,FONT(,7,,)
                           STRING('xx/xx/xxxx'),AT(885,417),USE(?ReportDateStamp),TRN,FONT(,7,,FONT:bold)
                           STRING('Unit Details:'),AT(104,573),USE(?String3:2),TRN,FONT(,7,,)
                           STRING(@s255),AT(885,573,2083,156),USE(unit_details_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('hh:mm'),AT(1458,417),USE(?ReportTimeStamp),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. / E.S.N.'),AT(104,781),USE(?String5),TRN,FONT(,7,,)
                           STRING(@s30),AT(885,781),USE(loa_ali:ESN),FONT(,7,,FONT:bold)
                           STRING('Inspector:'),AT(104,1146),USE(?String5:4),TRN,FONT(,7,,)
                           STRING(@s60),AT(885,1146),USE(tmp:PrintedBy),FONT(,7,,FONT:bold)
                           STRING('Failure Reason:'),AT(104,1406),USE(?String5:5),TRN,FONT(,7,,)
                           TEXT,AT(885,1406,2031,365),USE(glo:Notes_Global),FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('QA_Failed_Label_Loan_9X')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:LOAN_ALIAS.Open
  Access:USERS.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:LOAN_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, loa_ali:Ref_Number)
  ThisReport.AddSortOrder(loa_ali:Ref_Number_Key)
  ThisReport.AddRange(loa_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:LOAN_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:LOAN_ALIAS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL - QA'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  setcursor(cursor:wait)
  save_aud_id = access:audit.savefile()
  access:audit.clearkey(aud:action_key)
  aud:ref_number = glo:select1
  aud:action     = 'QA REJECTION'
  set(aud:action_key,aud:action_key)
  loop
      !omemo(audit)
      if access:audit.next()
         break
      end !if
      if aud:ref_number <> glo:select1      |
      or aud:action     <> 'QA REJECTION'      |
          then break.  ! end if
      Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
      aud2:AUDRecordNumber = aud:Record_Number
      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
          qa_reason_temp = aud2:notes
          Break
      END ! IF
  end !loop
  access:audit.restorefile(save_aud_id)
  setcursor()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportTimeStamp{PROP:Text}=FORMAT(CLOCK(),@T7)
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  unit_details_temp = Clip(loa_ali:model_number) & ' ' & Clip(loa_ali:manufacturer)
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Job_Retained_Accessories_Label_9X PROCEDURE           !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Company_Name)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(125,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,3000,2000),USE(?Label)
                           STRING(@s30),AT(375,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('RETAINED ACCESSORIES'),AT(667,208),USE(?String14),TRN,CENTER,FONT(,,,FONT:bold)
                           STRING('Job No:'),AT(1406,469),USE(?String7),TRN,FONT(,8,,)
                           STRING(@s10),AT(1875,625),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Customer:'),AT(52,781),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s60),AT(729,781,2396,156),USE(customer_name_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Company:'),AT(52,938),USE(?String20),TRN,FONT(,7,,)
                           STRING(@s30),AT(729,938),USE(job_ali:Company_Name),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Accessories:'),AT(52,1146),USE(?String21:2),TRN,FONT(,7,,)
                           TEXT,AT(729,1146,2240,573),USE(tmp:accessories),FONT(,7,,FONT:bold)
                           STRING(@d6b),AT(625,469),USE(job_ali:date_booked),FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(52,469),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s10),AT(1875,469,1156,156),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Job_Retained_Accessories_Label_9X')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBACC.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  
  acc_count# = 0
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job_ali:ref_number  |
          then break.  ! end if
      acc_count# += 1
      If acc_count# = 1
          tmp:accessories = Clip(jac:accessory)
      Else!If acc_count# = 1
          tmp:accessories = Clip(tmp:accessories) & ', ' & Clip(jac:accessory)
      End!If acc_count# = 1
  end !loop
  access:jobacc.restorefile(save_jac_id)
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Thermal_Labels_Loan_9X PROCEDURE                      !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
Bar_Code3_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Company_Name)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(100,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,3000,2000),USE(?Label)
                           STRING(@s30),AT(365,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('Job No:'),AT(1396,240),USE(?String7),TRN,FONT(,8,,)
                           STRING(@s10),AT(1844,396),USE(job_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Customer:'),AT(83,604),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s60),AT(604,604,2323,156),USE(customer_name_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Company:'),AT(83,760),USE(?String25),TRN,FONT(,7,,)
                           STRING(@s30),AT(604,760),USE(job_ali:Company_Name),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Job Type:'),AT(83,917),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s30),AT(604,917),USE(job_type_temp),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1323,1250,1677,240),USE(Bar_Code3_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Customer'),AT(83,1521),USE(?String23),TRN,FONT(,7,,)
                           STRING(@d6b),AT(604,240),USE(job_ali:date_booked),FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(83,240),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s10),AT(1844,240,1156,156),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Loan'),AT(83,1083),USE(?String15),TRN,FONT(,7,,)
                           STRING(@s30),AT(604,1083,677,156),USE(loa:Model_Number),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(604,1240,677,156),USE(loa:Manufacturer),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(604,1521,677,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('E.S.N.:'),AT(1323,1521),USE(?String18),TRN,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(1760,1521),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Unit:'),AT(83,1198),USE(?String19:2),TRN,FONT(,7,,)
                           STRING('Unit:'),AT(83,1635),USE(?String19),TRN,FONT(,7,,)
                           STRING(@s16),AT(1323,1677,1677,240),USE(Bar_Code2_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('E.S.N.:'),AT(1323,1083),USE(?String18:2),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1760,1083),USE(loa:ESN,,?loa:ESN:2),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(604,1677,677,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Thermal_Labels_Loan_9X')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBS_ALIAS.Open
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS_ALIAS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Unit Exchange Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  
  If def:PrintBarcode = 1
      Settarget(Report)
      ?Bar_Code2_Temp{prop:hide} = 0
      ?Bar_Code3_Temp{prop:hide} = 0
      Settarget()
  End!If def:PrintBarcode = 1
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'L' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  access:loan.clearkey(loa:ref_number_key)
  loa:ref_number = glo:select2
  access:loan.fetch(loa:ref_number_key)
  bar_code_string_temp = Clip(loa:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code3_Temp)
  
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Thermal_Labels_Loan_B452 PROCEDURE                    !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
Bar_Code3_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Company_Name)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(100,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,3000,2000),USE(?Label)
                           STRING(@s30),AT(365,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('Job No:'),AT(1396,240),USE(?String7),TRN,FONT(,8,,)
                           STRING(@s10),AT(1844,396),USE(job_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Customer:'),AT(83,604),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s60),AT(604,604,2323,156),USE(customer_name_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Company:'),AT(83,760),USE(?String25),TRN,FONT(,7,,)
                           STRING(@s30),AT(604,760),USE(job_ali:Company_Name),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Job Type:'),AT(83,917),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s30),AT(604,917),USE(job_type_temp),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1323,1240,1677,240),USE(Bar_Code3_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Customer'),AT(83,1521),USE(?String23),TRN,FONT(,7,,)
                           STRING(@d6b),AT(604,240),USE(job_ali:date_booked),FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(83,240),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s10),AT(1844,240,1156,156),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Exchange '),AT(83,1083),USE(?String15),TRN,FONT(,7,,)
                           STRING(@s30),AT(604,1083,677,156),USE(loa:Model_Number),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(604,1240,677,156),USE(loa:Manufacturer),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(604,1521,677,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('E.S.N.:'),AT(1323,1521),USE(?String18),TRN,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(1760,1521),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Unit:'),AT(83,1198),USE(?String19:2),TRN,FONT(,7,,)
                           STRING('Unit:'),AT(83,1635),USE(?String19),TRN,FONT(,7,,)
                           STRING(@s16),AT(1323,1677,1677,240),USE(Bar_Code2_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('E.S.N.:'),AT(1323,1083),USE(?String18:2),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1760,1083),USE(loa:ESN,,?loa:ESN:2),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(604,1677,677,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Thermal_Labels_Loan_B452')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBS_ALIAS.Open
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS_ALIAS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Unit Exchange Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'E' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  access:loan.clearkey(loa:ref_number_key)
  loa:ref_number = glo:select2
  access:loan.fetch(loa:ref_number_key)
  bar_code_string_temp = Clip(loa:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code3_Temp)
  
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Thermal_Labels_NT PROCEDURE (f_type)                  !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
save_job2_id         USHORT,AUTO
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(21)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
tmp:type             STRING(4)
tmp:UserName         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Company          STRING(40)
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Location)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(0,0,5000,2000),PAPER(PAPER:USER,5000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,4677,2000)
                           STRING(@s25),AT(646,21),USE(def:User_Name),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING(@s10),AT(2708,52),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@t1),AT(1229,271),USE(job_ali:time_booked),FONT(,7,,FONT:bold)
                           STRING('Contact:'),AT(646,385),USE(?Contact),TRN,FONT(,7,,)
                           STRING(@s20),AT(1219,385,1198,104),USE(customer_name_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Company:'),AT(646,490),USE(?String20),TRN,FONT(,7,,)
                           STRING(@s40),AT(1219,490,1781,156),USE(tmp:Company),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Job Type:'),AT(646,594),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s21),AT(1219,594),USE(job_type_temp),TRN,FONT(,7,,FONT:bold)
                           STRING(@s4),AT(3042,542,469,260),USE(tmp:type),TRN,HIDE,FONT(,12,,FONT:bold)
                           STRING('Location:'),AT(646,1344),USE(?Location),TRN,FONT(,7,,)
                           STRING(@s20),AT(1219,1354),USE(job_ali:Location),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1219,1240),USE(tmp:UserName),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Accessories:'),AT(646,979),USE(?String21:2),TRN,HIDE,FONT(,7,,)
                           TEXT,AT(1219,979,2240,260),USE(tmp:accessories),HIDE,FONT(,7,,FONT:bold)
                           STRING('Retained'),AT(646,1083),USE(?Retained),TRN,HIDE,FONT(,7,,)
                           STRING('Cust Unit'),AT(646,1469),USE(?String19),TRN,FONT(,7,,)
                           STRING('Fault:'),AT(646,708),USE(?String14),TRN,FONT(,7,,)
                           TEXT,AT(1219,708,2240,260),USE(jbn_ali:Fault_Description),FONT(,7,,FONT:bold)
                           STRING(@d6b),AT(1219,177),USE(job_ali:date_booked),TRN,FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(646,229),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s2),AT(1958,271),USE(jobe:SkillLevel),TRN,FONT(,7,,FONT:bold)
                           STRING('Skill Level'),AT(1813,167),USE(?String8:2),TRN,FONT(,7,,)
                           STRING(@s8),AT(2323,208,1198,260),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                           STRING(@s30),AT(1219,1458,677,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('IMEI: '),AT(2323,1354),USE(?String18),TRN,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(2552,1354),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(1219,1563,2344,208),USE(Bar_Code2_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',16,,)
                           STRING(@s30),AT(1906,1458,677,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Thermal_Labels_NT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:TRANTYPE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:TRANTYPE.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  
  ReturnValue = PARENT.OpenReport()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      SetTarget(Report)
      ?job_ali:Location{prop:Hide} = 1
      ?Location{prop:Hide} = 1
      SetTarget()
  End !def:HideLocation
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  Settarget(Report)
  If def:PrintBarcode = 1
      ?Bar_Code2_Temp{prop:hide} = 0
  Else
      ?job_ali:ESN{prop:Hide} = 1
      ?String18{prop:Hide} = 1
      ?Bar_Code2_Temp{prop:Hide} = 1
  End!If def:PrintBarcode = 1
  IF def:Job_Label_Accessories = 'YES'
    Settarget(Report)
    ?String21:2{Prop:Hide} = FALSE
    ?retained{Prop:Hide} = FALSE
    ?tmp:accessories{Prop:Hide} = FALSE
    Settarget()
  END
  Settarget()
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job_ali:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname)
  Else!If job_ali:surname <> ''
      customer_name_temp = ''
      SetTarget(Report)
      ?Contact{Prop:Hide} = 1
      SetTarget()
  End!
  
  tmp:Company = Clip(job_ali:Account_Number) & ' - ' & Clip(job_ali:Company_Name)
  
  acc_count# = 0
  Save_jac_ID = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:DamagedKey)
  jac:Ref_Number  = job_ali:Ref_Number
  jac:Damaged   = 0
  Set(jac:DamagedKey,jac:DamagedKey)
  Loop
      If Access:JOBACC.NEXT()
         Break
      End !If
      If jac:Ref_Number   <> job_ali:Ref_Number      |
          Then Break.  ! End If
      acc_count# += 1
      If jac:Damaged
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End !If jac:Damaged
      If acc_count# = 1
          tmp:accessories = Clip(jac:accessory)
      Else!If acc_count# = 1
          tmp:accessories = Clip(tmp:accessories) & ', ' & Clip(jac:accessory)
      End!If acc_count# = 1
  end !loop
  access:jobacc.restorefile(save_jac_id)
  
  refurb# = 0
  
  access:jobnotes_alias.clearkey(JBN_ALI:RefNumberKey)
  JBN_ALI:RefNumber   = job_ali:ref_number
  access:jobnotes_alias.tryfetch(JBN_ALI:RefNumberKey)
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          If tra:refurbcharge = 'YES'
              If job_ali:exchange_unit_number <> ''
                  refurb# = 1
              End!If job_ali:exchange_unit_number <> ''
              If refurb# = 0
                  access:trantype.clearkey(trt:transit_type_key)
                  trt:transit_type = job_ali:transit_type
                  if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                      If trt:exchange_unit = 'YES'
                          refurb# = 1
                      End!If trt:exchange = 'YES'
                  end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              End!If refurb# = 0
              If refurb# = 0
                  If Sub(job_ali:exchange_status,1,3) = '108'
                      refurb# = 1
                  End!If Sub(job_ali:current_status,1,3) = '108'
              End!If refurb# = 0
          End!If tra:refurbcharge = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
  
  
  If refurb# = 1
      tmp:username    = 'REFURBISHMENT REQUIRED'
  Else!If refurb# = 1
      tmp:username    = ''
      If f_type <> ''
          Settarget(report)
          Unhide(?tmp:type)
          tmp:type = f_type
          Settarget()
      Else
          If job_ali:esn <> 'N/A'
              If CountBouncer()
                  Settarget(report)
                  Unhide(?tmp:type)
                  tmp:type = 'SE'
                  Settarget()
              End!If found# = 1
          End!If job2:esn <> 'N/A'
      End
  End!If refurb# = 1
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Thermal_Labels       PROCEDURE  (f_type)              ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    Access:Defaults.Open()
    Access:Defaults.UseFile()

    SET(Defaults,0)
    IF Access:Defaults.Next()
      ! Error!
    ELSE
      IF def:UseSmallLabel = 1
        If tmp:High = 0
            Thermal_Labels_Small_NT(f_type)
        Else!If tmp:High = 0
            Thermal_Labels_Small_9X(f_type)
        End!If tmp:High = 0
      ELSE
        If tmp:High = 0
            Thermal_Labels_NT(f_type)
        Else!If tmp:High = 0
            Thermal_Labels_9X(f_type)
        End!If tmp:High = 0
      END
    END

    Access:Defaults.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Stock_Label_NT PROCEDURE (f_date,f_quantity,f_order_number,f_despatch_number,f_sale_cost) !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(STOCK_ALIAS)
                       PROJECT(sto_ali:Description)
                       PROJECT(sto_ali:Location)
                       PROJECT(sto_ali:Part_Number)
                       PROJECT(sto_ali:Ref_Number)
                       PROJECT(sto_ali:Second_Location)
                       PROJECT(sto_ali:Shelf_Location)
                       PROJECT(sto_ali:Supplier)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(0,0,5000,2000),PAPER(PAPER:USER,5000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,5000,2000),USE(?Label)
                           STRING(@s30),AT(906,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('Part Number:'),AT(646,469),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s30),AT(1427,469,1604,156),USE(sto_ali:Part_Number),FONT(,7,,FONT:bold)
                           STRING('Description:'),AT(646,625),USE(?String20),TRN,FONT(,7,,)
                           STRING(@s30),AT(1427,625),USE(sto_ali:Description),FONT(,7,,FONT:bold)
                           STRING('Supplier:'),AT(646,781),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s30),AT(1427,781),USE(sto_ali:Supplier),FONT(,7,,FONT:bold)
                           STRING('Location:'),AT(646,938),USE(?String21),TRN,FONT(,7,,)
                           STRING(@s30),AT(1427,938),USE(sto_ali:Location),FONT(,7,,FONT:bold)
                           STRING('Order Number:'),AT(646,1406),USE(?String19),TRN,FONT(,7,,)
                           STRING('Qty Recvd.:'),AT(2156,260),USE(?String14),TRN,FONT(,7,,)
                           STRING(@s8),AT(2729,260),USE(f_quantity),TRN,LEFT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING(@d6b),AT(1427,260),USE(f_date),FONT(,7,,FONT:bold)
                           STRING('Date Recvd.:'),AT(646,260),USE(?String8),TRN,FONT(,7,,)
                           STRING('Despatch No.:'),AT(646,1563),USE(?String15),TRN,FONT(,7,,)
                           STRING(@s30),AT(1427,1094,1510,156),USE(sto_ali:Shelf_Location),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1427,1250,1510,156),USE(sto_ali:Second_Location),FONT(,7,,FONT:bold)
                           STRING('Second Location:'),AT(646,1250),USE(?String18:2),TRN,FONT(,7,,)
                           STRING('Shelf Location:'),AT(646,1094),USE(?String18),TRN,FONT(,7,,)
                           STRING(@s30),AT(1427,1563),USE(f_despatch_number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1427,1406),USE(f_order_number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Sale Cost:'),AT(646,1719),USE(?String22),TRN,FONT(,7,,)
                           STRING(@n14.2),AT(1427,1719,677,156),USE(f_sale_cost),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Label_NT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBS_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:STOCK_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sto_ali:Ref_Number)
  ThisReport.AddSortOrder(sto_ali:Ref_Number_Key)
  ThisReport.AddRange(sto_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:STOCK_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS_ALIAS.Close
    Relate:STOCK_ALIAS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'STOCK LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

Stock_Label          PROCEDURE  (f_date,f_quantity,f_order_number,f_despatch_number,f_sale_cost) ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    If tmp:High = 0
        Stock_Label_NT(f_date,f_quantity,f_order_number,f_despatch_number,f_sale_cost)
    Else!If tmp:High = 0
        Stock_Label_9X(f_date,f_quantity,f_order_number,f_despatch_number,f_sale_cost)
    End!If tmp:High = 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Thermal_Labels_Exchange_NT PROCEDURE                  !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
Bar_Code3_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Company_Name)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(0,0,5000,2000),PAPER(PAPER:USER,5000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,5000,2000),USE(?Label)
                           STRING(@s30),AT(927,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('Job No:'),AT(1958,240),USE(?String7),TRN,FONT(,8,,)
                           STRING(@s10),AT(2406,396),USE(job_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Customer:'),AT(646,604),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s60),AT(1167,604,2323,156),USE(customer_name_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Company:'),AT(646,760),USE(?String25),TRN,FONT(,7,,)
                           STRING(@s30),AT(1167,760),USE(job_ali:Company_Name),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Job Type:'),AT(646,917),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s30),AT(1167,917),USE(job_type_temp),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1885,1240,1677,240),USE(Bar_Code3_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Customer'),AT(646,1521),USE(?String23),TRN,FONT(,7,,)
                           STRING(@d6b),AT(1167,240),USE(job_ali:date_booked),FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(646,240),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s10),AT(2406,240,1156,156),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Exchange '),AT(646,1083),USE(?String15),TRN,FONT(,7,,)
                           STRING(@s30),AT(1167,1083,677,156),USE(xch:Model_Number),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1167,1240,677,156),USE(xch:Manufacturer),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1167,1521,677,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('E.S.N.:'),AT(1885,1521),USE(?String18),TRN,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(2323,1521),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Unit:'),AT(646,1198),USE(?String19:2),TRN,FONT(,7,,)
                           STRING('Unit:'),AT(646,1635),USE(?String19),TRN,FONT(,7,,)
                           STRING(@s16),AT(1885,1677,1677,240),USE(Bar_Code2_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('E.S.N.:'),AT(1885,1083),USE(?String18:2),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(2323,1083),USE(xch:ESN,,?XCH:ESN:2),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(1167,1677,677,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(REPORT)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Thermal_Labels_Exchange_NT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS_ALIAS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Unit Exchange Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        REPORT{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  set(defaults)
  access:defaults.next()
  
  If def:PrintBarcode = 1
      Settarget(Report)
      ?Bar_Code2_Temp{prop:hide} = 0
      ?Bar_Code3_Temp{prop:hide} = 0
      Settarget()
  End!If def:PrintBarcode = 1
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'E' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  access:exchange.clearkey(xch:ref_number_key)
  xch:ref_number = glo:select2
  access:exchange.fetch(xch:ref_number_key)
  bar_code_string_temp = Clip(xch:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code3_Temp)
  
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

