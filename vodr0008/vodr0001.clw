

   MEMBER('vodr0008.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

EngineerPerformanceReport PROCEDURE                   !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!-----------------------------------------------
    MAP
DateToString        PROCEDURE( DATE ), STRING
Days_Between_Routine PROCEDURE  (DATE, DATE), LONG ! Declare Procedure
DaysBetween          PROCEDURE  (DATE, DATE), LONG
HoursBetween                PROCEDURE  ( LONG, LONG ), LONG
GetHeadAccount PROCEDURE( STRING )
GetDeliveryDateAtARC        PROCEDURE( LONG ), LONG ! BOOL
GetEngineer                 PROCEDURE( STRING )
GetRepairType   PROCEDURE( STRING ), STRING
!GetWebJobNumber PROCEDURE( LONG ), STRING
LoadJOBSE               PROCEDURE( LONG ), LONG ! BOOL
LoadREPTYDEF        PROCEDURE( STRING ), LONG ! BOOL
LoadSUBTRACC                PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC                    PROCEDURE( STRING ), LONG ! BOOL
LoadUSERS           PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
WorkingDaysBetween          PROCEDURE  (DATE, DATE), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME),LONG
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug  PROCEDURE( STRING )
    END !MAP
!-----------------------------------------------
tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Local_Group          GROUP,PRE(LOC)
SendToARC            BYTE
Spares_Req           BYTE
Estimate_Flag        BYTE
Third_Party_Flag     BYTE
engineer_used        STRING(5)
ARCDateBooked        DATE
ARCLocation          STRING(30)
AccountNumber        STRING(15)
Account_Name         STRING(100)
ApplicationName      STRING('ServiceBase')
Category             STRING(30)
CommentText          STRING(100)
Courier              STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
Different            STRING(3)
HeadAccountNumber    STRING(15)
JobsExtendedInSync   BYTE
MaxTabNumber         BYTE(2)
Path                 STRING(255)
Spares_Date          DATE
ProgramName          STRING(100)
RepairCentreType     STRING(3)
RepairTypeCategory   STRING(30)
SectionName          STRING(100)
TabNumber            BYTE(1)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
Version              STRING('3.1.0000')
date_held            DATE
Exchange_Date        DATE
WorkingHours         LONG
                     END
Misc_Group           GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
OLD:RepairTypeCategory STRING(30)
OPTION1              SHORT
Progress:Text        STRING(100)
RecordCount          LONG
Result               BYTE
WebJOB_OK            BYTE
                     END
DASTAG_Group         GROUP,PRE()
AccountChange        BYTE
AccountCount         LONG
AccountTag           STRING(1)
AccountValue         STRING(15)
                     END
RepairTypeCategoryQueue QUEUE,PRE(cq)
RepairType           STRING(30)
Category             STRING(30)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(100)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(2)
HeadLastCol          STRING('S {1}')
DataLastCol          STRING('V')
HeadSummaryRow       LONG(23)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
UserResultsQueue     QUEUE,PRE(uq)
Level0               REAL
spares_req           REAL
Level1               REAL
Level2               REAL
Level25              REAL
Level3               REAL
UserCode             STRING(3)
Forename             STRING(30)
Surname              STRING(30)
SiteLocation         STRING(30)
SkillLevel           LONG
JobsCompleted        LONG
JobCount             LONG
Jobs_BER             LONG
Jobs_Exchange        LONG
Jobs_Excluded        LONG
Jobs_LiquidDamage    LONG
Jobs_Modification    LONG
Jobs_NFF             LONG
Jobs_RNR             LONG
Jobs_RTM             LONG
Jobs_Repair          LONG
JobsEstimateRejected LONG
JobsLevel            LONG,DIM(10)
third_party_count    LONG
Software_Upgrade     LONG
Estimate             LONG
scrap                LONG
Network_Unlock       LONG
Phone_Unlock         LONG
QA                   REAL
JobsAllocated        LONG
RepairBefore3rdParty LONG
After3rdPartyTest    LONG
Weight               LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
JobsBooked           LONG
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
JobsBooked           LONG
                     END
tmp:rep_type_war     STRING(30)
tmp:rep_type_char    STRING(30)
Automatic            BYTE
BRW4::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:BranchIdentification)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:BranchIdentification LIKE(tra:BranchIdentification) !Browse hot field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Engineer Performance Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           CHECK('Show Excel'),AT(432,140,56,12),USE(excel:Visible),TRN,LEFT,FONT('Arial',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           BUTTON('&Rev tags'),AT(284,193,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           STRING('Completion Date'),AT(249,94,84,12),USE(?String4),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D06b),AT(341,94,64,10),USE(LOC:StartDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest complete date to show on report'),REQ
                           BUTTON,AT(409,90),USE(?StartDateCalendar),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(292,209,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON,AT(496,206),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,241),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,278),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           STRING('Completion End Date '),AT(249,114,84,12),USE(?String5),TRN,HIDE,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D06b),AT(341,114,64,10),USE(LOC:EndDate),IMM,HIDE,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest complete date to show on report'),REQ
                           BUTTON,AT(409,110),USE(?EndDateCalendar),TRN,FLAT,HIDE,LEFT,ICON('lookupp.jpg')
                           CHECK('All Repair Centres'),AT(192,142),USE(DoAll),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           LIST,AT(192,156,296,182),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Tab 2'),USE(?Tab2)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
CommandLine STRING(255)
tmpPos      LONG
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::5:QUEUE = glo:Queue2
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue2)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
OKButton_Pressed                ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        !debug:Active = True

        !-----------------------------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !LOC:Version = '3.1.0001'
        !-----------------------------------------------------------------



        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        !- LOCATION ------------------------------------------------------
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! DespatchToCustomer     = DESPATCHED
        !
        !- STATUS --------------------------------------------------------
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        !LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        !LOC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !LOC:DespatchToCustomer  = GETINI('RRC',  'DespatchToCustomer',          'DESPATCHED', '.\SB2KDEF.INI') ! [RRC]DespatchToCustomer=DESPATCHED
        !-----------------------------------------------------------------


        LOC:EndDate = LOC:StartDate
        DO ExportSetup

        DO InitColumns

        If Automatic = True
            !Get account criteria for automated ini  (DBH: 06-05-2004)
            Free(glo:Queue2)
            Set(tra_ali:Account_Number_Key)
            Loop
                If Access:TRADEACC_ALIAS.NEXT()
                   Break
                End !If
                If tra_ali:BranchIdentification = ''
                    Cycle
                End !If tra:BranchIdentification = ''

                If GETINI('ENGINEERPERFORMANCE',tra_ali:Account_Number,,CLIP(Path()) & '\REPAUTO.INI') = 1
                    glo:Pointer2 = tra_ali:RecordNumber
                    Add(glo:Queue2)
                End !If GETINI('TAT','tra:Account_Number',,CLIP(Path()) & '\REPAUTO.INI') = 1
            End !Loop

        End !If Automatic = True
        !-----------------------------------------------------------------
        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)

        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.

            IF tra_ali:Account_Number = LocalHeadAccount THEN
                LOC:RepairCentreType = 'ARC'
            ELSIF tra_ali:RemoteRepairCentre = 0 THEN
                CYCLE
            ELSE
                LOC:RepairCentreType = 'RRC'
            END !IF

            IF DoAll <> 'Y' THEN
                glo:Queue2.Pointer2 = tra_ali:RecordNumber
                GET(glo:Queue2,glo:Queue2.Pointer2)
                IF ERROR() THEN
                    CYCLE
                END !IF
            END !IF

            IF Automatic = TRUE
              IF tra_ali:EmailAddress = ''
                CYCLE
              END
            END

            DO GetFileName

            DO ExportBody
            IF CancelPressed
                EXIT
            END !IF
        END !LOOP
        !-----------------------------------------------------------------
        DO ExportFinalize
        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
!        IF CLIP(LOC:FileName) = ''
!            DO LoadFileDialog
!        END !IF
!
!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            SETCURSOR()
!
!            EXIT
!        END !IF LOC:FileName = ''
!
!        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
!            LOC:FileName = CLIP(LOC:FileName) & '.xls'
!        END !IF
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        DO XL_Setup
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        DO XL_AddWorkbook
            !-------------------------------------------------------------
            FREE(HeadAccount_Queue)
            CLEAR(HeadAccount_Queue)

            FREE(SubAccount_Queue)
            CLEAR(SubAccount_Queue)

            FREE(RepairTypeCategoryQueue)
            CLEAR(RepairTypeCategoryQueue)

            !FREE(HeaderQueue)
            !CLEAR(HeaderQueue)

            DO FillUserResultsQueue

            DO CreateTitleSheet
            DO CreateDataSheet
            DO WriteHeadSummary
            !-------------------------------------------------------------
        IF CancelPressed <> True
            !-------------------------------------------------------------
            Excel{'Sheets("Sheet3").Select'}
            Excel{'ActiveWindow.SelectedSheets.Delete'}

            Excel{'Sheets("Summary").Select'}
            Excel{'Range("A1").Select'}

            Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
            Excel{'Application.ActiveWorkBook.Close()'}
            !-------------------------------------------------------------
        END !IF
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            SETCURSOR()

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_Finalize
        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
CurrentRow LONG(9)
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = CLIP(sheet:HeadLastCol) ! 'Y'

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetLandscape

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                               = 20
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'}                               = 20
        Excel{'ActiveSheet.Columns("C:' & CLIP(sheet:HeadLastCol) & '").ColumnWidth'} = 10
!        !-----------------------------------------------------------------
!        ! Totals
!        !
!        Excel{'Range("A9:' & CLIP(sheet:HeadLastCol) & '9").Select'}
!            DO XL_SetTitle
!            DO XL_SetBorder
!
!        Excel{'Range("A9").Select'}
!            DO XL_SetBold
!            Excel{'ActiveCell.Formula'} = 'Totals'
!        !-----------------------------------------------------------------
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'B.E.R.'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'Exchange'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'Excluded'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'Liquid Damage'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'Modification'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'N.F.F.'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'R.N.R.'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'R.T.M.'
!
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                Excel{'ActiveCell.Formula'} = 'Repair'
!        !-----------------------------------------------------------------
!        CurrentRow += 1
!            Excel{'Range("A' & CurrentRow  & ':' & CLIP(sheet:HeadLastCol) & CurrentRow  & '").Select'}
!                DO XL_SetTitle
!                DO XL_SetBorder
!
!            Excel{'Range("A' & CurrentRow  & '").Select'}
!                DO XL_SetBold
!                Excel{'ActiveCell.Formula'} = 'Total Jobs'
!        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        CurrentRow           += 2

        Excel{'Range("A' & CurrentRow  & ':' & CLIP(sheet:HeadLastCol) & CurrentRow  & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow  & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Summary'
            DO XL_SetBold
        !-----------------------------------------------------------------
        CurrentRow           += 1
        sheet:HeadSummaryRow  = CurrentRow

        Excel{'Range("A' & CurrentRow  & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet

        LOC:SectionName = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        LOC:CommentText = ''
        LOC:Text        = LEFT(CLIP(LOC:ProgramName), 30)
        DO CreateSectionHeader
        !------------------------------------------
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
!        Access:JOBS.ClearKey(job:DateCompletedKey)
!        job:Date_Completed = LOC:StartDate
!        SET(job:DateCompletedKey, job:DateCompletedKey)
!        !------------------------------------------
!        Progress:Text    = LOC:SectionName
!        RecordsToProcess = (LOC:EndDate - LOC:StartDate) * 1000 ! RECORDS(JOBS)
!
!        DO ProgressBar_LoopPre
!        !-----------------------------------------------------------------
!        LOOP UNTIL Access:JOBS.Next()
!            !-------------------------------------------------------------
!            DO ProgressBar_Loop
!
!            IF CancelPressed
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            IF job:Date_Completed > LOC:EndDate
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!!            Access:AudStats.ClearKey(aus:NewStatusKey)
!!            aus:RefNumber = job:ref_number
!!            aus:Type = 'JOB'
!!            aus:NewStatus = '605 QA CHECK REQUIRED'
!!            IF Access:AudStats.Fetch(aus:NewStatusKey)
!!              !Error!
!!              !Job has not been QA'd!
!!            END
!!            IF ~INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
!!              CYCLE
!!            END
!            DO WriteColumns
!            !-------------------------------------------------------------
!        END !LOOP
!
!        DO ProgressBar_LoopPost
!        IF CancelPressed
!            EXIT
!        END !IF
        !-----------------------------------------------------------------
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
        !Get incomplete jobs now and check if they are in QA or not within date range!
        Access:Webjob.Clearkey(wob:DateCompletedKey)
        IF LOC:RepairCentreType = 'ARC'
          wob:HeadAccountNumber = ''
        ELSE
          wob:HeadAccountNumber = tra_ali:Account_Number
        END
        wob:DateCompleted = 0
        SET(wob:DateCompletedKey,wob:DateCompletedKey)
        !Access:JOBS.ClearKey(job:DateCompletedKey)
        !job:Date_Completed = 0
        !SET(job:DateCompletedKey, job:DateCompletedKey)
        !------------------------------------------
        Progress:Text    = LOC:SectionName
        IF LOC:RepairCentreType <> 'ARC'
          RecordsToProcess = (LOC:EndDate - LOC:StartDate) * 1000 ! RECORDS(JOBS)
        ELSE
          RecordsToProcess = RECORDS(WEBJOB)
        END
        !-----------------------------------------------------------------
        !LOOP UNTIL Access:JOBS.Next()
        LOOP UNTIL Access:WebJob.Next()
            !-------------------------------------------------------------
            IF LOC:RepairCentreType <> 'ARC'
              IF wob:HeadAccountNumber <> tra_ali:Account_Number
                BREAK
              END
              !Only count incomplete jobs - 4323 (DBH: 07-06-2004)
!              If wob:DateCompleted <> 0
!                Break
!              End !If wob:DateCompleted <> 0
             Else !IF LOC:RepairCentreType <> 'ARC'
                !Only count incomplete jobs - 4323 (DBH: 07-06-2004)
!                If wob:DateCompleted <> 0
!                    Cycle
!                End !If wob:DateCompleted <> 0
            END
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF

            !-------------------------------------------------------------
            !IF job:Date_Completed > LOC:EndDate
            !    BREAK
           ! END !IF
            !-------------------------------------------------------------

            LOC:Spares_Req = FALSE
            LOC:Spares_Date = ''
            !Check for spares Req`
            loop_break# = 0
            Access:AudStats.ClearKey(aus:StatusDateKey)
            aus:RefNumber = wob:RefNumber
            aus:DateChanged = loc:startdate
            SET(aus:StatusDateKey,aus:StatusDateKey)
            LOOP
              IF Access:AudStats.Next()
                BREAK
              END
              IF aus:RefNumber <> wob:RefNumber
                BREAK
              END
              IF aus:DateChanged <> loc:startdate
                BREAK
              END
              loop_break# = 1
              BREAK
            END
            IF loop_break# = 0
              CYCLE
            END

            Access:Jobs.Clearkey(job:Ref_Number_Key)
            job:Ref_Number = wob:RefNumber
            IF Access:Jobs.Fetch(job:Ref_Number_Key)
              !Error
              CYCLE
            END

            Access:AudStats.ClearKey(aus:NewStatusKey)
            aus:RefNumber = job:ref_number
            aus:Type = 'JOB'
            aus:NewStatus = '330 SPARES REQUESTED'
            SET(aus:NewStatusKey,aus:NewStatusKey)
            LOOP
              IF Access:AudStats.Next()
                BREAK
              END
              IF aus:RefNumber <> job:ref_number
                BREAK
              END
              IF aus:Type <> 'JOB'
                BREAK
              END
              IF aus:NewStatus <> '330 SPARES REQUESTED'
                BREAK
              END
              IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                LOC:spares_req = TRUE
                LOC:Spares_Date = aus:DateChanged
                BREAK
              END
            END

!            !Check for Jobs Allocated -  (DBH: 19-08-2003)
!            Access:AudStats.ClearKey(aus:NewStatusKey)
!            aus:RefNumber = job:ref_number
!            aus:Type = 'JOB'
!            aus:NewStatus = '315 IN REPAIR'
!            SET(aus:NewStatusKey,aus:NewStatusKey)
!            LOOP
!              IF Access:AudStats.Next()
!                BREAK
!              END
!              IF aus:RefNumber <> job:ref_number
!                BREAK
!              END
!              IF aus:Type <> 'JOB'
!                BREAK
!              END
!              IF aus:NewStatus <> '315 IN REPAIR'
!                BREAK
!              END
!              IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
!
!                BREAK
!              END
!            END


            IF LOC:RepairCentreType = 'ARC'
              no_fail# = 0
              loc:date_held = 0
              LOC:Estimate_Flag = FALSE
              LOC:Third_Party_Flag = FALSE
              LOC:SendToARC = FALSE
              Access:AudStats.ClearKey(aus:NewStatusKey)
              aus:RefNumber = job:ref_number
              aus:Type = 'JOB'
              aus:NewStatus = '605 QA CHECK REQUIRED'
              SET(aus:NewStatusKey,aus:NewStatusKey)
              LOOP
                IF Access:AudStats.Next()
                  BREAK
                END
                IF aus:RefNumber <> job:ref_number
                  BREAK
                END
                IF aus:Type <> 'JOB'
                  BREAK
                END
                IF aus:NewStatus <> '605 QA CHECK REQUIRED'
                  BREAK
                END
                IF aus:OldStatus <> '315 IN REPAIR'
                  CYCLE
                END
                IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                  No_Fail# = 1
                  loc:date_held = aus:DateChanged
                  BREAK
                END
              END
              IF no_fail# = 0
                  Access:AudStats.ClearKey(aus:NewStatusKey)
                  aus:RefNumber = job:ref_number
                  aus:Type = 'JOB'
                  aus:NewStatus = '405 SEND TO 3RD PARTY'
                  SET(aus:NewStatusKey,aus:NewStatusKey)
                  LOOP
                    IF Access:AudStats.Next()
                      BREAK
                    END
                    IF aus:RefNumber <> job:ref_number
                      BREAK
                    END
                    IF aus:Type <> 'JOB'
                      BREAK
                    END
                    IF aus:NewStatus <> '405 SEND TO 3RD PARTY'
                      BREAK
                    END
                    IF aus:OldStatus <> '315 IN REPAIR'
                      CYCLE
                    END
                    IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                      No_Fail# = 1
                      loc:date_held = aus:DateChanged
                      LOC:Third_Party_Flag = TRUE
                      BREAK
                    END
                  END
              END

              IF no_fail# = 0
                  Access:AudStats.ClearKey(aus:NewStatusKey)
                  aus:RefNumber = job:ref_number
                  aus:Type = 'JOB'
                  aus:NewStatus = '510 ESTIMATE READY'
                  SET(aus:NewStatusKey,aus:NewStatusKey)
                  LOOP
                    IF Access:AudStats.Next()
                      BREAK
                    END
                    IF aus:RefNumber <> job:ref_number
                      BREAK
                    END
                    IF aus:Type <> 'JOB'
                      BREAK
                    END
                    IF aus:NewStatus <> '510 ESTIMATE READY' 
                      BREAK
                    END
                    IF aus:OldStatus <> '505 ESTIMATE REQUIRED'
                        IF aus:OldStatus <> '315 IN REPAIR'
                          CYCLE
                        END
                    END
                    IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                      No_Fail# = 1
                      loc:date_held = aus:DateChanged
                      LOC:Estimate_Flag = TRUE
                      BREAK
                    END
                  END
              END
            ELSE !IF LOC:RepairCentreType = 'ARC'
              no_fail# = 0
              loc:date_held = 0
              LOC:Estimate_Flag = FALSE
              LOC:Third_Party_Flag = FALSE
              LOC:SendToARC = FALSE
              Access:AudStats.ClearKey(aus:NewStatusKey)
              aus:RefNumber = job:ref_number
              aus:Type = 'JOB'
              aus:NewStatus = '605 QA CHECK REQUIRED'
              SET(aus:NewStatusKey,aus:NewStatusKey)
              LOOP
                IF Access:AudStats.Next()
                  BREAK
                END
                IF aus:RefNumber <> job:ref_number
                  BREAK
                END
                IF aus:Type <> 'JOB'
                  BREAK
                END
                IF aus:NewStatus <> '605 QA CHECK REQUIRED'
                  BREAK
                END
                IF aus:OldStatus <> '310 ALLOCATED TO ENGINEER'
                  CYCLE
                END
                IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                  No_Fail# = 1
                  loc:date_held = aus:DateChanged
                  BREAK
                END
              END
              IF no_fail# = 0
                  Access:AudStats.ClearKey(aus:NewStatusKey)
                  aus:RefNumber = job:ref_number
                  aus:Type = 'JOB'
                  aus:NewStatus = '405 SEND TO 3RD PARTY'
                  SET(aus:NewStatusKey,aus:NewStatusKey)
                  LOOP
                    IF Access:AudStats.Next()
                      BREAK
                    END
                    IF aus:RefNumber <> job:ref_number
                      BREAK
                    END
                    IF aus:Type <> 'JOB'
                      BREAK
                    END
                    IF aus:NewStatus <> '405 SEND TO 3RD PARTY'
                      BREAK
                    END
                    IF aus:OldStatus <> '310 ALLOCATED TO ENGINEER'
                      CYCLE
                    END
                    IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                      No_Fail# = 1
                      loc:date_held = aus:DateChanged
                      LOC:Third_Party_Flag = TRUE
                      BREAK
                    END
                  END
              END

              IF no_fail# = 0
                  Access:AudStats.ClearKey(aus:NewStatusKey)
                  aus:RefNumber = job:ref_number
                  aus:Type = 'JOB'
                  aus:NewStatus = '510 ESTIMATE READY'
                  SET(aus:NewStatusKey,aus:NewStatusKey)
                  LOOP
                    IF Access:AudStats.Next()
                      BREAK
                    END
                    IF aus:RefNumber <> job:ref_number
                      BREAK
                    END
                    IF aus:Type <> 'JOB'
                      BREAK
                    END
                    IF aus:NewStatus <> '510 ESTIMATE READY' 
                      BREAK
                    END
                    IF aus:OldStatus <> '310 ALLOCATED TO ENGINEER' 
                      IF aus:OldStatus <> '505 ESTIMATE REQUIRED'
                          IF aus:OldStatus <> '315 IN REPAIR'
                            CYCLE
                          END
                      END
                    END
                    IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                      No_Fail# = 1
                      loc:date_held = aus:DateChanged
                      LOC:Estimate_Flag = TRUE
                      BREAK
                    END
                  END
              END

              IF no_fail# = 0
                  Access:AudStats.ClearKey(aus:NewStatusKey)
                  aus:RefNumber = job:ref_number
                  aus:Type = 'JOB'
                  aus:NewStatus = '450 SEND TO ARC'
                  SET(aus:NewStatusKey,aus:NewStatusKey)
                  LOOP
                    IF Access:AudStats.Next()
                      BREAK
                    END
                    IF aus:RefNumber <> job:ref_number
                      BREAK
                    END
                    IF aus:Type <> 'JOB'
                      BREAK
                    END
                    IF aus:NewStatus <> '450 SEND TO ARC'
                      BREAK
                    END
                    !IF aus:OldStatus <> '505 ESTIMATE REQUIRED'
                    !    IF aus:OldStatus <> '450 SEND TO ARC'
                    !      CYCLE
                    !    END
                    !END
                    IF INRANGE(aus:DateChanged,loc:startdate, Loc:enddate)
                      No_Fail# = 1
                      loc:date_held = aus:DateChanged
                      LOC:SendToArc = TRUE
                      BREAK
                    END
                  END
              END
            END
            IF LOC:Spares_Req = FALSE
              IF No_Fail# = 0
                CYCLE
              END
            END
            IF LOC:Spares_Req = FALSE
              IF ~INRANGE(loc:date_held,loc:startdate, Loc:enddate)
                CYCLE
              END
            ELSE
              IF ~INRANGE(LOC:Spares_Date,loc:startdate, Loc:enddate)
                CYCLE
              END
            END !IF LOC:RepairCentreType = 'ARC'
            DO WriteColumns2
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------


    EXIT
!-----------------------------------------------
CreateSectionHeader                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------       
        LOC:Text          = CLIP(LOC:ProgramName)
        sheet:TempLastCol = CLIP(sheet:DataLastCol) ! 'S'

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & CLIP(sheet:TempLastCol) & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            Excel{'Range("B' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & CLIP(sheet:TempLastCol) & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = LEFT(CLIP(LOC:Text),30)
        !-----------------------------------------------       
        Excel{'Range("A1:' & CLIP(sheet:TempLastCol) & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:TempLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBold
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'

        Excel{'Range("B' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Font.Size'}        = 8
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = Clip(tmp:VersionNumber)
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Complete From:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(LOC:StartDate)
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Complete To:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(LOC:EndDate)
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}     = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'} = LOC:UserName
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A4:' & CLIP(sheet:TempLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Franchise Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Booked'
            head:ColumnWidth  = 14.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Completed'
            head:ColumnWidth  = 14.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer Surname'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer Forename'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer User Code'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'QA User'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer Skill Level'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Job Skill Level'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Chargeable Charge Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Chargeable Repair Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Warranty Charge Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Warranty Repair Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = '3rd Party Repairer'
            head:ColumnWidth  = 14.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Estimate Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Make'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Model Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Unit Type'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'MSN'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'Summary Column'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange Date'
            head:ColumnWidth  = 14.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Spares Requested'
            head:ColumnWidth  = 14.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Original Repair Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)


        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
InitColumns2                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'Engineer Surname'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer Forename'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Jobs Allocated'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Jobs Completed'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Exchange'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Attempt To Repair Before 3rd Party'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'After 3rd Party Test'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Spares Requested'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'N.F.F.'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'RNR/Liquid Damage/BER/Scrap'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'R.T.M.'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Level 0'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Level 1'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Level 2'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Level 2.5'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Level 3'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Software Upgrade'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Estimates'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Unlock'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Accessory Exchange'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'QA'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Weight'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer Hours'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Weight Per Hour'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}

        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns(JOB="' & job:Ref_Number & '")')
        !-----------------------------------------------
        !DO UpdateOtherEngineersWhoHaveworkedOnJob
        !-----------------------------------------------
        IF     LOC:RepairCentreType = 'RRC'
            !-------------------------------------------
            IF NOT LoadWEBJOB(job:Ref_Number)
                ! ERROR

                EXIT
            END !IF

            IF NOT wob:HeadAccountNumber = tra_ali:Account_Number
                EXIT
            END !IF
            !-------------------------------------------
            IF GetDeliveryDateAtARC(job:Ref_Number)
                ! RRC -> ARC  <====================(1)
                !
                !Message('RRC?????')
                EXIT
            END !IF
            !-------------------------------------------
            ! RRC -> RRC
            !-------------------------------------------
            DO WriteColumns_RRC
            !-------------------------------------------
        ELSE
            !-------------------------------------------
            ! LOC:RepairCentreType = 'ARC'
            !-------------------------------------------
            IF NOT LoadWEBJOB(job:Ref_Number)
                ! ERROR

                EXIT
            END !IF
            !-------------------------------------------
            IF wob:HeadAccountNumber = LocalHeadAccount
                !---------------------------------------
                ! ARC -> ARC
                !---------------------------------------
                ! 22 Oct 2002 John
                ! If the job was booked at the ARC, the RRC date received should be blank.
                ! The date displayed here is the booking date for the job not the date it was received at the RRC.
                ! In fact none of these jobs were booked at the RRC therefore this column [RRC Date Received-john] should be blank.
                !
                DO WriteColumns_RRC
                !---------------------------------------
            ELSIF GetDeliveryDateAtARC(job:Ref_Number) = TRUE
                !---------------------------------------
                ! RRC -> ARC  <====================(1)
                !---------------------------------------
                !OC:ARCDateBooked   = set by GetDeliveryDateAtARC()

                DO WriteColumns_RRC
                !---------------------------------------
            ELSE
                !---------------------------------------
                ! RRC -> RRC Unreachable code
                !---------------------------------------
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
    EXIT
WriteColumns2                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns(JOB="' & job:Ref_Number & '")')
        !-----------------------------------------------
        !DO UpdateOtherEngineersWhoHaveworkedOnJob
        !-----------------------------------------------
        IF     LOC:RepairCentreType = 'RRC'
            !-------------------------------------------
            !IF NOT LoadWEBJOB(job:Ref_Number)
                ! ERROR

            !    EXIT
            !END !IF

            IF NOT wob:HeadAccountNumber = tra_ali:Account_Number
                EXIT
            END !IF
            !-------------------------------------------
            IF GetDeliveryDateAtARC(job:Ref_Number)
                ! RRC -> ARC  <====================(1)
                !
                !Message('RRC?????')
                !EXIT
            END !IF
            !-------------------------------------------
            ! RRC -> RRC
            !-------------------------------------------
            DO WriteColumns_RRC2
            !-------------------------------------------
        ELSE
            !-------------------------------------------
            ! LOC:RepairCentreType = 'ARC'
            !-------------------------------------------
            !IF NOT LoadWEBJOB(job:Ref_Number)
                ! ERROR

            !    EXIT
            !END !IF
            !-------------------------------------------
            IF wob:HeadAccountNumber = LocalHeadAccount
                !---------------------------------------
                ! ARC -> ARC
                !---------------------------------------
                ! 22 Oct 2002 John
                ! If the job was booked at the ARC, the RRC date received should be blank.
                ! The date displayed here is the booking date for the job not the date it was received at the RRC.
                ! In fact none of these jobs were booked at the RRC therefore this column [RRC Date Received-john] should be blank.
                !
                DO WriteColumns_RRC2
                !---------------------------------------
            ELSIF GetDeliveryDateAtARC(job:Ref_Number) = TRUE
                !---------------------------------------
                ! RRC -> ARC  <====================(1)
                !---------------------------------------
                !OC:ARCDateBooked   = set by GetDeliveryDateAtARC()

                DO WriteColumns_RRC2
                !---------------------------------------
            ELSE
                !---------------------------------------
                ! RRC -> RRC Unreachable code
                !---------------------------------------
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
    EXIT
WriteColumns_RRC                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        GetHeadAccount(wob:HeadAccountNumber)
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') START')
        LOC:JobsExtendedInSync = LoadJOBSE(job:Ref_Number)
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') 1')
        LOC:UserCode = job:Engineer

        GetEngineer(job:Engineer)
            DO GetUserDetailsFromQueue_Tabulations
        PUT(UserResultsQueue, +uq:UserCode)

        !While still have the job!

        !QA?!
!        Access:Audit.ClearKey(aud:Action_Key)
!        Aud:Ref_Number = Job:Ref_Number
!        Aud:Action = 'RAPID QA UPDATE: MANUAL QA PASSED'
!        SET(aud:Action_Key,aud:Action_Key)
!        IF Access:Audit.Next()
!          !Error!
!        END
!        IF Aud:Action <> 'RAPID QA UPDATE: MANUAL QA PASSED' !Not QA'd
!          !
!        ELSE
!          GetEngineer(aud:user)
!          uq:QA+=1
!          PUT(UserResultsQueue)
!        END
!        GetEngineer(job:Engineer)
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') 2')
        RecordCount += 1
            DO WriteColumn_JobNumber             ! Job Number
            DO WriteColumn_FranchiseBranchNumber ! Franchise Branch Number
            WriteColumn( wob:JobNumber         ) ! Franchise Job Number
            DO WriteColumn_DateBooked            ! Date Booked
            DO WriteColumn_DateCompleted         ! Date Completed

            DO WriteColumn_UserSurname           ! Engineer Surname
            DO WriteColumn_UserForename          ! Engineer Forename
            DO WriteColumn_Engineer              ! Engineer User Code

            !Neil!
            DO WriteColumn_Engineer_QA

            DO WriteColumn_EngineerSkillLevel    ! Eng. Skill Level
            DO WriteColumn_JobSkillLevel         ! Job Skill Level

            DO WriteColumn_ChargeableChargeType  ! Chargeable Charge Type
            DO WriteColumn_ChargeableRepairType  ! Chargeable Repair Type

            DO WriteColumn_WarrantyChargeType    ! Warranty Charge Type
            DO WriteColumn_WarrantyRepairType    ! Warranty Repair Type

            DO WriteColumn_3rdPartyRepairer      ! 3rd Party Repairer
            DO WriteColumn_EstimateStatus        ! Estimate Status

            DO WriteColumn_Manufacturer          ! Make
            DO WriteColumn_ModelNumber           ! Model
            DO WriteColumn_UnitType              ! Unit Type
            DO WriteColumn_ESN                   ! IMEI
            DO WriteColumn_MSN                   ! MSN

            DO WriteColumn_ColumnType            ! 16 May 2002 John
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF

        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') END')
        !-----------------------------------------------
    EXIT
WriteColumns_RRC2                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        GetHeadAccount(wob:HeadAccountNumber)
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') START')
        LOC:JobsExtendedInSync = LoadJOBSE(job:Ref_Number)
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') 1')


        IF LOC:Spares_Req = TRUE
          LOC:date_held = LOC:Spares_Date
        END

        count_act# = 0
        Access:JobsEng.ClearKey(joe:JobNumberKey)
        joe:JobNumber = job:ref_number
        SET(joe:JobNumberKey,joe:JobNumberKey)
        LOOP
          IF Access:JobsEng.Next()
            BREAK
          END
          IF joe:JobNumber <> job:ref_number
            BREAK
          END
          count_act# +=1
        END


        !Figure Out which User is on the job!
        count# = 0
        Access:JobsEng.ClearKey(joe:JobNumberKey)
        joe:JobNumber = job:ref_number
        joe:DateAllocated = TODAY()
        SET(joe:JobNumberKey,joe:JobNumberKey)
        LOOP
          IF Access:JobsEng.Next()
            BREAK
          END
          IF joe:JobNumber <> job:ref_number
            BREAK
          END
          IF joe:DateAllocated > LOC:date_held
            CYCLE
          END
          count# +=1
          IF joe:DateAllocated = LOC:date_held
            loc:engineer_used = joe:UserCode
            BREAK
          END
          loc:engineer_used = joe:UserCode
          BREAK
        END

!        IF job:ref_number = 10318
!          MESSAGE(joe:JobNumber)
!        END

        !Workaround for bug!
        IF count# = 1 AND loc:engineer_used = ''
          !ONly one user, use actual!
          loc:engineer_used = job:engineer
        ELSIF count_act# = 1 AND loc:engineer_used <> job:engineer
          Access:JobsEng.ClearKey(joe:JobNumberKey)
          joe:JobNumber = job:ref_number
          SET(joe:JobNumberKey,joe:JobNumberKey)
          Access:JobsEng.Next()
          loc:engineer_used = joe:UserCode
        END

        LOC:UserCode = loc:engineer_used

        GetEngineer(loc:engineer_used)
            DO GetUserDetailsFromQueue_Tabulations
        PUT(UserResultsQueue, +uq:UserCode)

        !While still have the job!

        aud:user = 'N/A'
        IF LOC:RepairCentreType = 'RRC'
          IF NOT uq:SiteLocation = tra_ali:SiteLocation
            EXIT
          END
        END

        IF LOC:Spares_Req = TRUE
          IF NOT uq:SiteLocation = tra_ali:SiteLocation
            EXIT
          END
        END


        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') 2')
        RecordCount += 1
            DO WriteColumn_JobNumber             ! Job Number
            DO WriteColumn_FranchiseBranchNumber ! Franchise Branch Number
            WriteColumn( wob:JobNumber         ) ! Franchise Job Number
            DO WriteColumn_DateBooked            ! Date Booked
            DO WriteColumn_DateCompleted         ! Date Completed

            DO WriteColumn_UserSurname           ! Engineer Surname
            DO WriteColumn_UserForename          ! Engineer Forename
            DO WriteColumn_Engineer              ! Engineer User Code

            !Neil!
            DO WriteColumn_Engineer_QA

            DO WriteColumn_EngineerSkillLevel    ! Eng. Skill Level
            DO WriteColumn_JobSkillLevel         ! Job Skill Level

            DO WriteColumn_ChargeableChargeType  ! Chargeable Charge Type
            DO WriteColumn_ChargeableRepairType  ! Chargeable Repair Type

            DO WriteColumn_WarrantyChargeType    ! Warranty Charge Type
            DO WriteColumn_WarrantyRepairType    ! Warranty Repair Type

            DO WriteColumn_3rdPartyRepairer      ! 3rd Party Repairer
            DO WriteColumn_EstimateStatus        ! Estimate Status

            DO WriteColumn_Manufacturer          ! Make
            DO WriteColumn_ModelNumber           ! Model
            DO WriteColumn_UnitType              ! Unit Type
            DO WriteColumn_ESN                   ! IMEI
            DO WriteColumn_MSN                   ! MSN

            DO WriteColumn_ColumnType
            DO WriteColumn_ExchangeDate
            DO WriteColumn_SparesReq
            DO WriteColumn_Different
            !IF job:Exchange_Unit_Number > 0         ! 16 May 2002 John
            !  DO WriteColumn_ExchangeYes
            !ELSE
            !  DO WriteColumn_ExchangeNo
            !END              ! 16 May 2002 John
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF

        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & job:Ref_Number & ') END')
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_JobNumber                   ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = job:Ref_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateBooked                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        ! DateBooked
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:date_booked, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:date_booked, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateCompleted                   ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(aus:DateChanged, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(aus:DateChanged, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_UserSurname                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:Surname

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:Surname

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_UserForename                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:Forename

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:Forename

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Engineer                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:engineer_used

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LOC:engineer_used

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Engineer_QA                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & 'N/A'

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = 'N/A' !aud:user

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_EngineerSkillLevel                  ROUTINE ! Eng. Skill Level
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:SkillLevel

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:SkillLevel

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_JobSkillLevel                       ROUTINE ! Job Skill Level
    DATA
jobLevel LONG
    CODE
        !-----------------------------------------------
        ! Job Skill Level
        !


        IF LOC:JobsExtendedInSync = True
            IF jobe:SkillLevel < 1
                jobLevel = 1
            ELSIF jobe:SkillLevel > 10
                jobLevel = 10
            ELSE
                jobLevel = jobe:SkillLevel
            END !IF

        END !IF

        jobLevel = 1

        CASE job:Repair_Type_Warranty
          OF 'LEVEL 0'
            jobLevel = 1
          OF 'LEVEL 1'
            jobLevel = 1
          OF 'LEVEL 2'
            jobLevel = 2
          OF 'LEVEL 2.5'
            jobLevel = 3
          OF 'LEVEL 3'
            jobLevel = 3
          OF 'LEVEL 4'
            jobLevel = 4
          OF 'LEVEL 5'
            jobLevel = 5
        END

        CASE job:Repair_Type
          OF 'LEVEL 0'
            jobLevel = 1
          OF 'LEVEL 1'
            jobLevel = 1
          OF 'LEVEL 2'
            jobLevel = 2
          OF 'LEVEL 2.5'
            jobLevel = 3
          OF 'LEVEL 3'
            jobLevel = 3
          OF 'LEVEL 4'
            jobLevel = 4
          OF 'LEVEL 5'
            jobLevel = 5
        END

        !-----------------------------------------------
        ! jobLevel in range 1 .. 10
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & JobLevel

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = JobLevel

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ChargeableChargeType                    ROUTINE ! Write value to data cell
    DATA
Temp LIKE(job:Charge_Type)
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            Temp = job:Charge_Type
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ChargeableRepairType            ROUTINE
    DATA
Temp LIKE(job:Repair_Type)
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            Temp = tmp:rep_type_char
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_WarrantyChargeType                  ROUTINE ! Write value to data cell
    DATA
Temp LIKE(job:Warranty_Charge_Type)
    CODE
        !-----------------------------------------------
        IF job:Warranty_Job = 'YES'
            Temp = job:Warranty_Charge_Type
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_WarrantyRepairType              ROUTINE
    DATA
Temp LIKE(job:Repair_Type_Warranty)
    CODE
        !-----------------------------------------------
        IF job:Warranty_Job = 'YES'
            Temp = tmp:rep_type_war
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = tmp:rep_type_war

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_3rdPartyRepairer                  ROUTINE ! 3rd Party Repairer
    DATA
    CODE
        !-----------------------------------------------
        ! 3rd Party Repairer
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Third_Party_Site

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Third_Party_Site

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_EstimateStatus                    ROUTINE ! Set values for data sheet column header cell
    DATA
Temp STRING('ACCEPTED')
    CODE
        !-----------------------------------------------
        IF    job:Estimate_Accepted = 'YES'
            Temp = 'ACCEPTED'
        ELSIF job:Estimate_Rejected = 'YES'
            Temp = 'REJECTED'
        ELSIF job:Estimate          = 'YES'
            Temp = 'REQUIRED'
        ELSE
            Temp = 'N/A'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Manufacturer                ROUTINE ! Make
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Manufacturer

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Manufacturer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ModelNumber                 ROUTINE ! Model
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_UnitType                    ROUTINE ! Unit Type
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Unit_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Unit_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ESN                         ROUTINE ! IMEI
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_MSN                         ROUTINE ! MSN
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:MSN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:MSN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ColumnType                     ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        ! 16 May 2002 John
        ! Write which column has been ticked
        !   RTM, BER, BER-Water Damaged, NFF, Estimate Required
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:Category

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LOC:Category

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_FranchiseJobNumber      ROUTINE
    DATA
Temp STRING(10)
    CODE
        !-----------------------------------------------
        IF WEBJOB_OK
            Temp = wob:JobNumber
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(Temp)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(Temp)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseBranchNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = haQ:BranchIdentification

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = haQ:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ExchangeYes  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & 'YES'
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = 'YES'

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ExchangeNO  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & 'NO'

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = 'NO'
        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ExchangeDate                   ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(LOC:Exchange_Date, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(LOC:Exchange_Date, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_SparesReq                   ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(LOC:Spares_Date, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(LOC:Spares_Date, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Different  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:Different

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LOC:Different
        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary details
        !-----------------------------------------------------------------
        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.FormulaR1C1'}     = '=SUBTOTAL(2, r' & sheet:DataHeaderRow+1 & 'c1:r' & sheet:DataHeaderRow+RecordCount & 'c1)'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                             ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

JobLevelLess  LONG
JobLevelEqual LONG
JobLevelMore  LONG

HeaderRow   LONG(23)
FirstRow    LONG
LastRow     LONG
SubTotalRow LONG
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}

        WriteDebug('WriteHeadSummary()')
        !-----------------------------------------------------------------
        !TitleHeaderRow = Excel{'ActiveSheet.Row'}
        ResultsCount = RECORDS(UserResultsQueue)

        HeaderRow    = sheet:HeadSummaryRow     ! Excel{'ActiveSheet.Row'} ! sheet:HeadSummaryRow
        FirstRow     = HeaderRow + 1            !
        LastRow      = HeaderRow + ResultsCount ! depends on engineers location
        SubTotalRow  = LastRow   + 1            ! depends on engineers location
        !-----------------------------------------------------------------
        Excel{'Range("A' & HeaderRow & ':' & CLIP(sheet:HeadLastCol) & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 00).Formula'} = 'Engineer Surname'  ! From "Surname"   13 Sep 2002 John
            Excel{'ActiveCell.Offset(0, 01).Formula'} = 'Engineer Forename' ! From "Forename"  13 Sep 2002 John
!            Excel{'ActiveCell.Offset(0, 02).Formula'} = 'User Code'
            Excel{'ActiveCell.Offset(0, 02).Formula'} = 'Jobs Completed'    ! From "Completed" 13 Sep 2002 John
!            Excel{'ActiveCell.Offset(0, 04).Formula'} = 'Hit Rate %'

            Excel{'ActiveCell.Offset(0, 03).Formula'} = 'Spares Requested'
            Excel{'ActiveCell.Offset(0, 04).Formula'} = 'Exchange'
            Excel{'ActiveCell.Offset(0, 05).Formula'} = 'Excluded'
!            Excel{'ActiveCell.Offset(0, 08).Formula'} = 'Liquid Damage'
!            Excel{'ActiveCell.Offset(0, 09).Formula'} = 'Modification'
            Excel{'ActiveCell.Offset(0, 6).Formula'} = 'N.F.F.'
            Excel{'ActiveCell.Offset(0, 7).Formula'} = 'RNR/Liquid Damage/BER/Scrap'
            Excel{'ActiveCell.Offset(0, 8).Formula'} = 'R.T.M.'
            Excel{'ActiveCell.Offset(0, 9).Formula'} = 'Repair'

            Excel{'ActiveCell.Offset(0, 10).Formula'} = 'Level 0'
            Excel{'ActiveCell.Offset(0, 11).Formula'} = 'Level 1'
            Excel{'ActiveCell.Offset(0, 12).Formula'} = 'Level 2'
            Excel{'ActiveCell.Offset(0, 13).Formula'} = 'Level 2.5'
            Excel{'ActiveCell.Offset(0, 14).Formula'} = 'Level 3'

!            Excel{'ActiveCell.Offset(0, 19).Formula'} = 'Skill Level 1'
!            Excel{'ActiveCell.Offset(0, 20).Formula'} = 'Skill Level 2'
!            Excel{'ActiveCell.Offset(0, 21).Formula'} = 'Skill Level 3'

            Excel{'ActiveCell.Offset(0, 15).Formula'} = '3rd Party Repair'
            Excel{'ActiveCell.Offset(0, 16).Formula'} = 'Software Upgrade'
            Excel{'ActiveCell.Offset(0, 17).Formula'} = 'Estimates'
!            Excel{'ActiveCell.Offset(0, 25).Formula'} = 'Scrap'
            Excel{'ActiveCell.Offset(0, 18).Formula'} = 'Unlock'
!            Excel{'ActiveCell.Offset(0, 27).Formula'} = 'Phone Unlock'

!            Excel{'ActiveCell.Offset(0, 28).Formula'} = 'First Line'
!            Excel{'ActiveCell.Offset(0, 29).Formula'} = 'Skill Level'

!            Excel{'ActiveCell.Offset(0, 30).Formula'} = 'Completed Above Level'
!            Excel{'ActiveCell.Offset(0, 31).Formula'} = 'Completed At Level'
!            Excel{'ActiveCell.Offset(0, 32).Formula'} = 'Completed Below Level'
!            Excel{'ActiveCell.Offset(0, 33).Formula'} = 'Units Quality Assured'

!        Excel{'Range("F' & HeaderRow & ':N' & HeaderRow & '").Select'}
!            DO XL_HighLight

        Excel{'Range("A' & FirstRow & '").Select'}

        IF ResultsCount = 0
            Excel{'ActiveCell.Formula'} = 'No Jobs Found'
            Excel{'Range("A' & FirstRow+2 & '").Select'}

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Summary'
        RecordsToProcess = ResultsCount

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        SORT(UserResultsQueue, +uq:Surname, +uq:Forename, +uq:UserCode)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------

            DO ProgressBar_Loop
            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(UserResultsQueue, QueueIndex)
            WriteDebug('WriteHeadSummary(Row=' & CLIP(uq:UserCode) & ', ' & CLIP(uq:Surname) & ')')

            IF NOT uq:SiteLocation = tra_ali:SiteLocation
                WriteDebug('Engineer works at "' & CLIP(uq:SiteLocation) & '" not "' & CLIP(tra_ali:SiteLocation) & '"')
                CYCLE
            END !IF
            IF uq:JobsCompleted = 0
              CYCLE
            END
            !RecordCount += 1

            Excel{'ActiveCell.Formula'}                    = uq:Surname
            Excel{'ActiveCell.Offset(0,  1).Formula'}      = uq:Forename
!            Excel{'ActiveCell.Offset(0,  2).Formula'}      = uq:UserCode
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = uq:JobsCompleted
!            Excel{'ActiveCell.Offset(0,  4).NumberFormat'} = '0.00%'
!            Excel{'ActiveCell.Offset(0,  4).FormulaR1C1'}  = '=IF(r' & HeaderRow-3 & 'c2=0, 0, RC[-1]/r' & HeaderRow-3 & 'c2)'

            Excel{'ActiveCell.Offset(0, 03).Formula'}      = uq:Spares_Req
            Excel{'ActiveCell.Offset(0, 04).Formula'}      = uq:Jobs_Exchange
            Excel{'ActiveCell.Offset(0, 05).Formula'}      = uq:Jobs_Excluded
!            Excel{'ActiveCell.Offset(0, 08).Formula'}      = uq:Jobs_LiquidDamage
!            Excel{'ActiveCell.Offset(0, 09).Formula'}      = uq:Jobs_Modification
            Excel{'ActiveCell.Offset(0, 6).Formula'}      = uq:Jobs_NFF
            Excel{'ActiveCell.Offset(0, 7).Formula'}      = uq:Jobs_RNR
            Excel{'ActiveCell.Offset(0, 8).Formula'}      = uq:Jobs_RTM
            Excel{'ActiveCell.Offset(0, 9).Formula'}      = uq:Jobs_Repair

            Excel{'ActiveCell.Offset(0, 10).Formula'}      = uq:level0
            Excel{'ActiveCell.Offset(0, 11).Formula'}      = uq:level1
            Excel{'ActiveCell.Offset(0, 12).Formula'}      = uq:level2
            Excel{'ActiveCell.Offset(0, 13).Formula'}      = uq:level25
            Excel{'ActiveCell.Offset(0, 14).Formula'}      = uq:level3

            JobLevelLess  = 0
            JobLevelEqual = 0
            JobLevelMore  = 0

 !           LOOP X# = 1 TO 3                   !JJ this was from 1 to 6
 !               IF uq:SkillLevel < X#
 !                   JobLevelMore  += uq:JobsLevel[ X# ]
 !               ELSIF uq:SkillLevel = X#
 !                   JobLevelEqual += uq:JobsLevel[ X# ]
 !               ELSE
 !                   JobLevelLess  += uq:JobsLevel[ X# ]
 !               END !if
 !                                               !JJ this was 13
 !               Excel{'ActiveCell.Offset(0, ' & 18+X# & ').Formula'} = uq:JobsLevel[ X# ]
 !           END !LOOP

            Excel{'ActiveCell.Offset(0, 15).Formula'} = uq:third_party_count
            Excel{'ActiveCell.Offset(0, 16).Formula'} = uq:Software_Upgrade
            Excel{'ActiveCell.Offset(0, 17).Formula'} = uq:Estimate
 !           Excel{'ActiveCell.Offset(0, 25).Formula'} = uq:scrap
            Excel{'ActiveCell.Offset(0, 18).Formula'} = uq:Network_Unlock
 !           Excel{'ActiveCell.Offset(0, 27).Formula'} = uq:Phone_Unlock

 !          Excel{'ActiveCell.Offset(0, 28).Formula'} = uq:JobCount ! '*First Line*'
 !           Excel{'ActiveCell.Offset(0, 29).Formula'} = uq:SkillLevel

 !           Excel{'ActiveCell.Offset(0, 30).Formula'} = JobLevelMore
 !           Excel{'ActiveCell.Offset(0, 31).Formula'} = JobLevelEqual
 !           Excel{'ActiveCell.Offset(0, 32).Formula'} = JobLevelLess
 !           Excel{'ActiveCell.Offset(0, 33).Formula'} = uq:qa

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        LastRow     = HeaderRow + RecordCount
        SubTotalRow = LastRow + 1
        !-----------------------------------------------------------------
        ! Totals
!        !
!        IF RecordCount > 0
!            Excel{'Range("B10").Select'}
!                Excel{'ActiveCell.Offset(0, 0).Formula'} = '=SUM(F' & FirstRow & ':F' & LastRow & ')' ! B10 ! B.E.R.
!                Excel{'ActiveCell.Offset(1, 0).Formula'} = '=SUM(G' & FirstRow & ':G' & LastRow & ')' ! B11 ! Exchange
!                Excel{'ActiveCell.Offset(2, 0).Formula'} = '=SUM(H' & FirstRow & ':H' & LastRow & ')' ! B12 ! Excluded
!
!                Excel{'ActiveCell.Offset(3, 0).Formula'} = '=SUM(I' & FirstRow & ':I' & LastRow & ')' ! B13 ! Liquid Damage
!                Excel{'ActiveCell.Offset(4, 0).Formula'} = '=SUM(J' & FirstRow & ':J' & LastRow & ')' ! B14 ! Modification
!                Excel{'ActiveCell.Offset(5, 0).Formula'} = '=SUM(K' & FirstRow & ':K' & LastRow & ')' ! B15 ! N.F.F.
!
!                Excel{'ActiveCell.Offset(6, 0).Formula'} = '=SUM(L' & FirstRow & ':L' & LastRow & ')' ! B16 ! R.N.R.
!                Excel{'ActiveCell.Offset(7, 0).Formula'} = '=SUM(M' & FirstRow & ':M' & LastRow & ')' ! B17 ! R.T.M.
!                Excel{'ActiveCell.Offset(8, 0).Formula'} = '=SUM(N' & FirstRow & ':N' & LastRow & ')' ! B18 ! Repair
!        END !IF
!
!        Excel{'Range("B19").Select'}
!            Excel{'ActiveCell.Formula'} = '=SUM(B10:B18)'

        Excel{'Range("A' & HeaderRow & ':' & CLIP(sheet:HeadLastCol) & LastRow & '").Select'}
            Excel{'Selection.AutoFilter'}
        !-----------------------------------------------------------------
        Excel{'Range("A' & SubTotalRow & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
AddJobNumberToQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
!        IF CancelPressed
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        IF RECORDS(JobNumberQueue) = 0
!            jq:JobNumber = LOC:JobNumber
!
!            ADD(JobNumberQueue)
!            RecordCount += 1
!            SORT(JobNumberQueue, +jq:JobNumber)
!
!            EXIT ! 
!        END !IF
!
!        jq:JobNumber = LOC:JobNumber
!        GET(JobNumberQueue, +jq:JobNumber)
!        IF ERRORCODE()
!            jq:JobNumber = LOC:JobNumber
!            ADD(JobNumberQueue, +jq:JobNumber)
!            RecordCount += 1
!
!        END !IF
        !-----------------------------------------------------------------
    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mquest.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mquest.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetUserName2                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE
        tmpPos = INSTRING('%', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Engineer Performance Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!!        IF CLIP(LOC:FileName) <> ''
!!            ! Already generated 
!!            EXIT
!!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END
!            desktop = CLIP(desktop)&'\EMAILED'
!            IF NOT EXISTS(CLIP(desktop))
!                IF MKdir( Desktop )
!                  MESSAGE('ERROR')
!                END
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0057 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) &  ' VODR0008 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
FillUserResultsQueue                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('FillUserResultsQueue')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        FREE(UserResultsQueue)
        CLEAR(UserResultsQueue)
!
!        IF RECORDS(UserResultsQueue) > 0
!            ! Already loaded, ignore when export button tried again after abort
!
!            EXIT
!        END !IF
        !-----------------------------------------------------------------
        Access:USERS.ClearKey(sub:Account_Number_Key)
        !use:User_Code = ''
        SET(use:User_Code_Key, use:User_Code_Key)
        !------------------------------------------
        Progress:Text    = 'Loading Engineers'
        RecordsToProcess = 1000 ! RECORDS(UserResultsQueue)
        RecordsProcessed = 0
        RecordCount      = 0

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:USERS.Next()
            !-------------------------------------------------------------
            WriteDebug('FillUserResultsQueue("' & CLIP(use:User_Code) & '")')

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF use:Active <> 'YES'
                WriteDebug('FillUserResultsQueue("' & CLIP(use:User_Code) & '") NOT ACTIVE')

                CYCLE
            ELSIF use:User_Type <> 'ENGINEER'
                WriteDebug('FillUserResultsQueue("' & CLIP(use:User_Code) & '") NOT AN ENGINEER')

                CYCLE
!            ELSIF use:Location <> tra_ali:SiteLocation
!                WriteDebug('FillUserResultsQueue("' & CLIP(use:User_Code) & '") WORKS AT "' & CLIP(use:Location) & '" NOT "' & CLIP(tra_ali:SiteLocation) & '"')
!
!                CYCLE
            END !IF
            !=============================================================
            WriteDebug('FillUserResultsQueue("' & CLIP(use:User_Code) & '") ADD')
   !         RecordCount += 1

            CLEAR(UserResultsQueue)
                uq:UserCode     = use:User_Code
                uq:Forename     = use:Forename
                uq:Surname      = use:Surname
                uq:SiteLocation = use:Location
                uq:SkillLevel   = use:SkillLevel
            ADD(UserResultsQueue)
        END !LOOP

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        SORT(UserResultsQueue, +uq:UserCode)

        DO ProgressBar_LoopPost

        WriteDebug('FillUserResultsQueue(EXIT)')
        !-----------------------------------------------
    EXIT
GetUserDetailsFromQueue_Tabulations ROUTINE
    DATA
jobLevel        LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('GetUserDetailsFromQueue_Tabulations START')
        IF LOC:Spares_Req = TRUE
            uq:spares_Req += 1
            EXIT
        ELSE
            uq:JobsCompleted += 1
        END
        !-----------------------------------------------------------------
        jobLevel = 1

        IF LOC:JobsExtendedInSync = True
            IF    jobe:SkillLevel < 1
                jobLevel = 1
            ELSIF jobe:SkillLevel > 5
                jobLevel = 5
            ELSE
                jobLevel = jobe:SkillLevel
            END !IF

        END !IF
        tmp:rep_type_char=''
        tmp:rep_type_war=''
        Access:Audit.ClearKey(aud:Action_Key)
        aud:Ref_Number = job:ref_number
        aud:Action = 'JOB UPDATED'
        SET(aud:Action_Key,aud:Action_Key)
        LOOP
            IF Access:Audit.Next()
                BREAK
            END
            IF aud:Ref_Number <> job:ref_number
                BREAK
            END
            IF aud:Action <> 'JOB UPDATED'
                BREAK
            END
            IF INRANGE(aud:Date,LOC:StartDate,LOC:EndDate)
            !Work out what repair type was!
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                    x# = INSTRING('CHAR. REPAIR TYPE:',aud2:Notes,1,1)
                    IF x# <> 0
                        LOOP y# = x#+19 TO LEN(CLIP(aud2:Notes))
                            IF SUB(aud2:Notes,y#,1) = CHR(13)
                                BREAK
                            END
                        END
                        tmp:rep_type_char = SUB(aud2:Notes,x#+19,y#-(x#+19))
                    END
                    x# = INSTRING('WARR. REPAIR TYPE:',aud2:Notes,1,1)
                    IF x# <> 0
                        LOOP y# = x#+19 TO LEN(CLIP(aud2:Notes))
                            IF SUB(aud2:Notes,y#,1) = CHR(13)
                                BREAK
                            END
                        END
                        tmp:rep_type_war = SUB(aud2:Notes,x#+19,y#-(x#+19))
                    END
                !BREAK
                END ! IF

            END
        END

!        uq:JobsLevel[ JobLevel ] += 1 ! JobLevel in range 1 .. 10
        !-----------------------------------------------------------------
        ! 13 Sep 2002 John
        ! Stop supressing warranty jobs - Joe
        !
        !IF job:Warranty_Job = 'YES'
        !    EXIT
        !END !IF
        !-----------------------------------------------------------------
        WriteDebug('GetUserDetailsFromQueue_Tabulations 3')

        !Check!


        LOC:Different = 'YES'
        IF job:Chargeable_Job = 'YES'
            IF CLIP(tmp:rep_type_char) <> ''
                IF tmp:rep_type_char <> job:Repair_Type
                    LOC:Different = 'NO'
                    LOC:Category = GetRepairType(CLIP(tmp:rep_type_char))
                ELSE
                    tmp:rep_type_char = job:Repair_Type
                    LOC:Category = GetRepairType(job:Repair_Type)
                END
            ELSE
                tmp:rep_type_char = job:Repair_Type
                LOC:Category = GetRepairType(job:Repair_Type)
            END
        ELSIF job:Warranty_Job = 'YES'
            IF CLIP(tmp:rep_type_char) <> ''
                IF tmp:rep_type_war <> job:Repair_Type_Warranty
                    LOC:Different = 'NO'
                    LOC:Category = GetRepairType(CLIP(tmp:rep_type_war))
                ELSE
                    tmp:rep_type_war = job:Repair_Type_Warranty
                    LOC:Category = GetRepairType(job:Repair_Type_Warranty)
                END
            ELSE
                tmp:rep_type_war = job:Repair_Type_Warranty
                LOC:Category = GetRepairType(job:Repair_Type_Warranty)
            END
        END !IF

        WriteDebug('GetUserDetailsFromQueue_Tabulations 4')

        IF CLIP(LOC:Category) = ''
            WriteDebug('GetUserDetailsFromQueue_Tabulations 5')

            IF job:Estimate_Rejected = 'YES'
                LOC:Category = 'Estimate Rejected'
            ELSE
                LOC:Category = 'Standard'
            END !IF
        END !IF
        !-----------------------------------------------------------------

        jobLevel = 1

        WriteDebug('GetUserDetailsFromQueue_Tabulations 6')
        ! 16 May 2002 John
        !
        CASE LOC:Category
        OF 'B.E.R.'
            uq:Jobs_RNR          += 1
        OF 'Exchange'
           ! uq:Jobs_Exchange     += 1
        OF 'Excluded'
            uq:Jobs_Excluded     += 1
        OF 'Liquid Damage'
            uq:Jobs_RNR          += 1
        OF 'Modification'
            uq:Jobs_Modification += 1
        OF 'N.F.F.'
            uq:Jobs_NFF          += 1
        OF 'R.N.R.'
            uq:Jobs_RNR          += 1
        OF 'R.T.M.'
            uq:Jobs_RTM          += 1
        OF 'Repair'
            !uq:Jobs_Repair       += 1
        ELSE !OF '' !<Unknown(' & rtd:BER & ')'
            ! null
        END !CASE

        !Look up JOO!
!        Access:JobOutFl.ClearKey(joo:JobNumberKey)
!        joo:JobNumber = job:ref_number
!        SET(joo:JobNumberKey,joo:JobNumberKey)
!        LOOP
!          IF Access:JobOutFl.Next()
!            BREAK
!          END
!          IF joo:JobNumber <> job:ref_number
!            BREAK
!          END
!          IF CLIP(joo:Description) = 'REPAIRED BY 3RD PARTY'
!            !uq:Jobs_Excluded     += 1
!          END
!          IF CLIP(joo:Description) = 'REPAIRED BY THIRD PARTY'
!            !uq:Jobs_Excluded     += 1
!          END
!          IF CLIP(joo:Description) = 'RETURN TO MANUFACTURER'
!            !uq:Jobs_RTM          += 1
!          END
!        END

        CASE job:Repair_Type_Warranty
        OF 'SOFTWARE UPGRADE'
            uq:Software_Upgrade += 1
            uq:Jobs_Repair       += 1
        OF 'SCRAP'
            uq:Jobs_RNR         += 1
        OF 'NETWORK UNLOCK'
            uq:Network_Unlock   += 1
            uq:Jobs_Repair       += 1
        OF 'PHONE UNLOCK'
            uq:Network_Unlock   += 1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 0'
            jobLevel = 1      !JJ was 1
            uq:Level0 +=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 1'
            jobLevel = 1
            uq:Level1+=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 2'
            jobLevel = 2
            uq:Level2+=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 2.5'
            jobLevel = 3
            uq:Level25+=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 3'
            jobLevel = 3
            uq:Level3+=1
            uq:Jobs_Repair       += 1
        END

        CASE job:Repair_Type
        OF 'SOFTWARE UPGRADE'
            uq:Software_Upgrade += 1
            uq:Jobs_Repair       += 1
        OF 'SCRAP'
            uq:Jobs_RNR         += 1
        OF 'NETWORK UNLOCK'
            uq:Network_Unlock   += 1
            uq:Jobs_Repair       += 1
        OF 'PHONE UNLOCK'
            uq:Network_Unlock   += 1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 0'
            jobLevel = 1      !JJ was 1
            uq:Level0 +=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 1'
            jobLevel = 1
            uq:Level1+=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 2'
            jobLevel = 2
            uq:Level2+=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 2.5'
            jobLevel = 3
            uq:Level25+=1
            uq:Jobs_Repair       += 1
        OF 'LEVEL 3'
            jobLevel = 3
            uq:Level3+=1
            uq:Jobs_Repair       += 1
        END

!        IF job:Estimate = 'YES'
!          uq:Estimate += 1
!        END
!        IF job:Third_Party_Site <> ''
!          uq:third_party_count +=1
!        END

        IF LOC:Estimate_Flag = TRUE
            uq:Estimate += 1
        END
        IF LOC:Third_Party_Flag = TRUE
            uq:third_party_count +=1
        END
        IF LOC:SendToARC = TRUE AND LOC:Third_Party_Flag = FALSE
            uq:third_party_count +=1
        END
        LOC:Exchange_Date = ''
        !Redefine Exchange Unit definition?!
        Access:Audit.ClearKey(aud:Action_Key)
        aud:Ref_Number = job:ref_number
        aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
        SET(aud:Action_Key,aud:Action_Key)
        LOOP
            IF Access:Audit.Next()
                BREAK
            END
            IF aud:Ref_Number <> job:ref_number
                BREAK
            END
            IF aud:Action <> 'EXCHANGE UNIT ATTACHED TO JOB'
                BREAK
            END
            IF INRANGE(aud:Date,LOC:StartDate,LOC:EndDate)
            ! Exchange allocated!
                uq:Jobs_Exchange     += 1
                LOC:Exchange_Date = aud:date
                BREAK
            END
        END

        !IF job:Exchange_Unit_Number > 0
        !  uq:Jobs_Exchange     += 1
        !END

        uq:JobsLevel[ JobLevel ] += 1 ! JobLevel in range 1 .. 10

        WriteDebug('GetUserDetailsFromQueue_Tabulations END')
        !-----------------------------------------------------------------
        EXIT

GetHeadAccountDetailsFromQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! on entry LOC:HeadAccountNumber = Account Number to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        haQ:AccountNumber = LOC:HeadAccountNumber
        GET(HeadAccount_Queue, +haQ:AccountNumber)
        IF ERRORCODE()
            ! Not in queue - ADD
            CLEAR(HeadAccount_Queue)
                IF LoadTRADEACC(LOC:HeadAccountNumber)
                    haQ:AccountNumber        = LOC:HeadAccountNumber
                    haQ:AccountName          = tra:Company_Name
                    haQ:BranchIdentification = tra:BranchIdentification
                ELSE
                    haQ:AccountNumber        = LOC:HeadAccountNumber
                    haQ:AccountName          = ''
                    haQ:BranchIdentification = '*T'
                END ! IF
            ADD(HeadAccount_Queue, +haQ:AccountNumber)
        END !IF

        ! In queue - AMEND
        haQ:JobsBooked += 1
        PUT(HeadAccount_Queue, +haQ:AccountNumber)
        !-----------------------------------------------------------------
    EXIT
UpdateOtherEngineersWhoHaveworkedOnJob              ROUTINE
    DATA
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        WriteDebug('UpdateOtherEngineersWhoHaveworkedOnJob(' & job:Ref_Number & ')')
        !-----------------------------------------------
        ! UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE
        ! AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE
        ! JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE
        !
        Access:JOBSENG.ClearKey(joe:AllocatedKey)
        joe:JobNumber = job:Ref_Number
        SET(joe:AllocatedKey, joe:AllocatedKey)

        LOOP WHILE Access:JOBSENG.NEXT() = Level:Benign
            !-----------------------------------------------------------------
            WriteDebug('UpdateOtherEngineersWhoHaveworkedOnJob("' & CLIP(joe:UserCode) & '")')

            IF joe:JobNumber <> job:Ref_Number
                WriteDebug('UpdateOtherEngineersWhoHaveworkedOnJob() EOI')

                BREAK
            ELSIF joe:UserCode = job:Engineer
                WriteDebug('UpdateOtherEngineersWhoHaveworkedOnJob(CurrentEngineer="' & CLIP(joe:UserCode) & '")')

                CYCLE
            END !IF
            !-----------------------------------------------------------------
            uq:UserCode  = joe:UserCode
            GET(UserResultsQueue, +uq:UserCode)
            CASE ERRORCODE()
            OF 00 ! Found
                IF uq:UserCode  = joe:UserCode
                    ! NULL
                ELSE
                    DoADD = True
                END !IF

            OF 30 ! NOT Found
                DoADD = True ! Not in queue - ADD
            ELSE
                CancelPressed = True

                EXIT
            END !CASE
            !-----------------------------------------------------------------
            IF DoADD = True
                LOC:UserCode = joe:UserCode
                
                CLEAR(UserResultsQueue)
                    uq:UserCode          = joe:UserCode

                    IF LoadUSERS(joe:UserCode)
                        uq:Forename      = use:Forename
                        uq:Surname       = use:Surname
                        uq:SiteLocation  = use:Location
                        uq:SkillLevel    = use:SkillLevel
                    END ! IF
                ADD(UserResultsQueue, +uq:UserCode)
            END !IF
            !-----------------------------------------------------------------
            ! In queue - AMEND
            uq:JobCount += 1
            PUT(UserResultsQueue, +uq:UserCode)
            !-----------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
SyncJOBSE                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Synchronise JOBSE (Jobs Extension) to current JOBS file
        !
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number

        IF Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            Result = True
        ELSE
            Result = False
        END !IF
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            recordspercycle      = 25
            recordsprocessed     = 0
            recordstoprocess     = 10 !***The Number Of Records, or at least a guess***
            percentprogress      = 0
            progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT
ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        Result            = False

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If tmp:cancel = 1
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        !IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        !
            Display()
        !END !IF
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT
ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT
ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted
ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment SIGNED
    CODE
        !-----------------------------------------------
        xlComment = Excel{'Selection.AddComment("' & CLIP(LOC:CommentText) & '")'}

        xlComment{'Shape.IncrementLeft'} = 127.50
        xlComment{'Shape.IncrementTop'}  =   8.25
        xlComment{'Shape.ScaleWidth'}    =   1.76
        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("A11"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0 ! Default/error value
        OperatingSystem     = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020597'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EngineerPerformanceReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  Access:REPTYCAT.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBSENG.UseFile
  Access:REPAIRTY.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBOUTFL.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Engineer Performance Report'                !              Job=896 Cust=N11
  
      excel:Visible    = False !
  
      LOC:StartDate = TODAY() !
      LOC:EndDate   = TODAY() !
  
      DO GetUserName2
  
      !debug:Active = True
  
      LOC:WorkingHours = 0
      SET(Defaults, 1)
      !IF Access:DEFAULTS.NEXT() = Level:Benign
          LOC:WorkingHours = HoursBetween(def:Start_Work_Hours, def:End_Work_Hours)
      !END !IF
  
  !    LOC:RepairTypeCategory = 'RTM'
  !    OLD:RepairTypeCategory = ''
  
  !    Unassigned_BRW.ActiveInvisible = 1
  !    Assigned_BRW.ActiveInvisible   = 1
  
      IF GUIMode = 1 THEN
         LocalTimeOut = 500
         MainWindow{PROP:ICONIZE} = TRUE
      END !IF
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  
        !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
        If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = True
        End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
   
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        LOC:StartDate       = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
        LOC:EndDate       = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
      END
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,tra:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,tra:Account_Number,1,BRW4)
  BRW4.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(LocalTag,BRW4.Q.LocalTag)
  BRW4.AddField(tra:Account_Number,BRW4.Q.tra:Account_Number)
  BRW4.AddField(tra:Company_Name,BRW4.Q.tra:Company_Name)
  BRW4.AddField(tra:BranchIdentification,BRW4.Q.tra:BranchIdentification)
  BRW4.AddField(tra:RecordNumber,BRW4.Q.tra:RecordNumber)
      IF Automatic = TRUE
        IF loc:startdate = ''
          LOC:StartDate = TODAY()
          LOC:EndDate = TODAY()
        END
        !DoAll = 'Y'
  
         !Start Date is Today  (DBH: 07-05m-2004)
  
         If Command('/DAILY')
             loc:StartDate = Today()
             loc:EndDate = Today()
         End !If Command('/DAILY')
  
         !Weekly and Monlthy were not specified for this report  (DBH: 07-05-2004)
  
         If Command('/WEEKLY')
             Loop day# = Today() To Today()-7 By -1
                 If day# %7 = 1
                     loc:StartDate = day#
                     Break
                 End !If day# %7 = 2
             End !Loop day# = Today() To Today()-7 By -1
             Loop day# = Today() To Today() + 7
                 IF day# %7 = 0
                     loc:EndDate = day#
                     Break
                 End !IF day# %7 = 0
             End !Loop day# = Today() To Today() + 7 By -1
         End !If Command('/WEEKLY')
         !Monthly - Start Date is 1st of Month, End Date is last of month  (DBH: 07-05-2004)
         If Command('/MONTHLY')
             loc:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
             loc:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
         End !If Command('/MONTHLY')
  
        DO OKButton_Pressed
        !POST(Event:Closewindow)
        !Call email program!
        PUTINI('MAIN','InputDir',CLIP(Loc:Path),CLIP(PATH()) & '\AUTOEMAIL.INI')
        PUTINI('MAIN','ExportDir',CLIP(Loc:Path)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
        RUN('EMAILDIR',1)
        POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020597'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020597'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020597'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?StartDateCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?EndDateCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?StartDateCalendar)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndDateCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW4) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
Days_Between_Routine PROCEDURE  (f_start_date,f_end_date) ! Declare Procedure
f_days LONG
  CODE
    Set(defaults)
    access:defaults.next()
    f_days = 0
    Loop
        f_start_date += 1
        If def:include_saturday <> 'YES'
            If (f_start_date) % 7 = 6
                Cycle
            End!If (f_start_date + count#) % 7 = 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If (f_start_date) % 7 = 0
                Cycle
            End!If (f_start_date + count#) % 7 = 6
        End!If def:include_saturday <> 'YES'
        f_days += 1
        If f_start_date > = f_end_date
            Break
        End!If f_start_date > = f_end_date
    End!Loop

    return f_days
DaysBetween PROCEDURE  (f_start_date,f_end_date)!,long ! Declare Procedure
StartWeekday LONG
EndWeekday   LONG

Weeks        LONG
Days         LONG

DaysDiff     LONG
DaysPerWeek  LONG(5)
    CODE
        !-------------------------------------------
        IF (f_start_date = f_end_date)
            RETURN 0
        ELSIF (f_start_date > f_end_date)
            RETURN DaysBetween(f_end_date, f_start_date)
        END !IF
        !-------------------------------------------
        StartWeekday = (f_start_date ) % 7
        EndWeekday   = (f_end_date   ) % 7

        DaysDiff     = (f_start_date - f_end_date)

        Weeks        = DaysDiff / 7
        Days         = DaysDiff % 7
        !-------------------------------------------
        IF EndWeekday < StartWeekday
            Days     = Days - 2
        END !IF
        !-------------------------------------------
        Set(defaults)
        access:defaults.next()

        IF def:include_saturday <> 'YES'
            DaysPerWeek += 1
        END !IF

        IF def:include_sunday <> 'YES'
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        message('Weeks='                        & Weeks       & '<13,10>' & |
                'DaysPerWeek='                  & DaysPerWeek & '<13,10>' & |
                'Days='                         & Days        & '<13,10>' & |
                ' = <13,10>'                                              & |
                '(Weeks * DaysPerWeek) + Days=' & (Weeks * DaysPerWeek) + Days)
        RETURN (Weeks * DaysPerWeek) + Days
        !-------------------------------------------
GetEngineer                 PROCEDURE( IN:UserCode )
DoADD LONG(False)
    CODE
        !-----------------------------------------------------------------
        WriteDebug('GetEngineer(' & CLIP(IN:UserCode) & ') START')
        ! on entry IN:UserCode = User code to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        DoAdd = FALSE
        SORT(UserResultsQueue, +uq:UserCode)
        uq:UserCode = IN:UserCode
        GET(UserResultsQueue, +uq:UserCode)
        CASE ERRORCODE()
        OF 00 ! FOUND

        OF 30
            ! Not in queue - ADD
            WriteDebug('GetEngineer(Not in queue - ADD)')
            DoADD = True
            WriteDebug('GetEngineer 1')

        END !CASE
        !-----------------------------------------------------------------
        IF DoADD = True
            WriteDebug('GetEngineer(ADD)')
            CLEAR(UserResultsQueue)
                uq:UserCode      = IN:UserCode

                IF LoadUSERS(IN:UserCode)
                    WriteDebug('GetEngineer(LoadUSERS OK)')

                    uq:Forename      = use:Forename
                    uq:Surname       = use:Surname
                    uq:SiteLocation  = use:Location
                    uq:SkillLevel    = use:SkillLevel
                ELSE
                    WriteDebug('GetEngineer(LoadUSERS FAIL)')
                END ! IF
            ADD(UserResultsQueue, +uq:UserCode)
        END !IF
        !-----------------------------------------------------------------
        WriteDebug('GetEngineer END')
        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF haq:AccountNumber  = IN:AccountNumber
                ! Found
            ELSE
                ! Partial Match - ADD
                DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
        IF DoAdd
            CLEAR(HeadAccount_Queue)
                haq:AccountNumber            = IN:AccountNumber

                IF LoadTRADEACC(IN:AccountNumber)
                    haq:AccountName          = tra:Company_Name
                    haq:BranchIdentification = tra:BranchIdentification

!                    !IF tra:UseTimingsFrom = 'TRA'
!                        IF tra:IncludeSaturday = 'YES'
!                            haQ:IncludeSaturday = True
!                        END !IF
!
!                        IF tra:IncludeSunday = 'YES'
!                            haQ:IncludeSunday = True
!                        END !IF
!                    !ELSE
!                    !    IF def:Include_Saturday = 'YES'
!                    !        haQ:IncludeSaturday = True
!                    !    END !IF
!                    !
!                    !    IF def:Include_Sunday = 'YES'
!                    !        haQ:IncludeSunday = True
!                    !    END !IF
!                    !END !IF

                ELSE
                    haq:AccountName          = '*T'
                    haq:BranchIdentification = '*T'
                END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        END !IF
        !-----------------------------------------------------------------
GetRepairType   PROCEDURE( IN:RepairType )! STRING
    CODE
        !-------------------------------------------
        WriteDebug('GetRepairType(' & CLIP(IN:RepairType) & ') START')

        cq:RepairType = IN:RepairType
        GET(RepairTypeCategoryQueue, +cq:RepairType)

        CASE ERRORCODE()
        OF 00
            ! Found
        OF 30
            !---------------------------------------
            cq:RepairType = IN:RepairType

            IF LoadREPTYDEF(IN:RepairType)
                CASE rtd:BER
                OF 0
                    cq:Category = 'Repair'
                OF 1
                    cq:Category = 'B.E.R.'
                OF 2
                    cq:Category = 'Modification'
                OF 3
                    cq:Category = 'Exchange'
                OF 4
                    cq:Category = 'Liquid Damage'
                OF 5
                    cq:Category = 'N.F.F.'
                OF 6
                    cq:Category = 'R.T.M.'
                OF 7
                    cq:Category = 'Excluded'
                OF 8
                    cq:Category = 'R.N.R.'
                ELSE
                    cq:Category = '' !<Unknown(' & rtd:BER & ')'
                END !CASE
            ELSE
                cq:Category = '' !<Unknown(' & CLIP(IN:RepairType) & ')>'
            END !IF

            ADD(RepairTypeCategoryQueue, +cq:RepairType)
            !---------------------------------------
        ELSE
            CancelPressed = True

            RETURN ''
        END !CASE

        WriteDebug('GetRepairType END')
        RETURN CLIP(cq:Category)
        !-------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
    SentToARC# = False
    LOC:ARCDateBooked = ''
    Access:LOCATLOG.Clearkey(lot:DateKey)
    lot:RefNumber = in:JOBNumber
    Set(lot:DateKey,lot:DateKey)
    Loop WHILE Access:LOCATLOG.Next() = Level:Benign
        If lot:RefNumber <> in:JobNumber 
            Break
        End !
        If lot:NewLocation = loc:ARCLocation
            SentToARC# = True
            loc:ARCDateBooked = lot:TheDate
            !MESSAGE('TRUE CONDITION MET')
        Break
        End!    
      
    End !Loop

    Return SentToARC#















!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!    CODE
!    SentToARC# = False
!    LOC:ARCDateBooked = ''
!    Access:LOCATLOG.Clearkey(lot:DateKey)
!    lot:RefNumber = in:JOBNumber
!    Set(lot:DateKey,lot:DateKey)
!    Loop 
!        If Access:LOCATLOG.Next()
!            Break
!        End !
!        If lot:RefNumber <> in:JobNumber 
!            Break
!        End !
!        If lot:NewLocation = loc:ARCLocation
!            SentToARC# = True
!            loc:ARCDateBooked = lot:TheDate
!            !MESSAGE('TRUE CONDITION MET')
!        Break
!        End!    
!      
!    End !Loop
!
!    Return SentToARC#
!
!
!
!
!
!
!






!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)! LONG
Hours LONG
    CODE
        !-------------------------------------------
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-------------------------------------------
LoadJOBSE                       PROCEDURE(IN:JobNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Synchronise JOBSE (Jobs Extension) to current JOBS file
        !
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number

        IF Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            RETURN True
        ELSE
            RETURN False
        END !IF
        !-----------------------------------------------
LoadUSERS           PROCEDURE( IN:UserCode )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = IN:UserCode
        IF Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
            RETURN True
        ELSE
            RETURN False
        END !IF
        !-----------------------------------------------
LoadREPTYDEF        PROCEDURE( IN:RepairType )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadREPTYDEF() START')

        Access:REPTYDEF.ClearKey(rtd:Repair_Type_Key)
            rtd:Repair_Type = IN:RepairType
        SET(rtd:Repair_Type_Key, rtd:Repair_Type_Key)
        !-----------------------------------------------
        IF Access:REPTYDEF.NEXT() <> Level:Benign
            WriteDebug('LoadREPTYDEF() 1')
            RETURN False
        END !IF

        IF NOT rtd:Repair_Type = IN:RepairType
            WriteDebug('LoadREPTYDEF() 2')
            RETURN False
        END !IF

        WriteDebug('LoadREPTYDEF() True')
        RETURN True
        !-----------------------------------------------
LoadSUBTRACC                PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
            RETURN True
        ELSE
            RETURN False
        END !IF
        !-----------------------------------------------
LoadTRADEACC                    PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            RETURN True
        ELSE
            RETURN False
        END !IF
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WorkingDaysBetween PROCEDURE  (IN:StartDate,IN:EndDate)!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
    CODE
        !-------------------------------------------
        IF (IN:StartDate > IN:EndDate)
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate)
        END !IF
        !-------------------------------------------
        IF (IN:StartDate = IN:EndDate)
            RETURN Days
        END !IF
        !-------------------------------------------
        Set(defaults)
        access:defaults.next()

        IF def:include_saturday = 'YES'
            DaysPerWeek += 1
        END !IF

        IF def:include_sunday = 'YES'
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        TempDate2 = IN:EndDate

        LOOP
            If (TempDate1 = TempDate2) Then
                RETURN Days
            End !If

            CASE (TempDate1 % 7)
            OF 0
                If def:include_sunday = 'YES'
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If def:include_saturday = 'YES'
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1

        END !LOOP
        !-------------------------------------------
        LOOP
            If (TempDate1 = TempDate2) Then
                RETURN Days
            End !If

            CASE (TempDate2 % 7)
            OF 0
                If def:include_sunday = 'YES'
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If def:include_saturday = 'YES'
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 += -1

        END !LOOP
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7

        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime) ! LONG
DaysBetween   LONG
Hours         LONG
    CODE
        !-------------------------------------------
        DaysBetween = WorkingDaysBetween(StartDate, EndDate)
        Hours       = 0

        IF DaysBetween = 0
            Hours += HoursBetween(StartTime, EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * LOC:WorkingHours

            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        END !IF

        RETURN Hours
        !-------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug  PROCEDURE( IN:Message )
    CODE
        !-------------------------------------------
        !IF NOT debug:Active
            RETURN
        !END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'c:\Debug.ini')
        !-------------------------------------------
!-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW4.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

