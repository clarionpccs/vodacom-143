

   MEMBER('BouncerCheck.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('BOUNCERCHECK001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Main PROCEDURE 

locJobNumber         LONG                                  !
locTotalBouncers     LONG                                  !
QuickWindow          WINDOW('Window'),AT(,,465,264),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT),RESIZE, |
  CENTER,GRAY,IMM,HLP('Main'),SYSTEM
                       BUTTON('&OK'),AT(336,246,52,14),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(389,246,52,14),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                       PROMPT('Job Number:'),AT(5,5),USE(?locJobNumber:Prompt)
                       ENTRY(@n-14),AT(59,6,60,10),USE(locJobNumber),RIGHT(2)
                       LIST,AT(59,33,177,95),USE(?LIST1),VSCROLL,FORMAT('160L(2)|M~Bouncer Job Number~@s40@'),FROM(glo:Queue20)
                       PROMPT('Total Bouncers:'),AT(5,20),USE(?locTotalBouncers:Prompt)
                       ENTRY(@n-14),AT(59,20,60,10),USE(locTotalBouncers),RIGHT(2),FLAT,SKIP,TRN
                       PROMPT('Manufacturer'),AT(261,10),USE(?man:Manufacturer:Prompt),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                       ENTRY(@s30),AT(334,10,124,10),USE(man:Manufacturer),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,UPR,MSG('Manufacturer'),READONLY,TIP('Manufacturer')
                       PROMPT('Bouncer Period:'),AT(261,39),USE(?man:BouncerPeriod:Prompt)
                       PROMPT('Bouncer Period IMEI:'),AT(261,175),USE(?man:BouncerPeriodIMEI:Prompt)
                       LIST,AT(59,134,177,110),USE(?LIST2),VSCROLL,FORMAT('160L(2)|M~Failures~@s40@'),FROM(glo:Queue19)
                       CHECK('Use Bouncer Rules'),AT(333,25),USE(man:UseBouncerRules),VALUE('1','0')
                       CHECK('Do Not Bounce Warranty'),AT(333,191,117,10),USE(man:DoNotBounceWarranty),VALUE('1', |
  '0')
                       BUTTON('Save'),AT(333,215,75),USE(?buttonSave)
                       BUTTON('Refresh'),AT(123,5),USE(?buttonRefresh)
                       GROUP,AT(325,22,99,179),USE(?groupBouncer),TRN
                         ENTRY(@n-14),AT(334,40,60,10),USE(man:BouncerPeriod),RIGHT(1)
                         OPTION('Bouncer Type'),AT(333,54,131,54),USE(man:BouncerType),BOXED
                           RADIO('Date Booked'),AT(347,66),USE(?OPTION1:RADIO1),VALUE('0')
                           RADIO('Date Completed'),AT(347,79,77,10),USE(?OPTION1:RADIO1:2),VALUE('1')
                           RADIO('Date Despatched'),AT(347,91,72,10),USE(?OPTION1:RADIO1:3),VALUE('2')
                         END
                         CHECK('Bounce In Fault'),AT(333,113,79,10),USE(man:BouncerInFault),VALUE('1','0')
                         CHECK('Bounce Out Fault'),AT(333,128,79,10),USE(man:BouncerOutFault),VALUE('1','0')
                         CHECK('Bounce Same Spares'),AT(333,143,79,10),USE(man:BouncerSpares),VALUE('1','0')
                         ENTRY(@n-14),AT(334,174,60,10),USE(man:BouncerPeriodIMEI),RIGHT(1)
                         CHECK('Off (AND) On (OR)'),AT(333,156),USE(man:BounceRulesType),MSG('Bounce Rules Type')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Main',QuickWindow)                         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?man:UseBouncerRules{Prop:Checked}
    ENABLE(?groupBouncer)
  END
  IF NOT ?man:UseBouncerRules{PROP:Checked}
    DISABLE(?groupBouncer)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('Main',QuickWindow)                      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?locJobNumber
      FREE(glo:Queue20)
      FREE(glo:Queue19)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number = locJobNumber
      IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          locTotalBouncers = vod.CountJobBouncers()
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              
          ELSE
          END
          
      ELSE
          locTotalBouncers = 0
      
      END
      DISPLAY()
      Select(?locJobNumber)
      POST(EVENT:Accepted,?man:UseBouncerRules)
    OF ?man:UseBouncerRules
      IF ?man:UseBouncerRules{PROP:Checked}
        ENABLE(?groupBouncer)
      END
      IF NOT ?man:UseBouncerRules{PROP:Checked}
        DISABLE(?groupBouncer)
      END
      ThisWindow.Reset
    OF ?buttonSave
      ThisWindow.Update
      IF (man:Manufacturer <> '')
              Access:MANUFACT.TryUpdate()
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Message('Done.','Saved',|
                  Icon:Asterisk,'&OK',1) 
              Of 1 ! &OK Button
              End!Case Message
          
      END
    OF ?buttonRefresh
      ThisWindow.Update
      Post(EVENT:Accepted,?locJobNumber)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

