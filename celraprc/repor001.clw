

   MEMBER('reportscheduler.clw')                      ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('REPOR001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:NextReportName   STRING(60)
tmp:NextReportCriteria STRING(60)
tmp:NextReportDate   DATE
tmp:NextReportTime   TIME
tmp:Today            DATE
tmp:Clock            TIME
AppFrame             WINDOW('Report Scheduler'),AT(,,223,149),FONT('Tahoma',,,),COLOR(09F677EH),IMM,ICON('Cellular3g.ico'),TIMER(100),GRAY,DOUBLE
                       STRING('SRN: 0000000'),AT(168,4),USE(?SRNNumber),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(4,16,216,128),USE(?Sheet1),SPREAD
                         TAB('Schedule Details'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Machine IP:'),AT(8,36),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(72,36),USE(glo:IPAddress),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Current Time:'),AT(8,46),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@d6),AT(72,46),USE(tmp:Today),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@t1b),AT(136,46),USE(tmp:Clock),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Next Report:'),AT(8,66),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(72,66,134,9),USE(tmp:NextReportName),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Criteria:'),AT(8,78),USE(?Prompt6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(72,78,134,9),USE(tmp:NextReportCriteria),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('To Run:'),AT(8,90),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@d6b),AT(72,90),USE(tmp:NextReportDate),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@t1b),AT(136,90),USE(tmp:NextReportTime),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(148,112),USE(?Close),SKIP,TRN,FLAT,ICON('closep.jpg')
                         END
                         TAB('Running Report'),USE(?Tab2),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Running Report:'),AT(8,42,68,12),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Criteria Type:'),AT(8,80,68,12),USE(?Prompt7:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(8,54,208,14),USE(tmp:NextReportName,,?tmp:NextReportName:2),TRN,CENTER,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(8,92,208,16),USE(tmp:NextReportCriteria,,?tmp:NextReportCriteria:2),TRN,CENTER,FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

GetNextReport       Routine
Data
local:RecordNumber      Long()
Code
    tmp:Today = Today()
    tmp:Clock = Clock()
    tmp:NextReportName = ''
    tmp:NextReportCriteria = ''
    tmp:NextReportDate = 0
    tmp:NextReportTime = 0

    Access:REPSCHED.Clearkey(rpd:ActiveMachineNextDateKey)
    rpd:Active  = 1
    rpd:MachineIP   = Clip(glo:IPAddress)
    rpd:NextReportDate = Today()
    Set(rpd:ActiveMachineNextDateKey,rpd:ActiveMachineNextDateKey)
    Loop ! Begin Loop
        If Access:REPSCHED.Next()
            Break
        End ! If Access:REPSCHED.Next()
        If rpd:Active <> 1
            Break
        End ! If rpd:Active <> 1
        If rpd:MachineIP <> Clip(glo:IPAddress)
            Break
        End ! If rpd:MachineIP <> glo:IPAddress
        If rpd:NextReportDate < Today()
            Break
        End ! If rpd:NextReportDate < Today()
        If rpd:NextReportDate = Today()
            If rpd:NextReportTime <= Clock()
                tmp:NextReportName = rpd:ReportName
                tmp:NextReportCriteria = rpd:ReportCriteriaType
                tmp:NextReportDate = rpd:NextReportDate
                tmp:NextReportTime = rpd:NextReportTime
                local:RecordNumber = rpd:RecordNumber
                Break
            End ! If rpd:NextReportTime < Clock()
        End ! If rpd:NextReportDate = Today()
        tmp:NextReportName = rpd:ReportName
        tmp:NextReportCriteria = rpd:ReportCriteriaType
        tmp:NextReportDate = rpd:NextReportDate
        tmp:NextReportTime = rpd:NextReportTime
        local:RecordNumber = rpd:RecordNumber
        Break
    End ! Loop
    If tmp:NextReportName = ''
        tmp:NextReportName = 'No Report Scheduled'
    Else ! If tmp:NextReportName = ''
        If tmp:NextReportDate = Today() And tmp:NextReportTime < Clock()
            0{prop:Timer} = 0

            ! Run Report
            ?Tab1{prop:Hide} = 1
            ?Tab2{prop:Hide} = 0
            ThisWindow.Reset(1)

            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = local:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(tmp:NextReportName) & ' - ' & Clip(tmp:NextReportCriteria) & |
                                  '<13,10>Machine: ' & Clip(glo:IPAddress) & |
                                  '<13,10>Report Started'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign

            Case tmp:NextReportName
            Of '48 Hour TAT Report'
                RUN('vodr0079.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Bounce Report'
                RUN('vodr0072.exe /SCHEDULE %' & local:RecordNumber,0)   !Changed this so it does not wait for completion - there seems to be an overlap
                                                                         !and it never completed - TB13052 - JC - 19/04/13
            Of 'Booking Performance Report'
                RUN('vodr0003.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Cost Adjustment Report'
                RUN('vodr0052.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Exchange Units Report'
                RUN('vodr0076.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Goods Received Report'
                RUN('vodr0049.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Goods Received Franchises Report'
                RUN('vodr0066.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Handling And Exchange Fee Report'
                RUN('vodr0062.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Income Report (ARC)'
                RUN('vodr0059.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Income Report (Retail)'
                RUN('vodr0046.exe /SCHEDULE %' & local:RecordNumber,1)
! Insert --- Add new report (DBH: 09/03/2009) #10615
            Of 'Monthly Warranty Claim Report'
                RUN('vodr0100.exe /SCHEDULE %' & local:RecordNumber,1)
! end --- (DBH: 09/03/2009) #10615
            Of 'Parts Usage Report'
                RUN('vodr0037.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Repeat IMEI Number Report'
                RUN('vodr0087.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Retail Sales Waybill Report'
                RUN('vodr0091.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Returned Third Party Despatch Report'
                RUN('vodr0082.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'RRC Chargeable Income Report'
                RUN('vodr0063.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'RRC TAT Report'
                RUN('vodr0078.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'RRC Warranty Income Report'
                RUN('vodr0064.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Sent To ARC Report'
                RUN('vodr0089.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Spares Forecasting Report'
                RUN('vodr0083.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Stock Value Report'
                RUN('vodr0095.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Stock Value Detailed Report'
                RUN('vodr0095.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Stock Adjustment Report'
                RUN('vodr0056.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'VCP Performance Report'
                RUN('vodr0088.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Warranty Technical Reports'
                RUN('celraptc.exe /SCHEDULE %' & local:RecordNumber,1)
            Of 'Warranty Rejection Report'
                RUN('vodr0092.exe /SCHEDULE %' & local:RecordNumber,1)
            End ! Case tmp:NextReportName


            Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
            rpd:RecordNumber = local:RecordNumber
            If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
                !Found
                rpd:LastReportDate = Today()
                rpd:LastReportTime = Clock()
                Case rpd:Frequency
                Of 1 !Once
                    rpd:NextReportDate = ''
                    rpd:NextReportTime = ''
                    rpd:Active = 0
                Of 2 !Daily
                    rpd:NextReportDate += 1
                Of 3 !Weekly
                    rpd:NextReportDate += 7
                Of 4 !Monthy
                    rpd:NextReportDate = Date(Month(rpd:NextReportDate) + 1,Day(rpd:NextReportDate),Year(rpd:NextReportDate))
                End ! Case rpd:Frequencey
                Access:REPSCHED.Update()
            Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
                !Error
            End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
            ?Tab1{prop:Hide} = 0
            ?Tab2{prop:Hide} = 1
            Display()

            0{prop:Timer} = 500
        End ! If tmp:NextReportDate = Today() And tmp:NextReportTime < Clock()
    End ! If tmp:NextReportName = ''

    

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020694'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SRNNumber
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  NetOptions(net:gethostiplist,glo:IPAddress)
  NetOptions(net:gethostname,glo:HostName)
  Loop x# = 1 To 30
      If Sub(glo:IPAddress,x#,1) = ' '
          glo:IPAddress = Sub(glo:IPAddress,1,x#-1)
          Break
      End ! If Sub(tmp:IPAddress,x#,1) = ' '
  End ! Loop x# = 1 To 30
  SELF.AddItem(?Close,RequestCancelled)
  Relate:REPSCHED.Open
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  OPEN(AppFrame)
  SELF.Opened=True
  Bryan.CompFieldColour()
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      Channel = DDESERVER('REPORTSCHEDULER') !set program as a DDE Server
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Do GetNextReport
  Display()
  INIMgr.Fetch('Main',AppFrame)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPSCHED.Close
  END
  IF SELF.Opened
    INIMgr.Update('Main',AppFrame)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      IF EVENT() = Event:DDEexecute
          IF DDEVALUE() = 'INFRONT' !tells it to bring itself to the front
              AppFrame{PROP:Iconize} = FALSE
              BringWindowToTop(AppFrame{PROP:Handle}) !and this does it
          END
      END
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      Do GetNextReport
      Display()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
