

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E005.INC'),ONCE        !Local module procedure declarations
                     END


ShowChargeableParts PROCEDURE (SentJobNo)             !Generated from procedure template - Window

JobNo                LONG
BRW3::View:Browse    VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Quantity)
                       PROJECT(par:Sale_Cost)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Part_Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Sale_Cost          LIKE(par:Sale_Cost)            !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Part_Ref_Number    LIKE(par:Part_Ref_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Webmaster Enquiry - Chargeable parts used on job'),AT(,,317,230),GRAY,DOUBLE
                       LIST,AT(14,10,292,185),USE(?List),IMM,MSG('Browsing Records'),FORMAT('80L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@32D(2)|M~Quantity~L@n8@56R(' &|
   '2)|M~Cost Each~L@n14.2@'),FROM(Queue:Browse)
                       BUTTON('Cancel'),AT(242,202,56,16),USE(?CancelButton)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowChargeableParts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:PARTS.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  JobNo = sentjobno
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ShowChargeableParts)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,par:PartRefNoKey)
  BRW3.AddRange(par:Part_Ref_Number,JobNo)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,par:Part_Ref_Number,1,BRW3)
  BRW3.AddField(par:Part_Number,BRW3.Q.par:Part_Number)
  BRW3.AddField(par:Description,BRW3.Q.par:Description)
  BRW3.AddField(par:Quantity,BRW3.Q.par:Quantity)
  BRW3.AddField(par:Sale_Cost,BRW3.Q.par:Sale_Cost)
  BRW3.AddField(par:Record_Number,BRW3.Q.par:Record_Number)
  BRW3.AddField(par:Part_Ref_Number,BRW3.Q.par:Part_Ref_Number)
  BRW3.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PARTS.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)

