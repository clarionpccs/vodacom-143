

   MEMBER('vodr0102.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


ARCToRRCQAFail PROCEDURE                                   ! Generated from procedure template - Window

tmp:VersionNumber    STRING(30)                            !Version Number
local:desktop        CSTRING(255)                          !
tmp:Clipboard        ANY                                   !
StatusQueue          QUEUE,PRE(staque)                     !
StatusMessage        STRING(60)                            !Status Message
                     END                                   !
SummaryQueue         QUEUE,PRE(que)                        !
AccountNumber        STRING(30)                            !
                     END                                   !
TempFilePath         CSTRING(255)                          !
tmp:StartDate        DATE                                  !Jobs Completed From
tmp:EndDate          DATE                                  !Jobs Completed To
tmp:LastDetailColumn STRING(2)                             !Last Detail Column
tmp:LastSummaryColumn STRING(2)                            !Last Summary Column
tmp:UserName         STRING(70)                            !User Name
tmp:HeadAccountNumber STRING(30)                           !Head Account Number
tmp:ARCLocation      STRING(30)                            !ARC Location
Progress:Thermometer BYTE(0)                               !Moving Bar
tmp:ARCAccount       BYTE(0)                               !Reporting on ARC?
tmp:ARCCompanyName   STRING(30)                            !Company Name
tmp:ARCUser          BYTE(0)                               !ARC User
TempFileQueue        QUEUE,PRE(tmpque)                     !
AccountNumber        STRING(30)                            !Account Number
CompanyName          STRING(30)                            !Company Name
Count                LONG                                  !Count
FileName             STRING(255)                           !FileName
JobsBooked           LONG                                  !Jobs Booked
JobsOpen             LONG                                  !Jobs Open
                     END                                   !
tmp:wasexchange      STRING(1)                             !
tmp:Audittype        STRING(30)                            !
tmp:IMEINumber       STRING(30)                            !
tmp:QAPassedName     STRING(50)                            !
tmp:DateQAPassed     DATE                                  !
tmp:Technician       STRING(50)                            !
tmp:technicianDate   DATE                                  !
tmp:TechnicianLocation STRING(50)                          !
tmp:Exchanged        STRING(3)                             !
tmp:FailureReason    STRING(50)                            !
tmp:QAFailedEngineer STRING(50)                            !
tmp:FranchiseID      STRING(30)                            !
TechnicianQueue      QUEUE,PRE(TQ)                         !
RecordNumber         LONG                                  !
UserCode             STRING(10)                            !
                     END                                   !
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(166,70,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('ARC to RRC QA Failures Report'),AT(170,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0207380'),AT(465,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(165,86,352,244),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('QA Fail Date From'),AT(254,198,,11),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(330,198,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           PROMPT('QA Fail Date to'),AT(254,220,,11),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(330,220,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           PROMPT(''),AT(171,106),USE(?StatusText)
                           PROMPT('Ensure Excel is NOT running before you begin!'),AT(237,264,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       PANEL,AT(166,334,352,26),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(376,334),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(448,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(172,342),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(399,194),USE(?Calendar),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(399,217),USE(?Calendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                     END

!Progress Window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow      WINDOW('Progress...'),AT(,,680,428),CENTER,ICON('cellular3g.ico'),FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),IMM,COLOR(00D6EAEFh),Tiled,DOUBLE
                        PANEL, AT(164,68,352,12), USE(?Panel1), FILL(009A6A7Ch)
                        LIST, AT(208,88,268,206), USE(?List1), FONT(,, 00010101h,, CHARSET:ANSI), FORMAT('20L(2)|M'), |
                            COLOR(COLOR:White, COLOR:White, 009A6A7Ch), VSCROLL, GRID(COLOR:WHITE), FROM(StatusQueue)
                        PANEL, AT(164,84,352,246), USE(?Panel4), FILL(009A6A7Ch)
                        PROGRESS, AT(206,314,268,12), USE(progress:thermometer), RANGE(0,100), SMOOTH
                        STRING(''), AT(259,300,161,10), USE(?progress:userstring), FONT('Arial', 8, 0080FFFFh, FONT:bold), |
                            COLOR(009A6A7Ch), CENTER
                        STRING(''), AT(232,136,161,10), USE(?progress:pcttext), FONT('Arial', 8), HIDE, TRN, CENTER
                        PANEL, AT(164,332,352,28), USE(?Panel2), FILL(009A6A7Ch)
                        PROMPT('Report Progress'), AT(168,70), USE(?WindowTitle2), FONT('Tahoma', 8, COLOR:White, FONT:bold, CHARSET:ANSI), |
                            TRN
                        BUTTON, AT(444,332), USE(?ProgressCancel), ICON('Cancelp.jpg'), FLAT, LEFT, TRN
                        BUTTON, AT(376,332), USE(?Button:OpenReportFolder), ICON('openrepp.jpg'), HIDE, FLAT, TRN
                        BUTTON, AT(444,332), USE(?Finish), ICON('Finishp.jpg'), HIDE, FLAT, TRN
                    END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
WriteLine               Procedure()
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                   String(2000)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
progressThermometer Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.UserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

Calendar12           CalendarClass
Calendar13           CalendarClass
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:RecordCount       Long()
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'ARCRRCFailures'

    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')

    glo:ExportFile = Clip(local:LocalPath) & 'ARCRRC' & Clock() & '.CSV'
    Remove(glo:ExportFile)
    Create(ExportFile)
    Open(ExportFile)

    !loop through the audit file for the date specified once for the ARC failures then for the exchange failures

    Access:Audit.Clearkey(aud:DateJobKey)
    aud:Date    = tmp:StartDate
    set(aud:DateJobKey,aud:DateJobKey)
    loop
        if (Access:Audit.Next())
            Break
        end ! if (Access:EXCHHIST.Next())
        if (aud:Date    > tmp:EndDate)
            Break
        end ! if (exh:Date    <> tmp:StartDate)

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        ! Criteria____________________________________________________________________________________
        !check to see if this record is either ARC QA FAILED OR EXCHANGE QA FAILED
        !is it arc qa failed
        If instring('ARC QA FAILED',clip(aud:Action),1,1) then
            !message('ARC QA FAILED audit entry found '& aud:record_number)
                        !check the engineer on this audit entry - if ARC engineer the ignore
                        !first we need to get the job number
                        Access:jobs.clearkey(job:Ref_Number_Key)
                        job:Ref_Number = aud:Ref_Number
                        If access:Jobs.fetch(job:Ref_Number_Key) = level:benign THEN
                                !now open the jobse file - to see the webjob value
                                Access:JOBSE.clearkey(jobe:RefNumberKey)
                                jobe:RefNumber = job:Ref_Number
                                If access:jobse.fetch(jobe:RefNumberKey) = level:benign THEN
                                        !now we need to look at the webjob record
                                        Access:WEBJOB.clearkey(wob:RefNumberKey)
                                        wob:RefNumber = job:Ref_Number
                                        If Access:WEBJOB.Fetch(wob:RefNumberKey) = level:benign THEN
                                                !now look up the user on the audit record
                                                Access:USERS.ClearKey(use:User_Code_Key)
                                                use:User_Code = aud:User
                                                If Access:USERS.fetch(use:User_Code_Key) = level:benign THEN
                                                        !compare the users team with the webjob header account
                                                        If use:Team = wob:HeadAccountNumber THEN
                                                                If jobe:WebJob = 1 THEN
                                                                        !this is an RRC job and technician
                                                                ELSE
                                                                        CYCLE
                                                                END
                                                        END
                                                END
                                        END
                                END
                        END
            tmp:wasexchange = 'N'
        Else
            If instring('EXCHANGE QA FAILED',clip(aud:Action),1,1) then
                !message('exchange audit entry found ' & aud:record_number)
                tmp:wasexchange = 'Y'
            Else
                !message('record ignored ' & aud:record_number)
                cycle
            End !If instring('EXCHANGE QA FAILED',clip(aud:Action),1,1) then
        End !If instring('ARC QA FAILED',clip(aud:Action),1,1) then

        !if we have got this far we have a valid record
        !find the failure reason
        x# = instring(':',clip(aud:Action),1,1)
        tmp:FailureReason = sub(clip(aud:Action),x# + 1, len(aud:Action))

        Local.WriteLine()
        local:RecordCount += 1

    End !Loop TRADEACC

!    Loop
!
!        Do GetNextRecord2
!        Do CancelCheck
!        If tmp:Cancel
!            Break
!        End !If tmp:Cancel = 1
!
!        Access:JOBS.ClearKey(job:Ref_Number_Key)
!        job:Ref_Number = lot:RefNumber
!        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!            !Found
!        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!            !Error
!            Cycle
!        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
!
!        Access:JOBSE.ClearKey(jobe:RefNumberKey)
!        jobe:RefNumber = job:Ref_Number
!        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!            !Found
!        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!            !Error
!            Cycle
!        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)
!        wob:RefNumber = job:Ref_Number
!        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!            !Found
!        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!            !Error
!            Cycle
!        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!
!        ! Criteria____________________________________________________________________________________
!
!        ! Has this account been selected? (DBH: 26/07/2006)
!        glo:Pointer = wob:HeadAccountNumber
!        Get(glo:Queue,glo:Pointer)
!        If Error()
!            Cycle
!        End ! If Error()
!
!        Local.WriteLine()
!        local:RecordCount += 1
!
!    End !Loop TRADEACC
    Close(ExportFile)

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        tmp:LastDetailColumn = 'N'

        SetClipBoard('')
        E1.OpenWorkBook(glo:ExportFile)
        E1.Copy('A1',Clip(tmp:LastDetailColumn) & local:RecordCount)
        tmp:Clipboard = ClipBoard()
        SetClipBoard('Blank')
        E1.CloseWorkBook(3)

        E1.NewWorkBook()
        E1.SelectCells('A12')
        SetClipBoard(tmp:ClipBoard)
        E1.Paste()
        SetClipBoard('Blank')
        E1.RenameWorkSheet(excel:ProgramName)

        Do DrawTitle

        local.DrawBox('A9',Clip(tmp:LastDetailColumn) & '9','A9',Clip(tmp:LastDetailColumn) & '9',color:Silver)
        local.DrawBox('A11',Clip(tmp:LastDetailColumn) & '11','A11',Clip(tmp:LastDetailColumn) & '11',color:Silver)

        E1.WriteToCell('Section Name','A9')
        E1.WriteToCell(excel:ProgramName,'B9')
        E1.WriteToCell('Total Records','E9')
        E1.WriteToCell(local:RecordCount,'F9')
        E1.WriteToCell('Showing','G9')
        E1.WriteToCell('=SUBTOTAL(2,A12:A' & local:RecordCount + 12,'H9')

        ! Define Column Titles (DBH: 01/08/2006)
        E1.WriteToCell('Franchise ID','A11')
        E1.WriteToCell('Job Number','B11')
        E1.WriteToCell('Job Number2','C11')
        E1.WriteToCell('IMEI Number','D11')
        E1.WriteToCell('Model','E11')
        E1.WriteToCell('Manufacturer','F11')
        E1.WriteToCell('QA Controller','G11')
        E1.WriteToCell('Technician','H11')
        E1.WriteToCell('3rd Party Repairer','I11')
        E1.WriteToCell('Exchange','J11')
        E1.WriteToCell('Date QA Failed','K11')
        E1.WriteToCell('QA Failure Reason','L11')
        E1.WriteToCell('Comments Field','M11')
        E1.WriteToCell('Failed By','N11')

        E1.SetCellFontName('Tahoma','A9',Clip(tmp:LastDetailColumn) & local:RecordCount + 12)
        E1.SetCellFontSize(8,'A1',Clip(tmp:LastDetailColumn) & local:RecordCount + 12)
        E1.SetCellFontStyle('Bold','A11',Clip(tmp:LastDetailColumn) & '11')
        E1.SetCellFontStyle('Bold','A9')
        E1.SetCellFontStyle('Bold','F9')
        E1.SetCellFontStyle('Bold','H9')
        E1.SetCellBackgroundColor(color:Silver,'A9',Clip(tmp:LastDetailColumn) & '9')
        E1.SetCellBackgroundColor(color:Silver,'A11',Clip(tmp:LastDetailColumn) & '11')

        E1.SetColumnWidth('A',Clip(tmp:LastDetailColumn))
        E1.SelectCells('B12')
        E1.FreezePanes()

!        ! Create Summary Worksheet (DBH: 01/08/2006)
!        E1.InsertWorksheet()
!        E1.RenameWorkSheet('Summary')
!
!        Do DrawTitle
!
!        local.DrawBox('A9','E9','A9','E9',color:Silver)
!        local.DrawBox('A10','E10','A10','E10',color:Silver)
!
!        E1.WriteToCell('Summary','A9')
!
!        ! Define Summary Column Titles (DBH: 01/08/2006)
!        E1.WriteToCell('Head Account Number','A10')
!        E1.WriteToCell('Head Account Name','B10')
!
!        E1.SelectCells('A11')
!
!        tmp:LastSummaryColumn = 'Z'
!        local:RecordCount = 11
!        Sort(SummaryQueue,que:AccountNumber)
!        Loop x# = 1 To Records(SummaryQueue)
!            Get(SummaryQueue,x#)
!            local:RecordCount += 1
!        End ! Loop x# = 1 To Records(SummaryQueue)
!
!        local.DrawBox('A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,'A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,color:Silver)
!        E1.SetCellFontName('Tahoma','A9',Clip(tmp:LastSummaryColumn) & local:RecordCount)
!        E1.SetCellFontStyle('Bold','A9',Clip(tmp:LastSummaryColumn) & '10')
!        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)
!        E1.SetCellFontSize(8,'A1',Clip(tmp:LastSummaryColumn) & local:RecordCount)
!        E1.SetColumnWidth('A',Clip(tmp:LastSummaryColumn))
!        E1.SelectCells('A11')

        E1.SaveAs(excel:FileName)
        E1.CloseWorkBook(3)
        E1.Kill()

        ! Use alternative method to assign autofilters (DBH: 31/07/2006)
        ExcelSetup(0)
        ExcelOpenDoc(excel:FileName)
        ExcelSelectSheet(excel:ProgramName)
        ExcelAutoFilter('A11:' & CLip(tmp:LastDetailColumn) & '11')
        ExcelAutoFit('A:' & CLip(tmp:LastDetailCOlumn))
        ExcelSelectRange('B12')
!        ExcelSelectSheet('Summary')
!        ExcelAutoFilter('A10:' & CLip(tmp:LastSummaryCOlumn) & '10')
!        ExcelAutoFit('A:' & CLip(tmp:LastSummaryColumn))
!        ExcelSelectRange('A11')
        ExcelSaveWorkBook(excel:FileName)
        ExcelClose()

        local.UpdatePRogressWindow('Finishing Off Formatting..')


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(excel:ProgramName, 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A7','D7',color:Silver)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')



getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
        percentprogress = (recordsprocessed / recordstoprocess)*100
        if percentprogress >= 100
            recordsprocessed        = 0
            percentprogress         = 0
            progress:thermometer    = 0
            recordsthiscycle        = 0
        end
        if percentprogress <> progress:thermometer then
            progress:thermometer = percentprogress
            ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
        Of Event:Timer
            Break
        Of Event:CloseWindow
            cancel# = 1
            Break
        Of Event:accepted
            Yield()
            If Field() = ?ProgressCancel
                cancel# = 1
                Break
            End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
            'mquest.jpg','\Cancel|Quit|/Finish')
        Of 3 ! Finish Button
            tmp:Cancel = 2
        Of 2 ! Quit Button
            tmp:Cancel = 1
        Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ARCToRRCQAFail')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:Audit_Alias.Open                                  ! File Audit_Alias used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSENG.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDIT2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  
  !Get the site location of the head office for ARC engineers
  
  tmp:TechnicianLocation = GETINI('BOOKING','HeadSiteLocation',,Clip(Path()) & '\SB2KDEF.INI')
  
  
  
  SELF.Open(window)                                        ! Open window
  ! ================ Set Report Version =====================
  include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5002'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  INIMgr.Fetch('ARCToRRCQAFail',window)                    ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:Audit_Alias.Close
    Relate:EXCHANGE.Close
    Relate:SRNTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('ARCToRRCQAFail',window)                 ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Print
      ThisWindow.Update
      Do Reporting
    OF ?Calendar
      ThisWindow.Update
      Calendar12.SelectOnClose = True
      Calendar12.Ask('Select a Date',tmp:StartDate)
      IF Calendar12.Response = RequestCompleted THEN
      tmp:StartDate=Calendar12.SelectedDate
      DISPLAY(?tmp:StartDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar13.SelectOnClose = True
      Calendar13.Ask('Select a Date',tmp:EndDate)
      IF Calendar13.Response = RequestCompleted THEN
      tmp:EndDate=Calendar13.SelectedDate
      DISPLAY(?tmp:EndDate)
      END
      ThisWindow.Reset(True)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WriteLine     Procedure()
locQAFailedDate date()
Code
    Clear(exp:Record)

    !write the information out to the file here
    !look up the jobs record first
    access:jobs.clearkey(job:Ref_Number_Key)
    job:Ref_Number = aud:Ref_Number
    If access:Jobs.fetch(job:Ref_Number_Key) = level:notify then
        !error
    End !If access:Jobs.fetch(job:Ref_Number_Key) = level:notify then

    !lookup the webjob
    Access:WebJob.Clearkey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    if Access:WebJob.Fetch(wob:RefNumberKey) = level:benign then
        tmp:FranchiseID = wob:HeadAccountNumber
    Else
        tmp:FranchiseID = 'N/A'
    End

    !look up the replacement job no
    access:Audit_Alias.clearkey(AUD1:Ref_Number_Key)
    AUD1:Ref_Number = job:Ref_Number
    set(AUD1:Ref_Number_Key,AUD1:Ref_Number_Key)
    loop
        If access:Audit_Alias.next() then
            break
        End !If access:Audit_Alias.next() then
        If AUD1:Ref_Number <> job:Ref_Number then
            break
        End !If AUD1:Ref_Number <> job:Ref_Number then
        !now check if any of these are new number entries
        If tmp:wasexchange = 'Y' then
            tmp:Audittype = 'EXCHANGE REBOOKED'
        Else
            tmp:Audittype = 'JOB REBOOKED'
        End !If tmp:wasexchange = 'Y' then

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud1:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF

        If clip(AUD1:Action) = clip(tmp:Audittype) then
            !find the new job number in the notes field
            x# = instring('=',clip(AUD2:Notes),1,1)
            newnumber" = sub(clip(AUD2:Notes),x# + 2,len(clip(AUD2:Notes)))
            break
        End !If clip(AUD1:Action) = clip(tmp:Audittype) then
    End

    !was there an exchange unit on the job?
    If tmp:wasexchange = 'Y' then
        !yes - so get the IMEI number from the exchange
        Access:Exchange.clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        If Access:Exchange.fetch(xch:Ref_Number_Key) = level:benign then
            tmp:IMEINumber = clip(xch:ESN)
        Else
            tmp:IMEINumber = 'N/A'
        End !If Access:Exchange.fetch(xch:Ref_Number_Key) = level:benign then
    Else
        !no - so the IMEI number is the one on the job
        tmp:IMEINumber = clip(job:ESN)
    End !If tmp:wasexchange = 'Y' then

    !Look up Person Who QA'd at ARC
    Free(TechnicianQueue)
    clear(TechnicianQueue)

    tmp:QAPassedName = ''
    If tmp:wasexchange = 'Y' then
        !look for exchange qa
        access:Audit_Alias.clearkey(AUD1:Ref_Number_Key)
        AUD1:Ref_Number = job:Ref_Number
        set(AUD1:Ref_Number_Key,AUD1:Ref_Number_Key)
        loop
            If access:Audit_Alias.next() then
                break
            End !If access:Audit_Alias.next() then
            If AUD1:Ref_Number <> job:Ref_Number then
                break
            End !If AUD1:Ref_Number <> job:Ref_Number then

            if (instring('EXCHANGE QA FAILED',clip(AUD1:action),1,1) and locQAFailedDate = 0)
                ! #11550 Record the RRC engineer who QA failed the job (DBH: 12/08/2010)
                access:Users.clearkey(use:User_Code_Key)
                use:User_Code = aud:User
                If access:Users.fetch(use:User_Code_Key) = level:benign then
                    !ok we have the user
                    !now check to see if he is not in the ARC location
                    If use:Location <> clip(tmp:TechnicianLocation) then
                        !this engineer is not an ARC engineer - so use this
                        locQAFailedDate = aud:Date
                    End
                End !If access:Users.fetch(use:User_Code_Key) = level:benign then
            END
            If AUD1:Type <> 'EXC' then
                cycle
            End !If AUD1:Type <> 'JOB' then
            !ok - this is an entry for a job within the right job number - so check if it is 'manual qa passed'

            If Instring('MANUAL QA PASSED', clip(AUD1:Action),1,1) then
                !add this to the queue - as there may be more than 1 and we need to get the most recent from the ARC
                TQ:RecordNumber = AUD1:record_number
                TQ:UserCode     = AUD1:User
                Add(TechnicianQueue)
            End !If Instring('QA PASSED', clip(AUD1:Notes),1,1) then



        End


        !we should now have queue entries - so lets sort them and look up the users
        Sort(TechnicianQueue,-TQ:RecordNumber)
        loop x# = 1 to records(TechnicianQueue)
            get(TechnicianQueue,x#)
            if error() then
                break
            End
            !check if this is the last ARC engineer
            access:Users.clearkey(use:User_Code_Key)
            use:User_Code = clip(TQ:UserCode)
            If access:Users.fetch(use:User_Code_Key) = level:benign then
                !ok we have the user
                !now check to see if he is in the ARC location
                If use:Location = clip(tmp:TechnicianLocation) then
                    !this engineer is an ARC engineer - so use this
                    tmp:QAPassedName = clip(use:Forename) & ' ' & clip(use:Surname)
                    break
                End
            End !If access:Users.fetch(use:User_Code_Key) = level:benign then
        End
    Else
        !look for normal QA
        access:Audit_Alias.clearkey(AUD1:Ref_Number_Key)
        AUD1:Ref_Number = job:Ref_Number
        set(AUD1:Ref_Number_Key,AUD1:Ref_Number_Key)
        loop
            If access:Audit_Alias.next() then
                break
            End !If access:Audit_Alias.next() then
            If AUD1:Ref_Number <> job:Ref_Number then
                break
            End !If AUD1:Ref_Number <> job:Ref_Number then
            If AUD1:Type <> 'JOB' then
                cycle
            End !If AUD1:Type <> 'JOB' then
            !ok - this is an entry for a job within the right job number - so check if it is 'manual qa passed'
            If Instring('MANUAL QA PASSED', clip(AUD1:Action),1,1) then
                !add this to the queue - as there may be more than 1 and we need to get the most recent from the ARC
                TQ:RecordNumber = AUD1:record_number
                TQ:UserCode     = AUD1:User
                Add(TechnicianQueue)
            End !If Instring('QA PASSED', clip(AUD1:Notes),1,1) then

        End

        !we should now have queue entries - so lets sort them and look up the users
        Sort(TechnicianQueue,-TQ:RecordNumber)
        loop x# = 1 to records(TechnicianQueue)
            get(TechnicianQueue,x#)
            if error() then
                break
            End
            !check if this is the last ARC engineer
            access:Users.clearkey(use:User_Code_Key)
            use:User_Code = clip(TQ:UserCode)
            If access:Users.fetch(use:User_Code_Key) = level:benign then
                !ok we have the user
                !now check to see if he is in the ARC location
                If use:Location = clip(tmp:TechnicianLocation) then
                    !this engineer is an ARC engineer - so use this
                    tmp:QAPassedName = clip(use:Forename) & ' ' & clip(use:Surname)
                    break
                End
            End !If access:Users.fetch(use:User_Code_Key) = level:benign then
        End

    End


    !Look Up the last Technician on the job
    Free(TechnicianQueue)
    clear(TechnicianQueue)

    tmp:Technician = ''
    Access:jobsEng.clearkey(joe:JobNumberKey)
    joe:JobNumber = job:Ref_Number
    set(joe:JobNumberKey,joe:JobNumberKey)
    loop
        If access:JobsEng.Next() then
            break
        End !If access:JobsEng.previous() then
        If joe:JobNumber <> job:Ref_Number then
            break
        End !If joe:JobNumber <> job:Ref_Number then
        !key isnt working as required
        !so we are writing the entries out to a ques for further sorting
        TQ:RecordNumber = joe:RecordNumber
        TQ:UserCode     = joe:UserCode
        Add(TechnicianQueue)
    End

    Sort(TechnicianQueue,-TQ:RecordNumber)
    loop x# = 1 to records(TechnicianQueue)
        get(TechnicianQueue,x#)
        if error() then
            break
        End
        !check if this is the last ARC engineer
        access:Users.clearkey(use:User_Code_Key)
        use:User_Code = clip(TQ:UserCode)
        If access:Users.fetch(use:User_Code_Key) = level:benign then
            !ok we have the user
            !now check to see if he is in the ARC location
            If use:Location = clip(tmp:TechnicianLocation) then
                !this engineer is an ARC engineer - so use this
                tmp:Technician = clip(use:Forename) & ' ' & clip(use:Surname)
                break
            End
        End !If access:Users.fetch(use:User_Code_Key) = level:benign then
    End

    !exchanged Yes/No
    If job:Exchange_Unit_Number > 0 then
        tmp:Exchanged = 'YES'
    Else
        tmp:Exchanged = 'NO'
    End !If job:Exchange_Unit_Number > 0 then

    !find the engineers notes
    Access:JobNotes.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    If Access:jobNotes.fetch(jbn:RefNumberKey) = level:notify then
        !error
    End

    !Engineer who failed the job
    Access:Users.clearkey(use:User_Code_Key)
    use:User_Code = aud:User
    If Access:Users.fetch(use:User_Code_Key) = level:benign then
        tmp:QAFailedEngineer = clip(use:Forename) & ' ' & clip(use:Surname)
    Else
        tmp:QAFailedEngineer = 'N/A'
    End !If Access:Users.fetch(use:User_Code_Key) = level:benign then


    !what do we need written
    !Franchise ID
    exp:Line1 = '"' & Clip(tmp:FranchiseID)
    !Job Number - original
    exp:Line1 = Clip(exp:Line1) & '","' & job:Ref_Number
    !Job Number2 - new job no
    exp:Line1 = Clip(exp:Line1) & '","' & clip(newnumber")
    !IMEI number (exch  IMEI if it is)
    exp:Line1 = Clip(exp:Line1) & '","' & '''' &clip(tmp:IMEINumber)
    !Model
    exp:Line1 = Clip(exp:Line1) & '","' & clip(job:Model_Number)
    !Manufacturer
    exp:Line1 = Clip(exp:Line1) & '","' & clip(job:Manufacturer)
    !QA Controller
    exp:Line1 = Clip(exp:Line1) & '","' & clip(tmp:QAPassedName)
    !Technician
    exp:Line1 = Clip(exp:Line1) & '","' & clip(tmp:Technician)
    !3rd Party Repairer - if any
    exp:Line1 = Clip(exp:Line1) & '","' & clip(job:Third_Party_Site)
    !Exchanged - Yes/No
    exp:Line1 = Clip(exp:Line1) & '","' & tmp:Exchanged
    !Date QA Failed
    exp:Line1 = Clip(exp:Line1) & '","' & Format(locQAFailedDate,@d06b)
    !QA Failure Reason
    exp:Line1 = Clip(exp:Line1) & '","' & clip(tmp:FailureReason)
    !Comments
    exp:Line1 = Clip(exp:Line1) & '","' & clip(jbn:Engineers_Notes)
    !Failed By
    exp:Line1 = Clip(exp:Line1) & '","' & clip(tmp:QAFailedEngineer)

    exp:Line1 = Clip(exp:Line1) & '"'



    Add(ExportFile)
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
Window
Prog.ProgressSetup        Procedure(Long func:Records)
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
Code
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}
        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.percentText = '0% Completed'

Prog.InsideLoop         Procedure(<String func:String>)
Code
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
    Prog.UserText = Clip(func:String)
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWinow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Prog.ProgressThermometer = 100
    Prog.PercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
    Code
    Yield()
    Prog.RecordsProcessed += 1
    If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    End
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0

XFiles PROCEDURE                                           ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles',QuickWindow)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles',QuickWindow)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

Missive PROCEDURE (SentMessage,SentTitle,SentIcon,SentButtons) ! Generated from procedure template - Window

MissiveText          STRING(255)                           !
IconName             STRING(20)                            !
FullButtonText       STRING(60)                            !
ButtonText           STRING(10),DIM(6)                     !
ButtonCount          SHORT                                 !
ButtonBreak          SHORT                                 !
RetValue             BYTE                                  !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
window               WINDOW('Caption'),AT(,,408,95),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6E7EFH),CENTER,GRAY,DOUBLE
                       BUTTON('Button 6'),AT(20,69,63,22),USE(?Button6),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 5'),AT(84,69,63,22),USE(?Button5),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 4'),AT(148,69,63,22),USE(?Button4),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 3'),AT(212,69,63,22),USE(?Button3),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 2'),AT(276,69,63,22),USE(?Button2),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 1'),AT(340,69,63,22),USE(?Button1),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI),DEFAULT
                       PANEL,AT(4,1,400,11),USE(?Panel3),FILL(09A6A7CH)
                       STRING('this string is changed on entry to the procedure - it is known as the stringtitl' &|
   'e.'),AT(12,2),USE(?StringTitle),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,13,400,52),USE(?Panel1),FILL(09A6A7CH)
                       IMAGE,AT(12,15,48,44),USE(?Image1)
                       TEXT,AT(68,16,332,46),USE(MissiveText),SKIP,VSCROLL,FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                       PANEL,AT(4,69,400,24),USE(?Panel2),FILL(09A6A7CH)
                       IMAGE('MPurp.jpg'),AT(20,70,63,22),USE(?ButtonImage6)
                       IMAGE('MPurp.jpg'),AT(84,70,63,22),USE(?ButtonImage5)
                       IMAGE('MPurp.jpg'),AT(148,70,63,22),USE(?ButtonImage4)
                       IMAGE('MPurp.jpg'),AT(212,70,63,22),USE(?ButtonImage3)
                       IMAGE('MPurp.jpg'),AT(276,70,63,22),USE(?ButtonImage2)
                       IMAGE('MPurp.jpg'),AT(340,70,63,22),USE(?ButtonImage1)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(RetValue)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Missive')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button6
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(window)                                        ! Open window
  !set up the window from sent variables
  ?StringTitle{Prop:Text}   = SentTitle
  IconName       = SentIcon
  if IconName    = '' then IconName = 'MBlank.jpg'.
  FullButtonText = SentButtons
  
  !sort out the Missive Text
  y# = 1
  loop x# = 1 to len(clip(SentMessage))
      if SentMessage[x#] = '|' then
          MissiveText[y#] = chr(13)
          y#+=1
          MissiveText[y#] = chr(10)
      ELSE
          MissiveText[y#]=SentMessage[x#]
      END
      y#+=1
  END !Loop through sent message
  
  if x# = y# then
      !there were no line breaks if they match
      !Add one to the start
      missiveText=chr(13)&chr(10)&MissiveText
  END !if x = y
  
  
  !Sort out the button texts
  ButtonCount = 0
  loop
      !Look for the divider
      ButtonBreak = instring('|',FullButtonText,1,1)
      if ButtonBreak = 0 then
          !No more buttons - just the last one to do
          ButtonCount += 1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount]=Clip(FullButtonText)
          Break  !From the loop
      ELSE
          !this button and another exists
          ButtonCount +=1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount] = FullButtonText[1:ButtonBreak-1]
          FullButtonText = FullButtonText[ButtonBreak+1:len(clip(FullButtonText))]
      END
  End!Loop through button text
  
  
  
  !Sort out the buttons
  !there must always be one!
  Case buttonText[1,1]
      of '/'
          ?ButtonImage1{prop:text} = 'Mgreen.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      of '\'
          ?ButtonImage1{prop:text} = 'Mred.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      else
          ?Button1{Prop:text}=ButtonText[1]
  END !Case
  
  if clip(buttonText[2]) = '' then
      hide(?Button2)
      ?ButtonImage2{prop:Text}='Blank.jpg'
  ELSE
      Case buttonText[2,1]
          of '/'
              ?ButtonImage2{prop:text} = 'Mgreen.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          of '\'
              ?ButtonImage2{prop:text} = 'Mred.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          else
              ?Button2{Prop:text}=ButtonText[2]
      END !Case
  END
  
  if clip(buttonText[3]) = '' then
      hide(?Button3)
      ?ButtonImage3{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[3,1]
          of '/'
              ?ButtonImage3{prop:text} = 'Mgreen.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          of '\'
              ?ButtonImage3{prop:text} = 'Mred.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          else
              ?Button3{Prop:text}=ButtonText[3]
      END !Case
  END
  
  if clip(buttonText[4]) = '' then
      hide(?Button4)
      ?ButtonImage4{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[4,1]
          of '/'
              ?ButtonImage4{prop:text} = 'Mgreen.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          of '\'
              ?ButtonImage4{prop:text} = 'Mred.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          else
              ?Button4{Prop:text}=ButtonText[4]
      END !Case
  END
  
  if clip(buttonText[5]) = '' then
      hide(?Button5)
      ?ButtonImage5{prop:text}='blank.jpg'
  ELSE
      Case buttonText[5,1]
          of '/'
              ?ButtonImage5{prop:text} = 'Mgreen.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          of '\'
              ?ButtonImage5{prop:text} = 'Mred.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          else
              ?Button5{Prop:text}=ButtonText[5]
      END !Case
  END
  
  if clip(buttonText[6]) = '' then
      hide(?Button6)
      ?ButtonImage6{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[6,1]
          of '/'
              ?ButtonImage6{prop:text} = 'Mgreen.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          of '\'
              ?ButtonImage6{prop:text} = 'Mred.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          else
              ?Button6{Prop:text}=ButtonText[6]
      END !Case
  
  END
  ?image1{prop:text}= IconName
  
  thiswindow.update()
  display()
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  INIMgr.Fetch('Missive',window)                           ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Missive',window)                        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
      RetValue = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button6
      ThisWindow.Update
      RetValue = 6
       POST(Event:CloseWindow)
    OF ?Button5
      ThisWindow.Update
      RetValue = 5
       POST(Event:CloseWindow)
    OF ?Button4
      ThisWindow.Update
      RetValue = 4
       POST(Event:CloseWindow)
    OF ?Button3
      ThisWindow.Update
      RetValue = 3
       POST(Event:CloseWindow)
    OF ?Button2
      ThisWindow.Update
      RetValue = 2
       POST(Event:CloseWindow)
    OF ?Button1
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      select(?Button1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('HelpBrowser',Window)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('HelpBrowser',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


