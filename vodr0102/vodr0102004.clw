

   MEMBER('vodr0102.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0102004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Missive PROCEDURE (SentMessage,SentTitle,SentIcon,SentButtons)

MissiveText          STRING(255)                           !
IconName             STRING(20)                            !
FullButtonText       STRING(60)                            !
ButtonText           STRING(10),DIM(6)                     !
ButtonCount          SHORT                                 !
ButtonBreak          SHORT                                 !
RetValue             BYTE                                  !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
window               WINDOW('Caption'),AT(,,408,95),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),DOUBLE,CENTER,COLOR(00D6E7EFh), |
  GRAY
                       BUTTON('Button 6'),AT(20,69,63,22),USE(?Button6),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI), |
  FLAT,TRN
                       BUTTON('Button 5'),AT(84,69,63,22),USE(?Button5),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI), |
  FLAT,TRN
                       BUTTON('Button 4'),AT(148,69,63,22),USE(?Button4),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI), |
  FLAT,TRN
                       BUTTON('Button 3'),AT(212,69,63,22),USE(?Button3),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI), |
  FLAT,TRN
                       BUTTON('Button 2'),AT(276,69,63,22),USE(?Button2),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI), |
  FLAT,TRN
                       BUTTON('Button 1'),AT(340,69,63,22),USE(?Button1),FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI), |
  DEFAULT,FLAT,TRN
                       PANEL,AT(4,1,400,11),USE(?Panel3),FILL(009A6A7Ch)
                       STRING('this string is changed on entry to the procedure - it is known as the stringtitle.'), |
  AT(12,2),USE(?StringTitle),FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),TRN
                       PANEL,AT(4,13,400,52),USE(?Panel1),FILL(009A6A7Ch)
                       IMAGE,AT(12,15,48,44),USE(?Image1)
                       TEXT,AT(68,16,332,46),USE(MissiveText),FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  VSCROLL,COLOR(COLOR:White),READONLY,SKIP
                       PANEL,AT(4,69,400,24),USE(?Panel2),FILL(009A6A7Ch)
                       IMAGE('MPurp.jpg'),AT(20,70,63,22),USE(?ButtonImage6)
                       IMAGE('MPurp.jpg'),AT(84,70,63,22),USE(?ButtonImage5)
                       IMAGE('MPurp.jpg'),AT(148,70,63,22),USE(?ButtonImage4)
                       IMAGE('MPurp.jpg'),AT(212,70,63,22),USE(?ButtonImage3)
                       IMAGE('MPurp.jpg'),AT(276,70,63,22),USE(?ButtonImage2)
                       IMAGE('MPurp.jpg'),AT(340,70,63,22),USE(?ButtonImage1)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(RetValue)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Missive')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button6
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(window)                                        ! Open window
  !set up the window from sent variables
  ?StringTitle{Prop:Text}   = SentTitle
  IconName       = SentIcon
  if IconName    = '' then IconName = 'MBlank.jpg'.
  FullButtonText = SentButtons
  
  !sort out the Missive Text
  y# = 1
  loop x# = 1 to len(clip(SentMessage))
      if SentMessage[x#] = '|' then
          MissiveText[y#] = chr(13)
          y#+=1
          MissiveText[y#] = chr(10)
      ELSE
          MissiveText[y#]=SentMessage[x#]
      END
      y#+=1
  END !Loop through sent message
  
  if x# = y# then
      !there were no line breaks if they match
      !Add one to the start
      missiveText=chr(13)&chr(10)&MissiveText
  END !if x = y
  
  
  !Sort out the button texts
  ButtonCount = 0
  loop
      !Look for the divider
      ButtonBreak = instring('|',FullButtonText,1,1)
      if ButtonBreak = 0 then
          !No more buttons - just the last one to do
          ButtonCount += 1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount]=Clip(FullButtonText)
          Break  !From the loop
      ELSE
          !this button and another exists
          ButtonCount +=1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount] = FullButtonText[1:ButtonBreak-1]
          FullButtonText = FullButtonText[ButtonBreak+1:len(clip(FullButtonText))]
      END
  End!Loop through button text
  
  
  
  !Sort out the buttons
  !there must always be one!
  Case buttonText[1,1]
      of '/'
          ?ButtonImage1{prop:text} = 'Mgreen.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      of '\'
          ?ButtonImage1{prop:text} = 'Mred.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      else
          ?Button1{Prop:text}=ButtonText[1]
  END !Case
  
  if clip(buttonText[2]) = '' then
      hide(?Button2)
      ?ButtonImage2{prop:Text}='Blank.jpg'
  ELSE
      Case buttonText[2,1]
          of '/'
              ?ButtonImage2{prop:text} = 'Mgreen.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          of '\'
              ?ButtonImage2{prop:text} = 'Mred.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          else
              ?Button2{Prop:text}=ButtonText[2]
      END !Case
  END
  
  if clip(buttonText[3]) = '' then
      hide(?Button3)
      ?ButtonImage3{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[3,1]
          of '/'
              ?ButtonImage3{prop:text} = 'Mgreen.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          of '\'
              ?ButtonImage3{prop:text} = 'Mred.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          else
              ?Button3{Prop:text}=ButtonText[3]
      END !Case
  END
  
  if clip(buttonText[4]) = '' then
      hide(?Button4)
      ?ButtonImage4{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[4,1]
          of '/'
              ?ButtonImage4{prop:text} = 'Mgreen.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          of '\'
              ?ButtonImage4{prop:text} = 'Mred.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          else
              ?Button4{Prop:text}=ButtonText[4]
      END !Case
  END
  
  if clip(buttonText[5]) = '' then
      hide(?Button5)
      ?ButtonImage5{prop:text}='blank.jpg'
  ELSE
      Case buttonText[5,1]
          of '/'
              ?ButtonImage5{prop:text} = 'Mgreen.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          of '\'
              ?ButtonImage5{prop:text} = 'Mred.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          else
              ?Button5{Prop:text}=ButtonText[5]
      END !Case
  END
  
  if clip(buttonText[6]) = '' then
      hide(?Button6)
      ?ButtonImage6{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[6,1]
          of '/'
              ?ButtonImage6{prop:text} = 'Mgreen.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          of '\'
              ?ButtonImage6{prop:text} = 'Mred.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          else
              ?Button6{Prop:text}=ButtonText[6]
      END !Case
  
  END
  ?image1{prop:text}= IconName
  
  thiswindow.update()
  display()
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  INIMgr.Fetch('Missive',window)                           ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Missive',window)                        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
      RetValue = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button6
      ThisWindow.Update
      RetValue = 6
       POST(EVENT:CloseWindow)
    OF ?Button5
      ThisWindow.Update
      RetValue = 5
       POST(EVENT:CloseWindow)
    OF ?Button4
      ThisWindow.Update
      RetValue = 4
       POST(EVENT:CloseWindow)
    OF ?Button3
      ThisWindow.Update
      RetValue = 3
       POST(EVENT:CloseWindow)
    OF ?Button2
      ThisWindow.Update
      RetValue = 2
       POST(EVENT:CloseWindow)
    OF ?Button1
      ThisWindow.Update
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      select(?Button1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!!! <summary>
!!! Generated from procedure template - Window
!!! Doesn't need a SRN, as this *IS* the help screen
!!! </summary>
HelpBrowser PROCEDURE (func:Path)

fepath               STRING(255)                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),RESIZE, |
  TILED,COLOR(009A6A7Ch),GRAY,MAX,SYSTEM,IMM
                       BUTTON,AT(0,0),USE(?Button1),ICON('closep.jpg'),FLAT,STD(STD:Close),TRN
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI), |
  TRN
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END


                     !Start: FE Class Declaration
ThisViewer1          CLASS(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('HelpBrowser',Window)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('HelpBrowser',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue

