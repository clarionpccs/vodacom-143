

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3007.INC'),ONCE        !Local module procedure declarations
                     END


NextWaybillNumber    PROCEDURE                             ! Declare Procedure
locKeyField          LONG                                  !
locWaybillNumber     LONG                                  !
WAYBILLS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    DO SaveFiles
    
    ! Remove blanks
    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = 0
    SET(way:WayBillNumberKey,way:WayBillNumberKey)
    LOOP UNTIL Access:WAYBILLS.Next()
        IF (way:WayBillNumber <> 0)
            BREAK
        END
        Relate:WAYBILLS.delete(0)
    END
    
    locWaybillNumber = 0
    locKeyField = 0
    LOOP
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = 99999999
        SET(way:WayBillNumberKey,way:WayBillNumberKey)
        LOOP 
            IF (Access:WAYBILLS.Previous())
                locKeyfield = 1
                BREAK
            END
            locKeyfield = way:WayBillNumber + 1
            BREAK
        END
        
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locKeyField
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
            ! Double check the number doesn't already exists
            IF locKeyField = 1
                
                BREAK
            END
            CYCLE
        ELSE
            ! Doesn't exist. Add it
            IF (Access:WAYBILLS.PrimeRecord() = Level:Benign)
                way:WayBillNumber = locKeyField
                IF (Access:WAYBILLS.TryInsert() = Level:Benign)
                    locWaybillNumber = way:WayBillNumber
                    BREAK
                ELSE
                    Access:WAYBILLS.CancelAutoInc()
                END
            END
        END
       
    END
    
    DO RestoreFiles
    DO CloseFiles
    
    RETURN locWayBillNumber
    
    
SaveFiles  ROUTINE
  WAYBILLS::State = Access:WAYBILLS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF WAYBILLS::State <> 0
    Access:WAYBILLS.RestoreFile(WAYBILLS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:WAYBILLS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WAYBILLS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:WAYBILLS.Close
     FilesOpened = False
  END
CreateNewBatch       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locExistingBatchNumber LONG                                !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESP_ALIAS::State  USHORT
JOBSE::State  USHORT
WAYBILLS::State  USHORT
COURIER::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
MULDESPJ::State  USHORT
WAYBILLJ::State  USHORT
MULDESP::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('CreateNewBatch')
  loc:formname = 'CreateNewBatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('CreateNewBatch','')
    p_web._DivHeader('CreateNewBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CreateNewBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESP_ALIAS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESP_ALIAS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('CreateNewBatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CreateNewBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.SSV('Hide:DespatchNoteButton',1)
  p_web.SSV('Hide:CreateInvoiceButton',1)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
      mulj:JobNumber = job:Ref_Number
      IF (Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey))
          ! only carry on if job is not already on batch
          ! this should cope if the user presses refresh on the window
      
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
          END
          
  
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = job:Account_Number
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  ! Invoice At Despatch Bit??
                  IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                  ELSE
                  END
          
              END
      
          END
  
          IF (job:Warranty_Job = 'YES')
              IF (tra:Use_Sub_Accounts = 'YES')
                  IF (sub:Print_Despatch_Despatch = 'YES')
                      IF (sub:Despatch_Note_Per_Item = 'YES')
                          p_web.SSV('Hide:DespatchNoteButton',0)
                      END
                  END
          
              ELSE
                  IF (tra:Print_Despatch_Despatch = 'YES')
                      IF (tra:Despatch_Note_Per_Item = 'YES')
                          p_web.SSV('Hide:DespathNoteButton',0)
                      END 
                  END
                  
              END
          END
  
          ! Add To Batch
          IF (Access:MULDESP.PrimeRecord() = Level:Benign)
              muld:BatchType = 'SUB'
              IF (jobe:Sub_Sub_Account = '')
                  muld:AccountNumber = job:Account_Number
              ELSE
                  muld:AccountNumber = jobe:Sub_Sub_Account
              END
              IF (p_web.GSV('BookingSite') <> 'RRC' AND jobe:WebJob)
                  IF (sub:Generic_Account AND p_web.GSV('BookingSite') <> 'RRC')
                      muld:AccountNumber = wob:HeadAccountNumber    
                      muld:BatchType = 'TRA'
                  END
                  
              ELSE
              END
              
              CASE p_web.GSV('DespatchType')
              OF 'JOB'
                  muld:Courier = job:Courier
              OF 'EXC'
                  muld:Courier = job:Exchange_Courier
              OF 'LOA'
                  muld:Courier = job:Loan_Courier
              END
              
              If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1 and p_web.GSV('BookingSite') <> 'RRC'
                  !Is this for a Virtual Site?
                  If ~sub:Generic_Account
                      If (vod.RemoteAccount(job:Account_Number))
                          muld:BatchType = 'TRA'
                          muld:AccountNumber = tra:Account_Number
                          muld:Courier        = tra:Courier_Outgoing
                      End !If VirualAccount(job:Account_Number)
                  End !If ~sub:Generic_Account            
              END
              muld:BatchTotal = 1
              !Allocate a Batch Number
              !Count between 1 to 1000, (that should be enough)
              !and if I can't find a Batch with that batch number, then
              !assign that batch number to this batch.
              BatchNumber# = 0
              Loop BatchNumber# = 1 To 1000
                  Access:MULDESP_ALIAS.ClearKey(muld_ali:BatchNumberKey)
                  muld_ali:BatchNumber = BatchNumber#
                  If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Found
                  Else!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      muld:BatchNumber    = BatchNumber#
                      !tmp:ReturnedBatchNumber = muld:BatchNumber
                      Break
  
                  End!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
              End !BatchNumber# = 1 To 1000
              
              muld:HeadAccountNumber = p_web.GSV('BookingAccount')
              
              IF (Access:MULDESP.TryInsert() = Level:Benign)
                  IF (Access:MULDESPJ.PrimeRecord() = Level:Benign)
                      mulj:RefNumber = muld:RecordNumber
                      mulj:JobNumber = job:Ref_Number
                      mulj:IMEINumber = job:ESN
                      mulj:MSN = job:MSN
                      mulj:AccountNumber = muld:AccountNumber
                      mulj:Courier = muld:Courier
                      mulj:Current = 1
                      mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
                      IF (Access:MULDESPJ.TryInsert() = Level:Benign)
                      END
                  END
              END
              p_web.SSV('job:Ref_Number','') ! Clear to ensure code isn't called again
              
          END
          
      END
      
  END
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CreateNewBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CreateNewBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('CreateNewBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CreateNewBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CreateNewBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CreateNewBatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Multiple Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Multiple Despatch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CreateNewBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CreateNewBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CreateNewBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Added To Batch') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Paperwork') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CreateNewBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CreateNewBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CreateNewBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Added To Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateNewBatch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Added To Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Added To Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Added To Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Added To Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textMessage
      do Comment::textMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Paperwork') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateNewBatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCreateInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::textMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textMessage',p_web.GetValue('NewValue'))
    do Value::textMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textMessage  Routine
  p_web._DivHeader('CreateNewBatch_' & p_web._nocolon('textMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Job Number ' & p_web.GSV('locJobNumber') & ' Added To Batch',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textMessage  Routine
    loc:comment = ''
  p_web._DivHeader('CreateNewBatch_' & p_web._nocolon('textMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonDespatchNote  Routine
  p_web._DivHeader('CreateNewBatch_' & p_web._nocolon('buttonDespatchNote') & '_value',Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DespatchNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','DespatchNote','Despatch Note','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchNote')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('CreateNewBatch_' & p_web._nocolon('buttonDespatchNote') & '_comment',Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:DespatchNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('CreateNewBatch_' & p_web._nocolon('buttonCreateInvoice') & '_value',Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice','Create Invoice','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreateInvoice')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('CreateNewBatch_' & p_web._nocolon('buttonCreateInvoice') & '_comment',Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateInvoiceButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CreateNewBatch',0)

PreCopy  Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CreateNewBatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CreateNewBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CreateNewBatch:Primed',0)
  p_web.setsessionvalue('showtab_CreateNewBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CreateNewBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('CreateNewBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CreateNewBatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
Despatch:Loa         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
    
    
        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'RRC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END
    
    
        p_web.SSV('wob:ReadyToDespatch',0)
    
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Loan_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    
    
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Loan_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Loan_Despatched',Today())
    
        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'ARC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END
    
        p_web.SSV('AddToAudit:Action','LOAN UNIT DESPATCHED VIA ' & p_web.GSV('job:Loan_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))
    
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            loa:Available = 'DES'
            if (Access:LOAN.TryUpdate() = Level:Benign)
                If (Access:LOANHIST.PrimeRecord() = Level:Benign)
                    loh:Ref_Number = loa:Ref_Number
                    loh:Date = Today()
                    loh:Time = Clock()
                    loh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:Ref_Number')
                    Access:LOANHIST.TryInsert()
                END
            END
        END
    
    
    END ! if (p_web.GSV('BookingSite') = 'RRC')
    
    p_web.SSV('GetStatus:Type','LOA')
    p_web.SSV('GetStatus:StatusNumber',901)
    GetStatus(p_web)
    
    p_web.SSV('jobe:LoaSecurityPackNo',p_web.GSV('locSecurityPackID'))
    
    
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','LOA')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOANHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOANHIST.Close
     Access:LOAN.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
Despatch:Job         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('GetStatus:Type','JOB')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusSentToPUP',,Clip(Path()) & '\SB2KDEF.INI'),1,3))                                        
                        
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
                        
            p_web.SSV('LocationChange:Location',Clip(GETINI('RRC','InTransitToPUPLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            IF (p_web.GSV('job:Paid') = 'YES' OR (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('jobe:RRCCSubTotal') = 0))
                p_web.SSV('GetStatus:StatusNumber','910') 
            ELSE 
                p_web.SSV('GetStatus:StatusNumber','905')
            END
                        
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
            p_web.SSV('LocationChange:Location',Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
        END
                    
        GetStatus(p_web)
        LocationChange(p_web)
                    
        IF (p_web.GSV('job:Loan_Unit_Number') <> 0)
            p_web.SSV('GetStatus:StatusNumber',812)
            p_web.SSV('GetStatus:Type','LOA')
                        
            ! Loan Collection Note???
            ! MissingFor Noe
            !! 
        END
    
        ! Update Job
                
        p_web.SSV('wob:DateJobDespatched',TODAY())
        p_web.SSV('jobe:DespatchType','')
        p_web.SSV('jobe:Despatched','')
        p_web.SSV('wob:ReadyToDespatch',0)                
                
        ! Update Files
                   
        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)                    
        END
                    
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Sub_Accounts = 'YES')
                    IF (sub:Print_Despatch_Despatch = 'YES' AND |
                        sub:Despatch_Note_Per_Item = 'YES')
                        p_web.SSV('Hide:PrintDespatchNote',0)
                    END
                                    
                ELSE
                    IF (tra:Print_Despatch_Despatch = 'YES' AND |
                        tra:Despatch_Note_Per_Item = 'YES')
                        p_web.SSV('Hide:PrintDespatchNote',0)
                    END
                END
            END
        END
                
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','COURIER: ' & p_web.GSV('job:Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Date_Despatched',Today())
        p_web.SSV('job:Despatched','YES')
        p_web.SSV('job:Consignment_Number',p_web.GSV('locWaybillNumber'))
        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusDespatchedToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
            p_web.SSV('GetStatus:Type','JOB')
            GetStatus(p_web)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE !if (p_web.GSV('jobe:WebJob') = 1)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
                    
            paid# = 1
            if (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('job:Paid') <> 'YES')
                paid# = 0
            END
            IF (paid# = 1 AND p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Paid_Warranty') <> 'YES')
                paid# = 0
            END
            IF (paid# = 1)
                p_web.SSV('GetStatus:StatusNumber',910) ! Despatch Paid
                p_web.SSV('GetStatus:Type','JOB')
                GetStatus(p_web)
            ELSE
                p_web.SSV('GetStatus:StatusNumber',905) ! Despatch UnPaid
                p_web.SSV('GetStatus:Type','JOB')
                GetStatus(p_web)
            END
                    
            if (p_web.GSV('job:Loan_Unit_Number') <> 0)
                p_web.SSV('GetStatus:StatusNumber',812)
                p_web.SSV('GetStatus:Type','LOA')
                GetStatus(p_web)
            END
                    
                    
        end
        p_web.SSV('AddToAudit:Action','JOB DESPATCHED VIA ' & p_web.GSV('job:Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NUMBER: ' & Clip(p_web.GSV('locWaybillNumber')))
    END ! if (p_web.GSV('BookingSite') = 'RRC')
            
    p_web.SSV('jobe:JobSecurityPackNo',p_web.GSV('locSecurityPackID'))
            
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','JOB')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
Despatch:Exc         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',468)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
        p_web.SSV('GetStatus:Type','EXC')
        GetStatus(p_web)
    
        p_web.SSV('wob:ReadyToDespatch',0)
    
    
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Exchange_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    
        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)
        END
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Exchange_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Exchange_Despatched',TOday())
        p_web.SSV('job:Despatched','')
        p_web.SSV('job:Exchange_Despatched_User',p_web.GSV('BookingUserCode'))
    
        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('wob:ExcWaybillNumber',p_web.GSV('locWaybillNumber'))
    
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! if (p_web.GSV('jobe:WebJob') = 1)
    
        p_web.SSV('AddToAudit:Action','EXCHANGE UNIT DESPATCH VIA ' & p_web.GSV('job:Exchange_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))
    
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            xch:Available = 'DES'
            xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
            if (Access:EXCHANGE.TryUpdate() = Level:Benign)
    
                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = Today()
                    exh:Time = Clock()
                    exh:User = p_web.GSV('BookingUserCode')
                    exh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:ref_Number')
                    Access:EXCHHIST.TryInsert()
                END
            END
        END
    
    END ! if (p_web.GSV('BookingSite') = 'RRC')
    
    p_web.SSV('jobe:ExcSecurityPackNo',p_web.GSV('locSecurityPackID'))
    
    p_web.SSV('GetStatus:Type','EXC')
    GetStatus(p_web)
    
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','EXC')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHHIST.Close
     Access:JOBSCONS.Close
     Access:EXCHANGE.Close
     FilesOpened = False
  END
BannerEngineeringDetails PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('BannerEngineeringDetails')
  loc:formname = 'BannerEngineeringDetails_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerEngineeringDetails','')
    p_web._DivHeader('BannerEngineeringDetails',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerEngineeringDetails_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BannerEngineeringDetails_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerEngineeringDetails')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerEngineeringDetails_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerEngineeringDetails_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerEngineeringDetails_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BannerEngineeringDetails" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BannerEngineeringDetails" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BannerEngineeringDetails" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BannerEngineeringDetails">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BannerEngineeringDetails">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BannerEngineeringDetails')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BannerEngineeringDetails')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BannerEngineeringDetails'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BannerEngineeringDetails')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)

PreCopy  Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BannerEngineeringDetails:Primed',0)

PreDelete       Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BannerEngineeringDetails:Primed',0)
  p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerEngineeringDetails_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerEngineeringDetails_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BannerEngineeringDetails:Primed',0)
heading  Routine
  packet = clip(packet) & |
    '<<table class="FormCentre"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">Engineering Details<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    ''
FormAddToBatch       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyDetails    STRING(60)                            !
locHeadAccountNumber STRING(30)                            !
locExistingBatchNumber LONG                                !
locBatchStatusText   STRING(100)                           !
locBatchAccountNumber STRING(30)                           !
locBatchCourier      STRING(30)                            !
locNumberInBatch     LONG                                  !
locBatchAccountName  STRING(30)                            !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESPJ::State  USHORT
MULDESP::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormAddToBatch')
  loc:formname = 'FormAddToBatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormAddToBatch','')
    p_web._DivHeader('FormAddToBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAddToBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('locCompanyDetails')
    p_web.DeleteSessionValue('locHeadAccountNumber')
    p_web.DeleteSessionValue('locExistingBatchNumber')
    p_web.DeleteSessionValue('locBatchStatusText')
    p_web.DeleteSessionValue('locBatchAccountNumber')
    p_web.DeleteSessionValue('locBatchCourier')
    p_web.DeleteSessionValue('locNumberInBatch')
    p_web.DeleteSessionValue('locBatchAccountName')
    p_web.DeleteSessionValue('Hide:CreateNewBatchButton')
    p_web.DeleteSessionvalue('Hide:ConsignmentNumber')
    p_web.DeleteSessionValue('locWaybillNumber')
    
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Start
  p_web.SSV('Hide:AddToBatch',0)
  p_web.SSV('Hide:CreateNewBatchButton',0)
  p_web.SSV('Hide:ConsignmentNumber',1)
  p_web.SSV('locWaybillNumber','')
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
      p_web.SSV('locCompanyDetails',p_web.GSV('job:Account_Number') & ' - ' & Clip(sub:Company_Name))
  END
  
  locHeadAccountNumber = p_web.GSV('job:Account_Number')
  
  IF (sub:Generic_Account)
      IF (p_web.GSV('BookingSite') = 'RRC' OR | 
          (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') <> 1))
      ELSE
          locHeadAccountNumber = p_web.GSV('wob:HeadAccountNumber')
      END
      
  ELSE
      IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') = 1)
          locHeadAccountNumber = p_web.GSV('wob:HeadAccountNumber')
      END
  END
  
  ! Is there an existing batch?
  VIRTUAL# = 0
  locExistingBatchNumber = 0
  IF (p_web.GSV('BookingSite') = 'ARC')
      Access:MULDESP.ClearKey(muld:HeadAccountKey)
      muld:HeadAccountNumber = p_web.GSV('BookingAccount')
      muld:AccountNumber = locHeadAccountNumber
      If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          IF (NOT sub:Generic_Account)
              IF (vod.RemoteAccount(p_web.GSV('job:Account_Number')))
                  muld:AccountNumber = tra:Account_Number
                  VIRTUAL# = 1
              END
          END
          
      END
      
  ELSE
      IF (p_web.GSV('jobe:Sub_Sub_Account') = '')
          Access:MULDESP.ClearKey(muld:AccountNumberKey)
          muld:HeadAccountNumber = p_web.GSV('BookingAccount')
          muld:AccountNumber = locHeadAccountNumber
      ELSE
          locHeadAccountNumber = p_web.GSV('jobe:Sub_Sub_Account')
          Access:MULDESP.ClearKey(muld:AccountNumberKey)
          muld:HeadAccountNumber = p_web.GSV('BookingAccount')
          muld:AccountNumber = locHeadAccountNumber
      END
      
  END
  SET(muld:HeadAccountKey,muld:HeadAccountKey)
  LOOP UNTIL Access:MULDESP.Next()
      IF (muld:HeadAccountNumber <> p_web.GSV('BookingAccount'))
          BREAK
      END
      IF (VIRTUAL# = 1)
          IF (muld:AccountNumber <> tra:Account_Number)
              BREAK
          END
          IF (muld:Courier = tra:Courier_Outgoing)
              locExistingBatchNumber = muld:RecordNumber
          END
      ELSE
          IF (muld:AccountNumber <> locHeadAccountNumber)
              BREAK
          END
          IF (p_web.GSV('BookingSite') = 'ARC')
              CASE p_web.GSV('job:Despatch_Type')
              OF 'JOB'
                  IF (p_web.GSV('job:Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
                  
              OF 'EXC'
                  IF (p_web.GSV('job:Exchange_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              OF 'LOA'
                  IF (p_web.GSV('job:Loan_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              END
              
          ELSE
              CASE p_web.GSV('jobe:DespatchType')
              OF 'JOB'
                  IF (p_web.GSV('job:Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
                  
              OF 'EXC'
                  IF (p_web.GSV('job:Exchange_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              OF 'LOA'
                  IF (p_web.GSV('job:Loan_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              END
          END
          
      END
  END
  
  IF (locExistingBatchNumber = 0)
      p_web.SSV('locBatchStatusText','No Batch Found')
      p_web.SSV('Hide:AddToBatch',1)
  ELSE
      ! Found an existing batch, fill in the details
      Access:MULDESP.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = locExistingBatchNumber
      IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
          p_web.SSV('locBatchAccountNumber',muld:AccountNumber)
          p_web.SSV('locBatchCourier',muld:Courier)
          
          locNumberInBatch = 0
          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
          mulj:RefNumber = muld:RecordNumber
          SET(mulj:JobNumberKey,mulj:JobNumberKey)
          LOOP UNTIL Access:MULDESPJ.Next()
              IF (mulj:RefNumber <> muld:RecordNumber)
                  BREAK
              END
              locNumberInBatch += 1
          END
          p_web.SSV('locNumberInBatch',locNumberInBatch)
          
          ! Get The Account Name
          IF (muld:BatchType = 'TRA')
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = muld:AccountNumber
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  p_web.SSV('locBatchAccountName',tra:Company_Name)
              END
          ELSE
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = muld:AccountNumber
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                  p_web.SSV('locBatchAccountName',sub:Company_Name)
              END
  
          END
          
      END
      
      p_web.SSV('locBatchStatusText','Existing Batch Found')
      p_web.SSV('Hide:AddToBatch',0)
  END
  p_web.SSV('locExistingBatchNumber',locExistingBatchNumber)
  
  ! Show Consignment Number?
  IF (p_web.GSV('cou:PrintWaybill') = 1)
  ELSE
      IF (p_web.GSV('cou:AutoConsignmentNo') = 1)
            
      ELSE
          p_web.SSV('Hide:CreateNewBatchButton',1)
          p_web.SSV('Hide:ConsignmentNumber',0)
      END
  END
          
  p_web.SetValue('FormAddToBatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Ref_Number')
    p_web.SetPicture('job:Ref_Number','@s8')
  End
  p_web.SetSessionPicture('job:Ref_Number','@s8')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('locExistingBatchNumber') > 0
    loc:TabNumber += 1
  End
  If p_web.GSV('locExistingBatchNumber') = 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locCompanyDetails',locCompanyDetails)
  p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('locBatchStatusText',locBatchStatusText)
  p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber)
  p_web.SetSessionValue('locBatchAccountName',locBatchAccountName)
  p_web.SetSessionValue('locBatchCourier',locBatchCourier)
  p_web.SetSessionValue('locNumberInBatch',locNumberInBatch)
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locCompanyDetails')
    locCompanyDetails = p_web.GetValue('locCompanyDetails')
    p_web.SetSessionValue('locCompanyDetails',locCompanyDetails)
  End
  if p_web.IfExistsValue('job:Ref_Number')
    job:Ref_Number = p_web.GetValue('job:Ref_Number')
    p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('locBatchStatusText')
    locBatchStatusText = p_web.GetValue('locBatchStatusText')
    p_web.SetSessionValue('locBatchStatusText',locBatchStatusText)
  End
  if p_web.IfExistsValue('locBatchAccountNumber')
    locBatchAccountNumber = p_web.GetValue('locBatchAccountNumber')
    p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber)
  End
  if p_web.IfExistsValue('locBatchAccountName')
    locBatchAccountName = p_web.GetValue('locBatchAccountName')
    p_web.SetSessionValue('locBatchAccountName',locBatchAccountName)
  End
  if p_web.IfExistsValue('locBatchCourier')
    locBatchCourier = p_web.GetValue('locBatchCourier')
    p_web.SetSessionValue('locBatchCourier',locBatchCourier)
  End
  if p_web.IfExistsValue('locNumberInBatch')
    locNumberInBatch = p_web.GetValue('locNumberInBatch')
    p_web.SetSessionValue('locNumberInBatch',locNumberInBatch)
  End
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormAddToBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCompanyDetails = p_web.RestoreValue('locCompanyDetails')
 locBatchStatusText = p_web.RestoreValue('locBatchStatusText')
 locBatchAccountNumber = p_web.RestoreValue('locBatchAccountNumber')
 locBatchAccountName = p_web.RestoreValue('locBatchAccountName')
 locBatchCourier = p_web.RestoreValue('locBatchCourier')
 locNumberInBatch = p_web.RestoreValue('locNumberInBatch')
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAddToBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAddToBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAddToBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'MultipleBatchDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormAddToBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormAddToBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormAddToBatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Add To Batch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Add To Batch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormAddToBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormAddToBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormAddToBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Current Job Details') & ''''
        If p_web.GSV('locExistingBatchNumber') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Existing Batch Details') & ''''
        End
        If p_web.GSV('locExistingBatchNumber') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Create New Batch') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Force Single Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormAddToBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormAddToBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('locExistingBatchNumber') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          If p_web.GSV('locExistingBatchNumber') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormAddToBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('locExistingBatchNumber') > 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
    if p_web.GSV('locExistingBatchNumber') = 0
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Current Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Ref_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Ref_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('job:Chargeable_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('job:warranty_job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('locExistingBatchNumber') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Existing Batch Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchStatusText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('locBatchAccountNumber') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchAccountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locBatchAccountName') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchAccountName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchAccountName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locBatchCourier') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchCourier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchCourier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locNumberInBatch') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNumberInBatch
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNumberInBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAddToExistingBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textCreateNewBatchMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('locExistingBatchNumber') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Create New Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:ConsignmentNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEnterConsingmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:ConsignmentNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateNewBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Force Single Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonForceSingleDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::locCompanyDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyDetails',p_web.GetValue('NewValue'))
    locCompanyDetails = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyDetails',p_web.GetValue('Value'))
    locCompanyDetails = p_web.GetValue('Value')
  End

Value::locCompanyDetails  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locCompanyDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCompanyDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBoldLarge')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyDetails'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Ref_Number  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Ref_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Ref_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Ref_Number',p_web.GetValue('NewValue'))
    job:Ref_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = job:Ref_Number
    do Value::job:Ref_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Ref_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    job:Ref_Number = p_web.GetValue('Value')
  End

Value::job:Ref_Number  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Ref_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Ref_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Ref_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Chargeable Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::locBatchStatusText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchStatusText',p_web.GetValue('NewValue'))
    locBatchStatusText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchStatusText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchStatusText',p_web.GetValue('Value'))
    locBatchStatusText = p_web.GetValue('Value')
  End

Value::locBatchStatusText  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchStatusText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchStatusText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBoldLarge')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchStatusText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locBatchAccountNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchAccountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchAccountNumber',p_web.GetValue('NewValue'))
    locBatchAccountNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchAccountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchAccountNumber',p_web.GetValue('Value'))
    locBatchAccountNumber = p_web.GetValue('Value')
  End

Value::locBatchAccountNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchAccountNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchAccountNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locBatchAccountName  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchAccountName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchAccountName',p_web.GetValue('NewValue'))
    locBatchAccountName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchAccountName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchAccountName',p_web.GetValue('Value'))
    locBatchAccountName = p_web.GetValue('Value')
  End

Value::locBatchAccountName  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountName') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchAccountName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchAccountName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locBatchCourier  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchCourier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchCourier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchCourier',p_web.GetValue('NewValue'))
    locBatchCourier = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchCourier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchCourier',p_web.GetValue('Value'))
    locBatchCourier = p_web.GetValue('Value')
  End

Value::locBatchCourier  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchCourier') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchCourier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchCourier'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locNumberInBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locNumberInBatch') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Units In Batch')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNumberInBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNumberInBatch',p_web.GetValue('NewValue'))
    locNumberInBatch = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNumberInBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNumberInBatch',p_web.GetValue('Value'))
    locNumberInBatch = p_web.GetValue('Value')
  End

Value::locNumberInBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locNumberInBatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locNumberInBatch
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locNumberInBatch'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::buttonAddToExistingBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAddToExistingBatch',p_web.GetValue('NewValue'))
    do Value::buttonAddToExistingBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAddToExistingBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('buttonAddToExistingBatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddToBatch','Add To Batch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('MultipleBatchDespatch?Action=AddToBatch&BatchNumber=' & p_web.GSV('locExistingBatchNumber'))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/packinsert.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::textCreateNewBatchMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textCreateNewBatchMessage',p_web.GetValue('NewValue'))
    do Value::textCreateNewBatchMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textCreateNewBatchMessage  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('textCreateNewBatchMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate(p_web.br & '(Note: If you wish to create a New Batch, you must despatch/delete the current batch first.)',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::textEnterConsingmentNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEnterConsingmentNumber',p_web.GetValue('NewValue'))
    do Value::textEnterConsingmentNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEnterConsingmentNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('textEnterConsingmentNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('Enter A Consignment Number, then press [TAB]',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locWaybillNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
  If locWaybillNumber = ''
    loc:Invalid = 'locWaybillNumber'
    loc:alert = p_web.translate('Consignment Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locWaybillNumber = Upper(locWaybillNumber)
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  IF (p_web.GSV('locWaybillNumber') <> '')
      p_web.SSV('Hide:CreateNewBatchButton',0)
  ELSE
      p_web.SSV('Hide:CreateNewBatchButton',1)
  END
  do Value::locWaybillNumber
  do SendAlert
  do Value::buttonCreateNewBatch  !1

Value::locWaybillNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWaybillNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''formaddtobatch_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWaybillNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_value')


Validate::buttonCreateNewBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateNewBatch',p_web.GetValue('NewValue'))
    do Value::buttonCreateNewBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateNewBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('buttonCreateNewBatch') & '_value',Choose(p_web.GSV('Hide:CreateNewBatchButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateNewBatchButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateNewBatch','Create New Batch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreateNewBatch')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/new.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAddToBatch_' & p_web._nocolon('buttonCreateNewBatch') & '_value')


Validate::buttonForceSingleDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonForceSingleDespatch',p_web.GetValue('NewValue'))
    do Value::buttonForceSingleDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonForceSingleDespatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('buttonForceSingleDespatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ForceSingleDespatch','Individual Despatch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('IndividualDespatch?PassedJobNumber=' & p_web.GSV('locJobNumber') & '&PassedIMEINumber=' & p_web.GSV('locIMEINumber'))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/package.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormAddToBatch_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)

PreCopy  Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormAddToBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('locExistingBatchNumber') > 0
  End
  If p_web.GSV('locExistingBatchNumber') = 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAddToBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormAddToBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
  If p_web.GSV('locExistingBatchNumber') > 0
    loc:InvalidTab += 1
  End
  ! tab = 5
  If p_web.GSV('locExistingBatchNumber') = 0
    loc:InvalidTab += 1
    If p_web.GSV('Hide:ConsignmentNumber') <> 1
        If locWaybillNumber = ''
          loc:Invalid = 'locWaybillNumber'
          loc:alert = p_web.translate('Consignment Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locWaybillNumber = Upper(locWaybillNumber)
          p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
        If loc:Invalid <> '' then exit.
    End
  End
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  p_web.StoreValue('locCompanyDetails')
  p_web.StoreValue('job:Ref_Number')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('locBatchStatusText')
  p_web.StoreValue('locBatchAccountNumber')
  p_web.StoreValue('locBatchAccountName')
  p_web.StoreValue('locBatchCourier')
  p_web.StoreValue('locNumberInBatch')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
AddToBatch           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locExistingBatchNumber LONG                                !
FilesOpened     Long
JOBS::State  USHORT
MULDESPJ::State  USHORT
MULDESP::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AddToBatch')
  loc:formname = 'AddToBatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AddToBatch','')
    p_web._DivHeader('AddToBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AddToBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AddToBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Start
  IF (p_web.GSV('locExistingBatchNumber') > 0)
      Access:MULDESP.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = p_web.GSV('locExistingBatchNumber')
      IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
          IF (Access:MULDESP.PrimeRecord() = Level:Benign)
              mulj:RefNumber = muld:RecordNumber
              mulj:JobNumber = p_web.GSV('job:Ref_Number')
              mulj:IMEINumber = p_web.GSV('job:ESN')
              mulj:MSN = p_web.GSV('job:MSN')
              mulj:AccountNumber = p_web.GSV('job:Account_Number')
              mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
              IF (p_web.GSV('BookingSite') = 'RRC')
                  CASE p_web.GSV('jobe:DespatchType')
                  OF 'JOB'
                      mulj:Courier = p_web.GSV('job:Courier')
                  OF 'EXC'
                      mulj:Courier = p_web.GSV('job:Exchange_Courier')
                  OF 'LOA'
                      mulj:Courier = p_web.GSV('job:Loan_Courier')
                  END
                  
              ELSE
                  CASE p_web.GSV('job:Despatch_Type')
                  OF 'JOB'
                      mulj:Courier = p_web.GSV('job:Courier')
                  OF 'EXC'
                      mulj:Courier = p_web.GSV('job:Exchange_Courier')
                  OF 'LOA'
                      mulj:Courier = p_web.GSV('job:Loan_Courier')
                  END
  
              END
              IF (muld:BatchType = 'TRA')
                  mulj:AccountNumber = p_web.GSV('job:Account_Number')
                  mulj:Courier = muld:Courier
              END
              IF (Access:MULDESPJ.TryInsert() = Level:Benign)
                  
              ELSE
                  Access:MULDESPJ.CancelAutoInc()
              END
          END
          
      END
      
  END
  p_web.SetValue('AddToBatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('AddToBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferAddToBatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AddToBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AddToBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('AddToBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="AddToBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="AddToBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="AddToBatch" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_AddToBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_AddToBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_AddToBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Add To Batch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_AddToBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_AddToBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_AddToBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Add To Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AddToBatch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Add To Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Add To Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Add To Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Add To Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_AddToBatch',0)

PreCopy  Routine
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AddToBatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('AddToBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('AddToBatch:Primed',0)
  p_web.setsessionvalue('showtab_AddToBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AddToBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AddToBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('AddToBatch:Primed',0)
FinishBatch          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESPJ_ALIAS::State  USHORT
MULDESP_ALIAS::State  USHORT
WAYBILLS::State  USHORT
WAYBILLJ::State  USHORT
LOAN::State  USHORT
EXCHANGE::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
COURIER::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
MULDESPJ::State  USHORT
MULDESP::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
    MAP
ShowAlert   PROCEDURE(STRING fAlert)
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FinishBatch')
  loc:formname = 'FinishBatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FinishBatch','')
    p_web._DivHeader('FinishBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FinishBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FinishBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
Despatch            ROUTINE
    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'InUse',CLIP(PATH()) & '\DESPATCH.LOG')
    p_web.SSV('Hide:DespatchNoteButton',1)
    IF (muld:BatchType = 'TRA')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = muld:AccountNumber
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            IF (tra:Print_Despatch_Despatch = 'YES')
                IF (tra:Summary_Despatch_Notes = 'YES')
                    ! Don't think this is used.
                    !p_web.SSV('Hide:DespatchNoteButton',0)
                END
            END
        END
        
    ELSE ! IF (muld:BatchType = 'TRA')
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = muld:AccountNumber
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Sub_Accounts = 'YES')
                    IF (sub:Print_Despatch_Despatch = 'YES')
                        IF (sub:Summary_Despatch_Notes = 'YES')
                            !p_web.SSV('Hide:DespatchNoteButton',0)
                        END
                    END
                    
                ELSE
                    IF (tra:Print_Despatch_Despatch = 'YES')
                        IF (tra:Summary_Despatch_Notes = 'YES')
                            !p_web.SSV('Hide:DespatchNoteButton',0)
                        END
                    END
                END
            END
        END
    END ! IF (muld:BatchType = 'TRA')
    
    Access:MULDESP.ClearKey(muld:RecordNumberKey)
    muld:RecordNumber = p_web.GSV('muld:RecordNumber')
    IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
        p_web.SetValue('AllOK',1)
        Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
        mulj:RefNumber = muld:RecordNumber
        SET(mulj:JobNumberKey,mulj:JobNumberKey)
        LOOP UNTIL Access:MULDESPJ.Next()
            IF (mulj:RefNumber <> muld:RecordNumber)
                BREAK
            END

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = mulj:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END
            
            
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                CYCLE
            END
            
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
            p_web.FileToSessionQueue(WEBJOB)
            
            ! Add to waybill job list
            ! SB does this regards of waybill. So I will to
            IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                waj:WayBillNumber = p_web.GSV('locWaybillNumber')
                waj:JobNumber = job:Ref_Number
                IF (p_web.GSV('BookingSite') = 'RRC')
                    CASE (jobe:DespatchType)
                    OF 'JOB'
                        waj:IMEINumber = job:ESN
                    OF 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                           
                        END
                        waj:IMEINumber = xch:ESN
                    OF 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        END
                        
                        waj:IMEINumber = loa:ESN
                    END
                    waj:JobType = jobe:DespatchType
                    p_web.SSV('DespatchType',jobe:DespatchType)
                ELSE
                    CASE (job:Despatch_Type)
                    OF 'JOB'
                        waj:IMEINumber = job:ESN
                    OF 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                           
                        END
                        waj:IMEINumber = xch:ESN
                    OF 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        END
                        
                        waj:IMEINumber = loa:ESN
                    END
                    waj:JobType = job:Despatch_Type
                    p_web.SSV('DespatchType',job:Despatch_Type)
                END
                waj:OrderNumber = job:Order_Number
                waj:SecurityPackNumber = mulj:SecurityPackNumber
                IF (Access:WAYBILLJ.TryInsert())
                    Access:WAYBILLJ.CancelAutoInc()
                    p_web.SetValue('AllOK',0)
                    CYCLE
                    
                END
                    
                CASE p_web.GSV('DespatchType')
                OF 'JOB'
                    Despatch:Job(p_web)
                OF 'EXC'
                    Despatch:Exc(p_web)
                OF 'LOA'
                    Despatch:Loa(p_web)
                END
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('job:Ref_Number')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    p_web.SessionQueueToFile(JOBS)
                    Access:JOBS.TryUpdate()
                END
            
            
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                END
            
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('wob:RefNumber')
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(WEBJOB)
                    Access:WEBJOB.TryUpdate()
                END
            END
        END    
        IF (p_web.GetValue('AllOK') = 1)
            Access:MULDESP.ClearKey(muld:RecordNumberKey)
            muld:RecordNumber = p_web.GSV('muld:RecordNumber')
            IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
                IF (Relate:MULDESP.Delete(0) = Level:Benign)
                    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'Free',CLIP(PATH()) & '\DESPATCH.LOG')
                END
            END
        END
    END
    
    
    
DeleteSessionVariables      ROUTINE
    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'Free',CLIP(PATH()) & '\DESPATCH.LOG')
    p_web.DeleteSessionValue('IgnoreInUse')    
    p_web.DeleteSessionValue('locWaybillNumber')
    p_web.DeleteSessionValue('Hide:PrintWaybillButton')
    p_web.DeleteSessionValue('Hide:DespatchNoteButton')
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ_ALIAS)
  p_web._OpenFile(MULDESP_ALIAS)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ_ALIAS)
  p_Web._CloseFile(MULDESP_ALIAS)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FinishBatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('locWaybillNumber') = ''
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FinishBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Batch already in use?
  IF (p_web.IfExistsValue('muld:RecordNumber'))
      p_web.StoreValue('muld:RecordNumber')
  END
  
  If GETINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),,CLIP(PATH())&'\DESPATCH.LOG') = 'InUse'
      IF (p_web.IfExistsValue('IgnoreInUse'))
          p_web.StoreValue('IgnoreInUse')
      END
      
      IF (p_web.GSV('IgnoreInUse') = 1)
      ELSE
          p_web.SSV('Message:Text','The selected batch appears to be in use by another station. If you continue despatching, corruption may occur.'&|
              '\n\nIf you are SURE this batch is not in use, click ''OK''. Otherwise click ''Cancel'' and try again.')
          p_web.SSV('Message:PassURL','FinishBatch?IgnoreInUse=1')
          p_web.SSV('Message:FailURL','MultipleBatchDespatch')
          MessageQuestion(p_web)
          EXIT
      END
      
  End !If Sub(Format(Clock(),@t01),4,2) = GETINI('MULTIPLEDESPATCH',muld:BatchNUmber,,CLIP(PATH())&'\DESPATCH.LOG')
  ! Despatch now, or cons number required?
  p_web.SSV('Hide:PrintWaybillButton',1)
  locWaybillNumber = ''
  p_web.SSV('locWaybillNumber',locWaybillNumber)
  Access:MULDESP.ClearKey(muld:RecordNumberKey)
  muld:RecordNumber = p_web.GSV('muld:RecordNumber')
  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
      
      Access:COURIER.ClearKey(cou:Courier_Key)
      cou:Courier = muld:Courier
      IF (Access:COURIER.TryFetch(cou:Courier_Key))
      END
      
      IF (cou:PrintWaybill)
          locWaybillNumber = NextWaybillNumber()
          
          Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
          way:WayBillNumber = locWaybillNumber
          IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
              way:WayBillType = 1 ! Created From RRC
              way:AccountNumber = muld:AccountNumber
              way:FromAccount = p_web.GSV('BookingAccount')
              IF (p_web.GSV('BookingSite') = 'RRC')
                  way:ToAccount = muld:AccountNumber
                  way:WaybillID = 10 ! RRC TO Customer (Multi)
                  ! Allow for mult desp back to PUP
                  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                  mulj:RefNumber = muld:RecordNumber
                  SET(mulj:JobNumberKey,mulj:JobNumberKey)
                  LOOP UNTIL Access:MULDESPJ.Next()
                      IF (mulj:RefNumber <> muld:RecordNumber)
                          BREAK
                      END
                      
                      Access:JOBS.ClearKey(job:Ref_Number_Key)
                      job:Ref_Number = mulj:JobNumber
                      IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                          CYCLE
                      END
                      
                      IF (job:who_booked = 'WEB')
                          way:WayBillType = 9
                          way:WaybillID = 22
                      END
                      BREAK
                  END
                  
              ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
                  way:ToAccount = muld:AccountNumber
                  ! Send To Customer or RRC?
                  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                  mulj:RefNumber = muld:RecordNumber
                  SET(mulj:JobNumberKey,mulj:JobNumberKey)
                  LOOP UNTIL Access:MULDESPJ.Next()
                      IF (mulj:RefNumber <> muld:RecordNumber)
                          BREAK
                      END
                      
                      Access:JOBS.ClearKey(job:Ref_Number_Key)
                      job:Ref_Number = mulj:JobNumber
                      IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                          CYCLE
                      END
                      Access:JOBSE.ClearKey(jobe:RefNumberKey)
                      jobe:RefNumber = job:Ref_Number
                      IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                          CYCLE
                      END
                      Access:WEBJOB.ClearKey(wob:RefNumberKey)
                      wob:RefNumber = job:Ref_Number
                      IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                          CYCLE
                      END
                      
                      IF (jobe:WebJob)
                          way:WaybillID = 12 ! ARC TO RRC (Multi)
                          way:AccountNumber = wob:HeadAccountNumber
                          way:ToAccount = wob:HeadAccountNumber
                      ELSE
                          way:WaybillID = 11 ! ARC TO Customer
                          way:WayBillType = 2 ! Don't show in confirmation
                      END
                      
                     BREAK
                  END
              END ! IF (p_web.GSV('BookingSite') = 'RRC')
              Access:WAYBILLS.TryUpdate()
              p_web.SSV('Hide:PrintWaybillButton',0)
              ! Waybill Variables
              ! locWaybillNumber
              p_web.SSV('Waybill:Courier',muld:Courier)
              ! Waybill:Courier
              IF (p_web.GSV('BookingSite') = 'RRC')
                  p_web.SSV('Waybill:FromType','TRA')
              ELSE
                  p_web.SSV('Waybill:FromType','DEF')
              END
              ! Waybill:FromType
              p_web.SSV('Waybill:FromAccount',p_web.GSV('BookingAccount'))
              ! Waybill:FromAccount
              p_web.SSV('Waybill:ToType',muld:BatchType)
              ! Waybill:ToType
              p_web.SSV('Waybill:ToAccount',muld:AccountNumber)
              ! Waybill:ToAccount
          END
          
          p_web.SSV('locWaybillNumber',locWaybillNumber)
          DO Despatch
      ELSE
          IF (cou:AutoConsignmentNo)
              cou:LastConsignmentNo += 1
              Access:COURIER.TryUpdate()
              locWaybillNumber = cou:LastConsignmentNo
              p_web.SSV('locWaybillNumber',locWaybillNumber)
              DO Despatch
          ELSE
              locWaybillNumber = ''
          END
      END
  END
  
  
  
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FinishBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FinishBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FinishBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'MultipleBatchDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FinishBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FinishBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FinishBatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Multiple Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Multiple Despatch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FinishBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FinishBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FinishBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Finish Batch') & ''''
        If p_web.GSV('locWaybillNumber') = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Insert Consignment Number') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Paperwork') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FinishBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FinishBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('locWaybillNumber') = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FinishBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('locWaybillNumber') = ''
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Finish Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FinishBatch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('locWaybillNumber') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textBatchFinished
      do Comment::textBatchFinished
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('locWaybillNumber') = ''
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Insert Consignment Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FinishBatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFinishBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFinishBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Paperwork') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FinishBatch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Paperwork')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::textBatchFinished  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBatchFinished',p_web.GetValue('NewValue'))
    do Value::textBatchFinished
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBatchFinished  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Batch Despatched',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textBatchFinished  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWaybillNumber  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt',Choose(p_web.GSV('locWaybillNumber') <> '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Consignment Number')
  If p_web.GSV('locWaybillNumber') <> ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
    locWaybillNumber = Upper(locWaybillNumber)
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  do Value::locWaybillNumber
  do SendAlert

Value::locWaybillNumber  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_value',Choose(p_web.GSV('locWaybillNumber') <> '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillNumber') <> '')
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''finishbatch_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_value')

Comment::locWaybillNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_comment',Choose(p_web.GSV('locWaybillNumber') <> '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWaybillNumber') <> ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFinishBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFinishBatch',p_web.GetValue('NewValue'))
    do Value::buttonFinishBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  IF (p_web.GSV('locWaybillNumber') <> '')
      DO Despatch
  END
  do Value::buttonFinishBatch
  do SendAlert
  do Value::buttonPrintDespatchNote  !1

Value::buttonFinishBatch  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFinishBatch'',''finishbatch_buttonfinishbatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FinishBatch','Finish Batch','MainButtonIcon',loc:formname,,,,loc:javascript,0,'images/package.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_value')

Comment::buttonFinishBatch  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybillButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybillButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Waybill','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonPrintWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_comment',Choose(p_web.GSV('Hide:PrintWaybillButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintWaybillButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DespatchNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','DespachNote','Despatch Note','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('MultipleDespatchNote')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')

Comment::buttonPrintDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:DespatchNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FinishBatch_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('FinishBatch_buttonFinishBatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFinishBatch
      else
        do Value::buttonFinishBatch
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FinishBatch',0)

PreCopy  Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FinishBatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FinishBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FinishBatch:Primed',0)
  p_web.setsessionvalue('showtab_FinishBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('locWaybillNumber') = ''
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FinishBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO DeleteSessionVariables
  p_web.DeleteSessionValue('FinishBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
  If p_web.GSV('locWaybillNumber') = ''
    loc:InvalidTab += 1
      If not (p_web.GSV('locWaybillNumber') <> '')
          locWaybillNumber = Upper(locWaybillNumber)
          p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
        If loc:Invalid <> '' then exit.
      End
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FinishBatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
alertmessage  Routine
  packet = clip(packet) & |
    '<<SCRIPT LANGUAGE="javascript"><13,10>'&|
    '<<!--<13,10>'&|
    'function CONFIRM(){{if (!confirm<13,10>'&|
    '("Change this to your message"))<13,10>'&|
    'window.location = "MultipleBatchDespatch";return " "}<13,10>'&|
    'document.writeln(CONFIRM())<13,10>'&|
    '<<!-- END --><13,10>'&|
    '<</SCRIPT><13,10>'&|
    ''
ShowAlert           Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
MessageQuestion      PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MessageQuestion -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('MessageQuestion')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    packet = clip(packet) & |
        '<<html>' & |
        '<<script type="text/javascript"><13,10>'&|
        '<<!--<13,10>'&|
        'function confirmation() {{<13,10>'&|
        'var answer = confirm("' & p_web.GSV('Message:Text') & '")<13,10>'&|
        'if (answer){{<13,10>'&|
        'window.location.href = "' & p_web.GSV('Message:PassURL') & '";<13,10>'&|
        '}<13,10>'&|
        'else{{<13,10>'&|
        'window.location.href = "' & p_web.GSV('Message:FailURL') & '";<13,10>'&|
        '}<13,10>'&|
        '}<13,10>'&|
        'document.write(confirmation())<13,10>'&|
        '// --><13,10>'&|
        '<</script>'&|
        '<</html>'
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
