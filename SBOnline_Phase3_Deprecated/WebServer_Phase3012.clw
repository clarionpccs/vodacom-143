

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3012.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3013.INC'),ONCE        !Req'd for module callout resolution
                     END


createWebOrder       PROCEDURE  (fAccountNumber,fPartNumber,fDescription,fQuantity,fRetailCost) ! Declare Procedure
locAccountNumber     STRING(30)                            !
TRADEACC::State  USHORT
ORDWEBPR::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = fAccountNumber
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        if (tra:StoresAccount <> '')
            locAccountNumber = tra:StoresAccount
        else ! if (tra:StoresAccount <> '')
            locAccountNumber = tra:Account_Number
        end ! if (tra:StoresAccount <> '')
    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    found# = 0
    Access:ORDWEBPR.Clearkey(orw:PartNumberKey)
    orw:AccountNumber    = locAccountNumber
    orw:PartNumber    = fPartNumber
    set(orw:PartNumberKey,orw:PartNumberKey)
    loop
        if (Access:ORDWEBPR.Next())
            Break
        end ! if (Access:ORDWEBPR.Next())
        if (orw:AccountNumber    <> locAccountNumber)
            Break
        end ! if (orw:AccountNumber    <> locAccountNumber)
        if (orw:PartNumber    <> fPartNumber)
            Break
        end ! if (orw:PartNumber    <> fPartNumber)
        if (orw:Description = fDescription)
            orw:Quantity += fQuantity
            access:ORDWEBPR.tryUpdate()
            found# = 1
            break
        end ! if (orw:Description = fDescription)
    end ! loop

    if (found# = 0)
        if (Access:ORDWEBPR.PrimeRecord() = Level:Benign)
            orw:AccountNumber    = locAccountNumber
            orw:PartNumber    = fPartNumber
            orw:Quantity    = fQuantity
            orw:ItemCost    = fRetailCost
            orw:Description = fDescription
            if (Access:ORDWEBPR.TryInsert() = Level:Benign)
                ! Inserted
            else ! if (Access:ORDWEBPR.TryInsert() = Level:Benign)
                ! Error
                Access:ORDWEBPR.CancelAutoInc()
            end ! if (Access:ORDWEBPR.TryInsert() = Level:Benign)
        end ! if (Access:ORDWEBPR.PrimeRecord() = Level:Benign)
    end !if (found# = 0)

    do restoreFiles
    do closeFiles
SaveFiles  ROUTINE
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ORDWEBPR::State = Access:ORDWEBPR.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ORDWEBPR::State <> 0
    Access:ORDWEBPR.RestoreFile(ORDWEBPR::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ORDWEBPR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ORDWEBPR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:ORDWEBPR.Close
     FilesOpened = False
  END
ConvertEstimateParts PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locCreateWebOrder    BYTE                                  !
locPartNumber        LONG                                  !
locQuantity          LONG                                  !
local       class
AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
            end
tmp:AddToStockAllocation Byte(0)
FilesOpened     BYTE(0)
  CODE
    do openFiles

    tmp:AddToStockAllocation = 0

    access:estparts.clearkey(epr:part_number_key)
    epr:ref_number  = p_web.GSV('job:ref_number')
    set(epr:part_number_key,epr:part_number_key)
    loop
        if access:estparts.next()
           break
        end !if
        if epr:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Access:STOCK.ClearKey(sto:ref_number_key)
        sto:ref_number = epr:Part_Ref_Number
        if not Access:STOCK.Fetch(sto:ref_number_key)
            if sto:ExchangeUnit = 'YES'

                !Check for existing unit - then exchange
                !chargeable parts and partnumber = EXCH?
                access:parts.clearkey(par:Part_Number_Key)
                par:Ref_Number = p_web.GSV('job:ref_number')
                par:Part_Number = 'EXCH'
                if access:parts.fetch(par:Part_NUmber_Key) = level:benign
                    !found an existing Exchange unit
                ELSE

                    Access:USERS.Clearkey(use:User_Code_Key)
                    use:User_Code   = p_web.GSV('job:Engineer')
                    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = 'EXCH'
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            access:location.clearkey(loc:Location_Key)
                            loc:Location = use:location
                            access:location.fetch(loc:Location_Key)
                            !changed 19/11 alway allocate at once
                            !change back 20/11
!                            if loc:UseRapidStock then
                                !Found
                                Local.AllocateExchangePart('CHA',0)
!                            ELSE
!                                ExchangeUnitNumber# = p_web.GSV('job:Exchange_Unit_Number')
!                                ViewExchangeUnit()
!                                Access:JOBS.TryUpdate()
!                                ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
!                                UpdateDateTimeStamp(p_web.GSV('job:Ref_Number'))
!                                ! End (DBH 16/09/2008) #10253
!                                If p_web.GSV('job:Exchange_Unit_Number') <> ExchangeUnitNumber#
!                                    Local.AllocateExchangePart('CHA',1)
!                                End !If p_web.GSV('job:Exchange_Unit_Number <> ExchangeUnitNumber#
!                            END !if loc:useRapidStock
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    Else ! If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign

                    p_web.SSV('GetStatus:Type','EXC')
                    p_web.SSV('GetStatus:StatusNumber',108)
                    getStatus(p_web)
                End !If exchang unit already existed

                !Remove the tick so that if the job is made "Unaccpeted"
                !then you won't have to remove these parts to stock twice
                If epr:UsedOnRepair
                    epr:UsedOnRepair = 0
                    Access:ESTPARTS.Update()
                End !If epr:UsedOnRepair
                cycle
            end
        end

        get(parts,0)
        glo:select1 = ''
        if access:parts.primerecord() = level:benign
            par:ref_number           = p_web.GSV('job:ref_number')
            par:adjustment           = epr:adjustment
            par:part_number     = epr:part_number
            par:description     = epr:description
            par:supplier        = epr:supplier
            par:purchase_cost   = epr:purchase_cost
            par:sale_cost       = epr:sale_cost
            par:retail_cost     = epr:retail_cost
            par:quantity             = epr:quantity
            par:exclude_from_order   = epr:exclude_from_order
            par:part_ref_number      = epr:part_ref_number
            !If not used, then mark to be allocated
            If epr:UsedOnRepair
                par:PartAllocated       = epr:PartAllocated
            Else !If epr:UsedOnRepair
                par:PartAllocated       = 0
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    IF (sto:Sundry_Item = 'YES') ! ! #11837 Don't decrement a sundry item. (Bryan: 08/12/2010)
                        par:PartAllocated = 1
                        par:Date_Ordered = TODAY()
                    ELSE
                        If par:Quantity <= sto:Quantity_Stock
                            !If its not a Rapid site, turn part allocated off
!                        If RapidLocation(sto:Location)
                            par:PartAllocated = 0
!                        End !If RapidLocation(sto:Location) = Level:Benign
                            sto:Quantity_Stock  -= par:Quantity
                            If sto:Quantity_Stock < 0
                                sto:Quantity_Stock = 0
                            End !If sto:Quantity_Stock < 0

                            tmp:AddToStockAllocation = 1

                            If Access:STOCK.Update() = Level:Benign
                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                    'DEC', | ! Transaction_Type
                                    par:Despatch_Note_Number, | ! Depatch_Note_Number
                                    p_web.GSV('job:Ref_Number'), | ! Job_Number
                                    0, | ! Sales_Number
                                    par:Quantity, | ! Quantity
                                    sto:Purchase_Cost, | ! Purchase_Cost
                                    sto:Sale_Cost, | ! Sale_Cost
                                    sto:Retail_Cost, | ! Retail_Cost
                                    'STOCK DECREMENTED', | ! Notes
                                    '', | !Information
                                    p_web.GSV('BookingUserCode'), |
                                    sto:Quantity_Stock) ! Information
                                    ! Added OK
                                Else ! AddToStockHistory
                                    ! Error
                                End ! AddToStockHistory
                            End! If Access:STOCK.Update() = Level:Benign
                        Else !If par:Quantity < sto:Quantity

                            CreateWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,par:Description,par:Quantity - sto:Quantity_Stock,par:Retail_Cost)
                            If sto:Quantity_Stock > 0
                                locCreateWebOrder = 1
                                locPartNumber = par:Part_Ref_Number
                                locQuantity = par:Quantity - sto:Quantity_Stock
                                !glo:Select1 = 'NEW PENDING WEB'
                                !glo:Select2 = par:Part_Ref_Number
                                !glo:Select3 = par:Quantity - sto:Quantity_Stock
                                !glo:Select4 =
                                par:Quantity    = sto:Quantity_Stock
                                par:Date_Ordered    = Today()
                                tmp:AddToStockAllocation = 0
                            Else !If sto:Quantity_Stock > 0
                                par:WebOrder    = 1
                                tmp:AddToStockAllocation = 2
                            End !If sto:Quantity_Stock > 0
                        End !If par:Quantity > sto:Quantity
                    END
                    
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            End !If epr:UsedOnRepair

            par:Fault_Code1         = epr:Fault_Code1
            par:Fault_Code2         = epr:Fault_Code2
            par:Fault_Code3         = epr:Fault_Code3
            par:Fault_Code4         = epr:Fault_Code4
            par:Fault_Code5         = epr:Fault_Code5
            par:Fault_Code6         = epr:Fault_Code6
            par:Fault_Code7         = epr:Fault_Code7
            par:Fault_Code8         = epr:Fault_Code8
            par:Fault_Code9         = epr:Fault_Code9
            par:Fault_Code10        = epr:Fault_Code10
            par:Fault_Code11        = epr:Fault_Code11
            par:Fault_Code12        = epr:Fault_Code12
            par:Fault_Codes_Checked = epr:Fault_Codes_Checked
            !Neil
            par:RRCPurchaseCost        = epr:RRCPurchaseCost
            par:RRCSaleCost            = epr:RRCSaleCost
            par:InWarrantyMarkUp       = epr:InWarrantyMarkUp
            par:OutWarrantyMarkUp      = epr:OutWarrantyMarkUp
            par:RRCAveragePurchaseCost = epr:RRCAveragePurchaseCost
            par:AveragePurchaseCost    = epr:AveragePurchaseCost

            access:parts.insert()

            !Add part to stock allocation list - L873 (DBH: 11-08-2003)
            Case tmp:AddToStockAllocation
            Of 1
                p_web.SSV('AddToStockAllocation:Type','CHA')
                p_web.SSV('AddToStockAllocation:Status','')
                p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                addToStockAllocation(p_web)  
            Of 2
                p_web.SSV('AddToStockAllocation:Type','CHA')
                p_web.SSV('AddToStockAllocation:Status','WEB')
                p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                addToStockAllocation(p_web)  
            End !If tmp:AddToStockAllocation

            If (locCreateWebOrder = 1)
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = locPartNumber
                If access:stock.fetch(sto:ref_number_key)
                Else!If access:stock.fetch(sto:ref_number_key)
                    sto:quantity_stock     = 0
                    access:stock.update()

                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                         p_web.GSV('job:Ref_Number'), | ! Job_Number
                                         0, | ! Sales_Number
                                         sto:Quantity_stock, | ! Quantity
                                         par:Purchase_Cost, | ! Purchase_Cost
                                         par:Sale_Cost, | ! Sale_Cost
                                         par:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '', |
                                         p_web.GSV('BookingUserCode'), |
                                         sto:Quantity_Stock) ! Information
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory

                    access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                    par_ali:ref_number      = p_web.GSV('job:ref_number')
                    par_ali:part_ref_number = par:part_Ref_Number
                    If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                        par:ref_number           = par_ali:ref_number
                        par:adjustment           = par_ali:adjustment
                        par:part_ref_number      = par_ali:part_ref_number

                        par:part_number     = par_ali:part_number
                        par:description     = par_ali:description
                        par:supplier        = par_ali:supplier
                        par:purchase_cost   = par_ali:purchase_cost
                        par:sale_cost       = par_ali:sale_cost
                        par:retail_cost     = par_ali:retail_cost
                        par:quantity             = locQuantity
                        par:warranty_part        = 'NO'
                        par:exclude_from_order   = 'NO'
                        par:despatch_note_number = ''
                        par:date_ordered         = ''
                        par:date_received        = ''
                        par:pending_ref_number   = ''
                        par:order_number         = ''
                        par:fault_code1    = par_ali:fault_code1
                        par:fault_code2    = par_ali:fault_code2
                        par:fault_code3    = par_ali:fault_code3
                        par:fault_code4    = par_ali:fault_code4
                        par:fault_code5    = par_ali:fault_code5
                        par:fault_code6    = par_ali:fault_code6
                        par:fault_code7    = par_ali:fault_code7
                        par:fault_code8    = par_ali:fault_code8
                        par:fault_code9    = par_ali:fault_code9
                        par:fault_code10   = par_ali:fault_code10
                        par:fault_code11   = par_ali:fault_code11
                        par:fault_code12   = par_ali:fault_code12
                        par:WebOrder = 1
                        access:parts.insert()
                        p_web.SSV('AddToStockAllocation:Type','CHA')
                        p_web.SSV('AddToStockAllocation:Status','WEB')
                        p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
                        addToStockAllocation(p_web)  
                    end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign

                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
            End !If glo:Select1 = 'NEW PENDING WEB'
            !End
        end!if access:parts.primerecord() = level:benign

        !Remove the tick so that if the job is made "Unaccpeted"
        !then you won't have to remove these parts to stock twice
        If epr:UsedOnRepair
            epr:UsedOnRepair = 0
            Access:ESTPARTS.Update()
        End !If epr:UsedOnRepair
    end !loop

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.Open                                  ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.UseFile                               ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:PARTS_ALIAS.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:USERS.Close
     FilesOpened = False
  END
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
Code
        Case func:Type
            Of 'WAR'
                get(warparts,0)
                if access:warparts.primerecord() = level:benign
                    wpr:PArt_Ref_Number      = sto:Ref_Number
                    wpr:ref_number            = p_web.GSV('job:ref_number')
                    wpr:adjustment            = 'YES'
                    wpr:part_number           = 'EXCH'
                    wpr:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                    wpr:quantity              = 1
                    wpr:warranty_part         = 'NO'
                    wpr:exclude_from_order    = 'YES'
                    wpr:PartAllocated         = func:Allocated !was func:Allocated but see VP116 - must always be allocated straight off!
                                                  !changed back from '1' 20/11 - original system reinstalled
                    if func:Allocated = 0 then
                        wpr:Status            = 'REQ'
                    ELSE
                        WPR:Status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                        !Try and get the fault codes. This key should get the only record
                        Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                        stm:Ref_Number  = sto:Ref_Number
                        stm:Part_Number = sto:Part_Number
                        stm:Location    = sto:Location
                        stm:Description = sto:Description
                        If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                            !Found
                            wpr:Fault_Code1  = stm:FaultCode1
                            wpr:Fault_Code2  = stm:FaultCode2
                            wpr:Fault_Code3  = stm:FaultCode3
                            wpr:Fault_Code4  = stm:FaultCode4
                            wpr:Fault_Code5  = stm:FaultCode5
                            wpr:Fault_Code6  = stm:FaultCode6
                            wpr:Fault_Code7  = stm:FaultCode7
                            wpr:Fault_Code8  = stm:FaultCode8
                            wpr:Fault_Code9  = stm:FaultCode9
                            wpr:Fault_Code10 = stm:FaultCode10
                            wpr:Fault_Code11 = stm:FaultCode11
                            wpr:Fault_Code12 = stm:FaultCode12
                        Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    end !If sto:Assign_Fault_Codes = 'YES'
                    if access:warparts.insert()
                        access:warparts.cancelautoinc()
                    end
                End

            Of 'CHA'
                get(parts,0)
                if access:parts.primerecord() = level:benign
                    par:PArt_Ref_Number      = sto:Ref_Number
                    par:ref_number            = p_web.GSV('job:ref_number')
                    par:adjustment            = 'YES'
                    par:part_number           = 'EXCH'
                    par:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                    par:quantity              = 1
                    par:warranty_part         = 'NO'
                    par:exclude_from_order    = 'YES'
                    par:PartAllocated         = func:Allocated
                    !see above for confusion
                    if func:allocated = 0 then
                        par:Status            = 'REQ'
                    ELSE
                        par:status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           par:Fault_Code1  = stm:FaultCode1
                           par:Fault_Code2  = stm:FaultCode2
                           par:Fault_Code3  = stm:FaultCode3
                           par:Fault_Code4  = stm:FaultCode4
                           par:Fault_Code5  = stm:FaultCode5
                           par:Fault_Code6  = stm:FaultCode6
                           par:Fault_Code7  = stm:FaultCode7
                           par:Fault_Code8  = stm:FaultCode8
                           par:Fault_Code9  = stm:FaultCode9
                           par:Fault_Code10 = stm:FaultCode10
                           par:Fault_Code11 = stm:FaultCode11
                           par:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Error
                           !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    End !If sto:Assign_Fault_Codes = 'YES'
                    if access:parts.insert()
                        access:parts.cancelautoinc()
                    end
                End !If access:Prime
        End !Case func:Type
AddToStockHistory    PROCEDURE  (Long f:RefNo,String f:Trans,String f:Desp,Long f:JobNo,Long f:SaleNo,Long f:Qty,Real f:Purch,Real f:Sale,Real f:Retail,String f:Notes,String f:Info,String f:UserCode,Long f:QtyStock,<Real f:AvPC>,<Real f:PC>,<Real f:SC>,<Real f:RC>) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    rtnValue# = 0
    do openFiles
    If Access:STOHIST.PrimeRecord() = Level:Benign
        shi:Ref_Number           = f:RefNo
        shi:User                 = f:UserCode
        shi:Transaction_Type     = f:Trans
        shi:Despatch_Note_Number = f:Desp
        shi:Job_Number           = f:JobNo
        shi:Sales_Number         = f:SaleNo
        shi:Quantity             = f:Qty
        shi:Date                 = Today()
        shi:Purchase_Cost        = f:Purch
        shi:Sale_Cost            = f:Sale
        shi:Retail_Cost          = f:Retail
        shi:Notes                = f:Notes
        shi:Information          = f:Info
        shi:StockOnHand          = f:QtyStock
        If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Successful
            rtnValue# = 1
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber = shi:Record_Number
                IF (f:AvPC = 0)
                    f:AvPC = f:Purch
                END
                IF (f:PC = 0)
                    f:PC = f:Purch
                END
                IF (f:SC = 0)
                    f:SC = f:Sale
                END
                IF (f:RC = 0)
                    f:RC = f:Retail
                END
                
                stoe:PreviousAveragePurchaseCost = f:AvPC
                stoe:PurchaseCost = f:PC
                stoe:SaleCost = f:SC
                stoe:RetailCost = f:RC
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END
            
        Else ! If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Failed
            Access:STOHIST.CancelAutoInc()
        End ! If Access:STOHIST.TryInsert() = Level:Benign
    End ! If Access:STOHIST.PrimeRecord() = Level:Benign

    do closeFiles

    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOHISTE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHISTE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOHISTE.Close
     Access:STOHIST.Close
     FilesOpened = False
  END
AddToStockAllocation PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
STOCKALL::State  USHORT
STOCK::State  USHORT
FilesOpened     BYTE(0)
  CODE
    !p_web.SSV('AddToStockAllocation:Type','WAR')
    !p_web.SSV('AddToStockAllocation:Status','STatus?')
    !p_web.SSV('AddToSTockAllocation:Qty',?)
    
    do openFiles
    do saveFiles
    Access:STOCKALL.Clearkey(stl:PartRecordNumberKey)
    stl:Location = p_web.GSV('BookingSiteLocation')
    stl:PartType = p_web.GSV('AddToStockAllocation:Type')
    Case stl:PartType
    of 'CHA'
        stl:PartRecordNumber = p_web.GSV('par:Record_Number')
                
        IF (p_web.GSV('par:Correction') = 1)
            DO ExitProc
                
        END
        
    of 'WAR'
        stl:PartRecordNumber = p_web.GSV('wpr:Record_Number') 
                    
        IF (p_web.GSV('wpr:Correction') = 1)
            DO ExitProc
        END 
                
    of 'EST'
        stl:PartRecordNumber = p_web.GSV('epr:Record_Number')
        IF (p_web.GSV('epr:Correction') = 1)
            DO ExitProc
        END 
                
    end
    if (Access:STOCKALL.TryFetch(stl:PartRecordNumberKey) = Level:Benign)
        Do WriteRecord
    else
        If (Access:STOCKALL.PrimeRecord() = Level:Benign)
            Do WriteRecord
            if (Access:STOCKALL.TryInsert())
            End
        End
    end
    
    DO ExitProc
              
ExitProc            ROUTINE
    do restoreFiles
    do closeFiles
            
    p_web.DeleteSessionValue('AddToStockAllocation:Type')
    p_web.DeleteSessionValue('AddToStockAllocation:Status')
    p_web.DeleteSessionValue('AddToStockAllocation:Qty')
WriteRecord         Routine
    stl:Location = p_web.GSV('BookingSiteLocation')
    stl:PartType = p_web.GSV('AddToStockAllocation:Type')
    stl:Status  = p_web.GSV('AddToStockAllocation:Status')
    stl:Engineer = p_web.GSV('job:Engineer')
    Case stl:PartType
    of 'CHA'
        stl:PartNumber = p_web.GSV('par:Part_Number')
        stl:Description = p_web.GSV('par:Description')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('par:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            stl:PartRefNumber = sto:Ref_Number
            stl:ShelfLocation = sto:Shelf_Location
        end
        stl:JobNumber = p_web.GSV('par:Ref_Number')
        stl:PartRecordNumber = p_web.GSV('par:Record_Number')
        stl:Quantity = p_web.GSV('AddToSTockAllocation:Qty')

    of 'WAR'
        stl:PartNumber = p_web.GSV('wpr:Part_Number')
        stl:Description = p_web.GSV('wpr:Description')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('wpr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            stl:PartRefNumber = sto:Ref_Number
            stl:ShelfLocation = sto:Shelf_Location
        end
        stl:JobNumber = p_web.GSV('wpr:Ref_Number')
        stl:PartRecordNumber = p_web.GSV('wpr:Record_Number')
        stl:Quantity = p_web.GSV('AddToStockAllocation:Qty')
    of 'EST'
        stl:PartNumber = p_web.GSV('epr:Part_Number')
        stl:Description = p_web.GSV('epr:Description')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('epr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            stl:PartRefNumber = sto:Ref_Number
            stl:ShelfLocation = sto:Shelf_Location
        end
        stl:JobNumber = p_web.GSV('epr:Ref_Number')
        stl:PartRecordNumber = p_web.GSV('epr:Record_Number')
        stl:Quantity = p_web.GSV('AddToStockAllocation:Qty')
    End
SaveFiles  ROUTINE
  STOCKALL::State = Access:STOCKALL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  STOCK::State = Access:STOCK.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF STOCKALL::State <> 0
    Access:STOCKALL.RestoreFile(STOCKALL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STOCK::State <> 0
    Access:STOCK.RestoreFile(STOCK::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKALL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKALL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKALL.Close
     Access:STOCK.Close
     FilesOpened = False
  END
CalculateBilling     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
local:CFaultCode        String(255)
local:CIndex            Byte(0)
local:CRepairType       String(30)
local:WFaultCOde        String(255)
local:WIndex            Byte(0)
local:WRepairType       String(30)
local:KeyRepair         Long()
local:FaultCode         String(255)
FilesOpened     BYTE(0)
  CODE
    do openfiles

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseInvTextForFaults = True
            If man:AutoRepairType = True
                ! Get the main out fault (DBH: 23/11/2007)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer = p_web.GSV('job:Manufacturer')
                maf:MainFault = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                    ! Check the out faults list (DBH: 23/11/2007)
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber = p_web.GSV('job:Ref_Number')
                    Set(joo:JobNumberKey,joo:JobNumberKey)
                    Loop
                        If Access:JOBOUTFL.Next()
                            Break
                        End ! If Access:JOBOUTFL.Next()
                        If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                            Break
                        End ! If joo:JobNumber <> job:Ref_Number

                        ! Get the lookup details (e.g. index and repair types) (DBH: 23/11/2007)
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field = joo:FaultCode
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            ! Is there a repair type? (DBH: 23/11/2007)
                            If mfo:RepairType <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:CIndex
                                    local:CFaultCode = mfo:Field
                                    local:CIndex = mfo:ImportanceLevel
                                    local:CRepairType = mfo:RepairType
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairType <> ''
                            If mfo:RepairTypeWarranty <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:WIndex
                                    local:WFaultCode = mfo:Field
                                    local:WIndex = mfo:ImportanceLevel
                                    local:WRepairType = mfo:RepairTypeWarranty
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairTypeWarranty <> ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End ! Loop

                    ! Inserting (DBH 30/04/2008) # 9723 - Is there a "Key Repair" fault code? If so, that is counted as the OUT Fault
                    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:KeyRepair = 1
                    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        ! Found
                        local:KeyRepair = map:Field_Number
                    Else ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        local:KeyRepair = 0
                    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                    ! End (DBH 30/04/2008) #9723

! Changing (DBH 21/05/2008) # 9723 - Can be more than one part out fault
!                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
!                    map:Manufacturer = job:Manufacturer
!                    map:MainFault = 1
!                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
! to (DBH 21/05/2008) # 9723
                    ! Is the out fault held on the parts. If so, check for the highest repair index there too (DBH: 23/11/2007)
                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:MainFault = 1
                    Set(map:MainFaultKey,map:MainFaultKey)
                    Loop ! Begin Loop
                        If Access:MANFAUPA.Next()
                            Break
                        End ! If Access:MANFAUPA.Next()
                        If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                            Break
                        End ! If map:Manufacturer <> job:Manufacturer
                        If map:MainFault <> 1
                            Break
                        End ! If map:MainFault <> 1

! End (DBH 21/05/2008) #9723
                        ! Check the Warranty Parts (DBH: 23/11/2007)
                        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = p_web.GSV('job:Ref_Number')
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.Next()
                                Break
                            End ! If Access:WARPARTS.Next()
                            If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                Break
                            End ! If wpr:Ref_Number <> job:Ref_Number

                            ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                            Case local:KeyRepair
                            Of 1
                                If wpr:Fault_Code1 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code1 <> 1
                            Of 2
                                If wpr:Fault_Code2 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code2 <> 1
                            Of 3
                                If wpr:Fault_Code3 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 4
                                If wpr:Fault_Code4 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 5
                                If wpr:Fault_Code5 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 6
                                If wpr:Fault_Code6 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 7
                                If wpr:Fault_Code7 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 8
                                If wpr:Fault_Code8 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 9
                                If wpr:Fault_Code9 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 10
                                If wpr:Fault_Code10 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 11
                                If wpr:Fault_Code11 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 12
                                If wpr:Fault_Code12 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            End ! Case local:KeyRepair
                            ! End (DBH 30/04/2008) #9723

                            ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                            mfo:Field_Number = maf:Field_NUmber
                            Case map:Field_Number
                            Of 1
                                mfo:Field = wpr:Fault_Code1
                            Of 2
                                mfo:Field = wpr:Fault_Code2
                            Of 3
                                mfo:Field = wpr:Fault_Code3
                            Of 4
                                mfo:Field = wpr:Fault_Code4
                            Of 5
                                mfo:Field = wpr:Fault_Code5
                            Of 6
                                mfo:Field = wpr:Fault_Code6
                            Of 7
                                mfo:Field = wpr:Fault_Code7
                            Of 8
                                mfo:Field = wpr:Fault_Code8
                            Of 9
                                mfo:Field = wpr:Fault_Code9
                            Of 10
                                mfo:Field = wpr:Fault_Code10
                            Of 11
                                mfo:Field = wpr:Fault_Code11
                            Of 12
                                mfo:Field = wpr:Fault_Code12
                            End ! Case map:Field_Number
                            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                ! Is there a repair type? (DBH: 23/11/2007)
                                If mfo:RepairType <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:CIndex
                                        local:CFaultCode = mfo:Field
                                        local:CIndex = mfo:ImportanceLevel
                                        local:CRepairType = mfo:RepairType
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairType <> ''
                                If mfo:RepairTypeWarranty <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:WIndex
                                        local:WFaultCode = mfo:Field
                                        local:WIndex = mfo:ImportanceLevel
                                        local:WRepairType = mfo:RepairTypeWarranty
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairTypeWarranty <> ''
                            End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                        End ! Loop

                        ! Check the estimate parts (DBH: 23/11/2007)
                        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Estimate_Accepted') <> 'YES' And p_web.GSV('job:Estimate_Rejected') <> 'YES'
                            Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                            epr:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(epr:Part_Number_Key,epr:Part_Number_Key)
                            Loop
                                If Access:ESTPARTS.Next()
                                    Break
                                End ! If Access:ESTPARTS.Next()
                                If epr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If epr:Ref_Number <> job:Ref_Number

                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If epr:Fault_Code1 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code1 <> 1
                                Of 2
                                    If epr:Fault_Code2 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code2 <> 1
                                Of 3
                                    If epr:Fault_Code3 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 4
                                    If epr:Fault_Code4 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 5
                                    If epr:Fault_Code5 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 6
                                    If epr:Fault_Code6 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 7
                                    If epr:Fault_Code7 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 8
                                    If epr:Fault_Code8 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 9
                                    If epr:Fault_Code9 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 10
                                    If epr:Fault_Code10 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 11
                                    If epr:Fault_Code11 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 12
                                    If epr:Fault_Code12 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = epr:Fault_Code1
                                Of 2
                                    mfo:Field = epr:Fault_Code2
                                Of 3
                                    mfo:Field = epr:Fault_Code3
                                Of 4
                                    mfo:Field = epr:Fault_Code4
                                Of 5
                                    mfo:Field = epr:Fault_Code5
                                Of 6
                                    mfo:Field = epr:Fault_Code6
                                Of 7
                                    mfo:Field = epr:Fault_Code7
                                Of 8
                                    mfo:Field = epr:Fault_Code8
                                Of 9
                                    mfo:Field = epr:Fault_Code9
                                Of 10
                                    mfo:Field = epr:Fault_Code10
                                Of 11
                                    mfo:Field = epr:Fault_Code11
                                Of 12
                                    mfo:Field = epr:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                            End ! Loop
                        Else ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:PARTS.Clearkey(par:Part_Number_Key)
                            par:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.Next()
                                    Break
                                End ! If Access:PARTS.Next()
                                If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If par:Ref_Number <> job:Ref_Number
                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If par:Fault_Code1 <> 1
                                        Cycle
                                    End ! If par:Fault_Code1 <> 1
                                Of 2
                                    If par:Fault_Code2 <> 1
                                        Cycle
                                    End ! If par:Fault_Code2 <> 1
                                Of 3
                                    If par:Fault_Code3 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 4
                                    If par:Fault_Code4 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 5
                                    If par:Fault_Code5 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 6
                                    If par:Fault_Code6 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 7
                                    If par:Fault_Code7 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 8
                                    If par:Fault_Code8 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 9
                                    If par:Fault_Code9 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 10
                                    If par:Fault_Code10 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 11
                                    If par:Fault_Code11 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 12
                                    If par:Fault_Code12 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = par:Fault_Code1
                                Of 2
                                    mfo:Field = par:Fault_Code2
                                Of 3
                                    mfo:Field = par:Fault_Code3
                                Of 4
                                    mfo:Field = par:Fault_Code4
                                Of 5
                                    mfo:Field = par:Fault_Code5
                                Of 6
                                    mfo:Field = par:Fault_Code6
                                Of 7
                                    mfo:Field = par:Fault_Code7
                                Of 8
                                    mfo:Field = par:Fault_Code8
                                Of 9
                                    mfo:Field = par:Fault_Code9
                                Of 10
                                    mfo:Field = par:Fault_Code10
                                Of 11
                                    mfo:Field = par:Fault_Code11
                                Of 12
                                    mfo:Field = par:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign

                            End ! Loop
                        End ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            End ! If man:AutoRepairType = True
        End ! If man:UseInvTextForFaults = True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    ! If the charge types are already filled then assume "overwrite" box has been ticked.
!    ! So don't change. (DBH: 23/11/2007)
!    if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
!        p_web.SSV('job:Repair_Type',local:CRepairType)
!    end ! if (p_web.GSV('jobe:COverwriteRepairType') = 0)
!
!    if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
!        p_web.SSV('job:Warranty_Repair_Type',local:WRepairType)
!    end ! if (p_web.GSV('jobe:WOverwriteRepairType') = 0)

    if (local:CRepairType <> '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType = '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType <> '' and local:WRepairType = '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:Warranty_Job','NO')
            end
            transferParts_C(p_web)
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:warranty_Job','NO')
            end
            p_web.SSV('job:chargeable_Job','YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')


    ! Inserting (DBH 13/06/2006) #6733 - Work out if to use Warranty or Chargeable Fault Code
    local:FaultCode = ''
    if (p_web.GSV('job:Chargeable_Job') <> 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        local:FaultCode = local:WFaultCode
    End ! If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') <> 'YES')
        local:FaultCode = local:CFaultCode
    End ! If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        If local:WFaultCode <> ''
            local:FaultCode = local:WFaultCode
        Else
            local:FaultCode = local:CFaultCode
        End ! If tmp:FaultCodeW_T <> ''
    End ! If job:Chargeable_Job = 'YES' ANd job:Warranty_Job = 'YES'
    ! End (DBH 13/06/2006) #6733



    Access:MANFAULT.Clearkey(maf:mainFaultKey)
    maf:manufacturer    = p_web.GSV('job:manufacturer')
    maf:mainFault    = 1
    if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Found
        if (maf:field_Number < 13)
            p_web.SSV('job:fault_Code' & maf:field_Number,local:faultCode)
        else ! if (maf:field_Number < 13)
            p_web.SSV('wob:faultCode' & maf:field_Number,local:faultCode)
        end ! if (maf:field_Number < 13)
    else ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)

    do closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:MANFAULO.Close
     Access:MANFAUPA.Close
     Access:WARPARTS.Close
     Access:ESTPARTS.Close
     Access:PARTS.Close
     FilesOpened = False
  END
JobFaultCodes        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FaultCode1       STRING(30)                            !
tmp:FaultCode2       STRING(30)                            !
tmp:FaultCode3       STRING(30)                            !
tmp:FaultCode4       STRING(30)                            !
tmp:FaultCode5       STRING(30)                            !
tmp:FaultCode6       STRING(30)                            !
tmp:FaultCode7       STRING(30)                            !
tmp:FaultCode8       STRING(30)                            !
tmp:FaultCode9       STRING(30)                            !
tmp:FaultCode10      STRING(30)                            !
tmp:FaultCode11      STRING(30)                            !
tmp:FaultCode12      STRING(30)                            !
tmp:FaultCode13      STRING(30)                            !
tmp:FaultCode14      STRING(30)                            !
tmp:FaultCode15      STRING(30)                            !
tmp:FaultCode16      STRING(30)                            !
tmp:FaultCode17      STRING(30)                            !
tmp:FaultCode18      STRING(30)                            !
tmp:FaultCode19      STRING(30)                            !
tmp:FaultCode20      STRING(30)                            !
tmp:MSN              STRING(30)                            !
tmp:VerifyMSN        STRING(30)                            !
tmp:ConfirmMSNChange BYTE                                  !
tmp:processExchange  BYTE                                  !
FilesOpened     Long
AUDIT::State  USHORT
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFAULO::State  USHORT
JOBOUTFL::State  USHORT
MANFAULT_ALIAS::State  USHORT
MANFAULT::State  USHORT
WARPARTS::State  USHORT
STOCK::State  USHORT
STOMODEL::State  USHORT
STOMJFAU::State  USHORT
MANFAUPA::State  USHORT
MANFPALO::State  USHORT
TRADEACC::State  USHORT
LOCATION::State  USHORT
MANUFACT::State  USHORT
JOBS::State  USHORT
MODPROD::State  USHORT
SBO_OutFaultParts::State  USHORT
PARTS::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       class
AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber ),Byte
AfterFaultCodeLookup        Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobFaultCodes')
  loc:formname = 'JobFaultCodes_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobFaultCodes','')
    p_web._DivHeader('JobFaultCodes',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
CheckStockModelFaultCodes        Routine
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            If sto:Assign_Fault_Codes = 'YES'
                Access:STOMODEL.Clearkey(stm:Mode_Number_Only_Key)
                stm:Ref_Number = sto:Ref_Number
                stm:Model_NUmber = p_web.GSV('job:Ref_Number')
                If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                    Access:STOMJFAU.Clearkey(stj:FieldKey)
                    stj:RefNumber = stm:RecordNumber
                    If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                        Case maf:Field_Number
                        Of 1
                            If stj:FaultCode1 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/10/2007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/10/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode1
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/10/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/10/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/10/2007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode1)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode1)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode1 <> ''
                        Of 2
                            If stj:FaultCode2 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/20/2007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/20/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode2
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/20/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/20/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/20/2007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode2)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode2)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode2 <> ''

                        Of 3
                            If stj:FaultCode3 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/30/3007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/30/3007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode3
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/30/3007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/30/3007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/30/3007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode3)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode3)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode3 <> ''

                        Of 4
                            If stj:FaultCode4 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 40/40/4007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 40/40/4007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode4
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 40/40/4007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 40/40/4007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 40/40/4007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode4)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode4)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode4 <> ''

                        Of 5
                            If stj:FaultCode5 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 50/50/5007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 50/50/5007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode5
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 50/50/5007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 50/50/5007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 50/50/5007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode5)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode5)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode5 <> ''

                        Of 6
                            If stj:FaultCode6 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 60/60/6007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 60/60/6007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode6
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 60/60/6007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 60/60/6007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 60/60/6007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode6)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode6)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode6 <> ''

                        Of 7
                            If stj:FaultCode7 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 70/70/7007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 70/70/7007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode7
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 70/70/7007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 70/70/7007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 70/70/7007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode7)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode7)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode7 <> ''

                        Of 8
                            If stj:FaultCode8 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 80/80/8008)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 80/80/8008)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode8
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 80/80/8008)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 80/80/8008)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 80/80/8008)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode8)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode8)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode8 <> ''

                        Of 9
                            If stj:FaultCode9 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 90/90/9009)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 90/90/9009)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode9
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 90/90/9009)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 90/90/9009)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 90/90/9009)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode9)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode9)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode9 <> ''

                        Of 10
                            If stj:FaultCode10 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 100/100/100010)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 100/100/100010)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode10
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 100/100/100010)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 100/100/100010)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 100/100/100010)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode10)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode10)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode10 <> ''

                        Of 11
                            If stj:FaultCode11 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 110/110/110011)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 110/110/110011)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode11
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 110/110/110011)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 110/110/110011)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 110/110/110011)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode11)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode11)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode11 <> ''

                        Of 12
                            If stj:FaultCode12 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 120/120/120012)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 120/120/120012)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode12
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 120/120/120012)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 120/120/120012)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 120/120/120012)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode12)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode12)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode12 <> ''

                        Of 13
                            If stj:FaultCode13 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 130/130/130013)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 130/130/130013)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode13
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 130/130/130013)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 130/130/130013)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 130/130/130013)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode13)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode13)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode13 <> ''

                        Of 14
                            If stj:FaultCode14 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 140/140/140014)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 140/140/140014)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode14
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 140/140/140014)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 140/140/140014)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 140/140/140014)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode14)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode14)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode14 <> ''

                        Of 15
                            If stj:FaultCode15 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 150/150/150015)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 150/150/150015)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode15
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 150/150/150015)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 150/150/150015)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 150/150/150015)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode15)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode15)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode15 <> ''

                        Of 16
                            If stj:FaultCode16 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 160/160/160016)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 160/160/160016)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode16
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 160/160/160016)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 160/160/160016)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 160/160/160016)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode16)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode16)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode16 <> ''

                        Of 17
                            If stj:FaultCode17 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 170/170/170017)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 170/170/170017)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode17
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 170/170/170017)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 170/170/170017)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 170/170/170017)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode17)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode17)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode17 <> ''

                        Of 18
                            If stj:FaultCode18 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 180/180/180018)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 180/180/180018)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode18
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 180/180/180018)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 180/180/180018)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 180/180/180018)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode18)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode18)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode18 <> ''

                        Of 19
                            If stj:FaultCode19 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 190/190/190019)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 190/190/190019)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode19
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 190/190/190019)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 190/190/190019)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 190/190/190019)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode19)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode19)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode19 <> ''

                        Of 20
                            If stj:FaultCode20 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 200/200/200020)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 200/200/200020)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode20
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 200/200/200020)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 200/200/200020)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 200/200/200020)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode20)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode20)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode20 <> ''

                        End ! Case maf:Field_Number
                    End ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                End ! If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
            End ! If sto:Assign_Fault_Codes = 'YES'
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
    End ! Loop
buildFaultCodes  routine
    data
locRequired     byte(0)
locFaultRequired    byte(0)
locChargeable   byte(0)
locWarranty     byte(0)
locHide         byte(0)
locReadOnly     byte(0)
locViewUnavailable  byte(0)
locAmendUnavailable byte(0)
locField            String(30)
locRepairIndex      Long()
locSetFaultCode     String(30)
    code
    if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
        loop x# = 1 to 20
            !p_web.SSV('tmp:FaultCode' & x#,'')
            p_web.SSV('Hide:JobFaultCode' & x#,1)
            p_web.SSV('Req:JobFaultCode' & x#,0)
            p_web.SSV('ReadOnly:JobFaultCode' & x#,0)
            p_web.SSV('Prompt:JobFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:JobFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:JobFaultCode' & x#,0)
            p_web.SSV('Lookup:JobFaultCode' & x#,0)
            p_web.SSV('Comment:JobFaultCode' & x#,'')
        end ! loop x# = 1 to 20
    end ! if (p_web.GSV('JobFaultCodes:FirstTime') = 0)

    if (p_web.GSV('job:Chargeable_Job') = 'YES')
        locChargeable = 1
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
    if (p_web.GSV('job:Warranty_Job') = 'YES')
        locWarranty = 1
    end ! if (p_web.GSV('job:Warranty_Job') = 'YES')

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = p_web.GSV('wob:HeadACcountNumber')
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = tra:SiteLocation
        if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Found
        else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Error
        end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'VIEW UNAVAILABLE FAULT CODES') = 0)
        locViewUnavailable = 1
        if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND UNAVAILABLE FAULT CODES') = 0)
            locAmendUnavailable = 1
        end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND UNAVAILABLE FAULT CODES') = 0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'VIEW UNAVAILABLE FAULT CODES') = 0)


    if (locChargeable = 1)
        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Force_Warranty = 'YES')
                if (p_web.GSV('job:Repair_Type') <> '')
                    Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
                    rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                    rtd:Chargeable    = 'YES'
                    rtd:Repair_Type    = p_web.GSV('job:Repair_Type')
                    if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Found
                        if (rtd:CompFaultCoding = 1)
                            locRequired = 1
                        end ! if (rtd:CompFaultCoding = 1)
                    else ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)

                else ! if (p_web.GSV('job:Repair_Type') <> '')
                    locRequired = 1
                end ! if (p_web.GSV('job:Repair_Type') <> '')

            end ! if (cha:Force_Warranty = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
    if (locWarranty = 1 and locRequired <> 1)
        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Warranty_Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Force_Warranty = 'YES')
                if (p_web.GSV('job:Repair_Type') <> '')
                    Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
                    rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                    rtd:Warranty    = 'YES'
                    rtd:Repair_Type    = p_web.GSV('job:Repair_Type_Warranty')
                    if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Found
                        if (rtd:CompFaultCoding = 1)
                            locRequired = 1
                        end ! if (rtd:CompFaultCoding = 1)
                    else ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)

                else ! if (p_web.GSV('job:Repair_Type') <> '')
                    locRequired = 1
                end ! if (p_web.GSV('job:Repair_Type') <> '')
            end !if (cha:Force_Warranty = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
    end ! if (p_web.GSV('job:Warranty_Job') = 'YES')

    Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
    maf:Manufacturer    = p_web.GSV('job:Manufacturer')
    maf:ScreenOrder    = 0
    set(maf:ScreenOrderKey,maf:ScreenOrderKey)
    loop
        if (Access:MANFAULT.Next())
            Break
        end ! if (Access:MANFAULT.Next())
        if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (maf:Manufacturer    <> job:Manufacturer)
        if (maf:ScreenOrder    = 0)
            Cycle
        end ! if (maf:ScreenOrder    <> 0)
        if (maf:MainFault)
            p_web.SSV('ReadOnly:JobFaultCode' & maf:ScreenOrder,1)
            p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,maf:Field_Name)
            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = maf:Manufacturer
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCode' & maf:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,clip(mfo:Description))
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)            
            cycle
        end ! if (maf:MainFault)

        locHide = 0
        locReadOnly = 0

        if (locWarranty = 1)
            if (maf:Compulsory_At_Booking = 'YES')
                if (locRequired)
                    locFaultRequired = 1
                end ! if (locRequired)
            end ! if (maf:Compulsory_At_Booking = 'YES')
            if (maf:Compulsory = 'YES' and locRequired)
                locFaultRequired = 1
            end !if (maf:Compulsory = 'YES' and locRequired)
        end ! if (loc:Warranty = 1)

        if (locChargeable = 1)
            if (maf:CharCompulsoryBooking)
                if (locRequired)
                    locFaultRequired = 1
                end ! if (locRequird)
            end ! if (maf:CharCompulsoryBooking)
            if (maf:CharCompulsory and locRequired)
                locFaultRequired = 1
            end ! if (maf:CharCompulsory and locRequired)
        end ! if (locChargeable = 1)

        if (maf:RestrictAvailability)
            If ((maf:RestrictServiceCentre = 1 and ~loc:Level1) Or |
                (maf:RestrictServiceCentre = 2 and ~loc:Level2) Or |
                (maf:RestrictServiceCentre = 3 and ~loc:Level3))
                if (locViewUnavailable = 0)
                    locHide = 1
                else ! if (locViewUnavailable = 0)
                    if (locAmendUnavailable = 0)
                        locReadOnly = 1
                    end ! if (locAmendUnavailable = 0)
                end ! if (locViewUnavailable = 0)
            end
        end ! if (maf:RestrictAvailability)

        if (maf:NotAvailable)
            if (locViewUnavailable = 0)
                locHide = 1
            else ! if (locViewUnavailable = 0)
                if (locAmendUnavailable = 0)
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        locHide = 1
                    else ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        locReadonly = 1
                    end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                end ! if (locAmendUnavailable = 0)
            end ! if (locViewUnavailable = 0)
        end ! if (maf:NotAvailable)

        if (locHide = 0)
            if (maf:HideRelatedCodeIfBlank)

                Access:MANFAULT_ALIAS.Clearkey(maf_ali:Field_Number_Key)
                maf_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf_ali:Field_Number    = maf:Field_Number
                if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        locHide = 1
                    end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                else ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)

            end ! if (maf:HideRelatedCodeIfBlank)
        end ! if (locHide = 0)

        if (p_web.GSV('job:Third_party_Site') <> '')
            if (locRequired = 1)
                if (maf:NotCompulsoryThirdParty)
                    locRequired = 0
                    if (maf:HideThirdParty)
                        locHide = 1
                    end ! if (maf:HideThirdParty)
                end ! if (maf:NotCompulsoryThirdParty)
            end ! if (locRequired = 1)
        end ! if (p_web.GSV('job:Third_party_Site') <> '')

        if (locHide = 0)
            p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,maf:Field_Name)

            if (locRequired = 0)
                Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                wpr:Ref_Number    = p_web.GSV('job:Ref_Number')
                set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                loop
                    if (Access:WARPARTS.Next())
                        Break
                    end ! if (Access:WARPARTS.Next())
                    if (wpr:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                        Break
                    end ! if (wpr:Ref_Number    <> p_web.GSV('job:Ref_Number'))

                    If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(2,wpr:Fault_Code2,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(3,wpr:Fault_Code3,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(4,wpr:Fault_Code4,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(5,wpr:Fault_Code5,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(6,wpr:Fault_Code6,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(7,wpr:Fault_Code7,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(8,wpr:Fault_Code8,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(9,wpr:Fault_Code9,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(10,wpr:Fault_Code10,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(11,wpr:Fault_Code11,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(12,wpr:Fault_Code12,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                end ! loop
            end ! if (locRequired = 0)

!            if (locRequired)
!                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,1)
!            else ! if (locRequired)
!                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
!            end ! if (locRequired)

            case (maf:Field_Type)
            of 'DATE'
                p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,clip(maf:DateType))
                p_web.SSV('ShowDate:JobFaultCode' & maf:ScreenOrder,1)
            of 'STRING'
                if (maf:RestrictLength)
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s' & maf:LengthTo)
                else ! if (maf:RestrictLength)
                    if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s255')
                    else ! if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s30')
                    end ! if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                end ! if (maf:RestrictLength)

                if (maf:Lookup = 'YES')
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCode' & maf:ScreenOrder)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,clip(mfo:Description))
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                    if (maf:Force_Lookup = 'YES')
                        locReadOnly =1
                    end ! if (maf:Force_Lookup = 'YES')
                else ! if (maf:Lookup = 'YES')
                    p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                end ! if (maf:Lookup = 'YES')
            of 'NUMBER'
                if (maf:RestrictLength)
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@n_' & maf:LengthTo)
                else ! if (maf:RestrictLength)
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@n_9')
                end ! if (maf:RestrictLength)
            end ! case (maf:Field_Type)

            if (maf:GenericFault)
                if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'GENERIC FAULT - AMEND'))
                    locReadOnly = 1
                end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'GENERIC FAULT - AMEND'))
            end ! if (maf:GenericFault)

            if (maf:MainFault)
                locReadOnly = 1
            end ! if (maf:MainFault)

            if (locReadonly)
                p_web.SSV('ReadOnly:JobFaulCode' & maf:ScreenOrder,1)
                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
                p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,0)
            end ! if (locReadonly)

            if (maf:Lookup = 'YES')
                p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,1)
                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) <> '' and |
                    p_web.GSV('Hide:JobFaultCode' & maf:ScreenOrder) = 0 and |
                    p_web.GSV('ReadOnly:JobFaultCode' & maf:ScreenOrder) = 0)

                    Access:MANFAULO.Clearkey(mfo:PrimaryLookupKey)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf:Field_Number
                    mfo:PrimaryLookup    = 1
                    if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                        ! Found
                        if (mfo:JobTypeAvailability = 0)
                            p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                        elsif mfo:JobTypeAvailability = 1
                            if (locChargeable = 1)
                                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                            end ! if (locChargeable = 1)

                        else ! if (mfo:JobTypeAvailability = 0)
                           if (locWarranty = 1)
                                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                            end ! if (locWarranty = 1)
                        end !if (mfo:JobTypeAvailability = 0)
                    else ! if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                        ! Error
                    end ! if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)

                end ! if (maf:Lookup = 'YES')
                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) <> '' and |
                    p_web.GSV('Hide:JobFaultCode' & maf:ScreenOrder) = 0 and |
                    p_web.GSV('ReadOnly:JobFaultCode' & maf:ScreenOrder) = 0)

                    locField = ''
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf:Field_Number
                    set(mfo:Field_Key,mfo:Field_Key)
                    loop
                        if (Access:MANFAULO.Next())
                            Break
                        end ! if (Access:MANFAULO.Next())
                        if (mfo:Manufacturer    <> maf:Manufacturer)
                            Break
                        end ! if (mfo:Manufacturer    <> maf:Manufacturer)
                        if (mfo:Field_Number    <> maf:Field_Number)
                            Break
                        end ! if (mfo:Field_Number    <> maf:Field_Number)

                        if (mfo:JobTypeAvailability = 1)
                            if (locChargeable = 0)
                                cycle
                            end ! if (locWarranty <> 1)
                        end ! if (mfo:JobTypeAvailability = 1)
                        if (mfo:JobTypeAvailability = 2)
                            if (locWarranty = 0)
                                cycle
                            end ! if (locWarranty = 0)
                        end ! if (mfo:JobTypeAvailability = 2)

                        count# += 1
                        if (count# > 1)
                           locField = ''
                            break
                        end ! if (count# > 1)
                        locField = mfo:Field
                    end ! loop
                    if (locField <> '')
                        p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,locField)
                    end ! if (locField <> '')
                end
            end ! if (maf:Lookup = 'YES')

            if (maf:FillFromDOP)
                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                    p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('job:DOP'))
                end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
            end ! if (maf:FillFromDOP)

            if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                do checkStockModelFaultCodes
            end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')

            locRepairIndex = 0
            locSetFaultCode = ''

            Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
            maf_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf_ali:MainFault    = 1
            if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                ! Found

                Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                joo:JobNumber    = p_web.GSV('job:Ref_number')
                set(joo:JobNumberKey,joo:JobNumberKey)
                loop
                    if (Access:JOBOUTFL.Next())
                        Break
                    end ! if (Access:JOBOUTFL.Next())
                    if (joo:JobNumber    <> p_web.GSV('job:Ref_number'))
                        Break
                    end ! if (joo:JobNumber    <> p_web.GSV('job:Ref_number'))

                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf_ali:Field_Number
                    mfo:Field    = joo:FaultCode
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        if (mfo:SetJobFaultCode)
                            if (mfo:SelectJobFaultCode = maf:Field_Number)
                                if (mfo:SkillLevel >= locRepairIndex)
                                    locSetFaultCode = mfo:JobFaultCodeValue
                                end ! if (mfo:SkillLevel >= locRepairIndex)
                            end ! if (mfo:SelectJobFaultCode = maf:Field_Number)
                        end ! if (mfo:SetJobFaultCode)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

                end ! loop
            else ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
            if (locSetFaultCode <> '')
                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,locSetFaultCode)
            end ! if (locSetFaultCode <> '')
        else! if (locHide = 0)
            p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,'')
            p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
            p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,1)
        end ! if (locHide = 0)
    end ! loop
ValidateFaultCodes      routine
    case p_web.GSV('job:manufacturer')
    of 'MOTOROLA' Orof 'SAMSUNG' orof 'PHILIPS'
        if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked')))
            p_web.SSV('locNextURL','ProofOfPurchase')
        else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
            p_web.SSV('job:warranty_Job','YES')
            p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
            if (p_web.GSV('job:POP') = '')
                p_web.SSV('job:POP','F')
            end ! if (p_web.GSV('job:POP') = '')
        end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
    of 'ALCATEL' orof 'SIEMENS'
        if (p_web.GSV('job:fault_code3') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code3'),p_web.GSV('job:date_Booked')))
                p_web.SSV('locNextURL','ProofOfPurchase')
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code3') <> '')
    of 'ERICSSON'
        if (p_web.GSV('job:fault_code7') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code7') & p_web.GSV('job:fault_code8'),p_web.GSV('job:date_Booked')))
                p_web.SSV('locNextURL','ProofOfPurchase')
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code7') <> '')
    of 'BOSCH'
        if (p_web.GSV('job:fault_code7') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code7') ,p_web.GSV('job:date_Booked')))
                p_web.SSV('locNextURL','ProofOfPurchase')
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code7') <> '')
    end ! case p_web.GSV('job:manufacturer')

    if (p_web.GSV('job:DOP') = '')
        p_web.SSV('locNextURL','ProofOfPurchase')
    end ! if (p_web.GSV('job:DOP') = '')
OpenFiles  ROUTINE
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(JOBOUTFL)
  p_web._OpenFile(MANFAULT_ALIAS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOMJFAU)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MODPROD)
  p_web._OpenFile(SBO_OutFaultParts)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(JOBOUTFL)
  p_Web._CloseFile(MANFAULT_ALIAS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOMJFAU)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MODPROD)
  p_Web._CloseFile(SBO_OutFaultParts)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobFaultCodes_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  p_web.deletesessionvalue('JobFaultCodes:FirstTime')

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:FaultCode1')
    p_web.SetPicture('tmp:FaultCode1',p_web.GSV('Picture:JobFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCode1',p_web.GSV('Picture:JobFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCode2')
    p_web.SetPicture('tmp:FaultCode2',p_web.GSV('Picture:JobFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCode2',p_web.GSV('Picture:JobFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCode3')
    p_web.SetPicture('tmp:FaultCode3',p_web.GSV('Picture:JobFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCode3',p_web.GSV('Picture:JobFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCode4')
    p_web.SetPicture('tmp:FaultCode4',p_web.GSV('Picture:JobFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCode4',p_web.GSV('Picture:JobFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCode5')
    p_web.SetPicture('tmp:FaultCode5',p_web.GSV('Picture:JobFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCode5',p_web.GSV('Picture:JobFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCode6')
    p_web.SetPicture('tmp:FaultCode6',p_web.GSV('Picture:JobFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCode6',p_web.GSV('Picture:JobFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCode7')
    p_web.SetPicture('tmp:FaultCode7',p_web.GSV('Picture:JobFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCode7',p_web.GSV('Picture:JobFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCode8')
    p_web.SetPicture('tmp:FaultCode8',p_web.GSV('Picture:JobFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCode8',p_web.GSV('Picture:JobFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCode9')
    p_web.SetPicture('tmp:FaultCode9',p_web.GSV('Picture:JobFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCode9',p_web.GSV('Picture:JobFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCode10')
    p_web.SetPicture('tmp:FaultCode10',p_web.GSV('Picture:JobFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCode10',p_web.GSV('Picture:JobFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCode11')
    p_web.SetPicture('tmp:FaultCode11',p_web.GSV('Picture:JobFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCode11',p_web.GSV('Picture:JobFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCode12')
    p_web.SetPicture('tmp:FaultCode12',p_web.GSV('Picture:JobFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCode12',p_web.GSV('Picture:JobFaultCode12'))
  If p_web.IfExistsValue('tmp:FaultCode13')
    p_web.SetPicture('tmp:FaultCode13',p_web.GSV('Picture:JobFaultCode13'))
  End
  p_web.SetSessionPicture('tmp:FaultCode13',p_web.GSV('Picture:JobFaultCode13'))
  If p_web.IfExistsValue('tmp:FaultCode14')
    p_web.SetPicture('tmp:FaultCode14',p_web.GSV('Picture:JobFaultCode14'))
  End
  p_web.SetSessionPicture('tmp:FaultCode14',p_web.GSV('Picture:JobFaultCode14'))
  If p_web.IfExistsValue('tmp:FaultCode15')
    p_web.SetPicture('tmp:FaultCode15',p_web.GSV('Picture:JobFaultCode15'))
  End
  p_web.SetSessionPicture('tmp:FaultCode15',p_web.GSV('Picture:JobFaultCode15'))
  If p_web.IfExistsValue('tmp:FaultCode16')
    p_web.SetPicture('tmp:FaultCode16',p_web.GSV('Picture:JobFaultCode16'))
  End
  p_web.SetSessionPicture('tmp:FaultCode16',p_web.GSV('Picture:JobFaultCode16'))
  If p_web.IfExistsValue('tmp:FaultCode17')
    p_web.SetPicture('tmp:FaultCode17',p_web.GSV('Picture:JobFaultCode17'))
  End
  p_web.SetSessionPicture('tmp:FaultCode17',p_web.GSV('Picture:JobFaultCode17'))
  If p_web.IfExistsValue('tmp:FaultCode18')
    p_web.SetPicture('tmp:FaultCode18',p_web.GSV('Picture:JobFaultCode18'))
  End
  p_web.SetSessionPicture('tmp:FaultCode18',p_web.GSV('Picture:JobFaultCode18'))
  If p_web.IfExistsValue('tmp:FaultCode19')
    p_web.SetPicture('tmp:FaultCode19',p_web.GSV('Picture:JobFaultCode19'))
  End
  p_web.SetSessionPicture('tmp:FaultCode19',p_web.GSV('Picture:JobFaultCode19'))
  If p_web.IfExistsValue('tmp:FaultCode20')
    p_web.SetPicture('tmp:FaultCode20',p_web.GSV('Picture:JobFaultCode20'))
  End
  p_web.SetSessionPicture('tmp:FaultCode20',p_web.GSV('Picture:JobFaultCode20'))
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('MSNValidation') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:ProductCode'
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODPROD)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode1')
  of 'tmp:FaultCode1'
      local.AfterFaultCodeLookup(1)
  of 'tmp:FaultCode2'
      local.AfterFaultCodeLookup(2)
  of 'tmp:FaultCode3'
      local.AfterFaultCodeLookup(3)
  of 'tmp:FaultCode4'
      local.AfterFaultCodeLookup(4)
  of 'tmp:FaultCode5'
      local.AfterFaultCodeLookup(5)
  of 'tmp:FaultCode6'
      local.AfterFaultCodeLookup(6)
  of 'tmp:FaultCode7'
      local.AfterFaultCodeLookup(7)
  of 'tmp:FaultCode8'
      local.AfterFaultCodeLookup(8)
  of 'tmp:FaultCode9'
      local.AfterFaultCodeLookup(9)
  of 'tmp:FaultCode10'
      local.AfterFaultCodeLookup(10)
  of 'tmp:FaultCode11'
      local.AfterFaultCodeLookup(11)
  of 'tmp:FaultCode12'
      local.AfterFaultCodeLookup(12)
  of 'tmp:FaultCode13'
      local.AfterFaultCodeLookup(13)
  of 'tmp:FaultCode14'
      local.AfterFaultCodeLookup(14)
  of 'tmp:FaultCode15'
      local.AfterFaultCodeLookup(15)
  of 'tmp:FaultCode16'
      local.AfterFaultCodeLookup(16)
  of 'tmp:FaultCode17'
      local.AfterFaultCodeLookup(17)
  of 'tmp:FaultCode18'
      local.AfterFaultCodeLookup(18)
  of 'tmp:FaultCode19'
      local.AfterFaultCodeLookup(19)
  of 'tmp:FaultCode20'
      local.AfterFaultCodeLookup(20)
  
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange)
  p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7)
  p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8)
  p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9)
  p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10)
  p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11)
  p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12)
  p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13)
  p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14)
  p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15)
  p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16)
  p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17)
  p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18)
  p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19)
  p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20)
  p_web.SetSessionValue('tmp:processExchange',tmp:processExchange)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  End
  if p_web.IfExistsValue('tmp:VerifyMSN')
    tmp:VerifyMSN = p_web.GetValue('tmp:VerifyMSN')
    p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  End
  if p_web.IfExistsValue('tmp:ConfirmMSNChange')
    tmp:ConfirmMSNChange = p_web.GetValue('tmp:ConfirmMSNChange')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange)
  End
  if p_web.IfExistsValue('job:ProductCode')
    job:ProductCode = p_web.GetValue('job:ProductCode')
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  End
  if p_web.IfExistsValue('tmp:FaultCode1')
    tmp:FaultCode1 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode1')),p_web.GSV('Picture:JobFaultCode1'))
    p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  End
  if p_web.IfExistsValue('tmp:FaultCode2')
    tmp:FaultCode2 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode2')),p_web.GSV('Picture:JobFaultCode2'))
    p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  End
  if p_web.IfExistsValue('tmp:FaultCode3')
    tmp:FaultCode3 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode3')),p_web.GSV('Picture:JobFaultCode3'))
    p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  End
  if p_web.IfExistsValue('tmp:FaultCode4')
    tmp:FaultCode4 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode4')),p_web.GSV('Picture:JobFaultCode4'))
    p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  End
  if p_web.IfExistsValue('tmp:FaultCode5')
    tmp:FaultCode5 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode5')),p_web.GSV('Picture:JobFaultCode5'))
    p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  End
  if p_web.IfExistsValue('tmp:FaultCode6')
    tmp:FaultCode6 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode6')),p_web.GSV('Picture:JobFaultCode6'))
    p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  End
  if p_web.IfExistsValue('tmp:FaultCode7')
    tmp:FaultCode7 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode7')),p_web.GSV('Picture:JobFaultCode7'))
    p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7)
  End
  if p_web.IfExistsValue('tmp:FaultCode8')
    tmp:FaultCode8 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode8')),p_web.GSV('Picture:JobFaultCode8'))
    p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8)
  End
  if p_web.IfExistsValue('tmp:FaultCode9')
    tmp:FaultCode9 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode9')),p_web.GSV('Picture:JobFaultCode9'))
    p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9)
  End
  if p_web.IfExistsValue('tmp:FaultCode10')
    tmp:FaultCode10 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode10')),p_web.GSV('Picture:JobFaultCode10'))
    p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10)
  End
  if p_web.IfExistsValue('tmp:FaultCode11')
    tmp:FaultCode11 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode11')),p_web.GSV('Picture:JobFaultCode11'))
    p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11)
  End
  if p_web.IfExistsValue('tmp:FaultCode12')
    tmp:FaultCode12 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode12')),p_web.GSV('Picture:JobFaultCode12'))
    p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12)
  End
  if p_web.IfExistsValue('tmp:FaultCode13')
    tmp:FaultCode13 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode13')),p_web.GSV('Picture:JobFaultCode13'))
    p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13)
  End
  if p_web.IfExistsValue('tmp:FaultCode14')
    tmp:FaultCode14 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode14')),p_web.GSV('Picture:JobFaultCode14'))
    p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14)
  End
  if p_web.IfExistsValue('tmp:FaultCode15')
    tmp:FaultCode15 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode15')),p_web.GSV('Picture:JobFaultCode15'))
    p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15)
  End
  if p_web.IfExistsValue('tmp:FaultCode16')
    tmp:FaultCode16 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode16')),p_web.GSV('Picture:JobFaultCode16'))
    p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16)
  End
  if p_web.IfExistsValue('tmp:FaultCode17')
    tmp:FaultCode17 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode17')),p_web.GSV('Picture:JobFaultCode17'))
    p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17)
  End
  if p_web.IfExistsValue('tmp:FaultCode18')
    tmp:FaultCode18 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode18')),p_web.GSV('Picture:JobFaultCode18'))
    p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18)
  End
  if p_web.IfExistsValue('tmp:FaultCode19')
    tmp:FaultCode19 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode19')),p_web.GSV('Picture:JobFaultCode19'))
    p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19)
  End
  if p_web.IfExistsValue('tmp:FaultCode20')
    tmp:FaultCode20 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode20')),p_web.GSV('Picture:JobFaultCode20'))
    p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20)
  End
  if p_web.IfExistsValue('tmp:processExchange')
    tmp:processExchange = p_web.GetValue('tmp:processExchange')
    p_web.SetSessionValue('tmp:processExchange',tmp:processExchange)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobFaultCodes_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.SSV('locNextURL','BillingConfirmation')
      if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
          CalculateBilling(p_web)
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = 0
          set(maf:ScreenOrderKey,maf:ScreenOrderKey)
          loop
              if (Access:MANFAULT.Next())
                  Break
              end ! if (Access:MANFAULT.Next())
              if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (maf:Field_Number < 13)
                 p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('job:Fault_Code' & maf:Field_Number))
              else ! if (maf:ScreenOrder < 13)
                 p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('wob:FaultCode' & maf:Field_Number))
              end ! if (maf:ScreenOrder < 13)
          end ! loop
          p_web.SSV('Hide:ExchangePrompt',1)
          p_web.SSV('tmp:processExchange',0)
          p_web.SSV('tmp:MSN','')
          p_web.SSV('tmp:VerifyMSN','')
          
          ! Verify MSN
          p_web.SSV('MSNValidation',0)
          p_web.SSV('MSNValidated',0)
          p_web.SSV('Hide:ButtonVerifyMSN',1)
          p_web.SSV('Hide:VerifyMSN',1)
          p_web.SSV('Hide:ButtonVerifyMSN2',1)
          p_web.SSV('Hide:ConfirmMSNChange',1)
  
          if (MSNRequired(p_web.GSV('job:Manufacturer')))
              found# = 0
  
              Access:AUDIT.Clearkey(aud:typeRefKey)
              aud:ref_Number    = p_web.GSV('job:Ref_Number')
              aud:type    = 'JOB'
              set(aud:typeRefKey,aud:typeRefKey)
              loop
                  if (Access:AUDIT.Next())
                      Break
                  end ! if (Access:AUDIT.Next())
                  if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (aud:type    <> 'JOB')
                      Break
                  end ! if (aud:type    <> 'JOB')
                  if (instring('MSN VERIFICATION',upper(aud:Action),1,1))
                      found# = 1
                      break
                  end ! if (instring('MSN VERIFICATION',upper(aud:Action),1,1)
              end ! loop
  
              if (found# = 0)
                  p_web.SSV('MSNValidation',1)
                  p_web.SSV('Comment:MSN','Verify MSN')
                  p_web.SSV('Hide:ButtonVerifyMSN',0)
              end ! if (found# = 0)
          end ! if (MSNRequired(p_web.GSV('job:Manufacturer'))        
          
      end ! if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
  
      do buildfaultcodes
  
      p_web.SSV('JobFaultCodes:FirstTime',1)
  
  
      if (productCodeRequired(p_web.GSV('job:Manufacturer') )= 0)
          p_web.SSV('Hide:ProductCode',0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'PRODUCT CODE - AMEND'))
              ! No Lookup
              p_web.SSV('ReadOnly:ProdutCode',1)
          else
              p_web.SSV('ReadOnly:ProdutCode',0)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'PRODUCT CODE - AMEND'))
      else ! if (productCodeRequired(p_web.GSV('job:Manufacturer')))
          p_web.SSV('Hide:ProductCode',1)
      end ! if (productCodeRequired(p_web.GSV('job:Manufacturer')))
  
  
      !Clear Part Out Fault Table
  
      Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
      sofp:sessionID    = p_web.sessionID
      set(sofp:faultKey,sofp:faultKey)
      loop
          if (Access:SBO_OUTFAULTPARTS.Next())
              Break
          end ! if (Access:SBO_OUTFAULTPARTS.Next())
          if (sofp:sessionID    <> p_web.sessionID)
              Break
          end ! if (sofp:sessionID    <> p_web.sessionID)
          access:SBO_OUTFAULTPARTS.deleterecord(0)
      end ! loop
  
      
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:VerifyMSN = p_web.RestoreValue('tmp:VerifyMSN')
 tmp:ConfirmMSNChange = p_web.RestoreValue('tmp:ConfirmMSNChange')
 tmp:FaultCode1 = p_web.RestoreValue('tmp:FaultCode1')
 tmp:FaultCode2 = p_web.RestoreValue('tmp:FaultCode2')
 tmp:FaultCode3 = p_web.RestoreValue('tmp:FaultCode3')
 tmp:FaultCode4 = p_web.RestoreValue('tmp:FaultCode4')
 tmp:FaultCode5 = p_web.RestoreValue('tmp:FaultCode5')
 tmp:FaultCode6 = p_web.RestoreValue('tmp:FaultCode6')
 tmp:FaultCode7 = p_web.RestoreValue('tmp:FaultCode7')
 tmp:FaultCode8 = p_web.RestoreValue('tmp:FaultCode8')
 tmp:FaultCode9 = p_web.RestoreValue('tmp:FaultCode9')
 tmp:FaultCode10 = p_web.RestoreValue('tmp:FaultCode10')
 tmp:FaultCode11 = p_web.RestoreValue('tmp:FaultCode11')
 tmp:FaultCode12 = p_web.RestoreValue('tmp:FaultCode12')
 tmp:FaultCode13 = p_web.RestoreValue('tmp:FaultCode13')
 tmp:FaultCode14 = p_web.RestoreValue('tmp:FaultCode14')
 tmp:FaultCode15 = p_web.RestoreValue('tmp:FaultCode15')
 tmp:FaultCode16 = p_web.RestoreValue('tmp:FaultCode16')
 tmp:FaultCode17 = p_web.RestoreValue('tmp:FaultCode17')
 tmp:FaultCode18 = p_web.RestoreValue('tmp:FaultCode18')
 tmp:FaultCode19 = p_web.RestoreValue('tmp:FaultCode19')
 tmp:FaultCode20 = p_web.RestoreValue('tmp:FaultCode20')
 tmp:processExchange = p_web.RestoreValue('tmp:processExchange')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('locNextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobFaultCodes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobFaultCodes_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobFaultCodes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobFaultCodes" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobFaultCodes" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobFaultCodes" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Fault Codes',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobFaultCodes">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobFaultCodes" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobFaultCodes')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('MSNValidation') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Verify MSN') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Fault Codes') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Tools') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobFaultCodes')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobFaultCodes'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MODPROD'
          If Not (p_web.GSV('Hide:JobFaultCode1') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode1')
          End
    End
  Else
    If False
    ElsIf p_web.GSV('MSNValidation') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:MSN')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:ProductCode')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('MSNValidation') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobFaultCodes')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('MSNValidation') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('MSNValidation') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Verify MSN') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonVerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonVerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:VerifyMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:VerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:VerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonVerifyMSN2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonVerifyMSN2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ConfirmMSNChange
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ConfirmMSNChange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ConfirmMSNChange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ProductCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode13
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode13
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode13
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode14
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode14
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode14
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode15
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode15
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode15
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode16
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode16
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode16
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode17
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode17
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode17
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode18
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode18
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode18
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode19
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode19
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode19
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode20
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode20
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode20
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Tools') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonRepairNotes
      do Comment::buttonRepairNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonOutFaults
      do Comment::buttonOutFaults
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::promptExchange
      do Comment::promptExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:processExchange
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:processExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:processExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:MSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_prompt')

Validate::tmp:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('NewValue'))
    tmp:MSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('Value'))
    tmp:MSN = p_web.GetValue('Value')
  End
  If tmp:MSN = ''
    loc:Invalid = 'tmp:MSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:MSN = Upper(tmp:MSN)
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  do Value::tmp:MSN
  do SendAlert
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN

Value::tmp:MSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:MSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('MSNValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('MSNValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:MSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:MSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:MSN'',''jobfaultcodes_tmp:msn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:MSN')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:MSN',p_web.GetSessionValueFormat('tmp:MSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_value')

Comment::tmp:MSN  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:MSN'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_comment')

Validate::buttonVerifyMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonVerifyMSN',p_web.GetValue('NewValue'))
    do Value::buttonVerifyMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('tmp:MSN') <> '')
      error# = 0
      if (len(clip(p_web.GSV('tmp:MSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON') 
          p_web.SSV('tmp:MSN',sub(p_web.GSV('tmp:MSN'),2,10))
      end ! if (len(clip(p_web.GSV('tmp:MSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
  
      if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN')))
          error# = 1
      else !if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN'))
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              if (man:ApplyMSNFormat)
                  if (checkFaultFormat(p_web.GSV('tmp:MSN'),man:MSNFormat))
                      p_web.SSV('Comment:MSN','M.S.N. Failed Format Validation')
                      error# = 1
                  end ! if (checkFaultFormat(p_web.GSV('tmp:MSN'),man:MSNFormat))
              end ! if (man:ApplyMSNFormat)
          else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      end ! if (checkLength('MSN   ',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN'))
  
      if (error# = 0)
          if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
              p_web.SSV('MSNValidated',1)
              p_web.SSV('Comment:MSN','M.S.N. Verification Passed')
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','MSN VERIFICATION: PASSED')
              p_web.SSV('AddToAudit:Notes','MSN: ' & p_web.GSV('job:MSN'))
              AddToAudit(p_web)
          else ! if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
              p_web.SSV('Hide:VerifyMSN',0)
              p_web.SSV('Hide:ButtonVerifyMSN',1)
              p_web.SSV('Hide:ButtonVerifyMSN2',0)
              p_web.SSV('Comment:MSN','MSN does not match job. Re-Verify entered MSN Below')
              p_web.SSV('Comment:MSN2','Verify MSN')
          end ! if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
      end ! if (error# = 0)
  end ! if (p_web.GSV('tmp:MSN') <> '')
  do Value::buttonVerifyMSN
  do SendAlert
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Value::buttonVerifyMSN2  !1

Value::buttonVerifyMSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_value',Choose(p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonVerifyMSN'',''jobfaultcodes_buttonverifymsn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','VerifyMSN','Verify M.S.N.','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_value')

Comment::buttonVerifyMSN  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_comment',Choose(p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:VerifyMSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_prompt',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Verify M.S.N.')
  If p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_prompt')

Validate::tmp:VerifyMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:VerifyMSN',p_web.GetValue('NewValue'))
    tmp:VerifyMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:VerifyMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:VerifyMSN',p_web.GetValue('Value'))
    tmp:VerifyMSN = p_web.GetValue('Value')
  End
  If tmp:VerifyMSN = ''
    loc:Invalid = 'tmp:VerifyMSN'
    loc:alert = p_web.translate('Verify M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:VerifyMSN = Upper(tmp:VerifyMSN)
    p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  do Value::tmp:VerifyMSN
  do SendAlert

Value::tmp:VerifyMSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_value',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
  ! --- STRING --- tmp:VerifyMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:VerifyMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:VerifyMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:VerifyMSN'',''jobfaultcodes_tmp:verifymsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:VerifyMSN',p_web.GetSessionValueFormat('tmp:VerifyMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_value')

Comment::tmp:VerifyMSN  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:VerifyMSN'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_comment',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_comment')

Validate::buttonVerifyMSN2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonVerifyMSN2',p_web.GetValue('NewValue'))
    do Value::buttonVerifyMSN2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Second MSN Validation
  if (p_web.GSV('tmp:VerifyMSN') <> '')
      error# = 0
      if (len(clip(p_web.GSV('tmp:VerifyMSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON')
          p_web.SSV('tmp:VerifyMSN',sub(p_web.GSV('tmp:VerifyMSN'),2,10))
      end ! if (len(clip(p_web.GSV('tmp:VerifyMSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
  
      if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN')))
          error# = 1
      else !if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN'))
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              if (man:ApplyMSNFormat)
                  if (checkFaultFormat(p_web.GSV('tmp:VerifyMSN'),man:MSNFormat))
                      p_web.SSV('Comment:VerifyMSN','M.S.N. Failed Format Validation')
                      error# = 1
                  end ! if (checkFaultFormat(p_web.GSV('tmp:VerifyMSN'),man:MSNFormat))
              end ! if (man:ApplyMSNFormat)
          else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      end ! if (checkLength('MSN   ',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN'))
  
      if (error# = 0)
          if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('tmp:MSN'))
              p_web.SSV('Hide:ConfirmMSNChange',0)
              p_web.SSV('Hide:ButtonVerifyMSN2',1)
              p_web.SSV('Comment:VerifyMSN','MSNs Match. Confirm Change To Job')
          else ! if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('job:MSN'))
              p_web.SSV('Comment:VerifyMSN','MSN does not match')
          end ! if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('job:MSN'))
      end ! if (error# = 0)
  end ! if (p_web.GSV('tmp:VerifyMSN') <> '')
  do Value::buttonVerifyMSN2
  do SendAlert
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN
  do Value::buttonVerifyMSN  !1
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Prompt::tmp:ConfirmMSNChange
  do Value::tmp:ConfirmMSNChange  !1
  do Comment::tmp:ConfirmMSNChange

Value::buttonVerifyMSN2  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_value',Choose(p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonVerifyMSN2'',''jobfaultcodes_buttonverifymsn2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','VerifyMSN2','Verify M.S.N.','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_value')

Comment::buttonVerifyMSN2  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_comment',Choose(p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ConfirmMSNChange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_prompt',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Change Job MSN?')
  If p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_prompt')

Validate::tmp:ConfirmMSNChange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',p_web.GetValue('NewValue'))
    tmp:ConfirmMSNChange = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ConfirmMSNChange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',p_web.GetValue('Value'))
    tmp:ConfirmMSNChange = p_web.GetValue('Value')
  End
  ! Confirm MSN Change
  if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
      p_web.SSV('MSNValidated',1)
      
      p_web.SSV('tmp:ConfirmMSNChange','')
  
      p_web.SSV('AddToAudit:Type','JOB')
      p_web.SSV('AddToAudit:Action','MSN VERIFICATION: AMENDMENT')
      p_web.SSV('AddToAudit:Notes','ORIGINAL MSN: ' & p_web.GSV('job:MSN') & '<13,10>NEW MSN: ' & p_web.GSV('tmp:MSN'))
      addToAudit(p_web)
      p_web.SSV('job:MSN',p_web.GSV('tmp:MSN'))
      p_web.SSV('Comment:MSN','Verified')
  else ! if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
      p_web.SSV('Hide:ButtonVerifyMSN2',1)
      p_web.SSV('Hide:ConfirmMSNChange',1)
      p_web.SSV('Hide:ButtonVerifyMSN',0)
      p_web.SSV('Hide:VerifyMSN',1)
      p_web.SSV('tmp:ConfirmMSNChange','')
      p_web.SSV('Comment:MSN','Verify MSN')
      p_web.SSV('Comment:VerifyMSN','')
  end  !if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
  do Value::tmp:ConfirmMSNChange
  do SendAlert
  do Prompt::tmp:ConfirmMSNChange
  do Comment::tmp:ConfirmMSNChange
  do Value::buttonVerifyMSN2  !1
  do Value::buttonVerifyMSN  !1
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN

Value::tmp:ConfirmMSNChange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_value',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1)
  ! --- RADIO --- tmp:ConfirmMSNChange
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ConfirmMSNChange')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ConfirmMSNChange') = 'Y'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ConfirmMSNChange'',''jobfaultcodes_tmp:confirmmsnchange_value'',1,'''&clip('Y')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ConfirmMSNChange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ConfirmMSNChange',clip('Y'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ConfirmMSNChange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ConfirmMSNChange') = 'N'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ConfirmMSNChange'',''jobfaultcodes_tmp:confirmmsnchange_value'',1,'''&clip('N')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ConfirmMSNChange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ConfirmMSNChange',clip('N'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ConfirmMSNChange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_value')

Comment::tmp:ConfirmMSNChange  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_comment',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_comment')

Prompt::job:ProductCode  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_prompt',Choose(p_web.GSV('Hide:ProductCode') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Product Code')
  If p_web.GSV('Hide:ProductCode') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ProductCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ProductCode',p_web.GetValue('NewValue'))
    job:ProductCode = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ProductCode
    do Value::job:ProductCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ProductCode',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:ProductCode = p_web.GetValue('Value')
  End
    job:ProductCode = Upper(job:ProductCode)
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  p_Web.SetValue('lookupfield','job:ProductCode')
  do AfterLookup
  do Value::job:ProductCode
  do SendAlert
  do Comment::job:ProductCode

Value::job:ProductCode  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_value',Choose(p_web.GSV('Hide:ProductCode') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ProductCode') = 1)
  ! --- STRING --- job:ProductCode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ProductCode') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:ProductCode') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:ProductCode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:ProductCode'',''jobfaultcodes_job:productcode_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:ProductCode')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:ProductCode',p_web.GetSessionValue('job:ProductCode'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupProductCodes')&'?LookupField=job:ProductCode&Tab=3&ForeignField=mop:ProductCode&_sort=mop:ProductCode&Refresh=sort&LookupFrom=JobFaultCodes&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_value')

Comment::job:ProductCode  Routine
      loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_comment',Choose(p_web.GSV('Hide:ProductCode') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ProductCode') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_comment')

Prompt::tmp:FaultCode1  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode1'))
  If p_web.GSV('Hide:JobFaultCode1') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode1',p_web.GetValue('NewValue'))
    tmp:FaultCode1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode1',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode1')))
    tmp:FaultCode1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode1')) !
  End
  If tmp:FaultCode1 = '' and p_web.GSV('Req:JobFaultCode1') = 1
    loc:Invalid = 'tmp:FaultCode1'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode1
  do SendAlert

Value::tmp:FaultCode1  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_value',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode1') = 1)
  ! --- STRING --- tmp:FaultCode1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode1 = '' and (p_web.GSV('Req:JobFaultCode1') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode1'',''jobfaultcodes_tmp:faultcode1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode1',p_web.GetSessionValue('tmp:FaultCode1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode1'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(1)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_value')

Comment::tmp:FaultCode1  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode1'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode1') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode2  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode2'))
  If p_web.GSV('Hide:JobFaultCode2') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.GetValue('NewValue'))
    tmp:FaultCode2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode2')))
    tmp:FaultCode2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode2')) !
  End
  If tmp:FaultCode2 = '' and p_web.GSV('Req:JobFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCode2'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode2
  do SendAlert

Value::tmp:FaultCode2  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_value',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode2') = 1)
  ! --- STRING --- tmp:FaultCode2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode2 = '' and (p_web.GSV('Req:JobFaultCode2') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''jobfaultcodes_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode2',p_web.GetSessionValue('tmp:FaultCode2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode2'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(2)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_value')

Comment::tmp:FaultCode2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode2'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode2') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode3  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode3'))
  If p_web.GSV('Hide:JobFaultCode3') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.GetValue('NewValue'))
    tmp:FaultCode3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode3')))
    tmp:FaultCode3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode3')) !
  End
  If tmp:FaultCode3 = '' and p_web.GSV('Req:JobFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCode3'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode3
  do SendAlert

Value::tmp:FaultCode3  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_value',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode3') = 1)
  ! --- STRING --- tmp:FaultCode3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode3 = '' and (p_web.GSV('Req:JobFaultCode3') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''jobfaultcodes_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode3',p_web.GetSessionValue('tmp:FaultCode3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode3'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(3)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_value')

Comment::tmp:FaultCode3  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode3'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode3') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode4  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode4'))
  If p_web.GSV('Hide:JobFaultCode4') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.GetValue('NewValue'))
    tmp:FaultCode4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode4')))
    tmp:FaultCode4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode4')) !
  End
  If tmp:FaultCode4 = '' and p_web.GSV('Req:JobFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCode4'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode4
  do SendAlert

Value::tmp:FaultCode4  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_value',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode4') = 1)
  ! --- STRING --- tmp:FaultCode4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode4') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode4') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode4 = '' and (p_web.GSV('Req:JobFaultCode4') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''jobfaultcodes_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode4',p_web.GetSessionValue('tmp:FaultCode4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode4'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(4)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_value')

Comment::tmp:FaultCode4  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode4'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode4') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode5  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode5'))
  If p_web.GSV('Hide:JobFaultCode5') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.GetValue('NewValue'))
    tmp:FaultCode5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode5')))
    tmp:FaultCode5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode5')) !
  End
  If tmp:FaultCode5 = '' and p_web.GSV('Req:JobFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCode5'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode5
  do SendAlert

Value::tmp:FaultCode5  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_value',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode5') = 1)
  ! --- STRING --- tmp:FaultCode5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode5') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode5') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode5 = '' and (p_web.GSV('Req:JobFaultCode5') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''jobfaultcodes_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode5',p_web.GetSessionValue('tmp:FaultCode5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode5'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(5)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_value')

Comment::tmp:FaultCode5  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode5'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode5') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode6  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode6'))
  If p_web.GSV('Hide:JobFaultCode6') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.GetValue('NewValue'))
    tmp:FaultCode6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode6')))
    tmp:FaultCode6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode6')) !
  End
  If tmp:FaultCode6 = '' and p_web.GSV('Req:JobFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCode6'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode6
  do SendAlert

Value::tmp:FaultCode6  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_value',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode6') = 1)
  ! --- STRING --- tmp:FaultCode6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode6') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode6') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode6 = '' and (p_web.GSV('Req:JobFaultCode6') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''jobfaultcodes_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode6',p_web.GetSessionValue('tmp:FaultCode6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode6'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(6)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_value')

Comment::tmp:FaultCode6  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode6'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode6') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode7  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode7'))
  If p_web.GSV('Hide:JobFaultCode7') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.GetValue('NewValue'))
    tmp:FaultCode7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode7')))
    tmp:FaultCode7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode7')) !
  End
  If tmp:FaultCode7 = '' and p_web.GSV('Req:JobFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCode7'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode7
  do SendAlert

Value::tmp:FaultCode7  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_value',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode7') = 1)
  ! --- STRING --- tmp:FaultCode7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode7') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode7') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode7 = '' and (p_web.GSV('Req:JobFaultCode7') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''jobfaultcodes_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode7',p_web.GetSessionValue('tmp:FaultCode7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode7'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(7)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_value')

Comment::tmp:FaultCode7  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode7'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode7') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode8  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode8'))
  If p_web.GSV('Hide:JobFaultCode8') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.GetValue('NewValue'))
    tmp:FaultCode8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode8')))
    tmp:FaultCode8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode8')) !
  End
  If tmp:FaultCode8 = '' and p_web.GSV('Req:JobFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCode8'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode8
  do SendAlert

Value::tmp:FaultCode8  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_value',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode8') = 1)
  ! --- STRING --- tmp:FaultCode8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode8') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode8') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode8 = '' and (p_web.GSV('Req:JobFaultCode8') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''jobfaultcodes_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode8',p_web.GetSessionValue('tmp:FaultCode8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode8'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(8)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_value')

Comment::tmp:FaultCode8  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode8'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode8') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode9  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode9'))
  If p_web.GSV('Hide:JobFaultCode9') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.GetValue('NewValue'))
    tmp:FaultCode9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode9')))
    tmp:FaultCode9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode9')) !
  End
  If tmp:FaultCode9 = '' and p_web.GSV('Req:JobFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCode9'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode9
  do SendAlert

Value::tmp:FaultCode9  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_value',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode9') = 1)
  ! --- STRING --- tmp:FaultCode9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode9') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode9') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode9')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode9 = '' and (p_web.GSV('Req:JobFaultCode9') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''jobfaultcodes_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode9',p_web.GetSessionValue('tmp:FaultCode9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode9'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(9)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_value')

Comment::tmp:FaultCode9  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode9'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode9') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode10  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode10'))
  If p_web.GSV('Hide:JobFaultCode10') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.GetValue('NewValue'))
    tmp:FaultCode10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode10')))
    tmp:FaultCode10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode10')) !
  End
  If tmp:FaultCode10 = '' and p_web.GSV('Req:JobFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCode10'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode10
  do SendAlert

Value::tmp:FaultCode10  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_value',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode10') = 1)
  ! --- STRING --- tmp:FaultCode10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode10') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode10') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode10')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode10 = '' and (p_web.GSV('Req:JobFaultCode10') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''jobfaultcodes_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode10',p_web.GetSessionValue('tmp:FaultCode10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode10'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(10)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_value')

Comment::tmp:FaultCode10  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode10'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode10') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode11  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode11'))
  If p_web.GSV('Hide:JobFaultCode11') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.GetValue('NewValue'))
    tmp:FaultCode11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode11')))
    tmp:FaultCode11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode11')) !
  End
  If tmp:FaultCode11 = '' and p_web.GSV('Req:JobFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCode11'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode11
  do SendAlert

Value::tmp:FaultCode11  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_value',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode11') = 1)
  ! --- STRING --- tmp:FaultCode11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode11') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode11') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode11')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode11 = '' and (p_web.GSV('Req:JobFaultCode11') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''jobfaultcodes_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode11',p_web.GetSessionValue('tmp:FaultCode11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode11'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(11)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_value')

Comment::tmp:FaultCode11  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode11'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode11') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode12  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode12'))
  If p_web.GSV('Hide:JobFaultCode12') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.GetValue('NewValue'))
    tmp:FaultCode12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode12')))
    tmp:FaultCode12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode12')) !
  End
  If tmp:FaultCode12 = '' and p_web.GSV('Req:JobFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCode12'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode12
  do SendAlert

Value::tmp:FaultCode12  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_value',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode12') = 1)
  ! --- STRING --- tmp:FaultCode12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode12') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode12') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode12')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode12 = '' and (p_web.GSV('Req:JobFaultCode12') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''jobfaultcodes_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode12',p_web.GetSessionValue('tmp:FaultCode12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode12'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(12)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_value')

Comment::tmp:FaultCode12  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode12'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode12') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode13  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode13'))
  If p_web.GSV('Hide:JobFaultCode13') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode13  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode13',p_web.GetValue('NewValue'))
    tmp:FaultCode13 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode13
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode13',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode13')))
    tmp:FaultCode13 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode13')) !
  End
  If tmp:FaultCode13 = '' and p_web.GSV('Req:JobFaultCode13') = 1
    loc:Invalid = 'tmp:FaultCode13'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode13')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode13
  do SendAlert

Value::tmp:FaultCode13  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_value',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode13') = 1)
  ! --- STRING --- tmp:FaultCode13
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode13') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode13') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode13') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode13')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode13 = '' and (p_web.GSV('Req:JobFaultCode13') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode13'',''jobfaultcodes_tmp:faultcode13_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode13',p_web.GetSessionValue('tmp:FaultCode13'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode13'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(13)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_value')

Comment::tmp:FaultCode13  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode13'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode13') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode14  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode14'))
  If p_web.GSV('Hide:JobFaultCode14') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode14  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode14',p_web.GetValue('NewValue'))
    tmp:FaultCode14 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode14
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode14',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode14')))
    tmp:FaultCode14 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode14')) !
  End
  If tmp:FaultCode14 = '' and p_web.GSV('Req:JobFaultCode14') = 1
    loc:Invalid = 'tmp:FaultCode14'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode14')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode14
  do SendAlert

Value::tmp:FaultCode14  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_value',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode14') = 1)
  ! --- STRING --- tmp:FaultCode14
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode14') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode14') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode14') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode14')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode14 = '' and (p_web.GSV('Req:JobFaultCode14') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode14'',''jobfaultcodes_tmp:faultcode14_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode14',p_web.GetSessionValue('tmp:FaultCode14'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode14'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(14)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_value')

Comment::tmp:FaultCode14  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode14'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode14') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode15  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode15'))
  If p_web.GSV('Hide:JobFaultCode15') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode15  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode15',p_web.GetValue('NewValue'))
    tmp:FaultCode15 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode15
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode15',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode15')))
    tmp:FaultCode15 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode15')) !
  End
  If tmp:FaultCode15 = '' and p_web.GSV('Req:JobFaultCode15') = 1
    loc:Invalid = 'tmp:FaultCode15'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode15')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode15
  do SendAlert

Value::tmp:FaultCode15  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_value',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode15') = 1)
  ! --- STRING --- tmp:FaultCode15
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode15') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode15') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode15') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode15')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode15 = '' and (p_web.GSV('Req:JobFaultCode15') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode15'',''jobfaultcodes_tmp:faultcode15_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode15',p_web.GetSessionValue('tmp:FaultCode15'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode15'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(15)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_value')

Comment::tmp:FaultCode15  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode15'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode15') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode16  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode16'))
  If p_web.GSV('Hide:JobFaultCode16') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode16  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode16',p_web.GetValue('NewValue'))
    tmp:FaultCode16 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode16
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode16',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode16')))
    tmp:FaultCode16 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode16')) !
  End
  If tmp:FaultCode16 = '' and p_web.GSV('Req:JobFaultCode16') = 1
    loc:Invalid = 'tmp:FaultCode16'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode16')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode16
  do SendAlert

Value::tmp:FaultCode16  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_value',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode16') = 1)
  ! --- STRING --- tmp:FaultCode16
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode16') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode16') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode16') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode16')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode16 = '' and (p_web.GSV('Req:JobFaultCode16') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode16'',''jobfaultcodes_tmp:faultcode16_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode16',p_web.GetSessionValue('tmp:FaultCode16'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode16'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(16)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_value')

Comment::tmp:FaultCode16  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode16'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode16') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode17  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode17'))
  If p_web.GSV('Hide:JobFaultCode17') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode17  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode17',p_web.GetValue('NewValue'))
    tmp:FaultCode17 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode17
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode17',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode17')))
    tmp:FaultCode17 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode17')) !
  End
  If tmp:FaultCode17 = '' and p_web.GSV('Req:JobFaultCode17') = 1
    loc:Invalid = 'tmp:FaultCode17'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode17')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode17
  do SendAlert

Value::tmp:FaultCode17  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_value',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode17') = 1)
  ! --- STRING --- tmp:FaultCode17
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode17') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode17') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode17') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode17')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode17 = '' and (p_web.GSV('Req:JobFaultCode17') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode17'',''jobfaultcodes_tmp:faultcode17_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode17',p_web.GetSessionValue('tmp:FaultCode17'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode17'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(17)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_value')

Comment::tmp:FaultCode17  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode17'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode17') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode18  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode18'))
  If p_web.GSV('Hide:JobFaultCode18') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode18  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode18',p_web.GetValue('NewValue'))
    tmp:FaultCode18 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode18
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode18',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode18')))
    tmp:FaultCode18 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode18')) !
  End
  If tmp:FaultCode18 = '' and p_web.GSV('Req:JobFaultCode18') = 1
    loc:Invalid = 'tmp:FaultCode18'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode18')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode18
  do SendAlert

Value::tmp:FaultCode18  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_value',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode18') = 1)
  ! --- STRING --- tmp:FaultCode18
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode18') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode18') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode18') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode18')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode18 = '' and (p_web.GSV('Req:JobFaultCode18') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode18'',''jobfaultcodes_tmp:faultcode18_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode18',p_web.GetSessionValue('tmp:FaultCode18'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode18'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(18)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_value')

Comment::tmp:FaultCode18  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode18'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode18') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode19  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode19'))
  If p_web.GSV('Hide:JobFaultCode19') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode19  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode19',p_web.GetValue('NewValue'))
    tmp:FaultCode19 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode19
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode19',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode19')))
    tmp:FaultCode19 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode19')) !
  End
  If tmp:FaultCode19 = '' and p_web.GSV('Req:JobFaultCode19') = 1
    loc:Invalid = 'tmp:FaultCode19'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode19')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode19
  do SendAlert

Value::tmp:FaultCode19  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_value',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode19') = 1)
  ! --- STRING --- tmp:FaultCode19
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode19') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode19') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode19') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode19')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode19 = '' and (p_web.GSV('Req:JobFaultCode19') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode19'',''jobfaultcodes_tmp:faultcode19_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode19',p_web.GetSessionValue('tmp:FaultCode19'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode19'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(19)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_value')

Comment::tmp:FaultCode19  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode19'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode19') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode20  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode20'))
  If p_web.GSV('Hide:JobFaultCode20') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode20  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode20',p_web.GetValue('NewValue'))
    tmp:FaultCode20 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode20
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode20',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode20')))
    tmp:FaultCode20 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode20')) !
  End
  If tmp:FaultCode20 = '' and p_web.GSV('Req:JobFaultCode20') = 1
    loc:Invalid = 'tmp:FaultCode20'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode20')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode20
  do SendAlert

Value::tmp:FaultCode20  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_value',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode20') = 1)
  ! --- STRING --- tmp:FaultCode20
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode20') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode20') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode20') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode20')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode20 = '' and (p_web.GSV('Req:JobFaultCode20') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode20'',''jobfaultcodes_tmp:faultcode20_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode20',p_web.GetSessionValue('tmp:FaultCode20'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode20'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(20)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_value')

Comment::tmp:FaultCode20  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode20'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode20') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonRepairNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRepairNotes',p_web.GetValue('NewValue'))
    do Value::buttonRepairNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonRepairNotes  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonRepairNotes') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RepairNotes','Repair Notes','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseRepairNotes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonRepairNotes  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonRepairNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonOutFaults  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonOutFaults',p_web.GetValue('NewValue'))
    do Value::buttonOutFaults
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonOutFaults  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonOutFaults') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','OutFaults','Edit Out Faults List','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayOutFaults')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonOutFaults  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonOutFaults') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::promptExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('promptExchange',p_web.GetValue('NewValue'))
    do Value::promptExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::promptExchange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('promptExchange') & '_value',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangePrompt') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('You have requested an Exchange Unit for this repair.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::promptExchange  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('promptExchange') & '_comment',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:processExchange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_prompt',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Process Exchange Request?')
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:processExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:processExchange',p_web.GetValue('NewValue'))
    tmp:processExchange = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:processExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:processExchange',p_web.GetValue('Value'))
    tmp:processExchange = p_web.GetValue('Value')
  End
  do Value::tmp:processExchange
  do SendAlert

Value::tmp:processExchange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_value',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangePrompt') = 1)
  ! --- RADIO --- tmp:processExchange
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:processExchange')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  if p_web.GSV('job:Warranty_Job') = 'YES'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Add Warranty Exchange') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  if p_web.GSV('job:Chargeable_Job') = 'YES'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Add Chargeable Exchange') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Cancel Exchange Request') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_value')

Comment::tmp:processExchange  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_comment',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobFaultCodes_tmp:MSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:MSN
      else
        do Value::tmp:MSN
      end
  of lower('JobFaultCodes_buttonVerifyMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonVerifyMSN
      else
        do Value::buttonVerifyMSN
      end
  of lower('JobFaultCodes_tmp:VerifyMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:VerifyMSN
      else
        do Value::tmp:VerifyMSN
      end
  of lower('JobFaultCodes_buttonVerifyMSN2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonVerifyMSN2
      else
        do Value::buttonVerifyMSN2
      end
  of lower('JobFaultCodes_tmp:ConfirmMSNChange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ConfirmMSNChange
      else
        do Value::tmp:ConfirmMSNChange
      end
  of lower('JobFaultCodes_job:ProductCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ProductCode
      else
        do Value::job:ProductCode
      end
  of lower('JobFaultCodes_tmp:FaultCode1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode1
      else
        do Value::tmp:FaultCode1
      end
  of lower('JobFaultCodes_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('JobFaultCodes_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('JobFaultCodes_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('JobFaultCodes_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('JobFaultCodes_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('JobFaultCodes_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('JobFaultCodes_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('JobFaultCodes_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('JobFaultCodes_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('JobFaultCodes_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('JobFaultCodes_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  of lower('JobFaultCodes_tmp:FaultCode13_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode13
      else
        do Value::tmp:FaultCode13
      end
  of lower('JobFaultCodes_tmp:FaultCode14_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode14
      else
        do Value::tmp:FaultCode14
      end
  of lower('JobFaultCodes_tmp:FaultCode15_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode15
      else
        do Value::tmp:FaultCode15
      end
  of lower('JobFaultCodes_tmp:FaultCode16_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode16
      else
        do Value::tmp:FaultCode16
      end
  of lower('JobFaultCodes_tmp:FaultCode17_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode17
      else
        do Value::tmp:FaultCode17
      end
  of lower('JobFaultCodes_tmp:FaultCode18_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode18
      else
        do Value::tmp:FaultCode18
      end
  of lower('JobFaultCodes_tmp:FaultCode19_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode19
      else
        do Value::tmp:FaultCode19
      end
  of lower('JobFaultCodes_tmp:FaultCode20_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode20
      else
        do Value::tmp:FaultCode20
      end
  of lower('JobFaultCodes_tmp:processExchange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:processExchange
      else
        do Value::tmp:processExchange
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)

PreCopy  Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobFaultCodes:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('MSNValidation') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobFaultCodes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('MSNValidation') = 1 And p_web.GSV('MSNValidated') = 0)
          loc:alert = 'You must Verify the MSN'
          loc:invalid = 'tmp:MSN'
          exit
      end ! if (p_web.GSV('MSNValidation') = 1 And p_web.GSV('MSNValidated') = 0)
  
      ! Double Check Fault Code Values
  
      loop x# = 1 to 20
          if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 or |
              p_web.GSV('ReadOnly:JobFaultCode' & x#) = 1 or |
              p_web.GSV('Lookup:JobFaultCode' & x#) <> 1 or |
              p_web.GSV('tmp:FaultCode' & x#) = '')
              cycle
          end ! p_web.GSV('ReadOnly:JobFaultCode' & x#) = 1)
          
  
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:ScreenOrder    = x#
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (maf:Lookup = 'YES' and maf:Force_Lookup = 'YES' and maf:MainFault <> 1)
                  Access:MANFAULO.Clearkey(mfo:HideFieldKey)
                  mfo:NotAvailable    = 0
                  mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfo:Field_Number    = maf:Field_Number
                  mfo:Field    = p_web.GSV('tmp:FaultCode' & x#)
                  if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
                      ! Found
                  else ! if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
                      ! Error
                      loc:Invalid = 'tmp:FaultCode' & x#
                      loc:Alert = 'Invalid Fault Code: ' & p_web.GSV('Prompt:JobFaultCode' & x#)
                      break
                  end ! if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
              end ! if (maf:Lookup = 'YES' and maf:Force_Lookup = 'YES')
          else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)        
      end ! loop x# = 1 to 20
  
      if (loc:Invalid = '' And p_web.GSV('Hide:ExchangePrompt') = 0 And p_web.GSV('tmp:ProcessExchange') < 1)
          loc:alert = 'Please select how you wish to process the Exchange Request'
          loc:invalid = 'tmp:promptExchange'
          exit
      end ! if (loc:Invalid = '' And p_web.GSV('Hide:ExchangePrompt') = 0 And p_web.GSV('tmp:ProcessExchange') = 0)
  
  
  p_web.DeleteSessionValue('JobFaultCodes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('MSNValidation') = 1
    loc:InvalidTab += 1
        If tmp:MSN = ''
          loc:Invalid = 'tmp:MSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:MSN = Upper(tmp:MSN)
          p_web.SetSessionValue('tmp:MSN',tmp:MSN)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
        If tmp:VerifyMSN = ''
          loc:Invalid = 'tmp:VerifyMSN'
          loc:alert = p_web.translate('Verify M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:VerifyMSN = Upper(tmp:VerifyMSN)
          p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
        If loc:Invalid <> '' then exit.
      End
  End
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:ProductCode') = 1)
          job:ProductCode = Upper(job:ProductCode)
          p_web.SetSessionValue('job:ProductCode',job:ProductCode)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode1') = 1)
        If tmp:FaultCode1 = '' and p_web.GSV('Req:JobFaultCode1') = 1
          loc:Invalid = 'tmp:FaultCode1'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode2') = 1)
        If tmp:FaultCode2 = '' and p_web.GSV('Req:JobFaultCode2') = 1
          loc:Invalid = 'tmp:FaultCode2'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode3') = 1)
        If tmp:FaultCode3 = '' and p_web.GSV('Req:JobFaultCode3') = 1
          loc:Invalid = 'tmp:FaultCode3'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode4') = 1)
        If tmp:FaultCode4 = '' and p_web.GSV('Req:JobFaultCode4') = 1
          loc:Invalid = 'tmp:FaultCode4'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode5') = 1)
        If tmp:FaultCode5 = '' and p_web.GSV('Req:JobFaultCode5') = 1
          loc:Invalid = 'tmp:FaultCode5'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode6') = 1)
        If tmp:FaultCode6 = '' and p_web.GSV('Req:JobFaultCode6') = 1
          loc:Invalid = 'tmp:FaultCode6'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode7') = 1)
        If tmp:FaultCode7 = '' and p_web.GSV('Req:JobFaultCode7') = 1
          loc:Invalid = 'tmp:FaultCode7'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode8') = 1)
        If tmp:FaultCode8 = '' and p_web.GSV('Req:JobFaultCode8') = 1
          loc:Invalid = 'tmp:FaultCode8'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode9') = 1)
        If tmp:FaultCode9 = '' and p_web.GSV('Req:JobFaultCode9') = 1
          loc:Invalid = 'tmp:FaultCode9'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode10') = 1)
        If tmp:FaultCode10 = '' and p_web.GSV('Req:JobFaultCode10') = 1
          loc:Invalid = 'tmp:FaultCode10'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode11') = 1)
        If tmp:FaultCode11 = '' and p_web.GSV('Req:JobFaultCode11') = 1
          loc:Invalid = 'tmp:FaultCode11'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode12') = 1)
        If tmp:FaultCode12 = '' and p_web.GSV('Req:JobFaultCode12') = 1
          loc:Invalid = 'tmp:FaultCode12'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode13') = 1)
        If tmp:FaultCode13 = '' and p_web.GSV('Req:JobFaultCode13') = 1
          loc:Invalid = 'tmp:FaultCode13'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode13')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode14') = 1)
        If tmp:FaultCode14 = '' and p_web.GSV('Req:JobFaultCode14') = 1
          loc:Invalid = 'tmp:FaultCode14'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode14')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode15') = 1)
        If tmp:FaultCode15 = '' and p_web.GSV('Req:JobFaultCode15') = 1
          loc:Invalid = 'tmp:FaultCode15'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode15')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode16') = 1)
        If tmp:FaultCode16 = '' and p_web.GSV('Req:JobFaultCode16') = 1
          loc:Invalid = 'tmp:FaultCode16'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode16')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode17') = 1)
        If tmp:FaultCode17 = '' and p_web.GSV('Req:JobFaultCode17') = 1
          loc:Invalid = 'tmp:FaultCode17'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode17')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode18') = 1)
        If tmp:FaultCode18 = '' and p_web.GSV('Req:JobFaultCode18') = 1
          loc:Invalid = 'tmp:FaultCode18'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode18')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode19') = 1)
        If tmp:FaultCode19 = '' and p_web.GSV('Req:JobFaultCode19') = 1
          loc:Invalid = 'tmp:FaultCode19'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode19')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode20') = 1)
        If tmp:FaultCode20 = '' and p_web.GSV('Req:JobFaultCode20') = 1
          loc:Invalid = 'tmp:FaultCode20'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode20')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      !Write Back The Fault Codes
      loop x# = 1 to 20
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = x#
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (p_web.GSV('Hide:JobFaultCode' & x#) <> 1 and p_web.GSV('ReadOnly:JobFaultCode' & x#) <> 1)
                  if (maf:Field_Number < 13)
                      p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('tmp:FaultCode' & x#))
                  else ! if (maf:Field_Number < 13)
                      p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('tmp:FaultCode' & x#))
                  end ! if (maf:Field_Number < 13)
              end !if (p_web.GSV('Hide:JobFaultCode') <> 1)
          else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
      end !loop x# = 1 to 20
  
      do ValidateFaultCodes
  
      IF (p_web.GSV('Job:ViewOnly') = 1)
          ! Don't call POP screen if view only job.
          p_web.SSV('locNextURL','BillingConfirmation')
      END
  
  
      if (p_web.GSV('Hide:ExchangePrompt') = 0)
          if (p_web.GSV('tmp:processExchange') = 1)
              local.AllocateExchangePart('CHA',0,0)
          end
          if (p_web.GSV('tmp:processExchange') = 2)
              local.AllocateExchangePart('CHA',0,0)
          end
      end
  
  
      p_web.deletesessionvalue('JobFaultCodes:FirstTime')
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:VerifyMSN')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ConfirmMSNChange')
  p_web.StoreValue('job:ProductCode')
  p_web.StoreValue('tmp:FaultCode1')
  p_web.StoreValue('tmp:FaultCode2')
  p_web.StoreValue('tmp:FaultCode3')
  p_web.StoreValue('tmp:FaultCode4')
  p_web.StoreValue('tmp:FaultCode5')
  p_web.StoreValue('tmp:FaultCode6')
  p_web.StoreValue('tmp:FaultCode7')
  p_web.StoreValue('tmp:FaultCode8')
  p_web.StoreValue('tmp:FaultCode9')
  p_web.StoreValue('tmp:FaultCode10')
  p_web.StoreValue('tmp:FaultCode11')
  p_web.StoreValue('tmp:FaultCode12')
  p_web.StoreValue('tmp:FaultCode13')
  p_web.StoreValue('tmp:FaultCode14')
  p_web.StoreValue('tmp:FaultCode15')
  p_web.StoreValue('tmp:FaultCode16')
  p_web.StoreValue('tmp:FaultCode17')
  p_web.StoreValue('tmp:FaultCode18')
  p_web.StoreValue('tmp:FaultCode19')
  p_web.StoreValue('tmp:FaultCode20')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:processExchange')
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:TabNumber)
    if loc:LookupDone
        ! Prompt For Exchange
        p_web.SSV('Hide:ExchangePrompt',1)
        loop x# = 1 to 20
            if (p_web.GSV('Hide:JobFaultCode' & x#) = 1)
                cycle
            end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1)

            Access:MANFAULT.Clearkey(maf:screenOrderKey)
            maf:Manufacturer    = p_web.GSV('job:manufacturer')
            maf:screenOrder    = x#
            if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)
                ! Found
                Access:MANFAULO.Clearkey(mfo:field_Key)
                mfo:manufacturer    = p_web.GSV('job:manufacturer')
                mfo:field_Number    = maf:field_number
                mfo:field    = p_web.GSV('tmp:FaultCode' & x#)
                if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)
                    ! Found
                    if (mfo:promptForExchange)
                        p_web.SSV('Hide:ExchangePrompt',0)
                        break
                    end ! if (mfo:promptForExchange)
                else ! if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)

            else ! if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)

        end ! loop x# = 1 to 20
!        do buildFaultCodes
!        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode' & fNumber)
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
local:SecondExchangeUnit        Byte(0)
local:ExchangePartAttached      Byte(0)
local:AttachUnit                Byte(0)
local:AttachSecondUnit          Byte(0)
local:FoundExchange             Byte(0)
Code
    Access:USERS.Clearkey(use:user_code_key)
    use:user_code    = p_web.GSV('job:engineer')
    if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Found
    else ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Error
    end ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)


    Access:STOCK.Clearkey(sto:location_key)
    sto:location    = use:location
    sto:part_number    = 'EXCH'
    if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)

    Case func:Type
        Of 'WAR'
            !Check to see if an Exchange Part line already exists -  (DBH: 19-12-2003)
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = p_web.GSV('job:ref_number')
            wpr:Part_Number = 'EXCH'
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> p_web.GSV('job:ref_number')      |
                Or wpr:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit = True
                    If wpr:SecondExchangeUnit
                        Return
                    End !If func:SecondUnit And wpr:SecondExchangeUnit
                Else !If func:SecondUnit = True
                    Return
                End !If func:SecondUnit = True
            End !Loop

            If Access:WARPARTS.PrimeRecord() = Level:Benign
                wpr:Part_Ref_Number = sto:Ref_Number
                wpr:Ref_Number      = p_web.GSV('job:ref_number')
                wpr:Adjustment      = 'YES'
                wpr:Part_Number     = 'EXCH'
                wpr:Description     = Clip(p_web.GSV('job:manufacturer')) & ' EXCHANGE UNIT'
                wpr:Quantity        = 1
                wpr:Warranty_Part   = 'NO'
                wpr:Exclude_From_Order = 'YES'
                wpr:PartAllocated   = func:Allocated
                If func:Allocated = 0
                    wpr:Status  = 'REQ'
                Else !If func:Allocated = 0
                    wpr:Status  = 'PIK'
                End !If func:Allocated = 0
                wpr:ExchangeUnit        = True
                wpr:SecondExchangeUnit = func:SecondUnit

                If sto:Assign_Fault_Codes = 'YES'
                    !Try and get the fault codes. This key should get the only record
                    Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                    stm:Ref_Number  = sto:Ref_Number
                    stm:Part_Number = sto:Part_Number
                    stm:Location    = sto:Location
                    stm:Description = sto:Description
                    If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Found
                        wpr:Fault_Code1  = stm:FaultCode1
                        wpr:Fault_Code2  = stm:FaultCode2
                        wpr:Fault_Code3  = stm:FaultCode3
                        wpr:Fault_Code4  = stm:FaultCode4
                        wpr:Fault_Code5  = stm:FaultCode5
                        wpr:Fault_Code6  = stm:FaultCode6
                        wpr:Fault_Code7  = stm:FaultCode7
                        wpr:Fault_Code8  = stm:FaultCode8
                        wpr:Fault_Code9  = stm:FaultCode9
                        wpr:Fault_Code10 = stm:FaultCode10
                        wpr:Fault_Code11 = stm:FaultCode11
                        wpr:Fault_Code12 = stm:FaultCode12
                    Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign

                End !If sto:Assign_Fault_Codes = 'YES'
                If Access:WARPARTS.TryInsert() = Level:Benign
                    !Insert Successful
                    p_web.SSV('AddToStockAllocation:Type','WAR')
                    p_web.SSV('AddToStockAllocation:Status','')
                    p_web.SSV('AddToStockAllocation:Qty',1)
                    addToStockAllocation(p_web)  
                Else !If Access:WARPARTS.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:WARPARTS.CancelAutoInc()
                End !If Access:WARPARTS.TryInsert() = Level:Benign
            End !If Access:WARPARTS.PrimeRecord() = Level:Benign

        Of 'CHA'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:ref_number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:ref_number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit = True
                    If par:SecondExchangeUnit
                        Return
                    End !If func:SecondUnit And wpr:SecondExchangeUnit
                Else !If func:SecondUnit = True
                    Return
                End !If func:SecondUnit = True
            End !Loop

            if access:parts.primerecord() = level:benign
                par:PArt_Ref_Number      = sto:Ref_Number
                par:ref_number            = p_web.GSV('job:ref_number')
                par:adjustment            = 'YES'
                par:part_number           = 'EXCH'
                par:description           = Clip(p_web.GSV('job:manufacturer')) & ' EXCHANGE UNIT'
                par:quantity              = 1
                par:warranty_part         = 'NO'
                par:exclude_from_order    = 'YES'
                par:PartAllocated         = func:Allocated
                par:ExchangeUnit        = True
                par:SecondExchangeUnit = func:SecondUnit

                !see above for confusion
                if func:allocated = 0 then
                    par:Status            = 'REQ'
                ELSE
                    par:status            = 'PIK'
                END
                If sto:Assign_Fault_Codes = 'YES'
                   !Try and get the fault codes. This key should get the only record
                   Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                   stm:Ref_Number  = sto:Ref_Number
                   stm:Part_Number = sto:Part_Number
                   stm:Location    = sto:Location
                   stm:Description = sto:Description
                   If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Found
                       par:Fault_Code1  = stm:FaultCode1
                       par:Fault_Code2  = stm:FaultCode2
                       par:Fault_Code3  = stm:FaultCode3
                       par:Fault_Code4  = stm:FaultCode4
                       par:Fault_Code5  = stm:FaultCode5
                       par:Fault_Code6  = stm:FaultCode6
                       par:Fault_Code7  = stm:FaultCode7
                       par:Fault_Code8  = stm:FaultCode8
                       par:Fault_Code9  = stm:FaultCode9
                       par:Fault_Code10 = stm:FaultCode10
                       par:Fault_Code11 = stm:FaultCode11
                       par:Fault_Code12 = stm:FaultCode12
                   Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                   End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                End !If sto:Assign_Fault_Codes = 'YES'
                if access:parts.insert()
                    access:parts.cancelautoinc()
                end
                p_web.SSV('AddToStockAllocation:Type','CHA')
                p_web.SSV('AddToStockAllocation:Status','')
                p_web.SSV('AddToStockAllocation:Qty',1)
                addToStockAllocation(p_web)  
            End !If access:Prime
    End !Case func:Type
local.PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber )
Code
    If f:FaultCode = ''
        Return 0
    End ! If f:FaultCode = ''

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = f:PartFieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        If map:NotAvailable = 0
            Access:MANFPALO.Clearkey(mfp:Field_Key)
            mfp:Manufacturer = job:Manufacturer
            mfp:Field_Number = f:PartFieldNumber
            mfp:Field = f:FaultCode
            If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                If mfp:ForceJobFaultCode
                    If mfp:ForceFaultCodeNumber = f:JobFieldNumber
                        Return 1
                    End ! If mfp:ForceFaultCodeNumber= maf:Field_Number
                End ! If mfp:ForceJobFaultCode
            Else ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            End ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
        End ! If map:NotAvailable = 0
    Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    Return 0
local.SetLookupButton      Procedure(Long fNumber)
Code
    if (p_web.GSV('ReadOnly:JobFaultCode' & fNumber) = 1)
        return
    end ! if (p_web.GSV('ReadOnly:JobFaultCode' & fNumber) = 1)
    if (p_web.GSV('ShowDate:JobFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''JobFaultCodes_pickdate_value'',1,FieldValue(this,1));nextFocus(JobFaultCodes_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:JobFaultCode' & fNumber) = 1)

        Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:ScreenOrder    = fNumber
        if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)

        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                    '?LookupField=tmp:FaultCode' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                    'sort&LookupFrom=JobFaultCodes&' & |
                    'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
    end
ProductCodeRequired  PROCEDURE  (func:Manufacturer)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseProductCode
            Return# = 1
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     FilesOpened = False
  END
BrowseOutFaultsEstimateParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
EstimateCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAULO::State  USHORT
PARTS::State  USHORT
  CODE
    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(epr:Part_Number_Key,epr:Part_Number_Key)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If epr:Fault_Code1 <> ''
            Local.EstimateCode(1,epr:fault_Code1)
        End !If epr:Fault_Code1
        If epr:Fault_Code2 <> ''
            Local.EstimateCode(2,epr:fault_Code2)
        End !If epr:Fault_Code1
        If epr:Fault_Code3 <> ''
            Local.EstimateCode(3,epr:fault_Code3)
        End !If epr:Fault_Code1
        If epr:Fault_Code4 <> ''
            Local.EstimateCode(4,epr:fault_Code4)
        End !If epr:Fault_Code1
        If epr:Fault_Code5 <> ''
            Local.EstimateCode(5,epr:fault_Code5)
        End !If epr:Fault_Code1
        If epr:Fault_Code6 <> ''
            Local.EstimateCode(6,epr:fault_Code6)
        End !If epr:Fault_Code1
        If epr:Fault_Code7 <> ''
            Local.EstimateCode(7,epr:fault_Code7)
        End !If epr:Fault_Code1
        If epr:Fault_Code8 <> ''
            Local.EstimateCode(8,epr:fault_Code8)
        End !If epr:Fault_Code1
        If epr:Fault_Code9 <> ''
            Local.EstimateCode(9,epr:fault_Code9)
        End !If epr:Fault_Code1
        If epr:Fault_Code10 <> ''
            Local.EstimateCode(10,epr:fault_Code10)
        End !If epr:Fault_Code1
        If epr:Fault_Code11 <> ''
            Local.EstimateCode(11,epr:fault_Code11)
        End !If epr:Fault_Code1
        If epr:Fault_Code12 <> ''
            Local.EstimateCode(12,epr:fault_Code12)
        End !If epr:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsEstimateParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsEstimateParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsEstimateParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsEstimateParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsEstimateParts_frm'
    End
    p_web.SSV('BrowseOutFaultsEstimateParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsEstimateParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsEstimateParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsEstimateParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsEstimateParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsEstimateParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsEstimateParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsEstimateParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseOutFaultsEstimateParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsEstimateParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsEstimateParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsEstimateParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@n8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupFrom')
  End!Else
  loc:formaction = 'BrowseOutFaultsEstimateParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsEstimateParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsEstimateParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Estimate Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Estimate Parts',0)&'</span>'&CRLF
  End
  If clip('Estimate Parts') <> ''
    packet = clip(packet) & p_web.br
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseOutFaultsEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseOutFaultsEstimateParts.locate(''Locator2BrowseOutFaultsEstimateParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsEstimateParts.cl(''BrowseOutFaultsEstimateParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseOutFaultsEstimateParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseOutFaultsEstimateParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsEstimateParts','Fault','Click here to sort by Fault',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Fault')&'">'&p_web.Translate('Fault')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsEstimateParts','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsEstimateParts','Repair Index','Click here to sort by Repair Index',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Repair Index')&'">'&p_web.Translate('Repair Index')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sofp:sessionid',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:sessionID'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:partType'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:fault'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''E'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsEstimateParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsEstimateParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseOutFaultsEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseOutFaultsEstimateParts.locate(''Locator1BrowseOutFaultsEstimateParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsEstimateParts.cl(''BrowseOutFaultsEstimateParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupField')) = sofp:fault and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sofp:fault = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseOutFaultsEstimateParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sofp:fault',clip(loc:field),,loc:checked,,,'onclick="BrowseOutFaultsEstimateParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sofp:fault',clip(loc:field),,'checked',,,'onclick="BrowseOutFaultsEstimateParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseOutFaultsEstimateParts.omv(this);" onMouseOut="BrowseOutFaultsEstimateParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseOutFaultsEstimateParts=new browseTable(''BrowseOutFaultsEstimateParts'','''&clip(loc:formname)&''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sofp:sessionID')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseOutFaultsEstimateParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsEstimateParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsEstimateParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsEstimateParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsEstimateParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsEstimateParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sofp:sessionID',loc:default)
    p_web.SetValue('sofp:partType',loc:default)
    p_web.SetValue('sofp:fault',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:fault_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:fault,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:description_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:level_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:level,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sofp:sessionID
  loc:default = sofp:partType
  loc:default = sofp:fault

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sofp:sessionID',loc:default)
    p_web.SetSessionValue('sofp:partType',loc:default)
    p_web.SetSessionValue('sofp:fault',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sofp:sessionID'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sofp:sessionID'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.EstimateCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0

                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END
                    
                    
                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'E'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'E'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
DisplayOutFaults     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('DisplayOutFaults')
  loc:formname = 'DisplayOutFaults_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DisplayOutFaults','')
    p_web._DivHeader('DisplayOutFaults',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DisplayOutFaults',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
   p_web.site.SaveButton.TextValue = 'OK'
  p_web.SetValue('DisplayOutFaults_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('DisplayOutFaults_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'JobFaultCodes'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DisplayOutFaults_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DisplayOutFaults_ChainTo')
    loc:formaction = p_web.GetSessionValue('DisplayOutFaults_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'JobFaultCodes'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="DisplayOutFaults" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="DisplayOutFaults" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="DisplayOutFaults" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Out Faults') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Out Faults',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_DisplayOutFaults" class="'&clip('FormCentreFixed')&'">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_DisplayOutFaults" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_DisplayOutFaults')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Out Faults') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Out Faults') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_DisplayOutFaults')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_DisplayOutFaults'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_DisplayOutFaults')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Out Faults') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DisplayOutFaults_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Out Faults')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Out Faults')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Out Faults')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Out Faults')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseJobOutFaults
      do Comment::BrowseJobOutFaults
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Out Faults') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DisplayOutFaults_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Out Faults')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2" class="'&clip('FormCentreFixed')&'">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Out Faults')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2" class="'&clip('FormCentreFixed')&'">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Out Faults')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Out Faults')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseOutFaultsChargeableParts
      do Comment::BrowseOutFaultsChargeableParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseOutFaultsWarrantyParts
      do Comment::BrowseOutFaultsWarrantyParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseOutFaultsEstimateParts
      do Comment::BrowseOutFaultsEstimateParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseJobOutFaults  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseJobOutFaults',p_web.GetValue('NewValue'))
    do Value::BrowseJobOutFaults
  Else
    p_web.StoreValue('joo:RecordNumber')
  End

Value::BrowseJobOutFaults  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseJobOutFaults --
  p_web.SetValue('BrowseJobOutFaults:NoForm',1)
  p_web.SetValue('BrowseJobOutFaults:FormName',loc:formname)
  p_web.SetValue('BrowseJobOutFaults:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&'"><!-- Net:BrowseJobOutFaults --></div><13,10>'
    p_web._DivHeader('DisplayOutFaults_' & lower('BrowseJobOutFaults') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('DisplayOutFaults_' & lower('BrowseJobOutFaults') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseJobOutFaults --><13,10>'
  end
  do SendPacket

Comment::BrowseJobOutFaults  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseJobOutFaults') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseOutFaultsChargeableParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts',p_web.GetValue('NewValue'))
    do Value::BrowseOutFaultsChargeableParts
  Else
    p_web.StoreValue('sofp:sessionID')
  End

Value::BrowseOutFaultsChargeableParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <>'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsChargeableParts --
  p_web.SetValue('BrowseOutFaultsChargeableParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&'"><!-- Net:BrowseOutFaultsChargeableParts --></div><13,10>'
    p_web._DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsChargeableParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('DisplayOutFaults_' & lower('BrowseOutFaultsChargeableParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsChargeableParts --><13,10>'
  end
  do SendPacket

Comment::BrowseOutFaultsChargeableParts  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsChargeableParts') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <>'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <>'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseOutFaultsWarrantyParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts',p_web.GetValue('NewValue'))
    do Value::BrowseOutFaultsWarrantyParts
  Else
    p_web.StoreValue('sofp:sessionID')
  End

Value::BrowseOutFaultsWarrantyParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsWarrantyParts --
  p_web.SetValue('BrowseOutFaultsWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&'"><!-- Net:BrowseOutFaultsWarrantyParts --></div><13,10>'
    p_web._DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsWarrantyParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('DisplayOutFaults_' & lower('BrowseOutFaultsWarrantyParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsWarrantyParts --><13,10>'
  end
  do SendPacket

Comment::BrowseOutFaultsWarrantyParts  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsWarrantyParts') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseOutFaultsEstimateParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts',p_web.GetValue('NewValue'))
    do Value::BrowseOutFaultsEstimateParts
  Else
    p_web.StoreValue('sofp:sessionID')
  End

Value::BrowseOutFaultsEstimateParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Estimate') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsEstimateParts --
  p_web.SetValue('BrowseOutFaultsEstimateParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsEstimateParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsEstimateParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&'"><!-- Net:BrowseOutFaultsEstimateParts --></div><13,10>'
    p_web._DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsEstimateParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('DisplayOutFaults_' & lower('BrowseOutFaultsEstimateParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsEstimateParts --><13,10>'
  end
  do SendPacket

Comment::BrowseOutFaultsEstimateParts  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsEstimateParts') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)

PreCopy  Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)

PreDelete       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DisplayOutFaults_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('DisplayOutFaults_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
BrowseOutFaultsChargeableParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
ChargeableCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAULO::State  USHORT
PARTS::State  USHORT
  CODE
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If par:Fault_Code1 <> ''
            Local.ChargeableCode(1,par:fault_Code1)
        End !If par:Fault_Code1
        If par:Fault_Code2 <> ''
            Local.ChargeableCode(2,par:fault_Code2)
        End !If par:Fault_Code1
        If par:Fault_Code3 <> ''
            Local.ChargeableCode(3,par:fault_Code3)
        End !If par:Fault_Code1
        If par:Fault_Code4 <> ''
            Local.ChargeableCode(4,par:fault_Code4)
        End !If par:Fault_Code1
        If par:Fault_Code5 <> ''
            Local.ChargeableCode(5,par:fault_Code5)
        End !If par:Fault_Code1
        If par:Fault_Code6 <> ''
            Local.ChargeableCode(6,par:fault_Code6)
        End !If par:Fault_Code1
        If par:Fault_Code7 <> ''
            Local.ChargeableCode(7,par:fault_Code7)
        End !If par:Fault_Code1
        If par:Fault_Code8 <> ''
            Local.ChargeableCode(8,par:fault_Code8)
        End !If par:Fault_Code1
        If par:Fault_Code9 <> ''
            Local.ChargeableCode(9,par:fault_Code9)
        End !If par:Fault_Code1
        If par:Fault_Code10 <> ''
            Local.ChargeableCode(10,par:fault_Code10)
        End !If par:Fault_Code1
        If par:Fault_Code11 <> ''
            Local.ChargeableCode(11,par:fault_Code11)
        End !If par:Fault_Code1
        If par:Fault_Code12 <> ''
            Local.ChargeableCode(12,par:fault_Code12)
        End !If par:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsChargeableParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsChargeableParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsChargeableParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsChargeableParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsChargeableParts_frm'
    End
    p_web.SSV('BrowseOutFaultsChargeableParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsChargeableParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsChargeableParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsChargeableParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsChargeableParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsChargeableParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsChargeableParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsChargeableParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseOutFaultsChargeableParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsChargeableParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsChargeableParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsChargeableParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@n8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupFrom')
  End!Else
  loc:formaction = 'BrowseOutFaultsChargeableParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsChargeableParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsChargeableParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Chargeable Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Chargeable Parts',0)&'</span>'&CRLF
  End
  If clip('Chargeable Parts') <> ''
    packet = clip(packet) & p_web.br
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseOutFaultsChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseOutFaultsChargeableParts.locate(''Locator2BrowseOutFaultsChargeableParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsChargeableParts.cl(''BrowseOutFaultsChargeableParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseOutFaultsChargeableParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseOutFaultsChargeableParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsChargeableParts','Fault','Click here to sort by Fault',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Fault')&'">'&p_web.Translate('Fault')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsChargeableParts','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsChargeableParts','Repair Index','Click here to sort by Repair Index',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Repair Index')&'">'&p_web.Translate('Repair Index')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sofp:sessionid',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:sessionID'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:partType'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:fault'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''C'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsChargeableParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsChargeableParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseOutFaultsChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseOutFaultsChargeableParts.locate(''Locator1BrowseOutFaultsChargeableParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsChargeableParts.cl(''BrowseOutFaultsChargeableParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupField')) = sofp:fault and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sofp:fault = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseOutFaultsChargeableParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
      
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sofp:fault',clip(loc:field),,loc:checked,,,'onclick="BrowseOutFaultsChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sofp:fault',clip(loc:field),,'checked',,,'onclick="BrowseOutFaultsChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseOutFaultsChargeableParts.omv(this);" onMouseOut="BrowseOutFaultsChargeableParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseOutFaultsChargeableParts=new browseTable(''BrowseOutFaultsChargeableParts'','''&clip(loc:formname)&''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sofp:sessionID')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseOutFaultsChargeableParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsChargeableParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsChargeableParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsChargeableParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsChargeableParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsChargeableParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sofp:sessionID',loc:default)
    p_web.SetValue('sofp:partType',loc:default)
    p_web.SetValue('sofp:fault',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:fault_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:fault,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:description_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:level_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:level,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sofp:sessionID
  loc:default = sofp:partType
  loc:default = sofp:fault

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sofp:sessionID',loc:default)
    p_web.SetSessionValue('sofp:partType',loc:default)
    p_web.SetSessionValue('sofp:fault',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sofp:sessionID'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sofp:sessionID'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.ChargeableCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0
                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END
                    


                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'C'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'C'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
