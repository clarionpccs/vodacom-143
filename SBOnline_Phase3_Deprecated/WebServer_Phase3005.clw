

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
JobReceipt PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
tmp:JobNumber        LONG                                  !Job Number
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(15)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !
VatNumber            STRING(30)                            !
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
Received_By_Temp     STRING(60)                            !Received_By_Temp
Amount_Received_Temp REAL                                  !Amount_Received_Temp
tmp:StandardText     STRING(255)                           !tmp:StandardText
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:Incoming_Consignment_Number)
                       PROJECT(job:Incoming_Courier)
                       PROJECT(job:Incoming_Date)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Transit_Type)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('Job Receipt'),AT(250,4323,7750,5813),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(323,438,7521,4115),USE(?unnamed),FONT('Tahoma')
                         STRING('Job Number: '),AT(5052,365),USE(?String25),FONT(,8),TRN
                         STRING(@s16),AT(5990,365),USE(tmp:Ref_number),FONT(,11,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(5052,677),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(5990,677),USE(job:date_booked),FONT(,8,,FONT:bold),TRN
                         STRING(@t1),AT(6875,677),USE(job:time_booked),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(1604,3240),USE(job:Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4635,3240),USE(job:Warranty_Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3125,3240),USE(job:Repair_Type),FONT(,8),TRN
                         STRING(@s20),AT(6146,3240),USE(job:Repair_Type_Warranty),FONT(,8),TRN
                         STRING('Model'),AT(156,3646),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1604,3646),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3125,3646),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4635,3646),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6146,3646),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4083,2344),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2344),USE(Delivery_Telephone_Temp),FONT(,8)
                         STRING(@s65),AT(4063,2500,2865,156),USE(clientName),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Order Number'),AT(156,3000),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(1604,3000),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(3125,3000),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Type'),AT(4635,3000),USE(?String40:5),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Repair Type'),AT(6146,3000),USE(?String40:6),FONT(,8,,FONT:bold),TRN
                         STRING('Tel:'),AT(2198,2031),USE(?String34:3),FONT(,8),TRN
                         STRING('Acc No:'),AT(2188,1563),USE(?String78),FONT(,8),TRN
                         STRING('Email:'),AT(156,2344),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(2667,2031),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2198,2188),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2667,2188),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2656,1563),USE(job:Account_Number,,?job:Account_Number:2),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(156,3240),USE(job:Order_Number),FONT(,8),TRN
                         STRING(@s30),AT(4083,1563),USE(delivery_Company_Name_temp),FONT(,8),TRN
                         STRING(@s50),AT(469,2344,3125,208),USE(invoice_EMail_Address),FONT(,8),TRN
                         STRING(@s30),AT(4083,1719,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,1875),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,2031),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,2188),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),FONT(,8),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:4)
DETAIL                   DETAIL,AT(0,0,,5646),USE(?DetailBand),FONT('Tahoma',8),TOGETHER
                           STRING(@s30),AT(229,0,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(1677,0,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(3198,0,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(4708,0,1396,156),USE(job:ESN),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(6219,0),USE(job:MSN),FONT(,8),TRN
                           STRING(@s15),AT(6219,365),USE(exchange_msn_temp),FONT(,8),TRN
                           STRING(@s16),AT(4708,365,1396,156),USE(exchange_esn_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(3198,365,1396,156),USE(exchange_unit_type_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(1677,365,1323,156),USE(exchange_manufacturer_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(229,365,1000,156),USE(exchange_model_number),FONT(,8),LEFT,TRN
                           STRING(@s15),AT(229,208),USE(exchange_unit_title_temp),FONT(,8,,FONT:bold),LEFT,TRN
                           STRING(@s20),AT(1354,208),USE(exchange_unit_number_temp),LEFT,TRN
                           STRING('ACCESSORIES'),AT(156,1042),USE(?String63),FONT(,8,,FONT:bold),TRN
                           TEXT,AT(1927,1042,5313,365),USE(tmp:accessories),FONT(,7),TRN
                           STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,1458),USE(?ExternalDamageCheckList),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Notes:'),AT(5990,1458),USE(?Notes),FONT(,7),TRN
                           TEXT,AT(6302,1458,1250,677),USE(jobe2:XNotes),FONT(,6),TRN
                           CHECK('None'),AT(5365,1458,625,156),USE(jobe2:XNone),TRN
                           CHECK('Keypad'),AT(156,1615,729,156),USE(jobe2:XKeypad),TRN
                           CHECK('Charger'),AT(2146,1615,781,156),USE(jobe2:XCharger),TRN
                           CHECK('Antenna'),AT(2146,1448,833,156),USE(jobe2:XAntenna),TRN
                           CHECK('Lens'),AT(2938,1448,573,156),USE(jobe2:XLens),TRN
                           CHECK('F/Cover'),AT(3563,1448,781,156),USE(jobe2:XFCover),TRN
                           CHECK('B/Cover'),AT(4448,1448,729,156),USE(jobe2:XBCover),TRN
                           CHECK('Battery'),AT(990,1615,729,156),USE(jobe2:XBattery),TRN
                           CHECK('LCD'),AT(2938,1615,625,156),USE(jobe2:XLCD),TRN
                           CHECK('System Connector'),AT(4448,1615,1250,156),USE(jobe2:XSystemConnector),TRN
                           CHECK('Sim Reader'),AT(3563,1604,885,156),USE(jobe2:XSimReader),TRN
                           STRING('Initial Transit Type: '),AT(156,4740,990,156),USE(?String66),FONT(,8),TRN
                           STRING(@s20),AT(1198,4740),USE(job:Transit_Type),FONT(,8,,FONT:bold),LEFT,TRN
                           STRING('Courier: '),AT(156,4896),USE(?String67),FONT(,8),TRN
                           STRING(@s15),AT(1198,4896,1302,188),USE(job:Incoming_Courier),FONT(,8,,FONT:bold),TRN
                           STRING('Consignment No:'),AT(156,5052),USE(?String70),FONT(,8),TRN
                           STRING(@s15),AT(1198,5052,1302,188),USE(job:Incoming_Consignment_Number),FONT(,8,,FONT:bold), |
  TRN
                           STRING('Customer ID No:'),AT(5573,5156),USE(?CustomerIDNo),FONT(,8),TRN
                           STRING('Date Received: '),AT(156,5208),USE(?String68),FONT(,8),TRN
                           STRING(@d6b),AT(1198,5208),USE(job:Incoming_Date),FONT(,8,,FONT:bold),TRN
                           STRING('Received By: '),AT(156,5365),USE(?String69),FONT(,8),TRN
                           STRING(@s15),AT(1198,5365),USE(Received_By_Temp),FONT(,8,,FONT:bold),TRN
                           STRING('REPORTED FAULT'),AT(156,573),USE(?String64),FONT(,8,,FONT:bold),TRN
                           TEXT,AT(1927,573,5313,417),USE(jbn:Fault_Description),FONT('Arial',7,,,CHARSET:ANSI),TRN
                           STRING(@s24),AT(3281,5365,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING('Booking Option:'),AT(5573,5313),USE(?String122),FONT(,8,,FONT:bold),TRN
                           STRING(@s30),AT(6563,5313,1563,156),USE(tmp:BookingOption),FONT(,8),TRN
                           STRING(@s13),AT(6563,5156),USE(jobe2:IDNumber),FONT(,8),TRN
                           STRING(@s20),AT(3281,4948,1760,198),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING(@s20),AT(2865,5156,2552,208),USE(barcodeIMEINumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White),TRN
                           STRING(@s20),AT(2865,4740,2552,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('Amount:'),AT(5573,4948),USE(?amount_text),FONT(,8),TRN
                           STRING('<128>'),AT(6510,4948,52,208),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                           STRING(@n-14.2b),AT(6615,4948),USE(Amount_Received_Temp),FONT(,8,,FONT:bold),TRN
                           STRING('Loan Deposit Paid'),AT(5573,4740),USE(?LoanDepositPaidTitle),FONT(,8,,,CHARSET:ANSI), |
  HIDE,TRN
                           STRING(@n-14.2b),AT(6667,4740),USE(tmp:LoanDepositPaid),FONT(,8,,FONT:bold,CHARSET:ANSI),HIDE, |
  TRN
                           TEXT,AT(208,2125,7333,2417),USE(stt:Text),TRN
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156),USE(?unnamed:2)
                         STRING('Estimate Required If Repair Costs Exceed:'),AT(156,63),USE(?estimate_text),FONT(,8), |
  HIDE,TRN
                         STRING(@s70),AT(2344,52,4115,260),USE(estimate_value_temp),FONT(,8),HIDE,TRN
                         TEXT,AT(83,83,10,10),USE(tmp:StandardText),FONT(,8),HIDE
                         STRING('LOAN UNIT ISSUED'),AT(156,271),USE(?LoanUnitIssued),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         STRING(@s30),AT(1906,271),USE(tmp:LoanModel),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  HIDE,TRN
                         STRING('IMEI'),AT(3969,271),USE(?IMEITitle),FONT('Arial',9,,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s30),AT(4292,271),USE(tmp:LoanIMEI),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  HIDE,TRN
                         STRING('LOAN ACCESSORIES'),AT(156,458),USE(?LoanAccessoriesTitle),FONT('Arial',8,,FONT:bold, |
  CHARSET:ANSI),HIDE,TRN
                         STRING(@s255),AT(1906,458,5417,208),USE(tmp:LoanAccessories),FONT('Arial',8,,FONT:regular, |
  CHARSET:ANSI),LEFT,HIDE,TRN
                         STRING('Customer Signature'),AT(156,875),USE(?String82),FONT(,,,FONT:bold),TRN
                         STRING('Date: {17}/ {18}/'),AT(5135,875,2156),USE(?String83),FONT(,,,FONT:bold),TRN
                         LINE,AT(1406,1042,3677,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(5573,1042,1521,0),USE(?Line2),COLOR(COLOR:Black)
                         TEXT,AT(156,615,5885,260),USE(locTermsText)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB RECEIPT'),AT(5917,0,1521,240),USE(?String20),FONT(,16,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GOODS RECEIVED DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('PAYMENT RECEIVED (INC. V.A.T.)'),AT(5552,8563),USE(?String73:2),FONT(,8,,FONT:bold), |
  TRN
                         STRING('REPAIR DETAILS'),AT(156,3677),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2552,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5521,8698,2135,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobReceipt')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB RECEIPT'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  locTermsText = p_web.GSV('Default:TermsText')
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('JobReceipt',ProgressWindow)                ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobReceipt',ProgressWindow)             ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
   Access:STANTEXT.ClearKey(stt:Description_Key)
   stt:Description = 'JOB RECEIPT'
   If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
       !Found
       tmp:StandardText = stt:Text
   Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
       !Error
   End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = job:Despatch_User
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
      Received_By_Temp = CLip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  
  Case jobe:Booking48HourOption
  Of 1
      tmp:BookingOption = '48 Hour Exchange'
  Of 2
      tmp:BookingOption = 'ARC Repair'
  Of 3
      tmp:BookingOption = '7 Day TAT'
  Else
      tmp:BookingOption = 'N/A'
  End ! Case jobe:Booking48HourOption
  
  If Clip(job:Title) = '' And Clip(job:Initial) = ''
      Customer_Name_Temp = job:Surname
  End ! If Clip(job:Title) = '' And Clip(job:Initial) = ''
  If Clip(job:Title) = '' And Clip(job:Initial) <> ''
      CUstomer_Name_Temp = Clip(job:Initial) & ' ' & Clip(job:Surname)
  End ! If Clip(job:Title) = '' And Clip(job:Initial) <> ''
  If Clip(job:Title) <> '' ANd CLip(job:Initial) <> ''
      Customer_Name_Temp = CLip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)
  End ! If Clip(job:Title) <> '' ANd CLip(job:Initial) <> ''
  
  If Customer_Name_Temp <> ''
      ClientName = 'Client: ' & Clip(CUstomer_Name_Temp)
      If jobe:EndUserTelNo <> ''
          CLientName = Clip(ClientName) & '  Tel: ' & jobe:EndUSerTelNo
      End ! If jobe:EndUserTelNo <> ''
  ELse
      If jobe:EndUserTelNo <> ''
          CLientName = 'Tel: ' & jobe:EndUSerTelNo
      End ! If jobe:EndUserTelNo <> ''
  End ! If Customer_Name_Temp <> ''
  
  delivery_Company_Name_temp = job:Company_Name_Delivery
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_Telephone_temp = job:Telephone_Delivery
  
  ! ---- Not relevant ----
  ! Deleting: DBH 29/01/2009 #10661
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Invoice_Address1_Temp = tra:Address_Line1
  !    Invoice_ADdress2_Temp = tra:Address_Line2
  !    Invoice_Address3_Temp = tra:Address_Line3
  !    Invoice_Address4_temp = tra:Postcode
  !    Invoice_Company_Name_Temp = tra:Company_Name
  !    Invoice_Telephone_Number_Temp = tra:Telephone_Number
  !    Invoice_Fax_Number_Temp = tra:Fax_Number
  !    Invoice_EMail_Address = tra:EmailAddress
  !Else ! If jobe:WebJob
  
  ! End: DBH 29/01/2009 #10661
  ! -----------------------------------------
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = job:Account_Number
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = job:Address_Line1
                      Invoice_ADdress2_Temp = job:Address_Line2
                      Invoice_Address3_Temp = job:Address_Line3
                      Invoice_Address4_temp = job:Postcode
                      Invoice_Company_Name_Temp = job:Company_Name
                      Invoice_Telephone_Number_Temp = job:Telephone_Number
                      Invoice_Fax_Number_Temp = job:Fax_Number
                      Invoice_EMail_Address = jobe:EndUserEmailAddress
                  Else ! If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = sub:Address_Line1
                      Invoice_ADdress2_Temp = sub:Address_Line2
                      Invoice_Address3_Temp = sub:Address_Line3
                      Invoice_Address4_temp = sub:Postcode
                      Invoice_Company_Name_Temp = sub:Company_Name
                      Invoice_Telephone_Number_Temp = sub:Telephone_Number
                      Invoice_Fax_Number_Temp = sub:Fax_Number
                      Invoice_EMail_Address = sub:EmailAddress
                  End ! If sub:Invoice_Customer_Address= 'YES'
              Else ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If tra:invoice_Customer_Address = 'YES'
                      Invoice_Address1_Temp = job:Address_Line1
                      Invoice_ADdress2_Temp = job:Address_Line2
                      Invoice_Address3_Temp = job:Address_Line3
                      Invoice_Address4_temp = job:Postcode
                      Invoice_Company_Name_Temp = job:Company_Name
                      Invoice_Telephone_Number_Temp = job:Telephone_Number
                      Invoice_Fax_Number_Temp = job:Fax_Number
                      Invoice_EMail_Address = jobe:EndUserEmailAddress
                  Else ! If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = tra:Address_Line1
                      Invoice_ADdress2_Temp = tra:Address_Line2
                      Invoice_Address3_Temp = tra:Address_Line3
                      Invoice_Address4_temp = tra:Postcode
                      Invoice_Company_Name_Temp = tra:Company_Name
                      Invoice_Telephone_Number_Temp = tra:Telephone_Number
                      Invoice_Fax_Number_Temp = tra:Fax_Number
                      Invoice_EMail_Address = tra:EmailAddress
                  End ! If tra:invoice_Customer_Address = 'YES'
              End ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  !End ! If jobe:WebJob
  
  If job:Exchange_Unit_Number > 0
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          Exchange_Unit_Number_Temp = 'Unit: ' & job:Exchange_Unit_Number
          Exchange_Model_Number    = xch:Model_NUmber
          Exchange_Manufacturer_Temp = xch:Manufacturer
          Exchange_Unit_Type_Temp = 'N/A'
          Exchange_ESN_Temp = xch:ESN
          Exchange_Unit_Title_Temp = 'EXCHANGE UNIT'
          Exchange_MSN_Temp = xch:MSN
      Else ! If Access:EXCHANGE.TryFetch(xch:Ref_NumberKey) = Level:Benign
          !Error
      End ! If Access:EXCHANGE.TryFetch(xch:Ref_NumberKey) = Level:Benign
  End ! If job:Exchange_Unit_Number > 0
  
  tmp:Accessories = ''
  Access:JOBACC.Clearkey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged = 0
  Set(jac:DamagedKey,jac:DamagedKey)
  Loop ! Begin Loop
      If Access:JOBACC.Next()
          Break
      End ! If Access:JOBACC.Next()
      If jac:Ref_Number <> job:Ref_Number
          Break
      End ! If jac:Ref_Number <> job:Ref_Number
  
      If jac:Damaged = 1
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged = 1
  
      If tmp:Accessories = ''
          tmp:Accessories = jac:Accessory
      Else ! If tmp:Accessories = ''
          tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
      End ! If tmp:Accessories = ''
  End ! Loop
  
  If job:Estimate = 'YES'
      Settarget(Report)
      ?Estimate_Text{prop:Hide} = 0
      ?Estimate_Value_Temp{prop:Hide} = 0
      Estimate_Value_Temp = Clip(Format(job:Estimate_If_Over,@n14.2)) & ' (Plus VAT)'
      SetTarget()
  End ! If job:Estimate = 'YES'
  
  If job:Chargeable_Job = 'YES'
      Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
      jpt:Ref_Number = job:Ref_Number
      Set(jpt:All_Date_Key,jpt:All_Date_Key)
      Loop ! Begin Loop
          If Access:JOBPAYMT.Next()
              Break
          End ! If Access:JOBPAYMT.Next()
          If jpt:Ref_Number <> job:Ref_Number
              Break
          End ! If jpt:Ref_Number <> job:Ref_Number
          Amount_Received_Temp += jpt:Amount
      End ! Loop
  Else ! If job:Chargeable_Job = 'YES'
      SetTarget(Report)
      ?Amount_Text{prop:Hide} = 1
      ?job:Charge_Type{prop:Hide} = 1
      ?String40:3{prop:Hide} = 1
      SetTarget()
  End ! If job:Chargeable_Job = 'YES'
  
  tmp:LoanModel = ''
  tmp:LoanIMEI = ''
  tmp:LoanAccessories = ''
  
  If job:Loan_Unit_Number > 0
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = job:Loan_Unit_Number
      If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
          !Found
          tmp:LoanModel = Clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI = loa:ESN
  
          Access:LOANACC.Clearkey(lac:Ref_Number_Key)
          lac:Ref_Number = loa:Ref_Number
          Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
          Loop ! Begin Loop
              If Access:LOANACC.Next()
                  Break
              End ! If Access:LOANACC.Next()
              If lac:Ref_Number <> loa:Ref_Number
                  Break
              End ! If lac:Ref_Number <> loa:Ref_Number
              If lac:Accessory_Status <> 'ISSUE'
                  Cycle
              End ! If lac:Accessory_Status <> 'ISSUE'
              If tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              Else ! If tmp:LoanAccessories = ''
                  tmp:LoanAccessories = Clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              End ! If tmp:LoanAccessories = ''
          End ! Loop
  
          Access:STANTEXT.ClearKey(stt:Description_Key)
          stt:Description = 'LOAN DISCLAIMER'
          If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
              !Found
              tmp:StandardText = Clip(tmp:StandardText) & '<13,10>' & stt:Text
          Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
              !Error
          End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
      Else ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
          !Error
      End ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
  
  End ! If job:Loan_Unit_Number > 0
  
  If tmp:LoanModel <> ''
      SetTarget(Report)
      ?LoanUnitIssued{prop:Hide} = 0
      ?tmp:LoanModel{prop:Hide} = 0
      ?IMEITitle{prop:Hide} = 0
      ?tmp:LoanIMEI{prop:Hide} = 0
      ?LoanAccessoriesTitle{prop:Hide} = 0
      ?tmp:LOanAccessories{prop:Hide} = 0
  End ! If tmp:LoanModel <> ''
  
  tmp:LoanDepositPaid = 0
  
  Access:JOBPAYMT.Clearkey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number = job:Ref_Number
  jpt:Loan_Deposit = 1
  Set(jpt:Loan_Deposit_Key,jpt:Loan_Deposit_Key)
  Loop ! Begin Loop
      If Access:JOBPAYMT.Next()
          Break
      End ! If Access:JOBPAYMT.Next()
      If jpt:Ref_Number <> job:Ref_Number
          Break
      End ! If jpt:Ref_Number <> job:Ref_Number
      If jpt:Loan_Deposit <> 1
          Break
      End ! If jpt:Loan_Deposit <> 1
      tmp:LoanDepositPaid += jpt:Amount
  End ! Loop
  
  If tmp:LoanDepositPaid > 0
      SetTarget(Report)
      ?LoanDepositPaidTitle{prop:Hide} = 0
      ?tmp:LoanDepositPaid{prop:Hide} = 0
      SetTarget()
  End ! If tmp:LoanDepositPaid > 0
  
  job_number_temp      = 'Job No: ' & job:ref_number
  esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
  barcodeIMEINumber = '*' & clip(job:ESN) & '*'
  
  !!Barcode Bit
  !code_temp   = 3
  !option_temp = 0
  !
  !bar_code_string_temp = job:ref_number
  !job_number_temp      = 'Job No: ' & job:ref_number
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  !
  !bar_code_string_temp = Clip(job:esn)
  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  !
  !SetTarget(Report)
  !DR_JobNo.Init256()
  !DR_JobNo.RenderWholeStrings = 1
  !DR_JobNo.Resize(DR_JobNo.Width * 5, DR_JobNo.Height * 5)
  !DR_JobNo.Blank(Color:White)
  !DR_JobNo.FontName = 'C128 High 12pt LJ3'
  !DR_JobNo.FontStyle = font:Regular
  !DR_JobNo.FontSize = 48
  !DR_JobNo.Show(0,0,Bar_Code_Temp)
  !DR_JobNo.Display()
  !DR_JobNo.Kill256()
  !
  !DR_IMEI.Init256()
  !DR_IMEI.RenderWholeStrings = 1
  !DR_IMEI.Resize(DR_IMEI.Width * 5, DR_IMEI.Height * 5)
  !DR_IMEI.Blank(Color:White)
  !DR_IMEI.FontName = 'C128 High 12pt LJ3'
  !DR_IMEI.FontStyle = font:Regular
  !DR_IMEI.FontSize = 48
  !DR_IMEI.Show(0,0,Bar_Code2_Temp)
  !DR_IMEI.Display()
  !DR_IMEI.Kill256()
  !SetTarget()
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','JobCard','JobCard','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

CreateTestRecord     PROCEDURE  (String f:Path)            ! Declare Procedure
tmp:TestRecord       STRING(255),STATIC                    !
TestRecord    File,Driver('ASCII'),Pre(test),Name(tmp:TestRecord),Create,Bindable,Thread
Record                  Record
TestLine                String(1)
                        End
                    End
  CODE
    ! Start - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
    tmp:TestRecord = Clip(f:Path)

    If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
    Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
    End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'

    Remove(TestRecord)
    Create(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Open(TestRecord)
    If Error()
        Return False
    End ! If Error()
    test:TestLine = '1'
    Add(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Close(TestRecord)
    Remove(TestRecord)
    If Error()
        Return False
    End ! If Error()
    ! End   - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005
    Return True
CID_XML              PROCEDURE  (String f:Mobile,String f:Account,Byte f:Type) ! Declare Procedure
seq                  LONG                                  !
RepositoryDir        CSTRING('C:\ServiceBaseMQ\MQ_Repository<0>{224}') !
save_invoice_id      USHORT,AUTO                           !
save_tradeacc_id     USHORT,AUTO                           !
save_subtracc_id     USHORT,AUTO                           !
tmp:STFMessage       STRING(30)                            !STF Message
tmp:AOWMessage       STRING(30)                            !AOW Message
tmp:RIVMessage       STRING(30)                            !RIV Message
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
XmlData group,type
ServiceCode string(3)
CID         group
MSISDN_OR_ICCID         string(20)
CALL_ID                 string(7)
ACCOUNT_ID              string(9)
SOURCE                  string(60)
TOPIC_CODE              string(60)
COMMENT                 string(255)
            end
        end

    map
SendXML         procedure(const *XmlData aXmlData, string aRepositoryDir)
    end

XmlValues GROUP(XmlData)
           END
  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
    ! Check default to see if should create messages - TrkBs: 6141 (DBH: 20-07-2005)
    If GETINI('XML','CreateCID',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        Return
    End ! If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> True

    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    If CreateTestRecord(RepositoryDir) = False
        Return
    End ! If CreateTestRecord(RepositoryDir) = False

    Do CreateCID
CreateCID                  Routine
    Access:TRADEACC_ALIAS.Open()
    Access:TRADEACC_ALIAS.UseFile()
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = f:Account
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign

    clear(XmlValues)
    XmlValues.ServiceCode         = 'CID'
    XmlValues.CID.MSISDN_OR_ICCID = f:Mobile
    XmlValues.CID.CALL_ID         = ''
    XmlValues.CID.ACCOUNT_ID      = ''
    XmlValues.CID.SOURCE          = 'ServiceBase'
    Case f:Type
    Of 1 !Booked In
        XmlValues.CID.TOPIC_CODE      = 'Repair Booked In'
        XmlValues.CID.COMMENT         = 'Repair Booked In At ' & Clip(tra_ali:Company_Name)
    Of 2 !Despatched
        XmlValues.CID.TOPIC_CODE      = 'Repair Collected'
        XmlValues.CID.COMMENT         = 'Repair Collected From ' & Clip(tra_ali:Company_Name)
    End ! Case f:Type

    SendXml(XmlValues, RepositoryDir)

    Access:TRADEACC_ALIAS.Close()
SendXML         procedure(aXmlData, aRepositoryDir)

TheDate date
TheTime time
MsgId   string(24)

    code

    TheDate = today()
    TheTime = clock()
    MsgId = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
    seq += 1
    if seq > 99999 then
        seq = 1
    end
    if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
        objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
            objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
                objXmlExport.WriteTag('SRC_SYSTEM', '')
                objXmlExport.WriteTag('SRC_APPLICATION', '')
                objXmlExport.WriteTag('SERVICING_APPLICATION', '')
                objXmlExport.WriteTag('ACTION_CODE', 'INS')
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
                objXmlExport.WriteTag('MESSAGE_ID', '')
                objXmlExport.OpenTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('VALIDITY','84600000','unit="millisecond"')
                objXmlExport.WriteTag('ACTION','','type="DISCARD"')
                objXmlExport.WriteTag('RESPONSE_REQUIRED','Y')
                objXmlExport.CloseTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('USER_NAME', '')
                objXmlExport.WriteTag('TOKEN', '')
            objXmlExport.CloseTag('MESSAGE_HEADER')
            objXmlExport.OpenTag('MESSAGE_BODY')

            objXmlExport.OpenTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')
                objXmlExport.WriteTag('MSISDN_OR_ICCID', clip(aXmlData.CID.MSISDN_OR_ICCID))
                objXmlExport.WriteTag('CALL_ID', clip(aXmlData.CID.CALL_ID))
                objXmlExport.WriteTag('ACCOUNT_ID', clip(aXmlData.CID.ACCOUNT_ID))
                objXmlExport.WriteTag('SOURCE', clip(aXmlData.CID.SOURCE))
                objXmlExport.WriteTag('TOPIC_CODE', clip(aXmlData.CID.TOPIC_CODE))
                objXmlExport.WriteTag('COMMENT', clip(aXmlData.CID.COMMENT))
             objXmlExport.CloseTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')

            objXmlExport.CloseTag('MESSAGE_BODY')
        objXmlExport.CloseTag('VODACOM_MESSAGE')
        objXmlExport.FClose();
    end





AddEmailSMS          PROCEDURE  (Long f:JobNumber, String f:AccountNumber, String f:MessageType, String f:SMSEmail, String f:MobileNumber, String f:EmailAddress, Real f:Cost, String f:EstimatePath) ! Declare Procedure
save_tra_id          USHORT,AUTO                           !
save_sub_id          USHORT,AUTO                           !
tmp:TelephoneNumber  STRING(30)                            !
tmp:CompanyName      STRING(30)                            !Trade Company Name
FilesOpened     BYTE(0)
  CODE
    VodacomClass.AddEmailSMS(f:JobNumber, f:AccountNumber, f:MessageType, f:SMSEmail,|
    f:MobileNumber, f:EmailAddress, f:Cost, f:EstimatePath)

!    Do OpenFiles
!
!    INCLUDE('AddEmailSMS.inc')
!
!    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SMSMAIL.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SMSMAIL.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:SMSMAIL.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
GetTheMainOutFault   PROCEDURE  (LONG jobNumber,*STRING outFault,*STRING description) ! Declare Procedure
  CODE
    vod.GetMainOutFault(jobNumber,outFault,description)
BouncerWarrantyParts PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(WARPARTS)
                      Project(wpr:Record_Number)
                      Project(wpr:Part_Number)
                      Project(wpr:Description)
                      Project(wpr:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BouncerWarrantyParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BouncerWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('BouncerWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('BouncerWarrantyParts:FormName')
    else
      loc:FormName = 'BouncerWarrantyParts_frm'
    End
    p_web.SSV('BouncerWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('BouncerWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BouncerWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('BouncerWarrantyParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BouncerWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BouncerWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(WARPARTS,wpr:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'WPR:PART_NUMBER') then p_web.SetValue('BouncerWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'WPR:DESCRIPTION') then p_web.SetValue('BouncerWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'WPR:QUANTITY') then p_web.SetValue('BouncerWarrantyParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BouncerWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BouncerWarrantyParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BouncerWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BouncerWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BouncerWarrantyParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BouncerWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BouncerWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Part_Number)','-UPPER(wpr:Part_Number)')
    Loc:LocateField = 'wpr:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Description)','-UPPER(wpr:Description)')
    Loc:LocateField = 'wpr:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'wpr:Quantity','-wpr:Quantity')
    Loc:LocateField = 'wpr:Quantity'
  end
  if loc:vorder = ''
    loc:vorder = '+wpr:Ref_Number,+UPPER(wpr:Part_Number)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('wpr:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BouncerWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BouncerWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BouncerWarrantyParts_LocatorPic','@n8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BouncerWarrantyParts:LookupFrom')
  End!Else
  loc:CloseAction = p_web.site.DefaultPage
  loc:formaction = 'BouncerWarrantyParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BouncerWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BouncerWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BouncerWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="WARPARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="wpr:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Warranty Parts',0)&'</span>'&CRLF
  End
  If clip('Warranty Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BouncerWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BouncerWarrantyParts.locate(''Locator2BouncerWarrantyParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerWarrantyParts.cl(''BouncerWarrantyParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BouncerWarrantyParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('FormCentre')&'" id="BouncerWarrantyParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BouncerWarrantyParts','Part Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BouncerWarrantyParts','Description',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BouncerWarrantyParts','Quantity',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Quantity')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('wpr:record_number',lower(Thisview{prop:order}),1,1) = 0 !and WARPARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'wpr:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('wpr:Record_Number'),p_web.GetValue('wpr:Record_Number'),p_web.GetSessionValue('wpr:Record_Number'))
      loc:FilterWas = 'wpr:Ref_Number = ' & p_web.GetSessionValue('job_ali:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BouncerWarrantyParts_Filter')
    p_web.SetSessionValue('BouncerWarrantyParts_FirstValue','')
    p_web.SetSessionValue('BouncerWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,WARPARTS,wpr:RecordNumberKey,loc:PageRows,'BouncerWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If WARPARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(WARPARTS,loc:firstvalue)
              Reset(ThisView,WARPARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If WARPARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(WARPARTS,loc:lastvalue)
            Reset(ThisView,WARPARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Warranty Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BouncerWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BouncerWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BouncerWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BouncerWarrantyParts.locate(''Locator1BouncerWarrantyParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerWarrantyParts.cl(''BouncerWarrantyParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BouncerWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BouncerWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = wpr:Record_Number
    p_web._thisrow = p_web._nocolon('wpr:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BouncerWarrantyParts:LookupField')) = wpr:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((wpr:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BouncerWarrantyParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If WARPARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(WARPARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If WARPARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(WARPARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wpr:Record_Number',clip(loc:field),,loc:checked,,,'onclick="BouncerWarrantyParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wpr:Record_Number',clip(loc:field),,'checked',,,'onclick="BouncerWarrantyParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BouncerWarrantyParts.omv(this);" onMouseOut="BouncerWarrantyParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BouncerWarrantyParts=new browseTable(''BouncerWarrantyParts'','''&clip(loc:formname)&''','''&p_web._jsok('wpr:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('wpr:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BouncerWarrantyParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BouncerWarrantyParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(WARPARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  Clear(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('wpr:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::wpr:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Part_Number_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Description_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Quantity_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Quantity,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = wpr:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('wpr:Record_Number',wpr:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('wpr:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wpr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wpr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseIMEIHistory    PROCEDURE  (NetWebServerWorker p_web)
tmp:BouncerCompanyName STRING(30)                          !Company Name
tmp:BouncerChargeType STRING(30)                           !Charge Type
tmp:BouncerDays      LONG                                  !Days
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBS_ALIAS)
                      Project(job_ali:Ref_Number)
                      Project(job_ali:Ref_Number)
                      Project(job_ali:date_booked)
                      Project(job_ali:Date_Completed)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBNOTES_ALIAS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseIMEIHistory')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseIMEIHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseIMEIHistory:FormName')
    else
      loc:FormName = 'BrowseIMEIHistory_frm'
    End
    p_web.SSV('BrowseIMEIHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseIMEIHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseIMEIHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseIMEIHistory:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseIMEIHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseIMEIHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  If p_web.RequestAjax = 0
    packet = clip(packet) & '<table><tr><td valign="top">'
    do SendPacket
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBS_ALIAS,job_ali:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOB_ALI:REF_NUMBER') then p_web.SetValue('BrowseIMEIHistory_sort','1')
    ElsIf (loc:vorder = 'TMP:BOUNCERCOMPANYNAME') then p_web.SetValue('BrowseIMEIHistory_sort','2')
    ElsIf (loc:vorder = 'JOB_ALI:DATE_BOOKED') then p_web.SetValue('BrowseIMEIHistory_sort','3')
    ElsIf (loc:vorder = 'JOB_ALI:DATE_COMPLETED') then p_web.SetValue('BrowseIMEIHistory_sort','4')
    ElsIf (loc:vorder = 'TMP:BOUNCERCHARGETYPE') then p_web.SetValue('BrowseIMEIHistory_sort','5')
    ElsIf (loc:vorder = 'TMP:BOUNCERDAYS') then p_web.SetValue('BrowseIMEIHistory_sort','6')
    ElsIf (loc:vorder = 'JBN_ALI:FAULT_DESCRIPTION') then p_web.SetValue('BrowseIMEIHistory_sort','7')
    ElsIf (loc:vorder = 'JBN_ALI:ENGINEERS_NOTES') then p_web.SetValue('BrowseIMEIHistory_sort','8')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseIMEIHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseIMEIHistory:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseIMEIHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseIMEIHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseIMEIHistory:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    If p_web.IfExistsValue('fromURL')
      p_web.StoreValue('fromURL')
    End
    If p_web.IfExistsValue('currentJob')
      p_web.StoreValue('currentJob')
    End
  
      p_web.site.ChangeButton.TextValue = 'View Job'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseIMEIHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseIMEIHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'job_ali:Ref_Number','-job_ali:Ref_Number')
    Loc:LocateField = 'job_ali:Ref_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:BouncerCompanyName','-tmp:BouncerCompanyName')
    Loc:LocateField = 'tmp:BouncerCompanyName'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'job_ali:date_booked','-job_ali:date_booked')
    Loc:LocateField = 'job_ali:date_booked'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'job_ali:Date_Completed','-job_ali:Date_Completed')
    Loc:LocateField = 'job_ali:Date_Completed'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:BouncerChargeType','-tmp:BouncerChargeType')
    Loc:LocateField = 'tmp:BouncerChargeType'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:BouncerDays','-tmp:BouncerDays')
    Loc:LocateField = 'tmp:BouncerDays'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jbn_ali:Fault_Description)','-UPPER(jbn_ali:Fault_Description)')
    Loc:LocateField = 'jbn_ali:Fault_Description'
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jbn_ali:Engineers_Notes)','-UPPER(jbn_ali:Engineers_Notes)')
    Loc:LocateField = 'jbn_ali:Engineers_Notes'
  end
  if loc:vorder = ''
    loc:vorder = '-job_ali:Ref_Number'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('job_ali:Ref_Number')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s8')
  Of upper('tmp:BouncerCompanyName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s30')
  Of upper('job_ali:date_booked')
    loc:SortHeader = p_web.Translate('Date Booked')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@d6b')
  Of upper('job_ali:Date_Completed')
    loc:SortHeader = p_web.Translate('Date Completed')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@D6b')
  Of upper('tmp:BouncerChargeType')
    loc:SortHeader = p_web.Translate('Charge Type')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s30')
  Of upper('tmp:BouncerDays')
    loc:SortHeader = p_web.Translate('Days')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@n4')
  Of upper('jbn_ali:Fault_Description')
    loc:SortHeader = p_web.Translate('Fault Description')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s255')
  Of upper('jbn_ali:Engineers_Notes')
    loc:SortHeader = p_web.Translate('Engineers Notes')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s255')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GSV('fromURL')
  End!Else
  loc:CloseAction = p_web.GSV('fromURL')
    loc:formaction = 'ViewBouncerJob'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseIMEIHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseIMEIHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseIMEIHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBS_ALIAS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="job_ali:Ref_Number_Key"></input><13,10>'
  end
  If p_web.Translate('Previous Job History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Previous Job History',0)&'</span>'&CRLF
  End
  If clip('Previous Job History') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseIMEIHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseIMEIHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseIMEIHistory.locate(''Locator2BrowseIMEIHistory'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseIMEIHistory.cl(''BrowseIMEIHistory'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseIMEIHistory_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseIMEIHistory_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseIMEIHistory','Job Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseIMEIHistory','Company Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseIMEIHistory','Date Booked',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Date Booked')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseIMEIHistory','Date Completed',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Date Completed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseIMEIHistory','Charge Type',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Charge Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseIMEIHistory','Days',,,'CenterJustify',,1)
        Else
          packet = clip(packet) & '<th class="CenterJustify">'&p_web.Translate('Days')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseIMEIHistory','Fault Description',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Fault Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'8','BrowseIMEIHistory','Engineers Notes',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Engineers Notes')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'job_ali:date_booked' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'job_ali:Date_Completed' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('job_ali:ref_number',lower(Thisview{prop:order}),1,1) = 0 !and JOBS_ALIAS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'job_ali:Ref_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('job_ali:Ref_Number'),p_web.GetValue('job_ali:Ref_Number'),p_web.GetSessionValue('job_ali:Ref_Number'))
      loc:FilterWas = 'Upper(job_ali:ESN)  = Upper(<39>' & p_web.GetSessionValue('job:ESN') & '<39>)'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseIMEIHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseIMEIHistory_Filter')
    p_web.SetSessionValue('BrowseIMEIHistory_FirstValue','')
    p_web.SetSessionValue('BrowseIMEIHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBS_ALIAS,job_ali:Ref_Number_Key,loc:PageRows,'BrowseIMEIHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBS_ALIAS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBS_ALIAS,loc:firstvalue)
              Reset(ThisView,JOBS_ALIAS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBS_ALIAS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBS_ALIAS,loc:lastvalue)
            Reset(ThisView,JOBS_ALIAS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (p_web.GSV('currentJob') > 0)
          if job_ali:ref_Number = p_web.GSV('currentJob')
              cycle
          end !if job_ali:ref_Number = p_web.GSV('currentJob')
      end ! if (p_web.GSV('currentJob') > 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(job_ali:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseIMEIHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseIMEIHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseIMEIHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseIMEIHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:found
          Loc:IsChange = 0
          If loc:selecting = 0
            If loc:viewonly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseIMEIHistory')
              loc:isChange = 1
            End
          End
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseIMEIHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseIMEIHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseIMEIHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseIMEIHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseIMEIHistory.locate(''Locator1BrowseIMEIHistory'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseIMEIHistory.cl(''BrowseIMEIHistory'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseIMEIHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseIMEIHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseIMEIHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseIMEIHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseIMEIHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseIMEIHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:found
        Loc:IsChange = 0
        If loc:selecting = 0
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseIMEIHistory')
            loc:isChange = 1
          End
        End
        do SendPacket
  End
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job_ali:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
    
            Else ! If Access:tradeacc.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:tradeacc.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else ! If Access:subtracc.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:subtracc.Tryfetch(sub:Account_Number_Key) = Level:Benign
        tmp:BouncerCompanyName = tra:Company_Name
        tmp:BouncerDays     = Today() - job_ali:Date_Booked
        If job_ali:Chargeable_Job = 'YES' and job_ali:Warranty_Job = 'YES'
            tmp:BouncerChargeType = job_ali:Charge_Type
            tmp:BouncerChargeType = job_ali:Warranty_Charge_Type
        Else !If job_ali:Chargeale_Job = 'YES' and job_ali:Warranty_Job = 'YES'
            If job_ali:Chargeable_Job = 'YES'
                tmp:BouncerChargeType = job_ali:Charge_Type
            End !If job_ali:Chargeable_Job = 'YES'
            If job_ali:Warranty_Job = 'YES'
                tmp:BouncerChargeType = job_ali:Warranty_Charge_Type
            End !If job_ali:Chargeable_Job = 'YES'
        End !If job_ali:Chargeale_Job = 'YES' and job_ali:Warranty_Job = 'YES'
    
        Access:JOBNOTES_ALIAS.ClearKey(jbn_ali:RefNumberKey)
        jbn_ali:RefNumber = job_ali:Ref_Number
        IF (Access:JOBNOTES_ALIAS.TryFetch(jbn_ali:RefNumberKey))
            Access:JOBNOTES_ALIAS.ClearKey(jbn_ali:RefNumberKey)
        END
    
    
    loc:field = job_ali:Ref_Number
    p_web._thisrow = p_web._nocolon('job_ali:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseIMEIHistory:LookupField')) = job_ali:Ref_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((job_ali:Ref_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseIMEIHistory.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:Silent = 0
        if loc:silent = 0
          loc:rowstyle = clip(loc:rowstyle) & 'sv('''',''bounceroutfaults_browseimeihistory'',''refresh='&clip('job_ali:Ref_Number')&''',''' & p_web._jsok(p_web._nocolon('job_ali:Ref_Number')&'='& p_web.escape(loc:field,net:BigPlus))&''',''_ParentProc=BrowseIMEIHistory'',''_Silent='&loc:silent&'''); '
        end
        loc:Silent = 0
        if loc:silent = 0
          loc:rowstyle = clip(loc:rowstyle) & 'sv('''',''bouncerchargeableparts_browseimeihistory'',''refresh='&clip('job_ali:Ref_Number')&''',''' & p_web._jsok(p_web._nocolon('job_ali:Ref_Number')&'='& p_web.escape(loc:field,net:BigPlus))&''',''_ParentProc=BrowseIMEIHistory'',''_Silent='&loc:silent&'''); '
        end
        loc:Silent = 0
        if loc:silent = 0
          loc:rowstyle = clip(loc:rowstyle) & 'sv('''',''bouncerwarrantyparts_browseimeihistory'',''refresh='&clip('job_ali:Ref_Number')&''',''' & p_web._jsok(p_web._nocolon('job_ali:Ref_Number')&'='& p_web.escape(loc:field,net:BigPlus))&''',''_ParentProc=BrowseIMEIHistory'',''_Silent='&loc:silent&'''); '
        end
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBS_ALIAS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBS_ALIAS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBS_ALIAS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBS_ALIAS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','job_ali:Ref_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseIMEIHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','job_ali:Ref_Number',clip(loc:field),,'checked',,,'onclick="BrowseIMEIHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job_ali:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:BouncerCompanyName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job_ali:date_booked
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job_ali:Date_Completed
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:BouncerChargeType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:BouncerDays < 90
              packet = clip(packet) & '<td class="'&clip('BrowseRed')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:BouncerDays
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jbn_ali:Fault_Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jbn_ali:Engineers_Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseIMEIHistory.omv(this);" onMouseOut="BrowseIMEIHistory.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseIMEIHistory=new browseTable(''BrowseIMEIHistory'','''&clip(loc:formname)&''','''&p_web._jsok('job_ali:Ref_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('job_ali:Ref_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''ViewBouncerJob'');<13,10>'&|
      'BrowseIMEIHistory.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseIMEIHistory.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseIMEIHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseIMEIHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseIMEIHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseIMEIHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBS_ALIAS)
  p_web._CloseFile(JOBNOTES_ALIAS)
  p_web._CloseFile(TRADEACC)
  p_web._CloseFile(SUBTRACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBS_ALIAS)
  Bind(job_ali:Record)
  Clear(job_ali:Record)
  NetWebSetSessionPics(p_web,JOBS_ALIAS)
  p_web._OpenFile(JOBNOTES_ALIAS)
  Bind(jbn_ali:Record)
  NetWebSetSessionPics(p_web,JOBNOTES_ALIAS)
  p_web._OpenFile(TRADEACC)
  Bind(tra:Record)
  NetWebSetSessionPics(p_web,TRADEACC)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('job_ali:Ref_Number',loc:default)
    p_web.SetValue('refresh','first')
    If loc:ParentSilent
      loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerOutFaults:NoForm',loc:noform)
      p_web.SetValue('BouncerOutFaults:FormName',loc:formname)
    end
    p_web.SetValue('_ParentProc','BrowseIMEIHistory')
    p_web.SetValue('_Silent',loc:Silent)
    p_web.SetValue('BouncerOutFaults:parentIs','Browse')
    BouncerOutFaults(p_web)
    If loc:ParentSilent
      loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerChargeableParts:NoForm',loc:noform)
      p_web.SetValue('BouncerChargeableParts:FormName',loc:formname)
    end
    p_web.SetValue('_ParentProc','BrowseIMEIHistory')
    p_web.SetValue('_Silent',loc:Silent)
    p_web.SetValue('BouncerChargeableParts:parentIs','Browse')
    BouncerChargeableParts(p_web)
    If loc:ParentSilent
      loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerWarrantyParts:NoForm',loc:noform)
      p_web.SetValue('BouncerWarrantyParts:FormName',loc:formname)
    end
    p_web.SetValue('_ParentProc','BrowseIMEIHistory')
    p_web.SetValue('_Silent',loc:Silent)
    p_web.SetValue('BouncerWarrantyParts:parentIs','Browse')
    BouncerWarrantyParts(p_web)

StartChildren  Routine
    packet = clip(packet) & '</td>'
    packet = clip(packet) & '</tr>'
    If Loc:ParentSilent = 1
      Loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerOutFaults:NoForm',loc:noform)
      p_web.SetValue('BouncerOutFaults:FormName',loc:formname)
    end
    p_web.SetValue('BouncerOutFaults:parentIs','Browse')
    loc:extra = ''
    packet = clip(packet) & '<tr>'&loc:extra&'<td valign="top"><!-- Net:BouncerOutFaults?Refresh=first&_ParentProc=BrowseIMEIHistory&_Silent='&loc:silent&' --></td></tr>'
    If Loc:ParentSilent = 1
      Loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerChargeableParts:NoForm',loc:noform)
      p_web.SetValue('BouncerChargeableParts:FormName',loc:formname)
    end
    p_web.SetValue('BouncerChargeableParts:parentIs','Browse')
    loc:extra = ''
    packet = clip(packet) & '<tr>'&loc:extra&'<td valign="top"><!-- Net:BouncerChargeableParts?Refresh=first&_ParentProc=BrowseIMEIHistory&_Silent='&loc:silent&' --></td></tr>'
    If Loc:ParentSilent = 1
      Loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerWarrantyParts:NoForm',loc:noform)
      p_web.SetValue('BouncerWarrantyParts:FormName',loc:formname)
    end
    p_web.SetValue('BouncerWarrantyParts:parentIs','Browse')
    loc:extra = ''
    packet = clip(packet) & '<tr>'&loc:extra&'<td valign="top"><!-- Net:BouncerWarrantyParts?Refresh=first&_ParentProc=BrowseIMEIHistory&_Silent='&loc:silent&' --></td></tr>'
    packet = clip(packet) & '</table>'
    p_web._DivHeader(lower('BrowseIMEIHistory_BouncerOutFaults_value'))
    p_web._DivFooter()
    p_web._RegisterDivEx(lower('BrowseIMEIHistory_BouncerOutFaults_value'))
    p_web._DivHeader(lower('BrowseIMEIHistory_BouncerChargeableParts_value'))
    p_web._DivFooter()
    p_web._RegisterDivEx(lower('BrowseIMEIHistory_BouncerChargeableParts_value'))
    p_web._DivHeader(lower('BrowseIMEIHistory_BouncerWarrantyParts_value'))
    p_web._DivFooter()
    p_web._RegisterDivEx(lower('BrowseIMEIHistory_BouncerWarrantyParts_value'))
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::job_ali:Ref_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job_ali:Ref_Number_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job_ali:Ref_Number,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:BouncerCompanyName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerCompanyName_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:BouncerCompanyName,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job_ali:date_booked   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job_ali:date_booked_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job_ali:date_booked,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job_ali:Date_Completed   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job_ali:Date_Completed_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job_ali:Date_Completed,'@D6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:BouncerChargeType   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerChargeType_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:BouncerChargeType,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:BouncerDays   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:BouncerDays < 90
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerDays_'&job_ali:Ref_Number,'BrowseRed',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:BouncerDays,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerDays_'&job_ali:Ref_Number,'CenterJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:BouncerDays,'@n4')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jbn_ali:Fault_Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jbn_ali:Fault_Description_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jbn_ali:Fault_Description,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jbn_ali:Engineers_Notes   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jbn_ali:Engineers_Notes_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jbn_ali:Engineers_Notes,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES_ALIAS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES_ALIAS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = job_ali:Ref_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('job_ali:Ref_Number',job_ali:Ref_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('job_ali:Ref_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('job_ali:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('job_ali:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BouncerChargeableParts PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(PARTS)
                      Project(par:Record_Number)
                      Project(par:Part_Number)
                      Project(par:Description)
                      Project(par:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BouncerChargeableParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BouncerChargeableParts:NoForm')
      loc:NoForm = p_web.GetValue('BouncerChargeableParts:NoForm')
      loc:FormName = p_web.GetValue('BouncerChargeableParts:FormName')
    else
      loc:FormName = 'BouncerChargeableParts_frm'
    End
    p_web.SSV('BouncerChargeableParts:NoForm',loc:NoForm)
    p_web.SSV('BouncerChargeableParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BouncerChargeableParts:NoForm')
    loc:FormName = p_web.GSV('BouncerChargeableParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BouncerChargeableParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BouncerChargeableParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(PARTS,par:recordnumberkey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'PAR:PART_NUMBER') then p_web.SetValue('BouncerChargeableParts_sort','1')
    ElsIf (loc:vorder = 'PAR:DESCRIPTION') then p_web.SetValue('BouncerChargeableParts_sort','2')
    ElsIf (loc:vorder = 'PAR:QUANTITY') then p_web.SetValue('BouncerChargeableParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BouncerChargeableParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BouncerChargeableParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BouncerChargeableParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BouncerChargeableParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BouncerChargeableParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BouncerChargeableParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BouncerChargeableParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(par:Part_Number)','-UPPER(par:Part_Number)')
    Loc:LocateField = 'par:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(par:Description)','-UPPER(par:Description)')
    Loc:LocateField = 'par:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'par:Quantity','-par:Quantity')
    Loc:LocateField = 'par:Quantity'
  end
  if loc:vorder = ''
    loc:vorder = '+par:Ref_Number,+UPPER(par:Part_Number)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('par:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BouncerChargeableParts_LocatorPic','@s30')
  Of upper('par:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BouncerChargeableParts_LocatorPic','@s30')
  Of upper('par:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BouncerChargeableParts_LocatorPic','@n8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BouncerChargeableParts:LookupFrom')
  End!Else
  loc:CloseAction = p_web.site.DefaultPage
  loc:formaction = 'BouncerChargeableParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BouncerChargeableParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BouncerChargeableParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BouncerChargeableParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="PARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="par:recordnumberkey"></input><13,10>'
  end
  If p_web.Translate('Chargeable Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Chargeable Parts',0)&'</span>'&CRLF
  End
  If clip('Chargeable Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerChargeableParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BouncerChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BouncerChargeableParts.locate(''Locator2BouncerChargeableParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerChargeableParts.cl(''BouncerChargeableParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BouncerChargeableParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('FormCentre')&'" id="BouncerChargeableParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BouncerChargeableParts','Part Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BouncerChargeableParts','Description',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BouncerChargeableParts','Quantity',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Quantity')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('par:record_number',lower(Thisview{prop:order}),1,1) = 0 !and PARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'par:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('par:Record_Number'),p_web.GetValue('par:Record_Number'),p_web.GetSessionValue('par:Record_Number'))
      loc:FilterWas = 'par:Ref_Number = ' & p_web.GetSessionValue('job_ali:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerChargeableParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BouncerChargeableParts_Filter')
    p_web.SetSessionValue('BouncerChargeableParts_FirstValue','')
    p_web.SetSessionValue('BouncerChargeableParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,PARTS,par:recordnumberkey,loc:PageRows,'BouncerChargeableParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If PARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(PARTS,loc:firstvalue)
              Reset(ThisView,PARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If PARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(PARTS,loc:lastvalue)
            Reset(ThisView,PARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(par:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Chargeable Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerChargeableParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BouncerChargeableParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BouncerChargeableParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BouncerChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BouncerChargeableParts.locate(''Locator1BouncerChargeableParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerChargeableParts.cl(''BouncerChargeableParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BouncerChargeableParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BouncerChargeableParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = par:Record_Number
    p_web._thisrow = p_web._nocolon('par:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BouncerChargeableParts:LookupField')) = par:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((par:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BouncerChargeableParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If PARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(PARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If PARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(PARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','par:Record_Number',clip(loc:field),,loc:checked,,,'onclick="BouncerChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','par:Record_Number',clip(loc:field),,'checked',,,'onclick="BouncerChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BouncerChargeableParts.omv(this);" onMouseOut="BouncerChargeableParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BouncerChargeableParts=new browseTable(''BouncerChargeableParts'','''&clip(loc:formname)&''','''&p_web._jsok('par:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('par:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BouncerChargeableParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BouncerChargeableParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerChargeableParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerChargeableParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerChargeableParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerChargeableParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  Clear(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('par:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::par:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Part_Number_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::par:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Description_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::par:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Quantity_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Quantity,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = par:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('par:Record_Number',par:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('par:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('par:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('par:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BouncerOutFaults     PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBOUTFL)
                      Project(joo:RecordNumber)
                      Project(joo:FaultCode)
                      Project(joo:Description)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BouncerOutFaults')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BouncerOutFaults:NoForm')
      loc:NoForm = p_web.GetValue('BouncerOutFaults:NoForm')
      loc:FormName = p_web.GetValue('BouncerOutFaults:FormName')
    else
      loc:FormName = 'BouncerOutFaults_frm'
    End
    p_web.SSV('BouncerOutFaults:NoForm',loc:NoForm)
    p_web.SSV('BouncerOutFaults:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BouncerOutFaults:NoForm')
    loc:FormName = p_web.GSV('BouncerOutFaults:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BouncerOutFaults') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BouncerOutFaults')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBOUTFL,joo:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOO:FAULTCODE') then p_web.SetValue('BouncerOutFaults_sort','1')
    ElsIf (loc:vorder = 'JOO:DESCRIPTION') then p_web.SetValue('BouncerOutFaults_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BouncerOutFaults:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BouncerOutFaults:LookupFrom','LookupFrom')
    p_web.StoreValue('BouncerOutFaults:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BouncerOutFaults:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BouncerOutFaults:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BouncerOutFaults_sort',net:DontEvaluate)
  p_web.SetSessionValue('BouncerOutFaults_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joo:FaultCode)','-UPPER(joo:FaultCode)')
    Loc:LocateField = 'joo:FaultCode'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joo:Description)','-UPPER(joo:Description)')
    Loc:LocateField = 'joo:Description'
  end
  if loc:vorder = ''
    loc:vorder = '+joo:JobNumber,+UPPER(joo:FaultCode)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joo:FaultCode')
    loc:SortHeader = p_web.Translate('Fault Code')
    p_web.SetSessionValue('BouncerOutFaults_LocatorPic','@s30')
  Of upper('joo:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BouncerOutFaults_LocatorPic','@s255')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BouncerOutFaults:LookupFrom')
  End!Else
  loc:CloseAction = p_web.site.DefaultPage
  loc:formaction = 'BouncerOutFaults'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BouncerOutFaults:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BouncerOutFaults:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BouncerOutFaults:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBOUTFL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joo:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Outfaults') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Outfaults',0)&'</span>'&CRLF
  End
  If clip('Outfaults') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerOutFaults',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BouncerOutFaults',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BouncerOutFaults.locate(''Locator2BouncerOutFaults'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerOutFaults.cl(''BouncerOutFaults'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BouncerOutFaults_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('FormCentre')&'" id="BouncerOutFaults_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BouncerOutFaults','Fault Code',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Fault Code')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BouncerOutFaults','Description',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('joo:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBOUTFL{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'joo:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joo:RecordNumber'),p_web.GetValue('joo:RecordNumber'),p_web.GetSessionValue('joo:RecordNumber'))
      loc:FilterWas = 'joo:JobNumber = ' & p_web.GetSessionValue('job_ali:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerOutFaults',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BouncerOutFaults_Filter')
    p_web.SetSessionValue('BouncerOutFaults_FirstValue','')
    p_web.SetSessionValue('BouncerOutFaults_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBOUTFL,joo:RecordNumberKey,loc:PageRows,'BouncerOutFaults',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBOUTFL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBOUTFL,loc:firstvalue)
              Reset(ThisView,JOBOUTFL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBOUTFL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBOUTFL,loc:lastvalue)
            Reset(ThisView,JOBOUTFL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joo:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerOutFaults.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerOutFaults.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerOutFaults.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerOutFaults.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerOutFaults',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BouncerOutFaults_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BouncerOutFaults_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BouncerOutFaults',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BouncerOutFaults.locate(''Locator1BouncerOutFaults'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerOutFaults.cl(''BouncerOutFaults'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BouncerOutFaults_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BouncerOutFaults_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerOutFaults.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerOutFaults.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerOutFaults.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerOutFaults.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = joo:RecordNumber
    p_web._thisrow = p_web._nocolon('joo:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BouncerOutFaults:LookupField')) = joo:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((joo:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BouncerOutFaults.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBOUTFL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBOUTFL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBOUTFL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBOUTFL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joo:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BouncerOutFaults.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joo:RecordNumber',clip(loc:field),,'checked',,,'onclick="BouncerOutFaults.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joo:FaultCode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joo:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BouncerOutFaults.omv(this);" onMouseOut="BouncerOutFaults.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BouncerOutFaults=new browseTable(''BouncerOutFaults'','''&clip(loc:formname)&''','''&p_web._jsok('joo:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('joo:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BouncerOutFaults.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BouncerOutFaults.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerOutFaults')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerOutFaults')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerOutFaults')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerOutFaults')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBOUTFL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBOUTFL)
  Bind(joo:Record)
  Clear(joo:Record)
  NetWebSetSessionPics(p_web,JOBOUTFL)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('joo:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::joo:FaultCode   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joo:FaultCode_'&joo:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joo:FaultCode,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joo:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joo:Description_'&joo:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joo:Description,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = joo:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('joo:RecordNumber',joo:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('joo:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joo:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joo:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ViewBouncerJob       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CurrentTradeAccount STRING(30)                         !Current Trade Account
FilesOpened     Long
JOBS_ALIAS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('ViewBouncerJob')
  loc:formname = 'ViewBouncerJob_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ViewBouncerJob','')
    p_web._DivHeader('ViewBouncerJob',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewBouncerJob',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ViewBouncerJob_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBS_ALIAS')
  p_web.SetValue('UpdateKey','job_ali:Ref_Number_Key')
  p_web.SetValue('IDField','job_ali:Ref_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('ViewBouncerJob:Primed') = 1
    p_web._deleteFile(JOBS_ALIAS)
    p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBS_ALIAS')
  p_web.SetValue('UpdateKey','job_ali:Ref_Number_Key')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tra:Account_Number',tra:Account_Number)
  p_web.SetSessionValue('tra:Company_Name',tra:Company_Name)
  p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1)
  p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2)
  p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3)
  p_web.SetSessionValue('tra:Postcode',tra:Postcode)
  p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number)
  p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number)
  p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tra:Account_Number')
    tra:Account_Number = p_web.GetValue('tra:Account_Number')
    p_web.SetSessionValue('tra:Account_Number',tra:Account_Number)
  End
  if p_web.IfExistsValue('tra:Company_Name')
    tra:Company_Name = p_web.GetValue('tra:Company_Name')
    p_web.SetSessionValue('tra:Company_Name',tra:Company_Name)
  End
  if p_web.IfExistsValue('tra:Address_Line1')
    tra:Address_Line1 = p_web.GetValue('tra:Address_Line1')
    p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1)
  End
  if p_web.IfExistsValue('tra:Address_Line2')
    tra:Address_Line2 = p_web.GetValue('tra:Address_Line2')
    p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2)
  End
  if p_web.IfExistsValue('tra:Address_Line3')
    tra:Address_Line3 = p_web.GetValue('tra:Address_Line3')
    p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3)
  End
  if p_web.IfExistsValue('tra:Postcode')
    tra:Postcode = p_web.GetValue('tra:Postcode')
    p_web.SetSessionValue('tra:Postcode',tra:Postcode)
  End
  if p_web.IfExistsValue('tra:Telephone_Number')
    tra:Telephone_Number = p_web.GetValue('tra:Telephone_Number')
    p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number)
  End
  if p_web.IfExistsValue('tra:Fax_Number')
    tra:Fax_Number = p_web.GetValue('tra:Fax_Number')
    p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number)
  End
  if p_web.IfExistsValue('tra:EmailAddress')
    tra:EmailAddress = p_web.GetValue('tra:EmailAddress')
    p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ViewBouncerJob_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  If p_web.GetSessionValue('job:Account_Number') <> ''
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GetSessionValue('job:Account_Number')
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          tmp:CurrentTradeAccount = sub:Main_Account_Number
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  End ! If p_web.GetSessionValue('tmp:AccountNumber') <> ''
  
  
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GetSessionValue('job_ali:Account_Number')
  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      p_web.FileToSessionQueue(SUBTRACC)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          p_web.FileToSessionQueue(TRADEACC)
      End ! If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  If tmp:CurrentTradeAccount = tra:Account_Number
      p_web.SetSessionValue('Local:NoAuthorise',1)
  Else ! If tmp:CurrentTradeAccount = p_web.GetSessionValue('tra:Account_Number')
      p_web.SetSessionValue('Local:NoAuthorise',0)
  End ! If tmp:CurrentTradeAccount = p_web.GetSessionValue('tra:Account_Number')
  
  If SecurityCheckFailed(p_web.GetSessionValue('BookingUserPassword'),'BOUNCER - AUTHORISE BILLING')
      p_web.SetSessionValue('Local:NoAuthorise',0)
  End ! If SecurityCheckFailed('RAPID ENG - CHANGE JOB STATUS')
  
  p_web.site.SaveButton.TextValue = 'Authorise'
  
  p_web.SSV('NewAccountAuthorised',0)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'NewJobBooking'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewBouncerJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewBouncerJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewBouncerJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseIMEIHistory'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GetSessionValue('Local:NoAuthorise') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBS_ALIAS__FileAction" value="'&p_web.getSessionValue('JOBS_ALIAS:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBS_ALIAS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBS_ALIAS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="job_ali:Ref_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ViewBouncerJob" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ViewBouncerJob" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ViewBouncerJob" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','job_ali:Ref_Number',p_web._jsok(p_web.getSessionValue('job_ali:Ref_Number'))) & '<13,10>'
  If p_web.Translate('View Job Details') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('View Job Details',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ViewBouncerJob">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ViewBouncerJob">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ViewBouncerJob')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ViewBouncerJob')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ViewBouncerJob'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ViewBouncerJob')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewBouncerJob_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Company_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Address_Line1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Address_Line2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Address_Line3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Telephone_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Fax_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:EmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:EmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewBouncerJob_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GetSessionValue('Local:NoAuthorise') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TextDisplay
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tra:Account_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Account_Number',p_web.GetValue('NewValue'))
    tra:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Account_Number
    do Value::tra:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Account_Number = p_web.GetValue('Value')
  End
  do Value::tra:Account_Number
  do SendAlert

Value::tra:Account_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Account_Number'',''viewbouncerjob_tra:account_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Account_Number',p_web.GetSessionValueFormat('tra:Account_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_value')


Prompt::tra:Company_Name  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Company_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Company_Name',p_web.GetValue('NewValue'))
    tra:Company_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Company_Name
    do Value::tra:Company_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Company_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Company_Name = p_web.GetValue('Value')
  End
  do Value::tra:Company_Name
  do SendAlert

Value::tra:Company_Name  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Company_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Company_Name'',''viewbouncerjob_tra:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Company_Name',p_web.GetSessionValueFormat('tra:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_value')


Prompt::tra:Address_Line1  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Address_Line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Address_Line1',p_web.GetValue('NewValue'))
    tra:Address_Line1 = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Address_Line1
    do Value::tra:Address_Line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Address_Line1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Address_Line1 = p_web.GetValue('Value')
  End
  do Value::tra:Address_Line1
  do SendAlert

Value::tra:Address_Line1  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Address_Line1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line1'',''viewbouncerjob_tra:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line1',p_web.GetSessionValueFormat('tra:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_value')


Prompt::tra:Address_Line2  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Address_Line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Address_Line2',p_web.GetValue('NewValue'))
    tra:Address_Line2 = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Address_Line2
    do Value::tra:Address_Line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Address_Line2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Address_Line2 = p_web.GetValue('Value')
  End
  do Value::tra:Address_Line2
  do SendAlert

Value::tra:Address_Line2  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Address_Line2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line2'',''viewbouncerjob_tra:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line2',p_web.GetSessionValueFormat('tra:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_value')


Prompt::tra:Address_Line3  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Address_Line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Address_Line3',p_web.GetValue('NewValue'))
    tra:Address_Line3 = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Address_Line3
    do Value::tra:Address_Line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Address_Line3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Address_Line3 = p_web.GetValue('Value')
  End
  do Value::tra:Address_Line3
  do SendAlert

Value::tra:Address_Line3  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Address_Line3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line3'',''viewbouncerjob_tra:address_line3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line3',p_web.GetSessionValueFormat('tra:Address_Line3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_value')


Prompt::tra:Postcode  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Postcode',p_web.GetValue('NewValue'))
    tra:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Postcode
    do Value::tra:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Postcode = p_web.GetValue('Value')
  End
  do Value::tra:Postcode
  do SendAlert

Value::tra:Postcode  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Postcode'',''viewbouncerjob_tra:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Postcode',p_web.GetSessionValueFormat('tra:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_value')


Prompt::tra:Telephone_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Telephone_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Telephone_Number',p_web.GetValue('NewValue'))
    tra:Telephone_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Telephone_Number
    do Value::tra:Telephone_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Telephone_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Telephone_Number = p_web.GetValue('Value')
  End
  do Value::tra:Telephone_Number
  do SendAlert

Value::tra:Telephone_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Telephone_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Telephone_Number'',''viewbouncerjob_tra:telephone_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Telephone_Number',p_web.GetSessionValueFormat('tra:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_value')


Prompt::tra:Fax_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Fax_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Fax_Number',p_web.GetValue('NewValue'))
    tra:Fax_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Fax_Number
    do Value::tra:Fax_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Fax_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Fax_Number = p_web.GetValue('Value')
  End
  do Value::tra:Fax_Number
  do SendAlert

Value::tra:Fax_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Fax_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Fax_Number'',''viewbouncerjob_tra:fax_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Fax_Number',p_web.GetSessionValueFormat('tra:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_value')


Prompt::tra:EmailAddress  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:EmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:EmailAddress',p_web.GetValue('NewValue'))
    tra:EmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:EmailAddress
    do Value::tra:EmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:EmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    tra:EmailAddress = p_web.GetValue('Value')
  End
  do Value::tra:EmailAddress
  do SendAlert

Value::tra:EmailAddress  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:EmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:EmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(60) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:EmailAddress'',''viewbouncerjob_tra:emailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:EmailAddress',p_web.GetSessionValueFormat('tra:EmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_value')


Validate::TextDisplay  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TextDisplay',p_web.GetValue('NewValue'))
    do Value::TextDisplay
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::TextDisplay  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('TextDisplay') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Select "Authorise" to use the above details on the job',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ViewBouncerJob_tra:Account_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Account_Number
      else
        do Value::tra:Account_Number
      end
  of lower('ViewBouncerJob_tra:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Company_Name
      else
        do Value::tra:Company_Name
      end
  of lower('ViewBouncerJob_tra:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line1
      else
        do Value::tra:Address_Line1
      end
  of lower('ViewBouncerJob_tra:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line2
      else
        do Value::tra:Address_Line2
      end
  of lower('ViewBouncerJob_tra:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line3
      else
        do Value::tra:Address_Line3
      end
  of lower('ViewBouncerJob_tra:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Postcode
      else
        do Value::tra:Postcode
      end
  of lower('ViewBouncerJob_tra:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Telephone_Number
      else
        do Value::tra:Telephone_Number
      end
  of lower('ViewBouncerJob_tra:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Fax_Number
      else
        do Value::tra:Fax_Number
      end
  of lower('ViewBouncerJob_tra:EmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:EmailAddress
      else
        do Value::tra:EmailAddress
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)

PreCopy  Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  p_web._PreCopyRecord(JOBS_ALIAS,job_ali:Ref_Number_Key)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)

PreDelete       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  If p_web.Failed = 0
      p_web.SetSessionValue('job:Account_Number',p_web.GetSessionValue('sub:Account_Number'))
      If p_web.GetSessionValue('sub:Generic_Account') = 1
          p_web.SetSessionValue('FranchiseAccount',2)
      Else ! If p_web.GetSessionValue('sub:GenericAcount') = 1
          p_web.SetSessionValue('FranchiseAccount',1)
      End ! If p_web.GetSessionValue('sub:GenericAcount') = 1
      p_web.SetSessionValue('Hide:PreviousAddress',1)
      p_web.SSV('NewAccountAuthorised',1)
  End ! If p_web.Failed = 0

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewBouncerJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewBouncerJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
  If p_web.GetSessionValue('ViewBouncerJob:Primed') = 1 ! Autonumbered fields are not validated unless the record has been pre-primed
    If job_ali:Ref_Number = 0
      loc:Invalid = 'job_ali:Ref_Number'
      loc:Alert = clip('job_ali:Ref_Number') & ' ' & p_web.site.NotZeroText
      !exit
    End
  End
  If Loc:Invalid <> '' then exit.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  p_web.StoreValue('tra:Account_Number')
  p_web.StoreValue('tra:Company_Name')
  p_web.StoreValue('tra:Address_Line1')
  p_web.StoreValue('tra:Address_Line2')
  p_web.StoreValue('tra:Address_Line3')
  p_web.StoreValue('tra:Postcode')
  p_web.StoreValue('tra:Telephone_Number')
  p_web.StoreValue('tra:Fax_Number')
  p_web.StoreValue('tra:EmailAddress')
  p_web.StoreValue('')

PostDelete      Routine
