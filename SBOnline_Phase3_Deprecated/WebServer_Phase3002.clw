

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3002.INC'),ONCE        !Local module procedure declarations
                     END


GetStatus            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
  CODE
    ! GetStatus:StatusNumber
    ! GetStatus:Type
    if (p_web.GSV('GetStatus:StatusNumber') <> '' and p_web.GSV('GetStatus:Type') <> '')
        relate:DEFAULTS.open()
        relate:STATUS.open()
        relate:AUDSTATS.open()
        relate:STAHEAD.open()
        relate:JOBSTAGE.open()
    
        Access:DEFAULTS.Clearkey(def:RecordNumberKey)
        def:Record_Number = 0
        Set(def:RecordNumberKey,def:RecordNumberKey)
        Loop
            If Access:DEFAULTS.Next()
                Break
            End ! If Access:DEFAULTS.Next()
            Break
        End ! Loop
    
        Case p_web.GSV('GetStatus:Type')
        Of 'JOB'
            If p_web.GSV('job:Cancelled') <> 'YES'
                Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
                sts:Ref_Number = p_web.GSV('GetStatus:StatusNumber')
                If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                    p_web.SSV('job:Current_Status','ERROR (' & Clip(p_web.GSV('GetStatus:StatusNumber')) & ')')
                Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                    If sts:Status <> p_web.GSV('job:Current_Status')
                        p_web.SSV('job:PreviousStatus',p_web.GSV('job:Current_Status'))
                        p_web.SSV('job:StatusUser',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Current_Status',sts:Status)
                        p_web.SSV('wob:Current_Status_Date',Today())
                        p_web.SSV('wob:Current_Status',sts:Status)
                        If Access:AUDSTATS.PrimeRecord() = Level:Benign
                            aus:RefNumber = p_web.GSV('job:Ref_Number')
                            aus:Type = 'JOB'
                            aus:DateChanged = Today()
                            aus:TimeChanged = Clock()
                            aus:OldStatus = p_web.GSV('job:PreviousStatus')
                            aus:NewStatus = p_web.GSV('job:Current_Status')
                            aus:UserCode = p_web.GSV('BookingUserCode')
                            If Access:AUDSTATS.TryInsert() = Level:Benign
    
                            Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                                Access:AUDSTATS.CancelAutoInc()
                            End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                        End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign
    
                        Access:STAHEAD.Clearkey(sth:Ref_Number_Key)
                        sth:Ref_Number = sts:Heading_Ref_Number
                        If Access:STAHEAD.TryFetch(sth:Ref_Number_Key) = Level:Benign
                            Access:JOBSTAGE.Clearkey(jst:Job_Stage_Key)
                            jst:Ref_Number = p_web.GSV('job:Ref_Number')
                            jst:Job_Stage = sth:Heading
                            If Access:JOBSTAGE.TryFetch(jst:Job_Stage_Key)
                                If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                                    jst:Ref_Number = p_web.GSV('job:Ref_Number')
                                    jst:Job_Stage = sth:Heading
                                    jst:Time = CLock()
                                    jst:Date = Today()
                                    jst:User = p_web.GSV('BookingUserCode')
                                    If Access:JOBSTAGE.TryInsert() = Level:Benign
    
                                    Else ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                                        Access:JOBSTAGE.CancelAutoInc()
                                    End ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                                End ! If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                            End ! If Access:JOBSTAGE.TryFetch()
                        End ! If Access:STAHEAD.TryFetch(sth:Ref_Number_Key) = Level:Benign
    
                        If sts:use_turnaround_time  = 'YES'
                            EndDays# = Today()
                            x# = 0
                            count# = 0
                            If sts:Turnaround_Days <> 0
                                Loop
                                    count# += 1
                                    day_number# = (Today() + count#) % 7
                                    If def:include_saturday <> 'YES'
                                        If day_number# = 6
                                            EndDays# += 1
                                            Cycle
                                        End
                                    End
                                    If def:include_sunday <> 'YES'
                                        If day_number# = 0
                                            EndDays# += 1
                                            Cycle
                                        End
                                    End
                                    EndDays# += 1
                                    x# += 1
                                    If x# >= sts:Turnaround_Days
                                        Break
                                    End!If x# >= sts:turnaround_days
                                End!Loop
                            End!If f_days <> ''
    
                            EndHours# = Clock()
                            If sts:Turnaround_Hours <> 0
                                start_time_temp# = Clock()
                                new_day# = 0
                                Loop x# = 1 To sts:Turnaround_Hours
                                    EndHours# += 360000
                                    If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
                                        If EndHours# > def:End_Work_Hours or EndHours# < def:Start_Work_Hours
                                            EndHours# = def:Start_Work_Hours
                                            EndDays# += 1
                                        End !If tmp:End_Hours > def:End_Work_Hours
                                    End !If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
    
                                End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
                            End!If f_hours <> ''
    
                            p_web.SSV('job:Status_End_Time',EndHours#)
                            p_web.SSV('job:status_end_date',EndDays#)
                        Else!If sts:use_turnaround_time = 'YES'
                            p_web.SSV('job:status_end_date',Deformat('1/1/2050',@d6))
                            p_web.SSV('job:status_end_Time',Clock())
                        End!If sts:use_turnaround_time  = 'YES'
                    Else ! If sts:Status <> job:Current_Status
                    End ! If sts:Status <> job:Current_Status
                End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
            End ! If job:Cancelled <> 'YES'
        Of 'EXC'
            Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
            sts:Ref_Number = p_web.GSV('GetStatus:StatusNumber')
            If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                p_web.SSV('job:Exchange_Status','ERROR (' & Clip(p_web.GSV('GetStatus:StatusNumber')) & ')')
            Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                If sts:Status <> p_web.GSV('job:Exchange_Status')
                    PreviousStatus" = p_web.GSV('job:Exchange_Status')
                    p_web.SSV('job:Exchange_Status',sts:Status)
                    p_web.SSV('wob:Exchange_Status_Date',Today())
                    p_web.SSV('wob:Exchange_Status',sts:Status)
                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
                        aus:RefNumber = p_web.GSV('job:Ref_Number')
                        aus:Type = 'EXC'
                        aus:DateChanged = Today()
                        aus:TimeChanged = Clock()
                        aus:OldStatus = PreviousStatus"
                        aus:NewStatus = p_web.GSV('job:Exchange_Status')
                        aus:UserCode = p_web.GSV('BookingUserCode')
                        If Access:AUDSTATS.TryInsert() = Level:Benign
    
                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                            Access:AUDSTATS.CancelAutoInc()
                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                    End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign
    
                Else ! If sts:Status <> job:Current_Status
                End ! If sts:Status <> job:Current_Status
            End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
    
        Of 'LOA'
            Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
            sts:Ref_Number = p_web.GSV('GetStatus:StatusNumber')
            If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                p_web.SSV('job:Loan_Status','ERROR (' & Clip(p_web.GSV('GetStatus:StatusNumber')) & ')')
            Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                If sts:Status <> p_web.GSV('job:Loan_Status')
                    PreviousStatus" = p_web.GSV('job:Loan_Status')
                    p_web.SSV('job:Loan_Status',sts:Status)
                    p_web.SSV('wob:Loan_Status_Date',Today())
                    p_web.SSV('wob:Loan_Status',sts:Status)
                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
                        aus:RefNumber = p_web.GSV('job:Ref_Number')
                        aus:Type = 'LOA'
                        aus:DateChanged = Today()
                        aus:TimeChanged = Clock()
                        aus:OldStatus = PreviousStatus"
                        aus:NewStatus = p_web.GSV('job:Loan_Status')
                        aus:UserCode = p_web.GSV('BookingUserCode')
                        If Access:AUDSTATS.TryInsert() = Level:Benign
    
                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                            Access:AUDSTATS.CancelAutoInc()
                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                    End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign
    
                Else ! If sts:Status <> job:Current_Status
                End ! If sts:Status <> job:Current_Status
            End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
    
        End ! Case p_web.GSV('GetStatus:Type')
    
    end ! if (p_web.GSV('GetStatus:StatusNumber') <> '' and p_web.GSV('GetStatus:StatusType') <> '')
    
    p_web.deleteSessionValue('GetStatus:StatusNumber')
    p_web.deleteSessionValue('GetStatus:Type')
AddToAudit           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
  CODE
    if (p_web.GSV('AddToAudit:Action') <> '' and p_web.GSV('AddToAudit:Type') <> '')
        relate:AUDIT.open()
        relate:AUDITE.open()

        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Date        = Today()
            aud:Time        = Clock()
            aud:Ref_Number  = p_web.GSV('job:Ref_Number')
            aud:User        = p_web.GSV('BookingUserCode')
            aud:Action      = p_web.GSV('AddToAudit:Action')
            aud:Notes       = p_web.GSV('AddToAudit:Notes')
            aud:Type        = p_web.GSV('AddToAudit:Type')
            If Access:AUDIT.TryInsert() = Level:Benign
                ! Insert Successful
                Access:AUDITE.Open()
                Access:AUDITE.UseFile()
                If Access:AUDITE.PrimeRecord() = Level:Benign
                    aude:RefNumber = aud:Record_Number
                    aude:IPAddress = p_web.RequestData.FromIP
                    aude:HostName = 'SB Online'
                    If Access:AUDITE.TryInsert() = Level:Benign
                        ! Insert Successful
                   Else ! If Access:AUDITE.TryInsert() = Level:Benign
                        ! Insert Failed
                        Access:AUDITE.CancelAutoInc()
                    End ! If Access:AUDITE.TryInsert() = Level:Benign
                End !If Access:AUDITE.PrimeRecord() = Level:Benign
                Access:AUDITE.Close()
            Else ! If Access:AUDIT.TryInsert() = Level:Benign
                ! Insert Failed
                Access:AUDIT.CancelAutoInc()
            End ! If Access:AUDIT.TryInsert() = Level:Benign
        End !If Access:AUDIT.PrimeRecord() = Level:Benign
        ! Clear fields to stop being written incorrectly (DBH: 02/03/2009)

        relate:AUDITE.close()
        relate:AUDIT.close()
    end ! if (p_web.GSV('AddToAudit:Action') <> '' and p_web.GSV('AddToAudit:Type') <> '')

    p_web.deleteSessionValue('AddToAudit:Action')
    p_web.deleteSessionValue('AddToAudit:Notes')
    p_web.deleteSessionValue('AddToAudit:Type')
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
DespatchNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locFullJobNumber     STRING(30)                            !
locChargeType        STRING(30)                            !
locRepairType        STRING(30)                            !
locOriginalIMEI      STRING(30)                            !
locFinalIMEI         STRING(30)                            !
locEngineerReport    STRING(255)                           !
locAccessories       STRING(255)                           !
locLoanExchangeUnit  STRING(100)                           !
locIDNumber          STRING(20)                            !
locQuantity          LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locUnitCost          REAL                                  !
locLineCost          REAL                                  !
locLoanReplacementValue REAL                               !
locLabour            REAL                                  !
locParts             REAL                                  !
locCarriage          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locTerms             STRING(1000)                          !
locBarCodeJobNumber  STRING(30)                            !
locBarCodeIMEINumber STRING(30)                            !
locDisplayJobNumber  STRING(30)                            !
locDisplayIMEINumber STRING(30)                            !
locCustomerName      STRING(60)                            !
locEndUserTelNo      STRING(60)                            !
locDespatchUser      STRING(60)                            !
locClientName        STRING(60)                            !
qOutFaults           QUEUE,PRE()                           !
Description          STRING(255)                           !
                     END                                   !
DefaultAddress       GROUP,PRE(address)                    !
Name                 STRING(40)                            !Name
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
InvoiceAddress       GROUP,PRE(invoice)                    !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
MobileNumber         STRING(30)                            !
                     END                                   !
DeliveryAddress      GROUP,PRE(delivery)                   !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
                     END                                   !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Courier)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Despatch_Number)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(250,6792,7760,1417),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(250,292,7750,10000),USE(?Header),FONT('Tahoma',8,,FONT:regular)
                         STRING('Job Number:'),AT(5125,375),USE(?STRING1)
                         STRING('Despach Batch No:'),AT(5125,625,1042,167),USE(?STRING1:2)
                         STRING('Date Booked:'),AT(5125,875),USE(?STRING2)
                         STRING('Date Completed:'),AT(5125,1125),USE(?STRING3)
                         STRING(@s8),AT(6250,625),USE(job:Despatch_Number),LEFT(1)
                         STRING(@d6b),AT(6250,875),USE(job:date_booked),LEFT
                         STRING(@D6b),AT(6250,1125),USE(job:Date_Completed),LEFT,TRN
                         STRING(@s30),AT(167,1708,2250),USE(invoice:Name)
                         STRING(@s30),AT(167,1875,2208),USE(invoice:AddressLine1)
                         STRING(@s30),AT(167,2042,2208),USE(invoice:AddressLine2)
                         STRING(@s30),AT(167,2208,2250),USE(invoice:AddressLine3)
                         STRING(@s30),AT(167,2375,2208),USE(invoice:AddressLine4)
                         STRING('Tel:'),AT(2500,1875),USE(?STRING4)
                         STRING('Fax:'),AT(2500,2042,302,167),USE(?STRING4:2)
                         STRING('Mobile:'),AT(2500,2208,417,167),USE(?STRING4:3)
                         STRING(@s30),AT(2958,1875,927),USE(invoice:TelephoneNumber)
                         STRING(@s30),AT(2958,2042,927),USE(invoice:FaxNumber)
                         STRING(@s30),AT(2958,2208,927),USE(invoice:MobileNumber)
                         STRING(@s30),AT(4125,1708,2250),USE(delivery:Name)
                         STRING(@s30),AT(4125,1875,2250),USE(delivery:AddressLine1)
                         STRING(@s30),AT(4125,2042,2250),USE(delivery:AddressLine2)
                         STRING(@s30),AT(4125,2208,2250),USE(delivery:AddressLine3)
                         STRING(@s30),AT(4125,2375,2250),USE(delivery:AddressLine4)
                         STRING('Tel:'),AT(4125,2542,198,167),USE(?STRING4:4)
                         STRING(@s30),AT(4375,2542,2250),USE(delivery:TelephoneNumber)
                         STRING(@s30),AT(6250,375,1375),USE(locFullJobNumber)
                         STRING(@s15),AT(156,3406),USE(job:Account_Number),LEFT,TRN
                         STRING(@s30),AT(1677,3406,1125),USE(job:Order_Number),LEFT,TRN
                         STRING(@s30),AT(3187,3406,1323),USE(job:Authority_Number),LEFT,TRN
                         STRING(@s30),AT(4698,3406,1437),USE(locChargeType)
                         STRING(@s30),AT(6208,3406,1500),USE(locRepairType)
                         STRING(@s30),AT(156,4104,1344),USE(job:Model_Number),LEFT,TRN
                         STRING(@s30),AT(1562,4104,1323),USE(job:Manufacturer),LEFT,TRN
                         STRING(@s30),AT(2958,4104,1042),USE(job:Unit_Type),LEFT,TRN
                         STRING(@s20),AT(4115,4104,1042),USE(locOriginalIMEI),LEFT,TRN
                         STRING(@s30),AT(5219,4104,1125),USE(locFinalIMEI),HIDE
                         STRING(@s20),AT(6479,4104,1187),USE(job:MSN),LEFT,TRN
                         STRING('REPORTED FAULT'),AT(167,4333),USE(?STRING5),FONT(,,,FONT:bold)
                         TEXT,AT(1490,4333,5948,427),USE(jbn:Fault_Description),TRN
                         STRING('ENGINEER REPORT'),AT(167,4792),USE(?STRING6),FONT(,,,FONT:bold)
                         TEXT,AT(1500,4792,5948,677),USE(locEngineerReport),TRN
                         STRING('ACCESSORIES'),AT(167,5500),USE(?STRING7),FONT(,,,FONT:bold)
                         TEXT,AT(1500,5500,5948,302),USE(locAccessories),FONT(,,,FONT:regular+FONT:underline)
                         STRING('EXCHANGE UNIT'),AT(167,5833),USE(?strExchangeUnit),FONT(,,,FONT:bold),HIDE
                         STRING(@s100),AT(1500,5833,5948),USE(locLoanExchangeUnit),HIDE
                         STRING('PARTS USED'),AT(167,6292),USE(?STRING9),FONT(,,,FONT:bold)
                         STRING('Qty'),AT(1500,6250),USE(?STRING10),FONT(,,,FONT:bold)
                         STRING('Part Number'),AT(2000,6250),USE(?STRING11),FONT(,,,FONT:bold)
                         STRING('ID NUMBER'),AT(167,6083),USE(?STRING12),FONT(,,,FONT:bold)
                         STRING(@s20),AT(1500,6042),USE(locIDNumber)
                         STRING('Description'),AT(3750,6250),USE(?STRING13),FONT(,,,FONT:bold)
                         STRING('Unit Type'),AT(5833,6250),USE(?STRING14),FONT(,,,FONT:bold)
                         STRING('Line Cost'),AT(6792,6250),USE(?STRING15),FONT(,,,FONT:bold)
                         LINE,AT(1510,6469,5875,0),USE(?LINE1)
                         LINE,AT(1510,8385,5875,0),USE(?lineTerms)
                         STRING('Customer Signature'),AT(167,8208),USE(?strTerms2),FONT(,,,FONT:bold)
                         STRING('Date'),AT(6000,8208),USE(?strTerms3),FONT(,,,FONT:bold)
                         STRING('Courier:'),AT(167,8917,792,167),USE(?STRING21:2)
                         STRING(@s30),AT(1083,8917,1625),USE(job:Courier),FONT(,,,FONT:bold)
                         STRING('Despatch Date:'),AT(167,8750),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(1083,8750),USE(?ReportDateStamp),FONT(,,,FONT:bold),TRN
                         STRING('Consignment No:'),AT(167,9083),USE(?STRING21)
                         STRING(@s30),AT(1083,9083,1625,167),USE(job:Consignment_Number),FONT(,,,FONT:bold)
                         STRING('Loan Replacement Value:'),AT(167,9333),USE(?strLoanReplacementValue),HIDE
                         STRING(@n10.2),AT(1500,9333),USE(locLoanReplacementValue),FONT(,,,FONT:bold),RIGHT(2),HIDE
                         STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(167,9844),USE(?strLoanTerms), |
  FONT(,7,,FONT:bold+FONT:underline),HIDE
                         STRING('Labour:'),AT(5458,8750),USE(?strLabour)
                         STRING('Parts:'),AT(5458,8917),USE(?strParts)
                         STRING('Carriage:'),AT(5458,9083),USE(?strCarriage)
                         STRING('V.A.T.:'),AT(5458,9250),USE(?strVAT)
                         STRING('Total:'),AT(5458,9500),USE(?strTotal),FONT(,,,FONT:bold)
                         LINE,AT(6344,9469,1000,0),USE(?LINE2)
                         STRING(@n14.2b),AT(6292,8750),USE(locLabour),RIGHT(2)
                         STRING(@n14.2b),AT(6292,8917),USE(locParts),RIGHT(2)
                         STRING(@n14.2b),AT(6292,9083),USE(locCarriage),RIGHT(2)
                         STRING(@n14.2b),AT(6292,9250),USE(locVAT),RIGHT(2)
                         STRING(@n14.2b),AT(6156,9500),USE(locTotal),FONT(,,,FONT:bold),RIGHT(2)
                         STRING(@s20),AT(2573,8750),USE(locBarCodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER
                         STRING(@s20),AT(2573,9167,2594,198),USE(locBarCodeIMEINumber),FONT('C39 High 12pt LJ3',12), |
  CENTER
                         STRING(@s30),AT(2729,8917),USE(locDisplayJobNumber),FONT(,,,FONT:bold),CENTER
                         STRING(@s30),AT(2729,9333),USE(locDisplayIMEINumber),FONT(,,,FONT:bold),CENTER
                         TEXT,AT(156,7917,5885,260),USE(locTermsText),FONT(,7)
                       END
Detail                 DETAIL,AT(0,0,7750,208),USE(?Detail)
                         STRING(@n-14b),AT(667,0),USE(locQuantity),RIGHT(2)
                         STRING(@s30),AT(2000,0),USE(locPartNumber)
                         STRING(@s30),AT(3750,0),USE(locDescription)
                         STRING(@n14.2b),AT(5302,0),USE(locUnitCost),RIGHT(2)
                         STRING(@n14.2b),AT(6229,0),USE(locLineCost),RIGHT(2)
                       END
                       FOOTER,AT(260,10208,7750,833),USE(?Footer)
                         TEXT,AT(125,0,7458,792),USE(stt:Text)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('DESPATCH NOTE'),AT(5708,-42,1917,240),USE(?strTitle),FONT(,16,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('CUSTOMER ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         STRING('Account Number'),AT(156,3156,1042,167),USE(?String91:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1677,3156,1042,167),USE(?String91:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3187,3156,1177,167),USE(?String91:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4698,3156,1042,167),USE(?strChargeableType),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Chargeable Repair Type'),AT(6208,3156,1437),USE(?strRepairType),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Model'),AT(156,3833,1042,167),USE(?String91:7),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1562,3833,1042,167),USE(?String91:8),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(2958,3833,1042,167),USE(?String91:9),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4115,3833,1042,167),USE(?strIMEINumber),FONT(,8,,FONT:bold),TRN
                         STRING('Exch IMEI No'),AT(5219,3833,1042,167),USE(?strExchangeIMEINumber),FONT(,8,,FONT:bold), |
  HIDE,TRN
                         STRING('M.S.N.'),AT(6479,3833,1042,167),USE(?String91:12),FONT(,8,,FONT:bold),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DespatchNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  locJobNumber = p_web.GSV('locJobNumber')
  
  SET(DEFAULT2)
  Access:DEFAULT2.Next()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = locJobNumber
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
  END
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey))
  END
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = wob:HeadAccountNumber
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key)) = Level:Benign
      delivery:Name = tra:Company_Name
      delivery:AddressLine1 = tra:Address_Line1
      delivery:AddressLine2 = tra:Address_Line2
      delivery:AddressLine3 = tra:Address_Line3
      delivery:AddressLine4 = tra:Postcode
      delivery:TelephoneNumber = tra:Telephone_Number
  END
  
  locFullJobNumber = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  
  
  IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
      SETTARGET(Report)
      ?strChargeableType{PROP:Text} = 'Warranty Type'
      ?strRepairType{PROP:Text} = 'Warranty Repair Type'
      SETTARGET()
      locChargeType = job:Warranty_Charge_Type
      locRepairType = job:Repair_Type_Warranty
  ELSE
      locChargeType = job:Charge_Type
      locRepairType = job:Repair_Type
  END
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = job:Account_Number
  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
  END
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = sub:Main_Account_Number
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  IF (tra:Price_Despatch <> 'YES' AND sub:PriceDespatchNotes <> 1)
      SETTARGET(Report)
      ?locUnitCost{PROP:Hide} = 1
      ?locLineCost{PROP:Hide} = 1
      ?locLabour{PROP:Hide} = 1
      ?locParts{PROP:Hide} = 1
      ?locCarriage{PROP:Hide} = 1
      ?locVAT{PROP:Hide} = 1
      ?locTotal{PROP:Hide} = 1
      ?strLabour{PROP:Hide} = 1
      ?strParts{PROP:Hide} = 1
      ?strCarriage{PROP:Hide} = 1
      ?strVAT{PROP:Hide} = 1
      ?strTotal{PROP:Hide} = 1
      SETTARGET()
  END
  
  hideDespatchAddress# = 0
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:HideDespAdd = 1)
          hideDespatchAddress# = 1
      END
      IF (sub:UseCustDespAdd = 'YES')
          invoice:AddressLine1 = job:Address_Line1
          invoice:AddressLine2 = job:Address_Line2
          invoice:AddressLine3 = job:Address_Line3
          invoice:AddressLine4 = job:Postcode
          invoice:Name = job:Company_Name
          invoice:TelephoneNumber = job:Telephone_Number
          invoice:FaxNumber = job:Fax_Number
          
      ELSE
          invoice:AddressLine1 = sub:Address_Line1
          invoice:AddressLine2 = sub:Address_Line2
          invoice:AddressLine3 = sub:Address_Line3
          invoice:AddressLine4 = sub:Postcode
          invoice:TelephoneNumber = sub:Telephone_Number
          invoice:Name = sub:Company_Name
          invoice:FaxNumber = sub:Fax_Number
      END
      
  ELSE
      IF (tra:HideDespAdd = 1)
          hideDespatchAddress# = 1
      END
      IF (tra:UseCustDespAdd = 'YES')
          invoice:AddressLine1 = job:Address_Line1
          invoice:AddressLine2 = job:Address_Line2
          invoice:AddressLine3 = job:Address_Line3
          invoice:AddressLine4 = job:Postcode
          invoice:Name = job:Company_Name
          invoice:TelephoneNumber = job:Telephone_Number
          invoice:FaxNumber = job:Fax_Number        
      ELSE
          invoice:AddressLine1 = tra:Address_Line1
          invoice:AddressLine2 = tra:Address_Line2
          invoice:AddressLine3 = tra:Address_Line3
          invoice:AddressLine4 = tra:Postcode
          invoice:TelephoneNumber = tra:Telephone_Number
          invoice:Name = tra:Company_Name
          invoice:FaxNumber = tra:Fax_Number
      END
  END
  IF (hideDespatchAddress# = 1)
      SETTARGET(Report)
      ?delivery:Name{prop:Hide} = 1
      ?delivery:AddressLine1{PROP:Hide} = 1
      ?delivery:AddressLine2{PROP:Hide} = 1
      ?delivery:AddressLine3{PROP:Hide} = 1
      ?delivery:AddressLine4{PROP:Hide} = 1
      ?delivery:TelephoneNumber{prop:Hide} = 1
      SETTARGET()
  END
  
  locIDNumber = jobe2:IDNumber
  
  IF (job:Title = '')
      locCustomerName = job:Initial
  ELSIF (job:Initial = '')
      locCustomerName = job:Title
  ELSE
      locCustomerName = CLIP(job:Title) & ' ' & CLIP(job:Initial)
  END
  
  IF (locCustomerName = '')
      locCustomerName = job:Surname
  ELSIF(job:Surname = '')
  ELSE
      locCustomerName = CLIP(locCustomerName) & ' ' & CLIP(locCustomerName)
  END
  
  locEndUserTelNo = CLIP(jobe:EndUserTelNo)
  
  IF (locCustomerName <> '')
      locClientName = 'Client: ' & CLIP(locCustomerName) & '    Tel: ' & CLIP(locEndUserTelNo)
  ELSE
      IF (locEndUserTelNo <> '')
          locClientName = 'Tel: ' & CLIP(locEndUserTelNo)
      END
  END
  
  IF NOT (p_web.GSV('BookingSite') <> 'RRC' AND jobe:WebJob = 1)
      delivery:Name = job:Company_Name_Delivery
      delivery:AddressLine1 = job:Address_Line1_Delivery
      delivery:AddressLine2 = job:Address_Line2_Delivery
      delivery:AddressLine3 = job:Address_Line3_Delivery
      delivery:AddressLine4 = job:Postcode_Delivery
      delivery:TelephoneNumber = job:Telephone_Delivery
  END
  
  IF (job:Chargeable_Job = 'YES')
      IF (job:Invoice_Number <> '')
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = job:Invoice_Number
          IF (Access:INVOICE.TryFetch(inv:INvoice_Number_Key))
          END
      
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (inv:ExportedRRCOracle)
                  locLabour = jobe:InvRRCCLabourCost
                  locParts = jobe:InvRRCCPartsCost
                  locCarriage = job:Invoice_Courier_Cost
                  locVAT = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + |
                      (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100) + |
                      (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
              ELSE
                  locLabour = jobe:RRCCLabourCost
                  locParts = jobe:RRCCPartsCost
                  locCarriage = job:Courier_Cost
                  locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                      (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                      (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
              END
          ELSE
              locLabour = job:Invoice_Labour_Cost
              locParts = job:Invoice_Parts_Cost
              locCarriage = job:Invoice_Courier_Cost
              locVAT = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100) + |
                  (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100) + |
                  (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
             
          END
      
      ELSE
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour = jobe:RRCCLabourCost
              locParts = jobe:RRCCPartsCost
              locCarriage = job:Courier_Cost
              locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
          ELSE
              locLabour = job:Labour_Cost
              locParts = job:Parts_Cost
              locCarriage = job:Courier_Cost
              locVAT = (job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
          END
      
      END
      
  END
  
  IF (job:Warranty_Job = 'YES')
      IF (job:Invoice_Number_Warranty <> '')
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = job:Invoice_Number_Warranty
          IF (Access:INVOICE.TryFetch(inv:Invoice_NUmber_Key))
          END
          
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour       = jobe:InvRRCWLabourCost
              locParts        = jobe:InvRRCWPartsCost
              locCarriage = job:WInvoice_Courier_Cost
              locVAT          = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100) + |
                  (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100) + |
                  (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)   
          ELSE
              locLabour       = jobe:InvoiceClaimValue
              locParts        = job_ali:Winvoice_parts_cost
              locCarriage = job_ali:Winvoice_courier_cost
              locVAT          = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour / 100) + |
                  (job:WInvoice_Parts_Cost * inv:Vat_Rate_Parts / 100) +                 |
                  (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          END
          
      ELSE
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour       = jobe:RRCWLabourCost
              locParts        = jobe:RRCWPartsCost
              locCarriage = job_ali:Courier_Cost_Warranty
              locVAT          = (jobe:RRCWLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (jobe:RRCWPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
  
          ELSE
              locLabour       = jobe:ClaimValue
              locParts        = job_ali:Parts_Cost_Warranty
              locCarriage = job_ali:Courier_Cost_Warranty
              locVAT          = (jobe:ClaimValue * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (job:Parts_Cost_Warranty * GetVATRate(job:Account_Number, 'P') / 100) +         |
                  (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
  
          END
          
      END
      
      
  END
  
  locTotal = locLabour + locParts + locCarriage + locVAT
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Despatch_User
  IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
      locDespatchUser = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  END
  
  
  
  
  
  
  ! Accessories & Engineer Report
  locAccessories = ''
  Access:JOBACC.ClearKey(jac:Ref_Number_Key)
  jac:Ref_Number = job:Ref_Number
  SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
  LOOP UNTIL Access:JOBACC.Next()
      IF (jac:Ref_Number <> job:Ref_Number)
          BREAK
      END
      
      IF (locAccessories = '')
          locAccessories = jac:Accessory
      ELSE
          locAccessories = CLIP(locAccessories) & ', ' & CLIP(jac:Accessory)
      END
  END
  
  FREE(qOutfaults)
  Access:JOBOUTFL.ClearKey(joo:LevelKey)
  joo:JobNumber = job:Ref_Number
  SET(joo:LevelKey,joo:LevelKey)
  LOOP UNTIL Access:JOBOUTFL.Next()
      IF (joo:JobNumber <> job:Ref_Number)
          BREAK
      END
      qOutfaults.Description = joo:Description
      GET(qOutfaults,qOutfaults.Description)
      IF (ERROR())
          locEngineerReport = CLIP(locEngineerReport) & ' ' & CLIP(joo:Description)
          qOutFaults.Description = joo:Description
          Add(qOutFaults)
      END
  END
  FREE(qOutfaults)
  
  
      
  ! Exchange Note
  
  exchangeNote# = 0
  IF (job:Exchange_Unit_Number <> '')
      exchangeNote# = 1
  END
  
  ! Check Parts For "Exchange Part"
  Access:PARTS.ClearKey(par:Part_Number_Key)
  par:Ref_Number = job:Ref_Number
  SET(par:Part_Number_Key,par:Part_Number_Key)
  LOOP UNTIL Access:PARTS.Next()
      IF (par:Ref_Number <> job:Ref_Number)
          BREAK
      END
      
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          IF (sto:ExchangeUnit = 'YES')
              exchangeNote# = 1
              BREAK
          END
      END
  END
  
  IF (exchangeNote# = 0)
      ! Check Warranty Parts for "Exchange Part"
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = job:Ref_Number
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP UNTIL Access:WARPARTS.next()
          IF (wpr:Ref_Number <> job:Ref_Number)
              BREAK
          END
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = par:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
              IF (sto:ExchangeUnit = 'YES')
                  exchangeNote# = 1
                  BREAK
              END
          END
      END
  END
  
  IF (exchangeNote# = 1)
      ! Do not put exchange unit repair on the
      ! Despatch Note Heading if it's a failed assessment
      IF (jobe:WebJob = 1 AND p_web.GSV('BookingSite') <> 'RRC' AND jobe:Engineer48HourOption = 1)
          IF (PassExchangeAssessment(job:Ref_Number) = 0)
              exchangeNote# = 0
          END
      END
      
  END
  
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  IF (job:Loan_Unit_Number <> '')
      SETTARGET(Report)
      ?strExchangeUnit{PROP:Text} = 'LOAN UNIT'
      IF (p_web.GSV('BookingSite') = 'RRC' AND jobe:DespatchType = 'LOA') OR |
                      (p_web.GSV('BookingSite') <> 'RRC' AND job:Despatch_Type = 'LOA')
          locIDNumber = jobe2:LoanIDNumber
          ?strTitle{PROP:Text} = 'LOAN DESPATCH NOTE'
      END
      ?strExchangeUnit{PROP:Hide} = 0
      ?locLoanExchangeUnit{PROP:Hide} = 0
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = job:Loan_Unit_Number
      IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
          locLoanExchangeUnit = '(' & loa:Ref_Number & ') ' & CLIP(loa:Manufacturer) & | 
                          ' ' & CLIP(loa:Model_Number) & ' - ' & CLIP(loa:ESN)
      END
      SETTARGET()
      ! #12084 New standard texts (Bryan: 17/05/2011)
      Access:STANTEXT.Clearkey(stt:Description_Key)
      stt:Description = 'LOAN DESPATCH NOTE'
      IF (Access:STANTEXT.TryFetch(stt:Description_Key))
      END
  END
  
  locOriginalIMEI = job:ESN
  
  ! Send To THird Party?
  IF (job:Third_Party_Site <> '')
      IMEIError# = 0
      Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
      jot:RefNumber = job:Ref_Number
      SET(jot:RefNumberKey,jot:RefNumberKey)
      IF (Access:JOBTHIRD.Next())
          IMEIError# = 1
      ELSE
          IF (jot:RefNumber <> job:Ref_Number)
              IMEIError# = 1
          ELSE
              IF (jot:OriginalIMEI <> job:ESN)
                  IMEIError# = 0
              ELSE
                  IMEIError# = 1
              END
          END
      END
  ELSE
      IMEIError# = 1
  END
  
  IF (IMEIError# = 1)
      locOriginalIMEI = job:ESN
  ELSE
      SETTARGET(Report)
      locOriginalIMEI = jot:OriginalIMEI
      locFinalIMEI = job:ESN
      ?strIMEINumber{PROP:Text} = 'Orig I.M.E.I. Number'
      ?strExchangeIMEINumber{PROP:Hide} = 0
      ?locFinalIMEI{PROP:Hide} = 0
      SETTARGET()
  END
  
      
  IF (exchangeNote# = 1)
      SETTARGET(Report)
      ?strTitle{prop:Text} = 'EXCHANGE DESPATCH NOTE'
      ?locLoanExchangeUnit{PROP:Hide} = 0
      ?strExchangeUnit{PROP:Hide} = 0
      ?locPartNumber{PROP:Hide} = 1
      ?locDescription{PROP:Hide} = 1
      ?locQuantity{PROP:Hide} = 1
      ?locUnitCost{prop:Hide} = 1
      ?locLineCost{PROP:Hide} = 1
      
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          locLoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) & |
                          ' - ' & CLip(xch:esn)
          locOriginalIMEI = job:ESN
          locFinalIMEI = xch:ESN
          ?locFinalIMEI{PROP:Hide} = 0
          ?strExchangeIMEINumber{PROP:Hide} = 0
      END
      SETTARGET()
  END
  
  
  
          
          
  
  ! Terms
  locTerms = ''
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign)
      locTerms = stt:Text
  END
  
  IF (job:Loan_Unit_Number <> '' AND job:Exchange_Unit_Number = 0)
      SETTARGET(Report)
      ?locLoanReplacementValue{PROP:Hide} = 0
      ?strLoanReplacementValue{PROP:Hide} = 0
      locLoanReplacementValue = jobe:LoanReplacementValue
      ?strLoanTerms{PROP:Hide} = 0
      !?strTerms1{PROP:Hide} = 0
      ?strTerms2{PROP:Hide} = 0
      ?strTerms3{PROP:Hide} = 0
      ?lineTerms{PROP:Hide} = 0
      SETTARGET()
      locTerms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
      locTerms = Clip(locTerms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
      locTerms = Clip(locTerms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
      locTerms = Clip(locTerms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
      locTerms = Clip(locTerms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
  
  END
  locTermsText = p_web.GSV('Default:TermsText')
  ! Barcode Bit
  locBarCodeJobNumber = '*' & job:Ref_Number & '*'
  locBarCodeIMEINumber = '*' & job:ESN & '*'
  locDisplayIMEINumber = 'I.M.E.I. No: ' & job:ESN
  locDisplayJobNumber = 'Job No: ' & job:Ref_Number
  Do DefineListboxStyle
  INIMgr.Fetch('DespatchNote',ProgressWindow)              ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('DespatchNote',ProgressWindow)           ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  countParts# = 0
  IF (job:Warranty_Job = 'YES')
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = job:Ref_Number
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP UNTIL Access:WARPARTS.Next()
          IF (wpr:Ref_Number <> job:Ref_Number)
              BREAK
          END
          locPartNumber = wpr:Part_Number
          locDescription = wpr:Description
          locQuantity = wpr:Quantity
          locUnitCost = wpr:Purchase_Cost
          locLineCost = wpr:Quantity * wpr:Purchase_Cost
          Print(rpt:Detail)
          countParts# += 1
      END
  END
  
  IF (countParts# = 0)
      locPartNumber = ''
      Print(rpt:Detail)
  END
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','DespatchNote','DespatchNote','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

PassExchangeAssessment PROCEDURE  (Long fJobNumber)        ! Declare Procedure
rtnValue             BYTE                                  !
PARTS::State  USHORT
WARPARTS::State  USHORT
MANFAUPA::State  USHORT
MANFAULO::State  USHORT
MANFAULT::State  USHORT
JOBOUTFL::State  USHORT
JOBS::State  USHORT
FilesOpened     BYTE(0)
tmp:FaultCode   STRING(255)
  CODE
    DO openFiles
    DO SaveFiles
    !Loop through outfaults
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = fJobNumber
    IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))
        
    END
    
    rtnValue = 0
    
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
            Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
                        Then Break.  ! End If
    !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                    Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                                Or mfo:Field_Number <> maf:Field_Number      |
                                Or mfo:Field        <> joo:FaultCode      |
                                Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
    !Make sure the descriptions match in case of duplicates
                    If mfo:ReturnToRRC
                        rtnValue = 0
                        Do ExitProc
                    End !If mfo:ReturnToRRC
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
    
        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
    End !Loop
    
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    map:Manufacturer = job:Manufacturer
    
    map:Manufacturer = job:Manufacturer
    
                        
    
    !Is an outfault records on parts for this manufacturer
        Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Found
    !Loop through the parts as see if any of the faults codes are excluded
            If job:Warranty_Job = 'YES'
    
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                        Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
    
    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = wpr:Fault_Code1
                        Of 2
                            tmp:FaultCode        = wpr:Fault_Code2
                        Of 3
                            tmp:FaultCode        = wpr:Fault_Code3
                        Of 4
                            tmp:FaultCode        = wpr:Fault_Code4
                        Of 5
                            tmp:FaultCode        = wpr:Fault_Code5
                        Of 6
                            tmp:FaultCode        = wpr:Fault_Code6
                        Of 7
                            tmp:FaultCode        = wpr:Fault_Code7
                        Of 8
                            tmp:FaultCode        = wpr:Fault_Code8
                        Of 9
                            tmp:FaultCode        = wpr:Fault_Code9
                        Of 10
                            tmp:FaultCode        = wpr:Fault_Code10
                        Of 11
                            tmp:FaultCode        = wpr:Fault_Code11
                        Of 12
                            tmp:FaultCode        = wpr:Fault_Code12
                        End !Case map:Field_Number
    
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop
    
                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
                End !Loop
            End !If job:Warranty_Job = 'YES'
    
            If job:Chargeable_Job = 'YES'
    !Loop through the parts as see if any of the faults codes are excluded
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                        Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
    
    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = par:Fault_Code1
                        Of 2
                            tmp:FaultCode        = par:Fault_Code2
                        Of 3
                            tmp:FaultCode        = par:Fault_Code3
                        Of 4
                            tmp:FaultCode        = par:Fault_Code4
                        Of 5
                            tmp:FaultCode        = par:Fault_Code5
                        Of 6
                            tmp:FaultCode        = par:Fault_Code6
                        Of 7
                            tmp:FaultCode        = par:Fault_Code7
                        Of 8
                            tmp:FaultCode        = par:Fault_Code8
                        Of 9
                            tmp:FaultCode        = par:Fault_Code9
                        Of 10
                            tmp:FaultCode        = par:Fault_Code10
                        Of 11
                            tmp:FaultCode        = par:Fault_Code11
                        Of 12
                            tmp:FaultCode        = par:Fault_Code12
                        End !Case map:Field_Number
    
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop
    
                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
                End !Loop
            End !If job:Chargeable_Job = 'YES'
        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    
        rtnValue = 1
        Do ExitProc
    

ExitProc    ROUTINE
    DO RestoreFiles
    DO CloseFiles
    RETURN rtnValue
SaveFiles  ROUTINE
  PARTS::State = Access:PARTS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  WARPARTS::State = Access:WARPARTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAUPA::State = Access:MANFAUPA.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULO::State = Access:MANFAULO.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULT::State = Access:MANFAULT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBOUTFL::State = Access:JOBOUTFL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS::State = Access:JOBS.SaveFile()                     ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF PARTS::State <> 0
    Access:PARTS.RestoreFile(PARTS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF WARPARTS::State <> 0
    Access:WARPARTS.RestoreFile(WARPARTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAUPA::State <> 0
    Access:MANFAUPA.RestoreFile(MANFAUPA::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULO::State <> 0
    Access:MANFAULO.RestoreFile(MANFAULO::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULT::State <> 0
    Access:MANFAULT.RestoreFile(MANFAULT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBOUTFL::State <> 0
    Access:JOBOUTFL.RestoreFile(JOBOUTFL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS::State <> 0
    Access:JOBS.RestoreFile(JOBS::State)                   ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:MANFAUPA.Close
     Access:MANFAULO.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:JOBS.Close
     FilesOpened = False
  END
CheckLength          PROCEDURE  (f_type,f_ModelNumber,f_number) ! Declare Procedure
tmp:LengthFrom       LONG                                  !Length From
tmp:LengthTo         LONG                                  !Length To
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MODELNUM::State  USHORT
FilesOpened     BYTE(0)
  CODE
    If f_Type = 'MOBILE'
        tmp:LengthFrom = GETINI('COMPULSORY','MobileLengthFrom',,Clip(Path()) & '\SB2KDEF.INI')
        tmp:LengthTo   = GETINI('COMPULSORY','MobileLengthTo',,Clip(Path()) & '\SB2KDEF.INI')
        If Len(Clip(f_Number)) < tmp:LengthFrom Or |
            Len(Clip(f_Number)) > tmp:LengthTo

            Return Level:Fatal
        End ! Len(Clip(f_Number)) > tmp:LengthTo

    Else ! If f_Type = 'MOBILE'
        !Return The Correct Length Of A Model Number
        Return# = 0
        Do OpenFiles
        Do SaveFiles
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = f_ModelNumber
        if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
            If f_number <> 'N/A'
                Case f_type
                    Of 'IMEI'
                        If Len(Clip(f_number)) < mod:esn_length_from Or Len(Clip(f_number)) > mod:esn_length_to
                            Return# = 1
                        End!If Clip(Len(f_number)) < mod:esn_length_from Or Clip(Len(f_number)) > mod:esn_length_to


                    Of 'MSN'
                        If Len(Clip(f_number)) < mod:msn_length_from Or len(clip(f_number)) > mod:msn_length_to
                            Return# = 1
                        End!If Clip(Len(f_number)) < mod:esn_length_from
                End!Case f_type
            End!If f_number <> 'N/A'

        End!if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
        Do RestoreFiles
        Do CloseFiles
        Return Return#
    End ! If f_Type = 'MOBILE'

    Return Level:Benign
SaveFiles  ROUTINE
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MODELNUM.Close
     FilesOpened = False
  END
PassMobileNumberFormat PROCEDURE  (String f:MobileNumber)  ! Declare Procedure
tmp:Format           STRING(30),AUTO                       !Format
tmp:StartQuotes      BYTE(0)                               !Start Quotes
  CODE
   If f:MobileNumber = ''
        Return True
    End ! If Clip(f:MobileNumber) = ''

    Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
    Of 0 ! Mobile Number Length
        If CheckLength('MOBILE','',f:MobileNumber)
            Return False
        End ! If CheckLength('MOBILE','',f:MobileNumber)
        Return True
    Of 1 ! Mobile Number Format
    ! End (DBH 22/06/2006) #7597

        tmp:Format  = GETINI('MOBILENUMBER','Format',,Clip(Path()) & '\SB2KDEF.INI')

        If tmp:Format = ''
            Return True
        End ! If Clip(tmp:Format) = ''

        ! Inserting (DBH 22/06/2006) #7597 - Check the length of the mobile before starting the format check
        Len# = 0
        Loop x# = 1 To Len(tmp:Format)
            If Sub(tmp:Format,x#,1) <> '*' And Sub(tmp:Format,x#,1) <> ''
                Len# += 1
            End ! If Sub(tmp:Format,x#,1) <> '*'
        End ! Loop x# = 1 To Len(tmp:Format)
        MobLen# = 0
        Loop x# = 1 To Len(f:MobileNumber)
            If Sub(f:MobileNumber,x#,1) <> ''
                MobLen# += 1
            End ! If Sub(f:MobileNumber,x#,1) <> ''
        End ! Loop x# = 1 To Len(f:MobileNumber)

        If MobLen# <> Len#
            Return False
        End ! If Len(f:MobileNumber) <> Len#
        ! End (DBH 22/06/2006) #7597


        Error# = 0
        y# = 1
        Loop x# = 1 To Len(tmp:Format)
            If tmp:StartQuotes
                If Sub(tmp:Format,x#,1) = '*'
                    tmp:StartQuotes = 0
                    Cycle
                End ! If Sub(tmp:Format,x#,1) = '"'
                If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    Error# = 1
                    Break
                Else ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    y# += 1
                    Cycle
                End ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
            End ! If tmp:StartQuotes
            If Sub(tmp:Format,x#,1) = '0'
                If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
                    Error# = 1
                    Break
                End ! If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
            End ! If Sub(tmp:Format,x#,1) = '0'

            If Sub(tmp:Format,x#,1) = '*'
                tmp:StartQuotes = 1
                Cycle
            End ! If Sub(tmp:Format,x#,1) = '"'

            y# += 1
        End ! Loop x# = 1 To Len(Clip(tmp:Format))

        If Error# = 0
            Return True
        Else ! If Error# = 0
            Return False
        End ! If Error# = 0
    Else
        Return True
    End ! Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
AmendAddress         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
VariablesGroup       GROUP,PRE(tmp)                        !
CompanyName          STRING(30)                            !TempCompanyName
AddressLine1         STRING(30)                            !TempAddressLine1
AddressLine2         STRING(30)                            !TempAddressLine2
Suburb               STRING(30)                            !TempSuburb
Postcode             STRING(30)                            !TempPostcode
TelephoneNumber      STRING(30)                            !TempTelephoneNumber
FaxNumber            STRING(30)                            !TempFaxNumber
EmailAddress         STRING(255)                           !Email Address
EndUserTelephoneNumber STRING(30)                          !End User Tel No
IDNumber             STRING(30)                            !ID Number
SMSNotification      BYTE(0)                               !SMS Notification
NotificationMobileNumber STRING(30)                        !NotificationMobileNumber
EmailNotification    BYTE(0)                               !EmailNotification
NotificationEmailAddress STRING(255)                       !Notification Email Address
CompanyNameDelivery  STRING(30)                            !tmp:CompanyNameDelivery
AddressLine1Delivery STRING(30)                            !tmp:AddressLine1Delivery
AddressLine2Delivery STRING(30)                            !AddressLine2Delivery
SuburbDelivery       STRING(30)                            !SubrubDelivery
PostcodeDelivery     STRING(30)                            !PostcodeDelivery
TelephoneNumberDelivery STRING(30)                         !TelephoneNumberDelivery
CompanyNameCollection STRING(30)                           !CompanyNameCollection
AddressLine1Collection STRING(30)                          !AddressLine1Collection
AddressLine2Collection STRING(30)                          !AddressLine2Collection
SuburbCollection     STRING(30)                            !SuburbCollection
PostcodeCollection   STRING(30)                            !PostcodeCollection
TelephoneNumberCollection STRING(30)                       !TelephoneNumberCollection
HubCustomer          STRING(30)                            !Hub
HubCollection        STRING(30)                            !Hub
HubDelivery          STRING(30)                            !Hub
                     END                                   !
ReplicateCustomerToDelivery BYTE(0)                        !ReplicateCustomerToDelivery
ReplicateCustomerToCollection BYTE(0)                      !ReplicateCustomerToCollection
ReplicateCollectionToDelivery BYTE(0)                      !ReplicateCollectionToDelivery
ReplicateDeliveryToCollection BYTE(0)                      !ReplicateDeliveryToCollection
FilesOpened     Long
SUBTRACC::State  USHORT
COURIER::State  USHORT
JOBNOTES::State  USHORT
SUBACCAD::State  USHORT
SUBURB::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBSE2::State  USHORT
TRANTYPE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AmendAddress')
  loc:formname = 'AmendAddress_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AmendAddress','')
    p_web._DivHeader('AmendAddress',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AmendAddress',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(SUBACCAD)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRANTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(SUBACCAD)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRANTYPE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('AmendAddress_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCustomer',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubCustomer')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3_Delivery'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Delivery',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubDelivery',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
  Of 'job:Address_Line3_Collection'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Collection',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCollection',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubDelivery')
  End
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Sub_Sub_Account'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBACCAD)
        p_web.setsessionvalue('job:Postcode_Delivery',sua:Postcode)
        p_web.setsessionvalue('job:Address_Line1_Delivery',sua:addressline1)
        p_web.setsessionvalue('job:Address_Line2_Delivery',sua:addressline2)
        p_web.setsessionvalue('job:Address_Line3_Delivery',sua:addressline3)
        p_web.setsessionvalue('job:Telephone_Delivery',sua:TelephoneNumber)
        p_web.setsessionvalue('job:Company_Name_Delivery',sua:CompanyName)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  p_web.SetSessionValue('job:Postcode',job:Postcode)
  p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Company_Name')
    job:Company_Name = p_web.GetValue('job:Company_Name')
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  End
  if p_web.IfExistsValue('job:Address_Line1')
    job:Address_Line1 = p_web.GetValue('job:Address_Line1')
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  End
  if p_web.IfExistsValue('job:Address_Line2')
    job:Address_Line2 = p_web.GetValue('job:Address_Line2')
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  End
  if p_web.IfExistsValue('job:Address_Line3')
    job:Address_Line3 = p_web.GetValue('job:Address_Line3')
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  End
  if p_web.IfExistsValue('jobe2:HubCustomer')
    jobe2:HubCustomer = p_web.GetValue('jobe2:HubCustomer')
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  End
  if p_web.IfExistsValue('job:Postcode')
    job:Postcode = p_web.GetValue('job:Postcode')
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  End
  if p_web.IfExistsValue('job:Telephone_Number')
    job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  End
  if p_web.IfExistsValue('job:Fax_Number')
    job:Fax_Number = p_web.GetValue('job:Fax_Number')
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress')
    jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  End
  if p_web.IfExistsValue('jobe:VatNumber')
    jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  End
  if p_web.IfExistsValue('jobe2:IDNumber')
    jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  End
  if p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber')
    jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  End
  if p_web.IfExistsValue('jobe2:EmailNotification')
    jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress')
    jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery')
    job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  End
  if p_web.IfExistsValue('job:Company_Name_Collection')
    job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery')
    job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection')
    job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery')
    job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection')
    job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery')
    job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection')
    job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  End
  if p_web.IfExistsValue('jobe2:HubDelivery')
    jobe2:HubDelivery = p_web.GetValue('jobe2:HubDelivery')
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  End
  if p_web.IfExistsValue('jobe2:HubCollection')
    jobe2:HubCollection = p_web.GetValue('jobe2:HubCollection')
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  End
  if p_web.IfExistsValue('job:Postcode_Delivery')
    job:Postcode_Delivery = p_web.GetValue('job:Postcode_Delivery')
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  End
  if p_web.IfExistsValue('job:Postcode_Collection')
    job:Postcode_Collection = p_web.GetValue('job:Postcode_Collection')
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  End
  if p_web.IfExistsValue('job:Telephone_Delivery')
    job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  End
  if p_web.IfExistsValue('job:Telephone_Collection')
    job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  End
  if p_web.IfExistsValue('jbn:Delivery_Text')
    jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  End
  if p_web.IfExistsValue('jbn:Collection_Text')
    jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  End
  if p_web.IfExistsValue('jbn:DelContactName')
    jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  End
  if p_web.IfExistsValue('jbn:ColContatName')
    jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  End
  if p_web.IfExistsValue('jbn:DelDepartment')
    jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  End
  if p_web.IfExistsValue('jbn:ColDepartment')
    jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account')
    jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('AmendAddress_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
      p_web.SSV('Hide:DeliveryAddress',1)
      p_web.SSV('Hide:CollectionAddress',1)
      Access:TRANTYPE.Clearkey(trt:transit_Type_Key)
      trt:transit_Type    = p_web.GSV('job:transit_Type')
      if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Found
          if (trt:Delivery_Address = 'YES')
              p_web.SSV('Hide:DeliveryAddress',0)
          end ! if (trt:DeliveryAddress = 'YES')
  
          if (trt:Collection_Address = 'YES')
              p_web.SSV('Hide:CollectionAddress',0)
          end ! if (trt:Collection_Address = 'YES')
  
      else ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Error
      end ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
  
      p_web.storeValue('FromURL')
  
  
      p_web.SSV('Comment:TelephoneNumber','')
      p_web.SSV('Comment:FaxNumber','')
      p_web.SSV('Comment:SMSAlertNumber','')
      p_web.SSV('Comment:TelephoneNumberDelivery','')
      p_web.SSV('Comment:TelephoneNumberCollection','')
  
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('job:Account_Number')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          IF (sub:UseAlternativeAdd)
              p_web.SSV('Hide:SubSubAccount',0)
          ELSE
              p_web.SSV('Hide:SubSubAccount',1)
          END
          ! Start - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
          If sub:Vat_Number = '' AND sub:OverrideHeadVATNo = True
              p_web.SSV('Hide:VatNumber',0)
          Else ! sub:Vat_Number = '' AND tra:Vat_Number = ''
              p_web.SSV('Hide:VatNumber',1)
          End ! sub:Vat_Number = '' AND tra:Vat_Number = ''
          ! End   - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
  
      END
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('FromURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AmendAddress_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AmendAddress_ChainTo')
    loc:formaction = p_web.GetSessionValue('AmendAddress_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="AmendAddress" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="AmendAddress" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="AmendAddress" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Amend Addresses') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Amend Addresses',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_AmendAddress">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_AmendAddress" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_AmendAddress')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('End User Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Collection / Delivery Details') & ''''
        If p_web.GSV('Hide:SubSubAccount') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Other Details') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_AmendAddress')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_AmendAddress'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Number')
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:CollectionAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Delivery')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Company_Name')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('Hide:SubSubAccount') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_AmendAddress')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('Hide:SubSubAccount') <> 1
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('End User Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::temp:Title
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::temp:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::temp:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubCustomer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubCustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubCustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Fax_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserEmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserTelNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:VatNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:VatNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:VatNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:VatNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:IDNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:IDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:IDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:SMSNotification
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:SMSNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:SMSNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:SMSAlertNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:SMSAlertNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:SMSAlertNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:EmailNotification
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:EmailNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:EmailNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:EmailAlertAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:EmailAlertAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:EmailAlertAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:CourierWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Collection / Delivery Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::DeliveryAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::DeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::DeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::CollectionAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::CollectionAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::CollectionAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubDelivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubDelivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubDelivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubCollection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubCollection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubCollection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:CopyEndUserAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:CopyEndUserAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:CopyEndUserAddress1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:CopyEndUserAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:CopyEndUserAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Delivery_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Collection_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Collection_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Collection_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:DelContactName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:DelContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:DelContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:ColContatName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:ColContatName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:ColContatName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:DelDepartment
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:DelDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:DelDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:ColDepartment
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:ColDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:ColDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('Hide:SubSubAccount') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Other Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Sub_Sub_Account
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Sub_Sub_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:Sub_Sub_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::temp:Title  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::temp:Title  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('temp:Title',p_web.GetValue('NewValue'))
    do Value::temp:Title
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::temp:Title  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('End User Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::temp:Title  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Company_Name  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Company_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name',p_web.GetValue('NewValue'))
    job:Company_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name
    do Value::job:Company_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name = p_web.GetValue('Value')
  End
    job:Company_Name = Upper(job:Company_Name)
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  do Value::job:Company_Name
  do SendAlert

Value::job:Company_Name  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Company_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name'',''amendaddress_job:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name',p_web.GetSessionValueFormat('job:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value')

Comment::job:Company_Name  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1',p_web.GetValue('NewValue'))
    job:Address_Line1 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1
    do Value::job:Address_Line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1 = p_web.GetValue('Value')
  End
    job:Address_Line1 = Upper(job:Address_Line1)
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  do Value::job:Address_Line1
  do SendAlert

Value::job:Address_Line1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Address_Line1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1'',''amendaddress_job:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1',p_web.GetSessionValueFormat('job:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value')

Comment::job:Address_Line1  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line2  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2',p_web.GetValue('NewValue'))
    job:Address_Line2 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2
    do Value::job:Address_Line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2 = p_web.GetValue('Value')
  End
    job:Address_Line2 = Upper(job:Address_Line2)
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  do Value::job:Address_Line2
  do SendAlert

Value::job:Address_Line2  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Address_Line2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2'',''amendaddress_job:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2',p_web.GetSessionValueFormat('job:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value')

Comment::job:Address_Line2  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line3  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3',p_web.GetValue('NewValue'))
    job:Address_Line3 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3
    do Value::job:Address_Line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3 = p_web.GetValue('Value')
  End
    job:Address_Line3 = Upper(job:Address_Line3)
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode',sur:Postcode)
          p_web.SSV('jobe2:HubCustomer',sur:Hub)
      END
  
  p_Web.SetValue('lookupfield','job:Address_Line3')
  do AfterLookup
  do Value::job:Postcode
  do Value::jobe2:HubCustomer
  do Value::job:Address_Line3
  do SendAlert
  do Comment::job:Address_Line3
  do Value::jobe2:HubCustomer  !1
  do Value::job:Postcode  !1

Value::job:Address_Line3  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Address_Line3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3'',''amendaddress_job:address_line3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3',p_web.GetSessionValue('job:Address_Line3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value')

Comment::job:Address_Line3  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_comment')

Prompt::jobe2:HubCustomer  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt')

Validate::jobe2:HubCustomer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetValue('NewValue'))
    jobe2:HubCustomer = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubCustomer
    do Value::jobe2:HubCustomer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubCustomer',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubCustomer = p_web.GetValue('Value')
  End
    jobe2:HubCustomer = Upper(jobe2:HubCustomer)
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  do Value::jobe2:HubCustomer
  do SendAlert

Value::jobe2:HubCustomer  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:HubCustomer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubCustomer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCustomer'',''amendaddress_jobe2:hubcustomer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCustomer',p_web.GetSessionValueFormat('jobe2:HubCustomer'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value')

Comment::jobe2:HubCustomer  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_comment')

Prompt::job:Postcode  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt')

Validate::job:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode',p_web.GetValue('NewValue'))
    job:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode
    do Value::job:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode = p_web.GetValue('Value')
  End
  do Value::job:Postcode
  do SendAlert

Value::job:Postcode  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode'',''amendaddress_job:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode',p_web.GetSessionValueFormat('job:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value')

Comment::job:Postcode  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_comment')

Prompt::job:Telephone_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Telephone_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Number',p_web.GetValue('NewValue'))
    job:Telephone_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Number
    do Value::job:Telephone_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Number = p_web.GetValue('Value')
  End
    job:Telephone_Number = Upper(job:Telephone_Number)
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Number')) = 0
          p_web.SSV('job:Telephone_Number','')
          p_web.SSV('Comment:TelephoneNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Number
  do SendAlert
  do Comment::job:Telephone_Number

Value::job:Telephone_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Telephone_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Number'',''amendaddress_job:telephone_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Number',p_web.GetSessionValueFormat('job:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value')

Comment::job:Telephone_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_comment')

Prompt::job:Fax_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Fax_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Fax_Number',p_web.GetValue('NewValue'))
    job:Fax_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Fax_Number
    do Value::job:Fax_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Fax_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Fax_Number = p_web.GetValue('Value')
  End
    job:Fax_Number = Upper(job:Fax_Number)
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
      If PassMobileNumberFormat(p_web.GSV('job:Fax_Number')) = 0
          p_web.SSV('job:Fax_Number','')
          p_web.SSV('Comment:FaxNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:FaxNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Fax_Number
  do SendAlert
  do Comment::job:Fax_Number

Value::job:Fax_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Fax_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Fax_Number'',''amendaddress_job:fax_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Fax_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Fax_Number',p_web.GetSessionValueFormat('job:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value')

Comment::job:Fax_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:FaxNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_comment')

Prompt::jobe:EndUserEmailAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserEmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetValue('NewValue'))
    jobe:EndUserEmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserEmailAddress
    do Value::jobe:EndUserEmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe:EndUserEmailAddress = p_web.GetValue('Value')
  End
  do Value::jobe:EndUserEmailAddress
  do SendAlert

Value::jobe:EndUserEmailAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe:EndUserEmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserEmailAddress'',''amendaddress_jobe:enduseremailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserEmailAddress',p_web.GetSessionValueFormat('jobe:EndUserEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value')

Comment::jobe:EndUserEmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:EndUserTelNo  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End User Tel No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserTelNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetValue('NewValue'))
    jobe:EndUserTelNo = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserTelNo
    do Value::jobe:EndUserTelNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:EndUserTelNo = p_web.GetValue('Value')
  End
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  do Value::jobe:EndUserTelNo
  do SendAlert

Value::jobe:EndUserTelNo  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:EndUserTelNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''amendaddress_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value')

Comment::jobe:EndUserTelNo  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:VatNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Vat Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:VatNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:VatNumber',p_web.GetValue('NewValue'))
    jobe:VatNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:VatNumber
    do Value::jobe:VatNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:VatNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:VatNumber = p_web.GetValue('Value')
  End
    jobe:VatNumber = Upper(jobe:VatNumber)
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  do Value::jobe:VatNumber
  do SendAlert

Value::jobe:VatNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:VatNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:VatNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:VatNumber'',''amendaddress_jobe:vatnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:VatNumber',p_web.GetSessionValueFormat('jobe:VatNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Vat Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value')

Comment::jobe:VatNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:IDNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:IDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:IDNumber',p_web.GetValue('NewValue'))
    jobe2:IDNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:IDNumber
    do Value::jobe2:IDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:IDNumber',p_web.dFormat(p_web.GetValue('Value'),'@s13'))
    jobe2:IDNumber = p_web.GetValue('Value')
  End
    jobe2:IDNumber = Upper(jobe2:IDNumber)
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  do Value::jobe2:IDNumber
  do SendAlert

Value::jobe2:IDNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:IDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:IDNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:IDNumber'',''amendaddress_jobe2:idnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:IDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:IDNumber',p_web.GetSessionValueFormat('jobe2:IDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'I.D. Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value')

Comment::jobe2:IDNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:SMSNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('SMS Notification')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:SMSNotification  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetValue('NewValue'))
    jobe2:SMSNotification = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:SMSNotification
    do Value::jobe2:SMSNotification
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetValue('Value'))
    jobe2:SMSNotification = p_web.GetValue('Value')
  End
  do Value::jobe2:SMSNotification
  do SendAlert
  do Prompt::jobe2:SMSAlertNumber
  do Value::jobe2:SMSAlertNumber  !1

Value::jobe2:SMSNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:SMSNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:SMSNotification'',''amendaddress_jobe2:smsnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:SMSNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:SMSNotification',clip(1),,loc:readonly,,,loc:javascript,,'SMS Notification') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value')

Comment::jobe2:SMSNotification  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:SMSAlertNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Mobile No')
  If p_web.GSV('jobe2:SMSNotification') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt')

Validate::jobe2:SMSAlertNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetValue('NewValue'))
    jobe2:SMSAlertNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:SMSAlertNumber
    do Value::jobe2:SMSAlertNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:SMSAlertNumber = p_web.GetValue('Value')
  End
    jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
      If PassMobileNumberFormat(p_web.GSV('jobe2:SMSAlertNumber')) = 0
          p_web.SSV('jobe2:SMSAlertNumber','')
          p_web.SSV('Comment:SMSAlertNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:SMSAlertNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::jobe2:SMSAlertNumber
  do SendAlert
  do Comment::jobe2:SMSAlertNumber

Value::jobe2:SMSAlertNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe2:SMSNotification') = 0)
  ! --- STRING --- jobe2:SMSAlertNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:SMSAlertNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:SMSAlertNumber'',''amendaddress_jobe2:smsalertnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSAlertNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:SMSAlertNumber',p_web.GetSessionValueFormat('jobe2:SMSAlertNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'SMS Alert Mobile Number') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value')

Comment::jobe2:SMSAlertNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:SMSAlertNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_comment',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('jobe2:SMSNotification') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_comment')

Prompt::jobe2:EmailNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Notification')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:EmailNotification  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetValue('NewValue'))
    jobe2:EmailNotification = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:EmailNotification
    do Value::jobe2:EmailNotification
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetValue('Value'))
    jobe2:EmailNotification = p_web.GetValue('Value')
  End
  do Value::jobe2:EmailNotification
  do SendAlert
  do Prompt::jobe2:EmailAlertAddress
  do Value::jobe2:EmailAlertAddress  !1

Value::jobe2:EmailNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:EmailNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:EmailNotification'',''amendaddress_jobe2:emailnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:EmailNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:EmailNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:EmailNotification',clip(1),,loc:readonly,,,loc:javascript,,'Email Notification') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value')

Comment::jobe2:EmailNotification  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:EmailAlertAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Email Address')
  If p_web.GSV('jobe2:EmailNotification') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt')

Validate::jobe2:EmailAlertAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetValue('NewValue'))
    jobe2:EmailAlertAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:EmailAlertAddress
    do Value::jobe2:EmailAlertAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe2:EmailAlertAddress = p_web.GetValue('Value')
  End
  do Value::jobe2:EmailAlertAddress
  do SendAlert

Value::jobe2:EmailAlertAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe2:EmailNotification') = 0)
  ! --- STRING --- jobe2:EmailAlertAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe2:EmailAlertAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:EmailAlertAddress'',''amendaddress_jobe2:emailalertaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:EmailAlertAddress',p_web.GetSessionValueFormat('jobe2:EmailAlertAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Alert Address') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value')

Comment::jobe2:EmailAlertAddress  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_comment',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('jobe2:EmailNotification') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Waybill Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:CourierWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.GetValue('NewValue'))
    jobe2:CourierWaybillNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:CourierWaybillNumber
    do Value::jobe2:CourierWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:CourierWaybillNumber = p_web.GetValue('Value')
  End
    jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  do Value::jobe2:CourierWaybillNumber
  do SendAlert

Value::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:CourierWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:CourierWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:CourierWaybillNumber'',''amendaddress_jobe2:courierwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:CourierWaybillNumber',p_web.GetSessionValueFormat('jobe2:CourierWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Courier Waybill Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value')

Comment::jobe2:CourierWaybillNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::DeliveryAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::DeliveryAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('DeliveryAddress',p_web.GetValue('NewValue'))
    do Value::DeliveryAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::DeliveryAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Delivery Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::DeliveryAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::CollectionAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::CollectionAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('CollectionAddress',p_web.GetValue('NewValue'))
    do Value::CollectionAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::CollectionAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Collection Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::CollectionAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Company_Name_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Company Name')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt')

Validate::job:Company_Name_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetValue('NewValue'))
    job:Company_Name_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name_Delivery
    do Value::job:Company_Name_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name_Delivery = p_web.GetValue('Value')
  End
    job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  do Value::job:Company_Name_Delivery
  do SendAlert

Value::job:Company_Name_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Company_Name_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Delivery'',''amendaddress_job:company_name_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Delivery',p_web.GetSessionValueFormat('job:Company_Name_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value')

Comment::job:Company_Name_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_comment')

Prompt::job:Company_Name_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Company Name')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Company_Name_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetValue('NewValue'))
    job:Company_Name_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name_Collection
    do Value::job:Company_Name_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name_Collection = p_web.GetValue('Value')
  End
  do Value::job:Company_Name_Collection
  do SendAlert

Value::job:Company_Name_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Company_Name_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Collection'',''amendaddress_job:company_name_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Collection',p_web.GetSessionValueFormat('job:Company_Name_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value')

Comment::job:Company_Name_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line1_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Address')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt')

Validate::job:Address_Line1_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line1_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1_Delivery
    do Value::job:Address_Line1_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  do Value::job:Address_Line1_Delivery
  do SendAlert

Value::job:Address_Line1_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line1_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Delivery'',''amendaddress_job:address_line1_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Delivery',p_web.GetSessionValueFormat('job:Address_Line1_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value')

Comment::job:Address_Line1_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_comment')

Prompt::job:Address_Line1_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Address')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line1_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetValue('NewValue'))
    job:Address_Line1_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1_Collection
    do Value::job:Address_Line1_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1_Collection = p_web.GetValue('Value')
  End
    job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  do Value::job:Address_Line1_Collection
  do SendAlert

Value::job:Address_Line1_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line1_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Collection'',''amendaddress_job:address_line1_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Collection',p_web.GetSessionValueFormat('job:Address_Line1_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value')

Comment::job:Address_Line1_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line2_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt')

Validate::job:Address_Line2_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line2_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2_Delivery
    do Value::job:Address_Line2_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  do Value::job:Address_Line2_Delivery
  do SendAlert

Value::job:Address_Line2_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line2_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Delivery'',''amendaddress_job:address_line2_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Delivery',p_web.GetSessionValueFormat('job:Address_Line2_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value')

Comment::job:Address_Line2_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_comment')

Prompt::job:Address_Line2_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line2_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2_Collection',p_web.GetValue('NewValue'))
    job:Address_Line2_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2_Collection
    do Value::job:Address_Line2_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2_Collection = p_web.GetValue('Value')
  End
    job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  do Value::job:Address_Line2_Collection
  do SendAlert

Value::job:Address_Line2_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line2_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Collection'',''amendaddress_job:address_line2_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Collection',p_web.GetSessionValueFormat('job:Address_Line2_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value')

Comment::job:Address_Line2_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line3_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suburb')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt')

Validate::job:Address_Line3_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line3_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3_Delivery
    do Value::job:Address_Line3_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Delivery')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Delivery',sur:postcode)
          p_web.SSV('jobe2:HubDelivery',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Delivery')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::jobe2:HubDelivery
  do Value::job:Address_Line3_Delivery
  do SendAlert
  do Comment::job:Address_Line3_Delivery
  do Value::jobe2:HubDelivery  !1

Value::job:Address_Line3_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line3_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Delivery'',''amendaddress_job:address_line3_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Delivery')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Delivery',p_web.GetSessionValue('job:Address_Line3_Delivery'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3_Delivery&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value')

Comment::job:Address_Line3_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_comment')

Prompt::job:Address_Line3_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suburb')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line3_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetValue('NewValue'))
    job:Address_Line3_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3_Collection
    do Value::job:Address_Line3_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3_Collection = p_web.GetValue('Value')
  End
    job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Collection')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Collection',sur:postcode)
          p_web.SSV('jobe2:HubCollection',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Collection')
  do AfterLookup
  do Value::job:Postcode_Collection
  do Value::jobe2:HubCollection
  do Value::job:Address_Line3_Collection
  do SendAlert
  do Comment::job:Address_Line3_Collection
  do Value::jobe2:HubCollection  !1

Value::job:Address_Line3_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line3_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Collection'',''amendaddress_job:address_line3_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Collection')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Collection',p_web.GetSessionValue('job:Address_Line3_Collection'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3_Collection&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value')

Comment::job:Address_Line3_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_comment')

Prompt::jobe2:HubDelivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Hub')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt')

Validate::jobe2:HubDelivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetValue('NewValue'))
    jobe2:HubDelivery = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubDelivery
    do Value::jobe2:HubDelivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubDelivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubDelivery = p_web.GetValue('Value')
  End
    jobe2:HubDelivery = Upper(jobe2:HubDelivery)
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  do Value::jobe2:HubDelivery
  do SendAlert
  do Value::job:Postcode_Delivery  !1

Value::jobe2:HubDelivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jobe2:HubDelivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubDelivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubDelivery'',''amendaddress_jobe2:hubdelivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubDelivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubDelivery',p_web.GetSessionValueFormat('jobe2:HubDelivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value')

Comment::jobe2:HubDelivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_comment')

Prompt::jobe2:HubCollection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Hub')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt')

Validate::jobe2:HubCollection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubCollection',p_web.GetValue('NewValue'))
    jobe2:HubCollection = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubCollection
    do Value::jobe2:HubCollection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubCollection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubCollection = p_web.GetValue('Value')
  End
    jobe2:HubCollection = Upper(jobe2:HubCollection)
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  do Value::jobe2:HubCollection
  do SendAlert
  do Value::job:Postcode_Collection  !1

Value::jobe2:HubCollection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- jobe2:HubCollection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubCollection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCollection'',''amendaddress_jobe2:hubcollection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubCollection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCollection',p_web.GetSessionValueFormat('jobe2:HubCollection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value')

Comment::jobe2:HubCollection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_comment')

Prompt::job:Postcode_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Postcode')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt')

Validate::job:Postcode_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode_Delivery',p_web.GetValue('NewValue'))
    job:Postcode_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode_Delivery
    do Value::job:Postcode_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode_Delivery = p_web.GetValue('Value')
  End
  do Value::job:Postcode_Delivery
  do SendAlert

Value::job:Postcode_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Postcode_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Delivery'',''amendaddress_job:postcode_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Delivery',p_web.GetSessionValueFormat('job:Postcode_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value')

Comment::job:Postcode_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_comment')

Prompt::job:Postcode_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Postcode')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt')

Validate::job:Postcode_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode_Collection',p_web.GetValue('NewValue'))
    job:Postcode_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode_Collection
    do Value::job:Postcode_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode_Collection = p_web.GetValue('Value')
  End
  do Value::job:Postcode_Collection
  do SendAlert

Value::job:Postcode_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Postcode_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Collection'',''amendaddress_job:postcode_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Collection',p_web.GetSessionValueFormat('job:Postcode_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value')

Comment::job:Postcode_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_comment')

Prompt::job:Telephone_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt')

Validate::job:Telephone_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetValue('NewValue'))
    job:Telephone_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Delivery
    do Value::job:Telephone_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Delivery = p_web.GetValue('Value')
  End
    job:Telephone_Delivery = Upper(job:Telephone_Delivery)
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Delivery')) = 0
          p_web.SSV('job:Telephone_Delivery','')
          p_web.SSV('Comment:TelephoneNumberDelivery','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberDelivery','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Delivery
  do SendAlert
  do Comment::job:Telephone_Delivery

Value::job:Telephone_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Telephone_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Delivery'',''amendaddress_job:telephone_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Delivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Delivery',p_web.GetSessionValueFormat('job:Telephone_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value')

Comment::job:Telephone_Delivery  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumberDelivery'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_comment')

Prompt::job:Telephone_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Telephone_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Collection',p_web.GetValue('NewValue'))
    job:Telephone_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Collection
    do Value::job:Telephone_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Collection = p_web.GetValue('Value')
  End
    job:Telephone_Collection = Upper(job:Telephone_Collection)
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Collection')) = 0
          p_web.SSV('job:Telephone_Collection','')
          p_web.SSV('Comment:TelephoneNumberCollection','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberCollection','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Collection
  do SendAlert
  do Comment::job:Telephone_Collection

Value::job:Telephone_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Telephone_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Collection'',''amendaddress_job:telephone_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Collection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Collection',p_web.GetSessionValueFormat('job:Telephone_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value')

Comment::job:Telephone_Collection  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumberCollection'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_comment')

Validate::Button:CopyEndUserAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CopyEndUserAddress',p_web.GetValue('NewValue'))
    do Value::Button:CopyEndUserAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
  p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
  p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
  p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
  p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
  p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))
  p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress
  do SendAlert
  do Value::job:Address_Line1_Delivery  !1
  do Value::job:Address_Line2_Delivery  !1
  do Value::job:Address_Line3_Delivery  !1
  do Value::job:Company_Name_Delivery  !1
  do Value::job:Telephone_Delivery  !1
  do Value::jobe2:HubDelivery  !1
  do Value::job:Postcode_Delivery  !1

Value::Button:CopyEndUserAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress')&'.disabled=true;sv(''Button:CopyEndUserAddress'',''amendaddress_button:copyenduseraddress_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CopyEndUserAddress','Copy / Clear Address','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ClearAddressDetails&AddType=D')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value')

Comment::Button:CopyEndUserAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:CopyEndUserAddress1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:CopyEndUserAddress1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CopyEndUserAddress1',p_web.GetValue('NewValue'))
    do Value::Button:CopyEndUserAddress1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
  p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
  p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
  p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
  p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
  p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))
  p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress1
  do SendAlert
  do Value::job:Address_Line1_Collection  !1
  do Value::job:Address_Line2_Collection  !1
  do Value::job:Address_Line3_Collection  !1
  do Value::job:Company_Name_Collection  !1
  do Value::job:Telephone_Collection  !1
  do Value::jobe2:HubCollection  !1
  do Value::job:Postcode_Collection  !1

Value::Button:CopyEndUserAddress1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress1')&'.disabled=true;sv(''Button:CopyEndUserAddress1'',''amendaddress_button:copyenduseraddress1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CopyEndUserAddress1','Copy / Clear Address','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ClearAddressDetails&AddType=C')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value')

Comment::Button:CopyEndUserAddress1  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Delivery_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Delivery Text')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Delivery_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Delivery_Text',p_web.GetValue('NewValue'))
    jbn:Delivery_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Delivery_Text
    do Value::jbn:Delivery_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Delivery_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Delivery_Text = p_web.GetValue('Value')
  End
    jbn:Delivery_Text = Upper(jbn:Delivery_Text)
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  do Value::jbn:Delivery_Text
  do SendAlert

Value::jbn:Delivery_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- TEXT --- jbn:Delivery_Text
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Delivery_Text')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Delivery_Text'',''amendaddress_jbn:delivery_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Delivery_Text',p_web.GetSessionValue('jbn:Delivery_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value')

Comment::jbn:Delivery_Text  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Collection_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Collection Text')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Collection_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Collection_Text',p_web.GetValue('NewValue'))
    jbn:Collection_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Collection_Text
    do Value::jbn:Collection_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Collection_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Collection_Text = p_web.GetValue('Value')
  End
    jbn:Collection_Text = Upper(jbn:Collection_Text)
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  do Value::jbn:Collection_Text
  do SendAlert

Value::jbn:Collection_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- TEXT --- jbn:Collection_Text
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Collection_Text')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Collection_Text'',''amendaddress_jbn:collection_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Collection_Text',p_web.GetSessionValue('jbn:Collection_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Collection_Text),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value')

Comment::jbn:Collection_Text  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:DelContactName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contact Name')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:DelContactName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:DelContactName',p_web.GetValue('NewValue'))
    jbn:DelContactName = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:DelContactName
    do Value::jbn:DelContactName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:DelContactName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:DelContactName = p_web.GetValue('Value')
  End
    jbn:DelContactName = Upper(jbn:DelContactName)
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  do Value::jbn:DelContactName
  do SendAlert

Value::jbn:DelContactName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:DelContactName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelContactName'',''amendaddress_jbn:delcontactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelContactName',p_web.GetSessionValueFormat('jbn:DelContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_value')

Comment::jbn:DelContactName  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:ColContatName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contact Name')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:ColContatName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:ColContatName',p_web.GetValue('NewValue'))
    jbn:ColContatName = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:ColContatName
    do Value::jbn:ColContatName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:ColContatName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:ColContatName = p_web.GetValue('Value')
  End
    jbn:ColContatName = Upper(jbn:ColContatName)
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  do Value::jbn:ColContatName
  do SendAlert

Value::jbn:ColContatName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColContatName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:ColContatName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColContatName'',''amendaddress_jbn:colcontatname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColContatName',p_web.GetSessionValueFormat('jbn:ColContatName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_value')

Comment::jbn:ColContatName  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:DelDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Department')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:DelDepartment  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:DelDepartment',p_web.GetValue('NewValue'))
    jbn:DelDepartment = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:DelDepartment
    do Value::jbn:DelDepartment
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:DelDepartment',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:DelDepartment = p_web.GetValue('Value')
  End
    jbn:DelDepartment = Upper(jbn:DelDepartment)
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  do Value::jbn:DelDepartment
  do SendAlert

Value::jbn:DelDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:DelDepartment')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelDepartment'',''amendaddress_jbn:deldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelDepartment',p_web.GetSessionValueFormat('jbn:DelDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_value')

Comment::jbn:DelDepartment  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:ColDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Department')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:ColDepartment  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:ColDepartment',p_web.GetValue('NewValue'))
    jbn:ColDepartment = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:ColDepartment
    do Value::jbn:ColDepartment
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:ColDepartment',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:ColDepartment = p_web.GetValue('Value')
  End
    jbn:ColDepartment = Upper(jbn:ColDepartment)
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  do Value::jbn:ColDepartment
  do SendAlert

Value::jbn:ColDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:ColDepartment')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColDepartment'',''amendaddress_jbn:coldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColDepartment',p_web.GetSessionValueFormat('jbn:ColDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_value')

Comment::jbn:ColDepartment  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:Sub_Sub_Account  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:Sub_Sub_Account  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',p_web.GetValue('NewValue'))
    jobe:Sub_Sub_Account = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:Sub_Sub_Account
    do Value::jobe:Sub_Sub_Account
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:Sub_Sub_Account = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','jobe:Sub_Sub_Account')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::job:Address_Line1_Delivery
  do Value::job:Address_Line2_Delivery
  do Value::job:Address_Line3_Delivery
  do Value::job:Telephone_Delivery
  do Value::job:Company_Name_Delivery
  do Value::jobe:Sub_Sub_Account
  do SendAlert
  do Comment::jobe:Sub_Sub_Account

Value::jobe:Sub_Sub_Account  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:Sub_Sub_Account
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe:Sub_Sub_Account')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Sub_Sub_Account'',''amendaddress_jobe:sub_sub_account_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Sub_Sub_Account')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','jobe:Sub_Sub_Account',p_web.GetSessionValue('jobe:Sub_Sub_Account'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseSubAddresses')&'?LookupField=jobe:Sub_Sub_Account&Tab=2&ForeignField=sua:AccountNumber&_sort=sua:CompanyName&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value')

Comment::jobe:Sub_Sub_Account  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('AmendAddress_job:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name
      else
        do Value::job:Company_Name
      end
  of lower('AmendAddress_job:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1
      else
        do Value::job:Address_Line1
      end
  of lower('AmendAddress_job:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2
      else
        do Value::job:Address_Line2
      end
  of lower('AmendAddress_job:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3
      else
        do Value::job:Address_Line3
      end
  of lower('AmendAddress_jobe2:HubCustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCustomer
      else
        do Value::jobe2:HubCustomer
      end
  of lower('AmendAddress_job:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode
      else
        do Value::job:Postcode
      end
  of lower('AmendAddress_job:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Number
      else
        do Value::job:Telephone_Number
      end
  of lower('AmendAddress_job:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Fax_Number
      else
        do Value::job:Fax_Number
      end
  of lower('AmendAddress_jobe:EndUserEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserEmailAddress
      else
        do Value::jobe:EndUserEmailAddress
      end
  of lower('AmendAddress_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('AmendAddress_jobe:VatNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VatNumber
      else
        do Value::jobe:VatNumber
      end
  of lower('AmendAddress_jobe2:IDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:IDNumber
      else
        do Value::jobe2:IDNumber
      end
  of lower('AmendAddress_jobe2:SMSNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSNotification
      else
        do Value::jobe2:SMSNotification
      end
  of lower('AmendAddress_jobe2:SMSAlertNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSAlertNumber
      else
        do Value::jobe2:SMSAlertNumber
      end
  of lower('AmendAddress_jobe2:EmailNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailNotification
      else
        do Value::jobe2:EmailNotification
      end
  of lower('AmendAddress_jobe2:EmailAlertAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailAlertAddress
      else
        do Value::jobe2:EmailAlertAddress
      end
  of lower('AmendAddress_jobe2:CourierWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:CourierWaybillNumber
      else
        do Value::jobe2:CourierWaybillNumber
      end
  of lower('AmendAddress_job:Company_Name_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Delivery
      else
        do Value::job:Company_Name_Delivery
      end
  of lower('AmendAddress_job:Company_Name_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Collection
      else
        do Value::job:Company_Name_Collection
      end
  of lower('AmendAddress_job:Address_Line1_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Delivery
      else
        do Value::job:Address_Line1_Delivery
      end
  of lower('AmendAddress_job:Address_Line1_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Collection
      else
        do Value::job:Address_Line1_Collection
      end
  of lower('AmendAddress_job:Address_Line2_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Delivery
      else
        do Value::job:Address_Line2_Delivery
      end
  of lower('AmendAddress_job:Address_Line2_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Collection
      else
        do Value::job:Address_Line2_Collection
      end
  of lower('AmendAddress_job:Address_Line3_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Delivery
      else
        do Value::job:Address_Line3_Delivery
      end
  of lower('AmendAddress_job:Address_Line3_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Collection
      else
        do Value::job:Address_Line3_Collection
      end
  of lower('AmendAddress_jobe2:HubDelivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubDelivery
      else
        do Value::jobe2:HubDelivery
      end
  of lower('AmendAddress_jobe2:HubCollection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCollection
      else
        do Value::jobe2:HubCollection
      end
  of lower('AmendAddress_job:Postcode_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Delivery
      else
        do Value::job:Postcode_Delivery
      end
  of lower('AmendAddress_job:Postcode_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Collection
      else
        do Value::job:Postcode_Collection
      end
  of lower('AmendAddress_job:Telephone_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Delivery
      else
        do Value::job:Telephone_Delivery
      end
  of lower('AmendAddress_job:Telephone_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Collection
      else
        do Value::job:Telephone_Collection
      end
  of lower('AmendAddress_Button:CopyEndUserAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress
      else
        do Value::Button:CopyEndUserAddress
      end
  of lower('AmendAddress_Button:CopyEndUserAddress1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress1
      else
        do Value::Button:CopyEndUserAddress1
      end
  of lower('AmendAddress_jbn:Delivery_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Delivery_Text
      else
        do Value::jbn:Delivery_Text
      end
  of lower('AmendAddress_jbn:Collection_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Collection_Text
      else
        do Value::jbn:Collection_Text
      end
  of lower('AmendAddress_jbn:DelContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelContactName
      else
        do Value::jbn:DelContactName
      end
  of lower('AmendAddress_jbn:ColContatName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColContatName
      else
        do Value::jbn:ColContatName
      end
  of lower('AmendAddress_jbn:DelDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelDepartment
      else
        do Value::jbn:DelDepartment
      end
  of lower('AmendAddress_jbn:ColDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColDepartment
      else
        do Value::jbn:ColDepartment
      end
  of lower('AmendAddress_jobe:Sub_Sub_Account_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Sub_Sub_Account
      else
        do Value::jobe:Sub_Sub_Account
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)

PreCopy  Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)

PreDelete       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.setsessionvalue('showtab_AmendAddress',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('jobe2:SMSNotification') = 0
            p_web.SetValue('jobe2:SMSNotification',0)
            jobe2:SMSNotification = 0
          End
          If p_web.IfExistsValue('jobe2:EmailNotification') = 0
            p_web.SetValue('jobe2:EmailNotification',0)
            jobe2:EmailNotification = 0
          End
  If p_web.GSV('Hide:SubSubAccount') <> 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          job:Company_Name = Upper(job:Company_Name)
          p_web.SetSessionValue('job:Company_Name',job:Company_Name)
        If loc:Invalid <> '' then exit.
          job:Address_Line1 = Upper(job:Address_Line1)
          p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
        If loc:Invalid <> '' then exit.
          job:Address_Line2 = Upper(job:Address_Line2)
          p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
        If loc:Invalid <> '' then exit.
          job:Address_Line3 = Upper(job:Address_Line3)
          p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
        If loc:Invalid <> '' then exit.
          jobe2:HubCustomer = Upper(jobe2:HubCustomer)
          p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
        If loc:Invalid <> '' then exit.
          job:Telephone_Number = Upper(job:Telephone_Number)
          p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
        If loc:Invalid <> '' then exit.
          job:Fax_Number = Upper(job:Fax_Number)
          p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
        If loc:Invalid <> '' then exit.
          jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
          p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
        If loc:Invalid <> '' then exit.
    If p_web.GSV('Hide:VatNumber') <> 1
          jobe:VatNumber = Upper(jobe:VatNumber)
          p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
        If loc:Invalid <> '' then exit.
    End
          jobe2:IDNumber = Upper(jobe2:IDNumber)
          p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('jobe2:SMSNotification') = 0)
          jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
          p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
        If loc:Invalid <> '' then exit.
      End
          jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
          p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
        If loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
          p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
          p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
          p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
          p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
          p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
          p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
          p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          jobe2:HubDelivery = Upper(jobe2:HubDelivery)
          p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          jobe2:HubCollection = Upper(jobe2:HubCollection)
          p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Telephone_Delivery = Upper(job:Telephone_Delivery)
          p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Telephone_Collection = Upper(job:Telephone_Collection)
          p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:Delivery_Text = Upper(jbn:Delivery_Text)
          p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:Collection_Text = Upper(jbn:Collection_Text)
          p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:DelContactName = Upper(jbn:DelContactName)
          p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:ColContatName = Upper(jbn:ColContatName)
          p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:DelDepartment = Upper(jbn:DelDepartment)
          p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:ColDepartment = Upper(jbn:ColDepartment)
          p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 4
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  !p_web.SetSessionValue('job:Company_Name',p_web.GetSessionValue('tmp:CompanyName'))
  !p_web.SetSessionValue('job:Address_Line1',p_web.GetSessionValue('tmp:AddressLine1'))
  !p_web.SetSessionValue('job:Address_Line2',p_web.GetSessionValue('tmp:AddressLine2'))
  !p_web.SetSessionValue('job:Address_Line3',p_web.GetSessionValue('tmp:Suburb'))
  !p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetSessionValue('tmp:HubCustomer'))
  !p_web.SetSessionValue('job:Telephone_Number',p_web.GetSessionValue('tmp:TelephoneNumber'))
  !p_web.SetSessionValue('job:Fax_Number',p_web.GetSessionValue('tmp:FaxNumber'))
  !p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetSessionValue('tmp:EmailAddress'))
  !p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetSessionValue('tmp:EndUserTelephoneNumber'))
  !p_web.SetSessionValue('jobe2:IDNumber',p_web.GetSessionValue('tmp:IDNumber'))
  !p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetSessionValue('tmp:SMSNotification'))
  !p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetSessionValue('tmp:NotificationMobileNumber'))
  !p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetSessionValue('tmp:EmailNotification'))
  !p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetSessionValue('tmp:NotificationEmailAddress'))
  !
  !p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetSessionValue('tmp:CompanyNameDelivery'))
  !p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetSessionValue('tmp:CompanyNameCollection'))
  !p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetSessionValue('tmp:AddressLine1Delivery'))
  !p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetSessionValue('tmp:AddressLine1Collection'))
  !p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetSessionValue('tmp:SuburbDelivery'))
  !p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetSessionValue('tmp:SuburbCollection'))
  !p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetSessionValue('tmp:HubDelivery'))
  !p_web.SetSessionValue('jobe2:HubCollection',p_web.GetSessionValue('tmp:HubCollection'))
  !p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetSessionValue('tmp:TelephoneNumberDelivery'))
  !p_web.SetSessionValue('job:Telephone_Collection',p_web.GetSessionValue('tmp:TelephoneNumberCollection'))
  !
  !
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name')
  p_web.StoreValue('job:Address_Line1')
  p_web.StoreValue('job:Address_Line2')
  p_web.StoreValue('job:Address_Line3')
  p_web.StoreValue('job:Postcode')
  p_web.StoreValue('job:Telephone_Number')
  p_web.StoreValue('job:Fax_Number')
  p_web.StoreValue('jobe:EndUserEmailAddress')
  p_web.StoreValue('jobe:EndUserTelNo')
  p_web.StoreValue('jobe:VatNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name_Delivery')
  p_web.StoreValue('job:Company_Name_Collection')
  p_web.StoreValue('job:Address_Line1_Delivery')
  p_web.StoreValue('job:Address_Line1_Collection')
  p_web.StoreValue('job:Address_Line2_Delivery')
  p_web.StoreValue('job:Address_Line2_Collection')
  p_web.StoreValue('job:Address_Line3_Delivery')
  p_web.StoreValue('job:Address_Line3_Collection')
  p_web.StoreValue('job:Postcode_Delivery')
  p_web.StoreValue('job:Postcode_Collection')
  p_web.StoreValue('job:Telephone_Delivery')
  p_web.StoreValue('job:Telephone_Collection')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Delivery_Text')
  p_web.StoreValue('jbn:Collection_Text')
  p_web.StoreValue('jbn:DelContactName')
  p_web.StoreValue('jbn:ColContatName')
  p_web.StoreValue('jbn:DelDepartment')
  p_web.StoreValue('jbn:ColDepartment')
  p_web.StoreValue('jobe:Sub_Sub_Account')
ClearAddressDetails  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locClearAddressOption BYTE                                 !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('ClearAddressDetails')
  loc:formname = 'ClearAddressDetails_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ClearAddressDetails','')
    p_web._DivHeader('ClearAddressDetails',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ClearAddressDetails',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ClearAddressDetails',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
deleteVariables     ROUTINE
    p_web.DeleteSessionValue('locClearAddressOption')
    p_web.DeleteSessionValue('AddType')
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ClearAddressDetails_form:inited_',1)
  do RestoreMem

CancelForm  Routine
      DO deleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locClearAddressOption',locClearAddressOption)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locClearAddressOption')
    locClearAddressOption = p_web.GetValue('locClearAddressOption')
    p_web.SetSessionValue('locClearAddressOption',locClearAddressOption)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ClearAddressDetails_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.StoreValue('AddType')
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locClearAddressOption = p_web.RestoreValue('locClearAddressOption')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'AmendAddress'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ClearAddressDetails_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ClearAddressDetails_ChainTo')
    loc:formaction = p_web.GetSessionValue('ClearAddressDetails_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'AmendAddress'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ClearAddressDetails" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ClearAddressDetails" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ClearAddressDetails" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Address Options') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Address Options',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ClearAddressDetails">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ClearAddressDetails" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ClearAddressDetails')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Options') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ClearAddressDetails')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ClearAddressDetails'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ClearAddressDetails')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Options') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ClearAddressDetails_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locClearAddressOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locClearAddressOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::locClearAddressOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locClearAddressOption',p_web.GetValue('NewValue'))
    locClearAddressOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locClearAddressOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locClearAddressOption',p_web.GetValue('Value'))
    locClearAddressOption = p_web.GetValue('Value')
  End
  do Value::locClearAddressOption
  do SendAlert

Value::locClearAddressOption  Routine
  p_web._DivHeader('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locClearAddressOption
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('locClearAddressOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locClearAddressOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Clear Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locClearAddressOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Customer Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  if p_web.GSV('Hide:CollectionAddress') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('AddType') = 'C') then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('locClearAddressOption') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Collection Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  if p_web.GSV('Hide:DeliveryAddress') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('AddType') = 'D') then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('locClearAddressOption') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(4)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Delivery Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_value')

Comment::locClearAddressOption  Routine
    loc:comment = ''
  p_web._DivHeader('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ClearAddressDetails_locClearAddressOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locClearAddressOption
      else
        do Value::locClearAddressOption
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)

PreCopy  Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)

PreDelete       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ClearAddressDetails_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      CASE p_web.GSV('locClearAddressOption')
      OF 1 ! Clear Address
          CASE p_web.GSV('AddType')
          OF 'C'
              p_web.SSV('job:Postcode_Collection','')
              p_web.SSV('job:Company_Name_Collection','')
              p_web.SSV('job:Address_Line1_Collection','')
              p_web.SSV('job:Address_Line2_Collection','')
              p_web.SSV('job:Address_Line3_Collection','')
              p_web.SSV('job:Telephone_Collection','')
              p_web.SSV('jobe2:HubCollection','')            
          OF 'D'
              p_web.SSV('job:Postcode_Delivery','')
              p_web.SSV('job:Company_Name_Delivery','')
              p_web.SSV('job:Address_Line1_Delivery','')
              p_web.SSV('job:Address_Line2_Delivery','')
              p_web.SSV('job:Address_Line3_Delivery','')
              p_web.SSV('job:Telephone_Delivery','')
              p_web.SSV('jobe2:HubDelivery','')            
          END
         
      OF 2 ! Copy Customer Address
          CASE p_web.GSV('AddType')
          OF 'C'
              p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
              p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
              p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
              p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
              p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
              p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
              p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))            
          OF 'D'
              p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
              p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
              p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
              p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
              p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
              p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
              p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))            
          END
      OF 3 ! Copy Collection Address
          IF (p_web.GSV('AddType') = 'D')
              p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode_Collection'))
              p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name_Collection'))
              p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1_Collection'))
              p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2_Collection'))
              p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3_Collection'))
              p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number_Collection'))
              p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer_Collection'))            
          END
          
      OF 4 ! Copy Delivery Address
          IF (p_web.GSV('AddType') = 'C')
              p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode_Delivery'))
              p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name_Delivery'))
              p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1_Delivery'))
              p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2_Delivery'))
              p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3_Delivery'))
              p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number_Delivery'))
              p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubDelivery'))  
          END
      ELSE
          loc:alert = 'You must select an option.'
          loc:invalid = 'locClearAddressOption'
  
      END
  
         
         DO deleteVariables
  p_web.DeleteSessionValue('ClearAddressDetails_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)
  p_web.StoreValue('locClearAddressOption')
SelectCouriers       PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(COURIER)
                      Project(cou:Courier)
                      Project(cou:Courier)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('SelectCouriers')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectCouriers:NoForm')
      loc:NoForm = p_web.GetValue('SelectCouriers:NoForm')
      loc:FormName = p_web.GetValue('SelectCouriers:FormName')
    else
      loc:FormName = 'SelectCouriers_frm'
    End
    p_web.SSV('SelectCouriers:NoForm',loc:NoForm)
    p_web.SSV('SelectCouriers:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectCouriers:NoForm')
    loc:FormName = p_web.GSV('SelectCouriers:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectCouriers') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectCouriers')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(COURIER,cou:Courier_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'COU:COURIER') then p_web.SetValue('SelectCouriers_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectCouriers:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectCouriers:LookupFrom','LookupFrom')
    p_web.StoreValue('SelectCouriers:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectCouriers:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectCouriers:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 22
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectCouriers_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectCouriers_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(cou:Courier)','-UPPER(cou:Courier)')
    Loc:LocateField = 'cou:Courier'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(cou:Courier)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('cou:Courier')
    loc:SortHeader = p_web.Translate('Courier')
    p_web.SetSessionValue('SelectCouriers_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('SelectCouriers:LookupFrom')
  End!Else
  loc:formaction = 'SelectCouriers'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectCouriers:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectCouriers:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectCouriers:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="COURIER"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="cou:Courier_Key"></input><13,10>'
  end
  If p_web.Translate('Select Courier') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Courier',0)&'</span>'&CRLF
  End
  If clip('Select Courier') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectCouriers',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2SelectCouriers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectCouriers.locate(''Locator2SelectCouriers'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectCouriers.cl(''SelectCouriers'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="SelectCouriers_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="SelectCouriers_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','SelectCouriers','Courier','Click here to sort by Courier',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Courier')&'">'&p_web.Translate('Courier')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,22,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('cou:courier',lower(Thisview{prop:order}),1,1) = 0 !and COURIER{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'cou:Courier'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('cou:Courier'),p_web.GetValue('cou:Courier'),p_web.GetSessionValue('cou:Courier'))
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectCouriers',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectCouriers_Filter')
    p_web.SetSessionValue('SelectCouriers_FirstValue','')
    p_web.SetSessionValue('SelectCouriers_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,COURIER,cou:Courier_Key,loc:PageRows,'SelectCouriers',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If COURIER{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(COURIER,loc:firstvalue)
              Reset(ThisView,COURIER)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If COURIER{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(COURIER,loc:lastvalue)
            Reset(ThisView,COURIER)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(cou:Courier)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectCouriers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectCouriers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectCouriers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectCouriers.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectCouriers',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectCouriers_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectCouriers_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1SelectCouriers',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectCouriers.locate(''Locator1SelectCouriers'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectCouriers.cl(''SelectCouriers'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('SelectCouriers_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectCouriers_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectCouriers.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectCouriers.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectCouriers.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectCouriers.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = cou:Courier
    p_web._thisrow = p_web._nocolon('cou:Courier')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectCouriers:LookupField')) = cou:Courier and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((cou:Courier = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="SelectCouriers.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If COURIER{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(COURIER)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If COURIER{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(COURIER)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','cou:Courier',clip(loc:field),,loc:checked,,,'onclick="SelectCouriers.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','cou:Courier',clip(loc:field),,'checked',,,'onclick="SelectCouriers.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cou:Courier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="SelectCouriers.omv(this);" onMouseOut="SelectCouriers.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var SelectCouriers=new browseTable(''SelectCouriers'','''&clip(loc:formname)&''','''&p_web._jsok('cou:Courier',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('cou:Courier')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'SelectCouriers.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'SelectCouriers.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectCouriers')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectCouriers')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectCouriers')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectCouriers')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(COURIER)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(COURIER)
  Bind(cou:Record)
  Clear(cou:Record)
  NetWebSetSessionPics(p_web,COURIER)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('cou:Courier',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&cou:Courier,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectCouriers',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectCouriers',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectCouriers',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cou:Courier   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cou:Courier_'&cou:Courier,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cou:Courier,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(cou:Courier_Key)
    loc:Invalid = 'cou:Courier'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Courier_Key --> '&clip('Courier')&' = ' & clip(cou:Courier)
  End
PushDefaultSelection  Routine
  loc:default = cou:Courier

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('cou:Courier',cou:Courier)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('cou:Courier',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('cou:Courier'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('cou:Courier'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseSubAddresses   PROCEDURE  (NetWebServerWorker p_web)
locSubRefNumber      LONG                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SUBACCAD)
                      Project(sua:RecordNumber)
                      Project(sua:CompanyName)
                      Project(sua:AccountNumber)
                      Project(sua:Postcode)
                      Project(sua:AddressLine1)
                      Project(sua:AddressLine2)
                      Project(sua:AddressLine3)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
SUBTRACC::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('BrowseSubAddresses')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseSubAddresses:NoForm')
      loc:NoForm = p_web.GetValue('BrowseSubAddresses:NoForm')
      loc:FormName = p_web.GetValue('BrowseSubAddresses:FormName')
    else
      loc:FormName = 'BrowseSubAddresses_frm'
    End
    p_web.SSV('BrowseSubAddresses:NoForm',loc:NoForm)
    p_web.SSV('BrowseSubAddresses:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseSubAddresses:NoForm')
    loc:FormName = p_web.GSV('BrowseSubAddresses:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseSubAddresses') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseSubAddresses')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SUBACCAD,sua:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SUA:COMPANYNAME') then p_web.SetValue('BrowseSubAddresses_sort','1')
    ElsIf (loc:vorder = 'SUA:ACCOUNTNUMBER') then p_web.SetValue('BrowseSubAddresses_sort','2')
    ElsIf (loc:vorder = 'SUA:POSTCODE') then p_web.SetValue('BrowseSubAddresses_sort','3')
    ElsIf (loc:vorder = 'SUA:ADDRESSLINE1') then p_web.SetValue('BrowseSubAddresses_sort','4')
    ElsIf (loc:vorder = 'SUA:ADDRESSLINE2') then p_web.SetValue('BrowseSubAddresses_sort','5')
    ElsIf (loc:vorder = 'SUA:ADDRESSLINE3') then p_web.SetValue('BrowseSubAddresses_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseSubAddresses:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseSubAddresses:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseSubAddresses:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseSubAddresses:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseSubAddresses:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseSubAddresses_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseSubAddresses_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 7
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:CompanyName)','-UPPER(sua:CompanyName)')
    Loc:LocateField = 'sua:CompanyName'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AccountNumber)','-UPPER(sua:AccountNumber)')
    Loc:LocateField = 'sua:AccountNumber'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:Postcode)','-UPPER(sua:Postcode)')
    Loc:LocateField = 'sua:Postcode'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AddressLine1)','-UPPER(sua:AddressLine1)')
    Loc:LocateField = 'sua:AddressLine1'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AddressLine2)','-UPPER(sua:AddressLine2)')
    Loc:LocateField = 'sua:AddressLine2'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sua:AddressLine3)','-UPPER(sua:AddressLine3)')
    Loc:LocateField = 'sua:AddressLine3'
  end
  if loc:vorder = ''
    loc:vorder = '+sua:RefNumber,+UPPER(sua:CompanyName)'
  end
  If False ! add range fields to sort order
  Else
    If Instring('SUA:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'sua:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sua:CompanyName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  Of upper('sua:AccountNumber')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s15')
  Of upper('sua:Postcode')
    loc:SortHeader = p_web.Translate('Postcode')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s15')
  Of upper('sua:AddressLine1')
    loc:SortHeader = p_web.Translate('Address')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  Of upper('sua:AddressLine2')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  Of upper('sua:AddressLine3')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('BrowseSubAddresses_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseSubAddresses:LookupFrom')
  End!Else
  loc:formaction = 'BrowseSubAddresses'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseSubAddresses:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseSubAddresses:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseSubAddresses:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SUBACCAD"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sua:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse Accounts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Accounts',0)&'</span>'&CRLF
  End
  If clip('Browse Accounts') <> ''
    packet = clip(packet) & p_web.br
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSubAddresses',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseSubAddresses',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseSubAddresses.locate(''Locator2BrowseSubAddresses'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSubAddresses.cl(''BrowseSubAddresses'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseSubAddresses_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseSubAddresses_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
        p_web.SSV('locSubRefNumber',sub:RecordNumber)
    END
    
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseSubAddresses','Company Name','Click here to sort by Company Name',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Company Name')&'">'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseSubAddresses','Account Number','Click here to sort by Account Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseSubAddresses','Postcode','Click here to sort by Postcode',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Postcode')&'">'&p_web.Translate('Postcode')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseSubAddresses','Address','Click here to sort by Address',,,,3)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Address')&'" colspan="'&clip(3)&'">'&p_web.Translate('Address')&'</th>'&CRLF
        End
        loc:SkipHeader =3 - 1
        do AddPacket
        loc:columns += 3
      If loc:SkipHeader
        loc:SkipHeader -= 1
      Else
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseSubAddresses','','',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('')&'">'&p_web.Translate('')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
      End ! if skipped header
      If loc:SkipHeader
        loc:SkipHeader -= 1
      Else
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseSubAddresses','','',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('')&'">'&p_web.Translate('')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
      End ! if skipped header
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sua:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SUBACCAD{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sua:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sua:RecordNumber'),p_web.GetValue('sua:RecordNumber'),p_web.GetSessionValue('sua:RecordNumber'))
    locSubRefNumber = p_web.RestoreValue('locSubRefNumber')
    loc:FilterWas = 'sua:RefNumber = ' & locSubRefNumber
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSubAddresses',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseSubAddresses_Filter')
    p_web.SetSessionValue('BrowseSubAddresses_FirstValue','')
    p_web.SetSessionValue('BrowseSubAddresses_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SUBACCAD,sua:RecordNumberKey,loc:PageRows,'BrowseSubAddresses',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SUBACCAD{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SUBACCAD,loc:firstvalue)
              Reset(ThisView,SUBACCAD)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SUBACCAD{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SUBACCAD,loc:lastvalue)
            Reset(ThisView,SUBACCAD)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sua:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSubAddresses.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSubAddresses.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSubAddresses.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSubAddresses.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSubAddresses',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseSubAddresses_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseSubAddresses_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseSubAddresses',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseSubAddresses.locate(''Locator1BrowseSubAddresses'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSubAddresses.cl(''BrowseSubAddresses'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseSubAddresses_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseSubAddresses_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSubAddresses.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSubAddresses.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSubAddresses.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSubAddresses.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sua:RecordNumber
    p_web._thisrow = p_web._nocolon('sua:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseSubAddresses:LookupField')) = sua:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sua:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseSubAddresses.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SUBACCAD{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SUBACCAD)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SUBACCAD{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SUBACCAD)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sua:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseSubAddresses.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sua:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseSubAddresses.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:CompanyName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AccountNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:Postcode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td colspan="'&clip(3)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AddressLine1
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AddressLine2
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sua:AddressLine3
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseSubAddresses.omv(this);" onMouseOut="BrowseSubAddresses.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseSubAddresses=new browseTable(''BrowseSubAddresses'','''&clip(loc:formname)&''','''&p_web._jsok('sua:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sua:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseSubAddresses.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseSubAddresses.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSubAddresses')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSubAddresses')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSubAddresses')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSubAddresses')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SUBACCAD)
  p_web._CloseFile(SUBTRACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SUBACCAD)
  Bind(sua:Record)
  Clear(sua:Record)
  NetWebSetSessionPics(p_web,SUBACCAD)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sua:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&sua:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseSubAddresses',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseSubAddresses',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseSubAddresses',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:CompanyName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:CompanyName_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:CompanyName,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AccountNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AccountNumber_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AccountNumber,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:Postcode   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:Postcode_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:Postcode,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AddressLine1   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AddressLine1_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AddressLine1,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AddressLine2   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AddressLine2_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AddressLine2,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sua:AddressLine3   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sua:AddressLine3_'&sua:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sua:AddressLine3,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(sua:AccountNumberOnlyKey)
    loc:Invalid = 'sua:AccountNumber'
    loc:Alert = clip(p_web.site.DuplicateText) & ' AccountNumberOnlyKey --> '&clip('Account Number')&' = ' & clip(sua:AccountNumber)
  End
PushDefaultSelection  Routine
  loc:default = sua:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sua:RecordNumber',sua:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sua:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sua:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sua:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
