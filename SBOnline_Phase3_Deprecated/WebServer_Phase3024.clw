

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3024.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3019.INC'),ONCE        !Req'd for module callout resolution
                     END


LookupJobStatus      PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(ACCSTAT)
                      Project(acs:RecordNumber)
                      Project(acs:Status)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('LookupJobStatus')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('LookupJobStatus:NoForm')
      loc:NoForm = p_web.GetValue('LookupJobStatus:NoForm')
      loc:FormName = p_web.GetValue('LookupJobStatus:FormName')
    else
      loc:FormName = 'LookupJobStatus_frm'
    End
    p_web.SSV('LookupJobStatus:NoForm',loc:NoForm)
    p_web.SSV('LookupJobStatus:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('LookupJobStatus:NoForm')
    loc:FormName = p_web.GSV('LookupJobStatus:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('LookupJobStatus') & '_' & lower(loc:parent)
  else
    loc:divname = lower('LookupJobStatus')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ACCSTAT,acs:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'ACS:STATUS') then p_web.SetValue('LookupJobStatus_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('LookupJobStatus:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('LookupJobStatus:LookupFrom','LookupFrom')
    p_web.StoreValue('LookupJobStatus:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('LookupJobStatus:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('LookupJobStatus:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('LookupJobStatus_sort',net:DontEvaluate)
  p_web.SetSessionValue('LookupJobStatus_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(acs:Status)','-UPPER(acs:Status)')
    Loc:LocateField = 'acs:Status'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(acs:AccessArea),+UPPER(acs:Status)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('acs:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('LookupJobStatus_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('LookupJobStatus:LookupFrom')
  End!Else
  loc:CloseAction = p_web.site.DefaultPage
  loc:formaction = 'LookupJobStatus'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
    do SendPacket
    Do Title
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="LookupJobStatus:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupJobStatus:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('LookupJobStatus:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ACCSTAT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="acs:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupJobStatus',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2LookupJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupJobStatus.locate(''Locator2LookupJobStatus'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupJobStatus.cl(''LookupJobStatus'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="LookupJobStatus_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('FormCentre')&'" id="LookupJobStatus_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','LookupJobStatus','Status','Click here to sort by Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('acs:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and ACCSTAT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'acs:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('acs:RecordNumber'),p_web.GetValue('acs:RecordNumber'),p_web.GetSessionValue('acs:RecordNumber'))
      loc:FilterWas = 'Upper(acs:AccessArea) = Upper(<39>' & p_web.GetSessionValue('EngineerLevel') & '<39>)'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupJobStatus',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('LookupJobStatus_Filter')
    p_web.SetSessionValue('LookupJobStatus_FirstValue','')
    p_web.SetSessionValue('LookupJobStatus_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ACCSTAT,acs:RecordNumberKey,loc:PageRows,'LookupJobStatus',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ACCSTAT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ACCSTAT,loc:firstvalue)
              Reset(ThisView,ACCSTAT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ACCSTAT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ACCSTAT,loc:lastvalue)
            Reset(ThisView,ACCSTAT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acs:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'LookupJobStatus')
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'LookupJobStatus',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('LookupJobStatus_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('LookupJobStatus_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1LookupJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="LookupJobStatus.locate(''Locator1LookupJobStatus'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'LookupJobStatus.cl(''LookupJobStatus'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('LookupJobStatus_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('LookupJobStatus_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'LookupJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'LookupJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'LookupJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'LookupJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'LookupJobStatus')
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = acs:RecordNumber
    p_web._thisrow = p_web._nocolon('acs:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('LookupJobStatus:LookupField')) = acs:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((acs:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="LookupJobStatus.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ACCSTAT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ACCSTAT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ACCSTAT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ACCSTAT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acs:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="LookupJobStatus.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acs:RecordNumber',clip(loc:field),,'checked',,,'onclick="LookupJobStatus.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::acs:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="LookupJobStatus.omv(this);" onMouseOut="LookupJobStatus.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var LookupJobStatus=new browseTable(''LookupJobStatus'','''&clip(loc:formname)&''','''&p_web._jsok('acs:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('acs:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'LookupJobStatus.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'LookupJobStatus.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupJobStatus')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupJobStatus')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1LookupJobStatus')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2LookupJobStatus')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ACCSTAT)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ACCSTAT)
  Bind(acs:Record)
  Clear(acs:Record)
  NetWebSetSessionPics(p_web,ACCSTAT)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('acs:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::acs:Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('acs:Status_'&acs:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(acs:Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = acs:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('acs:RecordNumber',acs:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('acs:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acs:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acs:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Title  Routine
  packet = clip(packet) & |
    '<<table class="FormSubTitle"><13,10>'&|
    '<<tr><13,10>'&|
    '    <<td><13,10>'&|
    '    <<span class="SubHeading">Select Job Status<</span><13,10>'&|
    '    <</td><13,10>'&|
    '<</tr><13,10>'&|
    '<</table><13,10>'&|
    ''
BrowseStatusChangesFilter PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:StatusTypeFilter STRING(3)                             !Status Type Filter
tmp:JOB              STRING('JOB')                         !
tmp:EXC              STRING('EXC')                         !
tmp:2NE              STRING('2NE')                         !
tmp:LOA              STRING('LOA')                         !
FilesOpened     Long
AUDSTATS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BrowseStatusChangesFilter')
  loc:formname = 'BrowseStatusChangesFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BrowseStatusChangesFilter','')
    p_web._DivHeader('BrowseStatusChangesFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBrowseStatusChangesFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusChangesFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusChangesFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BrowseStatusChangesFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusChangesFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BrowseStatusChangesFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BrowseStatusChangesFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:StatusTypeFilter')
    tmp:StatusTypeFilter = p_web.GetValue('tmp:StatusTypeFilter')
    p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BrowseStatusChangesFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BrowseStatusChangesFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BrowseStatusChangesFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('BrowseStatusChangesFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do Title
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BrowseStatusChangesFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BrowseStatusChangesFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BrowseStatusChangesFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Status Changes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Status Changes',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusChangesFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusChangesFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BrowseStatusChangesFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusChangesFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BrowseStatusChangesFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('BrowseStatusChangesFilter_BrowseStatusChanges_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusChangesFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusChangesFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StatusTypeFilter
      do Value::tmp:StatusTypeFilter
      do Comment::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseStatusChanges
      do Comment::BrowseStatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusChangesFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Job Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StatusTypeFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('NewValue'))
    tmp:StatusTypeFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StatusTypeFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('Value'))
    tmp:StatusTypeFilter = p_web.GetValue('Value')
  End
  do Value::tmp:StatusTypeFilter
  do SendAlert
  do Value::BrowseStatusChanges  !1

Value::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusChangesFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:StatusTypeFilter
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:StatusTypeFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:StatusTypeFilter') = tmp:JOB
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatuschangesfilter_tmp:statustypefilter_value'',1,'''&clip(tmp:JOB)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:StatusTypeFilter',clip(tmp:JOB),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:StatusTypeFilter_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Job') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:StatusTypeFilter') = tmp:EXC
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatuschangesfilter_tmp:statustypefilter_value'',1,'''&clip(tmp:EXC)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:StatusTypeFilter',clip(tmp:EXC),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:StatusTypeFilter_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Exchange') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:StatusTypeFilter') = tmp:2NE
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatuschangesfilter_tmp:statustypefilter_value'',1,'''&clip(tmp:2NE)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:StatusTypeFilter',clip(tmp:2NE),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:StatusTypeFilter_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('2nd Exchange') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:StatusTypeFilter') = tmp:LOA
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatuschangesfilter_tmp:statustypefilter_value'',1,'''&clip(tmp:LOA)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:StatusTypeFilter',clip(tmp:LOA),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:StatusTypeFilter_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Loan') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BrowseStatusChangesFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value')

Comment::tmp:StatusTypeFilter  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusChangesFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseStatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseStatusChanges',p_web.GetValue('NewValue'))
    do Value::BrowseStatusChanges
  Else
    p_web.StoreValue('aus:RecordNumber')
  End

Value::BrowseStatusChanges  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseStatusChanges --
  p_web.SetValue('BrowseStatusChanges:NoForm',1)
  p_web.SetValue('BrowseStatusChanges:FormName',loc:formname)
  p_web.SetValue('BrowseStatusChanges:parentIs','Form')
  p_web.SetValue('_parentProc','BrowseStatusChangesFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('BrowseStatusChangesFilter_BrowseStatusChanges_embedded_div')&'"><!-- Net:BrowseStatusChanges --></div><13,10>'
    p_web._DivHeader('BrowseStatusChangesFilter_' & lower('BrowseStatusChanges') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('BrowseStatusChangesFilter_' & lower('BrowseStatusChanges') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseStatusChanges --><13,10>'
  end
  do SendPacket

Comment::BrowseStatusChanges  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusChangesFilter_' & p_web._nocolon('BrowseStatusChanges') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BrowseStatusChangesFilter_tmp:StatusTypeFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StatusTypeFilter
      else
        do Value::tmp:StatusTypeFilter
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BrowseStatusChangesFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusChangesFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BrowseStatusChangesFilter',0)

PreCopy  Routine
  p_web.SetValue('BrowseStatusChangesFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusChangesFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BrowseStatusChangesFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BrowseStatusChangesFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusChangesFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BrowseStatusChangesFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('BrowseStatusChangesFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusChangesFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BrowseStatusChangesFilter:Primed',0)
  p_web.setsessionvalue('showtab_BrowseStatusChangesFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BrowseStatusChangesFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BrowseStatusChangesFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BrowseStatusChangesFilter:Primed',0)
  p_web.StoreValue('tmp:StatusTypeFilter')
  p_web.StoreValue('')
Title  Routine
  packet = clip(packet) & |
    '<<table class="FormSubTitle"><13,10>'&|
    '<<tr><13,10>'&|
    '    <<td><13,10>'&|
    '    <<span class="SubHeading">Browse Status Change History<</span><13,10>'&|
    '    <</td><13,10>'&|
    '<</tr><13,10>'&|
    '<</table><13,10>'&|
    ''
BannerBrowseJobs     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('BannerBrowseJobs')
  loc:formname = 'BannerBrowseJobs_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerBrowseJobs','')
    p_web._DivHeader('BannerBrowseJobs',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerBrowseJobs',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerBrowseJobs',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerBrowseJobs',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerBrowseJobs',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerBrowseJobs',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerBrowseJobs',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerBrowseJobs_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BannerBrowseJobs_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerBrowseJobs')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerBrowseJobs_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerBrowseJobs_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerBrowseJobs_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BannerBrowseJobs" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BannerBrowseJobs" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BannerBrowseJobs" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BannerBrowseJobs">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BannerBrowseJobs">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BannerBrowseJobs')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BannerBrowseJobs')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BannerBrowseJobs'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BannerBrowseJobs')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BannerBrowseJobs_form:ready_',1)
  p_web.SetSessionValue('BannerBrowseJobs_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BannerBrowseJobs',0)

PreCopy  Routine
  p_web.SetValue('BannerBrowseJobs_form:ready_',1)
  p_web.SetSessionValue('BannerBrowseJobs_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerBrowseJobs',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BannerBrowseJobs_form:ready_',1)
  p_web.SetSessionValue('BannerBrowseJobs_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BannerBrowseJobs:Primed',0)

PreDelete       Routine
  p_web.SetValue('BannerBrowseJobs_form:ready_',1)
  p_web.SetSessionValue('BannerBrowseJobs_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BannerBrowseJobs:Primed',0)
  p_web.setsessionvalue('showtab_BannerBrowseJobs',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerBrowseJobs_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerBrowseJobs_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BannerBrowseJobs:Primed',0)
heading  Routine
  packet = clip(packet) & |
    '<<table class="FormCentre"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">Browse Jobs<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    ''
BannerMainMenu       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('BannerMainMenu')
  loc:formname = 'BannerMainMenu_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerMainMenu','')
    p_web._DivHeader('BannerMainMenu',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerMainMenu',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerMainMenu',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerMainMenu',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerMainMenu_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BannerMainMenu_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerMainMenu')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerMainMenu_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerMainMenu_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerMainMenu_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BannerMainMenu" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BannerMainMenu" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BannerMainMenu" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BannerMainMenu">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BannerMainMenu">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BannerMainMenu')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BannerMainMenu')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BannerMainMenu'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BannerMainMenu')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BannerMainMenu',0)

PreCopy  Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerMainMenu',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BannerMainMenu:Primed',0)

PreDelete       Routine
  p_web.SetValue('BannerMainMenu_form:ready_',1)
  p_web.SetSessionValue('BannerMainMenu_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BannerMainMenu:Primed',0)
  p_web.setsessionvalue('showtab_BannerMainMenu',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerMainMenu_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerMainMenu_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BannerMainMenu:Primed',0)
heading  Routine
  packet = clip(packet) & |
    '<<table class="TopBanner"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">Main Menu<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    ''
SetHubRepair         PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:SetHubRepair -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
FilesOpened     Long
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('SetHubRepair')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'SetHubRepair' & '_' & loc:parent
  Else
    loc:divname = 'SetHubRepair'
  End
    Do OpenFiles
    Do SaveFiles
    p_web.SetSessionValue('jobe:HubRepair',1)
    p_web.SetSessionValue('jobe:HubRepairDate',Today())
    p_web.SetSessionValue('jobe:HubRepairTime',Clock())
    Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
    rtd:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
    Set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
    Loop
        If Access:REPTYDEF.Next()
            Break
        End ! If Access:REPTYDEF.Next()
        If rtd:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
            Break
        End ! If rtd:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
        If rtd:BER = 11
            If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
                p_web.SetSessionValue('job:Repair_Type',rtd:Repair_Type)
            End ! If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
            If p_web.GetSessionValue('job:Warranty_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type_Warranty') = ''
                p_web.SetSessionValue('job:Repair_Type_Warranty',rtd:Repair_Type)
            End ! If p_web.GetSessionValue('job:Chargeable_Job') = 'YES' And p_web.GetSessionValue('job:Repair_Type') = ''
            Break
        End ! If rtd:BER = 11
    End ! Loop

    ! Lookup up current engineer and mark as "HUB" (DBH: 18/01/2008)
    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber = p_web.GetSessionValue('job:Ref_Number')
    joe:UserCode = p_web.GetSessionValue('job:Engineer')
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.Next()
            Break
        End ! If Access:JOBSENG.Next()
        If joe:JobNumber <> p_web.GetSessionValue('job:Ref_Number')
            Break
        End ! If joe:JobNumber <> p_web.GetSessionValue('job:Ref_Number')
        If joe:UserCode <> p_web.GetSessionValue('job:Engineer')
            Break
        End ! If joe:UserCode <> p_web.GetSessionValue('job:Engineer')
        If joe:DateAllocated > Today()
            Break
        End ! If joe:DateAllocated > Today()
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        Access:JOBSENG.TryUPdate()
        Break
    End ! Loop

    ! Check for SMS/Email Alerts (DBH: 18/01/2008)
    If p_web.GetSessionValue('jobe2:SMSNotification')
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('job:account_Number'),|
            '2ARC','SMS',p_web.GetSessionValue('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('wob:HeadAccountNumber'),|
            '2ARC','SMS',p_web.GetSessionValue('jobe2:SMSAlertNumber'),'',0,'')
        end !if (p_web.GSV('job:Who_Booked') = 'WEB')
    End ! If p_web.GetSessionValue('jobe2:SMSNotification')
    If p_web.GetSessionValue('jobe2:EmailNotification')
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('job:account_Number'),|
            '2ARC','EMAIL',p_web.GetSessionValue('jobe2:EMailAlertAddress'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GetSessionValue('job:Ref_Number'),p_web.GetSessionValue('wob:HeadAccountNumber'),|
            '2ARC','EMAIL',p_web.GetSessionValue('jobe2:EMailAlertAddress'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    End ! If p_web.GetSessionValue('jobe2:SMSNotification')

    Do RestoreFiles
    Do CloseFiles
  p_web._DivHeader(loc:divname,'adiv')
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web._DivFooter()
  if loc:parent
    p_web._RegisterDivEx(loc:divname,timer,'''_parentProc='&clip(loc:parent)&'''')
  else
    p_web._RegisterDivEx(loc:divname,timer)
  End
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
SaveFiles  ROUTINE
  JOBSENG::State = Access:JOBSENG.SaveFile()               ! Save File referenced in 'Other Files' so need to inform it's FileManager
  REPTYDEF::State = Access:REPTYDEF.SaveFile()             ! Save File referenced in 'Other Files' so need to inform it's FileManager
!--------------------------------------
RestoreFiles  ROUTINE
  IF JOBSENG::State <> 0
    Access:JOBSENG.RestoreFile(JOBSENG::State)             ! Restore File referenced in 'Other Files' so need to inform it's FileManager
  END
  IF REPTYDEF::State <> 0
    Access:REPTYDEF.RestoreFile(REPTYDEF::State)           ! Restore File referenced in 'Other Files' so need to inform it's FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
     FilesOpened = False
  END
IsThisModelAlternative PROCEDURE  (func:OriginalModel,func:NewModel) ! Declare Procedure
ESNMODEL::State  USHORT
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
    esn:Model_Number = func:OriginalModel
    Set(esn:Model_Number_Key,esn:Model_Number_Key)
    Loop
        If Access:ESNMODEL.NEXT()
           Break
        End !If
        If esn:Model_Number <> func:OriginalModel      |
            Then Break.  ! End If
        !Now check the alternative models to see if the
        !scanned model matches -  (DBH: 29-10-2003)
        Access:ESNMODAL.ClearKey(esa:RefModelNumberKey)
        esa:RefNumber   = esn:Record_Number
        esa:ModelNumber = func:NewModel
        If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Found
            Return# = 1
            Break
        Else !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Error
        End !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign

    End !Loop

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  ESNMODEL::State = Access:ESNMODEL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODEL::State <> 0
    Access:ESNMODEL.RestoreFile(ESNMODEL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODEL.Close
     Access:ESNMODAL.Close
     FilesOpened = False
  END
IsDateFormatInvalid  PROCEDURE  (f:Date)                   ! Declare Procedure
  CODE
    If Len(Clip(f:Date)) <> 10
        Return 1
    End ! If Len(Clip(f:Date)) <> 10
    If Sub(f:Date,3,1) <> '/'
        Return 1
    End ! If Sub(f:Date,3,1) <> '/'

    If Sub(f:Date,6,1) <> '/'
        Return 1
    End ! If Sub(f:date,6,1) <> '/'

    Return 0
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
BouncerHistory PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:Parts            STRING(25),DIM(15)                    !
tmp:IMEINumber       STRING(30)                            !IMEI Number
code_temp            BYTE                                  !
option_temp          BYTE                                  !
bar_code_string_temp CSTRING(21)                           !
bar_code_temp        CSTRING(21)                           !
barcodeJobNumber     STRING(20)                            !
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_Despatched)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                         PROJECT(jbn_ali:Invoice_Text)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(438,1135,7521,9740),PRE(rpt),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular),THOUS
                       HEADER,AT(396,469,7521,1000),USE(?unnamed)
                         STRING('Page:'),AT(6406,208),USE(?String35),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(6823,208),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
detail                 DETAIL,AT(,,,3042),USE(?detailband)
                         STRING('Job Number:'),AT(208,52,1094,313),USE(?String2),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s8),AT(1823,260),USE(job_ali:Ref_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Unit Details:'),AT(208,521,1094,313),USE(?String2:2),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Model Number:'),AT(1823,677),USE(?String6:3),FONT(,8,,FONT:bold),TRN
                         STRING('Booked:'),AT(5156,52),FONT(,8,,FONT:bold),TRN
                         STRING(@d6b),AT(6302,52),USE(job_ali:date_booked),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Date:'),AT(4531,52,1094,313),USE(?String2:4),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@D6b),AT(6302,208),USE(job_ali:Date_Completed),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(1823,52,2604,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI), |
  LEFT,COLOR(COLOR:White)
                         STRING(@D6b),AT(6302,365),USE(job_ali:Date_Despatched),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Despatched:'),AT(5156,365),USE(?String6:8),FONT(,8,,FONT:bold),TRN
                         STRING('Completed:'),AT(5156,208),USE(?String6:7),FONT(,8,,FONT:bold),TRN
                         STRING('Reported Fault:'),AT(208,1042,1094,313),USE(?String2:5),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         TEXT,AT(1823,1042,5521,417),USE(jbn_ali:Fault_Description),FONT(,8,,,CHARSET:ANSI),TRN
                         TEXT,AT(1823,1510,5521,417),USE(jbn_ali:Invoice_Text),FONT(,8,,,CHARSET:ANSI),TRN
                         STRING(@s16),AT(6250,521),USE(job_ali:ESN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Mobile Number:'),AT(5156,833),USE(?String6:6),FONT(,8,,FONT:bold),TRN
                         STRING('Repair Details:'),AT(208,1510,1094,313),USE(?String2:6),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s16),AT(6250,677),USE(job_ali:MSN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Parts Used:'),AT(208,1979,1094,313),USE(?String2:3),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Chargeable Parts'),AT(208,2240),USE(?String37),FONT(,8,,FONT:underline),TRN
                         STRING('Warranty Parts'),AT(3594,2240),USE(?String37:2),FONT(,8,,FONT:underline),TRN
                         STRING(@s25),AT(1927,2448),USE(tmp:Parts[2]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2604),USE(tmp:Parts[3]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(1927,2604),USE(tmp:Parts[4]),FONT(,8)
                         STRING(@s25),AT(1927,2760),USE(tmp:Parts[6]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2448),USE(tmp:Parts[7]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2448),USE(tmp:Parts[8]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2760),USE(tmp:Parts[5]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2604),USE(tmp:Parts[10]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2604),USE(tmp:Parts[9]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2760),USE(tmp:Parts[12]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2760),USE(tmp:Parts[11]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2448),USE(tmp:Parts[1]),FONT(,8,,,CHARSET:ANSI)
                         LINE,AT(208,2969,7031,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Unit Type:'),AT(1823,833),USE(?String6:4),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,677),USE(job_ali:Model_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('I.M.E.I. Number:'),AT(5156,521),USE(?String6:5),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer:'),AT(1823,521),USE(?String6:2),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,521),USE(job_ali:Manufacturer),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('M.S.N.:'),AT(5156,677),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,833),USE(job_ali:Unit_Type),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s16),AT(6250,833),USE(job_ali:Mobile_Number),FONT(,8),TRN
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('BOUNCER HISTORY REPORT'),AT(1260,52,5000,417),USE(?string20),FONT(,24,,FONT:bold), |
  CENTER,TRN
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BouncerHistory')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:IMEINumber = p_web.GSV('job:ESN')
  Relate:JOBS_ALIAS.SetOpenRelated()
  Relate:JOBS_ALIAS.Open                                   ! File JOBS_ALIAS used by this procedure, so make sure it's RelationManager is open
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('BouncerHistory',ProgressWindow)            ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:ESN)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(job_ali:ESN_Key)
  ThisReport.AddRange(job_ali:ESN,tmp:IMEINumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
  END
  IF SELF.Opened
    INIMgr.Update('BouncerHistory',ProgressWindow)         ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Clear(tmp:Parts)
  PartNumber# = 0
  Access:PARTS.Clearkey(par:Part_Number_Key)
  par:Ref_Number = job_ali:Ref_Number
  Set(par:Part_Number_Key,par:Part_Number_Key)
  Loop
      If Access:PARTS.Next()
          Break
      End ! If Access:PARTS.Next()
      If par:Ref_Number <> job_ali:Ref_Number
          Break
      End ! If par:Ref_Number <> job_ali:Ref_Number
      PartNumber# += 1
      If PartNumber# > 6
          Break
      End ! If PartNumber# > 6
      tmp:Parts[PartNumber#] = par:Description
  End ! Loop
  
  PartNumber# = 6
  Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
  wpr:Ref_Number = job_ali:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.Next()
          Break
      End ! If Access:WARPARTS.Next()
      If wpr:Ref_Number <> job_ali:Ref_Number
          Break
      End ! If wpr:Ref_Number <> job_ali:Ref_Number
      PartNumber# += 1
      If PartNumber# > 12
          Break
      End ! If PartNumber# > 12
      tmp:Parts[PartNumber#] = wpr:Description
  End ! Loop
  
  barcodeJobNumber = '*' & clip(job_ali:Ref_Number) & '*'
  
  !!Barcode Bit and setup refno
  !code_temp            = 3
  !option_temp          = 0
  !
  !bar_code_string_temp = Clip(job_ali:Ref_Number)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  !
  !Settarget(Report)
  !
  !Draw_JobNo.Blank(Color:White)
  !Draw_JobNo.FontName = 'C128 High 12pt LJ3'
  !Draw_JobNo.FontStyle = font:Regular
  !Draw_JobNo.FontSize = 12
  !Draw_JobNo.Show(0,0,Bar_Code_Temp)
  !Draw_JobNo.Display()
  !
  !
  !SetTarget()
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebApp01','BouncerHistory','BouncerHistory','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

PushPack             PROCEDURE  (NetWebServerWorker p_web, string p_packet) ! Declare Procedure
packetlen   long
  CODE
  packetlen = len(clip(p_packet))
  if packetlen > 0
    p_web.ParseHTML(p_packet, 1, packetlen, NET:NoHeader)
    p_packet = ''
    packetlen = 0
  end
RefreshPage          PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('RefreshPage')
  p_web.SetValue('_parentPage','RefreshPage')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody','window.open(NewJobBooking)','PageBodyDiv')
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
