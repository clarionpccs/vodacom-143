ren *.js *.jsb

\netweb\jsmin\jsmin < netmsie.jsb    > netmsie.js
\netweb\jsmin\jsmin < chrome.jsb     > chrome.js
\netweb\jsmin\jsmin < netweb.jsb     > netweb.js
\netweb\jsmin\jsmin < prototype.jsb  > prototype.js
\netweb\jsmin\jsmin < rico.jsb       > rico.js
\netweb\jsmin\jsmin < sorttable.jsb  > sorttable.js
\netweb\jsmin\jsmin < tabs.jsb       > tabs.js
\netweb\jsmin\jsmin < calendar2.jsb  > calendar2.js
\netweb\jsmin\jsmin < calendar6.jsb  > calendar6.js