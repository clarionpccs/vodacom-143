

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABWMFPAR.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3014.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3015.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseAccessoryNumber PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBACCNO)
                      Project(joa:RecordNumber)
                      Project(joa:AccessoryNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BrowseAccessoryNumber')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAccessoryNumber:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAccessoryNumber:NoForm')
      loc:FormName = p_web.GetValue('BrowseAccessoryNumber:FormName')
    else
      loc:FormName = 'BrowseAccessoryNumber_frm'
    End
    p_web.SSV('BrowseAccessoryNumber:NoForm',loc:NoForm)
    p_web.SSV('BrowseAccessoryNumber:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAccessoryNumber:NoForm')
    loc:FormName = p_web.GSV('BrowseAccessoryNumber:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAccessoryNumber') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAccessoryNumber')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBACCNO,joa:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOA:ACCESSORYNUMBER') then p_web.SetValue('BrowseAccessoryNumber_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAccessoryNumber:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAccessoryNumber:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseAccessoryNumber:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAccessoryNumber:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAccessoryNumber:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAccessoryNumber_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAccessoryNumber_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joa:AccessoryNumber)','-UPPER(joa:AccessoryNumber)')
    Loc:LocateField = 'joa:AccessoryNumber'
  of 2
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+joa:RecordNumber'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joa:AccessoryNumber')
    loc:SortHeader = p_web.Translate('Accessory Number')
    p_web.SetSessionValue('BrowseAccessoryNumber_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseAccessoryNumber:LookupFrom')
  End!Else
    loc:formaction = 'FormAccessoryNumbers'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAccessoryNumber:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAccessoryNumber:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAccessoryNumber:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBACCNO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joa:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseAccessoryNumber',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAccessoryNumber.locate(''Locator2BrowseAccessoryNumber'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAccessoryNumber.cl(''BrowseAccessoryNumber'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseAccessoryNumber_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseAccessoryNumber_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseAccessoryNumber','Accessory Number','Click here to sort by Accessory Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Accessory Number')&'">'&p_web.Translate('Accessory Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('Job:ViewOnly') <> 1) AND  true
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Delete
        do AddPacket
        loc:columns += 1
    End ! Selecting
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('joa:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBACCNO{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'joa:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joa:RecordNumber'),p_web.GetValue('joa:RecordNumber'),p_web.GetSessionValue('joa:RecordNumber'))
      loc:FilterWas = 'joa:RefNumber = ' & p_web.GSV('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAccessoryNumber_Filter')
    p_web.SetSessionValue('BrowseAccessoryNumber_FirstValue','')
    p_web.SetSessionValue('BrowseAccessoryNumber_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBACCNO,joa:RecordNumberKey,loc:PageRows,'BrowseAccessoryNumber',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBACCNO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBACCNO,loc:firstvalue)
              Reset(ThisView,JOBACCNO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBACCNO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBACCNO,loc:lastvalue)
            Reset(ThisView,JOBACCNO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joa:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAccessoryNumber.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAccessoryNumber.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAccessoryNumber.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAccessoryNumber.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 0
      if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseAccessoryNumber')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAccessoryNumber_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAccessoryNumber_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseAccessoryNumber',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAccessoryNumber.locate(''Locator1BrowseAccessoryNumber'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAccessoryNumber.cl(''BrowseAccessoryNumber'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseAccessoryNumber_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAccessoryNumber_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAccessoryNumber.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAccessoryNumber.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAccessoryNumber.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAccessoryNumber.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 0
    if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseAccessoryNumber')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = joa:RecordNumber
    p_web._thisrow = p_web._nocolon('joa:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAccessoryNumber:LookupField')) = joa:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((joa:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseAccessoryNumber.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBACCNO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBACCNO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBACCNO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBACCNO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joa:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseAccessoryNumber.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joa:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseAccessoryNumber.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joa:AccessoryNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Job:ViewOnly') <> 1) AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseAccessoryNumber.omv(this);" onMouseOut="BrowseAccessoryNumber.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseAccessoryNumber=new browseTable(''BrowseAccessoryNumber'','''&clip(loc:formname)&''','''&p_web._jsok('joa:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('joa:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormAccessoryNumbers'');<13,10>'&|
      'BrowseAccessoryNumber.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseAccessoryNumber.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAccessoryNumber')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAccessoryNumber')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAccessoryNumber')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAccessoryNumber')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBACCNO)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBACCNO)
  Bind(joa:Record)
  Clear(joa:Record)
  NetWebSetSessionPics(p_web,JOBACCNO)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('joa:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::joa:AccessoryNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joa:AccessoryNumber_'&joa:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joa:AccessoryNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Job:ViewOnly') <> 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Delete_'&joa:RecordNumber,,net:crc)
          If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseAccessoryNumber',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = joa:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('joa:RecordNumber',joa:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('joa:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joa:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joa:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
JobAccessories       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FoundAccessory   STRING(1000)                          !
tmp:TheAccessory     STRING(1000)                          !
tmp:ShowAccessory    STRING(1000)                          !
tmp:TheJobAccessory  STRING(1000)                          !
FilesOpened     Long
ACCESSOR::State  USHORT
JOBSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobAccessories')
  loc:formname = 'JobAccessories_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobAccessories','')
    p_web._DivHeader('JobAccessories',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobAccessories',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCESSOR)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCESSOR)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobAccessories_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ShowAccessory')
    tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  End
  if p_web.IfExistsValue('tmp:TheAccessory')
    tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  End
  if p_web.IfExistsValue('jobe:AccessoryNotes')
    jobe:AccessoryNotes = p_web.GetValue('jobe:AccessoryNotes')
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobAccessories_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
 tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobAccessories_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobAccessories_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobAccessories_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobAccessories" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobAccessories" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobAccessories" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Accessories') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Accessories',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobAccessories">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobAccessories" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobAccessories')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Accessory Numbers') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobAccessories')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobAccessories'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ShowAccessory')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobAccessories')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobAccessories_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShowAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShowAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TheAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TheAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonAddAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAddAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonRemoveAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonRemoveAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:AccessoryNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:AccessoryNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Accessory Numbers') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobAccessories_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessory Numbers')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseAccessoryNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::textAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textAccessoryMessage',p_web.GetValue('NewValue'))
    do Value::textAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textAccessoryMessage  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('textAccessoryMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one accessory',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::tmp:ShowAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ShowAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('NewValue'))
    tmp:ShowAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShowAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('Value'))
    tmp:ShowAccessory = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::tmp:TheAccessory  !1

Value::tmp:ShowAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ShowAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ShowAccessory'',''jobaccessories_tmp:showaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ShowAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ShowAccessory',loc:fieldclass,loc:readonly,20,180,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Available Accessories ---','',choose('' = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GSV('job:Model_Number')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      If acr:Model_Number <> p_web.GSV('job:Model_Number')
          Break
      End ! If acr:Model_Number <> p_web.GSV('tmp:ModelNumber')
      If Instring(';' & Clip(acr:Accessory),p_web.GSV('tmp:TheJobAccessory'),1,1)
          Cycle
      End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('tmp:TheJobAccessory'),1,1)
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.GSV('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_value')


Prompt::tmp:TheAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TheAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('NewValue'))
    tmp:TheAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TheAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('Value'))
    tmp:TheAccessory = p_web.GetValue('Value')
  End
  do SendAlert

Value::tmp:TheAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:TheAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TheAccessory'',''jobaccessories_tmp:theaccessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:TheAccessory',loc:fieldclass,loc:readonly,20,180,0,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Accessories On Job ---','',choose('' = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('tmp:TheJobAccessory') <> ''
          Loop x# = 1 To 1000
              If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
  
              If Start# > 0
                 If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                     tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                     If tmp:FoundAccessory <> ''
                         loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                         packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                         loc:even = Choose(loc:even=1,2,1)
                     End ! If tmp:FoundAccessory <> ''
                     Start# = 0
                 End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
             tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
             If tmp:FoundAccessory <> ''
                 loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                 packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                 loc:even = Choose(loc:even=1,2,1)
             End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('tmp:TheJobAccessory') <> ''
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_value')


Prompt::buttonAddAccessories  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Job:ViewOnly') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonAddAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAddAccessories',p_web.GetValue('NewValue'))
    do Value::buttonAddAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & p_web.GSV('tmp:ShowAccessory'))
  p_web.SSV('tmp:ShowAccessory','')
  do Value::buttonAddAccessories
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::buttonAddAccessories  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_value',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAddAccessories'',''jobaccessories_buttonaddaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddAccessories','Add Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_value')


Prompt::buttonRemoveAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Job:ViewOnly') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonRemoveAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRemoveAccessory',p_web.GetValue('NewValue'))
    do Value::buttonRemoveAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  tmp:TheJobAccessory = p_web.GSV('tmp:TheJobAccessory') & ';'
  tmp:TheAccessory = '|;' & Clip(p_web.GSV('tmp:TheAccessory')) & ';'
  Loop x# = 1 To 1000
      pos# = Instring(Clip(tmp:TheAccessory),tmp:TheJobAccessory,1,1)
      If pos# > 0
          tmp:TheJobAccessory = Sub(tmp:TheJobAccessory,1,pos# - 1) & Sub(tmp:TheJobAccessory,pos# + Len(Clip(tmp:TheAccessory)),1000)
          Break
      End ! If pos# > 0#
  End ! Loop x# = 1 To 1000
  p_web.SetSessionValue('tmp:TheJobAccessory',Sub(tmp:TheJobAccessory,1,Len(Clip(tmp:TheJobAccessory)) - 1))
  p_web.SetSessionValue('tmp:TheAccessory','')
  
  do Value::buttonRemoveAccessory
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::buttonRemoveAccessory  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_value',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAccessory'',''jobaccessories_buttonremoveaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAccessory','Remove Accessory','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_value')


Prompt::jobe:AccessoryNotes  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessory Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:AccessoryNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:AccessoryNotes',p_web.GetValue('NewValue'))
    jobe:AccessoryNotes = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:AccessoryNotes
    do Value::jobe:AccessoryNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:AccessoryNotes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe:AccessoryNotes = p_web.GetValue('Value')
  End
    jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  do Value::jobe:AccessoryNotes
  do SendAlert

Value::jobe:AccessoryNotes  Routine
  p_web._DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jobe:AccessoryNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Job:ViewOnly') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('jobe:AccessoryNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:AccessoryNotes'',''jobaccessories_jobe:accessorynotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jobe:AccessoryNotes',p_web.GetSessionValue('jobe:AccessoryNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jobe:AccessoryNotes),'Accessory Notes',Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_value')


Validate::BrowseAccessoryNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseAccessoryNumber',p_web.GetValue('NewValue'))
    do Value::BrowseAccessoryNumber
  Else
    p_web.StoreValue('joa:RecordNumber')
  End

Value::BrowseAccessoryNumber  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAccessoryNumber --
  p_web.SetValue('BrowseAccessoryNumber:NoForm',1)
  p_web.SetValue('BrowseAccessoryNumber:FormName',loc:formname)
  p_web.SetValue('BrowseAccessoryNumber:parentIs','Form')
  p_web.SetValue('_parentProc','JobAccessories')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&'"><!-- Net:BrowseAccessoryNumber --></div><13,10>'
    p_web._DivHeader('JobAccessories_' & lower('BrowseAccessoryNumber') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('JobAccessories_' & lower('BrowseAccessoryNumber') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAccessoryNumber --><13,10>'
  end
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobAccessories_tmp:ShowAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ShowAccessory
      else
        do Value::tmp:ShowAccessory
      end
  of lower('JobAccessories_tmp:TheAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TheAccessory
      else
        do Value::tmp:TheAccessory
      end
  of lower('JobAccessories_buttonAddAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAddAccessories
      else
        do Value::buttonAddAccessories
      end
  of lower('JobAccessories_buttonRemoveAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAccessory
      else
        do Value::buttonRemoveAccessory
      end
  of lower('JobAccessories_jobe:AccessoryNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:AccessoryNotes
      else
        do Value::jobe:AccessoryNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)

PreCopy  Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.setsessionvalue('showtab_JobAccessories',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
          p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ShowAccessory')
  p_web.StoreValue('tmp:TheAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:AccessoryNotes')
  p_web.StoreValue('')

VodacomSingleInvoice PROCEDURE(<NetWebServerWorker p_web>)
Default_Invoice_Company_Name_Temp STRING(30)
locInvoiceNumber     LONG
locInvoiceDate       DATE
save_tradeacc_id     USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_audit_id        USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:PrintA5Invoice   BYTE(0)
tmp:Ref_Number       STRING(20)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
RejectRecord         LONG,AUTO
save_par_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
customer_name_temp   STRING(60)
sub_total_temp       REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(30)
Name2                STRING(40)
Location             STRING(40)
RegistrationNo       STRING(30)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:CourierCost      REAL
tmp:InvoiceNumber    STRING(30)
InvoiceAddressGroup  GROUP,PRE(ia)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
DeliveryAddressGroup GROUP,PRE(da)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
                     END
locFranchiseAccountNumber STRING(20)
locDateIn            DATE
locDateOut           DATE
locTimeIn            TIME
locTimeOut           TIME
locMainOutFault      STRING(1000)
locAuditNotes        STRING(255)
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
Report               REPORT,AT(385,6646,7521,2260),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,2240,7521,6687),USE(?unnamed)
                         STRING(@s16),AT(6354,1615),USE(tmp:Ref_Number),FONT(,8),LEFT,TRN
                         STRING(@s10),AT(3906,1615),USE(wob:HeadAccountNumber),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2917,1615),USE(tmp:InvoiceNumber),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(5000,1615),USE(job:Order_Number),FONT(,8),LEFT,TRN
                         STRING(@s60),AT(156,52),USE(ia:CustomerName),FONT(,8),TRN
                         STRING(@s60),AT(4115,52),USE(da:CustomerName),FONT(,8),TRN
                         STRING(@d6b),AT(156,1615),USE(locInvoiceDate),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1458,2135,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(156,2135,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s16),AT(5000,2135,990,156),USE(job:ESN),FONT(,8),LEFT,TRN
                         STRING(@s16),AT(2917,2135),USE(job:MSN),FONT(,8),TRN
                         STRING('REPORTED FAULT'),AT(104,2552,1187),USE(?String64),FONT(,9,,FONT:bold),TRN
                         TEXT,AT(1625,2542,5313,656),USE(jbn:Fault_Description),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('ENGINEERS REPORT'),AT(83,3208,1354),USE(?String88),FONT(,9,,FONT:bold),TRN
                         STRING('PARTS USED'),AT(104,4219),USE(?String79),FONT(,9,,FONT:bold),TRN
                         STRING('Qty'),AT(1521,4219),USE(?String80),FONT(,8,,FONT:bold),TRN
                         STRING('Part Number'),AT(1917,4219),USE(?String81),FONT(,8,,FONT:bold),TRN
                         STRING('Description'),AT(3677,4219),USE(?String82),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(5719,4219),USE(?String83),FONT(,8,,FONT:bold),TRN
                         STRING('Line Cost'),AT(6677,4219),USE(?String89),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1354,4375,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,833),USE(ia:AddressLine4),FONT(,8),TRN
                         STRING('Tel: '),AT(4115,990),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,990),USE(da:TelephoneNumber),FONT(,8)
                         STRING('Vat No:'),AT(156,990),USE(?String34:3),FONT(,8),TRN
                         STRING(@s30),AT(625,990,1406,208),USE(ia:VatNumber),FONT(,8),LEFT,TRN
                         STRING('Tel:'),AT(2083,677),USE(?String34:2),FONT(,8),TRN
                         STRING(@s30),AT(2344,677,1406,208),USE(ia:TelephoneNumber),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2083,833),USE(?String35:2),FONT(,8),TRN
                         STRING(@s30),AT(2344,833,1094,208),USE(ia:FaxNumber),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(4115,208),USE(da:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(4115,365,1917,156),USE(da:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(4115,521),USE(da:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(4115,677),USE(da:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(4115,833),USE(da:AddressLine4),FONT(,8),TRN
                         STRING(@s30),AT(156,208),USE(ia:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(156,365),USE(ia:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(156,677,1885),USE(ia:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(156,521),USE(ia:AddressLine2),FONT(,8),TRN
                         TEXT,AT(1625,3208,5312,875),USE(locMainOutFault),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?BREAK1)
DETAIL                   DETAIL,AT(0,0,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(par:Quantity),FONT(,8),RIGHT,TRN
                           STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,8),TRN
                           STRING(@s25b),AT(3677,0),USE(par:Description),FONT(,8),TRN
                           STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),FONT(,8),RIGHT,TRN
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),FONT(,8),RIGHT,TRN
                         END
                       END
ANewPage               DETAIL,AT(0,0,7521,83),USE(?detailANewPage),PAGEAFTER(1)
                       END
                       FOOTER,AT(385,9104,7521,2406),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,104),USE(?String90),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,104),USE(tmp:LabourCost),FONT(,8),RIGHT,TRN
                         STRING('Time In:'),AT(1615,156),USE(?String90:3),FONT(,8),TRN
                         STRING(@d17b),AT(781,573),USE(locDateOut),FONT(,8),TRN
                         STRING('Other:'),AT(4844,417),USE(?String93),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,260),USE(tmp:PartsCost),FONT(,8),RIGHT,TRN
                         STRING(@t7b),AT(2240,573),USE(locTimeOut),FONT(,8),TRN
                         STRING(@t7b),AT(2240,156),USE(locTimeIn),FONT(,8),TRN
                         STRING('Date Out:'),AT(156,573),USE(?String90:4),FONT(,8),TRN
                         STRING('V.A.T.'),AT(4844,729),USE(?String94),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,417),USE(tmp:CourierCost),FONT(,8),RIGHT,TRN
                         STRING('Technician:'),AT(156,990),USE(?String90:6),FONT(,8),TRN
                         STRING(@s30),AT(833,990),USE(job:Engineer),FONT(,8),TRN
                         STRING('Time Out:'),AT(1615,573),USE(?String90:5),FONT(,8),TRN
                         STRING('Date In:'),AT(156,156),USE(?String90:2),FONT(,8),TRN
                         STRING('Total:'),AT(4844,990),USE(?String95),FONT(,8,,FONT:bold),TRN
                         LINE,AT(6302,937,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(6417,990),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Received By: '),AT(156,1510),USE(?String90:7),FONT(,10),TRN
                         STRING('Signature: '),AT(3021,1510),USE(?String90:8),FONT(,10),TRN
                         STRING('Date:'),AT(5885,1510),USE(?String90:9),FONT(,10),TRN
                         LINE,AT(990,1719,2031,0),USE(?Line9),COLOR(COLOR:Black)
                         LINE,AT(3646,1719,2240,0),USE(?Line9:2),COLOR(COLOR:Black)
                         LINE,AT(6198,1719,1250,0),USE(?Line9:3),COLOR(COLOR:Black)
                         STRING(@d17b),AT(781,156),USE(locDateIn),FONT(,8),TRN
                         LINE,AT(6302,573,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6396,573),USE(sub_total_temp),FONT(,8),RIGHT,TRN
                         STRING('Sub Total:'),AT(4844,573),USE(?String106),FONT(,8),TRN
                         STRING(@n14.2),AT(6396,729),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING('<128>'),AT(6344,990),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Parts:'),AT(4844,260),USE(?String92),FONT(,8),TRN
                       END
                       FORM,AT(385,406,7521,11198),USE(?unnamed:3),COLOR(COLOR:White)
                         BOX,AT(52,3125,7396,260),USE(?Box2),COLOR(COLOR:Black),FILL(00ECECECh),LINEWIDTH(1),ROUND
                         BOX,AT(52,3646,7396,260),USE(?Box2:3),COLOR(COLOR:Black),FILL(00ECECECh),LINEWIDTH(1),ROUND
                         STRING('TAX INVOICE'),AT(4167,0,3281),USE(?Text:Invoice),FONT(,18,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,417,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1250),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1406),USE(?String16),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(521,1406,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,260,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,573,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(625,729,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1406,313,208),USE(?String19),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1406,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1510,417,208),USE(?String19:2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(521,1510,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,833,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('INVOICE ADDRESS'),AT(104,1667,1229),USE(?String119),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(156,3698),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Date'),AT(156,3177),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1354,3125,0,1042),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Our Ref'),AT(1458,3177),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Invoice No'),AT(2917,3177),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Your Order No'),AT(5000,3177),USE(?String40:5),FONT(,8,,FONT:bold),TRN
                         STRING('Job No'),AT(6354,3177),USE(?String40:6),FONT(,8,,FONT:bold),TRN
                         STRING('Account No'),AT(3906,3177),USE(?String40:7),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,3385,7396,260),USE(?Box2:2),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(6250,3125,0,521),USE(?Line5:4),COLOR(COLOR:Black)
                         LINE,AT(2812,3125,0,1042),USE(?Line5:2),COLOR(COLOR:Black)
                         LINE,AT(3802,3125,0,521),USE(?Line5:8),COLOR(COLOR:Black)
                         BOX,AT(52,3906,7396,260),USE(?Box2:4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('Model'),AT(1458,3698),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Serial Number'),AT(2917,3698),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(5000,3698),USE(?String43),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,8698,2760,1302),USE(?Box4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(4688,8698,2760,1302),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(52,4219,7396,4323),USE(?Box3),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(4896,3125,0,1042),USE(?Line5:3),COLOR(COLOR:Black)
                         BOX,AT(52,1823,3438,1250),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(4010,1823,3438,1250),USE(?Box6),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('DELIVERY ADDRESS'),AT(4063,1667,1396),USE(?String119:2),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(4542,292,2917,1333),USE(de2:GlobalPrintText),FONT(,8),RIGHT,TRN
                         STRING('REG NO:'),AT(104,729,542),USE(?String16:2),FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White), |
  TRN
                         STRING('VAT NO:'),AT(104,833,542,156),USE(?String16:3),FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White), |
  TRN
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v7.30
! CW Template version    v7.3
! CW Version             7300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('VodacomSingleInvoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  locInvoiceNumber = p_web.GSV('job:Invoice_Number')
  
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBOUTFL.UseFile
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:AUDIT.UseFile
  Access:JOBSINV.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATLOG.UseFile
  DO BindFileFields
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    LocalReportTarget &= PDFReporter.IReportGenerator
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = locInvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
      
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Printing Details
        
        ! Is this a credit note?
        IF (p_web.GSV('CreditNoteCreated') = 1)
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'C'
            jov:Suffix = p_web.GSV('CreditSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
        !    PrintReport(2)
        !    Print(rpt:Detail)
            tmp:InvoiceNumber = 'CN' & Clip(tmp:InvoiceNumber) & p_web.GSV('CreditSuffix')
            Total_Temp = -jov:CreditAmount
            SetTarget(Report)
            ?Text:Invoice{prop:Text} = 'CREDIT NOTE'
            SetTarget()
            DO PrintParts
            Print(rpt:ANewPage)
        !    locDetail = ''
        
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'I'
            jov:Suffix = p_web.GSV('InvoiceSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
            tmp:InvoiceNumber = Clip(tmp:InvoiceNumber) & p_web.GSV('InvoiceSuffix')
            Total_Temp = jov:NewTotalCost
        !    PrintReport(1)
            DO PrintParts
        
        
        
        
        ELSE
        !    PrintReport(0)
            DO PrintParts
        END
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
    If Not p_web &= Null
      p_web.NoOp()
    End
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  SYSTEM{PROP:PrintMode} = 3
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Get Records
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('wob:RefNumber')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('jobe:RefNumber')
  IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
  END
  
  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
  jbn:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  SET(DEFAULT2,0) ! #12079 Pick up new "Global Text" (Bryan: 13/04/2011)
  Access:DEFAULT2.Next()
  ! Set Default Address
  ! Set Job Number / Invoice Number
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      tmp:Ref_Number = p_web.GSV('job:Ref_Number') & '-' & Clip(tra:BranchIdentification) & p_web.GSV('wob:JobNumber')
      tmp:InvoiceNumber = CLIP(p_web.GSV('job:Invoice_Number')) & '-' & Clip(tra:BranchIdentification)
  END
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  
      foundPrinted# = 0
      Access:AUDIT.Clearkey(aud:Action_Key)
      aud:Ref_Number = p_web.GSV('job:Ref_Number')
      aud:Action = 'INVOICE PRINTED'
      SET(aud:Action_Key,aud:Action_Key)
      LOOP UNTIL Access:AUDIT.Next()
          IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
              aud:Action <> 'INVOICE PRINTED')
              BREAK
          END
          IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & | 
              CLIP(tra:BranchIdentification),aud:Notes,1,1))
              foundPrinted# = 1
              BREAK
          END
      END
  
      If (foundPrinted# = 1)
          REPORT $ ?Text:Invoice{prop:Text} = 'COPY TAX INVOICE'
      END
          
      Do CreateARCRRCInvoices
  END
  
  ! Set Invoice  / Delivery Address
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
      END
  END
  CLEAR(InvoiceAddressGroup)
  
  IF (sub:OverrideHeadVATNo = 1)
      ia:VatNumber = sub:Vat_Number
  ELSE
      ia:VatNumber = tra:Vat_Number
  END
  
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = sub:Company_Name
          ia:AddressLine1 = sub:Address_Line1
          ia:AddressLine2 = sub:Address_Line2
          ia:AddressLine3 = sub:Address_Line3
          ia:AddressLine4 = sub:Postcode
          ia:TelephoneNumber = sub:Telephone_Number
          ia:FaxNumber = sub:Fax_Number
          ia:CustomerName = sub:Contact_Name
      END
      IF (ia:VatNumber = '')
          ia:VatNumber = p_web.GSV('jobe:VatNumber')
      END
  ELSE
      IF (tra:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = tra:Company_Name
          ia:AddressLine1 = tra:Address_Line1
          ia:AddressLine2 = tra:Address_Line2
          ia:AddressLine3 = tra:Address_Line3
          ia:AddressLine4 = tra:Postcode
          ia:TelephoneNumber = tra:Telephone_Number
          ia:FaxNumber = tra:Fax_Number
          ia:CustomerName = tra:Contact_Name
      END
  
  END
  IF (ia:CompanyName) = ''
      IF (p_web.GSV('job:Surname') <> '')
          IF (p_web.GSV('job:Title') = '')
              ia:CustomerName = p_web.GSV('job:Surname')
          ELSE
              IF (p_web.GSV('job:Initial') <> '')
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Initial')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              ELSE
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              END
          END
      END
  
      ia:CompanyName = p_web.GSV('job:Company_Name')
      ia:AddressLine1 = p_web.GSV('job:Address_Line1')
      ia:AddressLine2 = p_web.GSV('job:Address_Line2')
      ia:AddressLine3 = p_web.GSV('job:Address_Line3')
      ia:AddressLine4 = p_web.GSV('job:Postcode')
      ia:TelephoneNumber = p_web.GSV('job:Telephone_Number')
      ia:FaxNumber = p_web.GSV('job:Fax_Number')
  END
  
  da:CompanyName = p_web.GSV('job:Company_Name_Delivery')
  da:AddressLine1 = p_web.GSV('job:Address_Line1_Delivery')
  da:AddressLine2 = p_web.GSV('job:Address_Line2_Delivery')
  da:AddressLine3 = p_web.GSV('job:Address_Line3_Delivery')
  da:AddressLine4 = p_web.GSV('job:Postcode_Delivery')
  da:TelephoneNumber = p_web.GSV('job:Telephone_Delivery')
  
  IF (glo:WebJob <> 1)
      IF (p_web.GSV('jobe:WebJob') = 1)
          ! RRC to ARC job.
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
          IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
              da:CompanyName  = tra:Company_Name
              da:AddressLine1 = tra:Address_Line1
              da:AddressLine2 = tra:Address_Line2
              da:AddressLine3 = tra:Address_Line3
              da:AddressLine4 = tra:Postcode
              da:TelephoneNumber = tra:Telephone_Number
  
              ia:CompanyName  = tra:Company_Name
              ia:AddressLine1 = tra:Address_Line1
              ia:AddressLine2 = tra:Address_Line2
              ia:AddressLine3 = tra:Address_Line3
              ia:AddressLine4 = tra:Postcode
              ia:TelephoneNumber = tra:Telephone_Number
              ia:VatNumber = tra:Vat_Number
  
          END
      ELSE ! ! IF (p_web.GSV('jobe:WebJob = 1)
  
  
  
  
      END ! IF (p_web.GSV('jobe:WebJob = 1)
  
  END ! IF (glo:WebJob = 1)
  
  
  ! Lookups
  ! OutFault
  locMainOutFault = ''
  ! #12091 Display outfaults list like the old A5 invoice. (Bryan: 09/05/2011)
  line# = 0
  Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
  joo:JobNumber = job:Ref_Number
  Set(joo:JobNumberKey,joo:JobNumberKey)
  Loop ! Begin Loop
      If Access:JOBOUTFL.Next()
          Break
      End ! If Access:JOBOUTFL.Next()
      If joo:JobNumber <> job:Ref_Number
          Break
      End ! If joo:JobNumber <> job:Ref_Number
      If Instring('IMEI VALIDATION',joo:Description,1,1)
          Cycle
      End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
      line# += 1
      locMainOutFault = Clip(locMainoutfault) & '<13,10>' & Format(line#,@n2) & ' - ' & Clip(joo:Description)
  End ! Loop
  locMainOutFault = CLIP(Sub(locMainOutFault,3,1000))
  
  
  !Costs
  
  
  If (p_Web.GSV('BookingSite') = 'RRC')
      tmp:LabourCost = p_web.GSV('jobe:InvRRCCLabourCost')
      tmp:PartsCost = p_web.GSV('jobe:InvRRCCPartsCost')
      tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
  
      vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
          (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
          (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
      total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
          p_web.GSV('jobe:InvRRCCPartsCost') + |
          p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
  ELSE
      tmp:LabourCost = p_web.GSV('job:Invoice_Labour_Cost')
      tmp:PartsCost = p_web.GSV('job:Invoice_Parts_Cost')
      tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
      vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
          (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
          (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
      total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
          p_web.GSV('jobe:InvRRCCPartsCost') + |
          p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
  END
  Sub_Total_Temp = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
  
  
  ! Find Date In/Out
  
      locDateIn = job:Date_Booked
      locTimeIn = job:Time_Booked
      ! #11881 Use booking date unless RRC job AT ARC. Then use the "date received at ARC".
      ! (Note: Vodacom didn't specifically mention this, but i've assumed
      ! for VCP jobs to use the "received at RRC" date, rather than the booking date (Bryan: 16/03/2011)
      If (p_web.GSV('BookingSite') = 'RRC')
          IF (p_web.GSV('job:Who_Booked') = 'WEB')
              Access:AUDIT.Clearkey(aud:TypeActionKey)
              aud:Ref_Number = p_web.GSV('job:Ref_Number')
              aud:Type = 'JOB'
              aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
              SET(aud:TypeActionKey,aud:TypeActionKey)
              LOOP UNTIL Access:AUDIT.Next()
                  IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                      aud:Type <> 'JOB' OR |
                      aud:Action <> 'UNIT RECEIVED AT RRC FROM PUP')
                      BREAK
                  END
                  locDateIn = aud:Date
                  locTimeIn = aud:Time
                  BREAK
              END
  
          END ! IF (job:Who_Booked = 'WEB')
  
      ELSE ! If (glo:WebJob = 1)
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              foundDateIn# = 0
              Access:AUDSTATS.Clearkey(aus:DateChangedKey)
              aus:RefNumber = p_web.GSV('job:Ref_Number')
              aus:Type = 'JOB'
              SET(aus:DateChangedKey,aus:DateChangedKey)
              LOOP UNTIL Access:AUDSTATS.Next()
                  IF (aus:RefNumber <> p_web.GSV('job:Ref_Number') OR |
                      aus:TYPE <> 'JOB')
                      BREAK
                  END
                  IF (SUB(UPPER(aus:NewStatus),1,3) = '452')
                      locDateIn = aus:DateChanged
                      locTimeIn = aus:TimeChanged
                      foundDateIn# = 1
                      BREAK
                  END
              END
              IF (foundDateIn# = 0)
                  Access:LOCATLOG.Clearkey(lot:DateKey)
                  lot:RefNumber = p_web.GSV('job:Ref_Number')
                  SET(lot:DateKey,lot:DateKey)
                  LOOP UNTIL Access:LOCATLOG.Next()
                      IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                          BREAK
                      END
                      IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                          locDateIn = lot:TheDate
                          locTimeIn = lot:TheTime
                          BREAK
                      END
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  
  
      ! Use despatch to customer date (use last occurance), unless ARC to RRC job
      locDateOut = 0
      locTimeOut = 0
      Access:LOCATLOG.Clearkey(lot:DateKey)
      lot:RefNumber = p_web.GSV('job:Ref_Number')
      SET(lot:DateKey,lot:DateKey)
      LOOP UNTIL Access:LOCATLOG.Next()
          IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
              BREAK
          END
          IF (lot:NewLocation = Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
              locDateOut = lot:TheDate
              locTimeOut = lot:TheTime
  !            BREAK
          END
      END
  
      If (p_web.GSV('BookingSite') <> 'RRC')
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              Access:LOCATLOG.Clearkey(lot:DateKey)
              lot:RefNumber = p_web.GSV('job:Ref_Number')
              SET(lot:DateKey,lot:DateKey)
              LOOP UNTIL Access:LOCATLOG.Next()
                  IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                      BREAK
                  END
                  IF (lot:NewLocation = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')))
                      locDateOut = lot:TheDate
                      locTimeOut = lot:TheTime
                      BREAK
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  Report{PROPPRINT:Extend}=True
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='VodacomSingleInvoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  END
  LocalAttribute.Init(Report)
  Do SetStaticControlsAttributes
  



CreateARCRRCInvoices        Routine
    If (p_web.GSV('BookingSite') = 'RRC')
        IF (inv:RRCInvoiceDate = 0)
            ! RRC Invoice has not been created
            inv:RRCInvoiceDate = TODAY()
            IF (inv:ExportedRRCOracle = 0)
                p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
                p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
                p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
                p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
                p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                    
                END
                
                inv:ExportedRRCOracle = 1
                Access:INVOICE.TryUpdate()

                Line500_XML('RIV')
            END

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |                   
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                IF (p_web.GSV('LoanAttachedToJob') = 1)
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                END
                IF (p_web.GSV('jobe:Engineer48HourOption') = 1)
                    IF (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    END
                END
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)

                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web)
            END
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
            AddToAudit(p_web)
        ELSE


        END
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
        AddToAudit(p_web)
        locInvoiceDate = inv:RRCInvoiceDate
    ELSE ! If (glo:WebJob = 1)
        IF (inv:ARCInvoiceDate = 0)
            inv:ARCInvoiceDate = TODAY()
            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + |
                p_web.GSV('job:Invoice_Parts_Cost') + |
                p_web.GSV('job:Invoice_Courier_Cost'))                 
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            END
            
            

            inv:ExportedARCOracle = 1
            Access:INVOICe.TryUpdate()
            

            Line500_XML('AOW')

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                If (p_web.GSV('LoanAttachedToJob') = 1)
                    ! Changing (DBH 27/06/2006) #7584 - If loan attached, then invoice number is not being written to the audit trail
                    ! locAuditNotes   = '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost,@n14.2)
                    ! to (DBH 27/06/2006) #7584
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    ! End (DBH 27/06/2006) #7584

                End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
                If p_web.GSV('jobe:Engineer48HourOption') = 1
                    If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    End !If p_web.GSV('jobe:ExcReplcamentCharge
                End !If p_web.GSV('jobe:Engineer48HourOption = 1
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web)
            END
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
            AddToAudit(p_web)

        ELSE ! IF (inv:ARCInvoiceDate = 0)

        END ! IF (inv:ARCInvoiceDate = 0)
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
        AddToAudit(p_web)        
        locInvoiceDate = inv:ARCInvoiceDate
    END !If (glo:WebJob = 1)
PrintParts          ROUTINE
DATA
PartsAttached       BYTE(0)
CODE
    
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP UNTIL Access:PARTS.Next()
        IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        
        part_number_temp = par:Part_Number
        line_cost_temp = par:Quantity * par:Sale_Cost
        Print(rpt:Detail)
        PartsAttached = 1
    END
    IF (PartsAttached = 0)
        part_number_temp = 'No Parts Attached'
        Print(rpt:Detail)
    END
GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('locInvoiceNumber',locInvoiceNumber)
  BIND(aud:RECORD)
  BIND(aus:RECORD)
  BIND(de2:RECORD)
  BIND(def:RECORD)
  BIND(dep:RECORD)
  BIND(inv:RECORD)
  BIND(jac:RECORD)
  BIND(jbn:RECORD)
  BIND(joo:RECORD)
  BIND(jpt:RECORD)
  BIND(job:RECORD)
  BIND(jobe:RECORD)
  BIND(jov:RECORD)
  BIND(lot:RECORD)
  BIND(mfo:RECORD)
  BIND(maf:RECORD)
  BIND(par:RECORD)
  BIND(stt:RECORD)
  BIND(sub:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
  BIND(wob:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','SBOnline','VodacomSingleInvoice','VodacomSingleInvoice','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = False
  SELF.CompressImages = False
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
InvoiceNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locDetail            STRING(10000)                         !
locCompanyName       STRING(30)                            !
locAddressLine1      STRING(30)                            !
locAddressLine2      STRING(30)                            !
locAddressLine3      STRING(30)                            !
locPostcode          STRING(30)                            !
locTelephoneNumber   STRING(30)                            !
locCustCompanyName   STRING(30)                            !
locCustAddressLine1  STRING(30)                            !
locCustAddressLine2  STRING(30)                            !
locCustAddressLine3  STRING(30)                            !
locCustPostcode      STRING(30)                            !
locCustTelephoneNumber STRING(30)                          !
locCustVatNumber     STRING(30)                            !
locSubTotal          REAL                                  !
locVat               REAL                                  !
locTotal             REAL                                  !
locInvoiceTime       TIME                                  !
locInvoiceDate       DATE                                  !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(0,0,8430,8230),PRE(RPT),PAPER(PAPER:USER,8430,8230),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
Detail                 DETAIL,AT(0,0,8430,8230),USE(?Detail),ABSOLUTE
                         TEXT,AT(375,-10,7656,7800),USE(locDetail),FONT('Courier New',11)
                       END
ANewPage               DETAIL,AT(0,0,8427,83),USE(?ANewPage),PAGEAFTER(1)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

                    MAP
addToDetail             PROCEDURE(<STRING textString>)
newLine                 PROCEDURE(<LONG number>)
PrintReport             PROCEDURE(BYTE fCredit)
CheckCreateInvoiceRRC   PROCEDURE(BYTE fCredit)
CheckCreateInvoiceARC   PROCEDURE(BYTE fCredit)
                    END

Detail_Queue        Queue,Pre(detail)
Line                    String(56)
Filler1                 String(4)
Amount                  String(20)
                    End ! Detail_Queue        Queue,Pre(detail)

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CreatePartsList     Routine
    Data
local:FaultDescription  String(255)
    Code
        Free(Detail_Queue)
        Clear(Detail_Queue)

        local:FaultDescription = Clip(BHStripReplace(p_web.GSV('jbn:Fault_Description'),'<13,10>','. '))

        If Clip(local:FaultDescription) <> ''
            detail:Line = Clip(Sub(local:FaultDescription,1,55))
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = Clip(Sub(local:FaultDescription,56,55))
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End            
        Else ! If Clip(jbn:Fault_Description) <> ''
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End            
        End ! If Clip(jbn:Fault_Description) <> ''

        detail:Line = ALL('-',55)
        detail:Amount = ''
        Add(Detail_Queue)

    ! Show Parts (DBH: 27/07/2007)
        RowNum# = 0
        Access:PARTS.Clearkey(par:Order_Number_Key)
        par:Ref_Number = p_web.GSV('job:Ref_Number')
        Set(par:Order_Number_Key,par:Order_Number_Key)
        Loop ! Begin Loop
            If Access:PARTS.Next()
                Break
            End ! If Access:PARTS.Next()
            If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                Break
            End ! If par:Ref_Number <> job:Ref_Number
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(par:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

    ! Show Fault Codes (DBH: 27/07/2007)
        Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
        joo:JobNumber = p_web.GSV('job:Ref_Number')
        Set(joo:JobNumberKey,joo:JobNumberKey)
        Loop ! Begin Loop
            If Access:JOBOUTFL.Next()
                Break
            End ! If Access:JOBOUTFL.Next()
            If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                Break
            End ! If joo:JobNumber <> job:Ref_Number
            If Instring('IMEI VALIDATION',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(joo:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

!    ! I guess this is filling in the blank lines (DBH: 27/07/2007)
!        detail:Line = ''
!        detail:Amount = ''
!        Loop
!            If Records(Detail_Queue) % prn:DetailRows = 0
!                Break
!            End ! If Records(Detail_Queue) % prn:DetailRows = 0
!            Add(Detail_Queue)
!        End ! Loop

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Print A5 Invoice
  IF (GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI') <> 1)
      VodacomSingleInvoice(p_web)
      RETURN RequestCompleted
  END
  GlobalErrors.SetProcedureName('InvoiceNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  locJobNumber = p_web.GSV('job:Ref_Number')
  
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('InvoiceNote',ProgressWindow)               ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  IF SELF.Opened
    INIMgr.Update('InvoiceNote',ProgressWindow)            ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue

addToDetail         PROCEDURE(<STRING textString>)
    CODE
        locDetail = CLIP(locDetail) & textString
        
newLine             PROCEDURE(<LONG number>)
    CODE
        IF (number = 0)
            locDetail = CLIP(locDetail) & '<13,10>'
        ELSE
            LOOP xx# = 1 TO number
                locDetail = CLIP(locDetail) & '<13,10>'
            END
        END
        
CheckCreateInvoiceARC       Procedure(BYTE fCredit)
local:AuditNotes        String(255)
    Code
        If inv:ARCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
            inv:ARCInvoiceDate = Today()

            locInvoiceTime = Clock()

            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + p_web.GSV('job:Invoice_Parts_Cost') |
                + p_web.GSV('job:Invoice_Courier_Cost'))
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = p_web.GSV('jobe:RefNumber')
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.tryupdate()
            END
            

            inv:ExportedARCOracle = 1

            Access:INVOICE.Update()

            Line500_XML('AOW')

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
            local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            LoanAttachedToJob(p_web)
            
            If p_web.GSV('LoanAttachedToJob') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If LoanAttachedToJob(p_web.GSV('job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
            If (p_web.GSV('jobe:Engineer48HourOption') = 1)
                If (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                End ! If p_web.GSV('jobe:ExcReplcamentCharge
            End  ! If p_web.GSV('jobe:Engineer48HourOption = 1
            
           
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2) & |
                '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2) & |
                '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2) & |
                '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2) & |
                '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                    

            
            p_web.SSV('AddToAudit:Type','JOB') 
            p_web.SSV('AddToAudit:Action','INVOICE CREATED')
            p_web.SSV('AddToAudit:Notes',Clip(local:AuditNotes))
            AddToAudit(p_web)
        Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
        ! Normal Invoice Reprint (DBH: 27/07/2007)
            p_web.SSV('AddToAudit:Type','JOB') 
            p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06) )
            AddToAudit(p_web)            

            locInvoiceTime = p_web.GSV('job:Time_Booked')
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Type = 'JOB'
            aud:Action = 'INVOICE CREATED'
            aud:Date = inv:ARCInvoiceDate
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                    Break
                End ! If aud:Ref_Number <> p_web.GSV('job:Ref_Number
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type <> 'JOB'
                If aud:Action <> 'INVOICE CREATED'
                    Break
                End ! If aud:Action <> 'INVOICE CREATED'
                If aud:Date <> inv:ARCInvoiceDate
                    Break
                End ! If aud:Date <> inv:RRCInvoiceDate
                locInvoiceTime = aud:Time
                Break
            End ! Loop
        End ! If inv:RRCInvoiceDate = 0

        locVAT = (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
            (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100)
        locSubTotal = p_web.GSV('job:Invoice_Sub_Total')
        locTotal = locSubTotal + locVAT

CheckCreateInvoiceRRC       Procedure(BYTE fCredit)
local:AuditNotes                String(255)
Code
    If inv:RRCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
        inv:RRCInvoiceDate = Today()

        locInvoiceTime = Clock()

        p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
        p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
        p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('jobe:RefNumber')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = level:Benign)
            p_web.SessionQueueToFile(JOBSE)
            Access:JOBSE.TryUpdate()
        END
            
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('jobe2:RefNumber')
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE2)
            Access:JOBSE2.TryUpdate()
        END
            

        inv:ExportedRRCOracle = 1

        Access:INVOICE.Update()

!    If f:Suffix = '' And f:Prefix = ''
!            ! Don't export credit notes (DBH: 02/08/2007)
!        Line500_XML('RIV')
!    End ! If f:Suffix = '' And f:Prefix = ''

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
        local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            
        LoanAttachedToJob(p_web)
        If (p_web.GSV('LoanAttachedToJob') = 1)
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
        End ! If LoanAttachedToJob(job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
        If p_web.GSV('jobe:Engineer48HourOption') = 1
            If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If jobe:ExcReplcamentCharge
        End ! If jobe:Engineer48HourOption = 1

        local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2) & |
            '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2) & |
            '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2) & |
            '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2) & |
            '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
            
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Notes',CLIP(local:AuditNotes))
        p_web.SSV('AddToAudit:Action','INVOICE CREATED')
        AddToAudit(p_web)

    Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
            
        IF (fCredit = 2)
                
            ! Credit Note (DBH: 27/07/2007)
            p_web.SSV('AddToAudit:Notes','CREDIT NOTE: ' & Clip('CN') & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                'AMOUNT: ' & Format(jov:CreditAmount,@n14.2))
            p_web.SSV('AddToAudit:Action','CREDIT NOTE PRINTED')
            p_web.SSV('AddToAudit:Type','JOB')
            AddToAudit(p_web)
        Else ! If f:Prefix <> ''
            If (fCredit = 1)
                ! Credit Invoice (DBH: 27/07/2007)
                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                    'AMOUNT: ' & Format(jov:NewTotalCost,@n14.2))
                p_web.SSV('AddToAudit:Action','CREDITED INVOICE PRINTED')
                p_web.SSV('AddToAudit:Type','JOB')
                AddToAudit(p_web)                    
            Else ! If f:Suffix <> ''
                ! Normal Invoice Reprint (DBH: 27/07/2007)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                    '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
                p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
                AddToAudit(p_web)            

                locInvoiceTime = p_web.GSV('job:Time_Booked')
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:Ref_Number = p_web.GSV('job:Ref_Number')
                aud:Type = 'JOB'
                aud:Action = 'INVOICE CREATED'
                aud:Date = inv:RRCInvoiceDate
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop ! Begin Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If Access:AUDIT.Next()
                    If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                        Break
                    End ! If aud:Ref_Number <> job:Ref_Number
                    If aud:Type <> 'JOB'
                        Break
                    End ! If aud:Type <> 'JOB'
                    If aud:Action <> 'INVOICE CREATED'
                        Break
                    End ! If aud:Action <> 'INVOICE CREATED'
                    If aud:Date <> inv:RRCInvoiceDate
                        Break
                    End ! If aud:Date <> inv:RRCInvoiceDate
                    locInvoiceTime = aud:Time
                    Break
                End ! Loop


            End ! If f:Suffix <> ''
        End ! If f:Prefix <> ''
    End ! If inv:RRCInvoiceDate = 0

    locVAT = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
        (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
        (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
    locSubTotal = p_web.GSV('jobe:InvRRCCLabourCost') + | 
        p_web.GSV('jobe:InvRRCCPartsCost') + | 
        p_web.GSV('job:Invoice_Courier_Cost')
    locTotal = locSubTotal + locVAT

    IF (fCredit = 1)
        locTotal = jov:NewTotalCost
    END
    IF (fCredit = 2)
        locTotal = -jov:CreditAmount
    END
PrintReport         procedure(BYTE fCredit)
CODE
    
    
    newLine(7)

    IF (p_web.GSV('BookingSite') = 'ARC')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('ARC:AccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number
        
        locInvoiceDate = inv:ARCInvoiceDate
        CheckCreateInvoiceARC(fCredit)
    ELSE
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number
        
        locInvoiceDate = inv:RRCInvoiceDate
        CheckCreateInvoiceRRC(fCredit)
    END


    IF (p_web.GSV('jobe:WebJob') = 1)
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        IF (sub:OverrideHeadVATNo = TRUE)
            locCustVatNumber = sub:VAT_Number            
        ELSE
            locCustVatNumber = tra:VAT_Number
        END
        
        IF (sub:Invoice_Customer_Address = 'YES')
            IF (p_web.GSV('job:Company_Name') = '')
                locCustCompanyName = p_web.GSV('job:Initial') & ' ' & p_web.GSV('job:Initial') & | 
                    ' ' & p_web.GSV('job:Surname')
            ELSE
                locCustCompanyName = p_web.GSV('job:Company_Name')
            END
            locCustAddressLine1 = p_web.GSV('job:Address_Line1')
            locCustAddressLine2 = p_web.GSV('job:Address_Line2')
            locCustAddressLine3 = p_web.GSV('job:Address_Line3')
            locCustPostcode = p_web.GSV('job:Postcode')
            locCustTelephoneNumber = p_web.GSV('job:Telephone_Number')
        ELSE
            locCustCompanyName = sub:Company_Name
            locCustAddressLine1 = sub:Address_Line1
            locCustAddressLine2 = sub:Address_Line2
            locCustAddressLine3 = sub:Address_Line3
            locCustPostcode = sub:Postcode
            locCustTelephoneNumber = sub:Telephone_Number
        END
    END

        
    addToDetail(LEFT(locCustCOmpanyName,33) & locCompanyName)
    newLine()
    addToDetail(LEFT(locCustAddressLine1,33) & locAddressLine1)
    newLine()
    addToDetail(LEFT(locCustAddressLine2,33) & locAddressLine2)
    newLine()
    addToDetail(LEFT(locCustAddressLine3,33) & locAddressLine3)
    newLine()
    addToDetail(LEFT(locCustPostcode,33) & locPostcode)
    newLine()
    addToDetail(LEFT(locCustTelephoneNumber,33) & locTelephoneNumber)
    newLine()
    addToDetail('VAT NO.: ' & locCustVatNumber)
    newLine(2)
        
! Job Number
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
    END
    addToDetail(RIGHT('',33) & 'Job Number ' & p_web.GSV('job:Ref_Number') & | 
        '-' & tra:BranchIdentification & p_web.GSV('wob:JobNumber'))
    newLine(5)
        
! Order Line
        
    IF (fCredit = 1)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT(CLIP(inv:Invoice_Number) & p_web.GSV('InvoiceSuffix'),13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSIF (fCredit = 2)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT('CN' & CLIP(inv:Invoice_Number) & p_web.GSV('CreditSuffix'),13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSE
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT(inv:Invoice_Number,13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))

    END
    
        
    newLine(5)
        
! IMEI LIne
    addToDetail(LEFT(p_web.GSV('job:Manufacturer'),16) & | 
        LEFT(p_web.GSV('job:Model_Number'),17) & |
        LEFT(p_web.GSV('job:MSN'),25) & |
        LEFT(p_web.GSV('job:ESN'),20))
        
    newLine(2)
        
    Do CreatePartsList

    Loop x# = 1 To 14
        Get(Detail_Queue,x#)
        IF (ERROR())
            ERROR# = TRUE
        ELSE
            ERROR# = FALSE
        END
        detail:Amount = ''
        Case x#
        Of 1
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCLabourCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Labour_Cost'),@n-14.2))
            END
                
        Of 3
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCPartsCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Parts_Cost'),@n-14.2))
            END
                
        Of 5
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            END
                
        Of 10
            detail:Amount = (Format(locSubTotal,@n-14.2))
        Of 12
            detail:Amount = (Format(locVAT,@n-14.2))
        Of 14
            detail:Amount = (Format(locTotal,@n-14.2))
        End ! Case x#

        IF (ERROR#)
            detail:Line = '-'
            Add(Detail_Queue)
        ELSE
            Put(Detail_Queue)
        END
        
    End ! Loop x# = 1 To Records(Detail_Queue)

    Get(Detail_Queue,1)

    Loop x# = 1 To 15
        Get(Detail_Queue,x#)
        IF ~(ERROR())
            addToDetail(LEFT(detail:Line,68) & | 
                RIGHT(detail:Amount,10))
        END
        newLine()
    End ! Loop x# = 1 To Records(Detail_Queue)        
        
! Two Dates
    addToDetail(LEFT('',10) & |
        LEFT(FORMAT(p_web.GSV('job:Date_Booked'),@d06),32) & | 
        FORMAT(p_web.GSV('job:Time_Booked'),@t01))
    newLine(2)
    
    IF (fCredit <> 0)
        ! If credit use dates
        locInvoiceDate = jov:DateCreated
        locInvoiceTime = jov:TimeCreated
    END
    
    addToDetail(LEFT('',10) & | 
        LEFT(FORMAT(locInvoiceDate,@d06),32) & | 
        LEFT(FORMAT(locInvoiceTime,@t01),20) & | 
        p_web.GSV('job:Engineer'))
              


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Printing Details
      
  Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
  inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
  IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
  END
  
  ! Is this a credit note?
  IF (p_web.GSV('CreditNoteCreated') = 1)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'C'
      jov:Suffix = p_web.GSV('CreditSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(2)
      Print(rpt:Detail)
      Print(rpt:ANewPage)
      locDetail = ''
      
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'I'
      jov:Suffix = p_web.GSV('InvoiceSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(1)
      Print(rpt:Detail)
      
      
  
      
  ELSE
      PrintReport(0)
      Print(rpt:Detail)
  END
  
  
  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase2','InvoiceNote','InvoiceNote','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = False
  SELF.CompressImages = False
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

FormEngineeringOption PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locUserPassword      STRING(30)                            !
locEngineeringOption STRING(30)                            !
locExchangeManufacturer STRING(30)                         !
locExchangeModelNumber STRING(30)                          !
locExchangeNotes     STRING(255)                           !
locNewEngineeringOption STRING(20)                         !
locSplitJob          BYTE                                  !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
MODELNUM::State  USHORT
JOBS::State  USHORT
MANUFACT::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
STOMODEL::State  USHORT
AUDIT::State  USHORT
STOCKALL::State  USHORT
JOBSE::State  USHORT
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
WEBJOB::State  USHORT
EXCHOR48::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEngineeringOption')
  loc:formname = 'FormEngineeringOption_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEngineeringOption','Change')
    p_web._DivHeader('FormEngineeringOption',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
add:ExchangeOrderPart         Routine
data
local:foundUnit             Byte(0)
code
    !Check to see if a part already exists

    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            local:FoundUnit = True
            If local:FoundUnit
                wpr:Status  = 'ORD'
                Access:WARPARTS.Update()

                Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                stl:PartType    = 'WAR'
                stl:PartRecordNumber    = wpr:Record_Number
                if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Found
                    relate:STOCKALL.delete(0)
                else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Error
                end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                Break
            End !If local:FoundUnit
        End !Loop

    End !If job:Warranty_Job = 'YES'

    If local:FoundUnit = True
        If p_web.GSV('ob:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                local:FoundUnit = True
                If local:FoundUnit
                    par:Status  = 'ORD'
                    Access:PARTS.Update()
                    Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                    stl:PartType    = 'CHA'
                    stl:PartRecordNumber    = par:Record_Number
                    if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Found
                        relate:STOCKALL.delete(0)
                    else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    Break
                End !If local:FoundUnit
            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If FoundEXCHPart# = 0

    If local:FoundUnit = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If job:Engineer = ''

         !Found
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = 'ORD'
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = 0
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = 'ORD'
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = 0
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If FoundEXCHPart# = 0
create:ExchangeOrder        Routine
    if (Access:EXCHOR48.PrimeRecord() = Level:Benign)
        ex4:Location    = p_web.GSV('Default:SiteLocation')
        ex4:Manufacturer    = p_web.GSV('locExchangeManufacturer')
        ex4:ModelNumber    = p_web.GSV('locExchangeModelNumber')
        ex4:Received    = 0
        ex4:Notes    = p_web.GSV('locExchangeNotes')
        ex4:JobNumber    = p_web.GSV('wob:RefNumber')
                
        if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Inserted

            p_web.SSV('jobe:Engineer48HourOption',1)
            p_web.SSV('locEngineeringOption','48 Hour Exchange')

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','48 HOUR EXCHANGE ORDER CREATED')
            p_web.SSV('AddToAudit:Notes','MANUFACTURER: ' & p_web.GSV('locExchangeManufacturer') & |
                '<13,10>MODEL NUMBER: ' & p_web.GSV('locExchangeModelNumber'))
            AddToAudit(p_web)

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE')
            p_web.SSV('AddToAudit:Notes','')
            AddToAudit(p_web)


            p_web.SSV('GetStatus:StatusNumber',360)
            p_web.SSV('GetStatus:Type','EXC')

            GetStatus(p_web)

            p_web.SSV('GetStatus:StatusNumber',355)
            p_web.SSV('GetStatus:Type','JOB')

            GetStatus(p_web)

! #11939 Don't force split (even though original spec states you should) (Bryan: 01/03/2011)
!            !If warranty job, auto change to spilt chargeable - 3876 (DBH: 29-03-2004)
!            if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
!                p_web.SSV('job:Chargeable_Job','YES')
!                if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
!                else ! if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
!                end ! if p_web.GSV('BookingSite') = 'RRC'
!                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
!            end ! if ( p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
            
            CASE (p_web.GSV('locSplitJob'))
            OF 1 ! Chargeable Only
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','NO')
            OF 2 ! Warranty Only
                p_web.SSV('job:Chargeable_Job','NO')
                p_web.SSV('job:Warranty_Job','YES')
            OF 3 ! Split
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','YES')
            END

            IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
                else ! if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                end ! if p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
            END

            do add:ExchangeOrderPart

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber    = p_web.GSV('wob:RefNumber')
            if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.TryUpdate()
            else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Error
            end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = p_web.GSV('wob:RefNumber')
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Found
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Error
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Error
            Access:EXCHOR48.CancelAutoInc()
        end ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
    end ! if (Access:EXCHOR48.PrimeRecord() = Level:Benign)

    p_web.SSV('exchangeOrderCreated',1)
    p_web.SSV('ExchangeOrder:RecordNumber',ex4:RecordNumber)
    p_web.SSV('locWarningMessage','Exchange Order Created')

set:HubRepair      routine
    p_web.SSV('jobe:HubRepairDate',Today())
    p_web.SSV('jobe:HubRepairTime',Clock())

    p_web.SSV('GetStatus:StatusNumber',sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    p_web.SSV('GetStatus:Type','JOB')
    GetStatus(p_web)

    if (p_web.GSV('BookingSite') = 'RRC')
        if (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
            rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
            set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
            loop
                if (Access:REPTYDEF.Next())
                    Break
                end ! if (Access:REPTYDEF.Next())
                if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                    Break
                end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                if (rtd:BER = 11)
                    if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                        p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                        p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    break
                end ! if (rtd:BER = 11)
            end ! loop
        end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
    end ! if (p_web.GSV('BookingSite') = 'RRC')


    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber    = p_web.GSV('job:Ref_Number')
    joe:UserCode    = p_web.GSV('job:Engineer')
    joe:DateAllocated    = Today()
    set(joe:UserCodeKey,joe:UserCodeKey)
    loop
        if (Access:JOBSENG.Previous())
            Break
        end ! if (Access:JOBSENG.Next())
        if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
        if (joe:UserCode    <> p_web.GSV('job:Engineer'))
            Break
        end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
        if (joe:DateAllocated    > Today())
            Break
        end ! if (joe:DateAllocated    <> Today())
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        access:JOBSENG.update()
        break
    end ! loop


    if (p_web.GSV('jobe2:SMSNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
    if (p_web.GSV('jobe2:EmailNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
Validate:locEngineeringOption           Routine
    p_web.SSV('locErrorMessage','')
    case p_web.GSV('locNewEngineeringOption')
    of 'Standard Repair'
        if (p_web.GSV('BookingSite') = 'RRC')
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Found
                if (mod:ExcludedRRCRepair)
                    p_web.SSV('locErrorMessage','Warning! This model should not be repaired by an RRC and should be sent to the Hub.')
                    if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
                        p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
                        p_web.SSV('locValidationFailed',1)
                        p_web.SSV('locNewEngineeringOption','')
                        Exit
                    end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))

                end ! if (mod:ExcludedRRCRepair)
            else ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('BookingSite') = 'RRC')
    of '48 Hour Exchange'
        if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange has already been requested for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange Unit has already been requested and despatched for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
            ! #11939 Job is warranty only, show the "split job" option. (Bryan: 01/03/2011)
            p_web.SSV('locSplitJob',0)
            p_web.SSV('Hide:SplitJob',0)
        ELSE
            ! #11980 Job is chargeable so no need to show split option. (Bryan: 01/03/2011)
            IF (p_web.GSV('job:Warranty_Job') = 'YES')
                p_web.SSV('locSplitJob',3)
            ELSE
                p_web.SSV('locSplitJob',1)
            END
        END
        
    of 'ARC Repair'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    of '7 Day TAT'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    end ! case p_web.GSV('locEngineeringOption')
Validate:locExchangeModelNumber           Routine
    if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! The selected unit is not an "alternative" Model Number for this job!')                        
        else ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! You have selected a different Model Number for this job!')
        end ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
    else ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locWarningMessage','')
    end ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))

OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(EXCHOR48)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(EXCHOR48)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !Init Form
  if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('locValidationFailed',0)
      p_web.SSV('locUserPassword','')
      p_web.SSV('locErrorMessage','')
      p_web.SSV('locWarningMessage','')
      p_web.SSV('Comment:UserPassword','Enter User Password and press [TAB]')
      p_web.SSV('filter:Manufacturer','')
      p_web.SSV('locNewEngineeringOption','')
      p_web.SSV('locExchangeManufacturer',p_web.GSV('job:Manufacturer'))
      p_web.SSV('locExchangeModelNumber',p_web.GSV('job:Model_number'))
      p_web.SSV('locExchangeNotes','')
      p_web.SSV('locSplitJob',0)
      p_web.SSV('Hide:SplitJob',1)
  
      p_web.SSV('exchangeOrderCreated',0)
  
      p_web.SSV('FormEngineeringOption:FirstTime',0)
  
      ! Activate the 48 Hour option and show the booking option (DBH: 24-03-2005)
      p_web.SSV('hide:48HourOption',0)
      If AccountActivate48Hour(p_web.GSV('wob:HeadAccountNumber')) = True
          If Allow48Hour(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),p_web.GSV('wob:HeadAccountNumber')) = True
  
          Else ! If Allow48Hour(job:ESN,job:Model_Number) = True
              p_web.SSV('hide:48HourOption',1)
          End ! If Allow48Hour(job:ESN,job:Model_Number) = True
      Else ! AccountActivate48Hour(wob:HeadAccountNumber) = True
          p_web.SSV('hide:48HourOption',1)
      End ! AccountActivate48Hour(wob:HeadAccountNumber) = True
  
  end ! if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
  p_web.SetValue('FormEngineeringOption_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:DOP',p_web.site.DatePicture)
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locExchangeManufacturer'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      p_web.SSV('filter:ModelNumber','Upper(mod:Manufacturer) = Upper(<39>' & clip(man:Manufacturer) & '<39>)')
      
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
  Of 'locExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
      do Validate:locExchangeModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locUserPassword',locUserPassword)
  p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  p_web.SetSessionValue('job:DOP',job:DOP)
  p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  p_web.SetSessionValue('locSplitJob',locSplitJob)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locUserPassword')
    locUserPassword = p_web.GetValue('locUserPassword')
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  End
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  End
  if p_web.IfExistsValue('locNewEngineeringOption')
    locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:DOP',job:DOP)
  End
  if p_web.IfExistsValue('locExchangeManufacturer')
    locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  End
  if p_web.IfExistsValue('locExchangeModelNumber')
    locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  End
  if p_web.IfExistsValue('locExchangeNotes')
    locExchangeNotes = p_web.GetValue('locExchangeNotes')
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  End
  if p_web.IfExistsValue('locSplitJob')
    locSplitJob = p_web.GetValue('locSplitJob')
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormEngineeringOption_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locUserPassword = p_web.RestoreValue('locUserPassword')
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
 locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
 locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
 locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
 locSplitJob = p_web.RestoreValue('locSplitJob')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEngineeringOption_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEngineeringOption_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEngineeringOption_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormEngineeringOption" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormEngineeringOption" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormEngineeringOption" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Change Engineering Option') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Change Engineering Option',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormEngineeringOption">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormEngineeringOption" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormEngineeringOption')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Engineering Option') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineeringOption')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormEngineeringOption'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locUserPassword')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineeringOption')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm User') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUserPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Engineering Option') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorMessage
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:ExchangeOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:ExchangeOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarningMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarningMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSplitJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSplitJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSplitJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:PrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:PrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locUserPassword  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Enter Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUserPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUserPassword',p_web.GetValue('NewValue'))
    locUserPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUserPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUserPassword',p_web.GetValue('Value'))
    locUserPassword = p_web.GetValue('Value')
  End
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('Comment:UserPassword','User Does Not Have Access')
  
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('locUserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'AMEND ENGINEERING OPTION'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              p_web.SSV('locPasswordValidated',1)
              p_web.SSV('Comment:UserPassword','')
              p_web.SSV('headingExchangeOrder','Exchange Order')
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locUserPassword
  do SendAlert
  do Prompt::locNewEngineeringOption
  do Value::locNewEngineeringOption  !1
  do Comment::locNewEngineeringOption
  do Comment::locUserPassword
  do Prompt::locEngineeringOption
  do Value::locEngineeringOption  !1
  do Comment::locEngineeringOption

Value::locUserPassword  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locUserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locUserPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locUserPassword'',''formengineeringoption_locuserpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locUserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locUserPassword',p_web.GetSessionValueFormat('locUserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value')

Comment::locUserPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment')

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate(p_web.GSV('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Option')
  If p_web.GSV('locPasswordValidated') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt')

Validate::locEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('NewValue'))
    locEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('Value'))
    locEngineeringOption = p_web.GetValue('Value')
  End

Value::locEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value')

Comment::locEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment')

Prompt::locNewEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Engineering Option')
  If p_web.GSV('locPasswordValidated') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt')

Validate::locNewEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewEngineeringOption',p_web.GetValue('NewValue'))
    locNewEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewEngineeringOption',p_web.GetValue('Value'))
    locNewEngineeringOption = p_web.GetValue('Value')
  End
  If locNewEngineeringOption = ''
    loc:Invalid = 'locNewEngineeringOption'
    loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do validate:locEngineeringOption
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<</DIV>',''))
  
  
  do Value::locNewEngineeringOption
  do SendAlert
  do Value::locErrorMessage  !1
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Prompt::locExchangeManufacturer
  do Value::locExchangeManufacturer  !1
  do Comment::locExchangeManufacturer
  do Prompt::locExchangeModelNumber
  do Value::locExchangeModelNumber  !1
  do Comment::locExchangeModelNumber
  do Prompt::locExchangeNotes
  do Value::locExchangeNotes  !1
  do Comment::locExchangeNotes
  do Value::text:ExchangeOrder  !1
  do Value::button:CreateOrder  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

Value::locNewEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewEngineeringOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewEngineeringOption = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewEngineeringOption'',''formengineeringoption_locnewengineeringoption_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewEngineeringOption')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNewEngineeringOption',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'Standard Repair'
    packet = clip(packet) & p_web.CreateOption('Standard Repair','Standard Repair',choose('Standard Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '48 Hour Exchange'  and p_web.GSV('hide:48HourOption') <> 1
    packet = clip(packet) & p_web.CreateOption('48 Hour Exchange','48 Hour Exchange',choose('48 Hour Exchange' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'ARC Repair'
    packet = clip(packet) & p_web.CreateOption('ARC Repair','ARC Repair',choose('ARC Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '7 Day TAT'
    packet = clip(packet) & p_web.CreateOption('7 Day TAT','7 Day TAT',choose('7 Day TAT' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value')

Comment::locNewEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment')

Validate::text:ExchangeOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:ExchangeOrder',p_web.GetValue('NewValue'))
    do Value::text:ExchangeOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:ExchangeOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Exchange Order',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value')

Comment::text:ExchangeOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:DOP  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Of Purchase')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt')

Validate::job:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture))
    job:DOP = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do Value::job:DOP
  do SendAlert

Value::job:DOP  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DATE --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:lookuponly = ''
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.site.datepicture,loc:javascript,,) & '<13,10>'
  if not loc:readonly
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' '&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:DateLookupButton,loc:formname,,,|
    'sd('''&clip(loc:formName)&''','''&p_web._nocolon('job:DOP')&''','''&clip(upper(p_web.site.datepicture))&''','&lower(p_web._nocolon('''FormEngineeringOption_job:DOP_value'''))&');','onfocus="rad();" ')

  End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value')

Comment::job:DOP  Routine
    loc:comment = p_web._DateFormat(p_web.site.DatePicture)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment')

Prompt::locExchangeManufacturer  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manufacturer')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt')

Validate::locExchangeManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeManufacturer',p_web.GetValue('NewValue'))
    locExchangeManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeManufacturer',p_web.GetValue('Value'))
    locExchangeManufacturer = p_web.GetValue('Value')
  End
  If locExchangeManufacturer = ''
    loc:Invalid = 'locExchangeManufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locExchangeManufacturer')
  do AfterLookup
  do Value::locExchangeManufacturer
  do SendAlert
  do Comment::locExchangeManufacturer

Value::locExchangeManufacturer  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('exchangeOrderCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locExchangeManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locExchangeManufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeManufacturer'',''formengineeringoption_locexchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeManufacturer',p_web.GetSessionValueFormat('locExchangeManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=locExchangeManufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=FormEngineeringOption&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value')

Comment::locExchangeManufacturer  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment')

Prompt::locExchangeModelNumber  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt')

Validate::locExchangeModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeModelNumber',p_web.GetValue('NewValue'))
    locExchangeModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeModelNumber',p_web.GetValue('Value'))
    locExchangeModelNumber = p_web.GetValue('Value')
  End
  If locExchangeModelNumber = ''
    loc:Invalid = 'locExchangeModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locExchangeModelNumber')
  do AfterLookup
  do Value::locExchangeModelNumber
  do SendAlert
  do Comment::locExchangeModelNumber
  do Value::locWarningMessage  !1

Value::locExchangeModelNumber  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('exchangeOrderCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locExchangeModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locExchangeModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeModelNumber'',''formengineeringoption_locexchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeModelNumber',p_web.GetSessionValueFormat('locExchangeModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=locExchangeModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort&LookupFrom=FormEngineeringOption&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value')

Comment::locExchangeModelNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment')

Prompt::locExchangeNotes  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Notes')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt')

Validate::locExchangeNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeNotes',p_web.GetValue('NewValue'))
    locExchangeNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeNotes',p_web.GetValue('Value'))
    locExchangeNotes = p_web.GetValue('Value')
  End
    locExchangeNotes = Upper(locExchangeNotes)
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  do Value::locExchangeNotes
  do SendAlert

Value::locExchangeNotes  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- TEXT --- locExchangeNotes
  loc:fieldclass = Choose(sub('TextEntry',1,1) = ' ',clip('FormEntry') & 'TextEntry','TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locExchangeNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeNotes'',''formengineeringoption_locexchangenotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeNotes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locExchangeNotes',p_web.GetSessionValue('locExchangeNotes'),3,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locExchangeNotes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value')

Comment::locExchangeNotes  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment')

Validate::locWarningMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarningMessage',p_web.GetValue('NewValue'))
    do Value::locWarningMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locWarningMessage  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locWarningMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value')

Comment::locWarningMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSplitJob  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Select Job Type')
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt')

Validate::locSplitJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSplitJob',p_web.GetValue('NewValue'))
    locSplitJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSplitJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSplitJob',p_web.GetValue('Value'))
    locSplitJob = p_web.GetValue('Value')
  End
  do Value::locSplitJob
  do SendAlert
  do Value::button:CreateOrder  !1

Value::locSplitJob  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
  ! --- RADIO --- locSplitJob
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSplitJob')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Warranty Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Split Warranty/Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value')

Comment::locSplitJob  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_comment',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:CreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:CreateOrder',p_web.GetValue('NewValue'))
    do Value::button:CreateOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('locExchangeManufacturer') <> '' and p_web.GSV('locExchangeModelNumber') <> '')
      do create:ExchangeOrder
  end ! if (p_web.GSV('locExchangeManufacturer') <> '' and |
  do Value::button:CreateOrder
  do SendAlert
  do Value::button:PrintOrder  !1
  do Value::locExchangeManufacturer  !1
  do Value::locExchangeModelNumber  !1
  do Value::locExchangeNotes  !1
  do Value::locWarningMessage  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

Value::button:CreateOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value',Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:CreateOrder'',''formengineeringoption_button:createorder_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateExchangeOrder','Create Exch. Order','DoubleButton',loc:formname,,,,loc:javascript,0,'images\star.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value')

Comment::button:CreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_comment',Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:PrintOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:PrintOrder',p_web.GetValue('NewValue'))
    do Value::button:PrintOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:PrintOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value',Choose(p_web.GSV('exchangeOrderCreated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintOrder','Print Order','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ExchangeOrder')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images\printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value')

Comment::button:PrintOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_comment',Choose(p_web.GSV('exchangeOrderCreated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('exchangeOrderCreated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormEngineeringOption_locUserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locUserPassword
      else
        do Value::locUserPassword
      end
  of lower('FormEngineeringOption_locNewEngineeringOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewEngineeringOption
      else
        do Value::locNewEngineeringOption
      end
  of lower('FormEngineeringOption_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormEngineeringOption_locExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeManufacturer
      else
        do Value::locExchangeManufacturer
      end
  of lower('FormEngineeringOption_locExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeModelNumber
      else
        do Value::locExchangeModelNumber
      end
  of lower('FormEngineeringOption_locExchangeNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeNotes
      else
        do Value::locExchangeNotes
      end
  of lower('FormEngineeringOption_locSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSplitJob
      else
        do Value::locSplitJob
      end
  of lower('FormEngineeringOption_button:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:CreateOrder
      else
        do Value::button:CreateOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)

PreCopy  Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
      ! Validate Form
      if (p_web.GSV('locPasswordValidated') = 0)
          loc:Invalid = 'locUserPassword'
          loc:Alert = 'Your password has not yet been validated'
          p_web.SSV('locUserPassword','')
          exit
      end ! if (p_web.GSV('local:PasswordValidated') = 0)
  
      if (p_web.GSV('locValidationFailed') = 1)
          loc:Invalid = 'locUserPassword'
          loc:Alert = p_web.GSV('locErrorMessage')
          exit
      end ! if (p_web.GSV('locValidationFailed') = 1)
  
  
  
      if ((p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption')) and p_web.GSV('locNewEngineeringOption') <> '')
          p_web.SSV('locEngineeringOption',p_web.GSV('locNewEngineeringOption'))
  
          case p_web.GSV('locNewEngineeringOption')
          of 'ARC Repair'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: ARC REPAIR')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',2)
  
              
          of '7 Day TAT'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 7 DAY TAT')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',3)
              
          of 'Standard Repair'
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: STANDARD REPAIR')
              p_web.SSV('AddToAudit:Notes','')
              AddToAudit(p_web)
              p_web.SSV('jobe:Engineer48HourOption',4)
          end !case p_web.GSV('locNewEngineeringOption')
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE)
              Access:JOBSE.tryUpdate()
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              Access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber = p_web.GSV('wob:RefNumber')
          if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              Access:WEBJOB.TryUpdate()
          end ! if (Access:WEBJOB.TryFetch(wob:Ref_Number_Key) = Level:Benign)
          
  
          p_web.SSV('FirstTime',1)
  
  !
  !        p_web.SSV('locMessage','Your changes have been saved')
  !        p_web.SSV('loc:Alert','Your changes have been saved')
  
      end ! if (p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption'))
  
  
  
      

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
      If not (p_web.GSV('locPasswordValidated') = 0)
        If locNewEngineeringOption = ''
          loc:Invalid = 'locNewEngineeringOption'
          loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If locExchangeManufacturer = ''
          loc:Invalid = 'locExchangeManufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If locExchangeModelNumber = ''
          loc:Invalid = 'locExchangeModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          locExchangeNotes = Upper(locExchangeNotes)
          p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.StoreValue('locUserPassword')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('locNewEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('locExchangeManufacturer')
  p_web.StoreValue('locExchangeModelNumber')
  p_web.StoreValue('locExchangeNotes')
  p_web.StoreValue('')
  p_web.StoreValue('locSplitJob')
  p_web.StoreValue('')
  p_web.StoreValue('')
Is48HourOrderProcessed PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 1
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 1      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
Is48HourOrderCreated PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 0
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 0      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#

    
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Report
!!! Report the EXCHOR48 File
!!! </summary>
ExchangeOrder PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locRecordNumber      LONG                                  !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
address:Telephone_Number STRING(20)                        !
address:Fax_No       STRING(20)                            !
address:Email        STRING(50)                            !
tmp:DOP              DATE                                  !
Process:View         VIEW(EXCHOR48)
                       PROJECT(ex4:JobNumber)
                       PROJECT(ex4:Manufacturer)
                       PROJECT(ex4:ModelNumber)
                       PROJECT(ex4:RecordNumber)
                     END
ProgressWindow       WINDOW('Report EXCHOR48'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(385,2865,7521,7344),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(52,833),USE(address:Post_Code),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Date Printed:'),AT(4938,948),USE(?String16),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Email:'),AT(52,1302),USE(?String33),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s50),AT(365,1302),USE(address:Email),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(5677,938),USE(?ReportDateStamp),FONT('Tahoma',8,,FONT:bold), |
  TRN
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Fax:'),AT(52,1146),USE(?String34),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Tel:'),AT(52,990),USE(?String32),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,0),USE(Address:User_Name),FONT('Tahoma',14,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                       END
detail1                DETAIL,AT(,,,167),USE(?unnamed:4)
                         STRING(@s30),AT(104,0),USE(ex4:Manufacturer),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2135,0,,208),USE(ex4:ModelNumber),FONT('Tahoma',8),TRN
                         STRING('1'),AT(4427,0),USE(?String30),FONT(,8),TRN
                         STRING(@s15),AT(4844,0),USE(ex4:JobNumber),FONT(,8),TRN
                         STRING(@d17b),AT(6458,0),USE(tmp:DOP),FONT(,8),RIGHT,TRN
                       END
                       FOOTER,AT(396,10833,7521,302),USE(?unnamed:3)
                         LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Quantity Ordered:  1'),AT(104,104),USE(?String29),FONT('Tahoma',8,,FONT:bold, |
  CHARSET:ANSI),TRN
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         BOX,AT(104,2292,7344,7448),USE(?Box2),COLOR(COLOR:Black),ROUND
                         BOX,AT(104,1875,7344,365),USE(?Box1),COLOR(COLOR:Black),FILL(00C2E3F3h),ROUND
                         BOX,AT(4844,365,2552,1406),USE(?Box3),COLOR(COLOR:Black),ROUND
                         STRING('EXCHANGES ORDERED'),AT(4740,52),USE(?String3),FONT('Tahoma',16,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Quantity'),AT(4198,1979),USE(?String5),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Job Number'),AT(4875,1979),USE(?JobNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Activation Date'),AT(6458,1979),USE(?ActivationDate),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer'),AT(156,1979),USE(?String6),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Model Number'),AT(2167,1979),USE(?String31),FONT('Tahoma',8,,FONT:bold),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeOrder')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:EXCHOR48.Open                                     ! File EXCHOR48 used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = p_web.GSV('BookingAccount')
  IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
     !Found
     address:User_Name = tra:company_name
     address:Address_Line1 = tra:address_line1
     address:Address_Line2 = tra:address_line2
     address:Address_Line3 = tra:address_line3
     address:Post_code = tra:postcode
     address:Telephone_Number = tra:telephone_number
     address:Fax_No = tra:Fax_Number
     address:Email = tra:EmailAddress
  ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locRecordNumber = p_web.GSV('ExchangeOrder:RecordNumber')
  tmp:DOP = p_web.GSV('job:DOP')
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('ExchangeOrder',ProgressWindow)             ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:EXCHOR48, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ex4:RecordNumber)
  ThisReport.AddSortOrder(ex4:RecordNumberKey)
  ThisReport.AddRange(ex4:RecordNumber,locRecordNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:EXCHOR48.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHOR48.Close
    Relate:JOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('ExchangeOrder',ProgressWindow)          ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:detail1)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','ExchangeOrder','ExchangeOrder','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

BannerNewJobBooking  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('BannerNewJobBooking')
  loc:formname = 'BannerNewJobBooking_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerNewJobBooking','')
    p_web._DivHeader('BannerNewJobBooking',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerNewJobBooking',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerNewJobBooking_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BannerNewJobBooking_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerNewJobBooking')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerNewJobBooking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerNewJobBooking_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerNewJobBooking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BannerNewJobBooking" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BannerNewJobBooking" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BannerNewJobBooking" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BannerNewJobBooking">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BannerNewJobBooking">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BannerNewJobBooking')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BannerNewJobBooking')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BannerNewJobBooking'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BannerNewJobBooking')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)

PreCopy  Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)

PreDelete       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerNewJobBooking_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerNewJobBooking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)
heading  Routine
  packet = clip(packet) & |
    '<<table class="TopBanner"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">New Job Booking<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    ''
UseReplenishmentProcess PROCEDURE  (fModelNumber,fAccountNumber) ! Declare Procedure
MANUFACT::State  USHORT
MODELNUM::State  USHORT
TRADEACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles


    Return# = 0
    ! Inserting (DBH 21/02/2008) # 9717 - If model/manufacturer is used in Replenishment, then it can't be 48 Hour
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = fAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:IgnoreReplenishmentProcess = 0
            ! This account CAN use the replenishment process (DBH: 21/02/2008)
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number = fModelNumber
            If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                If mod:UseReplenishmentProcess = 1
                    Return# = 1
                Else ! If mod:UseReplenishmentProcess = 1
                    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        If man:UseReplenishmentProcess = 1
                            Return# = 1
                        End ! If man:UseReplenishmentProcess = 1
                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End ! If mod:UseReplenishmentProcess = 1
            End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        End ! If tra:IgnoreReplenishmentProcess = 0
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MODELNUM.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
