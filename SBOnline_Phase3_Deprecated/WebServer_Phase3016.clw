

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3016.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3017.INC'),ONCE        !Req'd for module callout resolution
                     END


FormContactHistory   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
ACTION::State  USHORT
CONTHIST::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
cht:Action_OptionView   View(ACTION)
                          Project(act:Action)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormContactHistory')
  loc:formname = 'FormContactHistory_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormContactHistory','')
    p_web._DivHeader('FormContactHistory',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormContactHistory',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormContactHistory',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACTION)
  p_web._OpenFile(CONTHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACTION)
  p_Web._CloseFile(CONTHIST)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormContactHistory_form:inited_',1)
  p_web.SetValue('UpdateFile','CONTHIST')
  p_web.SetValue('UpdateKey','cht:Record_Number_Key')
  p_web.SetValue('IDField','cht:Record_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormContactHistory:Primed') = 1
    p_web._deleteFile(CONTHIST)
    p_web.SetSessionValue('FormContactHistory:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','CONTHIST')
  p_web.SetValue('UpdateKey','cht:Record_Number_Key')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'cht:Action'
    p_web.setsessionvalue('showtab_FormContactHistory',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(ACTION)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.cht:Notes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormContactHistory_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseContactHistory'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormContactHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormContactHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormContactHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormBrowseContactHistory'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(cht:SystemHistory = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="CONTHIST__FileAction" value="'&p_web.getSessionValue('CONTHIST:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="CONTHIST" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="CONTHIST" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="cht:Record_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormContactHistory" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormContactHistory" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormContactHistory" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','cht:Record_Number',p_web._jsok(p_web.getSessionValue('cht:Record_Number'))) & '<13,10>'
  If p_web.Translate('Contact History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Contact History',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormContactHistory">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormContactHistory" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormContactHistory')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormContactHistory')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormContactHistory'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.cht:Action')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormContactHistory')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormContactHistory_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::cht:Action
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::cht:Action
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::cht:Action
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::cht:Notes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::cht:Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::cht:Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:ContactHistoryNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:ContactHistoryNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::cht:Action  Routine
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('cht:Action') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Action')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::cht:Action  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('cht:Action',p_web.GetValue('NewValue'))
    cht:Action = p_web.GetValue('NewValue') !FieldType= STRING Field = cht:Action
    do Value::cht:Action
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('cht:Action',p_web.dFormat(p_web.GetValue('Value'),'@s80'))
    cht:Action = p_web.GetValue('Value')
  End
  do Value::cht:Action
  do SendAlert

Value::cht:Action  Routine
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('cht:Action') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('cht:Action')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cht:Action'',''formcontacthistory_cht:action_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('cht:Action',loc:fieldclass,loc:readonly,,178,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('cht:Action') = 0
    p_web.SetSessionValue('cht:Action','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('cht:Action')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(ACTION)
  bind(act:Record)
  p_web._OpenFile(CONTHIST)
  bind(cht:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(cht:Action_OptionView)
  cht:Action_OptionView{prop:order} = 'UPPER(act:Action)'
  Set(cht:Action_OptionView)
  Loop
    Next(cht:Action_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('cht:Action') = 0
      p_web.SetSessionValue('cht:Action',act:Action)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,act:Action,choose(act:Action = p_web.getsessionvalue('cht:Action')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(cht:Action_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(ACTION)
  p_Web._CloseFile(CONTHIST)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormContactHistory_' & p_web._nocolon('cht:Action') & '_value')

Comment::cht:Action  Routine
    loc:comment = ''
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('cht:Action') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::cht:Notes  Routine
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::cht:Notes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('cht:Notes',p_web.GetValue('NewValue'))
    cht:Notes = p_web.GetValue('NewValue') !FieldType= STRING Field = cht:Notes
    do Value::cht:Notes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('cht:Notes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    cht:Notes = p_web.GetValue('Value')
  End
    cht:Notes = Upper(cht:Notes)
    p_web.SetSessionValue('cht:Notes',cht:Notes)
  do Value::cht:Notes
  do SendAlert

Value::cht:Notes  Routine
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- cht:Notes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('cht:Notes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cht:Notes'',''formcontacthistory_cht:notes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('cht:Notes',p_web.GetSessionValue('cht:Notes'),10,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_value')

Comment::cht:Notes  Routine
      loc:comment = ''
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:ContactHistoryNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:ContactHistoryNotes',p_web.GetValue('NewValue'))
    do Value::button:ContactHistoryNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:ContactHistoryNotes  Routine
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('button:ContactHistoryNotes') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ContactHistoryNotes','Contact History Notes','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickContactHistoryNotes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::button:ContactHistoryNotes  Routine
    loc:comment = ''
  p_web._DivHeader('FormContactHistory_' & p_web._nocolon('button:ContactHistoryNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormContactHistory_cht:Action_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cht:Action
      else
        do Value::cht:Action
      end
  of lower('FormContactHistory_cht:Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cht:Notes
      else
        do Value::cht:Notes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormContactHistory',0)
  cht:Date = Today()
  p_web.SetSessionValue('cht:Date',cht:Date)
  cht:Time = clock()
  p_web.SetSessionValue('cht:Time',cht:Time)
  cht:User = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('cht:User',cht:User)
  cht:Ref_Number = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('cht:Ref_Number',cht:Ref_Number)

PreCopy  Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormContactHistory',0)
  p_web._PreCopyRecord(CONTHIST,cht:Record_Number_Key)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormContactHistory:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormContactHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormContactHistory',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormContactHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormContactHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
          cht:Notes = Upper(cht:Notes)
          p_web.SetSessionValue('cht:Notes',cht:Notes)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormContactHistory:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormContactHistory:Primed',0)
  p_web.StoreValue('')

PostDelete      Routine
FormBrowseContactHistory PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseContactHistory')
  loc:formname = 'FormBrowseContactHistory_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormBrowseContactHistory','')
    p_web._DivHeader('FormBrowseContactHistory',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseContactHistory',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseContactHistory_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseContactHistory_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseContactHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseContactHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseContactHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseContactHistory" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseContactHistory" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseContactHistory" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Contact History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Contact History',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseContactHistory">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseContactHistory" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseContactHistory')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseContactHistory')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseContactHistory'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseContactHistory_BrowseContactHistory_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseContactHistory')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseContactHistory_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseContactHistory
      do Comment::BrowseContactHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseContactHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseContactHistory',p_web.GetValue('NewValue'))
    do Value::BrowseContactHistory
  Else
    p_web.StoreValue('cht:Record_Number')
  End

Value::BrowseContactHistory  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseContactHistory --
  p_web.SetValue('BrowseContactHistory:NoForm',1)
  p_web.SetValue('BrowseContactHistory:FormName',loc:formname)
  p_web.SetValue('BrowseContactHistory:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseContactHistory')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseContactHistory_BrowseContactHistory_embedded_div')&'"><!-- Net:BrowseContactHistory --></div><13,10>'
    p_web._DivHeader('FormBrowseContactHistory_' & lower('BrowseContactHistory') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseContactHistory_' & lower('BrowseContactHistory') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseContactHistory --><13,10>'
  end
  do SendPacket

Comment::BrowseContactHistory  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseContactHistory_' & p_web._nocolon('BrowseContactHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseContactHistory:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseContactHistory_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseContactHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseContactHistory',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseContactHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseContactHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseContactHistory:Primed',0)
  p_web.StoreValue('')
CustCollectionValidated PROCEDURE  (LONG jobNumber)        ! Declare Procedure
  CODE
    RETURN vod.CustomerCollectionValidated(jobNumber)
CustClassificationFields PROCEDURE  (LONG refNumber,*STRING lifeTimeValue,*STRING averageSpend,*STRING loyaltyStatus,*DATE upgradeDate,*STRING IDNumber) ! Declare Procedure
  CODE
    vod.CustomerClassificationFields(refNumber,|
        lifeTimeValue,|
        averageSpend,|
        loyaltyStatus,|
        upgradeDate,|
        IDNumber)
InvoiceCreated       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('InvoiceCreated')
  loc:formname = 'InvoiceCreated_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('InvoiceCreated','')
    p_web._DivHeader('InvoiceCreated',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_InvoiceCreated',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInvoiceCreated',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_InvoiceCreated',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('InvoiceCreated_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('InvoiceCreated_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('InvoiceCreated_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('InvoiceCreated_ChainTo')
    loc:formaction = p_web.GetSessionValue('InvoiceCreated_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="InvoiceCreated" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="InvoiceCreated" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="InvoiceCreated" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Invoice Created') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Invoice Created',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_InvoiceCreated">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_InvoiceCreated" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_InvoiceCreated')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_InvoiceCreated')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_InvoiceCreated'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_InvoiceCreated')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_InvoiceCreated_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::buttonPrintInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintInvoice',p_web.GetValue('NewValue'))
    do Value::buttonPrintInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintInvoice  Routine
  p_web._DivHeader('InvoiceCreated_' & p_web._nocolon('buttonPrintInvoice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintInvoice','Print Invoice','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('InvoiceNote?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images\money.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonPrintInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('InvoiceCreated_' & p_web._nocolon('buttonPrintInvoice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_InvoiceCreated',0)

PreCopy  Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_InvoiceCreated',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('InvoiceCreated:Primed',0)

PreDelete       Routine
  p_web.SetValue('InvoiceCreated_form:ready_',1)
  p_web.SetSessionValue('InvoiceCreated_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('InvoiceCreated:Primed',0)
  p_web.setsessionvalue('showtab_InvoiceCreated',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('InvoiceCreated_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('InvoiceCreated_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('InvoiceCreated:Primed',0)
  p_web.StoreValue('')
CreateInvoice        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('CreateInvoice')
  loc:formname = 'CreateInvoice_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('CreateInvoice','')
    p_web._DivHeader('CreateInvoice',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateInvoice',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateInvoice',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CreateInvoice',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      p_web.StoreValue('returnURL')
  p_web.SetValue('CreateInvoice_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CreateInvoice_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'InvoiceCreated'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CreateInvoice_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CreateInvoice_ChainTo')
    loc:formaction = p_web.GetSessionValue('CreateInvoice_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('returnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CreateInvoice" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CreateInvoice" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CreateInvoice" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Create Invoice') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Create Invoice',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CreateInvoice">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CreateInvoice" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CreateInvoice')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CreateInvoice')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CreateInvoice'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CreateInvoice')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateInvoice_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::messageText
      do Comment::messageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::messageText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('messageText',p_web.GetValue('NewValue'))
    do Value::messageText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::messageText  Routine
  p_web._DivHeader('CreateInvoice_' & p_web._nocolon('messageText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Are you sure you want to create an invoice?',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::messageText  Routine
    loc:comment = ''
  p_web._DivHeader('CreateInvoice_' & p_web._nocolon('messageText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CreateInvoice',0)

PreCopy  Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CreateInvoice',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CreateInvoice:Primed',0)

PreDelete       Routine
  p_web.SetValue('CreateInvoice_form:ready_',1)
  p_web.SetSessionValue('CreateInvoice_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CreateInvoice:Primed',0)
  p_web.setsessionvalue('showtab_CreateInvoice',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CreateInvoice_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Process
      CreateTheInvoice(p_web)
  p_web.DeleteSessionValue('CreateInvoice_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CreateInvoice:Primed',0)
  p_web.StoreValue('')
Line500_XML          PROCEDURE  (f:ID)                     ! Declare Procedure
!    INCLUDE('Line500XML.inc','Data')
  CODE
!    INCLUDE('Line500XML.inc','Code')
CreateTheInvoice     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locPaid              BYTE                                  !
FilesOpened     BYTE(0)
  CODE
    IF (p_web.GSV('job:Invoice_Number') > 0)
        RETURN
    END

    IF (JobInUse(p_web.GSV('job:Ref_Number')) = 1)
        RETURN
    END

    DO openfiles

    SET(DEFAULTS,0)
    Access:DEFAULTS.Next()


    IF (vod.InvoiceTheSubAccount(p_web.GSV('job:Account_Number')))
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
            DO CloseFiles
            RETURN
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            DO CloseFiles
            RETURN
        END        
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = sub:Labour_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END
        
        locLabourVatRate = vat:VAT_Rate
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = sub:Parts_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END        
            
        locPartsVatRate = vat:VAT_Rate
        
        ! Don't think this is relevant to Vodacom        
!        IF (sub:Despatch_Invoiced_Jobs = 'YES')
!            IF (sub:Despatch_Paid_Jobs = 'YES')
!                IF (job:Paid = 'YES')
!                    ! Print Despatch = 1
!                ELSE
!                    ! Print Despatch = 2
!                END
!            ELSE
!                ! Print Despatch = 1
!            END
!        END
        
        
    ELSE
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
            DO CloseFiles
            RETURN
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            DO CloseFiles
            RETURN
        END  
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = tra:Labour_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END
        
        locLabourVatRate = vat:VAT_Rate
        
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:Vat_Code = tra:Parts_Vat_Code
        IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
            DO CloseFiles
            RETURN
        END        
            
        locPartsVatRate = vat:VAT_Rate       
    END

    JobPricingRoutine(p_web)

    IF (Access:INVOICE.PrimeRecord() = Level:Benign)
        inv:Invoice_Type = 'SIN'
        inv:Job_Number = p_web.GSV('job:Ref_Number')
        inv:Date_Created = TODAY()
        inv:Account_Number = p_web.GSV('job:Account_Number')
        IF (tra:Invoice_Sub_Accounts = 'YES')
            inv:AccountType = 'SUB'
        ELSE
            inv:AccountType = 'MAI'
        END
        inv:Total = job:Sub_Total
        inv:RRCVatRateLabour = locLabourVatRate
        inv:RRCVatRateParts = locPartsVatRate
        inv:RRCVatRateRetail = locLabourVatRate
        inv:Vat_Rate_Labour = locLabourVatRate
        inv:Vat_Rate_Parts = locPartsVatRate
        inv:Vat_Rate_Retail = locLabourVatRate
        inv:VAT_Number = def:Vat_Number
        inv:Courier_Paid = job:Courier_Cost
        inv:Parts_Paid = job:Parts_Cost
        inv:Labour_Paid = job:Labour_Cost
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            ! An RRC Created Invoice
            inv:RRCInvoiceDate = TODAY()
            inv:ExportedRRCOracle = TRUE
        END
        
        IF (Access:INVOICE.TryInsert())
            Access:INVOICE.CancelAutoInc()
            DO CloseFiles
            RETURN
        END
        
        locPaid = FALSE
        IF (p_web.GSV('job:Warranty_Job') = 'YES')
            IF (p_web.GSV('job:EDI') = 'FIN')
                locPaid = TRUE
            END
        END
        IF (p_web.GSV('job:Chargeable_Job') = 'YES')
            locPaid = FALSE
            p_web.SSV('TotalPrice:Type','C')
            TotalPrice(p_web)
            IF (p_web.GSV('TotalPrice:Total') = 0 OR p_web.GSV('TotalPrice:Balance') <= 0)
                locPaid = TRUE
            END
        END
        
        Access:COURIER.ClearKey(cou:Courier_Key)
        cou:Courier = job:Courier
        IF (Access:COURIER.TryFetch(cou:Courier_Key))
        END
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            IF (p_web.GSV('jobe:Despatched') <> 'REA')
                IF (locPaid = TRUE)
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',910) ! Despatched Paid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                    
                ELSE
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',905) ! Despatched UnPaid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                END
            ELSE
                IF (locPaid = true)
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',915) ! Paid Awaiting Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',916) ! Paid Awaiting Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END
                    
                ELSE
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',805) ! Ready To Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',810) ! Ready To Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END                    
                END
            END
        ELSE
            !ARC Invoice
            IF (p_web.GSV('job:Despatched') <> 'REA')
                IF (locPaid = TRUE)
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',910) ! Despatched Paid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                    
                ELSE
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        p_web.SSV('GetStatus:StatusNumber',905) ! Despatched UnPaid
                        p_web.SSV('GetSTatus:Type','JOB')
                        GetStatus(p_web)
                    END
                END
            ELSE
                IF (locPaid = true)
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',915) ! Paid Awaiting Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',916) ! Paid Awaiting Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END
                    
                ELSE
                    IF (cou:CustomerCollection)
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',805) ! Ready To Collection
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                        
                    ELSE
                        IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                            p_web.SSV('GetStatus:StatusNumber',810) ! Ready To Despatch
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web)
                        END                                                
                    END                    
                END
                IF (p_web.GSV('job:Despatch_Type') = 'JOB' AND | 
                    p_web.GSV('jobe:WebJob') = 1)
                    p_web.SSV('GetStatus:StatusNumber',p_web.GSV('Default:StatusSentToRRC'))
                    p_web.SSV('GetSTatus:Type','JOB')
                    GetStatus(p_web)   
                END
                
            END
           
        END
        
        
        p_web.SSV('job:Invoice_Number',inv:Invoice_Number)
        p_web.SSV('job:Invoice_Date',TODAY())
        p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
        p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
        p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Sub_Total'))
        
        p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('jobe:InvRRCCSubTotal', p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
        p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate') )
        p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
     
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('job:Ref_Number')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE)
            Access:JOBSE.TryUpdate()
        END
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('job:Ref_Number')
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            p_web.SessionQueueToFile(JOBS)
            Access:JOBS.TryUpdate()
        END
        
        Access:JOBSE2.CLearkey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('job:Ref_Number')
        If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE2)
            Access:JOBSE2.TryUpdate()
        end
        
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            !todo: Line 500
            Line500_XML('RIV')
        END
        DO CloseFiles
        RETURN
        
    END



!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:COURIER.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:COURIER.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSE2.Close
     Access:VATCODE.Close
     Access:COURIER.Close
     Access:INVOICE.Close
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:JOBSE.Close
     Access:JOBS.Close
     Access:DEFAULTS.Close
     FilesOpened = False
  END
CountHistory         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_job2_id         USHORT,AUTO                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)
  CODE
    p_web.SSV('CountHistory',0)
    do openFiles
    Count# = 0

    Save_job2_ID = Access:JOBS2_ALIAS.SaveFile()
    Access:JOBS2_ALIAS.ClearKey(job2:ESN_Key)
    job2:ESN = p_web.GSV('wob:IMEINumber')
    Set(job2:ESN_Key,job2:ESN_Key)
    Loop
        If Access:JOBS2_ALIAS.NEXT()
           Break
        End !If
        If job2:ESN <> p_web.GSV('wob:IMEINumber')      |
            Then Break.  ! End If

        If job2:Cancelled = 'YES'
            Cycle
        End !If job2:Cancelled = 'YES'

        if job2:ref_number = p_web.GSV('wob:RefNumber')
            Cycle
        end ! if job2:ref_number >= p_web.GSV('wob:RefNumber')

        count# += 1
    End !Loop
    Access:JOBS2_ALIAS.RestoreFile(Save_job2_ID)

    do closeFiles

    p_web.SSV('CountHistory',count#)

!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS2_ALIAS.Open                                  ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS2_ALIAS.UseFile                               ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS2_ALIAS.Close
     FilesOpened = False
  END
CompulsoryFieldCheck PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_taf_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
tmp:ForceFaultCodes  BYTE(0)                               !Force Fault Codes
tmp:FoundRequestedParts BYTE(0)                            !Found Requested Parts
tmp:FoundOrderedParts BYTE(0)                              !Found Ordered Parts
tmp:FoundCommonFault BYTE(0)                               !Found Common Fault
save_aud_id          USHORT,AUTO                           !
save_map_id          USHORT                                !
Skip_non_Main        BYTE(0)                               !
tmp:KeyRepair        BYTE(0)                               !Key Repair
tmp:FaultCode        STRING(255),DIM(20)                   !Fault Codes
locErrorMessage      STRING(10000)                         !
func:Type            STRING('C')                           !
local                      CLASS
ValidateFaultCodes             Procedure(),Byte
OutFaultPart                   Procedure(Long func:FieldNumber, String func:JobType),Byte
                           END ! local   CLASS
FilesOpened     BYTE(0)
  CODE
!Main Process
    !return

    do openFiles
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    func:Type = p_web.GSV('CompulsoryFieldCheck:Type')
    error# = 0

    ! Inserting (DBH 09/11/2007) # 9278 - Save the fault codes in a "easy to use" form
    tmp:FaultCode[1]  = p_web.GSV('job:Fault_Code1')
    tmp:FaultCode[2]  = p_web.GSV('job:Fault_Code2')
    tmp:FaultCode[3]  = p_web.GSV('job:Fault_Code3')
    tmp:FaultCode[4]  = p_web.GSV('job:Fault_Code4')
    tmp:FaultCode[5]  = p_web.GSV('job:Fault_Code5')
    tmp:FaultCode[6]  = p_web.GSV('job:Fault_Code6')
    tmp:FaultCode[7]  = p_web.GSV('job:Fault_Code7')
    tmp:FaultCode[8]  = p_web.GSV('job:Fault_Code8')
    tmp:FaultCode[9]  = p_web.GSV('job:Fault_Code9')
    tmp:FaultCode[10] = p_web.GSV('job:Fault_Code10')
    tmp:FaultCode[11] = p_web.GSV('job:Fault_Code11')
    tmp:FaultCode[12] = p_web.GSV('job:Fault_Code12')
    tmp:FaultCode[13] = p_web.GSV('wob:FaultCode13')
    tmp:FaultCode[14] = p_web.GSV('wob:FaultCode14')
    tmp:FaultCode[15] = p_web.GSV('wob:FaultCode15')
    tmp:FaultCode[16] = p_web.GSV('wob:FaultCode16')
    tmp:FaultCode[17] = p_web.GSV('wob:FaultCode17')
    tmp:FaultCode[18] = p_web.GSV('wob:FaultCode18')
    tmp:FaultCode[19] = p_web.GSV('wob:FaultCode19')
    tmp:FaultCode[20] = p_web.GSV('wob:FaultCode20')
    ! End (DBH 09/11/2007) #9278

    If ForceTransitType(func:Type)
        If p_web.GSV('job:Transit_Type') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Transit Type Missing.'
        End!If p_web.GSV('job:Transit_Type = ''
    End !If ForceTransitType(func:Type)

    If p_web.GSV('jobe2:CourierWaybillNumber') = ''
        ! #11880 The compulsory-ness of the waybill is determined by the Transit Type setup. (Bryan: 05/05/2011)
        Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
        trt:Transit_Type = p_web.GSV('job:Transit_Type')
        IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
            ! #11880 If comp at booking, always compulsory, otherwise check at completion (Bryan: 05/05/2011)
            IF (trt:WaybillComBook = 1 OR |
                (func:Type = 'C' And trt:WaybillComComp = 1))
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Courier Waybill Number Missing.'
            END
        END ! IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
    END ! If jobe2:CourierWaybillNumber = ''

    If p_web.GSV('job:POP') = 'REJ' And p_web.GSV('job:Warranty_Job') = 'YES'
        error# = 1
        locErrorMessage = Clip(locErrorMessage) & '<13,10>Cannot be a warranty job, it has been rejected (no POP).'
    End !If p_web.GSV('job:POP = 'REJ' And p_web.GSV('job:Warranty_Job = 'YES'

    If ForceMobileNumber(func:Type)
        If p_web.GSV('job:Mobile_Number') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Mobile Number Missing.'
        End!If p_web.GSV('job:Mobile_Number = ''
    End !If ForceMobileNumber(func:Type)

    ! Inserting (DBH 07/12/2007) # 9491 - Check if an SMS alert number has been filled in
    If (p_web.GSV('jobe2:SMSNotification') = 1 And p_web.GSV('jobe2:SMSAlertNumber') = '')
        If p_web.GSV('job:Mobile_Number') <> ''
            ! May as well use the mobile number if it exists (DBH: 07/12/2007)
            p_web.SSV('jobe2:SMSAlertNumber',p_web.GSV('job:Mobile_Number'))
            Access:JOBSE2.TryUpdate()
        Else ! If p_web.GSV('job:Mobile_Number <> ''
            Error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>SMS Alert Number Missing.'
        End ! If p_web.GSV('job:Mobile_Number <> ''
    End ! If p_web.GSV('jobe:2:SMSNotification And p_web.GSV('jobe:2:SMSAlertNumber = ''
    
    !Added by Paul 05/10/2010 - Log no 11691
    !Do this only for chargeable jobs
    If p_web.GSV('job:Chargeable_Job') = 'YES' then
        If func:Type = 'C' then
            !we need to check of the default labour cost is now lower than the labour cost set on the job
            DefaultLabourCost(p_web)
            !now we have the default labour cost - is it less than the labour cost entered
            ! #11773 Only check if ignoring costs. (Bryan: 28/10/2010)
            If p_web.GSV('jobe:RRCCLabourCost') > p_web.GSV('DefaultLabourCost') AND p_web.GSV('jobe:IgnoreRRCChaCosts') = 1 then

                !labour costs need to be changed
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Override Labour Cost is Higher than Default Cost.'
            End
        End
    End

    ! Inserting (DBH 24/01/2008) # 9612 - Check for waybill courier number
    If p_web.GSV('job:Transit_Type') = 'ARRIVED FRANCHISE'
        If p_web.GSV('jobe2:CourierWaybillNumber') = ''
            If securityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND COURIER WAYBILL NUMBER')
                ! Only force field if the user has access to change it
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Courier Waybill Number Missing.'
            End ! If func:Type = 'B' Or SecurityCheck('AMEND COURIER WAYBILL NUMBER') = 0
        End ! If p_web.GSV('jobe:2:CourierWaybillNumber = ''
    End ! If p_web.GSV('job:Transit_Type = 'ARRIVED FRANCHISE'
    ! End (DBH 24/01/2008) #9612

    If ForceModelNumber(func:Type)
        If p_web.GSV('job:Model_Number') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Model Number Missing.'
        End!If p_web.GSV('job:Model_Number = ''
    End !If ForceModelNumber(func:Type)

    If ForceUnitType(func:Type)
        If p_web.GSV('job:Unit_Type') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Unit Type Type Missing.'
        End!If p_web.GSV('job:Unit_Type = ''
    End !If ForceUnitType(func:Type)

    If ForceColour(func:Type)
        If p_web.GSV('job:Colour') = ''
            error# = 1
            If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Missing.'
            Else !If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Colour Missing'
            End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
        End!If p_web.GSV('job:Unit_Type = ''
    End !If ForceColour(func:Type)

    If ForceFaultDescription(func:Type)
        If p_web.GSV('jbn:Fault_Description') = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Description Missing.'
        End!If jbn:Fault_Description = ''
    End !If ForceFaultDescription(func:Type)

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:UseInvTextForFaults = FALSE
            If ~man:QAAtCompletion And p_web.GSV('job:QA_Passed') <> 'YES'
                If ForceInvoiceText(func:Type)
                    IF jbn:Invoice_Text = ''
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Invoice Text Missing.'
                    End!IF jbn:Invoice_Text = ''
                End !If ForceInvoiceText(func:Type)
            END
        ELSE
           !Check to see if any havebeen added to the file?!
            If ForceInvoiceText(func:Type)
                fail# = 0
                Access:ReptyDef.ClearKey(rtd:ManRepairTypeKey)
                rtd:Manufacturer = p_web.GSV('job:manufacturer')
                rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
                IF Access:ReptyDef.Fetch(rtd:ManRepairTypeKey)
                  !Error!
                ELSE
                    IF rtd:Warranty = 'YES'
                        IF rtd:ExcludeFromEDI = TRUE
                      !Fail Outfault!
                            fail# = 1
                        END
                    END
                END

                JOFL# = 0
                Access:JobOutFL.ClearKey(joo:JobNumberKey)
                joo:JobNumber = p_web.GSV('job:ref_number')
                SET(joo:JobNumberKey,joo:JobNumberKey)
                LOOP
                    IF Access:JobOutFL.Next()
                        BREAK
                    END
                    IF joo:JobNumber <> p_web.GSV('job:ref_number')
                        BREAK
                    END
                 !Do not include free text Out Faults
                    If p_web.GSV('job:Warranty_Job') = 'YES' AND fail# = 0
                        If joo:FaultCode = '0'
                      !JOFL# = 0
                            CYCLE
                        End !If joo:FaultCode <> 0

                        IF SUB(joo:FaultCode,1,1) = '*'
                      !JOFL# = 0
                            CYCLE
                        End !If joo:FaultCode <> 0
                    END !If p_web.GSV('job:Warranty_Job = 'YES' AND fail# = 0
                    jofl# = 1
                END
                IF JOFL# = 0
                    Access:MANFAUPA.ClearKey(map:MainFaultKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:MainFault    = 1
                        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                        !Found
                            If p_web.GSV('job:Chargeable_Job') = 'YES'
                                If Local.OutFaultPart(map:Field_Number,'C')
                                    JOFL# = 1
                                End !If Local.OutFaultPart(map:Field_Number,'C')
                            End !If p_web.GSV('job:Chargeable_Job = 'YES'

                            If p_web.GSV('job:Warranty_Job') = 'YES' And JOFL# = 0
                                If Local.OutFaultPart(map:Field_Number,'W')
                                    JOFL# = 1
                                End !If Local.OutFaultPart(map:Field_Number,'W')
                            End !If p_web.GSV('job:Warranty_Job = 'YES'

                        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                    End !If JOFL#
                        If JOFL# = 0
                            error# = 1
                            locErrorMessage = Clip(locErrorMessage) & '<13,10>Out Faults Missing.'
                        End!IF JOFL# = 0
                    End !If ForceInvoiceText(func:Type)
                End
            END

            If ForceIMEI(func:Type)
                If p_web.GSV('job:ESN') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>I.M.E.I. Number Missing.'
                End!If p_web.GSV('job:ESN = ''
            End !If ForceIMEI(func:Type)

            If ForceMSN(p_web.GSV('job:Manufacturer'),func:Type)
                If p_web.GSV('job:MSN') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>M.S.N. Missing.'
                End!If p_web.GSV('job:MSN = ''
            End !If ForceMSN(func:Type)

    !Check I.M.E.I. Number Length
            If CheckLength(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),'IMEI')
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>I.M.E.I. Number Incorrect Length.'
            End !If CheckLength(p_web.GSV('job:ESN,p_web.GSV('job:Model_Number,'IMEI')

            If MSNRequired(p_web.GSV('job:Manufacturer')) = Level:Benign
                If CheckLength(p_web.GSV('job:MSN'),p_web.GSV('job:Model_Number'),'MSN')
                    Error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>M.S.N. Incorrect Length.'
                End !If CheckLength(p_web.GSV('job:MSN,p_web.GSV('job:Model_Number,'MSN')
                If man:ApplyMSNFormat
                    If CheckFaultFormat(p_web.GSV('job:MSN'),man:MSNFormat)
                        Error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>M.S.N. Incorrect Format.'
                    End !If CheckFaultFormat(p_web.GSV('job:MSN,man:MSNFormat)
                End !If man:ApplyMSNFormat
            End !If MSNRequired(p_web.GSV('job:Manufacturer) = Level:Benign

            If (def:Force_Job_Type = 'B' And func:Type = 'B') Or |
                (def:Force_Job_Type <> 'I' And func:Type = 'C')
                If p_web.GSV('job:Chargeable_Job') <> 'YES' And p_web.GSV('job:Warranty_Job') <> 'YES'
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Job Type(s) Missing.'
                End!If p_web.GSV('job:Chargeable_Job <> 'YES' And p_web.GSV('job:Warranty_Job <> 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Charge_Type') = ''
                    Error# = 1
                    locErrorMessage   = Clip(locErrorMessage) & '<13,10>Chargeable Charge Type Missing.'
                End !If p_web.GSV('job:Chargeable_Job = 'YES' And p_web.GSV('job:Charge_Type = ''
                If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Warranty_Charge_Type') = ''
                    Error# = 1
                    locErrorMessage   = Clip(locErrorMessage) & '<13,10>Warranty Charge Type Missing.'
                End !If p_web.GSV('job:Warranty_Job = 'YES' and p_web.GSV('job:Warranty_Charge_Type = ''
            End!If def:Force_Job_Type = 'B'

            If (def:Force_Engineer = 'B' And func:Type = 'B') Or |
                (def:Force_Engineer <> 'I' And func:Type = 'C')
                If p_web.GSV('job:Engineer') = '' And p_web.GSV('job:Workshop') = 'YES'
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Engineer Missing.'
                End!If p_web.GSV('job:Engineer = ''
            End!If def:Force_Engineer = 'B'

            If ForceRepairType(func:Type)
                If (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Repair_Type') = '') Or |
                    (p_web.GSV('job:Warranty_Job') = 'YES' And p_web.GSV('job:Repair_Type_Warranty') = '')
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Repair Type(s) Missing.'
                Else
            !Check to see if the repair type is an "Exchange Fee" or "Handling Fee" one.
                    If p_web.GSV('job:Chargeable_Job') = 'YES'
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = p_web.GSV('job:Repair_Type')
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Found
                            If rtd:BER = 10
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Chargeable Repair Type is for "Exchange Fee".'
                            End !If rtd:BER = 10 or rtd:BER = 11
                            If rtd:BER = 11
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Chargeable Repair Type is for "Handling Fee".'
                            End !If rtd:BER = 11
                        Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    End !If p_web.GSV('job:Chargeable_Job = 'YES'
                    If p_web.GSV('job:Warranty_Job') = 'YES'
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                        rtd:Warranty   = 'YES'
                        rtd:Repair_Type  = p_web.GSV('job:Repair_Type_Warranty')
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                    !Found
                            If rtd:BER = 10
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Warranty Repair Type is for "Exchange Fee".'
                            End !If rtd:BER = 10 or rtd:BER = 11
                            If rtd:BER = 11
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Warranty Repair Type is for "Handling Fee".'
                            End !If rtd:BER = 11
                        Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    End !If p_web.GSV('job:Chargeable_Job = 'YES'
                End
            End!If def:Force_Repair_Type = 'B'

            If ForceAuthorityNumber(func:Type)
                If p_web.GSV('job:Authority_Number') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Authority Number Missing.'
                End
            Else
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    Access:CHARTYPE.Clearkey(cha:Warranty_Key)
                    cha:Charge_Type = p_web.GSV('job:Charge_Type')
                    cha:Warranty    = 'NO'
                    If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                !Found
                        If cha:ForceAuthorisation
                            If p_web.GSV('job:Authority_Number') = ''
                                error# = 1
                                locErrorMessage = Clip(locErrorMessage) & '<13,10>Authority Number Missing.'
                            End !If p_web.GSV('job:Authority_Number = ''
                        End !If cha:ForceAuthorisation
                    Else ! If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                !Error
                    End !If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                End !If p_web.GSV('job:Chargeable_Job = 'YES'
            End !If ForceAuthorityNumber(func:Type)


            If ForceIncomingCourier(func:Type)
                If p_web.GSV('job:Incoming_Courier') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Incoming Courier Missing.'
                End!If p_web.GSV('job:Incoming_Courier = ''
            End !If ForceIncomingCourier(func:Type)

            If ForceCourier(func:Type)
                If p_web.GSV('job:Courier') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Courier Missing.'
                End!If p_web.GSV('job:Courier = ''
            End !If ForceCourier(func:Type)

            If ForceDOP(p_web.GSV('job:Transit_Type'),p_web.GSV('job:Manufacturer'),p_web.GSV('job:Warranty_Job'),func:Type)
                If p_web.GSV('job:DOP') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>DOP Missing, required by Transit Type.'
                End !If p_web.GSV('job:DOP = ''
            End !If ForceDOP(p_web.GSV('job:Transit_Type,p_web.GSV('job:Manufacturer,p_web.GSV('job:Warranty_Job,func:Type)

            If ForceLocation(p_web.GSV('job:Transit_Type'),p_web.GSV('job:Workshop'),func:Type)
                If p_web.GSV('job:Location') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Location Missing, required by Transit Type.'
                End !If p_web.GSV('job:Location = ''
            End !If ForceLocation(p_web.GSV('job:Transit_Type,p_web.GSV('job:Workshop,func:Type)

            If ForceCustomerName(p_web.GSV('job:Account_Number'),func:Type)
                If p_web.GSV('job:Initial') = '' And p_web.GSV('job:Title') = '' And p_web.GSV('job:Surname') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Customer Name Missing.'
                End!If p_web.GSV('job:Initial = '' And p_web.GSV('job:Title = '' And p_web.GSV('job:Surname = ''
            End !If ForceCustomerName(p_web.GSV('job:Account_Number,func:Type)

            If ForcePostcode(func:type)
                If p_web.GSV('job:Postcode') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Postcode Missing.'
                End!If p_web.GSV('job:Postcode = ''
            End !If ForcePostcode(func:type)

            If ForceDeliveryPostcode(func:Type)
                If p_web.GSV('job:Postcode_Delivery') = ''
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Delivery Postcode Missing.'
                End!If p_web.GSV('job:Postcode_Delivery = ''
            End !If ForceDeliveryPostcode(func:Type)

            Error# = local.ValidateFaultCodes()

    !The Trade Fault codes, should be independent of Charge Types,
    !therefore, they are compulsory if the boxes are ticked.

            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = p_web.GSV('job:account_number')
            if access:subtracc.fetch(sub:account_number_key) = level:benign
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number = sub:main_account_number
                If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
                End!If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
            End!if access:subtracc.fetch(sub:account_number_key) = level:benign
            If ForceNetwork(func:Type)
                If p_web.GSV('jobe:Network') = ''
                    Error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Network Missing.'
                End !If p_web.GSV('jobe::Network = ''
            End !If ForceNetwork(func:Type)


    !Check For Adjustments only at completion

            LookForParts# = 0
            If p_web.GSV('job:warranty_job') = 'YES' And func:Type = 'C' !and tmp:ForceFaultCodes = 1
                access:manufact.clearkey(man:manufacturer_key)
                man:manufacturer  = p_web.GSV('job:manufacturer')
                If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Found
                    If man:forceparts
                        LookForParts# = 1

                    End!If man:forceparts


                Else! If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Error
                End! If access:.tryfetch(man:manufacturer_key) = Level:Benign

                Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
                rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                rtd:Repair_Type  = p_web.GSV('job:Repair_Type_Warranty')
                If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
            !Found
                    If rtd:ForceAdjustment
                        LookForParts# = 1
                    End !If rtd:ForceAdjustment
                Else!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign


                If LookForParts#
                    setcursor(cursor:wait)
                    parts# = 0
                    access:warparts.clearkey(wpr:part_number_key)
                    wpr:ref_number  = p_web.GSV('job:ref_number')
                    set(wpr:part_number_key,wpr:part_number_key)
                    loop
                        if access:warparts.next()
                            break
                        end !if
                        if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                            then break.  ! end if
                        parts# = 1
                        Break
                    end !loop
                    setcursor()
                    If parts# = 0
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Warranty Spares Missing'
                    End !If found# = 0
                End !If LookForParts#
            End!If p_web.GSV('job:warranty_job = 'YES'

            If (def:Force_Spares = 'B' And func:Type = 'B') Or |
                (def:Force_Spares <> 'I' And func:Type = 'C')
                If p_web.GSV('job:chargeable_job')    = 'YES'
                    setcursor(cursor:wait)
                    parts# = 0
                    access:parts.clearkey(par:part_number_key)
                    par:ref_number  = p_web.GSV('job:ref_number')
                    set(par:part_number_key,par:part_number_key)
                    loop
                        if access:parts.next()
                            break
                        end !if
                        if par:ref_number  <> p_web.GSV('job:ref_number')      |
                            then break.  ! end if
                        parts# = 1
                        Break
                    end !loop
                    setcursor()
                    If parts# = 0
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Chargeable Spares Missing'
                    End !If found# = 0
                End !If p_web.GSV('job:job_Type = 'CHARGEABLE'
            End!If def:Force_Spares = 'B'

    !Qa before Completion

    ! Set Key Repair to "1". Turn it off, if Key Repair Is Required.
            tmp:KeyRepair = 1
            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = p_web.GSV('job:Manufacturer')
            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
                If man:UseQA And func:Type = 'C'
                    If ~man:QAAtCompletion And p_web.GSV('job:QA_Passed') <> 'YES'
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>QA Has Not Been Passed'

                    End !If man:QAAtCompletion And p_web.GSV('job:QA_Passed <> 'YES'
                End !If man:UseQA
                If man:KeyRepairRequired And func:Type = 'C'
                    tmp:KeyRepair = 0
                End ! If man:KeyRepairRequired
            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    !Parts Remaining
            tmp:FoundRequestedParts = 0
            tmp:FoundOrderedParts = 0

    !Also check if parts have not been removed,
    !if rapid stock is being used
            RestockPart# = 0

            CountParts# = 0
            setcursor(cursor:wait)
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = p_web.GSV('job:ref_number')
            Set(wpr:part_number_key,wpr:part_number_key)
            Loop
                if access:warparts.next()
                    break
                end !if
                if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                    then break.  ! end if
        !Do not validate a Exchange unit
                If wpr:Part_Number = 'EXCH'
                    Cycle
                End !If wpr:Part_Number = 'EXCH'

        ! Inserting (DBH 23/11/2007) # 9568 - Only check key repair if there ARE parts attached
                CountParts# += 1
        ! End (DBH 23/11/2007) #9568

        !Bodge to make Fault Code 3 compulsory for warranty adjustments for Siemens
                If p_web.GSV('job:Manufacturer') = 'SIEMENS' and func:Type = 'C'
                    If wpr:Fault_Code3 = '' and wpr:Adjustment = 'YES'
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code 3 Missing On Warranty Adjustment'
                    End !If wpr:Fault_Code3 = '' and wpr:Adjustment = 'YES'
                End !If p_web.GSV('job:Manufacturer = 'SIEMENS'
                
                If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
                    p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'),|
                    p_web.GSV('job:Repair_Type'),p_web.GSV('job:Repair_Type_Warranty'),'W', |
                    p_web.GSV('job:Manufacturer'))
                    Do WarrantyPartFaultCodes
                Else
            ! This may be wrong, but if Fault Codes aren't required, don't check for Key Repair (DBH: 06/11/2007)
                    tmp:KeyRepair = 1
                End!If tmp:ForceFaultCodes

                If wpr:Status = 'RET'
                    RestockPart# += 1
                End !If wpr:Status = 'RET'

                If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                    tmp:FoundRequestedParts = 1
                    Break
                End
                If wpr:order_number <> '' And wpr:date_received = ''
                    tmp:FoundOrderedParts = 1
                    Break
                End
            End !loop
            access:warparts.restorefile(save_wpr_id)
            setcursor()

! Changing (DBH 23/11/2007) # 9568 - Only check for Key Repair if there are parts attached
!    If tmp:KeyRepair = 0
! to (DBH 23/11/2007) # 9568
            If tmp:KeyRepair = 0 And CountParts# > 0
! End (DBH 23/11/2007) #9568
                Error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>None of the warranty parts have been marked as "Key Repair".'
            End ! If tmp:KeyRepair = 0

            If func:Type = 'C'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = p_web.GSV('job:ref_number')
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                        break
                    end !if
                    if par:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if

                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                    If par:Part_Number <> 'EXCH'
                        If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
                            p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'),|
                            p_web.GSV('job:Repair_Type'),p_web.GSV('job:Repair_Type_Warranty'),'X', |
                            p_web.GSV('job:Manufacturer'))
                            Do ChargeablePartFaultCodes
                        End !If ForceFaultCodes(p_web.GSV('job:Chargeable_Job,p_web.GSV('job:Warranty_Job,|
                    End!If tmp:ForceFaultCodes
                    If (useStockAllocation(sto:Location) & par:Status = 'RET')
                        RestockPart# += 1
                    End !If wpr:Status = 'RET'

                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        tmp:FoundRequestedParts = 1
                        Break
                    End
                    If par:order_number <> '' And par:date_received = ''
                        tmp:FoundOrderedParts = 1
                        Break
                    End

                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()

                If tmp:FoundRequestedParts = 1
                    error# = 1
                    If def:SummaryOrders
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>There are Unreceived spares attached'
                    Else !If def:SummaryOrders
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>There are Unordered spares attached'
                    End !If def:SummaryOrders

                Elsif tmp:FoundOrderedParts = 1
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>There are Unreceived spares attached'
                End

                If RestockPart#
                    Error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>There are spares that have NOT been returned to stock by Allocations'
                End !If RestockPart#
            End!If func:Type = 'C'


            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = p_web.GSV('job:Account_Number')
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
                    If tra:Invoice_Sub_Accounts = 'YES'
                        If sub:ForceOrderNumber and p_web.GSV('job:Order_Number') = ''
                            error# = 1
                            locErrorMessage   = Clip(locErrorMessage) & '<13,10>Order Number Missing.'
                        End !If tra:ForceOrderNumber
                    Else !If tra:Invoice_Sub_Accounts
                        If tra:ForceOrderNumber and p_web.GSV('job:Order_Number') = ''
                            error# = 1
                            locErrorMessage   = Clip(locErrorMessage) & '<13,10>Order Number Missing.'
                        End !If tra:ForceOrderNumber
                    End !If tra:Invoice_Sub_Accounts
                Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

            Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

            If ForceAccessories(func:Type)
                If NoAccessories(p_web.GSV('job:Ref_Number'))
                    error# = 1
                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Job Accessories Missing.'
                End !If AccError# = 1
            End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B'


            If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                If p_web.GSV('job:Chargeable_Job') = 'YES' and func:Type = 'C'
                    If p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' And p_web.GSV('job:Estimate_Rejected') <> 'YES'
                        Error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Estimate has not been Accepted/Rejected.'
                    End !If p_web.GSV('job:Estimate = 'YES' and (p_web.GSV('job:Estiamte_Accepted <> 'YES' Or p_web.GSV('job:Estimate_Rejected <> 'YES')
                End !If p_web.GSV('job:Chargeable_Job = 'YES'
            End !If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1

            p_web.SSV('locCompleteRepair',clip(p_web.GSV('locCompleteRepair')) & clip(locErrorMessage))

            do closeFiles
ChargeablePartFaultCodes      Routine
    !Warranty Parts Fault Codes
    !Before we start Check if PART is an accessory

    Save_map_ID = Access:MANFAUPA.SaveFile()
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:Manufacturer')
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.NEXT()
           Break
        End !If
        If map:Manufacturer <> p_web.GSV('job:Manufacturer')      |
            Then Break.  ! End If
        !Check ok!
        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
          !Error!
        ELSE
          IF sto:Accessory <> 'YES' AND map:Manufacturer = 'MOTOROLA' !ALERT WORKAROUND!
            Skip_non_main = TRUE
          END
        END
        IF Skip_non_Main = TRUE
          IF map:MainFault = FALSE
            CYCLE
          END
        END
        Case map:Field_Number
            Of 1
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code1 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'
                If map:restrictlength
                    If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength

                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 2
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code2 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code2)) < map:lengthfrom or Len(Clip(par:Fault_Code2)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code2,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

            Of 3
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code3 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code3)) < map:lengthfrom or Len(Clip(par:Fault_Code3)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code3,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 4
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code4 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code4)) < map:lengthfrom or Len(Clip(par:Fault_Code4)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code4,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 5
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code5 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code5)) < map:lengthfrom or Len(Clip(par:Fault_Code5)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code5,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 6
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code6 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code6)) < map:lengthfrom or Len(Clip(par:Fault_Code6)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code6,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 7
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code7 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code7)) < map:lengthfrom or Len(Clip(par:Fault_Code7)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code7,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 8
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code8 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code8)) < map:lengthfrom or Len(Clip(par:Fault_Code8)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code8,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 9
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code9 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code9)) < map:lengthfrom or Len(Clip(par:Fault_Code9)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code9,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 10
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code10 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code10)) < map:lengthfrom or Len(Clip(par:Fault_Code10)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code10,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat



            Of 11
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code11 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code11)) < map:lengthfrom or Len(Clip(par:Fault_Code11)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code11,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 12
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code12 = ''
!                        Error# = 1
!                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code12)) < map:lengthfrom or Len(Clip(par:Fault_Code12)) > map:lengthto
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code12,map:FieldFormat)
                        error# = 1
                        locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

        End !Case map:Field_Number
    End !Loop
    Access:MANFAUPA.RestoreFile(Save_map_ID)
WarrantyPartFaultCodes      Routine
Data
local:FaultCode     String(30),Dim(12)
Code
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number = wpr:Part_Ref_Number
    If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    Clear(local:FaultCode)

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:Field_Number = 0
                        Set(map:Field_Number_Key,map:Field_Number_Key)
                        Loop
                            If Access:MANFAUPA.Next()
                                Break
                            End ! If Access:MANFAUPA.Next()
                            If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                                Break
                            End ! If map:Manufacturer <> job:Manufacturer
                            If map:NotAvailable
                                Cycle
                            End ! If map:NotAvailable

                            Case map:Field_Number
                            Of 1
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code1
                            Of 2
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code2
                            Of 3
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code3
                            Of 4
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code4
                            Of 5
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code5
                            Of 6
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code6
                            Of 7
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code7
                            Of 8
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code8
                            Of 9
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code9
                            Of 10
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code10
                            Of 11
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code11
                            Of 12
                                local:FaultCode[map:Field_Number] = wpr:Fault_Code12
                            End ! Case map:Field_Number
                        End ! Loop

                        
                        Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:Field_Number = 0
                        Set(map:Field_Number_Key,map:Field_Number_Key)
                        Loop
                            If Access:MANFAUPA.Next()
                                Break
                            End ! If Access:MANFAUPA.Next()
                            If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                                Break
                            End ! If map:Manufacturer <> job:Manufacturer

                            If map:MainFault = 0
                                If sto:Accessory <> 'YES' And man:ForceAccessoryCode
                                    Cycle
                                End ! If sto:Accessory <> 'YES' And man:ForceAcessoryCode
                            End ! If map:MainFault = 0

                            If (map:Compulsory = 'YES' And wpr:Adjustment = 'NO') Or |
                                (map:Compulsory = 'YES' And wpr:Adjustment = 'YES' And map:CompulsoryForAdjustment)

! Compulsory if not Adjustment, or if Adjustment and fault marked as compulsory FOR adjustment
                                If local:FaultCode[map:Field_Number] = ''
                                    Error# = 1
                                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                                End ! If local:FaultCode[map:Field_Number] = ''
                            End ! If map:Compulsory = 'YES'

                            If map:RestrictLength
                                If Len(Clip(local:FaultCode[map:Field_Number])) < map:LengthFrom Or Len(Clip(local:FaultCode[map:Field_Number])) > map:LengthTo
                                    Error# = 1
                                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                                End ! If Len(Clip(local:FaultCode[map:Field_Number])) < map:LengthFrom Or Len(Clip(local:FaultCode[map:Field_Number])) > map:LengthTo
                            End ! If map:RestrictLength

                            If map:ForceFormat = 'YES'
                                If CheckFaultFormat(local:FaultCode[map:Field_Number],map:FieldFormat)
                                    Error# = 1
                                    locErrorMessage = Clip(locErrorMessage) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                                End ! If CheckFaultFormat(local:FaultCode[map:Field_Number],map:FieldFormat)
                            End ! If map:ForceFormat = 'YES'

                            If map:KeyRepair
                                If local:FaultCode[map:Field_Number] = 1
! The Key Repair Fault Code should only by 1 or 0.
                                    tmp:KeyRepair = 1
                                End ! If local:FaultCode[map:Field_Number] <> ''
                            End ! If map:KeyRepair
                        End ! Loop
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRANTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRANTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRANTYPE.Close
     Access:MANFAUPA.Close
     Access:STOCK.Close
     Access:WARPARTS.Close
     Access:SUBTRACC.Close
     Access:PARTS.Close
     Access:TRADEACC.Close
     Access:CHARTYPE.Close
     Access:JOBOUTFL.Close
     Access:REPTYDEF.Close
     Access:MANUFACT.Close
     Access:DEFAULTS.Close
     FilesOpened = False
  END
Local.OutFaultPart      Procedure(Long  func:FieldNumber,String func:JobType)
Code
    Case func:JobType
        Of 'C'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If
                Case func:FieldNumber
                    Of 1
                        If par:Fault_Code1 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                    Of 2
                        If par:Fault_Code2 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 3
                        If par:Fault_Code3 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 4
                        If par:Fault_Code4 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 5
                        If par:Fault_Code5 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 6
                        If par:Fault_Code6 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 7
                        If par:Fault_Code7 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 8
                        If par:Fault_Code8 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 9
                        If par:Fault_Code9 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 10
                        If par:Fault_Code10 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 11
                        If par:Fault_Code11 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 12
                        If par:Fault_Code12 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                End !Case map:Field_Number
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            Return Found#
        Of 'W'
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If
                Case func:FieldNumber
                    Of 1
                        If wpr:Fault_Code1 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                    Of 2
                        If wpr:Fault_Code2 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 3
                        If wpr:Fault_Code3 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 4
                        If wpr:Fault_Code4 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 5
                        If wpr:Fault_Code5 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 6
                        If wpr:Fault_Code6 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 7
                        If wpr:Fault_Code7 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 8
                        If wpr:Fault_Code8 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 9
                        If wpr:Fault_Code9 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 10
                        If wpr:Fault_Code10 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 11
                        If wpr:Fault_Code11 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 12
                        If wpr:Fault_Code12 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                End !Case map:Field_Number
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)

            Return Found#

    End !Case func:Type
local.ValidateFaultCodes        Procedure()
local:PartFaultCode     String(30),Dim(12)
local:CExcludeFromEDI           Byte(0)
local:WExcludeFromEDI           Byte(0)
Code
    Error# = 0
    ChaComp# = False
    WarComp# = False
    If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
            p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'), |
        p_web.GSV('job:Repair_type'),p_web.GSV('job:Repair_Type_Warranty'),'C', |
        p_web.GSV('job:Manufacturer'))
        ChaComp# = True
        ! Inserting (DBH 01/05/2008) # 9723 - Fault codes should be forced. Check exclude from edi flag
        Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
        rtd:Chargeable = 'YES'
        rtd:Repair_Type = p_web.GSV('job:Repair_Type')
        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
            ! Found
            If rtd:ExcludeFromEDI = 1
                local:CExcludeFromEDI = 1
            End ! If rtd:ExcludeFromEDI = 1
        Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        ! End (DBH 01/05/2008) #9723


    End ! p_web.GSV('job:Warranty_Charge_Type,p_web.GSV('job:Repair_type,p_web.GSV('job:Repair_Type_Warranty,'C')

    If ForceFaultCodes(p_web.GSV('job:Chargeable_Job'),p_web.GSV('job:Warranty_Job'),|
                p_web.GSV('job:Charge_Type'),p_web.GSV('job:Warranty_Charge_Type'),|
        p_web.GSV('job:Repair_type'),p_web.GSV('job:Repair_Type_Warranty'),'W', |
        p_web.GSV('job:Manufacturer'))
        WarComp# = True
        ! Inserting (DBH 01/05/2008) # 9723 - Fault codes should be forced. Check exclude from edi flag
        Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
        rtd:Manufacturer = p_web.GSV('job:Manufacturer')
        rtd:Warranty = 'YES'
        rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
            ! Found
            If rtd:ExcludeFromEDI = 1
                local:WExcludeFromEDI = 1
            End ! If rtd:ExcludeFromEDI = 1
        Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        ! End (DBH 01/05/2008) #9723
    End ! p_web.GSV('job:Warranty_Charge_Type,p_web.GSV('job:Repair_type,p_web.GSV('job:Repair_Type_Warranty,'C')

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = p_web.GSV('wob:HeadACcountNumber')
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location = tra:SiteLocation
        If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
        End ! If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign


    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = p_web.GSV('job:manufacturer')
    maf:Field_Number = 0
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> p_web.GSV('job:manufacturer')      |
            then break.  ! end if

! Changing (DBH 09/11/2007) # 9278 - Logic improved (i hope)
!        If (maf:Compulsory_At_Booking = 'YES' And ~maf:CompulsoryIfExchange And WarComp# = True) Or |  !Maybe long winded but sometimes logic fails me
!            (maf:Compulsory_At_Booking = 'YES' And maf:CompulsoryIfExchange And p_web.GSV('job:Exchange_Unit_Number <> 0 And WarComp# = True) Or |
!            (maf:Compulsory = 'YES' And func:Type = 'C' And ~maf:CompulsoryIfExchange And WarComp# = True) Or |
!            (maf:Compulsory = 'YES' And func:Type = 'C' And maf:CompulsoryIfExchange And p_web.GSV('job:Exchange_Unit_Number <> 0 And WarComp# = True) Or |
!            (maf:CharCompulsoryBooking = True And ChaComp# = True) Or |
!            (maf:CharCompulsory = True And func:Type = 'C' And ChaComp# = True)
! to (DBH 09/11/2007) # 9278
        Continue# = 0
        If WarComp# = 1
            ! Warranty p_web.GSV('job: Force Fault Codes (DBH: 09/11/2007)
            If maf:CompulsoryIfExchange = 0
                ! Doesn't matter if exchange is attached (DBH: 09/11/2007)
                If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                    ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                    Continue# = 1
                End ! If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
            Else ! If maf:CompulsoryIfExchange = 0
                ! Only forced if exchange attached (DBH: 09/11/2007)
                If p_web.GSV('job:Exchange_Unit_Number') <> 0
                    If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                        ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                        Continue# = 1
                    End ! If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                End ! If p_web.GSV('job:Exchange_Unit_Number <> 0
            End ! If maf:CompulsoryIfExchange = 0
            If Continue# = 1
                ! Inserting (DBH 06/05/2008) # 9723 - Only force fault code if the warranty repair type matches
                If maf:CompulsoryForRepairType = 1
                    If maf:CompulsoryRepairType <> p_web.GSV('job:Repair_Type_Warranty')
                        ! Fault Code is only compulsory when the repair type matches (DBH: 06/05/2008)
                        Continue# = 0
                    End ! If maf:CompulsoryRepairType <> p_web.GSV('job:Repair_Type_Warranty
                End ! If maf:CompulsoryForRepairType = 1
                ! End (DBH 06/05/2008) #9723
            End ! If Continue# = 1
        End ! If WarComp# = 1

        If ChaComp# = 1
            ! Chargeable p_web.GSV('job: Force Fault Codes (DBH: 09/11/2007)
            If maf:CharCompulsoryBooking = 1 Or (maf:CharCompulsory = 1 and func:Type = 'C')
                ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                Continue# = 1
            End ! If maf:Compulsory_At_Booking = 'YES' or (maf:Compulsory = 'YES' and func:Type = 'C')
        End ! If ChaComp# = 1

        If Continue# = 0
            ! Check all the part fault codes to see if any of the value mean this fault code is forced (DBH: 09/11/2007)
            Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
            wpr:Ref_Number = p_web.GSV('job:Ref_Number')
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.Next()
                    Break
                End ! If Access:WARPARTS.Next()
                If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                    Break
                End ! If wpr:Ref_Number <> p_web.GSV('job:Ref_Number

                ! Save the fault codes in an easy format (DBH: 09/11/2007)
                local:PartFaultCode[1] = wpr:Fault_Code1
                local:PartFaultCode[2] = wpr:Fault_Code2
                local:PartFaultCode[3] = wpr:Fault_Code3
                local:PartFaultCode[4] = wpr:Fault_Code4
                local:PartFaultCode[5] = wpr:Fault_Code5
                local:PartFaultCode[6] = wpr:Fault_Code6
                local:PartFaultCode[7] = wpr:Fault_Code7
                local:PartFaultCode[8] = wpr:Fault_Code8
                local:PartFaultCode[9] = wpr:Fault_Code9
                local:PartFaultCode[10] = wpr:Fault_Code10
                local:PartFaultCode[11] = wpr:Fault_Code11
                local:PartFaultCode[12] = wpr:Fault_Code12

                Loop x# = 1 To 12
                    If local:PartFaultCode[x#] <> ''
                        Access:MANFPALO.Clearkey(mfp:Field_Key)
                        mfp:Manufacturer = p_web.GSV('job:Manufacturer')
                        mfp:Field_Number = x#
                        mfp:Field = local:PartFaultCode[x#]
                        If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                            If mfp:ForceJobFaultCode
                                If mfp:ForceFaultCodeNumber = maf:Field_Number
                                    Continue# = 1
                                    Break
                                End ! If mfp:ForceFaultCodeNumber = 1
                            End ! If mfp:ForceJobFaultCode
                        End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                    End ! If local:PartFaultCode[x#] <> ''
                End ! Loop x# = 1 To 12
                If Continue# = 1
                    Break
                End ! If Continue# = 1
            End ! Loop
        End ! If Continue# = 0

        If Continue# = 0
            Cycle
        End ! If Continue# = 0
! End (DBH 09/11/2007) #9278

!            Stop('maf:Compulsory_At_Booking: ' & maf:Compulsory_At_Booking & |
!                '<13,10>maf:CompulsoryIfExchange: ' & maf:CompulsoryIfExchange & |
!                '<13,10>p_web.GSV('job:Exchange_Unit_Number: ' & p_web.GSV('job:Exchange_Unit_Number & |
!                '<13,10>maf:Compulsory: ' & maf:Compulsory & |
!                '<13,10>maf:CharCompulsory: ' & maf:CharCompulsory & |
!                '<13,10>maf:CharCompulsoryBooking:' & maf:CharCompulsoryBooking & |
!                '<13,10>ChaComp#: ' & ChaComp# & |
!                '<13,10>WarComp#: ' & WarComp# & |
!                '<13,10,13,10>maf:Field_Name: ' & maf:Field_Name)

        ! Inserting (DBH 14/03/2007) # 8718 - Do not force fault codes (apart from in and out faults) for obf jobs,
        If ~man:UseFaultCodesForOBF And p_web.GSV('jobe:OBFValidated') = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <> 1
        End ! If ~man:UseFaultCodeForOBF
        ! End (DBH 14/03/2007) #8718

        ! Inserting (DBH 01/05/2008) # 9723 - If exclude from edi and fault code forced, only force the in/out fault
        If WarComp# = 1 And local:WExcludeFromEDI = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <>1
        End ! If WarComp# = 1 And local:WExcludeFromEDI = 1
        If ChaComp# = 1 And local:CExcludeFromEDI = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <> 1
        End ! If ChaComp# = 1 And local:CExcludeFromEDI = 1
        ! End (DBH 01/05/2008) #9723

        ! An old override. Don't know why this exists, but thought it safer not to delete (DBH: 09/11/2007)
        If p_web.GSV('job:Manufacturer') = 'ERICSSON' and p_web.GSV('job:DOP') = ''
            Cycle
        End ! If p_web.GSV('job:Manufacturer = 'ERICSSON' and p_web.GSV('job:DOP = ''

        ! If fault code is hidden due to another code being blank, then don't check (DBH: 09/11/2007)
        If maf:HideRelatedCodeIfBlank
            If tmp:FaultCode[maf:BlankRelatedCode] = ''
                Cycle
            End ! If tmp:FaultCode[maf:BlankRelatedCode} = ''
        End ! If maf:HideRelatedCodeIfBlank

        ! Is the fault code restricted by site location (DBH: 09/11/2007)
        If maf:RestrictAvailability
            If (maf:RestrictServiceCentre = 1 And ~loc:Level1) Or |
                (maf:RestrictServiceCentre = 2 And ~loc:Level2) Or |
                (maf:RestrictServiceCentre = 3 And ~loc:Level3)
                Cycle
            End ! (maf:RestrictServiceCentre = 3 And ~loc:Level3)
        End ! If maf:RestrictAvailability

        If maf:RestrictLength
            If Len(Clip(tmp:FaultCode[maf:Field_Number])) < maf:LengthFrom Or Len(Clip(tmp:FaultCode[maf:Field_Number])) > maf:LengthTo
                error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
            End ! If Len(Clip(tmp:FaultCode[maf:Field_Number])) < maf:LengthFrom Or Len(Clip(tmp:FaultCode[maf:Field_Number])) > maf:LengthTo
        End ! If maf:RetrictLength

        If maf:ForceFormat
            If CheckFaultFormat(tmp:FaultCode[maf:Field_Number],maf:FieldFormat)
                error# = 1
                locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
            End ! If CheckFaultFormat(tmp:FaultCode[maf:Field_Number],maf:FieldFormat)
        End ! If maf:ForceFormat

        If tmp:FaultCode[maf:Field_Number] = ''
            error# = 1
            locErrorMessage = Clip(locErrorMessage) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
        End ! If tmp:FaultCode[maf:Field_Number] = ''

    End !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()

    Return Error#
