

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3025.INC'),ONCE        !Local module procedure declarations
                     END


ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TimeOut') > 0
    TempTimeOut = ThisMessageBox.GetGlobalSetting('TimeOut')
    ThisMessageBox.SetGlobalSetting('TimeOut', 0)
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
  if TempTimeOut
    ThisMessageBox.SetGlobalSetting('TimeOut',TempTimeOut)
  end
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('MS Sans Serif',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisMessageBox.DontShowWindow = 1
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
        ThisMessageBox.ReturnValue = ThisMessageBox.PreOpen(LMBD:MessageText,LMBD:HeadingText,LMBD:Defaults)
        if ThisMessageBox.ReturnValue then
          self.Response = RequestCompleted
          return level:notify
        end
        ThisMessageBox.DontShowThisAgain = ?DontShowThisAgain
        ThisMessageBox.TimeOutPrompt = ?TimerCounter
        ThisMessageBox.HAControl = ?HALink
  SELF.Open(window)                                        ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    ThisMessageBox.Close()
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

RunDOS               PROCEDURE  (string aCmd, byte aWait)  ! Declare Procedure
! dwCreationFlag values
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
  CODE
! Run DOS Program

    CommandLine = clip(aCmd)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = CreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = GETINI('MQ','TimeOut',,Clip(Path()) & '\SB2KDEF.INI')
    if (timeout# = 0)
        timeout# = 20
    end ! if (timeout# = 0)
    timeout# = clock() + (timeout# * 100)

    if aWait then
        setcursor(cursor:wait)
        loop
            Sleep(100)
            ! check for when process is finished
            ReturnCode# = GetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
        while ProcessExitCode = STILL_ACTIVE and clock() < timeout#
        setcursor
    end

    return(true)
ForcePostcode        PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Postcode
    If (def:ForcePostcode = 'B' and func:Type = 'B') Or |
        (def:ForcePostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForcePostcode = 'B''
    Return Level:Benign
CustomerNameRequired PROCEDURE  (func:AccountNumber)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    !Check the trade account details to see if a customer name is required
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = func:AccountNumber
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            If tra:Invoice_Sub_Accounts = 'YES'
                If sub:ForceEndUserName
                    Return# = 1
                End !If sub:ForceEndUserName
            Else !If tra:Invoice_Sub_Accounts = 'YES'
                If tra:Use_Contact_Name = 'YES'
                    Return# = 1
                End!If tra:Use_Contact_Name = 'YES'

            End !If tra:Invoice_Sub_Accounts = 'YES'
        End!If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    End!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles

    Return Return#

SaveFiles  ROUTINE
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     FilesOpened = False
  END
Pricing_Routine      PROCEDURE  (f_type,f_labour,f_parts,f_pass,f_claim,f_handling,f_exchange,f_RRCRate,f_RRCParts) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
tmp:AccessoryExists  BYTE(0)                               !
tmp:TradeAccount     STRING(15)                            !
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_epr_id   ushort,auto
  CODE
    !This does nothing anymore...
    !Replaced by JobPricingRoutine
    Return




!
!
!! Source Bit
!    f_labour = 0
!    f_parts = 0
!    f_pass = 0
!    f_claim = 0
!    f_handling = 0
!    f_exchange = 0
!    f_RRCRate   = 0
!    f_RRCParts = 0
!! Chargeable Job?
!
!    If job:account_number = ''
!        Return
!    End
!    Case f_type
!        Of 'C'
!            If job:chargeable_job = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!            ! No Charge?
!!                If job:ignore_chargeable_charges = 'YES'
!!                    Return
!!                End
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate = 0
!                        f_RRCParts = 0
!
!                    Else !If cha:no_charge = 'YES'
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            rrc_total$   = 0
!                            save_par_id = access:parts.savefile()
!                            access:parts.clearkey(par:part_number_key)
!                            par:ref_number  = job:ref_number
!                            set(par:part_number_key,par:part_number_key)
!                            loop
!                                if access:parts.next()
!                                   break
!                                end !if
!                                if par:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += par:sale_cost * par:quantity
!                                rrc_total$  += par:RRCSaleCost * par:Quantity   !was + changed JC
!                                !message(par:rrcSaleCost * par:quantity&' - '&rcc_total$)
!                            end !loop
!                            access:parts.restorefile(save_par_id)
!                            f_parts = parts_total$
!                            f_rrcparts  = rrc_total$
!
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_rrcparts = 0
!
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!
!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    f_handling  = trc:HandlingFee
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                f_handling  = suc:HandlingFee
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:invoice_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                f_handling  = trc:HandlingFee
!                                use_standard# = 0
!
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:invoice_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                                f_handling  = sta:HandlingFee
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'W'
!
!        !Warranty Job
!            If job:warranty_job = 'YES'
!                !Do not reprice Warranty Jobs that are completed - L945 (DBH: 03-09-2003)
!                If job:Date_Completed <> 0
!
!                    Return
!                End !If job:Date_Completed <> 0
!                parts_discount# = 0
!                labour_discount# = 0
!                f_pass = 1
!!               If job:ignore_warranty_charges = 'YES'
!!                   Return
!!               End
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:warranty_charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCParts  = 0
!                    f_RRCRate   = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCParts  = 0
!                        f_RRCRate   = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!
!                            Access:STOCK.Open()
!                            Access:STOCK.UseFile()
!
!                            tmp:AccessoryExists = false
!                            tmp:TradeAccount = ''
!
!                            save_wpr_id = access:warparts.savefile()
!                            access:warparts.clearkey(wpr:part_number_key)
!                            wpr:ref_number  = job:ref_number
!                            set(wpr:part_number_key,wpr:part_number_key)
!                            loop
!                                if access:warparts.next()
!                                   break
!                                end !if
!                                if wpr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += wpr:purchase_cost * wpr:quantity
!                                RRCParts_Total$ += wpr:RRCPurchaseCost * wpr:Quantity
!
!                                ! --------------------------------------------------------------------
!                                ! Check if any parts are accessories
!                                Access:STOCK.ClearKey(sto:Ref_Number_Key)
!                                sto:Ref_Number = wpr:Part_Ref_Number
!                                if not Access:STOCK.Fetch(sto:Ref_Number_Key)
!                                    if sto:Accessory = 'YES'
!                                        tmp:AccessoryExists = true
!                                    end
!                                    i# = instring('<32>',clip(sto:Location),1,1)
!                                    if i# <> 0
!                                        tmp:TradeAccount = sto:Location[1:i#]
!                                    end
!                                end
!                                ! --------------------------------------------------------------------
!                            end !loop
!                            access:warparts.restorefile(save_wpr_id)
!                            Access:STOCK.Close()
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!
!                        use_standard# = 1
!!                        If TRA:ZeroChargeable = 'YES'
!!                            f_parts = 0
!!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:Warranty_charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type_warranty
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:warranty_charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type_warranty
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_claim     = trc:WarrantyClaimRate
!                                    f_handling  = trc:HandlingFee
!                                    f_Exchange  = trc:Exchange
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_claim     = suc:WarrantyClaimRate
!                                f_handling  = suc:HandlingFee
!                                f_Exchange  = suc:Exchange
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = tra:account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:warranty_charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type_warranty
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_claim     = trc:WarrantyClaimRate
!                                f_handling  = trc:HandlingFee
!                                f_Exchange  = trc:Exchange
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:warranty_charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type_warranty
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_claim     = sta:WarrantyClaimRate
!                                f_handling  = sta:HandlingFee
!                                f_Exchange  = sta:Exchange
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                        ! ------------------------------------------------------------------------
!!                        if tmp:AccessoryExists = true and job:warranty_charge_type = 'WARRANTY (MFTR)'
!!
!!                            if tmp:TradeAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!!                                ! ARC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_Exchange = 0
!!                                else
!!                                    f_Exchange = 0
!!                                    f_handling  = 0
!!                                end
!!                            else
!!                                ! RRC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_handling  = 0
!!                                else
!!                                    f_handling  = 0
!!                                end
!!                            end
!!                        else
!                            If job:Exchange_Unit_Number <> 0 AND jobe:ExchangedATRRC = TRUE
!                                !jobe:ExchangeRate = Exchange"
!                                f_handling  = 0
!                            Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                                IF SentToHub(job:Ref_Number)
!                                    f_Exchange = 0
!                                    !jobe:HandlingFee  = Handling"
!                                ELSE
!                                    f_Exchange = 0
!                                    f_handling  = 0
!                                END
!                            End
!                        !End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                        ! ------------------------------------------------------------------------
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'E'
!            If job:estimate = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!!                If job:ignore_estimate_charges = 'YES'
!!                    Return
!!                End!If job:ignore_estimate_charges = 'YES'
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCRate   = 0
!                    f_RRCParts  = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate  = 0
!                        f_RRCParts = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!                            save_epr_id = access:estparts.savefile()
!                            access:estparts.clearkey(epr:part_number_key)
!                            epr:ref_number  = job:ref_number
!                            set(epr:part_number_key,epr:part_number_key)
!                            loop
!                                if access:estparts.next()
!                                   break
!                                end !if
!                                if epr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += epr:sale_cost * epr:quantity
!                                RRCParts_Total$ += epr:RRCSaleCost * epr:Quantity
!                            end !loop
!                            access:estparts.restorefile(save_epr_id)
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                        End!If TRA:ZeroChargeable = 'YES'
!
!                        If tra:invoice_sub_accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:estimate = 'YES'
!    End!Case f_type
!
DateCodeValidation   PROCEDURE  (func:Manufacturer,func:DateCode,func:DateBooked) ! Declare Procedure
tmp:YearCode         STRING(30)                            !Year COde
tmp:MonthCode        STRING(30)                            !Month Code
tmp:Year             STRING(30)                            !Year
tmp:Month            STRING(30)                            !Month
  CODE
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer

    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

    Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


    !Now depending on the manufacturer determines where the
    !The date code is picked up from
    DoWeekCheck# = 0
    Case func:Manufacturer
        Of 'ALCATEL'
            tmp:YearCode    = Sub(func:DateCode,3,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'ERICSSON'
            tmp:YearCode    = Sub(func:DateCode,1,2)
            tmp:MonthCode   = Sub(func:DateCode,3,2)
            DoWeekCheck#    = 1
        Of 'PHILIPS'
            tmp:YearCode    = Sub(func:DateCode,5,2)
            tmp:MonthCode   = Sub(func:DateCode,7,2)
            DoWeekCheck#    = 1
        Of 'SAMSUNG'
            tmp:YearCode    = Sub(func:DateCode,4,1)
            tmp:MonthCode   = Sub(func:DateCode,5,1)
        Of 'BOSCH'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'SIEMENS'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode    = Sub(func:DateCode,2,1)
        Of 'MOTOROLA'
            tmp:YearCode    = Sub(func:DateCode,5,1)
            tmp:MonthCode   = Sub(func:DateCode,6,1)
    End !Case func:Manufacturer

    If DoWeekCheck#
        tmp:Year    = tmp:YearCode
        StartOfYear# = Deformat('1/1/' & tmp:Year,@d5)

        DayOfYear#   = StartOfYear# + (tmp:MonthCode * 7)

        tmp:Year    = Year(DayOfYear#)
        tmp:Month   = Month(DayOfYear#)

    Else
        Access:MANUDATE.Clearkey(mad:DateCodeKey)
        mad:Manufacturer    = func:Manufacturer
        If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Sub(func:DateCode,1,1) & Clip(tmp:MonthCode) & Clip(tmp:YearCode)
        Else !If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Clip(tmp:YearCode) & Clip(tmp:MonthCode)
        End !If func:Manufacturer = 'ALCATEL'

        If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Found
            tmp:Year    = mad:TheYear
            tmp:Month   = mad:TheMonth

        Else! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
    End !If DoWeekCheck#

    Loop x# = 1 To man:ClaimPeriod
        tmp:Month += 1
        If tmp:Month > 12
            tmp:Month = 1
            tmp:Year += 1
        End !If tmp:Month > 12
    End !Loop x# = 1 To man:ClaimDate

    If tmp:Year < Year(func:DateBooked)
        !POP Required
        Return Level:Fatal
    End !If tmp:Year > func:DateBooked
    If tmp:Year = Year(func:DateBooked) And tmp:Month < Month(func:DateBooked)
        Return Level:Fatal
    End !If tmp:Year = func:DateBooked And tmp:Month < Month(func:DateBooked)

    Return Level:Benign
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
SendEmail PROCEDURE (string pEmailFrom, string pEmailTo, string pEmailSubject, string pEmailCC, string pEmailBcc, string pEmailFileList, string pEmailMessageText)

FilesOpened          BYTE                                  !
EmailServer          STRING(80)                            !
EmailPort            USHORT                                !
EmailFrom            STRING(252)                           !
EmailTo              STRING(1024)                          !
EmailSubject         STRING(252)                           !
EmailCC              STRING(1024)                          !
EmailBCC             STRING(1024)                          !
EmailFileList        STRING(1024)                          !
EmailMessageText     STRING(16384)                         !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Email_Spacer         USHORT                                !
window               WINDOW('Sending Email'),AT(,,135,29),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),RESIZE
                       BUTTON('&Send'),AT(9,8,51,16),USE(?EmailSend),LEFT,TIP('Send Email Now')
                       BUTTON('Close'),AT(69,8,51,16),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisSendEmail        CLASS(NetEmailSend)                   ! Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SendEmail')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EmailSend
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(window)                                        ! Open window
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  !Window{prop:Hide} = 1
  !post(event:accepted,?EmailSend)
                                               ! Generated by NetTalk Extension (Start)
  ThisSendEmail.init()
  if ThisSendEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ! Generated by NetTalk Extension
  ThisSendEmail.OptionsMimeTextTransferEncoding = '7bit'           ! '7bit', '8bit' or 'quoted-printable'
  ThisSendEmail.OptionsMimeHtmlTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
  Do DefineListboxStyle
  INIMgr.Fetch('SendEmail',window)                         ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisSendEmail.Kill()                      ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  IF SELF.Opened
    INIMgr.Update('SendEmail',window)                      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?EmailSend
      ThisWindow.Update
      EmailTo          = pEmailTo
      EmailFrom        = pEmailFrom
      EmailCC          = pEmailCC
      EmailBCC         = pEmailBCC
      EmailSubject     = pEmailSubject
      EmailFileList    = pEmailFileList
      EmailMessageText = pEmailMessageText
      ! Generated by NetTalk Extension
      ThisSendEmail.Server = def:EmailServerAddress
      ThisSendEmail.Port = def:EmailServerPort
      ThisSendEmail.From = EmailFrom
      ThisSendEmail.ToList = EmailTo
      ThisSendEmail.ccList = EmailCC
      ThisSendEmail.bccList = EmailBCC
      
      ThisSendEmail.Subject = EmailSubject
      ThisSendEmail.AttachmentList = EmailFileList
      ThisSendEmail.SetRequiredMessageSize (0, len(clip(EmailMessageText)), 0) ! You must call this function before populating self.MessageText  #ELSIF ( <> '')
      if ThisSendEmail.Error = 0
        ThisSendEmail.MessageText = EmailMessageText
        display()
        ThisSendEmail.SendMail(NET:EMailMadeFromPartsMode)
        display()
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisSendEmail.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ! Generated by NetTalk Extension
      if records (ThisSendEmail.DataQueue) > 0
        if Message ('The email is still being sent.|Are you sure you want to quit?','Email Sending',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No) = Button:No
          cycle
        end
      end
      ! Generated by NetTalk Extension
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisSendEmail.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
  PARENT.ErrorTrap(errorStr,functionName)
  !post(event:closewindow)


ThisSendEmail.MessageSent PROCEDURE


  CODE
  PARENT.MessageSent
  !post(event:closewindow)

PickPaperwork        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickPaperwork')
  loc:formname = 'PickPaperwork_frm'
  WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickPaperwork','')
    p_web._DivHeader('PickPaperwork',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickPaperwork',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickPaperwork',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickPaperwork',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickPaperwork',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickPaperwork',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickPaperwork',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickPaperwork_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickPaperwork_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Finished'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickPaperwork_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickPaperwork_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickPaperwork_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickPaperwork" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickPaperwork" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickPaperwork" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickPaperwork">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickPaperwork">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickPaperwork')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickPaperwork')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickPaperwork'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickPaperwork')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickPaperwork_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormTable')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textFinished
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textFinished
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::break1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::break1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GetSessionValue('Hide:PrintJobCard') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::urlJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::urlJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PrintJobReceipt') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::break2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::break2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PrintJobReceipt') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::url:JobReceipt
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::url:JobReceipt
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::textFinished  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textFinished',p_web.GetValue('NewValue'))
    do Value::textFinished
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textFinished  Routine
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('textFinished') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('All the document(s) are now available to download below.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textFinished  Routine
    loc:comment = ''
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('textFinished') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::break1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('break1',p_web.GetValue('NewValue'))
    do Value::break1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::break1  Routine
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('break1') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('<br>',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::break1  Routine
    loc:comment = ''
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('break1') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::urlJobCard  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('urlJobCard',p_web.GetValue('NewValue'))
    do Value::urlJobCard
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::urlJobCard  Routine
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('urlJobCard') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Download Job Card'),'\reports\JobCard_' & Format(p_web.GSV('tmp:JobNumber'), @n08) & '.pdf','_blank',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::urlJobCard  Routine
    loc:comment = ''
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('urlJobCard') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::break2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('break2',p_web.GetValue('NewValue'))
    do Value::break2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::break2  Routine
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('break2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('<br>',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::break2  Routine
    loc:comment = ''
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('break2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::url:JobReceipt  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('url:JobReceipt',p_web.GetValue('NewValue'))
    do Value::url:JobReceipt
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::url:JobReceipt  Routine
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('url:JobReceipt') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Download Job Receipt'),'\reports\JobReceipt_' & Format(p_web.GSV('tmp:JobNumber'), @n08) & '.pdf','_blank',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::url:JobReceipt  Routine
    loc:comment = ''
  p_web._DivHeader('PickPaperwork_' & p_web._nocolon('url:JobReceipt') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickPaperwork_form:ready_',1)
  p_web.SetSessionValue('PickPaperwork_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickPaperwork',0)

PreCopy  Routine
  p_web.SetValue('PickPaperwork_form:ready_',1)
  p_web.SetSessionValue('PickPaperwork_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickPaperwork',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickPaperwork_form:ready_',1)
  p_web.SetSessionValue('PickPaperwork_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickPaperwork:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickPaperwork_form:ready_',1)
  p_web.SetSessionValue('PickPaperwork_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickPaperwork:Primed',0)
  p_web.setsessionvalue('showtab_PickPaperwork',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickPaperwork_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PickPaperwork_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickPaperwork:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
