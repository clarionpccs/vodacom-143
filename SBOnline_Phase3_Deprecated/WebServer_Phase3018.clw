

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3018.INC'),ONCE        !Local module procedure declarations
                     END


ForceInvoiceText     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Invoice Text
    If (def:Force_Invoice_Text = 'B' And func:Type = 'B') Or |
        (def:Force_Invoice_Text <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Invoice_Text = 'B'
    Return Level:Benign
ForceIncomingCourier PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Incoming Courier
    If (def:Force_Incoming_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Incoming_Courier <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Incoming_Courier = 'B'
    Return Level:Benign
ForceIMEI            PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !ESN
    If (def:Force_ESN = 'B' And func:Type = 'B') Or |
        (def:Force_ESN <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_ESN = 'B'
    Return Level:Benign
ForceFaultDescription PROCEDURE  (func:Type)               ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFIles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    !Fault Description
    If (def:Force_Fault_Description = 'B' and func:Type = 'B') Or |
        (def:Force_Fault_Description <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Fault_Description = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
ForceFaultCodes      PROCEDURE  (func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,func:Type,fManufacturer) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    !(func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,fManufacturer)

    If func:Type = 'C' or func:type = 'X'
        If func:ChargeableJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:CChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:CRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = func:CRepairType
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
! Deleted (DBH 20/05/2006) #6733 - Do not check the manufacturer for char fault codes
!                                 Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!                                 man:Manufacturer    = fManufacturer
!                                 If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Found
!                                     If man:ForceCharFaultCodes
!                                         Return Level:Fatal
!                                     End !If man:ForceCharFaultCodes
!                                 Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Error
!                                 End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
! End (DBH 20/05/2006) #6733
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                    Else !If func:CRepairType <> ''
                        Return Level:Fatal
                    End !If func:CRepairType <> ''
                End !If cha:Force_Warranty = 'YES'
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Chargeable_Job = 'YES'
    End !If func:Type = 'C' or func:type = 'X'

    If func:Type = 'W' or func:Type = 'X'
        If func:WarrantyJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:WChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:WRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = func:WRepairType
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                    Else !If WRetairType <> ''
                        Return Level:Fatal
                    End !If WRetairType <> ''
                End !If cha:Force_Warranty = 'YES'

            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Warranty_Job = 'YES'
    End !If func:Type = 'W' or func:Type = 'X'
    Return Level:Benign
ForceDOP             PROCEDURE  (func:TransitType,func:Manufacturer,func:WarrantyJob,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
TRANTYPE::State  USHORT
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    !Date Of Purchase
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        error# = 0
        If trt:force_dop = 'YES' And func:WarrantyJob = 1
            Return# = 1
        End !If trt:force_dop = 'YES'
    end

    If Return# = 0
        !Only check this if it hasn't already been forced by the Transit Type
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = func:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            IF man:DOPCompulsory And func:WarrantyJob = 1
                Return Level:Fatal
            End !IF man:DOPCompulsory and job:dop = '' and func:Type = 'C'
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    End ! If Return# = 0
    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRANTYPE::State = Access:TRANTYPE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRANTYPE::State <> 0
    Access:TRANTYPE.RestoreFile(TRANTYPE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRANTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRANTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     Access:TRANTYPE.Close
     Access:MANUFACT.Close
     FilesOpened = False
  END
ForceDeliveryPostcode PROCEDURE  (func:Type)               ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Delivery Postcode
    If (def:ForceDelPostcode = 'B' And func:Type = 'B') Or |
        (def:ForceDelPostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForceDelPostcode = 'B'
    Return Level:Benign
ForceCustomerName    PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Customer Name
    If (def:Customer_Name = 'B' and func:Type = 'B') Or |
         (def:Customer_Name <> 'I' and func:Type = 'C')
         If CustomerNameRequired(func:AccountNumber) = Level:Benign
            Return Level:Fatal
         End!If CustomerNameRequired(job:Account_Number) = Level:Benign
    End!If def:Customer_Name = 'B'
    Return Level:Benign
ForceCourier         PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    !Incoming Courier
    If (def:Force_Outoing_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Outoing_Courier <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Incoming_Courier = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     FilesOpened = False
  END
ForceColour          PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0

    !Colour!
    If (def:ForceCommonFault = 'B' And func:Type = 'B') Or |
        (def:ForceCommonFault <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Unit_Type = 'B'

    Do RestoreFiles
    Do CloseFiles

    Return Return#
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
