

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3008.INC'),ONCE        !Req'd for module callout resolution
                     END


MQProcess            PROCEDURE  (String f:IMEINumber,*String f:POP,*Date f:DOP,*Byte f:Error,*String f:ErrorMessage,String f:OneYearWarranty) ! Declare Procedure
    include('MQProcess.inc','Local Data')
  CODE
    include('MQProcess.inc','Processed Code')
IsUnitLiquidDamaged  PROCEDURE  (String f:ESN, Long f:JobNumber,<Byte ignoreARCCheck>) ! Declare Procedure
tmp:JobDate          DATE                                  !Job Date
  CODE
    Return vod.IsUnitLiquidDamage(f:ESN,f:JobNumber,ignoreARCCheck)
IsOneYearWarranty    PROCEDURE  (STRING manufacturer,STRING modelNumber) ! Declare Procedure
  CODE
    RETURN vod.OneYearWarranty(manufacturer,modelNumber)
CopyDOPFromBouncer   PROCEDURE  (String f:Manufacturer, Date f:PreviousDOP, *Date f:DOP,  String f:IMEINumber, String f:Bouncer, *Byte f:Found) ! Declare Procedure
    include('CopyDOPFromBouncer.inc','Local Data')
  CODE
    include('CopyDOPFromBouncer.inc','Processed Code')
ReleasedForDespatch  PROCEDURE  (f:JobNumber)              ! Declare Procedure
AUDIT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles
    Return# = 0
    Access:AUDIT.Clearkey(aud:Action_Key)
    aud:Ref_Number = f:JobNumber
    aud:Action = 'OUT OF REGION: RELEASED FOR DESPATCH'
    Set(aud:Action_Key,aud:Action_Key)
    Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> f:JobNumber
            Break
        End ! If aud:Ref_Number <> job:Ref_Number
        If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
            Break
        End ! If aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH'
        Return# = 1
        Break
    End ! Loop
    do restoreFiles
    do closeFiles
    
    Return Return#
SaveFiles  ROUTINE
  AUDIT::State = Access:AUDIT.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF AUDIT::State <> 0
    Access:AUDIT.RestoreFile(AUDIT::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT.Close
     FilesOpened = False
  END
MultipleBatchDespatch PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locErrorMessage      STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locAccessoryErrorMessage STRING(255)                       !
locAccessoryPassword STRING(30)                            !
locAuditTrail        STRING(255)                           !
FilesOpened     Long
MULDESPJ::State  USHORT
MULDESP::State  USHORT
LOAN::State  USHORT
EXCHAMF::State  USHORT
COURIER::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('MultipleBatchDespatch')
  loc:formname = 'MultipleBatchDespatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('MultipleBatchDespatch','')
    p_web._DivHeader('MultipleBatchDespatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      ROUTINE
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('locSecurityPackNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('Hide:Accessories',1)
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')
    p_web.SSV('AccessoryConfirmationRequired','')
    p_web.SSV('AccessoriesValidated','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('Hide:AddToBatchButton',1)
    p_web.SSV('Hide:ValidateAccessoriesButton',0)
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locSecurityPackNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('Hide:Accessories')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('Hide:AddToBatchButton')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('UnitValidated')
ValidateUnitDetails         ROUTINE
DATA
txtJobInUse EQUATE('Error! The selected job is in use.')
txtMissingJob       EQUATE('Error! Cannot find the selected job.')
txtDespatchString   EQUATE('Error! The selected job is not ready for despatch.')
txtOutofRegion    EQUATE('Error! The delivery address is outside your region.')
txtNotPaid  EQUATE('Error! The selected job has not been paid.')
txtNotInvoiced      EQUATE('Error! The selected job has not been invoiced.')
txtNoCourier        EQUATE('Error! The selected unit does not have a courier assigned to it.')
txtLoanNotReturned  EQUATE('Error! The loan unit attached to this job has not been returned.')
txtAlreadyInBatch   EQUATE('Error! The selected job is already on a batch.')
txtWrongIMEI        EQUATE('Error! I.M.E.I. Number does not match the selected job.')
txtWrongExchangeIMEI        EQUATE('Error! I.M.E.I. Number does not match the selected job''s Exchange Unit.')
txtWrongLoanIMEI    EQUATE('Error! I.M.E.I. Number does not match the selected job''s Loan Unit.')
txtIndividualOnly   EQUATE('Error! The selected job can only be despatched individually and not as part of a batch.')
txtAccountOnStop    EQUATE('Error! Cannot despatch. The selected account is "on stop".')
locDespatchType     STRING(3)
CODE
    ClearTagFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end
    
    p_web.SSV('locErrorMessage','')
    IF (JobInUse(p_web.GSV('locJobNumber')))
        p_web.SSV('locErrorMessage',txtJobInUse)
            
        EXIT
    END
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage',txtMissingJob)
        EXIT
    END
    
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey)) 
    END
    
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END
    
    
    IF (p_web.GSV('BookingSite') = 'RRC')
        locDespatchType = jobe:DespatchType
        IF (Inlist(jobe:DespatchType,'JOB','EXC','LOA') = 0)
            p_web.SSV('locErrorMessage',txtDespatchString)
            EXIT
        END
        
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            IF (HubOutOfRegion(p_web.GSV('BookingAccount'),sub:Hub) = 1)
                IF (ReleasedForDespatch(job:Ref_Number) = 0)
                    p_web.SSV('locErrorMessage',txtOutOfRegion) 
                    EXIT
                END
            END
        END
        
        
    ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
        locDespatchType = job:Despatch_Type
        IF (InList(job:Despatch_Type,'JOB','EXC','LOA') = 0)
            p_web.SSV('locErrorMessage',txtDespatchString)
            EXIT
        END
        CASE job:Despatch_Type
        OF 'JOB'
            IF (job:Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        OF 'EXC'
            IF (job:Exchange_Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        OF 'LOA'
            IF (job:Loan_Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        END
        
    END ! IF (p_web.GSV('BookingSite') = 'RRC')
    
    ! Job Been Paid/Invoiced?
    IF (job:Chargeable_Job = 'YES')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            IF (tra:Despatch_Paid_Jobs = 'YES' AND NOT JobPaid(job:Ref_Number))
                p_web.SSV('locerrorMessage',txtNotPaid)
                EXIT
            END
            IsJobInvoiced(p_web)
            IF (tra:Despatch_Invoiced_Jobs = 'YES' AND p_web.GSV('IsJobInvoiced') <> 1)
                p_web.SSV('locErrorMessage',txtNotInvoiced)
                EXIT
            END
            
        END
        
    END
    
    ! Have courier attached?
    Access:COURIER.ClearKey(cou:Courier_Key)
    CASE locDespatchType
    OF 'JOB'
        IF (job:Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Courier
    OF 'EXC'
        IF (job:Exchange_Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Exchange_Courier
    OF 'LOA'
        IF (job:Loan_Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Loan_Courier
    END
    IF (Access:COURIER.tryfetch(cou:Courier_Key))
    END
    
    
    !Has the job got a loan attached?
    If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1))
            ! #11817 Only stop despatch if RRC, or ARC back to customer. (Bryan: 11/05/2011)
            If job:Loan_Unit_Number <> 0 And locDespatchType = 'JOB'
                p_web.SSV('locErrorMessage',txtLoanNotReturned)
                EXIT
            End !If job:Loan_Unit_Number <> 0
        END ! IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1))
        
    End !If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1    
    
    IF (vod.IsTheJobAlreadyInBatch(job:Ref_Number,p_web.GSV('BookingAccount')))
        p_web.SSV('locErrorMessage',txtAlreadyInBatch)
        EXIT
    END
    p_web.SSV('AccessoryRefNumber',job:Ref_Number)
            
    ! Check IMEI Number
    CASE locDespatchType
    OF 'JOB'
        IF (p_web.GSV('locIMEINumber') <> job:ESN)
            p_web.SSV('locErrorMessage',txtWrongIMEI)
            EXIT
        END
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
    OF 'EXC'
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage',txtWrongExchangeIMEI)
            EXIT
        ELSE
            IF (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage',txtWrongExchangeIMEI)
                EXIT
            END
            
        END
        
    OF 'LOA'
        Access:LOAN.ClearKey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage',txtWrongLoanIMEI)
            EXIT
        ELSE
            IF (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage',txtWrongLoanIMEI)
                EXIT
            END
            p_web.SSV('AccessoryRefNumber',loa:Ref_Number)
        END
        
    END
    
    ! Check if it has to be indivdual despatch only.
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                    p_web.SSV('locErrorMessage',txtAccountOnStop)
                    EXIT
                End !If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                If sub:UseCustDespAdd = 'YES'
                    p_web.SSV('locErrorMessage',txtIndividualOnly)
                    EXIT
                End !If sub:UseCustDespAdd = 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                    p_web.SSV('locErrorMessage',txtAccountOnStop)
                    EXIT
                End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                If tra:UseCustDespAdd = 'YES'
                    p_web.SSV('locErrorMessage',txtIndividualOnly)
                    EXIT
                End !If tra:UseCustDespAdd = 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        END
    END
    
    
! A Ok
    p_web.SSV('Hide:Accessories',0)
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('DespatchType',locDespatchType)
    p_web.SSV('UnitValidated',1)
    p_web.SSV('ValidateButtonText','Despatch Another Unit')        
    
    p_web.FileToSessionQueue(JOBS)
    p_web.FileToSessionQueue(WEBJOB)
    p_web.FileToSessionQueue(JOBSE)
    p_web.FileToSessionQueue(COURIER)
        
    
    
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHAMF)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHAMF)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SetValue('MultipleBatchDespatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locSecurityPackNumber')
    locSecurityPackNumber = p_web.GetValue('locSecurityPackNumber')
    p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('MultipleBatchDespatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Add To Batch
  IF p_web.IfExistsValue('Action')
      p_web.StoreValue('Action')
      IF p_web.IfExistsValue('BatchNumber')
          p_web.StoreValue('BatchNumber')
          IF (p_web.GSV('Action') = 'AddToBatch')
              ! Start
              IF (p_web.GSV('BatchNumber') > 0)
                  Access:MULDESP.ClearKey(muld:RecordNumberKey)
                  muld:RecordNumber = p_web.GSV('BatchNumber')
                  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
                      IF (p_web.GSV('job:Ref_Number') > 0)
                          ! Check job isn't already there
                          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                          mulj:RefNumber = muld:RecordNumber
                          mulj:JobNumber = p_web.GSV('job:Ref_Number')
                          IF (Access:MULDESPJ.TryFetch(mulj:JobNumberKey))
                              IF (Access:MULDESPJ.PrimeRecord() = Level:Benign)
                                  mulj:RefNumber = muld:RecordNumber
                                  mulj:JobNumber = p_web.GSV('job:Ref_Number')
                                  mulj:IMEINumber = p_web.GSV('job:ESN')
                                  mulj:MSN = p_web.GSV('job:MSN')
                                  mulj:AccountNumber = p_web.GSV('job:Account_Number')
                                  mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
                                  IF (p_web.GSV('BookingSite') = 'RRC')
                                      CASE p_web.GSV('jobe:DespatchType')
                                      OF 'JOB'
                                          mulj:Courier = p_web.GSV('job:Courier')
                                      OF 'EXC'
                                          mulj:Courier = p_web.GSV('job:Exchange_Courier')
                                      OF 'LOA'
                                          mulj:Courier = p_web.GSV('job:Loan_Courier')
                                      END
                  
                                  ELSE
                                      CASE p_web.GSV('job:Despatch_Type')
                                      OF 'JOB'
                                          mulj:Courier = p_web.GSV('job:Courier')
                                      OF 'EXC'
                                          mulj:Courier = p_web.GSV('job:Exchange_Courier')
                                      OF 'LOA'
                                          mulj:Courier = p_web.GSV('job:Loan_Courier')
                                      END
  
                                  END
                                  IF (muld:BatchType = 'TRA')
                                      mulj:AccountNumber = p_web.GSV('job:Account_Number')
                                      mulj:Courier = muld:Courier
                                  END
                                  IF (Access:MULDESPJ.TryInsert() = Level:Benign)
                                      CountBatch# = 0
                                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                                      mulj:RefNumber = muld:RecordNumber
                                      Set(mulj:JobNumberKey,mulj:JobNumberKey)
                                      Loop
                                          If Access:MULDESPJ.NEXT()
                                              Break
                                          End !If
                                          If mulj:RefNumber <> muld:RecordNumber      |
                                              Then Break.  ! End If
                                          CountBatch# += 1
                                      End !Loop
  
                                      muld:BatchTotal = CountBatch#
                                      Access:MULDESP.Update()
                                  ELSE
                                      Access:MULDESPJ.CancelAutoInc()
                                  END
                              END
                          END
                      END
                  END
              END
          END
      END
  END
        
  p_web.DeleteSessionValue('Action')
  p_web.DeleteSessionValue('BatchNumber')
  p_web.DeleteSessionValue('job:Ref_Number')
  
  
  ClearTagFile(p_web)
  DO ClearVariables
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locSecurityPackNumber = p_web.RestoreValue('locSecurityPackNumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferMultipleBatchDespatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('MultipleBatchDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('MultipleBatchDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('MultipleBatchDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="MultipleBatchDespatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="MultipleBatchDespatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="MultipleBatchDespatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Multiple Batch Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Multiple Batch Despatch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_MultipleBatchDespatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_MultipleBatchDespatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_MultipleBatchDespatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Batches In Progress') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Input Batch') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_MultipleBatchDespatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_MultipleBatchDespatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('MultipleBatchDespatch_BrowseBatchesInProgress_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('MultipleBatchDespatch_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_MultipleBatchDespatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Batches In Progress') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Batches In Progress')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseBatchesInProgress
      do Comment::BrowseBatchesInProgress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Input Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Input Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSecurityPackNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_MultipleBatchDespatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAddToBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonAddToBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseBatchesInProgress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseBatchesInProgress',p_web.GetValue('NewValue'))
    do Value::BrowseBatchesInProgress
  Else
    p_web.StoreValue('muld:RecordNumber')
  End
  do SendAlert

Value::BrowseBatchesInProgress  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseBatchesInProgress --
  p_web.SetValue('BrowseBatchesInProgress:NoForm',1)
  p_web.SetValue('BrowseBatchesInProgress:FormName',loc:formname)
  p_web.SetValue('BrowseBatchesInProgress:parentIs','Form')
  p_web.SetValue('_parentProc','MultipleBatchDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('MultipleBatchDespatch_BrowseBatchesInProgress_embedded_div')&'"><!-- Net:BrowseBatchesInProgress --></div><13,10>'
    p_web._DivHeader('MultipleBatchDespatch_' & lower('BrowseBatchesInProgress') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('MultipleBatchDespatch_' & lower('BrowseBatchesInProgress') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseBatchesInProgress --><13,10>'
  end
  do SendPacket

Comment::BrowseBatchesInProgress  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('BrowseBatchesInProgress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locJobNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''multiplebatchdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''multiplebatchdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSecurityPackNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Security Pack No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSecurityPackNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSecurityPackNumber',p_web.GetValue('NewValue'))
    locSecurityPackNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSecurityPackNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSecurityPackNumber',p_web.GetValue('Value'))
    locSecurityPackNumber = p_web.GetValue('Value')
  End
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locSecurityPackNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locSecurityPackNumber  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSecurityPackNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSecurityPackNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackNumber'',''multiplebatchdespatch_locsecuritypacknumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSecurityPackNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackNumber',p_web.GetSessionValueFormat('locSecurityPackNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_value')

Comment::locSecurityPackNumber  Routine
      loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonProcessJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonProcessJob',p_web.GetValue('NewValue'))
    do Value::buttonProcessJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  DO ValidateUnitDetails
  do Value::buttonProcessJob
  do SendAlert
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Value::locJobNumber  !1
  do Value::locSecurityPackNumber  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonFailAccessory  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonAddToBatch  !1

Value::buttonProcessJob  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonProcessJob'',''multiplebatchdespatch_buttonprocessjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ProcessJob',p_web.GSV('ValidateButtonText'),'button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_value')

Comment::buttonProcessJob  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('Hide:Accessories') = 1,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','MultipleBatchDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('MultipleBatchDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('MultipleBatchDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('MultipleBatchDespatch_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  p_web.SSV('Hide:AddToBatchButton',1)
  
  ! Validate
  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  AccessoryCheck(p_web)
  Case p_web.GSV('AccessoryCheck:Return')
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end            
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')    
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end                
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('Hide:AddToBatchButton',0)
      !DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::buttonConfirmMismatch  !1
  do Value::buttonFailAccessory  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonAddToBatch  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''multiplebatchdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('NewValue'))
    locAccessoryMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('Value'))
    locAccessoryMessage = p_web.GetValue('Value')
  End

Value::locAccessoryMessage  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1)
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value')

Comment::locAccessoryMessage  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('NewValue'))
    locAccessoryErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('Value'))
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End

Value::locAccessoryErrorMessage  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1)
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value')

Comment::locAccessoryErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmMismatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryErrorMessage','Password Required')    
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryErrorMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
              p_web.SSV('Hide:AddToBatchButton',0)
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      p_web.SSV('Hide:AddToBatchButton',0)
      !DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonAddToBatch  !1
  do Value::buttonFailAccessory  !1
  do Value::buttonProcessJob  !1
  do Value::buttonValidateAccessories  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryPassword  !1

Value::buttonConfirmMismatch  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''multiplebatchdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmAccessoryValidation','Confirm Access. Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'images/tick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value')

Comment::buttonConfirmMismatch  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFailAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFailAccessory',p_web.GetValue('NewValue'))
    do Value::buttonFailAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Fail Accessory Check
  p_web.SSV('GetStatus:StatusNumber',850)
  p_web.SSV('GetStatus:Type',p_web.GSV('DespatchType'))
  GetStatus(p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
      IF (tag:tagged = 1)
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tag:TaggedValue)
      END
  END
  
  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web)
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonAddToBatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonProcessJob  !1
  do Value::buttonValidateAccessories  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryPassword  !1
  do Value::locErrorMessage  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::locSecurityPackNumber  !1

Value::buttonFailAccessory  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''multiplebatchdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FailAccessory','Fail Accessory Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'images/cross.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value')

Comment::buttonFailAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPassword  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt')

Validate::locAccessoryPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('NewValue'))
    locAccessoryPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('Value'))
    locAccessoryPassword = p_web.GetValue('Value')
  End
    locAccessoryPassword = Upper(locAccessoryPassword)
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  do Value::locAccessoryPassword
  do SendAlert

Value::locAccessoryPassword  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locAccessoryPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''multiplebatchdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value')

Comment::locAccessoryPassword  Routine
      loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAddToBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAddToBatch',p_web.GetValue('NewValue'))
    do Value::buttonAddToBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAddToBatch  Routine
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_value',Choose(p_web.GSV('Hide:AddToBatchButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:AddToBatchButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddToBatch','Add Job To Batch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormAddToBatch')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_value')

Comment::buttonAddToBatch  Routine
    loc:comment = ''
  p_web._DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_comment',Choose(p_web.GSV('Hide:AddToBatchButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:AddToBatchButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('MultipleBatchDespatch_BrowseBatchesInProgress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseBatchesInProgress
      else
        do Value::BrowseBatchesInProgress
      end
  of lower('MultipleBatchDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('MultipleBatchDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('MultipleBatchDespatch_locSecurityPackNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackNumber
      else
        do Value::locSecurityPackNumber
      end
  of lower('MultipleBatchDespatch_buttonProcessJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonProcessJob
      else
        do Value::buttonProcessJob
      end
  of lower('MultipleBatchDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('MultipleBatchDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('MultipleBatchDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('MultipleBatchDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)

PreCopy  Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('MultipleBatchDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('MultipleBatchDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
          locAccessoryPassword = Upper(locAccessoryPassword)
          p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locSecurityPackNumber')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')
JobPaid              PROCEDURE  (fJobNumber)               ! Declare Procedure
tmp:VAT              REAL                                  !
tmp:Total            REAL                                  !
tmp:AmountPaid       REAL                                  !
tmp:Paid             BYTE                                  !
tmp:LabourVatRate    REAL                                  !
tmp:PartsVatRate     REAL                                  !
tmp:Balance          REAL                                  !
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBS_ALIAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openfiles
    do savefiles
    
    
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = fJobNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
    End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
    
    tmp:Paid = 0
    tmp:AmountPaid = 0
    Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
    jpt:Ref_Number = job_ali:Ref_Number
    Set(jpt:All_Date_Key,jpt:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT.Next()
            Break
        End ! If Access:JOBPAYMT.Next()
        If jpt:Ref_Number <> job_ali:Ref_Number
            Break
        End ! If jpt:RefNumber <> f:RefNumber
        tmp:AmountPaid += jpt:Amount
    End ! Loop
    
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job_ali:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
    
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
    
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job_ali:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
    
    If job_ali:Invoice_Number > 0
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! If job_ali:Invoice_Number > 0
    
    If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:InvRRCCLabourCost *(inv:Vat_Rate_Labour / 100) + |
                jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts / 100) + |
                job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)
    
            tmp:Total = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job_ali:Invoice_Courier_Cost + tmp:VAT
    
        Else ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:RRCCLabourCost *(tmp:LabourVatRate / 100) + |
                jobe:RRCCPartsCost * (tmp:PartsVatRate / 100) + |
                job_ali:Courier_Cost * (tmp:LabourVatRate / 100)
    
            tmp:Total = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job_ali:Courier_Cost + tmp:VAT
    
        End ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    Else ! If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100) + |
                job_ali:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) + |
                job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)
    
            tmp:Total = job_ali:Invoice_Labour_Cost + job_ali:Invoice_Parts_Cost + job_ali:Invoice_Courier_Cost + tmp:VAT
    
        Else ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Labour_Cost * (tmp:LabourVatRate / 100) + |
                job_ali:Parts_Cost * (tmp:PartsVatRate / 100) + |
                job_ali:Courier_Cost * (tmp:LabourVatRate / 100)
    
            tmp:Total = job_ali:Labour_Cost + job_ali:Parts_Cost + job_ali:Courier_Cost + tmp:VAT
    
        End ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    End ! If glo:WebJob
    
    If tmp:Balance < 0.01
        tmp:Paid = 1
    End ! If tmp:Balance < 0
    
    do restorefiles
    do closefiles
    
    return tmp:Paid
SaveFiles  ROUTINE
  JOBPAYMT::State = Access:JOBPAYMT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBSE::State = Access:JOBSE.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  VATCODE::State = Access:VATCODE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
  INVOICE::State = Access:INVOICE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS_ALIAS::State = Access:JOBS_ALIAS.SaveFile()         ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBPAYMT::State <> 0
    Access:JOBPAYMT.RestoreFile(JOBPAYMT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBSE::State <> 0
    Access:JOBSE.RestoreFile(JOBSE::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF VATCODE::State <> 0
    Access:VATCODE.RestoreFile(VATCODE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF INVOICE::State <> 0
    Access:INVOICE.RestoreFile(INVOICE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS_ALIAS::State <> 0
    Access:JOBS_ALIAS.RestoreFile(JOBS_ALIAS::State)       ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBPAYMT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.Open                                   ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.UseFile                                ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBPAYMT.Close
     Access:JOBSE.Close
     Access:VATCODE.Close
     Access:INVOICE.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:JOBS_ALIAS.Close
     FilesOpened = False
  END
JobInUse             PROCEDURE  (fSessionValue)            ! Declare Procedure
  CODE
    rtnValue# = 0
    pointer# = Pointer(JOBS)
    Hold(JOBS,1)
    Get(JOBS,pointer#)
    if (errorcode()  = 43)
        rtnValue# = 1
    else
        relate:JOBSLOCK.open()
        
        ! First step. Remove any blanks from the file
        Access:JOBSLOCK.ClearKey(lock:JobNumberKey)
        lock:JobNumber = 0
        SET(lock:JobNumberKey,lock:JobNumberKey)
        LOOP
            IF (Access:JOBSLOCK.Next())
                BREAK
            END
            IF (lock:JobNumber <> 0)
                BREAK
            END
            Access:JOBSLOCK.DeleteRecord(0)
        END
        
        access:JOBSLOCK.clearkey(lock:jobNumberKey)
        lock:jobNUmber = job:ref_number
        if (access:JOBSLOCK.tryfetch(lock:jobNumberKey) = level:benign)
            if (lock:SessionValue <> fSessionValue)
                if (TODAY() = lock:DateLocked and clock() < (lock:timelocked + 60000)) ! 10 Minutes Time Out
                ! If it's the same user then there's no need to give the locked error?!
                    rtnValue# = 1
                end
                
            end
        end
        relate:JOBSLOCK.close()
        

    end ! if (errorcode()  = 43)
    return rtnValue#
isJobInvoiced        PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    p_web.SSV('IsJobInvoiced',0)
    if (p_web.GSV('job:Invoice_Number') > 0)
        Access:INVOICE.Clearkey(inv:invoice_Number_Key)
        inv:invoice_Number    = p_web.GSV('job:Invoice_Number')
        if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
            ! Found
            if (p_web.GSV('BookingSite') = 'RRC')
                if (inv:RRCINvoiceDate > 0)
                    p_web.SSV('IsJobInvoiced',1)
                end ! if (inv:RRCINvoiceDate > 0)
            else ! if (p_web.GSV('BookingSite') = 'RRC')
                if (inv:ARCInvoiceDate > 0)
                    p_web.SSV('IsJobInvoiced',1)
                end ! if (inv:ARCInvoiceDate > 0)
            end ! if (p_web.GSV('BookingSite') = 'RRC')
        else ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
    end !if (p_web.GSV('job:Invoice_Number') = 0)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:INVOICE.Close
     FilesOpened = False
  END
HubOutOfRegion       PROCEDURE  (f:HeadAccountNumber,f:Hub) ! Declare Procedure
TRAHUBS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    If f:HeadAccountNumber = ''
        Return 0
    End ! If f:HeadAccountNumber = ''
    If f:Hub = ''
        Return 0
    End ! If f:Hub = ''
    Do OpenFiles
    Do SaveFiles

    Found# = 0
    Access:TRAHUBS.Clearkey(trh:HubKey)
    trh:TRADEACCAccountNumber = f:HeadAccountNumber
    Set(trh:HubKey,trh:HubKey)
    Loop
        If Access:TRAHUBS.Next()
            Break
        End ! If Access:TRAHUBS.Next()
        If trh:TRADEACCAccountNumber <> f:HeadAccountNumber
            Break
        End ! If trh:TRADEACCAccountNumber <> wob:HeadAccountNumber
        Found# = 1
        If trh:Hub = f:Hub
            Found# = 2
            Break
        End ! If trh:Hub = f:Hub
    End ! Loop

    Do RestoreFiles
    Do CloseFiles
    !0 = No Relevant Hub Data
    !1 = Hub is out of region
    !2 = Found Hub.
    Return Found#

SaveFiles  ROUTINE
  TRAHUBS::State = Access:TRAHUBS.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRAHUBS::State <> 0
    Access:TRAHUBS.RestoreFile(TRAHUBS::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRAHUBS.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRAHUBS.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRAHUBS.Close
     FilesOpened = False
  END
