

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetWeb.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3022.INC'),ONCE        !Req'd for module callout resolution
                     END


PageFooter           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('PageFooter')
  loc:formname = 'PageFooter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PageFooter','')
    p_web._DivHeader('PageFooter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PageFooter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PageFooter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PageFooter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PageFooter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferPageFooter')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PageFooter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PageFooter_ChainTo')
    loc:formaction = p_web.GetSessionValue('PageFooter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PageFooter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PageFooter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PageFooter" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PageFooter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PageFooter">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PageFooter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PageFooter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PageFooter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PageFooter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PageFooter',0)

PreCopy  Routine
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PageFooter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PageFooter:Primed',0)

PreDelete       Routine
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PageFooter:Primed',0)
  p_web.setsessionvalue('showtab_PageFooter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PageFooter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PageFooter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PageFooter:Primed',0)
heading  Routine
  packet = clip(packet) & |
    '<<div id="_VersionText" class="BottomBannerText"><13,10>'&|
    '<<span class="SmallText"><<!-- Net:s:VersionNumber --><</span><13,10>'&|
    '<</div><13,10>'&|
    ''
!!! <summary>
!!! Generated from procedure template - Window
!!! Don't Forget To Update Version Number
!!! </summary>
Main PROCEDURE 

WebLog               GROUP,PRE(web)                        !
EnableLogging        LONG(1)                               !
LastGet              STRING(4096)                          !
LastPost             STRING(4096)                          !
StartDate            LONG                                  !
StartTime            LONG                                  !
PagesServed          LONG                                  !
                     END                                   !
LogQueue             QUEUE,PRE(LogQ)                       !
Port                 LONG                                  !
Date                 LONG                                  !
Time                 LONG                                  !
Desc                 STRING(4096)                          !
                     END                                   !
tmp:DataPath         STRING(255)                           !Data Path
Path:Scripts         STRING(255)                           !Path:Scripts
Path:Web             STRING(255)                           !Path:Web
Path:Styles          STRING(255)                           !Path:Styles
AmendDetails         BYTE                                  !
tmp:OpenError        BYTE(0)                               !Open Error
tmp:VersionNumber    STRING(30)                            !Version Number
s_web              &NetWebServer
Net:ShortInit      Long
loc:RequestData    Group(NetWebServerRequestDataType).
loc:OverString     String(size(loc:RequestData)),over(loc:RequestData)
Window               WINDOW('ServiceBase 3g Online'),AT(0,0,352,243),FONT('Tahoma',8,,FONT:regular),DOUBLE,ICONIZE, |
  GRAY
                       SHEET,AT(4,2,344,220),USE(?Sheet1),COLOR(COLOR:White),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Verison Number:'),AT(8,20,336,14),USE(?VersionNumber:Prompt),FONT(,12,,FONT:bold), |
  CENTER,TRN
                           IMAGE('sbonline.jpg'),AT(76,34),USE(?Image1),CENTERED
                           CHECK('Amend Defaults'),AT(144,130),USE(AmendDetails),VALUE('1','0')
                           PROMPT('Server Port'),AT(28,142),USE(?Prompt1)
                           TEXT,AT(104,142,64,10),USE(glo:WebserverPort),SINGLE,BOXED,FLAT
                           TEXT,AT(104,158,200,10),USE(glo:LocalPath),SINGLE,BOXED,DISABLE,FLAT
                           PROMPT('Local Path'),AT(28,158)
                           TEXT,AT(104,174,200,10),USE(glo:DataPath),SINGLE,BOXED,FLAT
                           BUTTON('...'),AT(308,174,12,10),USE(?LookupFile)
                           TEXT,AT(104,190,200,10),USE(Path:Web),SINGLE,BOXED,FLAT
                           BUTTON('...'),AT(308,190,12,10),USE(?LookupFile:2)
                           PROMPT('Path To "Web" Folder'),AT(28,190),USE(?Prompt1:2)
                           BUTTON('Save Changes And Close WebServer'),AT(104,204,148,14),USE(?Button4),LEFT,ICON('psave.ico')
                           PROMPT('Data Path'),AT(28,174),USE(?Prompt1:3)
                         END
                         TAB('Logging'),USE(?Tab2)
                           GROUP('Logging Group'),AT(8,16,336,202),USE(?LogGroup)
                             LIST,AT(8,20,332,98),USE(?LogQueue),VSCROLL,COLOR(,COLOR:Black,00F0F0F0h),GRID(00F0F0F0h), |
  FORMAT('51L(2)|M~Port~@n5@51L(2)|M~Date~@D17B@36L(2)|M~Time~@T4B@1020L(2)|M~Description~@s255@'), |
  FROM(LogQueue)
                             TEXT,AT(8,122,168,66),USE(web:LastGet),VSCROLL,BOXED
                             TEXT,AT(180,122,160,66),USE(web:LastPost),VSCROLL,BOXED
                             OPTION('Log:'),AT(8,192,141,22),USE(web:EnableLogging),BOXED
                               RADIO('No'),AT(16,203),USE(?Option1:Radio1),VALUE('0')
                               RADIO('Screen'),AT(44,203),USE(?Option1:Radio2),VALUE('1')
                               RADIO('Screen && Disk'),AT(88,203),USE(?Option1:Radio3),VALUE('2')
                             END
                             BUTTON('Clear'),AT(152,196,45,16),USE(?Clear)
                           END
                         END
                       END
                       STRING('Started At:'),AT(4,226),USE(?StartedAt),TRN
                       STRING(@d1),AT(44,226),USE(web:StartDate),TRN
                       STRING(@t1),AT(88,226),USE(web:StartTime)
                       STRING('Pages:'),AT(160,226),USE(?Pages),TRN
                       STRING(@n14),AT(188,226),USE(web:PagesServed),TRN
                       BUTTON('Close WebServer'),AT(268,224,80,14),USE(?Cancel),LEFT,ICON('pcancel.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
p_web                CLASS(NetWebServer)                   ! Generated by NetTalk Extension (Class Definition)
AddLog                 PROCEDURE(String p_Data,<string p_ip>),DERIVED
StartNewThread         PROCEDURE(NetWebServerRequestDataType p_RequestData),DERIVED
TakeEvent              PROCEDURE(),DERIVED

                     END

FileLookup1          SelectFileClass
FileLookup4          SelectFileClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CheckOmittedWeb  routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

!s_web    &NetWebServer
locTempFolder       CSTRING(255)
  CODE
  ! Get Defaults
  glo:LocalPath = Clip(LongPath(Path()))
  tmp:OpenError = 0
  Path:Web = GETINI('DEFAULTS','pathtoweb','web',Clip(glo:LocalPath) & '\WEBSERVER.INI')
  If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') <> ''
      glo:WebserverPort = GETINI('DEFAULTS','port',,Clip(glo:LocalPath) & '\WEBSERVER.INI')
      glo:DataPath = GETINI('DEFAULTS','pathtodata',,Clip(glo:LocalPath) & '\WEBSERVER.INI')
      SetPath(Clip(glo:DataPath))
      tmp:DataPath = Path()
      
        ! Create the "temp" folder
        locTempFolder = CLIP(Path:Web) & '\temp'
        IF (NOT EXISTS(locTempFolder))
            
          rtn# = MKDir(locTempFolder)
      END 
          
  
      glo:TagFile = clip(Path:Web) & '\temp\tagfile' & clock() & '.tmp'
      glo:sbo_outfaultparts = clip(Path:Web) & '\temp\outfaultparts' & clock() & '.tmp'
      glo:tempFaultCodes = clip(Path:Web) & '\temp\faultcodes' & clock() & '.tmp'
      glo:tempAuditQueue = clip(Path:Web) & '\temp\auditqueue' & clock() & '.tmp'
      create(TempFaultCodes)
      share(TempFaultCodes)
      create(TempAuditQueue)
      share(TempAuditQueue)
  Else ! If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') = ''
      Beep(Beep:SystemHand)  ;  Yield()
      Case Message('No defaults have been setup for this WebServer.'&|
          '|You must ensure they are setup correctly before this WebServer can function correctly.','Amend Details',|
                     icon:Hand,'&OK',1,1)
          Of 1 ! &OK Button
      End!Case Message
      tmp:OpenError = 1
  End ! If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') = ''
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VersionNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
                                               ! Generated by NetTalk Extension (Start)
  do CheckOmittedWeb
    s_web &= p_web
    p_web.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
    p_web.init()
    p_web.Port = glo:WebserverPort
    p_web.Open()
    Get(p_web._SitesQueue,1)
  !---------------------------------
  s_web._SitesQueue.Defaults.WebHandlerProcName = 'WebHandler'
  s_web._SitesQueue.Defaults.DefaultPage = 'LoginForm'
  s_web._SitesQueue.Defaults.SessionExpiryAfterHS = 90001
  s_web._SitesQueue.Defaults.LoginPage = 'LoginForm'
  s_web._SitesQueue.Defaults.LoginPageIsControl = 0
  s_web._SitesQueue.Defaults.WebFolderPath = Path:Web
  If instring('\',s_web._SitesQueue.Defaults.WebFolderPath,1,1) = 0
    s_web._SitesQueue.Defaults.WebFolderPath = clip(s_web._SitesQueue.defaults.appPath) & s_web._SitesQueue.Defaults.WebFolderPath
  End
  s_web._SitesQueue.Defaults.UploadsPath = clip(s_web._SitesQueue.Defaults.WebFolderPath) & '\uploads'
  s_web._SitesQueue.Defaults.HtmlCharset = 'ISO-8859-1'
  s_web._SitesQueue.Defaults.LocatePromptText = s_web.Translate('Locate')
  s_web._SitesQueue.Defaults.scriptsdir = 'scripts'
  s_web._SitesQueue.Defaults.stylesdir  = 'styles'
  s_web.MakeFolders()
  s_web._SitesQueue.defaults.AllowAjax = 1
  s_web._SitesQueue.defaults._CheckForParseHeader = 1         ! Check for Parse Header String
  s_web._SitesQueue.defaults._CheckForParseHeaderSize = 1000  ! Check for the Parse Header in the first x bytes
  s_web._SitesQueue.defaults._CheckParseHeader = '<!-- NetWebServer --><13,10>'
  s_web._SitesQueue.defaults.securedir = 'secure'
  s_web._SitesQueue.defaults.loggedindir = 'loggedin'
  s_web._SitesQueue.defaults.InsertPromptText = s_web.Translate('Insert')
  s_web._SitesQueue.defaults.CopyPromptText = s_web.Translate('Copy')
  s_web._SitesQueue.defaults.ChangePromptText = s_web.Translate('Change')
  s_web._SitesQueue.defaults.ViewPromptText   = s_web.Translate('View')
  s_web._SitesQueue.defaults.DeletePromptText = s_web.Translate('Delete')
  s_web._SitesQueue.defaults.RequiredText = s_web.Translate('Required')
  s_web._SitesQueue.defaults.NumericText = s_web.Translate('A Number')
  s_web._SitesQueue.defaults.MoreThanText = s_web.Translate('More than or equal to')
  s_web._SitesQueue.defaults.LessThanText = s_web.Translate('Less than or equal to')
  s_web._SitesQueue.defaults.NotZeroText = s_web.Translate('Must not be Zero or Blank')
  s_web._SitesQueue.defaults.OneOfText = s_web.Translate('Must be one of')
  s_web._SitesQueue.defaults.InListText = s_web.Translate('Must be one of')
  s_web._SitesQueue.defaults.InFileText = s_web.Translate('Must be in table')
  s_web._SitesQueue.defaults.DuplicateText = s_web.Translate('Creates Duplicate Record on')
  s_web._SitesQueue.defaults.RestrictText = s_web.Translate('Unable to Delete, Child records exist in table')
  s_web._SitesQueue.Defaults.DatePicture = '@D6'
  s_web._SitesQueue.Defaults.PageHeaderTag = '<!-- Net:BannerBlank -->'
  s_web._SitesQueue.Defaults.PageFooterTag = '<!-- Net:PageFooter -->'
  s_web._SitesQueue.Defaults.PageTitle = 'ServiceBase Online'
  s_web._SitesQueue.Defaults.WebFormStyle = Net:Web:Plain
  s_web._SitesQueue.Defaults.BrowseClass = 'BrowseTable'
  s_web._SitesQueue.Defaults.PromptClass = 'FormPrompt'
  s_web._SitesQueue.Defaults.CommentClass = 'FormComments'
  s_web._SitesQueue.Defaults.BodyClass = 'PageBody'
  s_web._SitesQueue.Defaults.BodyDivClass = 'PageBodyDiv'
  s_web._SitesQueue.Defaults.BrowseHighlightColor = 314111
  s_web._SitesQueue.Defaults.BrowseOverColor = -1
  s_web._SitesQueue.Defaults.BrowseOneColor = 16777215
  s_web._SitesQueue.Defaults.BrowseTwoColor = 16315633
  s_web._SitesQueue.Defaults.UploadButton.Name = 'upload_btn'
  s_web._SitesQueue.Defaults.UploadButton.TextValue = s_web.Translate('Upload')
  s_web._SitesQueue.Defaults.UploadButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.UploadButton.ToolTip = s_web.Translate('Click here to Upload the file')
  s_web._SitesQueue.Defaults.LookupButton.Name = 'lookup_btn'
  s_web._SitesQueue.Defaults.LookupButton.TextValue = s_web.Translate('...')
  s_web._SitesQueue.Defaults.LookupButton.Class = 'LookupButton'
  s_web._SitesQueue.Defaults.LookupButton.ToolTip = s_web.Translate('Click here to Search for a value')
  s_web._SitesQueue.Defaults.SaveButton.Name = 'save_btn'
  s_web._SitesQueue.Defaults.SaveButton.TextValue = s_web.Translate('Save')
  s_web._SitesQueue.Defaults.SaveButton.Image = '/images/psave.png'
  s_web._SitesQueue.Defaults.SaveButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.SaveButton.ToolTip = s_web.Translate('Click on this to Save the form')
  s_web._SitesQueue.Defaults.CancelButton.Name = 'cancel_btn'
  s_web._SitesQueue.Defaults.CancelButton.TextValue = s_web.Translate('Cancel')
  s_web._SitesQueue.Defaults.CancelButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.CancelButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.CancelButton.ToolTip = s_web.Translate('Click on this to Cancel the form')
  s_web._SitesQueue.Defaults.DeletefButton.Name = 'deletef_btn'
  s_web._SitesQueue.Defaults.DeletefButton.TextValue = s_web.Translate('Delete')
  s_web._SitesQueue.Defaults.DeletefButton.Image = '/images/pdelete.png'
  s_web._SitesQueue.Defaults.DeletefButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.DeletefButton.ToolTip = s_web.Translate('Click here to Delete this record')
  s_web._SitesQueue.Defaults.CloseButton.Name = 'close_btn'
  s_web._SitesQueue.Defaults.CloseButton.TextValue = s_web.Translate('Close')
  s_web._SitesQueue.Defaults.CloseButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.CloseButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.CloseButton.ToolTip = s_web.Translate('Click here to Close this form')
  s_web._SitesQueue.Defaults.InsertButton.Name = 'insert_btn'
  s_web._SitesQueue.Defaults.InsertButton.TextValue = s_web.Translate('Insert')
  s_web._SitesQueue.Defaults.InsertButton.Image = '/images/pinsert.png'
  s_web._SitesQueue.Defaults.InsertButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.InsertButton.ToolTip = s_web.Translate('Click here to Insert a new record')
  s_web._SitesQueue.Defaults.ChangeButton.Name = 'change_btn'
  s_web._SitesQueue.Defaults.ChangeButton.TextValue = s_web.Translate('Change')
  s_web._SitesQueue.Defaults.ChangeButton.Image = '/images/pchange.png'
  s_web._SitesQueue.Defaults.ChangeButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.ChangeButton.ToolTip = s_web.Translate('Click here to Change the highlighted record')
  s_web._SitesQueue.Defaults.ViewButton.Name = 'view_btn'
  s_web._SitesQueue.Defaults.ViewButton.TextValue = s_web.Translate('View')
  s_web._SitesQueue.Defaults.ViewButton.Image = '/images/psearch.png'
  s_web._SitesQueue.Defaults.ViewButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.ViewButton.ToolTip = s_web.Translate('Click here to view details of the highlighted record')
  s_web._SitesQueue.Defaults.DeletebButton.Name = 'deleteb_btn'
  s_web._SitesQueue.Defaults.DeletebButton.TextValue = s_web.Translate('Delete')
  s_web._SitesQueue.Defaults.DeletebButton.Image = '/images/pdelete.png'
  s_web._SitesQueue.Defaults.DeletebButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.DeletebButton.ToolTip = s_web.Translate('Click here to Delete the highlighted record')
  s_web._SitesQueue.Defaults.SelectButton.Name = 'select_btn'
  s_web._SitesQueue.Defaults.SelectButton.TextValue = s_web.Translate('Select')
  s_web._SitesQueue.Defaults.SelectButton.Image = '/images/pselect.png'
  s_web._SitesQueue.Defaults.SelectButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.SelectButton.ToolTip = s_web.Translate('Click here to Select the highlighted record')
  s_web._SitesQueue.Defaults.BrowseCancelButton.Name = 'browsecancel_btn'
  s_web._SitesQueue.Defaults.BrowseCancelButton.TextValue = s_web.Translate('Cancel')
  s_web._SitesQueue.Defaults.BrowseCancelButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.BrowseCancelButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.BrowseCancelButton.ToolTip = s_web.Translate('Click here to return without selecting anything')
  s_web._SitesQueue.Defaults.BrowseCloseButton.Name = 'browseclose_btn'
  s_web._SitesQueue.Defaults.BrowseCloseButton.TextValue = s_web.Translate('Close')
  s_web._SitesQueue.Defaults.BrowseCloseButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.BrowseCloseButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.BrowseCloseButton.ToolTip = s_web.Translate('Click here to Close this browse')
  s_web._SitesQueue.Defaults.SmallInsertButton.Name = 'insert_btn'
  s_web._SitesQueue.Defaults.SmallInsertButton.TextValue = s_web.Translate('Insert')
  s_web._SitesQueue.Defaults.SmallInsertButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallInsertButton.ToolTip = s_web.Translate('Click here to Insert a new record')
  s_web._SitesQueue.Defaults.SmallChangeButton.Name = 'change_btn'
  s_web._SitesQueue.Defaults.SmallChangeButton.TextValue = s_web.Translate('Change')
  s_web._SitesQueue.Defaults.SmallChangeButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallChangeButton.ToolTip = s_web.Translate('Click here to Change this record')
  s_web._SitesQueue.Defaults.SmallViewButton.Name = 'view_btn'
  s_web._SitesQueue.Defaults.SmallViewButton.TextValue = s_web.Translate('View')
  s_web._SitesQueue.Defaults.SmallViewButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallViewButton.ToolTip = s_web.Translate('Click here to view details of this record')
  s_web._SitesQueue.Defaults.SmallDeleteButton.Name = 'deleteb_btn'
  s_web._SitesQueue.Defaults.SmallDeleteButton.TextValue = s_web.Translate('Delete')
  s_web._SitesQueue.Defaults.SmallDeleteButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallDeleteButton.ToolTip = s_web.Translate('Click here to Delete this record')
  s_web._SitesQueue.Defaults.SmallSelectButton.Name = 'select_btn'
  s_web._SitesQueue.Defaults.SmallSelectButton.TextValue = s_web.Translate('Select')
  s_web._SitesQueue.Defaults.SmallSelectButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallSelectButton.ToolTip = s_web.Translate('Click here to Select this record')
  s_web._SitesQueue.Defaults.LocateButton.Name = 'locate_btn'
  s_web._SitesQueue.Defaults.LocateButton.TextValue = s_web.Translate('Search')
  s_web._SitesQueue.Defaults.LocateButton.Image = '/images/psearch.png'
  s_web._SitesQueue.Defaults.LocateButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.LocateButton.ToolTip = s_web.Translate('Click here to start the Search')
  s_web._SitesQueue.Defaults.FirstButton.Name = 'first_btn'
  s_web._SitesQueue.Defaults.FirstButton.TextValue = s_web.Translate('Top')
  s_web._SitesQueue.Defaults.FirstButton.Image = '/images/listtop.png'
  s_web._SitesQueue.Defaults.FirstButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.FirstButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.FirstButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.FirstButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.FirstButton.ToolTip = s_web.Translate('Click here to go to the First page in the list')
  s_web._SitesQueue.Defaults.PreviousButton.Name = 'previous_btn'
  s_web._SitesQueue.Defaults.PreviousButton.TextValue = s_web.Translate('Back')
  s_web._SitesQueue.Defaults.PreviousButton.Image = '/images/listback.png'
  s_web._SitesQueue.Defaults.PreviousButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.PreviousButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.PreviousButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.PreviousButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.PreviousButton.ToolTip = s_web.Translate('Click here to go to the Previous page in the list')
  s_web._SitesQueue.Defaults.NextButton.Name = 'next_btn'
  s_web._SitesQueue.Defaults.NextButton.TextValue = s_web.Translate('Next')
  s_web._SitesQueue.Defaults.NextButton.Image = '/images/listnext.png'
  s_web._SitesQueue.Defaults.NextButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.NextButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.NextButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.NextButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.NextButton.ToolTip = s_web.Translate('Click here to go to the Next page in the list')
  s_web._SitesQueue.Defaults.LastButton.Name = 'last_btn'
  s_web._SitesQueue.Defaults.LastButton.TextValue = s_web.Translate('Bottom')
  s_web._SitesQueue.Defaults.LastButton.Image = '/images/listbottom.png'
  s_web._SitesQueue.Defaults.LastButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.LastButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.LastButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.LastButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.LastButton.ToolTip = s_web.Translate('Click here to go to the Last page in the list')
  s_web._SitesQueue.Defaults.PrintButton.Name = 'print_btn'
  s_web._SitesQueue.Defaults.PrintButton.TextValue = s_web.Translate('Print')
  s_web._SitesQueue.Defaults.PrintButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.PrintButton.ToolTip = s_web.Translate('Click here to Print this page')
  s_web._SitesQueue.Defaults.StartButton.Name = 'start_btn'
  s_web._SitesQueue.Defaults.StartButton.TextValue = s_web.Translate('Start')
  s_web._SitesQueue.Defaults.StartButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.StartButton.ToolTip = s_web.Translate('Click here to Start the report')
  s_web._SitesQueue.Defaults.DateLookupButton.Name = 'lookup_btn'
  s_web._SitesQueue.Defaults.DateLookupButton.TextValue = s_web.Translate('...')
  s_web._SitesQueue.Defaults.DateLookupButton.Class = 'LookupButton'
  s_web._SitesQueue.Defaults.DateLookupButton.ToolTip = s_web.Translate('Click here to select a date')
  s_web._SitesQueue.Defaults.WizPreviousButton.Name = 'wizprevious_btn'
  s_web._SitesQueue.Defaults.WizPreviousButton.TextValue = s_web.Translate('Previous')
  s_web._SitesQueue.Defaults.WizPreviousButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.WizPreviousButton.ToolTip = s_web.Translate('Click here to go back to the Previous step')
  s_web._SitesQueue.Defaults.WizNextButton.Name = 'wiznext_btn'
  s_web._SitesQueue.Defaults.WizNextButton.TextValue = s_web.Translate('Next')
  s_web._SitesQueue.Defaults.WizNextButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.WizNextButton.ToolTip = s_web.Translate('Click here to go to the Next step')
  s_web._SitesQueue.Defaults.SmallOtherButton.Name = 'other_btn'
  s_web._SitesQueue.Defaults.SmallOtherButton.TextValue = s_web.Translate('Other')
  s_web._SitesQueue.Defaults.SmallOtherButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallOtherButton.ToolTip = s_web.Translate('')
  s_web._SitesQueue.Defaults.SmallPrintButton.Name = 'print_btn'
  s_web._SitesQueue.Defaults.SmallPrintButton.TextValue = s_web.Translate('Print')
  s_web._SitesQueue.Defaults.SmallPrintButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallPrintButton.ToolTip = s_web.Translate('Click here to print this record')
  s_web._SitesQueue.Defaults.CopyButton.Name = 'copy_btn'
  s_web._SitesQueue.Defaults.CopyButton.TextValue = s_web.Translate('Copy')
  s_web._SitesQueue.Defaults.CopyButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.CopyButton.ToolTip = s_web.Translate('Click here to copy the highlighted record')
  s_web._SitesQueue.Defaults.SmallCopyButton.Name = 'copy_btn'
  s_web._SitesQueue.Defaults.SmallCopyButton.TextValue = s_web.Translate('Copy')
  s_web._SitesQueue.Defaults.SmallCopyButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallCopyButton.ToolTip = s_web.Translate('Click here to copy this record')
  s_web._SitesQueue.Defaults.ClearButton.Name = 'clear_btn'
  s_web._SitesQueue.Defaults.ClearButton.TextValue = s_web.Translate('Clear')
  s_web._SitesQueue.Defaults.ClearButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.ClearButton.ToolTip = s_web.Translate('Click here to clear the locator')
  s_web._SitesQueue.Defaults.LogoutButton.Name = 'logout_btn'
  s_web._SitesQueue.Defaults.LogoutButton.TextValue = s_web.Translate('Logout')
  s_web._SitesQueue.Defaults.LogoutButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.LogoutButton.ToolTip = s_web.Translate('Click here to logout')
  s_web._SitesQueue.Defaults.PreCompressed = 1
  s_web._SitesQueue.Defaults.HtmlCommonScripts = |
    s_web.AddScript('all.js') &|
    s_web.AddScript('dhtmlgoodies_calendar.js') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE6Scripts = |
    s_web.AddScript('msie6.js') &|
  ''
  s_web._SitesQueue.Defaults.HtmlCommonStyles = |
    s_web.AddStyle('all.css') &|
    s_web.AddStyle('pccs.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE6Styles = |
    s_web.AddStyle('msie6.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIEStyles = |
    s_web.AddStyle('msie.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlFireFoxStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlSafariStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlChromeStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlOperaStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMozillaStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  Put(s_web._SitesQueue)
  If Net:ShortInit
    ReturnValue = Level:Notify
  End
  !--------------------------------------------------------------
  if p_web.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ! =====================================
  tmp:VersionNumber = 'Version Number: ' & kVersionNumber & '.' & kWebserverVersion_PH3
  ?VersionNumber:Prompt{prop:Text} = tmp:VersionNumber
  glo:VersionNumber = tmp:VersionNumber
  
  ! Do not start logging (DBH: 02/03/2009)
  web:EnableLogging = 0
  Do DefineListboxStyle
  IF ?AmendDetails{Prop:Checked}
    UNHIDE(?LookupFile)
    UNHIDE(?Button4)
    UNHIDE(?LookupFile:2)
    ENABLE(?glo:WebserverPort)
    ENABLE(?glo:DataPath)
    ENABLE(?Path:Web)
  END
  IF NOT ?AmendDetails{PROP:Checked}
    HIDE(?LookupFile)
    HIDE(?Button4)
    HIDE(?LookupFile:2)
    DISABLE(?glo:WebserverPort)
    DISABLE(?glo:DataPath)
    DISABLE(?Path:Web)
  END
  FileLookup1.Init
  FileLookup1.ClearOnCancel = True
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:Directory)  ! Allow Directory Dialog
  FileLookup1.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup4.Init
  FileLookup4.ClearOnCancel = True
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:Directory)  ! Allow Directory Dialog
  FileLookup4.SetMask('All Files','*.*')                   ! Set the file mask
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  p_web.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  remove(TagFile)
  remove(SBO_OutFaultParts)
  close(tempFaultCodes)
  remove(tempFaultCodes)
  close(tempAuditQueue)
  remove(tempAuditQueue)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AmendDetails
      IF ?AmendDetails{PROP:Checked}
        UNHIDE(?LookupFile)
        UNHIDE(?Button4)
        UNHIDE(?LookupFile:2)
        ENABLE(?glo:WebserverPort)
        ENABLE(?glo:DataPath)
        ENABLE(?Path:Web)
      END
      IF NOT ?AmendDetails{PROP:Checked}
        HIDE(?LookupFile)
        HIDE(?Button4)
        HIDE(?LookupFile:2)
        DISABLE(?glo:WebserverPort)
        DISABLE(?glo:DataPath)
        DISABLE(?Path:Web)
      END
      ThisWindow.Reset
      If ~0{prop:AcceptAll}
          If AmendDetails = 1
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Message('After any changes are made, the program will quit. '&|
                  '|Anyone connected to this websever at the time will lose information.'&|
                  '|'&|
                  '|Are you sure you want to continue?','Amend Details',|
                             icon:exclamation,'&Yes|&No',2,2)
                  Of 1 ! &Yes Button
                      ?AmendDetails{prop:Disable} = 1
                      ?Cancel{prop:Disable} = 1
                  Of 2 ! &No Button
                      AmendDetails = 0
                      Display()
                      Cycle
              End!Case Message
          End
      End ! If ~0{prop:AcceptAll}
    OF ?LookupFile
      ThisWindow.Update
      glo:DataPath = FileLookup1.Ask(1)
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update
      Path:Web = FileLookup4.Ask(1)
      DISPLAY
    OF ?Button4
      ThisWindow.Update
      If glo:WebServerPort <> '' And glo:DataPath <> '' And Path:Web <> ''
          PUTINI('DEFAULTS','port',glo:WebserverPort,Clip(glo:LocalPath) & '\WEBSERVER.INI')
          PUTINI('DEFAULTS','pathtodata',Clip(glo:DataPath),Clip(glo:LocalPath) & '\WEBSERVER.INI')
          PUTINI('DEFAULTS','pathtoweb',Clip(Path:Web),Clip(glo:LocalPath) & '\WEBSERVER.INI')
          Halt()
      End !If glo:WebServerPort <> '' And glo:DatPath <> ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    p_web.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If glo:webserverport = ''
          0{prop:Iconize} = 0
      End ! If glo:webserverpost = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


p_web.AddLog PROCEDURE(String p_Data,<string p_ip>)


  CODE
    self._wait()
    If web:EnableLogging > 0
      clear(LogQueue)
      LogQueue.Port = Self.Port
      LogQueue.Date = today()
      LogQueue.Time = clock()
      LogQueue.Desc = p_Data
      Add(LogQueue,1)
      Loop While Records(LogQueue) > 500
        Get(LogQueue,501)
        Delete(LogQueue)
      End
    End
    self._release()
  PARENT.AddLog(p_Data,p_ip)


p_web.StartNewThread PROCEDURE(NetWebServerRequestDataType p_RequestData)

!loc:RequestData    Group(NetWebServerRequestDataType).
!loc:OverString     String(size(loc:RequestData)),over(loc:RequestData)

  CODE
    loc:RequestData :=: p_RequestData
    web:PagesServed = self._PagesServed + 1
    if p_RequestData.DataStringLen >= 4
      case (upper(p_RequestData.DataString[1 : 4]))
      of 'POST'
        web:LastPost = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastPost)
      of 'GET '
        web:LastGet = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastGet)
      else
        web:LastGet = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastGet)
      end
    end
    self.AddLog(p_RequestData.DataString,p_RequestData.FromIP)
    START (WebHandler, 35000, loc:OverString)
    RETURN ! Don't call parent
  PARENT.StartNewThread(p_RequestData)


p_web.TakeEvent PROCEDURE


  CODE
  PARENT.TakeEvent
    if event() = Event:openWindow
      web:StartDate = Today()
      web:StartTime = Clock()
    End
    If Event() = Event:Accepted and Field() = ?Clear
      Free(LogQueue)
      web:LastGet = ''
      web:LastPost = ''
      display()
    End
    If Field() = ?LogQueue and Event() = Event:NewSelection
      Get(LogQueue,Choice(?LogQueue))
      If ErrorCode() = 0
        Case Upper(Sub(LogQueue.Desc,1,3))
        Of 'POS'
          web:LastPost = LogQueue.Desc
        Of 'GET'
          web:LastGet = LogQueue.Desc
        Else
          web:LastGet = LogQueue.Desc
        End
        Display()
      End
    End

!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
Waybill PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locWaybillNumber     LONG                                  !
FromAddressGroup     GROUP,PRE(from)                       !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(39)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(60)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
ToAddressGroup       GROUP,PRE(to)                         !
AccountNumber        STRING(30)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
locDateDespatched    DATE                                  !
locTimeDespatched    TIME                                  !
locCourier           STRING(30)                            !
locConsignmentNumber STRING(30)                            !
locBarCode           STRING(60)                            !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locOrderNumber       STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locAccessories       STRING(255)                           !
locExchanged         STRING(1)                             !
Process:View         VIEW(WAYBILLJ)
                       PROJECT(waj:JobNumber)
                       PROJECT(waj:WayBillNumber)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('JOBS Report'),AT(250,3875,7750,6594),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,COLOR:Black, |
  FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(250,250,7750,3250),USE(?Header),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT)
                         STRING('Courier:'),AT(4896,781),USE(?String23),FONT(,8),TRN
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),FONT(,8),TRN
                         STRING(@d6),AT(6042,365),USE(locDateDespatched),FONT(,8,,FONT:bold),TRN
                         STRING(@t1b),AT(6042,573),USE(locTimeDespatched),FONT(,8,,FONT:bold),TRN
                         STRING('From Sender:'),AT(83,83),USE(?String16),FONT(,8),TRN
                         STRING('WAYBILL REJECTION'),AT(3750,31,3781),USE(?WaybillRejection),FONT(,16,,FONT:bold),RIGHT, |
  HIDE,TRN
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),FONT(,8),TRN
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),FONT(,8),TRN
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),FONT(,8),TRN
                         STRING(@s60),AT(938,1198),USE(from:ContactName),FONT(,8,,FONT:bold),TRN
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Email:'),AT(104,1354),USE(?String16:4),FONT(,8),TRN
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),FONT(,12,,FONT:bold),CENTER, |
  TRN
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),FONT(,8),TRN
                         STRING(@s30),AT(83,250,4687,365),USE(from:CompanyName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(3490,1979,3906,260),USE(locBarCode),FONT('C39 High 12pt LJ3',12,COLOR:Black, |
  ,CHARSET:ANSI),CENTER,COLOR(COLOR:White)
                         STRING(@s30),AT(83,542),USE(from:AddressLine1),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(83,708),USE(from:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),FONT(,8,,FONT:bold),TRN
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),FONT(,8),TRN
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),FONT(,8),TRN
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),FONT(,8),TRN
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),FONT(,8,,FONT:bold),TRN
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),FONT(,8),TRN
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),FONT(,8),TRN
                         STRING(@s30),AT(83,875),USE(from:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(3490,2292,3906,260),USE(locConsignmentNumber),FONT('Arial',12,,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING(@s255),AT(4323,2969,3385,208),USE(glo:ErrorText),FONT(,8,,FONT:bold),RIGHT,TRN
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@s30),AT(6042,781),USE(locCourier),FONT(,,,FONT:bold)
                         STRING(@N3),AT(6042,990),USE(ReportPageNumber),FONT(,,,FONT:bold)
                       END
Detail                 DETAIL,AT(0,0,7750,229),USE(?Detail)
                         STRING(@s30),AT(104,0,1094),USE(locJobNumber)
                         STRING(@s30),AT(1260,0,1302),USE(locIMEINumber)
                         STRING(@s30),AT(2625,-10),USE(locOrderNumber),FONT(,7)
                         STRING(@s30),AT(4354,-10,958),USE(locSecurityPackNumber),FONT(,7)
                         TEXT,AT(5375,-10,2021,146),USE(locAccessories),RESIZE
                         STRING(@s1),AT(7458,-10),USE(locExchanged)
                       END
                       FOOTER,AT(250,10531,7750,906),USE(?Footer)
                         TEXT,AT(125,0,7500,833),USE(stt:Text),TRN
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT)
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),FONT(,7),TRN
                         STRING('I.M.E.I. No'),AT(1313,3333),USE(?IMEINo),FONT(,7),TRN
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),FONT(,7),TRN
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),FONT(,7),TRN
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),FONT(,7),TRN
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Waybill Variables
  ! p_web.SSV('locWaybillNumber',)
  ! p_web.SSV('Waybill:Courier,)
  ! p_web.SSV('Waybill:FromType,)
  ! p_web.SSV('Waybill:FromAccount,)
  ! p_web.SSV('Waybill:ToType,)
  ! p_web.SSV('Waybill:ToAccount,)
  GlobalErrors.SetProcedureName('Waybill')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBACC.SetOpenRelated()
  Relate:JOBACC.Open                                       ! File JOBACC used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WAYBILLJ.SetOpenRelated()
  Relate:WAYBILLJ.Open                                     ! File WAYBILLJ used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WAYBILLS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locWaybillNumber = p_web.GSV('locWaybillNumber')
  Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
  way:WayBillNumber = locWaybillNumber
  IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
  
  END
  SET(DEFAULTS,0)
  Access:DEFAULTS.Next()
  
  locCourier = p_web.GSV('Waybill:Courier')
  
  ! Set consignment number
  IF (way:FromAccount = p_web.GSV('ARC:AccountNumber'))
      IF (GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1)
          locConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(way:WaybillNumber,@n07)
      ELSE
          locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
      END
  ELSE
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          If clip(tra:RRCWaybillPrefix) <> ''
              locConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(way:WaybillNumber,@n07)
          ELSE
              locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
          END
      END
  END
  
  
  
  locDateDespatched = way:TheDate
  locTimeDespatched = way:TheTime
  
  ! Set addresses
  CASE p_web.GSV('Waybill:FromType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName        = tra:Company_Name
          from:AddressLine1       = tra:Address_Line1
          from:AddressLine2       = tra:Address_Line2
          from:AddressLine3       = tra:Address_Line3
          from:TelephoneNumber    = tra:Telephone_Number
          from:ContactName        = tra:Contact_Name
          from:EmailAddress       = tra:EmailAddress        
      END
      
  OF 'SUB'
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          from:CompanyName        = sub:Company_Name
          from:AddressLine1       = sub:Address_Line1
          from:AddressLine2       = sub:Address_Line2
          from:AddressLine3       = sub:Address_Line3
          from:TelephoneNumber    = sub:Telephone_Number
          from:ContactName        = sub:Contact_Name
          from:EmailAddress       = sub:EmailAddress
      END
      
  OF 'DEF'
      from:CompanyName        = def:User_Name
      from:AddressLine1       = def:Address_Line1
      from:AddressLine2       = def:Address_Line2
      from:AddressLine3       = def:Address_Line3
      from:TelephoneNumber    = def:Telephone_Number
      from:ContactName        = ''
      from:EmailAddress       = def:EmailAddress
  END
  
  CASE p_web.GSV('Waybill:ToType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = tra:Account_Number
          to:CompanyName        = tra:Company_Name
          to:AddressLine1       = tra:Address_Line1
          to:AddressLine2       = tra:Address_Line2
          to:AddressLine3       = tra:Address_Line3
          to:TelephoneNumber    = tra:Telephone_Number
          to:ContactName        = tra:Contact_Name
          to:EmailAddress       = tra:EmailAddress
      END
      
  OF 'SUB'
      Access:SUBTRACC.clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = sub:Account_Number
          to:CompanyName        = sub:Company_Name
          to:AddressLine1       = sub:Address_Line1
          to:AddressLine2       = sub:Address_Line2
          to:AddressLine3       = sub:Address_Line3
          to:TelephoneNumber    = sub:Telephone_Number
          to:ContactName        = sub:Contact_Name
          to:EmailAddress       = sub:EmailAddress
      END
      
  END
  
  
  IF (way:WaybillID <> 0)
      IF (way:WaybillID = 13)
          ! This is a rejection
          SETTARGET(Report)
          ?WaybillRejection{PROP:Hide} = FALSE
          SETTARGET()
      END
      
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName    = tra:Company_Name
          from:AddressLine1   = tra:Address_Line1
          from:AddressLine2   = tra:Address_Line2
          from:AddressLine3   = tra:Address_Line3
          from:TelephoneNumber= tra:Telephone_Number
          from:ContactName    = tra:Contact_Name
          from:EmailAddress   = tra:EmailAddress
      END
      
      IF (way:WaybillID = 20) ! PUP to RRC. Get address from job
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:FromAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END
          
          IF (sub:VCPWaybillPrefix <> '')
              locConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  IF (tra:VCPWaybillPrefix <> '')
                      locConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
                  END
                  
              END
          END
      END
      
      CASE way:WaybillID
      OF 1 OROF 5 OROF 6 OROF 11 OROF 12 OROF 13 OROF 20
          IF (way:ToAccount = 'CUSTOMER') ! PUP to Customer
              to:ContactName = ''
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = way:ToAccount
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:benign)
                  IF (sub:Use_Delivery_Address = 'YES')
                      to:ContactName = sub:Contact_Name
                  END
              END
              to:AccountNumber   = job:Account_Number
              to:CompanyName     = job:Company_Name
              to:AddressLine1    = job:Address_Line1
              to:AddressLine2    = job:Address_Line2
              to:AddressLine3    = job:Address_Line3
              to:TelephoneNumber = job:Telephone_Number
  
              to:EmailAddress    = jobe:EndUserEmailAddress
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = way:ToAccount
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  to:AccountNumber  = tra:Account_Number
                  to:CompanyName    = tra:Company_Name
                  to:AddressLine1   = tra:Address_Line1
                  to:AddressLine2   = tra:Address_Line2
                  to:AddressLine3   = tra:Address_Line3
                  to:TelephoneNumber= tra:Telephone_Number
                  to:ContactName    = tra:Contact_Name
                  to:EmailAddress   = tra:EmailAddress
              END
              
          END
      OF 21 OROF 22 ! RRC to PUP
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:ToAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END   
      OF 300 ! Sundry Waybill
          ! Do this later.
      ELSE
          ! Get details from the job on the waybill
          Access:WAYBILLJ.ClearKey(waj:DescWaybillNoKey)
          waj:WayBillNumber = way:WayBillNumber
          IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey) = Level:Benign)
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = waj:JobNumber
              IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                  
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  IF (Access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
                  END
                  
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = way:ToAccount
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:UseCustDespAdd = 'YES')
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress                    
                      ELSE
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress                        
                      END
                  end
              END
          END
      END
  END
  
  locBarcode = '*' & CLIP(locConsignmentNumber) & '*'
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'WAYBILL'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('Waybill',ProgressWindow)                   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:WAYBILLJ, ?Progress:PctText, Progress:Thermometer, ProgressMgr, waj:JobNumber)
  ThisReport.AddSortOrder(waj:JobNumberKey)
  ThisReport.AddRange(waj:WayBillNumber,locWaybillNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:WAYBILLJ.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Waybill',ProgressWindow)                ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = waj:JobNumber
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
      RETURN Level:User
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
      RETURN Level:User
  END
  
  locExchanged = ''
  locJobNumber = CLIP(job:Ref_Number) & '-' & p_web.GSV('BookingBranchID') & CLIP(wob:JobNumber)
  IF (waj:WayBillNumber = job:Exchange_Consignment_Number)
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
          locIMEINumber = waj:IMEINumber
      ELSE
          locIMEINumber = xch:ESN
      END
  ELSE
      locIMEINumber = waj:IMEINumber
      IF (job:Exchange_Unit_Number <> 0)
          locExchanged = 'E'
      END
  END
  locSecurityPackNumber = waj:SecurityPackNumber
  locAccessories = ''
        
  If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
      way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
  
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
          If way:WaybillType = 0 Or way:WaybillType = 1
              ! End (DBH 08/03/2007) #8703
              !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
              If jac:Attached <> True
                  Cycle
              End !If jac:Attached <> True
          End ! If way:WaybillType <> 0 And way:WaybillType <> 1
          If locAccessories = ''
              locAccessories = Clip(jac:Accessory)
          Else !If locAccessories = ''
              locAccessories = Clip(locAccessories) & ', ' & Clip(jac:Accessory)
          End !If tmp:Accessories = ''
      End !Loop
  End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
      
  locOrderNumber = job:Order_Number
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','Waybill','Waybill','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

PrintRoutines        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(20)                            !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
TRADEACC::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WAYBILLJ::State  USHORT
JOBNOTES::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PrintRoutines')
  loc:formname = 'PrintRoutines_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PrintRoutines','')
    p_web._DivHeader('PrintRoutines',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PrintRoutines',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PrintRoutines',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
HideButtons     routine
    p_web.SSV('Hide:PrintJobCard',1)
    p_web.SSV('Hide:PrintEstimate',1)
    p_web.SSV('Hide:PrintDespatchNote',1)
HideWaybillButtons  ROUTINE
    p_web.SSV('Hide:PrintWaybill',1)
OpenFiles  ROUTINE
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(JOBNOTES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(JOBNOTES)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PrintRoutines_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PrintRoutines_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'OK'
  
  if (p_web.GSV('PrintRoutines:SecondTime') = 0)
      p_web.SSV('locJObNumber','')
      p_web.SSV('Comment:JobNumber','Enter Job Number and press [TAB]')
      do HideButtons
      p_web.SSV('PrintRoutines:SecondTime',1)
      p_web.SSV('locWaybillNumber','')
      p_web.SSV('Comment:WaybillNumber','Enter Waybill Number and press [TAB]')
      do HideWaybillButtons
  end ! if (p_web.GSV('PrintRoutines:SecondTime') = 0)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PrintRoutines_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PrintRoutines_ChainTo')
    loc:formaction = p_web.GetSessionValue('PrintRoutines_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PrintRoutines" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PrintRoutines" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PrintRoutines" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Print Routines') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Print Routines',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PrintRoutines">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PrintRoutines" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PrintRoutines')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select Waybill Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PrintRoutines')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PrintRoutines'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PrintRoutines')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Job Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonPrintJobCard
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPrintEstimate
      do Comment::buttonPrintEstimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPrintDespatchNote
      do Comment::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select Waybill Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonPrintWaybill
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locJobNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  Access:JOBS.Clearkey(job:ref_Number_Key)
  job:ref_Number    = p_web.GSV('locJobNumber')
  if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
      ! Found
      p_web.FileToSessionQueue(JOBS)
  
  
      Access:JOBNOTES.Clearkey(jbn:refNumberKey)
      jbn:refNumber    = job:Ref_Number
      if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
          ! Found
          p_web.FileToSessionQueue(JOBNOTES)
      else ! if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
  
  
      Access:WEBJOB.Clearkey(wob:refNumberKey)
      wob:refNumber    = job:Ref_Number
      if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
          ! Found
          p_web.FileToSessionQueue(WEBJOB)
      else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:refNumberKey)
      jobe:refNumber    = job:Ref_Number
      if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
          ! Found
          p_web.FileToSessionQueue(JOBSE)
      else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
  
      if (job:Estimate = 'YES')
          p_web.SSV('Hide:PrintEstimate',0)
      end ! if (job:Estimate = 'YES')
  
      p_web.SSV('Hide:PrintJObCard',0)
      
      p_web.SSV('Hide:PrintDespatchNote',0)
  
      p_web.SSV('job:Ref_Number',job:Ref_Number)
      p_web.SSV('tmp:JobNumber',job:Ref_Number)
      p_web.SSV('Comment:JobNumber','Job Found')
  else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
      ! Error
      p_web.SSV('Comment:JobNumber','Cannot find the selected Job Number')
      do HideButtons
  end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  do Value::locJobNumber
  do SendAlert
  do Value::buttonPrintEstimate  !1
  do Comment::locJobNumber
  do Value::buttonPrintJobCard  !1
  do Value::buttonPrintDespatchNote  !1

Value::locJobNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''printroutines_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobNumber'))
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_comment')

Prompt::buttonPrintJobCard  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_prompt',Choose(p_web.GSV('Hide:PrintJobCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:PrintJobCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintJobCard  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintJobCard',p_web.GetValue('NewValue'))
    do Value::buttonPrintJobCard
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintJobCard  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_value',Choose(p_web.GSV('Hide:PrintJobCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintJobCard') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintJobCard','Job Card','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobCard')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_value')

Comment::buttonPrintJobCard  Routine
    loc:comment = ''
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_comment',Choose(p_web.GSV('Hide:PrintJobCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintJobCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintEstimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintEstimate',p_web.GetValue('NewValue'))
    do Value::buttonPrintEstimate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintEstimate  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_value',Choose(p_web.GSV('Hide:PrintEstimate') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintEstimate') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintEstimate','Estimate','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Estimate')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_value')

Comment::buttonPrintEstimate  Routine
    loc:comment = ''
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_comment',Choose(p_web.GSV('Hide:PrintEstimate') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintEstimate') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintDespatch','Despatch Note','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchNote')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')

Comment::buttonPrintDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWaybillNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Waybill Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
  Access:WAYBILLJ.Clearkey(waj:DescWaybillNoKey)
  waj:WaybillNumber = p_web.GSV('locWaybillNumber')
  IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey))
      p_web.SSV('Comment:WaybillNumber','Cannot find the selected Waybill Number')
      do HideWaybillButtons
  ELSE
      
      Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
      way:WayBillNumber = waj:WayBillNumber
      IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
          p_web.SSV('Comment:WaybillNumber','Cannot find the selected Waybill Number')
          do HideWaybillButtons
  
      ELSE
          ! Get the details from the first job
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = waj:JobNumber
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
              p_web.SSV('Comment:WaybillNumber','Cannot find the job attached to the selected Waybill Number')
              do HideWaybillButtons           
          END
          
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
              p_web.SSV('Comment:WaybillNumber','Cannot find the job attached to the selected Waybill Number')
              do HideWaybillButtons             
          END
          
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
              p_web.SSV('Comment:WaybillNumber','Cannot find the job attached to the selected Waybill Number')
              do HideWaybillButtons             
          END
          
  
          error# = 0
          IF (p_web.GSV('BookingSite') = 'RRC')
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = p_web.GSV('BookingAccount')
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
              END
              
              IF (way:FromAccount <> p_web.GSV('BookingAccount'))
                  p_web.SSV('Comment:WaybillNumber','The selected waybill was not created at this site.')
                  do HideWaybillButtons
                  error# = 1
              ELSE
                  CASE way:WayBillType
                  OF 1 ! RRC to ARC
                      p_web.SSV('Waybill:ToAccount',p_web.GSV('ARC:AccountNumber'))
                      p_web.SSV('Waybill:ToType','TRA')
                  OF 2 !RRC to Customer
                      p_web.SSV('Waybill:ToAccount','')
                      p_web.SSV('Waybill:ToType','CUS')
                  OF 3 ! RRC Exchange to Customer
                      p_web.SSV('Waybill:ToAccount','')
                      p_web.SSV('Waybill:ToType','CUS')
                  ELSE
                      IF (jobe:Sub_Sub_Account <> '')
                          p_web.SSV('Waybill:ToAccount',jobe:Sub_Sub_Account)
                      ELSE
                          p_web.SSV('Waybill:ToAccount',job:Account_Number)
                      END
                      p_web.SSV('Waybill:ToType','SUB')
                      p_web.SSV('Hide:PrintWaybill',0)
                      p_web.SSV('Comment:WaybillNumber','Waybill Found')
                  END
                  If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                      p_web.SSV('Waybill:Courier',GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI'))
                  ELSE
                      p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                  END
                  p_web.SSV('Waybill:FromType','TRA')
                  p_web.SSV('Waybill:FromAccount',p_web.GSV('BookingAccount'))
              END
          ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
              IF (way:FromAccount <> p_web.GSV('ARC:AccountNumber'))
                  p_web.SSV('Comment:WaybillNumber','The selected waybill was not created at this site.')
                  do HideWaybillButtons
              ELSE
                  
                  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = p_web.GSV('ARC:AccountNumber')
                  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                  END
                  
                  p_web.SSV('Waybill:FromAccount',p_web.GSV('ARC:AccountNumber'))
                  IF (jobe:WebJob = 1)
                      p_web.SSV('Waybill:ToAccount',wob:HeadAccountNumber)
                      p_web.SSV('Waybill:ToType','TRA')
                  ELSE
                      p_web.SSV('Waybill:ToAccount',job:Account_Number)
                      p_web.SSV('Waybill:ToType','SUB')
                  END
                  If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                      p_web.SSV('Waybill:Courier',GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI'))
                  ELSE
                      p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                  END
                  p_web.SSV('Hide:PrintWaybill',0)
                  p_web.SSV('Comment:WaybillNumber','Waybill Found')
              END
          END
          
      end
  
  END
  do Value::locWaybillNumber
  do SendAlert
  do Comment::locWaybillNumber
  do Value::buttonPrintWaybill  !1

Value::locWaybillNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''printroutines_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWaybillNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_value')

Comment::locWaybillNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:WaybillNumber'))
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_comment')

Prompt::buttonPrintWaybill  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_prompt',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Print Waybill','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_value')

Comment::buttonPrintWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_comment',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PrintRoutines_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('PrintRoutines_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PrintRoutines',0)

PreCopy  Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PrintRoutines',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PrintRoutines:Primed',0)

PreDelete       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  p_web.setsessionvalue('showtab_PrintRoutines',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PrintRoutines_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PrintRoutines_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! tab = 5
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      p_web.deleteSessionValue('PrintRoutines:SecondTime')
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
InvoiceSubAccounts   PROCEDURE  (func:AccountNumber)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                Return Level:Fatal
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign
VATRate_OLD          PROCEDURE  (fAccountNumber,fType)     ! Declare Procedure
rtnValue             REAL                                  !
FilesOpened     BYTE(0)
  CODE
    DO openFiles
    rtnValue = 0
    If InvoiceSubAccounts(fAccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = sub:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = sub:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnvalue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    Else !If InvoiceSubAccounts(func:AccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = tra:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = tra:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnValue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    End !If InvoiceSubAccounts(func:AccountNumber)
    Do CloseFiles
    Return rtnValue
    
    
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
JobCard PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
tmp:JobNumber        LONG                                  !Job Number
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(20)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Courier)
                       PROJECT(job:DOP)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Date_QA_Passed)
                       PROJECT(job:ESN)
                       PROJECT(job:Location)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Time_Completed)
                       PROJECT(job:Time_QA_Passed)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(260,6458,7750,813),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(260,615,7750,8250),USE(?unnamed),FONT('Tahoma',7)
                         STRING(@s50),AT(6094,469),USE(who_booked),FONT(,8,,FONT:bold),TRN
                         STRING('Job Number: '),AT(5104,52),USE(?String25),FONT(,8),TRN
                         STRING(@s16),AT(6010,21),USE(tmp:Ref_number),FONT(,11,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(5104,313),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(6094,313),USE(job:date_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@t1b),AT(6979,313),USE(job:time_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Booked By:'),AT(5104,469),USE(?String158),FONT(,8),TRN
                         STRING(@d6b),AT(6094,781),USE(job:DOP),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Engineer:'),AT(5104,625),USE(?String96),FONT(,8),TRN
                         STRING(@s30),AT(6094,625),USE(engineer_temp),FONT(,8,,FONT:bold),TRN
                         STRING('Date Of Purchase:'),AT(5104,781),USE(?String96:2),FONT(,8),TRN
                         STRING('Model'),AT(177,3500),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Location'),AT(177,2833),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Type'),AT(4708,2833),USE(?warranty_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Repair Type'),AT(6219,2833),USE(?warranty_repair_type),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Chargeable Type'),AT(1688,2833),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(3198,2833),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1688,3500),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3198,3500),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4708,3510),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6219,3510),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(1688,3073),USE(job:Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3198,3073),USE(job:Repair_Type),FONT(,8),TRN
                         STRING(@s20),AT(177,3073),USE(job:Location),FONT(,8),TRN
                         STRING(@s30),AT(177,3698,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1688,3698,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3198,3698,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(4708,3698,1396,156),USE(job:ESN),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6219,3698),USE(job:MSN),FONT(,8),TRN
                         STRING(@s20),AT(6219,4010),USE(exchange_msn_temp),FONT(,8),TRN
                         STRING(@s16),AT(4708,4010,1396,156),USE(exchange_esn_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3198,4010,1396,156),USE(exchange_unit_type_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1688,4010,1323,156),USE(exchange_manufacturer_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(177,4010,1000,156),USE(exchange_model_number),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(177,3854),USE(exchange_unit_title_temp),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s20),AT(1354,3854),USE(exchange_unit_number_temp),LEFT,TRN
                         STRING('REPORTED FAULT'),AT(3562,4229,1062,156),USE(?String64),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(4625,4219,2406,844),USE(jbn:Fault_Description),FONT(,7),TRN
                         STRING('ESTIMATE'),AT(156,4583,1354,156),USE(?Estimate),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING(@s70),AT(1625,5031,5625,208),USE(estimate_value_temp),FONT(,8),TRN
                         STRING('ENGINEERS REPORT'),AT(156,5031,1354,156),USE(?String88),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR NOTES'),AT(156,4229),USE(?String88:3),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(1615,4646,1927,417),USE(tmp:InvoiceText),FONT(,7),TRN
                         TEXT,AT(1615,5208,5417,313),USE(tmp:accessories),FONT(,7),TRN
                         STRING('ACCESSORIES'),AT(156,5208,1354,156),USE(?String100),FONT(,8,,FONT:bold),TRN
                         STRING('Type'),AT(521,5677),USE(?String98),FONT(,7,,FONT:bold),TRN
                         STRING('PARTS REQUIRED'),AT(156,5521,1135,156),USE(?String79),FONT(,8,,FONT:bold),TRN
                         STRING('Qty'),AT(156,5677),USE(?String80),FONT(,7,,FONT:bold),TRN
                         STRING('Part Number'),AT(885,5677),USE(?String81),FONT(,7,,FONT:bold),TRN
                         STRING('Description'),AT(2292,5677),USE(?String82),FONT(,7,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(4063,5677),USE(?String83),FONT(,7,,FONT:bold),TRN
                         STRING('Line Cost'),AT(4948,5677),USE(?String89),FONT(,7,,FONT:bold),TRN
                         LINE,AT(156,5833,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(156,6667,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('BOUNCER'),AT(156,6719,1354,156),USE(?BouncerTitle),FONT('Tahoma',8,,FONT:bold),HIDE, |
  TRN
                         TEXT,AT(1667,6719,5417,208),USE(tmp:bouncers),FONT(,8),HIDE,TRN
                         STRING('LOAN UNIT ISSUED'),AT(156,6927,1354,156),USE(?LoanUnitIssued),FONT('Tahoma',8,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s30),AT(1667,6927),USE(tmp:LoanModel),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  LEFT,HIDE,TRN
                         STRING('IMEI'),AT(3646,6927),USE(?IMEITitle),FONT('Tahoma',9,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         STRING(@s16),AT(4010,6927),USE(tmp:LoanIMEI),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  LEFT,HIDE,TRN
                         STRING('CUSTOMER SIGNATURE'),AT(167,8021,1583,156),USE(?String820),FONT('Tahoma',8,,FONT:bold), |
  TRN
                         STRING('REPLACEMENT VALUE'),AT(5104,6927),USE(?LoanValueText),FONT('Tahoma',9,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@n10.2),AT(6406,6927),USE(tmp:ReplacementValue),FONT(,8),RIGHT,HIDE,TRN
                         LINE,AT(4719,8156,1406,0),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Date: {22}/ {23}/'),AT(4500,8031,1563,156),USE(?String830),FONT(,,,FONT:bold),TRN
                         LINE,AT(1594,8156,2917,0),USE(?Line5:2),COLOR(COLOR:Black)
                         STRING('LOAN ACCESSORIES'),AT(156,7083,1354,156),USE(?LoanAccessoriesTitle),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s255),AT(1667,7083,5417,208),USE(tmp:LoanAccessories),FONT('Tahoma',8,COLOR:Black, |
  FONT:regular,CHARSET:ANSI),LEFT,HIDE,TRN
                         STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,7219),USE(?ExternalDamageCheckList),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Notes:'),AT(5781,7240),USE(?Notes),FONT(,7),TRN
                         TEXT,AT(6094,7240,1458,469),USE(jobe2:XNotes),FONT(,6),TRN
                         CHECK('None'),AT(5156,7240,625,156),USE(jobe2:XNone),TRN
                         CHECK('Keypad'),AT(156,7365,729,156),USE(jobe2:XKeypad),TRN
                         CHECK('Charger'),AT(2083,7365,781,156),USE(jobe2:XCharger),TRN
                         CHECK('Antenna'),AT(2083,7240,833,156),USE(jobe2:XAntenna),TRN
                         CHECK('Lens'),AT(2927,7240,573,156),USE(jobe2:XLens),TRN
                         CHECK('F/Cover'),AT(3552,7240,781,156),USE(jobe2:XFCover),TRN
                         CHECK('B/Cover'),AT(4437,7240,729,156),USE(jobe2:XBCover),TRN
                         CHECK('Battery'),AT(990,7365,729,156),USE(jobe2:XBattery),TRN
                         CHECK('LCD'),AT(2927,7365,625,156),USE(jobe2:XLCD),TRN
                         CHECK('System Connector'),AT(4437,7365,1250,156),USE(jobe2:XSystemConnector),TRN
                         CHECK('Sim Reader'),AT(3552,7365,885,156),USE(jobe2:XSimReader),TRN
                         STRING(@s30),AT(156,1927),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4167,2083),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4427,2083),USE(Delivery_Telephone_Temp),FONT(,8),TRN
                         STRING(@s50),AT(521,2083,3490,156),USE(invoice_EMail_Address),FONT(,8),LEFT,TRN
                         STRING('Email:'),AT(156,2083),USE(?unnamed:4),FONT(,8),TRN
                         STRING('Tel:'),AT(156,2240),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(521,2240),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(156,2396),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(521,2396),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2760,1302),USE(job:Account_Number),FONT(,8),TRN
                         STRING(@s20),AT(4708,3073),USE(job:Warranty_Charge_Type),FONT(,8),TRN
                         STRING(@s20),AT(6219,3073),USE(job:Repair_Type_Warranty),FONT(,8),TRN
                         STRING('Acc No:'),AT(2188,1302),USE(?String94),FONT(,8,COLOR:Black),TRN
                         STRING(@s30),AT(4167,1302),USE(delivery_Company_Name_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1458,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s20),AT(2760,1458),USE(job:Order_Number),FONT(,8),TRN
                         STRING('Order No:'),AT(2188,1458),USE(?String140),FONT(,8),TRN
                         STRING(@s30),AT(4167,1615),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1771),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1927),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1302),USE(invoice_company_name_temp),FONT(,8,COLOR:Black),TRN
                         STRING(@s30),AT(156,1458),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1771),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1615),USE(invoice_address2_temp),FONT(,8),TRN
                         STRING(@s65),AT(4167,2396,2865,156),USE(clientName),FONT('Tahoma',8,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         TEXT,AT(156,7781,7375,250),USE(stt:Text),TRN
                         TEXT,AT(156,7531,5885,260),USE(locTermsText)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:3)
DETAIL                   DETAIL,AT(0,0,,115),USE(?DetailBand)
                           STRING(@n8b),AT(-104,0),USE(Quantity_temp),FONT(,7),RIGHT,TRN
                           STRING(@s25),AT(885,0),USE(part_number_temp),FONT(,7),TRN
                           STRING(@s25),AT(2292,0),USE(Description_temp),FONT(,7),TRN
                           STRING(@n14.2b),AT(3802,0),USE(Cost_Temp),FONT(,7),RIGHT,TRN
                           STRING(@n14.2b),AT(4740,0),USE(line_cost_temp),FONT(,7),RIGHT,TRN
                           STRING(@s4),AT(521,0),USE(part_type_temp),FONT(,7),TRN
                         END
                         FOOTER,AT(396,8896,,2406),USE(?unnamed:2),ABSOLUTE,TOGETHER
                           STRING('Loan Deposit Paid'),AT(5313,21),USE(?LoanDepositPaidTitle),FONT(,8,,FONT:regular,CHARSET:ANSI), |
  HIDE,TRN
                           STRING(@n14.2b),AT(6396,21),USE(tmp:LoanDepositPaid),FONT(,8,,,CHARSET:ANSI),RIGHT,HIDE,TRN
                           STRING('Completed:'),AT(52,156),USE(?String66),FONT(,8),TRN
                           STRING(@D6b),AT(719,167),USE(job:Date_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING(@t1b),AT(1677,167),USE(job:Time_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('QA Passed:'),AT(52,313),USE(?qa_passed),FONT(,8),HIDE,TRN
                           STRING('Labour:'),AT(5313,167),USE(?labour_string),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,167),USE(labour_temp),FONT(,8),RIGHT,TRN
                           STRING(@s20),AT(2500,260,2552,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('Outgoing Courier:'),AT(52,573),USE(?String66:2),FONT(,8),TRN
                           STRING(@s20),AT(2917,469,1760,198),USE(job_number_temp),FONT('Tahoma',8,,FONT:bold),CENTER, |
  TRN
                           STRING('Carriage:'),AT(5313,458),USE(?carriage_string),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,313),USE(parts_temp),FONT(,8),RIGHT,TRN
                           STRING(@s20),AT(2500,729,2552,208),USE(barcodeIMEINumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('V.A.T.'),AT(5313,604),USE(?vat_String),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,458),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                           STRING(@d6b),AT(719,313),USE(job:Date_QA_Passed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('Total:'),AT(5313,781),USE(?total_string),FONT(,8,,FONT:bold),TRN
                           LINE,AT(6281,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6302,781),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('<128>'),AT(6250,781),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                           STRING('FAULT CODES'),AT(52,1146),USE(?String107),FONT(,8,,FONT:bold),TRN
                           STRING('Booking Option:'),AT(5313,938),USE(?BookingOption),FONT(,8,,FONT:bold),TRN
                           STRING(@s30),AT(6354,938,1406,156),USE(tmp:BookingOption),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                           STRING(@s30),AT(52,1302),USE(fault_code_field_temp[1]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1302,4844,208),USE(tmp:FaultCodeDescription[1]),FONT(,8),TRN
                           STRING(@s30),AT(52,1458),USE(fault_code_field_temp[2]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1458,4844,208),USE(tmp:FaultCodeDescription[2]),FONT(,8)
                           STRING(@s30),AT(52,1615),USE(fault_code_field_temp[3]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1615,4844,208),USE(tmp:FaultCodeDescription[3]),FONT(,8)
                           STRING(@s30),AT(52,1771),USE(fault_code_field_temp[4]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1771,4844,208),USE(tmp:FaultCodeDescription[4]),FONT(,8)
                           STRING(@s30),AT(52,1927),USE(fault_code_field_temp[5]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1927,4844,208),USE(tmp:FaultCodeDescription[5]),FONT(,8)
                           STRING(@s30),AT(52,2083),USE(fault_code_field_temp[6]),FONT(,8),TRN
                           STRING(@s255),AT(2135,2083,4844,208),USE(tmp:FaultCodeDescription[6]),FONT(,8)
                           STRING(@n14.2b),AT(6396,604),USE(vat_temp),FONT(,8),RIGHT,TRN
                           STRING(@s13),AT(990,729,1406,156),USE(jobe2:IDNumber),FONT(,8,,FONT:bold),TRN
                           STRING('Customer ID No:'),AT(52,729),USE(?String66:3),FONT(,8),TRN
                           STRING(@s15),AT(990,573,1563,156),USE(job:Courier),FONT(,8,,FONT:bold),TRN
                           STRING(@t1b),AT(1677,313),USE(job:Time_QA_Passed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING(@s30),AT(2760,938,2031,260),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING('Parts:'),AT(5313,313),USE(?parts_string),FONT(,8),TRN
                         END
                       END
                       FOOTER,AT(396,10271,7521,1156),USE(?Fault_Code9:2)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB CARD'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),FONT(,16,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),FONT(,8,,FONT:bold),TRN
                         STRING('COMPLETION DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobCard')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB CARD'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  locTermsText = p_web.GSV('Default:TermsText')
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('JobCard',ProgressWindow)                   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobCard',ProgressWindow)                ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
    job_Number_Temp = 'Job No: ' & clip(job:Ref_Number)
    esn_Temp = 'I.M.E.I.: ' & clip(job:ESN)
    barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
    barcodeIMEINumber = '*' & clip(job:ESN) & '*'

!  !Barcode Bit and setup refno
!  code_temp            = 1
!  option_temp          = 0
!  
!  bar_code_string_temp = Clip(job:ref_number)
!  !job_number_temp      = 'Job No: ' & Clip(job:ref_number)
!  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
!  
!  bar_code_string_temp = Clip(job:esn)
!  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
!  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
!  
!  job_number_temp = '*' & job:Ref_Number & '*'
!  
!  Settarget(Report)
!  Draw_JobNo.Init256()
!  Draw_JobNo.RenderWholeStrings = 1
!  Draw_JobNo.Resize(Draw_JobNo.Width * 5, Draw_JobNo.Height * 5)
!  Draw_JobNo.Blank(Color:White)
!  Draw_JobNo.FontName = 'C39 High 12pt LJ3'
!  Draw_JobNo.FontStyle = font:Regular
!  Draw_JobNo.FontSize = 24
!  Draw_JobNo.Show(0,0,Clip(job_Number_temp))
!  Draw_JobNo.Display()
!  Draw_JobNo.Kill256()
!  
!  drawer4.Init256()
!  drawer4.RenderWholeStrings = 1
!  drawer4.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer4.Resize(drawer4.Width * 5, drawer4.Height * 5)
!  drawer4.Blank(Color:White)
!  drawer4.FontName = 'C39 High 12pt LJ3'
!  drawer4.FontStyle = font:Regular
!  drawer4.FontSize = 24
!  drawer4.Show(0,0,Clip(job_Number_temp))
!  drawer4.Display()
!  drawer4.Kill256()
!
!  drawer6.Init256()
!  drawer6.RenderWholeStrings = 1
!  drawer6.Resize(drawer6.Width * 5, drawer6.Height * 5)
!  drawer6.Blank(Color:White)
!  drawer6.FontName = 'C39 High 12pt LJ3'
!  drawer6.FontStyle = font:Regular
!  drawer6.FontSize = 24
!  drawer6.Show(0,0,Bar_Code_Temp)
!  drawer6.Display()
!  drawer6.Kill256()
!!  
!  drawer7.Init256()
!  drawer7.RenderWholeStrings = 1
!  drawer7.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer7.Resize()
!  drawer7.Blank(Color:White)
!  drawer7.FontName = 'C39 High 12pt LJ3'
!  drawer7.FontStyle = font:Regular
!  drawer7.FontSize = 24
!  drawer7.Show(0,0,Clip(job_Number_temp))
!  drawer7.Display()
!  drawer7.Kill256()
!!  
!  drawer8.Init256()
!  drawer8.RenderWholeStrings = 1
!  !drawer8.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer8.Resize()
!  drawer8.Blank(Color:White)
!  drawer8.FontName = 'C39 High 12pt LJ3'
!  drawer8.FontStyle = font:Regular
!  drawer8.FontSize = 24
!  drawer8.Show(0,0,Clip(job_Number_temp))
!  drawer8.Display()
!  drawer8.Kill256()
!!  
!!  drawer9.Init256()
!!  drawer9.RenderWholeStrings = 1
!!  drawer9.Resize(drawer9.Width * 5, drawer9.Height * 5)
!!  drawer9.Blank(Color:White)
!!  drawer9.FontName = 'C39 High 12pt LJ3'
!!  drawer9.FontStyle = font:Regular
!!  drawer9.FontSize = 22
!!  drawer9.Show(2,2,Clip(job_Number_temp))
!!  drawer9.Display()
!!  drawer9.Kill256()
!!
!!  !Draw_IMEI.Init256()
!!  !Draw_IMEI.RenderWholeStrings = 1
!!  !Draw_IMEI.Resize(Draw_IMEI.Width * 5, Draw_IMEI.Height * 5)
!!  !Draw_IMEI.Blank(Color:White)
!!  !Draw_IMEI.FontName = 'C39 High 12pt LJ3'
!!  !Draw_IMEI.FontStyle = font:Regular
!!  !Draw_IMEI.FontSize = 48
!!  !Draw_IMEI.Show(0,0,Bar_Code2_Temp)
!!  !Draw_IMEI.Display()
!!  !Draw_IMEI.Kill256()
!  SetTarget()
!  
!  
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
  
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Engineer
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Engineer_Temp = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Engineer_Temp = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Who_Booked
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Who_Booked = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Who_Booked = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Despatched_User_Temp = ''
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  Settarget(Report)
  
  ! Show Engineering Option If Set - 4285 (DBH: 10-06-2004)
  Case jobe:Engineer48HourOption
  Of 1
      tmp:BookingOption         = '48 Hour Exchange'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 2
      tmp:BookingOption         = 'ARC Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 3
      tmp:BookingOption         = '7 Day TAT'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 4
      tmp:BookingOption         = 'Standard Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Else
      Case jobe:Booking48HourOption
      Of 1
          tmp:BookingOption = '48 Hour Exchange'
      Of 2
          tmp:BookingOption = 'ARC Repair'
      Of 3
          tmp:BookingOption = '7 Day TAT'
      Else
          tmp:BookingOption = 'N/A'
      End ! Case jobe:Booking48HourOption
  End ! Case jobe:Engineer48HourOption
  
  If job:warranty_job <> 'YES'
      Hide(?job:warranty_charge_type)
      Hide(?job:repair_type_warranty)
      Hide(?warranty_type)
      Hide(?Warranty_repair_Type)
  End! If job:warranty_job <> 'YES'
  
  If job:chargeable_job <> 'YES'
      Hide(?job:charge_type)
      Hide(?job:repair_type)
      Hide(?Chargeable_type)
      Hide(?repair_Type)
  End! If job:chargeable_job <> 'YES'
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      ! Found
      If man:UseQA
          UnHide(?qa_passed)
      End! If def:qa_required <> 'YES'
  
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  ! Error
  End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  
  If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
      Hide(?labour_string)
      Hide(?parts_string)
      Hide(?Carriage_string)
      Hide(?vat_string)
      Hide(?total_string)
      Hide(?line)
      Hide(?Euro)
  End! If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
  
  If job:estimate = 'YES' and job:Chargeable_Job = 'YES'
      Unhide(?estimate)
      estimate_value_temp = 'ESTIMATE REQUIRED IF REPAIR COST EXCEEDS ' & Clip(Format(job:estimate_if_over, @n14.2)) & ' (Plus V.A.T.)'
  End ! If
  
  
  
  
  
  !------------------------------------------------------------------
  IF CLIP(job:title) = ''
      customer_name_temp = job:initial
  ELSIF CLIP(job:initial) = ''
      customer_name_temp = job:title
  ELSE
      customer_name_temp = CLIP(job:title) & ' ' & CLIP(job:initial)
  END ! IF
  !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job:surname
  ELSIF CLIP(job:surname) = ''
  ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job:surname)
  END ! IF
  !------------------------------------------------------------------
  Settarget()
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  !**************************************************
  delivery_name_temp         = customer_name_temp
  delivery_company_Name_temp = job:Company_Name_Delivery
  
  delivery_address1_temp = job:address_line1_delivery
  delivery_address2_temp = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_telephone_temp     = job:Telephone_Delivery
  !**************************************************
  
  
  !**************************************************
  
  ! If an RRC job, then put the RRC as the invoice address,
  
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Access:WEBJOB.Clearkey(wob:RefNumberKey)
  !    wob:RefNumber   = job:Ref_Number
  !    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !        ! Found
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = wob:HeadAccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            ! Found
  !            invoice_address1_temp = tra:address_line1
  !            invoice_address2_temp = tra:address_line2
  !            If job:address_line3_delivery   = ''
  !                invoice_address3_temp = tra:postcode
  !                invoice_address4_temp = ''
  !            Else
  !                invoice_address3_temp = tra:address_line3
  !                invoice_address4_temp = tra:postcode
  !            End ! If
  !            invoice_company_name_temp     = tra:company_name
  !            invoice_telephone_number_temp = tra:telephone_number
  !            invoice_fax_number_temp       = tra:fax_number
  !            invoice_EMail_Address         = tra:EmailAddress
  !        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !        ! Error
  !        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !    ! Error
  !    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !
  !Else ! If ~glo:WebJob And jobe:WebJob
  
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      access:subtracc.fetch(sub:account_number_key)
  
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress  ! '' ! tra:EmailAddress
          ! MEssage('case 1')
          Else! If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = sub:address_line1
              invoice_address2_temp = sub:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = sub:address_line3
                  invoice_address4_temp = sub:postcode
              End ! If
              invoice_company_name_temp     = sub:company_name
              invoice_telephone_number_temp = sub:telephone_number
              invoice_fax_number_temp       = sub:fax_number
              invoice_EMail_Address         = sub:EmailAddress
  
          End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress ! '' ! tra:EmailAddress
  
          Else! If tra:invoice_customer_address = 'YES'
              !            UseAlternativeAddress# = 1
              invoice_address1_temp = tra:address_line1
              invoice_address2_temp = tra:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = tra:address_line3
                  invoice_address4_temp = tra:postcode
              End ! If
              invoice_company_name_temp     = tra:company_name
              invoice_telephone_number_temp = tra:telephone_number
              invoice_fax_number_temp       = tra:fax_number
              invoice_EMail_Address         = tra:EmailAddress
  
          End! If tra:invoice_customer_address = 'YES'
      end!!if tra:use_sub_accounts = 'YES'
  !End ! If ~glo:WebJob And jobe:WebJob
  
  !**************************************************
  
  
  !*************************set the display address to the head account if booked on as a web job***********************
  ! but only if this was not from a generic account
  
  
  !*********************************************************************************************************************
  
  
  !**************************************************
  If job:chargeable_job <> 'YES'
      labour_temp       = ''
      parts_temp        = ''
      courier_cost_temp = ''
      vat_temp          = ''
      total_temp        = ''
  Else! If job:chargeable_job <> 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job:invoice_number <> ''
          ! Total_Price('I',vat",total",balance")
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          if glo:webjob then
              If inv:ExportedRRCOracle
                  Labour_Temp = jobe:InvRRCCLabourCost
                  Parts_Temp  = jobe:InvRRCCPartsCost
                  Vat_Temp    = jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100 + |
                  jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100 +                 |
                  job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
                  courier_cost_temp = job:invoice_courier_cost
              Else ! If inv:ExportedRRCOracle
                  Labour_Temp = jobe:RRCCLabourCost
                  Parts_Temp  = jobe:RRCCPartsCost
                  Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
                  jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
                  job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
                  courier_cost_temp = job:courier_cost
              End ! If inv:ExportedRRCOracle
          else
              labour_temp = job:invoice_labour_cost
              Parts_temp  = job:Invoice_Parts_Cost
              Vat_temp    = job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100 + |
              job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100 +                 |
              job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
              courier_cost_temp = job:invoice_courier_cost
          end ! if
  
      Else! If job:invoice_number <> ''
          If glo:Webjob Then
              Labour_Temp = jobe:RRCCLabourCost
              Parts_Temp  = jobe:RRCCPartsCost
              Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
              jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          Else
              labour_temp = job:labour_cost
              parts_temp  = job:parts_cost
              Vat_Temp    = job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100 + |
              job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          End ! If
  
          courier_cost_temp = job:courier_cost
      End! If job:invoice_number <> ''
      Total_Temp  = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End! If job:chargeable_job <> 'YES'
  !**************************************************
  
  setcursor(cursor:wait)
  
  FieldNumber# = 1
  save_maf_id  = access:manfault.savefile()
  access:manfault.clearkey(maf:field_number_key)
  maf:manufacturer = job:manufacturer
  set(maf:field_number_key, maf:field_number_key)
  loop
      if access:manfault.next()
          break
      end ! if
      if maf:manufacturer <> job:manufacturer      |
          then break   ! end if
      end ! if
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
  
      If maf:Compulsory_At_Booking <> 'YES'
          Cycle
      End ! If maf:Compulsory_At_Booking <> 'YES'
  
      fault_code_field_temp[FieldNumber#] = maf:field_name
  
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
  
  
      Case maf:Field_Number
      Of 1
          mfo:Field = job:Fault_Code1
      Of 2
          mfo:Field = job:Fault_Code2
      Of 3
          mfo:Field = job:Fault_Code3
      Of 4
          mfo:Field = job:Fault_Code4
      Of 5
          mfo:Field = job:Fault_Code5
      Of 6
          mfo:Field = job:Fault_Code6
      Of 7
          mfo:Field = job:Fault_Code7
      Of 8
          mfo:Field = job:Fault_Code8
      Of 9
          mfo:Field = job:Fault_Code9
      Of 10
          mfo:Field = job:Fault_Code10
      Of 11
          mfo:Field = job:Fault_Code11
      Of 12
          mfo:Field = job:Fault_Code12
      ! Inserting (DBH 21/04/2006) #7551 - Display the extra fault codes, if comp at booking
      Of 13
          mfo:Field = wob:FaultCode13
      Of 14
          mfo:Field = wob:FaultCode14
      Of 15
          mfo:Field = wob:FaultCode15
      Of 16
          mfo:Field = wob:FaultCode16
      Of 17
          mfo:Field = wob:FaultCode17
      Of 18
          mfo:Field = wob:FaultCode18
      Of 19
          mfo:Field = wob:FaultCode19
      Of 20
          mfo:Field = wob:FaultCode20
      End ! Case maf:Field_Number
      ! End (DBH 21/04/2006) #7551
  
      tmp:FaultCodeDescription[FieldNumber#] = mfo:Field
  
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          ! Found
          tmp:FaultCodeDescription[FieldNumber#] = Clip(tmp:FaultCodeDescription[FieldNumber#]) & ' - ' & Clip(mfo:Description)
      Else! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
          ! Error
          ! Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  
      FieldNumber# += 1
      If FieldNumber# > 6
          Break
      End ! If FieldNumber# > 6
  end ! loop
  access:manfault.restorefile(save_maf_id)
  setcursor()
  
  tmp:accessories = ''
  setcursor(cursor:wait)
  x#          = 0
  Save_jac_ID = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged    = 0
  jac:Pirate     = 0
  Set(jac:DamagedPirateKey, jac:DamagedPirateKey)
  Loop
      If Access:JOBACC.NEXT()
          Break
      End ! If
      If jac:Ref_Number <> job:Ref_Number |
          Then Break
      End ! If
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
      x# += 1
      If jac:Damaged
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged
      If jac:Pirate
          jac:Accessory = 'PIRATE ' & Clip(jac:Accessory)
      End ! If jac:Pirate
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else! If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End! If tmp:accessories = ''
  End ! loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  If job:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end! if access:exchange.fetch(xch:ref_number_key) = Level:Benign
  Else! If job:exchange_unit_number <> ''
      x#          = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job:ref_number
      set(jea:part_number_key, jea:part_number_key)
      loop
          if access:jobexacc.next()
              break
          end ! if
          if jea:job_ref_number <> job:ref_number      |
              then break   ! end if
          end ! if
          x# = 1
          Break
      end ! loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End! If x# = 1
  End! If job:exchange_unit_number <> ''
  
  ! Check For Bouncer
  access:manufact.clearkey(man:manufacturer_key)
  man:manufacturer = job:manufacturer
  access:manufact.tryfetch(man:manufacturer_key)
  
  tmp:bouncers = ''
  If job:ESN <> 'N/A' And job:ESN <> ''
      setcursor(cursor:wait)
      save_job2_id = access:jobs2_alias.savefile()
      access:jobs2_alias.clearkey(job2:esn_key)
      job2:esn = job:esn
      set(job2:esn_key, job2:esn_key)
      loop
          if access:jobs2_alias.next()
              break
          end ! if
          if job2:esn <> job:esn      |
              then break   ! end if
          end ! if
          yldcnt# += 1
          if yldcnt# > 25
              yield()
              yldcnt# = 0
          end ! if
          If job2:Cancelled = 'YES'
              Cycle
          End ! If job2:Cancelled = 'YES'
  
          If job2:ref_number <> job:ref_number
  !            If job2:date_booked + man:warranty_period > job:date_booked And job2:date_booked <= job:date_booked
                  If tmp:bouncers = ''
                      tmp:bouncers = 'Previous Job Number(s): ' & job2:ref_number
                  Else! If tmp:bouncer = ''
                      tmp:bouncers = CLip(tmp:bouncers) & ', ' & job2:ref_number
                  End! If tmp:bouncer = ''
  !            End! If job2:date_booked + man:warranty_period < Today()
          End! If job2:esn <> job2:ref_number
      end ! loop
      access:jobs2_alias.restorefile(save_job2_id)
      setcursor()
  End ! job:ESN <> 'N/A' And job:ESN <> ''
  
  If tmp:bouncers <> ''
      Settarget(report)
      Unhide(?bouncertitle)
      Unhide(?tmp:bouncers)
      Settarget()
  End! If CheckBouncer(job:ref_number)
  
  If man:UseInvTextForFaults
      tmp:InvoiceText = ''
      Save_joo_ID     = Access:JOBOUTFL.SaveFile()
      Access:JOBOUTFL.ClearKey(joo:LevelKey)
      joo:JobNumber = job:Ref_Number
      joo:Level     = 10
      Set(joo:LevelKey, joo:LevelKey)
      Loop
          If Access:JOBOUTFL.PREVIOUS()
              Break
          End ! If
          If joo:JobNumber <> job:Ref_Number      |
              Then Break   ! End If
          End ! If
          If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          Else ! If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          End ! If tmp:InvoiceText = ''
      End ! Loop
      Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  Else ! man:UseInvTextForFaults
      tmp:InvoiceText = jbn:Invoice_Text
  End ! man:UseInvTextForFaults
  
  ! Loan unit information (Added by Gary (7th Aug 2002))
  ! //------------------------------------------------------------------------
  tmp:LoanModel       = ''
  tmp:LoanIMEI        = ''
  tmp:LoanAccessories = ''
  
  if job:Loan_Unit_Number <> 0                                            ! Is a loan unit attached to this job ?
  
      save_loa_id = Access:Loan.SaveFile()
      Access:Loan.Clearkey(loa:Ref_Number_Key)
      loa:Ref_Number  = job:Loan_Unit_Number
      if not Access:Loan.TryFetch(loa:Ref_Number_Key)                         ! Fetch loan unit record
          tmp:LoanModel        = clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI         = loa:ESN
          tmp:ReplacementValue = jobe:LoanReplacementValue
  
          save_lac_id = Access:LoanAcc.SaveFile()
          Access:LoanAcc.ClearKey(lac:ref_number_key)
          lac:ref_number = loa:ref_number
          set(lac:ref_number_key, lac:ref_number_key)
          loop until Access:LoanAcc.Next()                                    ! Fetch loan accessories
              if lac:ref_number <> loa:ref_number then break.
              if lac:Accessory_Status <> 'ISSUE' then cycle.                  ! Only display issued loan accessories
              if tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              else
                  tmp:LoanAccessories = clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              end ! if
  
          end ! loop
          Access:LoanAcc.RestoreFile(save_lac_id)
  
      end ! if
      Access:Loan.RestoreFile(save_loa_id)
  
  end ! if
  
  if tmp:LoanModel <> ''                                                      ! Display loan details on report
      SetTarget(report)
      Unhide(?LoanUnitIssued)
      Unhide(?tmp:LoanModel)
      Unhide(?IMEITitle)
      Unhide(?tmp:LoanIMEI)
      Unhide(?LoanAccessoriesTitle)
      Unhide(?tmp:LoanAccessories)
      Unhide(?tmp:ReplacementValue)
      Unhide(?LoanValueText)
      SetTarget()
  end ! if
  
  
  tmp:LoanDepositPaid = 0
  
  Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number   = job:Ref_Number
  jpt:Loan_Deposit = True
  set(jpt:Loan_Deposit_Key, jpt:Loan_Deposit_Key)
  loop until Access:JobPaymt.Next()                                           ! Loop through payments attached to job
      if jpt:Ref_Number <> job:Ref_Number then break.
      if jpt:Loan_Deposit <> True then break.
      tmp:LoanDepositPaid += jpt:Amount                                       ! Check for deposit value
  end ! loop
  
  if tmp:LoanDepositPaid <> 0
      SetTarget(report)
      Unhide(?LoanDepositPaidTitle)
      Unhide(?tmp:LoanDepositPaid)                                            ! Display fields if a deposit was found
      SetTarget()
  end ! if
  ! //------------------------------------------------------------------------
  
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName(loc:PDFName)
  SELF.SetDocumentInfo('Job Card','ServiceBase OnLine','ServiceBase OnLine','Job Card','ServiceBase OnLine','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = False
  SELF.CompressImages = False
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

TotalPrice           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
paid_chargeable_temp REAL                                  !
paid_warranty_temp   REAL                                  !
labour_rate_temp     REAL                                  !
parts_rate_temp      REAL                                  !
FilesOpened     BYTE(0)
  CODE
!How much has been paid?
! Parameters
! TotalPrice:VAT
! TotalPrice:Total
! TotalPrice:Balance
! TotalPrice:Type

    do openFiles
    p_web.SSV('TotalPrice:VAT',0)
    p_web.SSV('TotalPrice:Total',0)
    p_web.SSV('TotalPrice:Balance',0)


    Paid_Warranty_Temp = 0
    Paid_Chargeable_Temp = 0
    Access:JOBPAYMT_ALIAS.Clearkey(jpt_ali:All_Date_Key)
    jpt_ali:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(jpt_ali:All_Date_Key,jpt_ali:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT_ALIAS.Next()
            Break
        End ! If Access:JOBPAYMT_ALIAS.Next()
        If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number

        Paid_Chargeable_Temp += jpt_ali:Amount
    End ! Loop

    !Look up VAT Rates
    Labour_Rate_Temp = 0
    Parts_Rate_Temp = 0

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Case p_web.GSV('TotalPrice:Type')
    Of 'C' !Chargeable
        If p_web.GSV('BookingSite') = 'RRC'
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCCLabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCCPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCCLabourCost') + |
                        p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:courier_cost') + p_web.GSV('TotalPrice:VAT'))

            p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)

        Else !If glo:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost') + |
                    p_web.GSV('job:parts_cost') + p_web.GSV('job:courier_cost') + p_web.GSV('TotalPrice:VAT'))

            p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
        End !If glo:WebJob
    Of 'W' ! Warranty
        If p_web.GSV('BookingSite') = 'RRC'
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCWLabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCWPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCWLabourCost') + |
                        p_web.GSV('jobe:RRCWPartsCost') + p_web.GSV('job:courier_cost_warranty') + p_web.GSV('TotalPrice:VAT'))

        Else !If glo:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost_warranty') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost_warranty') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost_warranty') + |
                        p_web.GSV('job:parts_cost_warranty') + p_web.GSV('job:courier_cost_warranty') + p_web.GSV('TotalPrice:VAT'))

        End !If glo:WebJob
    Of 'E' !Estimate
        If p_web.GSV('jobe:WebJob') = 1
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCELabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCEPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCELabourCost') + |
                        p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:courier_cost_estimate') + p_web.GSV('TotalPrice:VAT'))

        Else !If p_web.GSV('jobe:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost_estimate') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost_estimate') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost_estimate') + |
                        p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate') + p_web.GSV('TotalPrice:VAT'))

        End !If p_web.GSV('jobe:WebJob
    Of 'I' ! Chargealbe Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:InvRRCCLabourCost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100))

                p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:InvRRCCLabourCost') + |
                        p_web.GSV('jobe:InvRRCCPartsCost') + p_web.GSV('job:invoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            Else !If glo:webJOb
                p_web.SSV('TotalPrice:VAT',p_web.GSV('job:invoice_labour_cost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('job:invoice_parts_cost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100))

                p_web.SSV('TotalPrice:Total',p_web.GSV('job:invoice_labour_cost') + |
                            p_web.GSV('job:invoice_parts_cost') + p_web.GSV('job:invoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            End !If glo:webJOb

        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    Of 'V' ! Warranty Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number_Warranty')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:InvRRCWLabourCost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('jobe:InvRRCWPartsCost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100))
                p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:InvRRCWLabourCost') + p_web.GSV('jobe:InvRRCWPartsCost') + |
                            p_web.GSV('job:Winvoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            Else !If glo:WebJob
                p_web.SSV('TotalPrice:VAT',p_web.GSV('job:Winvoice_labour_cost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('job:Winvoice_parts_cost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100))
                p_web.SSV('TotalPrice:Total',p_web.GSV('job:Winvoice_labour_cost') + p_web.GSV('job:Winvoice_parts_cost') + |
                            p_web.GSV('job:Winvoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            End !If glo:WebJob
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! Case f:Type

    p_web.SSV('TotalPrice:VAT',Round(p_web.GSV('TotalPrice:VAT'),.01))
    p_web.SSV('TotalPrice:Total',Round(p_web.GSV('TotalPrice:Total'),.01))
    p_web.SSV('TotalPrice:Balance',Round(p_web.GSV('TotalPrice:Balance'),.01))
    p_web.SSV('TotalPrice:Type','')

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.Open                               ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.UseFile                            ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     Access:JOBPAYMT_ALIAS.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Report
!!! Estimate
!!! </summary>
Estimate PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
RejectRecord         LONG,AUTO                             !
tmp:Ref_Number       STRING(20)                            !
tmp:PrintedBy        STRING(255)                           !
save_epr_id          USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Company_Name_Temp STRING(30)                      !Delivery Company Name
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Number_Temp STRING(30)                  !Delivery Telephone Number
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(40)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(24)                            !
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
customer_name_temp   STRING(60)                            !
sub_total_temp       REAL                                  !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:InvoiceText      STRING(255)                           !Invoice Text
DefaultAddress       GROUP,PRE(address)                    !
Location             STRING(40)                            !
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(30)                            !
Name                 STRING(40)                            !Name
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
locRefNumber         LONG                                  !
locAccountNumber     STRING(30)                            !
locOrderNumber       STRING(30)                            !
locAuthorityNumber   STRING(20)                            !
locChargeType        STRING(20)                            !
locRepairType        STRING(20)                            !
locModelNumber       STRING(30)                            !
locManufacturer      STRING(30)                            !
locUnitType          STRING(30)                            !
locESN               STRING(20)                            !
locMSN               STRING(20)                            !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,6917,7521,1927),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,6313),USE(?unnamed)
                         STRING('Job No:'),AT(4896,365),USE(?String25),FONT(,12),TRN
                         STRING(@s16),AT(5729,365),USE(tmp:Ref_Number),FONT(,12,,FONT:bold),LEFT,TRN
                         STRING('Estimate Date:'),AT(4896,573),USE(?String58),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(5729,573),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),FONT(,8),TRN
                         STRING(@s60),AT(4063,1563),USE(customer_name_temp,,?customer_name_temp:2),FONT(,8),TRN
                         STRING(@s20),AT(1604,3240),USE(locOrderNumber),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4531,3240),USE(locChargeType),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3156,3240),USE(locAuthorityNumber),FONT(,8),TRN
                         STRING(@s20),AT(6042,3229),USE(locRepairType),FONT(,8),TRN
                         STRING(@s30),AT(156,3917,1000,156),USE(locModelNumber),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1604,3917,1323,156),USE(locManufacturer),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3156,3917,1396,156),USE(locUnitType),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4604,3917,1396,156),USE(locESN),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6083,3917),USE(locMSN),FONT(,8),TRN
                         STRING('Reported Fault: '),AT(156,4323),USE(?String64),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(1615,4323,5625,417),USE(jbn:Fault_Description),FONT(,8,,,CHARSET:ANSI),TRN
                         TEXT,AT(1615,5052,5625,885),USE(tmp:InvoiceText),FONT(,8,,,CHARSET:ANSI),TRN
                         STRING('Engineers Report:'),AT(156,5000),USE(?String88),FONT(,8,,FONT:bold),TRN
                         GROUP,AT(104,5938,7500,208),USE(?PartsHeadingGroup),FONT('Tahoma')
                           STRING('PARTS REQUIRED'),AT(156,6042),USE(?String79),FONT(,9,,FONT:bold),TRN
                           STRING('Qty'),AT(1510,6042),USE(?String80),FONT(,8,,FONT:bold),TRN
                           STRING('Part Number'),AT(1917,6042),USE(?String81),FONT(,8,,FONT:bold),TRN
                           STRING('Description'),AT(3677,6042),USE(?String82),FONT(,8,,FONT:bold),TRN
                           STRING('Unit Cost'),AT(5719,6042),USE(?UnitCost),FONT(,8,,FONT:bold),TRN
                           STRING('Line Cost'),AT(6677,6042),USE(?LineCost),FONT(,8,,FONT:bold),TRN
                           LINE,AT(1406,6197,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4063,2500),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2500),USE(Delivery_Telephone_Number_Temp),FONT(,8)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2240,2500),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(156,3240),USE(locAccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(4063,1719),USE(Delivery_Company_Name_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,1875,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2031),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2188),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2344),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),FONT(,8),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,198),USE(?DetailBand)
                         STRING(@n8b),AT(1156,0),USE(Quantity_temp),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,8),TRN
                         STRING(@s25),AT(3677,0),USE(Description_temp),FONT(,8),TRN
                         STRING(@n14.2b),AT(5208,0),USE(Cost_Temp),FONT(,8),RIGHT,TRN
                         STRING(@n14.2b),AT(6198,0),USE(line_cost_temp),FONT(,8),RIGHT,TRN
                       END
                       FOOTER,AT(385,9177,7521,2146),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,52),USE(?labour_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,52),USE(labour_temp),FONT(,8),RIGHT,TRN
                         STRING(@s20),AT(2865,313,1771,156),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING(@s20),AT(2875,83,1760,198),USE(Bar_Code_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING('Carriage:'),AT(4844,365),USE(?carriage_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,208),USE(parts_temp),FONT(,8),RIGHT,TRN
                         STRING('V.A.T.'),AT(4844,677),USE(?vat_String),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,365),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                         STRING('Total:'),AT(4844,833),USE(?total_string),FONT(,8,,FONT:bold),TRN
                         STRING('<128>'),AT(6146,833,156,208),USE(?Euro),FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         LINE,AT(6354,833,1000,0),USE(?line),COLOR(COLOR:Black)
                         LINE,AT(6354,521,1000,0),USE(?line:2),COLOR(COLOR:Black)
                         STRING(@n14.2b),AT(6198,833),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Sub Total:'),AT(4844,521),USE(?String92),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,521),USE(sub_total_temp),FONT(,8),RIGHT,TRN
                         STRING(@s20),AT(2865,521,1760,198),USE(Bar_Code2_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING(@n14.2b),AT(6250,677),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING('Parts:'),AT(4844,208),USE(?parts_string),FONT(,8),TRN
                         TEXT,AT(104,1094,7365,958),USE(stt:Text),FONT(,8)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,11198),USE(?Image1)
                         STRING('ESTIMATE'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),FONT(,16,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),FONT(,9,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1500),USE(?String24),FONT(,9,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),FONT(,9,,FONT:bold),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),FONT(,9,,FONT:bold),TRN
                         STRING('AUTHORISATION DETAILS'),AT(156,8479),USE(?String73),FONT(,9,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),FONT(,9,,FONT:bold),TRN
                         STRING('Estimate Accepted'),AT(208,8750),USE(?String94),FONT(,8,,FONT:bold),TRN
                         BOX,AT(1406,8750,156,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Estimate Refused'),AT(208,8958),USE(?String95),FONT(,8,,FONT:bold),TRN
                         BOX,AT(1406,8958,156,156),USE(?Box2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name (Capitals)'),AT(208,9219),USE(?String96),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1198,9323,1354,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Signature'),AT(208,9479),USE(?String97),FONT(,8,,FONT:bold),TRN
                         LINE,AT(833,9583,1719,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Model'),AT(156,3844),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1635,3844),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4563,3844),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),FONT(,9,,FONT:bold),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Estimate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:VATCODE.Open                                      ! File VATCODE used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAUPA.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:CHARTYPE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locRefNumber = p_web.GSV('job:Ref_Number')
  
  set(DEFAULTS)
  access:DEFAULTS.next()
  
  set(DEFAULT2)
  access:DEFAULT2.next()
  
  
  locAccountNumber = p_web.GSV('job:Account_Number')
  locOrderNumber = p_web.GSV('job:Order_Number')
  locAuthorityNumber = p_web.GSV('job:Authority_Number')
  locChargeType = p_web.GSV('job:Charge_Type')
  locRepairType = p_web.GSV('job:Repair_Type')
  locModelNumber = p_web.GSV('job:Model_Number')
  locManufacturer = p_web.GSV('job:Manufacturer')
  locUnitType = p_web.GSV('job:Unit_Type')
  locESN = p_web.GSV('job:ESN')
  locMSN = p_web.GSV('job:MSN')
  SELF.Open(ProgressWindow)                                ! Open window
  System{prop:Icon} = '~cellular3g.ico'
  0{prop:Icon} = '~cellular3g.ico'
  Do DefineListboxStyle
  INIMgr.Fetch('Estimate',ProgressWindow)                  ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locRefNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetReportTarget(PDFReporter.IReportGenerator)
    SELF.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = '$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Estimate',ProgressWindow)               ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(clip(p_web.site.WebFolderPath) & '\' &loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Delivery_Company_Name_Temp = p_web.GSV('job:Company_Name_Delivery')
      delivery_address1_temp     = p_web.GSV('job:address_line1_delivery')
      delivery_address2_temp     = p_web.GSV('job:address_line2_delivery')
      If p_web.GSV('job:address_line3_delivery')   = ''
          delivery_address3_temp = p_web.GSV('job:postcode_delivery')
          delivery_address4_temp = ''
      Else
          delivery_address3_temp  = p_web.GSV('job:address_line3_delivery')
          delivery_address4_temp  = p_web.GSV('job:postcode_delivery')
      End
      Delivery_Telephone_Number_Temp = p_web.GSV('job:Telephone_Delivery')
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = p_web.GSV('job:account_number')
      access:subtracc.fetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
          Else!If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = sub:address_line1
              invoice_address2_temp     = sub:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
              invoice_company_name_temp   = sub:company_name
              invoice_telephone_number_temp   = sub:telephone_number
              invoice_fax_number_temp = sub:fax_number
  
  
          End!If sub:invoice_customer_address = 'YES'
  
      else!if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
  
          Else!If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = tra:address_line1
              invoice_address2_temp     = tra:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = tra:address_line3
                  invoice_address4_temp  = tra:postcode
              End
              invoice_company_name_temp   = tra:company_name
              invoice_telephone_number_temp   = tra:telephone_number
              invoice_fax_number_temp = tra:fax_number
  
          End!If tra:invoice_customer_address = 'YES'
      End!!if tra:use_sub_accounts = 'YES'
  
  
      if p_web.GSV('job:title') = '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') = '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      else
          customer_name_temp = ''
      end
  
      access:stantext.clearkey(stt:description_key)
      stt:description = 'ESTIMATE'
      access:stantext.fetch(stt:description_key)
  
  
      courier_cost_temp = p_web.GSV('job:courier_cost_estimate')
  
      !If booked at RRC, use RRC costs for estimate
      If p_web.GSV('jobe:WebJob') = 1
          Labour_Temp = p_web.GSV('jobe:RRCELabourCost')
          Parts_Temp  = p_web.GSV('jobe:RRCEPartsCost')
          Sub_Total_Temp = p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate')
      Else !p_web.GSV('jobe:WebJob')
          labour_temp = p_web.GSV('job:labour_cost_estimate')
          parts_temp  = p_web.GSV('job:parts_cost_estimate')
          sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate')
      End !p_web.GSV('jobe:WebJob')
  
  
        ! Inserting (DBH 03/12/2007) # 8218 - Use the ARC details, if it's an RRC-ARC estimate job
        sentToHub(p_web)
        If p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob') = 1
            ! ARC Estimate, use ARC Details
            If p_web.GSV('BookingSite') <> 'RRC'
                ! Use ARC Details
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        Customer_Name_Temp = ''
                        Invoice_Company_Name_Temp = tra:Company_Name
                        Invoice_Address1_Temp = tra:Address_Line1
                        Invoice_Address2_Temp = tra:Address_Line2
                        Invoice_Address3_Temp = tra:Address_Line3
                        Invoice_Address4_Temp = tra:Postcode
                        Invoice_Telephone_Number_Temp = tra:Telephone_Number
                        Invoice_Fax_Number_Temp = tra:Fax_Number
  
                        Delivery_Company_Name_Temp = tra:Company_Name
                        Delivery_Address1_Temp = tra:Address_Line1
                        Delivery_Address2_Temp = tra:Address_Line2
                        Delivery_Address3_Temp = tra:Address_Line3
                        Delivery_Address4_Temp = tra:Postcode
                        Delivery_Telephone_Number_Temp = tra:Telephone_Number
  
                        labour_temp = p_web.GSV('job:labour_cost_estimate')
                        parts_temp  = p_web.GSV('job:parts_cost_estimate')
                        sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate')+ p_web.GSV('job:courier_cost_estimate')
  
                    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            End ! If glo:WebJob
        End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
        ! End (DBH 03/12/2007) #8218
  
      p_web.SSV('TotalPrice:Type','E')
      totalPrice(p_web)
      vat_temp = p_web.GSV('TotalPrice:VAT')
      total_temp = p_web.GSV('TotalPrice:Total')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If ~man:UseInvTextForFaults
              tmp:InvoiceText = jbn:Invoice_Text
          Else !If ~man:UseInvTextForFaults
              tmp:InvoiceText = ''
              Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
              joo:JobNumber = p_web.GSV('job:Ref_Number')
              Set(joo:JobNumberKey,joo:JobNumberKey)
              Loop
                  If Access:JOBOUTFL.NEXT()
                     Break
                  End !If
                  If joo:JobNumber <> p_web.GSV('job:Ref_Number')      |
                      Then Break.  ! End If
                  ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
                  If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                      Cycle
                  End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                  ! End (DBH 16/10/2006) #8059
                  If tmp:InvoiceText = ''
                      tmp:InvoiceText = Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  Else !If tmp:Invoice_Text = ''
                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  End !If tmp:Invoice_Text = ''
              End !Loop
  
              Access:MANFAUPA.ClearKey(map:MainFaultKey)
              map:Manufacturer = p_web.GSV('job:Manufacturer')
              map:MainFault    = 1
              If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Found
                  Access:MANFAULT.ClearKey(maf:MainFaultKey)
                  maf:Manufacturer = p_web.GSV('job:Manufacturer')
                  maf:MainFault    = 1
                  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Found
                      Save_epr_ID = Access:ESTPARTS.SaveFile()
                      Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                      epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                      Set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop
                          If Access:ESTPARTS.NEXT()
                             Break
                          End !If
                          If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                              Then Break.  ! End If
                          Access:MANFAULO.ClearKey(mfo:Field_Key)
                          mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfo:Field_Number = maf:Field_Number
  
                          Case map:Field_Number
                              Of 1
                                  mfo:Field   = epr:Fault_Code1
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code1) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 2
                                  mfo:Field   = epr:Fault_Code2
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code2) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 3
                                  mfo:Field   = epr:Fault_Code3
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code3) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 4
                                  mfo:Field   = epr:Fault_Code4
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code4) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 5
                                  mfo:Field   = epr:Fault_Code5
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code5) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 6
                                  mfo:Field   = epr:Fault_Code6
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code6) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 7
                                  mfo:Field   = epr:Fault_Code7
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code7) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 8
                                  mfo:Field   = epr:Fault_Code8
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code8) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 9
                                  mfo:Field   = epr:Fault_Code9
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code9) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 10
                                  mfo:Field   = epr:Fault_Code10
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code10) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 11
                                  mfo:Field   = epr:Fault_Code11
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code11) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 12
                                  mfo:Field   = epr:Fault_Code12
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code12) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          End !Case map:Field_Number
                      End !Loop
  
                  Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
              Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
  
          End !If ~man:UseInvTextForFaults
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
      End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      job_Number_Temp = locRefNumber
      esn_Temp = locESN
      Bar_Code_Temp = '*' & clip(job_Number_Temp) & '*'
      Bar_Code2_Temp = '*' & clip(esn_Temp) & '*'
  
      job_Number_temp = 'Job No: ' & clip(job_Number_Temp)
      esn_Temp = 'IMEI No: ' & clip(esn_Temp)
  !*********CHANGE LICENSE ADDRESS*********
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  tmp:Ref_Number = p_web.GSV('Job:Ref_Number') & '-' & |
      tra:BranchIdentification & p_web.GSV('wob:JobNumber')
      
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
      jbn:Fault_Description = p_web.GSV('jbn:Fault_Description')
  
  
      count# = 0
      FixedCharge# = 0
      Access:CHARTYPE.ClearKey(cha:Warranty_Key)
      cha:Warranty    = 'NO'
      cha:Charge_Type = p_web.GSV('job:Charge_Type')
      If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Found
          If glo:WebJob
              If cha:Zero_Parts
                  FixedCharge# = 1
              End !If cha:Fixed_Charge
          Else !If glo:WebJob
              If cha:Zero_Parts_ARC
                  FixedCharge# = 1
              End !If cha:Fixed_Charge_ARC
          End !If glo:WebJob
      Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
  
      If FixedCharge#
          SetTarget(Report)
          ?Cost_Temp{prop:Hide} = 1
          ?Line_Cost_Temp{prop:Hide} = 1
          ?UnitCost{prop:Hide} = 1
          ?LineCost{prop:Hide} = 1
          SetTarget()
      End !FixedCharge#
  
      access:estparts.clearkey(epr:part_number_key)
      epr:ref_number  = p_web.GSV('job:ref_number')
      set(epr:part_number_key,epr:part_number_key)
      loop
          if access:estparts.next()
             break
          end !if
          if epr:ref_number  <> p_web.GSV('job:ref_number')      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          count# += 1
          part_number_temp = epr:part_number
          description_temp = epr:description
          quantity_temp = epr:quantity
          cost_temp = epr:sale_cost
          line_cost_temp = epr:quantity * epr:sale_cost
          Print(rpt:detail)
  
      end !loop
  
      If count# = 0
          SetTarget(Report)
          ?PartsHeadingGroup{Prop:Hide} = 1
          Part_Number_Temp = ''
          !Print Blank Line
          Print(Rpt:Detail)
          SetTarget()
      End!If count# = 0
  
  
      If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:estimate_accepted') <> 'YES' and p_web.GSV('job:estimate_rejected') <> 'YES'
  !        p_web.SSV('GetStatus:Type','JOB')
  !        p_web.SSV('GetStatus:StatusNumber',520)
  !        getStatus(p_web)
  !
  !        Access:JOBS.Clearkey(job:ref_Number_Key)
  !        job:ref_Number    = p_web.GSV('job:Ref_Number')
  !        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(JOBS)
  !            access:JOBS.tryUpdate()
  !        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Error
  !        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !
  !
  !        Access:WEBJOB.Clearkey(wob:refNumberKey)
  !        wob:refNumber    = p_web.GSV('job:Ref_Number')
  !        if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(WEBJOB)
  !            access:WEBJOB.tryUpdate()
  !        else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Error
  !        end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !
  !        p_web.SSV('AddToAudit:Type','JOB')
  !        p_web.SSV('AddToAudit:Notes','ESTIMATE VALUE: ' & Format(Total_Temp,@n14.2))
  !        p_web.SSV('AddToAudit:Action','ESTIMATE SENT')
  !        addToAudit(p_web)
  
      End!If p_web.GSV('job:estimate = 'YES' And p_web.GSV('job:estimate_accepted <> 'YES' and p_web.GSV('job:estimate_refused <> 'YES'
  
  
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:DETAIL)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer','Estimate','Estimate','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(p_web.site.WebFolderPath) & '\' & clip(loc:PDFName))
  End

SentToHub            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)
  CODE
    do openFiles
    p_web.SSV('SentToHub',0)
    if (p_web.GSV('jobe:HubRepair') = 1 Or p_web.GSV('jobe:HubRepairDate') > 0)
        p_web.SSV('SentToHub',1)
    Else !If jobe:HubRepair
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = p_web.GSV('job:Ref_Number')
        lot:NewLocation = p_web.GSV('Default:ARCLocation')
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
            p_web.SSV('SentToHub',1)
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
    End !If jobe:HubRepair
    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
