

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module


   INCLUDE('NetWeb.inc'),ONCE

                     MAP
                       INCLUDE('WEBSERVER_PHASE3022.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3019.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3023.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3024.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3025.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3026.INC'),ONCE        !Req'd for module callout resolution
                     END


IndividualDespatch   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
tmp:LoanModelNumber  STRING(30)                            !
Ans                  LONG                                  !
locAccessoryErrorMessage STRING(255)                       !
locIMEINumber        STRING(30)                            !
locJobNumber         LONG                                  !
locErrorMessage      STRING(255)                           !
locMessage           STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locSecurityPackID    STRING(30)                            !
locAccessoryPassword STRING(30)                            !
locAccessoryPasswordMessage STRING(255)                    !
locAuditTrail        STRING(255)                           !
locConsignmentNumber STRING(30)                            !
FilesOpened     Long
LOANACC::State  USHORT
JOBACC::State  USHORT
WEBJOB::State  USHORT
WAYBILLJ::State  USHORT
WAYBILLS::State  USHORT
TagFile::State  USHORT
COURIER::State  USHORT
MULDESPJ::State  USHORT
SUBTRACC::State  USHORT
LOAN::State  USHORT
TRADEACC::State  USHORT
EXCHANGE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
cou:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('IndividualDespatch')
  loc:formname = 'IndividualDespatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('IndividualDespatch','')
    p_web._DivHeader('IndividualDespatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      Routine
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')        
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('Show:DespatchInvoiceDetails',0) ! Show invoice details on despatch screen
    p_web.SSV('CreateDespatchInvoice',0) ! Auto create invoice
    p_web.SSV('Show:ConsignmentNumber',0)

ValidateUnitDetails Routine
    ClearTagFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locErrorMessage','')
    p_web.SSV('locMessage','')
    p_web.SSV('DespatchType','')
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('locSecurityPackID','')
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')    
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)

    if (p_web.GSV('locJobNumber')= '')
        exit
    end
    if (p_web.GSV('locIMEINumber') = '')
        exit
    end

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    If (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage','Cannot find selected Job Number!')
        Exit
    end

        
    if (JobInUse(job:Ref_Number))
        p_web.SSV('locErrorMessage','Cannot despatch the selected job is in use.')
        exit
    end
    
    p_web.FileToSessionQueue(JOBS)
    
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
    End
    p_web.FileToSessionQueue(JOBSE)
    
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber  = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END
    p_web.FileToSessionQueue(WEBJOB)
    
    if (p_web.GSV('BookingSite') = 'RRC')
        ! RRC Despatch
        
        IF (wob:ReadyToDespatch <> 1)
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            EXIT
        END
        
        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            p_web.SSV('locErrorMessage','The selected job is not from your RRC.')
            EXIT
        END
        
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                if (sub:UseCustDespAdd = 'YES')
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubCustomer) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                else
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubDelivery) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                end
            end
        end

        found# = 0
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        set(mulj:JobNumberOnlyKey,mulj:JobNumberOnlyKey)
        Loop Until Access:MULDESPJ.Next()
            if (mulj:JobNumber <> job:Ref_Number)
                break
            end
            Access:MULDESP.Clearkey(muld:RecordNumberKey)
            muld:RecordNumber = mulj:RefNumber
            if (Access:MULDESP.TryFetch(muld:RecordNumberKey)= Level:Benign)
                if (muld:HeadAccountNumber = p_web.GSV('BookingAccount'))
                    found# = 1
                    break
                end
            end
        end
        if (found# = 1)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end
        
        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And jobe:Despatchtype = 'JOB')
            IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1)) 
                ! #11817 Only stop despatch if RRC, or ARC back to customer. (Bryan: 11/05/2011)
                if (job:Loan_Unit_Number <> 0)
                    p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                    exit
                end
            END !IF (jobe:WebJob <> 1)
            
        end
        
        Access:COURIER.ClearKey(cou:Courier_Key)
        CASE jobe:DespatchType
        OF 'JOB'
            cou:Courier = job:Courier
        OF 'EXC'
            cou:Courier = job:Exchange_Courier
        of 'LOA'
            cou:Courier = job:Loan_Courier
        end
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit            
        end
        
        
        p_web.SSV('DespatchType',jobe:DespatchType)
        
       
        
    else ! if (p_web.GSV('BookingSite') = 'RRC')
        ! ARC Despatch
        
        ! Can job be despatched?
        if (job:Chargeable_Job = 'YES')
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('Default:ARCLocation')
            if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) )
                
            End 
            if (tra:Despatch_Paid_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
                p_web.SSV('locErrorMessage','Unable to despatch. The selected job has not been paid.')
                exit
            end
            if (tra:Despatch_Invoiced_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
                p_web.SSV('locErrorMessage','Unble to despatch. The selected job has not been invoiced.')
                exit
            end
        end
        
        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And job:Despatch_type = 'JOB')
            if (job:Loan_Unit_Number <> 0)
                p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                exit
            end
        end
        
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        if (Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:benign)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end
        
        if (job:Despatched <> 'REA')
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            exit
        end

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_NUmber_Key) = Level:Benign)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            if (access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                if (tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES')
                    if (sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                        
                else
                    if (tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                end
            end
        end
        
        Access:COURIER.Clearkey(cou:Courier_Key)
        cou:Courier = job:Current_Courier
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit
        end
        
        p_web.SSV('DespatchType',job:Despatch_Type)
        
    end
    
    p_web.FileToSessionQueue(COURIER)
    
    case p_web.GSV('DespatchType')
    of 'JOB'
        if (job:ESN <> p_web.GSV('locIMEINumber'))
            p_web.SSV('locErrorMessage','The selected I.M.E.I. Number does not match the selected job.')
            exit
        end
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    of 'EXC'
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Exchange Despatch! Unable to find Exchange Unit on selected job.')
            exit
        else
            if (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Exchange Despatch! The selected I.M.E.I. Number does not match the Exchange Unit attached to the selected job.')
                exit
            end
        end
        p_web.SSV('tmp:LoanModelNumber',xch:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('ValidateAccessories',0)
        p_web.SSV('Hide:ValidateAccessoriesButton',1)
        p_web.SSV('AccessoriesValidated',1)
    of 'LOA'
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Loan Despatch! Unable to find Loan Unit on selected job.')
            exit
        else
            if (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Loan Despatch! The selected I.M.E.I. Number does not match the Loan Unit attached to the selected job.')
                exit    
            end
        end
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Loan_Unit_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    end

    
    p_web.SSV('UnitValidated',1)
    
    p_web.SSV('ValidateButtonText','Despatch Another Unit')
    
    ! Can the job be invoiced?
    IF (job:Bouncer <> 'X' AND job:Chargeable_Job = 'YES')
        p_web.SSV('Show:DespatchInvoiceDetails',1)
        IF (InvoiceSubAccounts(job:Account_Number))
            IF (sub:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE sub:InvoiceType
                OF 0 ! Manual invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 !Auto Invoice
                    IF (job:Invoice_Number = '')
                        ! Create Invoice
                        p_web.SSV('CreateDespatchInvoice',1)
                    END
                    
                END
                
                
            END
            
        ELSE
            IF (tra:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE tra:InvoiceType
                OF 0 ! Manual Invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 ! Auto Invoice
                    p_web.SSV('CreateDespatchInvoice',1)
                END
                 
            END
            
        END
        
    END
    

    
ShowConsignmentNumber       ROUTINE
    Access:COURIER.ClearKey(cou:Courier_Key)
    cou:Courier = p_web.GSV('cou:Courier')
    IF (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
        p_web.FileToSessionQueue(COURIER)
    
        IF (p_web.GSV('cou:PrintWaybill') <> 1 AND p_web.GSV('cou:AutoConsignmentNo') <> 1)
            p_web.SSV('Show:ConsignmentNumber',1)
        ELSE
            p_web.SSV('Show:ConsignmentNumber',0)
        END
    END
    
    
        
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('tmp:LoanModelNumber')
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('locMessage')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locSecurityPackID')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('locAccessoryPasswordMessage')
    p_web.DeleteSessionValue('locAuditTrail')
    p_web.DeleteSessionValue('locConsignmentNumber')

    ! Other Variables
    p_web.DeleteSessionValue('UnitValidated')
    p_web.DeleteSessionValue('ValidateButtonText')
    p_web.DeleteSessionValue('AccessoryRefNumber')
    p_web.DeleteSessionValue('DespatchType')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('ValidateAccessories')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('AccessoryPasswordRequired')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('Show:DespatchInvoiceDetails')
    p_web.DeleteSessionValue('CreateDespatchInvoice')

OpenFiles  ROUTINE
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SSV('ValidateButtonText','Validate Unit Details')
  Do ClearVariables
  p_web.SetValue('IndividualDespatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do deletesessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'cou:Courier'
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locConsignmentNumber')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  p_web.SetSessionValue('cou:Courier',cou:Courier)
  p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  End
  if p_web.IfExistsValue('locAccessoryPasswordMessage')
    locAccessoryPasswordMessage = p_web.GetValue('locAccessoryPasswordMessage')
    p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  End
  if p_web.IfExistsValue('cou:Courier')
    cou:Courier = p_web.GetValue('cou:Courier')
    p_web.SetSessionValue('cou:Courier',cou:Courier)
  End
  if p_web.IfExistsValue('locConsignmentNumber')
    locConsignmentNumber = p_web.GetValue('locConsignmentNumber')
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  End
  if p_web.IfExistsValue('locSecurityPackID')
    locSecurityPackID = p_web.GetValue('locSecurityPackID')
    p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('IndividualDespatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
 locAccessoryPasswordMessage = p_web.RestoreValue('locAccessoryPasswordMessage')
 locConsignmentNumber = p_web.RestoreValue('locConsignmentNumber')
 locSecurityPackID = p_web.RestoreValue('locSecurityPackID')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('IndividualDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('IndividualDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('IndividualDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="IndividualDespatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="IndividualDespatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="IndividualDespatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Individual Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Individual Despatch',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_IndividualDespatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_IndividualDespatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_IndividualDespatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_IndividualDespatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_IndividualDespatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_IndividualDespatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateJobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateJobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locErrorMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TagValidateLoanAccessories
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryErrorMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPasswordMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::cou:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::cou:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::cou:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locConsignmentNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locConsignmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locConsignmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSecurityPackID
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSecurityPackID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSecurityPackID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locJobNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locJobNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('UnitValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''individualdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment')

Prompt::locIMEINumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('UnitValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''individualdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment')

Validate::buttonValidateJobDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateJobDetails',p_web.GetValue('NewValue'))
    do Value::buttonValidateJobDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do ValidateUnitDetails
  do Value::buttonValidateJobDetails
  do SendAlert
  do Value::locErrorMessage  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonValidateJobDetails  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateJobDetails'',''individualdespatch_buttonvalidatejobdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateJob',p_web.GSV('ValidateButtonText'),'button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value')

Comment::buttonValidateJobDetails  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_prompt',Choose(p_web.GSV('locErrorMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locErrorMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TagValidateLoanAccessories  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Validate Accessories')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','IndividualDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  
  ! Validate
  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  AccessoryCheck(p_web)
  Case p_web.GSV('AccessoryCheck:Return')
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end            
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')    
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end                
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonFailAccessory  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''individualdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_prompt',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('NewValue'))
    locAccessoryMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('Value'))
    locAccessoryMessage = p_web.GetValue('Value')
  End

Value::locAccessoryMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryMessage') = '')
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value')

Comment::locAccessoryMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_prompt',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('NewValue'))
    locAccessoryErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('Value'))
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End

Value::locAccessoryErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryErrorMessage') = '')
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value')

Comment::locAccessoryErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPassword  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Password Required For Confirmation')
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt')

Validate::locAccessoryPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('NewValue'))
    locAccessoryPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('Value'))
    locAccessoryPassword = p_web.GetValue('Value')
  End
  If locAccessoryPassword = ''
    loc:Invalid = 'locAccessoryPassword'
    loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locAccessoryPassword = Upper(locAccessoryPassword)
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  do Value::locAccessoryPassword
  do SendAlert

Value::locAccessoryPassword  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('AccessoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locAccessoryPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locAccessoryPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''individualdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value')

Comment::locAccessoryPassword  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmMismatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryPasswordMessage','Password Required')    
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryPasswordMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do SendAlert
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonValidateAccessories  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonConfirmMismatch  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''individualdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmMismatch',p_web.GSV('ConfirmMismatchText'),'button-entryfield',loc:formname,,,,loc:javascript,0,'images\tick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value')

Comment::buttonConfirmMismatch  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFailAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFailAccessory',p_web.GetValue('NewValue'))
    do Value::buttonFailAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Fail Accessory Check
  p_web.SSV('GetStatus:StatusNumber',850)
  p_web.SSV('GetStatus:Type',p_web.GSV('DespatchType'))
  GetStatus(p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
      IF (tag:tagged = 1)
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tag:TaggedValue)
      END
  END
  
  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web)
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonValidateJobDetails  !1
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Value::locJobNumber  !1
  do Comment::locJobNumber
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryErrorMessage  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonFailAccessory  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''individualdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FailAccessory','Fail Accessory Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'\images\cross.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value')

Comment::buttonFailAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPasswordMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_prompt',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryPasswordMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPasswordMessage',p_web.GetValue('NewValue'))
    locAccessoryPasswordMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPasswordMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPasswordMessage',p_web.GetValue('Value'))
    locAccessoryPasswordMessage = p_web.GetValue('Value')
  End

Value::locAccessoryPasswordMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryPasswordMessage') = '')
  ! --- DISPLAY --- locAccessoryPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryPasswordMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value')

Comment::locAccessoryPasswordMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_comment',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::cou:Courier  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Courier')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt')

Validate::cou:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('cou:Courier',p_web.GetValue('NewValue'))
    cou:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = cou:Courier
    do Value::cou:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('cou:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    cou:Courier = p_web.GetValue('Value')
  End
  DO ShowConsignmentNumber
  do Value::cou:Courier
  do SendAlert
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::cou:Courier  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('cou:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cou:Courier'',''individualdespatch_cou:courier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('cou:Courier')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('cou:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('cou:Courier') = 0
    p_web.SetSessionValue('cou:Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  p_web._OpenFile(JOBACC)
  bind(jac:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(WAYBILLJ)
  bind(waj:Record)
  p_web._OpenFile(WAYBILLS)
  bind(way:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(MULDESPJ)
  bind(mulj:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(cou:Courier_OptionView)
  cou:Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(cou:Courier_OptionView)
  Loop
    Next(cou:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('cou:Courier') = 0
      p_web.SetSessionValue('cou:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(cou:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value')

Comment::cou:Courier  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locConsignmentNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Consignment Number')
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt')

Validate::locConsignmentNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConsignmentNumber',p_web.GetValue('NewValue'))
    locConsignmentNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConsignmentNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConsignmentNumber',p_web.GetValue('Value'))
    locConsignmentNumber = p_web.GetValue('Value')
  End
    locConsignmentNumber = Upper(locConsignmentNumber)
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  do Value::locConsignmentNumber
  do SendAlert

Value::locConsignmentNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Show:ConsignmentNumber') <> 1)
  ! --- STRING --- locConsignmentNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locConsignmentNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConsignmentNumber'',''individualdespatch_locconsignmentnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locConsignmentNumber',p_web.GetSessionValueFormat('locConsignmentNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value')

Comment::locConsignmentNumber  Routine
      loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_comment',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSecurityPackID  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Security Pack ID')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt')

Validate::locSecurityPackID  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSecurityPackID',p_web.GetValue('NewValue'))
    locSecurityPackID = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSecurityPackID
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSecurityPackID',p_web.GetValue('Value'))
    locSecurityPackID = p_web.GetValue('Value')
  End
  do Value::locSecurityPackID
  do SendAlert

Value::locSecurityPackID  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- STRING --- locSecurityPackID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSecurityPackID')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackID'',''individualdespatch_locsecuritypackid_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackID',p_web.GetSessionValueFormat('locSecurityPackID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value')

Comment::locSecurityPackID  Routine
      loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmDespatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonConfirmDespatch
  do SendAlert

Value::buttonConfirmDespatch  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmDespatch'',''individualdespatch_buttonconfirmdespatch_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmDespatch','Confirm Despatch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchConfirmation')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value')

Comment::buttonConfirmDespatch  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('IndividualDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('IndividualDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('IndividualDespatch_buttonValidateJobDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateJobDetails
      else
        do Value::buttonValidateJobDetails
      end
  of lower('IndividualDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('IndividualDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  of lower('IndividualDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('IndividualDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('IndividualDespatch_cou:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cou:Courier
      else
        do Value::cou:Courier
      end
  of lower('IndividualDespatch_locConsignmentNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConsignmentNumber
      else
        do Value::locConsignmentNumber
      end
  of lower('IndividualDespatch_locSecurityPackID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackID
      else
        do Value::locSecurityPackID
      end
  of lower('IndividualDespatch_buttonConfirmDespatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmDespatch
      else
        do Value::buttonConfirmDespatch
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)

PreCopy  Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 4
    loc:InvalidTab += 1
      If not (p_web.GSV('AccessoryPasswordRequired') <> 1)
        If locAccessoryPassword = ''
          loc:Invalid = 'locAccessoryPassword'
          loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locAccessoryPassword = Upper(locAccessoryPassword)
          p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 5
    loc:InvalidTab += 1
      If not (p_web.GSV('Show:ConsignmentNumber') <> 1)
          locConsignmentNumber = Upper(locConsignmentNumber)
          p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPasswordMessage')
  p_web.StoreValue('cou:Courier')
  p_web.StoreValue('locConsignmentNumber')
  p_web.StoreValue('locSecurityPackID')
  p_web.StoreValue('')
DespatchConfirmation PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyName       STRING(30)                            !
locCourierCost       REAL                                  !
locLabourCost        REAL                                  !
locPartsCost         REAL                                  !
locSubTotal          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locInvoiceNumber     REAL                                  !
locInvoiceDate       DATE                                  !
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locCourier           STRING(30)                            !
FilesOpened     Long
EXCHHIST::State  USHORT
LOANHIST::State  USHORT
EXCHANGE::State  USHORT
LOAN::State  USHORT
JOBSCONS::State  USHORT
COURIER::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
job:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DespatchConfirmation')
  loc:formname = 'DespatchConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DespatchConfirmation','')
    p_web._DivHeader('DespatchConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DespatchConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locCompanyName')
    p_web.DeleteSessionValue('locCourierCost')
    p_web.DeleteSessionValue('locLabourCost')
    p_web.DeleteSessionValue('locPartsCost')
    p_web.DeleteSessionValue('locSubTotal')
    p_web.DeleteSessionValue('locVAT')
    p_web.DeleteSessionValue('locTotal')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locInvoiceDate')
    p_web.DeleteSessionValue('locLabourVatRate')
    p_web.DeleteSessionValue('locPartsVatRate')
    p_web.DeleteSessionValue('locCourier')

    ! Other Variables
    p_web.DeleteSessionValue('DespatchConfirmation:FirstTime')

DespatchProcess     Routine
DATA
locWaybillNumber    STRING(30)
CODE
    p_web.SSV('Hide:PrintDespatchNote',1)
    p_web.SSV('Hide:PrintWaybill',1)
    
    IF (p_web.GSV('cou:PrintWaybill') = 1)
            
        p_web.SSV('Hide:PrintWaybill',0)
            
        locWaybillNumber = NextWaybillNumber()
        IF (locWaybillNumber = 0)
            EXIT
        END
        
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locWaybillNumber
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
            EXIT
        END
        
        IF (p_web.GSV('BookingSite') = 'RRC')

            way:AccountNumber = p_web.GSV('BookingAccount')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                way:WaybillID = 2
                p_web.SSV('wob:JobWaybillNumber',locWaybillNumber)
            OF 'EXC'
                way:WaybillID = 3
                p_web.SSV('wob:ExcWaybillNumber',way:WayBillNumber)
            OF 'LOA'
                way:WaybillID = 4
                p_web.SSV('wob:LoaWaybillNumber',way:WayBillNumber)
            END
            
            IF (p_web.GSV('job:Who_Booked') = 'WEB')
                ! PUP Job
                way:WayBillType = 9
                way:WaybillID = 21
            ELSE
                way:WayBillType = 2
            END
            
            way:FromAccount = p_web.GSV('BookingAccount')
            way:ToAccount = p_web.GSV('job:Account_Number')
            
        ELSE
            way:WayBillType = 1 ! Created From RRC
            way:AccountNumber = p_web.GSV('wob:HeadAccountNumber')
            way:FromAccount = p_web.GSV('ARC:AccountNumber')
            IF (p_web.GSV('jobe:WebJob') = 1)
                way:ToAccount = p_web.GSV('wob:HeadAccountNumber')
                Case p_web.GSV('DespatchType')
                of 'JOB'
                    way:WaybillID = 5 ! ARC to RRC (JOB)
                of 'EXC'
                    way:WaybillID = 6 ! ARC to RRC (EXC)
                END
                
            ELSE
                way:ToAccount = job:Account_Number
                Case p_web.GSV('DespatchType')
                OF 'JOB'
                    way:WaybillID = 7 ! ARC To Customer (JOB)
                of 'EXC'
                    way:WaybillID = 8 ! ARC To Customer (EXC)
                of 'LOA'
                    way:WaybillID = 9 ! ARC To Customer (LOA)
                END
                
            END
            
        END
        
            
        IF (Access:WAYBILLS.TryUpdate())
            EXIT
        END
            
        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
            waj:WayBillNumber = way:WayBillNumber
            waj:JobNumber = p_web.GSV('job:Ref_Number')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                waj:IMEINumber = p_web.GSV('job:ESN')
            OF 'EXC'
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Available_Key) = Level:Benign)
                    waj:IMEINumber = xch:ESN
                END
                    
            OF 'LOA'
                Access:LOAN.ClearKey(loa:Ref_Number_Key)
                loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                    waj:IMEINumber = loa:ESN
                END
                    
            END
            waj:OrderNumber = p_web.GSV('job:Order_Number')
            waj:SecurityPackNumber = p_web.GSV('locSecurityPackID')
            waj:JobType = p_web.GSV('DespatchType')
            IF (Access:WAYBILLJ.TryUpdate() = Level:Benign)
            ELSE
                Access:WAYBILLJ.CancelAutoInc()
            END
        END
            
    ELSE ! IF (p_web.GSV('cou:PrintWaybill') = 1)
        if (p_web.GSV('cou:AutoConsignmentNo') = 1)
            Access:COURIER.Clearkey(cou:Courier_Key)
            cou:Courier = p_web.GSV('cou:Courier')
            if (Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign)
                cou:LastConsignmentNo += 1
                Access:COURIER.TryUpdate()
                locWaybillNumber = cou:LastConsignmentNo
            end
        ELSE
            locWaybillNumber = p_web.GSV('locConsignmentNumber')
        end
        p_web.SSV('Hide:PrintDespatchNote',0)
            
    END ! IF (p_web.GSV('cou:PrintWaybill') = 1)
            
    p_web.SSV('locWaybillNumber',locWaybillNumber)
    
    CASE p_web.GSV('DespatchType')
    OF 'JOB'
        !Despatch:Job(locWayBillNumber)
        Despatch:Job(p_web)
        
    OF 'EXC'
        Despatch:Exc(p_web)                
                
    OF 'LOA'
        Despatch:Loa(p_web)
        
    END

    ! Save Files
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(WEBJOB)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                    
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        IF (Access:JOBSE.TryUpdate() = Level:Benign)
                            
                        END
                    END
                END
            END
        END
    END
    
    
    
    
ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString    CSTRING(30)
CODE
    
    paymentFail# = 0
        
    IF (p_web.GSV('BookingSite') = 'RRC')
        locPaymentString = locPaymentStringRRC
    ELSE
        locPaymentString = locPaymentStringARC
    END
    
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
        IF (p_web.GSV('job:Loan_Unit_Number') = 0)
            IF (p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Chargeable_Job') <> 'YES')
                paymentFail# = 1
            END
        END
    ELSE
        paymentFail# = 1
    END
    IF (paymentFail# = 1)
        p_web.SSV('Hide:PaymentDetails',1)
    ELSE
        p_web.SSV('Hide:PaymentDetails',0)
    END
    
OpenFiles  ROUTINE
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBSCONS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(VATCODE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('DespatchConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO deletesessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('locCourierCost')
    p_web.SetPicture('locCourierCost','@n14.2')
  End
  p_web.SetSessionPicture('locCourierCost','@n14.2')
  If p_web.IfExistsValue('locLabourCost')
    p_web.SetPicture('locLabourCost','@n14.2')
  End
  p_web.SetSessionPicture('locLabourCost','@n14.2')
  If p_web.IfExistsValue('locPartsCost')
    p_web.SetPicture('locPartsCost','@n14.2')
  End
  p_web.SetSessionPicture('locPartsCost','@n14.2')
  If p_web.IfExistsValue('locSubTotal')
    p_web.SetPicture('locSubTotal','@n14.2')
  End
  p_web.SetSessionPicture('locSubTotal','@n14.2')
  If p_web.IfExistsValue('locInvoiceDate')
    p_web.SetPicture('locInvoiceDate','@d06')
  End
  p_web.SetSessionPicture('locInvoiceDate','@d06')
  If p_web.IfExistsValue('locVAT')
    p_web.SetPicture('locVAT','@n14.2')
  End
  p_web.SetSessionPicture('locVAT','@n14.2')
  If p_web.IfExistsValue('locTotal')
    p_web.SetPicture('locTotal','@n14.2')
  End
  p_web.SetSessionPicture('locTotal','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('locCompanyName',locCompanyName)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  p_web.SetSessionValue('locCourierCost',locCourierCost)
  p_web.SetSessionValue('locLabourCost',locLabourCost)
  p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  p_web.SetSessionValue('locPartsCost',locPartsCost)
  p_web.SetSessionValue('locSubTotal',locSubTotal)
  p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  p_web.SetSessionValue('locVAT',locVAT)
  p_web.SetSessionValue('locTotal',locTotal)
  p_web.SetSessionValue('job:Courier',job:Courier)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  End
  if p_web.IfExistsValue('locCourierCost')
    locCourierCost = p_web.dformat(clip(p_web.GetValue('locCourierCost')),'@n14.2')
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  End
  if p_web.IfExistsValue('locLabourCost')
    locLabourCost = p_web.dformat(clip(p_web.GetValue('locLabourCost')),'@n14.2')
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  End
  if p_web.IfExistsValue('locPartsCost')
    locPartsCost = p_web.dformat(clip(p_web.GetValue('locPartsCost')),'@n14.2')
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  End
  if p_web.IfExistsValue('locSubTotal')
    locSubTotal = p_web.dformat(clip(p_web.GetValue('locSubTotal')),'@n14.2')
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  End
  if p_web.IfExistsValue('locInvoiceDate')
    locInvoiceDate = p_web.dformat(clip(p_web.GetValue('locInvoiceDate')),'@d06')
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  End
  if p_web.IfExistsValue('locVAT')
    locVAT = p_web.dformat(clip(p_web.GetValue('locVAT')),'@n14.2')
    p_web.SetSessionValue('locVAT',locVAT)
  End
  if p_web.IfExistsValue('locTotal')
    locTotal = p_web.dformat(clip(p_web.GetValue('locTotal')),'@n14.2')
    p_web.SetSessionValue('locTotal',locTotal)
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('DespatchConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.site.SaveButton.TextValue = 'OK'
  
  IF (p_web.GSV('DespatchConfirmation:FirstTime') = 0)
      p_web.SSV('DespatchConfirmation:FirstTime',1)
      ! Call this code once
      IF (p_web.GSV('Show:DespatchInvoiceDetails') = 1)
         
          IF (p_web.GSV('CreateDespatchInvoice') = 1 )
              CreateTheInvoice(p_web)
          END
      END
      DO DespatchProcess
  
      
  END
  ! Hideous Calculations to work out invoice/costs
  p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DespatchConfirmation')
  p_web.SSV('URL:CreateInvoiceTarget','_self')
  p_web.SSV('URL:CreateInvoiceText','Create Invoice')
  
  isJobInvoiced(p_web)
  IF (p_web.GSV('IsJobInvoiced') = 1)
      p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
      p_web.SSV('URL:CreateInvoiceTarget','_blank')
      p_web.SSV('URL:CreateInvoiceText','Print Invoice')
              
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCLabourCost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCPartsCost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('BookingAccount')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
                  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:RRCInvoiceDate)
      else 
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Invoice_Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
  
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('Default:AccountNumber')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
                  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:ARCInvoiceDate)        
      end
  
  ELSE
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
  
                  
      else 
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Parts_Cost'))
  
      end
      IF (InvoiceSubAccounts(p_web.GSV('job:Account_Number')))
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
                  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
      ELSE
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
                  
                  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
                  
      END
              
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCLabourCost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCPartsCost') * locPartsVatRate/100))                
      ELSE
                  
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Labour_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Parts_Cost') * locPartsVatRate/100))
      END
              
              
  END
  p_web.SSV('locSubTotal',p_web.GSV('locCourierCost') + | 
      p_web.GSV('locLabourCost') + | 
      p_web.GSV('locPartsCost'))        
  p_web.SSV('locTotal',p_web.GSV('locSubTotal') + | 
      p_web.GSV('locVAT'))        
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
  END    
  p_web.SSV('locCompanyName',sub:Company_Name)
  
  
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PAYMENT TYPES'))
      p_web.SSV('Hide:PaymentDetails',1)
  ELSE
      p_web.SSV('Hide:PaymentDetails',0)
  END
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locCourierCost = p_web.RestoreValue('locCourierCost')
 locLabourCost = p_web.RestoreValue('locLabourCost')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locPartsCost = p_web.RestoreValue('locPartsCost')
 locSubTotal = p_web.RestoreValue('locSubTotal')
 locInvoiceDate = p_web.RestoreValue('locInvoiceDate')
 locVAT = p_web.RestoreValue('locVAT')
 locTotal = p_web.RestoreValue('locTotal')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndividualDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DespatchConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DespatchConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('DespatchConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndividualDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="DespatchConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="DespatchConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="DespatchConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Despatch Confirmation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Despatch Confirmation',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_DespatchConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_DespatchConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_DespatchConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Show:DespatchInvoiceDetails') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Invoice Details') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_DespatchConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_DespatchConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Show:DespatchInvoiceDetails') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Courier')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Show:DespatchInvoiceDetails') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_DespatchConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Show:DespatchInvoiceDetails') = 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Invoice Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DespatchConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCompanyName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::aLine
      do Comment::aLine
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textBillingDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textBillingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textBillingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textInvoiceDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textInvoiceDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textInvoiceDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCourierCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCourierCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCourierCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLabourCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLabourCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLabourCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPartsCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPartsCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPartsCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSubTotal
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSubTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSubTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locVAT
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locVAT
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locVAT
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTotal
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonCreateInvoice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateInvoice
      do Comment::buttonCreateInvoice
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPaymentDetails
      do Comment::buttonPaymentDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DespatchConfirmation_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:Account_Number  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End

Value::job:Account_Number  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Account_Number  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCompanyName  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCompanyName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('NewValue'))
    locCompanyName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('Value'))
    locCompanyName = p_web.GetValue('Value')
  End

Value::locCompanyName  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCompanyName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCompanyName  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Repair_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type',p_web.GetValue('NewValue'))
    job:Repair_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type
    do Value::job:Repair_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type = p_web.GetValue('Value')
  End

Value::job:Repair_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Repair_Type  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::aLine  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('aLine',p_web.GetValue('NewValue'))
    do Value::aLine
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::aLine  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::aLine  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textBillingDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Billing Details:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textBillingDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBillingDetails',p_web.GetValue('NewValue'))
    do Value::textBillingDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBillingDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textBillingDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textInvoiceDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice Details:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textInvoiceDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textInvoiceDetails',p_web.GetValue('NewValue'))
    do Value::textInvoiceDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textInvoiceDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textInvoiceDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCourierCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCourierCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCourierCost',p_web.GetValue('NewValue'))
    locCourierCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCourierCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCourierCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locCourierCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locCourierCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locCourierCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locCourierCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCourierCost',p_web.GetSessionValue('locCourierCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCourierCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locLabourCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Labour Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locLabourCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLabourCost',p_web.GetValue('NewValue'))
    locLabourCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLabourCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLabourCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locLabourCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locLabourCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locLabourCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locLabourCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locLabourCost',p_web.GetSessionValue('locLabourCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locLabourCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceNumber  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('NewValue'))
    locInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('Value'))
    locInvoiceNumber = p_web.GetValue('Value')
  End

Value::locInvoiceNumber  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_value',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locInvoiceNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_comment',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPartsCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Parts Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPartsCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPartsCost',p_web.GetValue('NewValue'))
    locPartsCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPartsCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartsCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locPartsCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locPartsCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPartsCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locPartsCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartsCost',p_web.GetSessionValue('locPartsCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locPartsCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSubTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Sub Total')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSubTotal  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSubTotal',p_web.GetValue('NewValue'))
    locSubTotal = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSubTotal
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSubTotal',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locSubTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locSubTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSubTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locSubTotal')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSubTotal',p_web.GetSessionValue('locSubTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locSubTotal  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceDate  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Date')
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceDate',p_web.GetValue('NewValue'))
    locInvoiceDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locInvoiceDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End

Value::locInvoiceDate  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_value',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locInvoiceDate'),'@d06')) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locInvoiceDate  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_comment',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locVAT  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('VAT')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locVAT  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locVAT',p_web.GetValue('NewValue'))
    locVAT = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locVAT
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locVAT',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locVAT = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locVAT  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locVAT
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locVAT')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locVAT',p_web.GetSessionValue('locVAT'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locVAT  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Total')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTotal  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTotal',p_web.GetValue('NewValue'))
    locTotal = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTotal
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTotal',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locTotal')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locTotal',p_web.GetSessionValue('locTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locTotal  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Courier  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Courier',p_web.GetValue('NewValue'))
    job:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Courier
    do Value::job:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Courier = p_web.GetValue('Value')
  End
    job:Courier = Upper(job:Courier)
    p_web.SetSessionValue('job:Courier',job:Courier)
  do Value::job:Courier
  do SendAlert

Value::job:Courier  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''despatchconfirmation_job:courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBSCONS)
  bind(joc:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(VATCODE)
  bind(vat:Record)
  p_web._OpenFile(INVOICE)
  bind(inv:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Courier_OptionView)
  job:Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Courier_OptionView)
  Loop
    Next(job:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Courier') = 0
      p_web.SetSessionValue('job:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value')

Comment::job:Courier  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonCreateInvoice  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonCreateInvoice
  do SendAlert

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateInvoice'',''despatchconfirmation_buttoncreateinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),'DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value')

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPaymentDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPaymentDetails',p_web.GetValue('NewValue'))
    do Value::buttonPaymentDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPaymentDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_value',Choose(p_web.GSV('Hide:PaymentDetails') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PaymentDetails','PaymentDetails','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=DespatchConfirmation')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonPaymentDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_comment',Choose(p_web.GSV('Hide:PaymentDetails') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintWaybill
  do SendAlert

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''despatchconfirmation_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Print Waybill','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value')

Comment::buttonPrintWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_comment',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintDespatchNote
  do SendAlert

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''despatchconfirmation_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintDespatchNote','Print Despatch Note','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchNote?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')

Comment::buttonPrintDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('DespatchConfirmation_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      else
        do Value::job:Courier
      end
  of lower('DespatchConfirmation_buttonCreateInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateInvoice
      else
        do Value::buttonCreateInvoice
      end
  of lower('DespatchConfirmation_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  of lower('DespatchConfirmation_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)

PreCopy  Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO deletesessionvalues
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:InvalidTab += 1
          job:Courier = Upper(job:Courier)
          p_web.SetSessionValue('job:Courier',job:Courier)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Repair_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCourierCost')
  p_web.StoreValue('locLabourCost')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locPartsCost')
  p_web.StoreValue('locSubTotal')
  p_web.StoreValue('locInvoiceDate')
  p_web.StoreValue('locVAT')
  p_web.StoreValue('locTotal')
  p_web.StoreValue('job:Courier')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
ClearJobVariables    PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ClearJobVariables -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('ClearJobVariables')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    ! Clear Insert Job Variables
    p_web.DeleteSessionValue('tmp:ESN')
    p_web.DeleteSessionValue('tmp:TransitType')
    p_web.DeleteSessionValue('tmp:Manufacturer')
    p_web.DeleteSessionValue('tmp:ModelNumber')
    If p_web.gsv('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:ARC = 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:RRC = 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'
    p_web.SSV('tmp:TransitType',GETINI(p_web.GetSessionValue('BookingAccount'), 'InitialTransitType',, CLIP(PATH()) & '\SB2KDEF.INI'))
    p_web.SSV('Comment:TransitType','Required')
    p_web.DeleteSessionValue('Comment:IMEINumber')
    p_web.SSV('Comment:DOP','dd/mm/yyyy')

    p_web.DeleteSessionValue('Filter:Manufacturer')
    p_web.DeleteSessionValue('Filter:ModelNumber')

    p_web.SSV('Hide:IMEINumberButton',1)

    p_web.DeleteSessionValue('OBFValidation')
    p_web.DeleteSessionValue('Prompt:Validation')
    p_web.DeleteSessionValue('Error:Validation')
    p_web.DeleteSessionValue('Passed:Validation')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:OldDOP')
    p_web.SSV('Hide:ChangeDOP',1)
    p_web.SSV('Hide:ChangeDOPButton',1)
    p_web.DeleteSessionValue('tmp:NewDOP')
    p_web.DeleteSessionValue('tmp:ChangeReason')


    p_web.DeleteSessionValue('tmp:DateOfPurchase')

    p_web.DeleteSessionValue('Save:DOP')
    p_web.DeleteSessionValue('Required:DOP')
    p_web.DeleteSessionValue('ReadOnly:DOP')
    p_web.SSV('ReadOnly:Manufacturer',1)
    p_web.SSV('ReadOnly:ModelNumber',1)
    p_web.DeleteSessionValue('ReadOnly:TransitType')
    p_web.DeleteSessionValue('ReadOnly:IMEINumber')
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')

    p_web.DeleteSessionValue('tmp:Network')
    p_web.DeleteSessionValue('tmp:ReturnDate')
    p_web.DeleteSessionValue('tmp:DateOfPurchase')
    p_web.DeleteSessionValue('tmp:BOXIMEINumber')
    p_web.DeleteSessionValue('tmp:BranchOfReturn')
    p_web.DeleteSessionValue('tmp:LAccountNumber')
    p_web.DeleteSessionValue('tmp:OriginalAccessories')
    p_web.DeleteSessionValue('tmp:OriginalDealer')
    p_web.DeleteSessionValue('tmp:OriginalManuals')
    p_web.DeleteSessionValue('tmp:OriginalPackaging')
    p_web.DeleteSessionValue('tmp:PhysicalDamage')
    p_web.DeleteSessionValue('tmp:ProofOfPurchase')
    p_web.DeleteSessionValue('tmp:Replacement')
    p_web.DeleteSessionValue('tmp:ReplacementIMEINumber')
    p_web.DeleteSessionValue('StoreReferenceNumber')
    p_web.DeleteSessionValue('tmp:TalkTime')
    p_web.SSV('Hide:PreviousAddress',1)
    p_web.DeleteSessionValue('tmp:StoreReferenceNumber')

    p_web.SSV('ReadyForNewJobBooking',1)
    p_web.SSV('FirstTime',1)


    p_web.DeleteSessionValue('Required:Engineer')

    p_web.DeleteSessionValue('tmp:ExternalDamageCheckList')
    p_web.DeleteSessionValue('tmp:AmendFaultCodes')

    p_web.DeleteSessionValue('tmp:AmendAddressess')

    p_web.DeleteSessionValue('FranchiseAccount')


    p_web.DeleteSessionValue('tmp:AmendAccessories')
    p_web.DeleteSessionValue('tmp:ShowAccessory')
    p_web.DeleteSessionValue('tmp:TheAccessory')
    p_web.DeleteSessionValue('tmp:TheJobAccessory')
    p_web.DeleteSessionValue('tmp:AmendAddresses')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:POPType')
    p_web.DeleteSessionValue('tmp:PreviousAddress')
    p_web.DeleteSessionValue('tmp:UsePreviousAddress')
    p_web.DeleteSessionValue('AmendFaultCodes')
    p_web.DeleteSessionValue('tmp:FaultCode1')
    p_web.DeleteSessionValue('tmp:FaultCode2')
    p_web.DeleteSessionValue('tmp:FaultCode3')
    p_web.DeleteSessionValue('tmp:FaultCode4')
    p_web.DeleteSessionValue('tmp:FaultCode5')
    p_web.DeleteSessionValue('tmp:FaultCode6')
    p_web.DeleteSessionValue('tmp:ExternalDamageChecklist')
    p_web.DeleteSessionValue('filter:WarrantyChargeTYpe')
    p_web.DeleteSessionValue('Comment:POP')
    p_web.SSV('Drop:JobType','-------------------------------------')
    p_web.DeleteSessionValue('Drop:JobTypeDefault')


    ! --- Clear passed mq variables ----
    ! Inserting:  DBH 30/01/2009 #N/A
    p_web.DeleteSessionValue('mq:ChargeableJob')
    p_web.DeleteSessionValue('mq:Charge_Type')
    p_web.DeleteSessionValue('mq:WarrantyJob')
    p_web.DeleteSessionValue('mq:Warranty_Charge_Type')
    p_web.DeleteSessionValue('mq:DOP')
    p_web.DeleteSessionValue('mq:POP')
    p_web.DeleteSessionValue('mq:IMEIValidation')
    p_web.DeleteSessionValue('mq:IMEIValidationText')
    ! End: DBH 30/01/2009 #N/A
    ! -----------------------------------------

!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
LoginForm            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
Loc:Login            STRING(20)                            !
Loc:Password         STRING(20)                            !
Loc:Remember         LONG                                  !
loc:BranchID         STRING(30)                            !
tmp:LicenseText      STRING(10000)                         !
locNewPassword       STRING(30)                            !
locConfirmNewPassword STRING(30)                           !
FilesOpened     Long
USERS_ALIAS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('LoginForm')
  loc:formname = 'LoginForm_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('LoginForm','')
    p_web._DivHeader('LoginForm',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_LoginForm',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_LoginForm',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS_ALIAS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS_ALIAS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Clear Variables
  BHAddToDebugLog('LoginForm: InitForm')
  p_web.SSV('Hide:EnterNewPassword',1)
  p_web.SSV('loc:Password','')
  p_web.SSV('loc:BranchID','')
  p_web.SSV('BookingName','')
  p_web.SSV('BookingAccount','')
  p_web.SetValue('LoginForm_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  p_web.SetSessionValue('Loc:Password',Loc:Password)
  p_web.SetSessionValue('locNewPassword',locNewPassword)
  p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('loc:BranchID')
    loc:BranchID = p_web.GetValue('loc:BranchID')
    p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  End
  if p_web.IfExistsValue('Loc:Password')
    Loc:Password = p_web.GetValue('Loc:Password')
    p_web.SetSessionValue('Loc:Password',Loc:Password)
  End
  if p_web.IfExistsValue('locNewPassword')
    locNewPassword = p_web.GetValue('locNewPassword')
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  End
  if p_web.IfExistsValue('locConfirmNewPassword')
    locConfirmNewPassword = p_web.GetValue('locConfirmNewPassword')
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('LoginForm_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Login'
  p_web.site.CancelButton.TextValue = 'Reset'
  do RestoreMem
  p_web.SSV('locNewPassword','')
  p_web.SSV('locConfirmNewPassword','')
  IF (p_web.GSV('Hide:EnterNewPassword') = 1)
      p_web.SSV('loc:BranchID','')
      p_web.SSV('loc:Password','')
  
  END
  
  
  ClearJobVariables(p_web)
  
  p_web.SetSessionValue('VersionNumber',glo:VersionNumber)
  
  p_web.SSV('tmp:LicenseText','<FONT FACE="Tahoma" size="3">' & |
      '<b>SOFTWARE LICENCE AGREEMENT</b>' & |
      '<br>' & |
      'Copying, duplicating or otherwise distributing any part of this product without the' & |
      'prior               written consent of an authorised representative of the above is prohibited.' & |
      'This                Software is licensed subject to the following conditions.' & |
      '<br>' & |
      '<b>LICENCE AGREEMENT.</b> This is a legal agreement between you (either an' & |
      'individual          or an entity) and the manufacturer of the computer software PC' & |
      'Control             Systems Ltd..' & |
      'If you operate this SOFTWARE you are agreeing to be bound by the terms of' & |
      'this        agreement. If you do not agree to the terms of this agreement, promptly' & |
      '    return the SOFTWARE and the accompanying items (including written materials' & |
      '    and binders or other containers) to the place you obtained them from.' & |
      'Discretionary       compensation may be paid dependent upon the time elapsed since' & |
      'the                 purchase date of the SOFTWARE in all events any form of compensation will' & |
      'be                  at the sole discretion of and expressly set by PC Control Systems Ltd..' & |
      '<br>' & |
      '<b>GRANT OF LICENCE.</b> This Licence Agreement permits you to use one copy' & |
      'of the enclosed software program ( the "SOFTWARE") on a single computer' & |
      'if you purchased a single user version or on a network of computers if you' & |
      'purchased   a network user version. The SOFTWARE is in "use" on a computer' & |
      'when        it is loaded into temporary memory (i.e. RAM) or installed into permanent' & |
      '    memory (i.e. hard disk, CD-ROM, or other storage device) of that computer.' & |
      'Network             users are limited to the number of simultaneous accesses to the' & |
      'SOFTWARE            by the network. This is dependent upon the user version' & |
      'purchased.          The user version is clearly stated on the original SOFTWARE' & |
      'purchase            invoice.' & |
      '<br>' & |
      '<b>COPYRIGHT:</b> The enclosed SOFTWARE and Documentation is protected by' & |
      'copyright           laws and international treaty provisions and are the proprietary' & |
      'products        of PC Control Systems Ltd. and its third party suppliers from whom' & |
      'PC                  Control Systems Ltd. has licensed portions of the Software. Such suppliers' & |
      'are                 expressly understood to be beneficiaries of the terms and provisions of' & |
      'this                Agreement. All rights that are not expressly granted are reserved by' & |
      'PC                  Control Systems Ltd. or its suppliers. You must treat the SOFTWARE like' & |
      'any                 other copyrighted material (e.g. a book or musical recording) except that' & |
      'you                 may either (a) make one copy of the SOFTWARE solely for backup' & |
      'or archival purposes, or (b) transfer the SOFTWARE to a single hard disk' & |
      'provided            you keep the original solely for backup or archival purposes.' & |
      'You                 may not copy the written materials accompanying the SOFTWARE.' & |
      '<br>' & |
      '<b>DUEL MEDIA SOFTWARE.</b> If the SOFTWARE package contains both 3.5"' & |
      'and 5.25" or 3.5" and CD ROM disks, then you may use only the disks' & |
      'appropriate         for your single-user computer or your file server. You may not use' & |
      'the                 other disks on any other computer or loan, rent, lease, or transfer them to' & |
      'another             user except as part of the permanent transfer (as provided above) of all' & |
      'SOFTWARE            and written materials.' & |
      '<br>' & |
      '<b>LIMITED WARRANTY.</b> PC Control Systems Ltd. warrants that (a) the' & |
      'SOFTWARE            will perform substantially in accordance with the accompanying' & |
      'written             materials for a period of ninety (90) days from the date of receipt,' & |
      'and (b) any hardware accompanying the SOFTWARE will be free of defects in' & |
      'materials           and workmanship under normal use and service for a period of one' & |
      '(1) year from the date of receipt. Any implied warranties on the' & |
      'SOFTWARE            and hardware are limited to ninety (90) days and one' & |
      '(1) year, respectively. Some jurisdictions do not allow limitations' & |
      'duration        of an implied warranty, so the above limitation may not' & |
      'apply               to you.' & |
      '<br>' & |
      '<b>CUSTOMER REMEDIES.</b> PC Control Systems Ltd. and its suppliers entire' & |
      'liability           and your exclusive remedy shall be, at PC Control Systems Ltd.' & |
      'option, either (a) return of the price paid , or (b) repair or replacement of' & |
      'the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which' & |
      'is  returned to PC Control Systems Ltd. with a copy of your SOFTWARE' & |
      'purchase    invoice. This Limited Warranty is void if failure of the SOFTWARE' & |
      'has resulted from accident, abuse, or misapplication. Any replacement' & |
      'SOFTWARE    will be warranted for the remainder of the original warranty period' & |
      '    or thirty (30) days, whichever is the longer.' & |
      '<br>' & |
      '<b>NO OTHER WARRANTIES.</b> To the maximum extent permitted by applicable' & |
      'law, PC Control Systems Ltd. and its suppliers disclaim all other warranties,' & |
      'either              express or implied, including, but not limited to implied warranties of' & |
      'merchantability     and fitness for a particular purpose, with regard to the' & |
      'SOFTWARE, the accompanying written materials, and any accompanying' & |
      'hardware.           This limited warranty gives you specific legal rights, and you may' & |
      'also                have other rights which vary from jurisdiction to jurisdiction.' & |
      '<br>' & |
      '<b>NO LIABILITY FOR CONSEQUENTIAL DAMAGES.</b> To the maximum extent permitted' & |
      'by applicable law, in no event shall PC Control Systems Ltd. or its suppliers' & |
      'be                  liable for any damage whatsoever ( including without limitation,' & |
      'damages             for loss of business profits, business interruption, loss of business' & |
      'information, or any other pecuniary loss) arising out of the use of or' & |
      'inability           to use this product, even if PC Control Systems Ltd. has been advised' & |
      'of the possibility of such damages. Because some jurisdictions do not allow the' & |
      'exclusion           or limitation of liability for consequential or incidental damages, the' & |
      'above               limitation may not apply to you.' & |
      '<br>' & |
      '<b>SUPPORT.</b> PC Control Systems Ltd. will attempt to answer your technical' & |
      'support             requests concerning the SOFTWARE; however, this service is offered' & |
      'on                  a reasonable efforts basis only, and PC Control Systems Ltd. may not be' & |
      'able                to resolve every support request. PC Control Systems Ltd. supports' & |
      'the                 Software only if it is used under conditions and on operating systems' & |
      'for                 which the Software is designed.' & |
      '<br>' & |
      'By Logging in you are agreeing to the above agreement.' & |
      '</FONT>')
  
  !p_web.SetSessionValue('tmp:LicenseText','<body style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);" alink="#000099" link="#000099" vlink="#990099">' & |
  !'<div style="text-align: center;"><small><span style="font-family: Tahoma; font-weight: bold; font-size: 12px;">SOFTWARE LICENCE AGREEMENT' & |
  !'</span><br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;">Copying, duplicating or otherwise ' & |
  !'distributing any part of this product without the</span><br style="font-family: Tahoma;"></small><small>' & |
  !'<span style="font-family: Tahoma;">prior written consent of an authorised representative of the above is ' & |
  !'prohibited.</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'This Software is licensed subject to the following conditions.</span><br style="font-family: Tahoma;">' & |
  !'</small><br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span style=' & |
  !'"font-weight: bold;">LICENCE AGREEMENT.</span> This is a legal agreement between you ' & |
  !'(either an</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'individual or an entity) and the manufacturer of the computer software&nbsp; PC</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">Control Systems Ltd..</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">If you operate this ' & |
  !'SOFTWARE&nbsp; you are agreeing to be bound by the terms of</span><br style="font-family: Tahoma;">' & |
  !'</small><small><span style="font-family: Tahoma;">this agreement. If you do not agree to the ' & |
  !'terms of this agreement, promptly</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">return the SOFTWARE and the accompanying items (including written materials' & |
  !'</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'and binders or other containers) to the place you obtained them from.</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">Discretionary ' & |
  !'compensation may be paid dependent upon the time elapsed since</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">the purchase ' & |
  !'date of the SOFTWARE in all events any form of compensation will</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">be at the ' & |
  !'sole discretion of and expressly set by PC Control Systems Ltd..</span><br style=' & |
  !'"font-family: Tahoma;"></small><br style="font-family: Tahoma;"><small><span style=' & |
  !'"font-family: Tahoma;"><span style="font-weight: bold;">GRANT OF LICENCE.</span>&nbsp; ' & |
  !'This Licence Agreement permits you to use one copy</span><br style="font-family: Tahoma;"' & |
  !'></small><small><span style="font-family: Tahoma;">of the enclosed&nbsp; software ' & |
  !'program ( the "SOFTWARE") on a single computer</span><br style="font-family: Tahoma;">' & |
  !'</small><small><span style="font-family: Tahoma;">if you purchased a single user version or ' & |
  !'on a network of computers if you</span><br style="font-family: Tahoma;"></small><small><span ' & |
  !'style="font-family: Tahoma;">purchased a network user version. The SOFTWARE is in "use" on a ' & |
  !'computer</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">when it is loaded into temporary memory (i.e. RAM) or installed into permanent</span><br ' & |
  !'style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">memory (i.e. hard disk, ' & |
  !'CD-ROM, or other&nbsp; storage device) of that computer.</span><br style="font-family: Tahoma;"></small>' & |
  !'<small><span style="font-family: Tahoma;">Network users are limited to the number of&nbsp; simultaneous ' & |
  !'accesses to the</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'SOFTWARE&nbsp; by the network. This is dependent upon the user version</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">purchased.&nbsp; The user version is clearly ' & |
  !'stated on the original SOFTWARE</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">purchase invoice.</span><br style="font-family: Tahoma;"></small><br style="font-family:' & |
  !' Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;">COPYRIGHT:' & |
  !'</span>&nbsp; The enclosed SOFTWARE and Documentation is protected by</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">copyright laws and international treaty ' & |
  !'provisions and are the proprietary</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">products of PC Control Systems Ltd. and its third party suppliers from ' & |
  !'whom</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'PC Control Systems Ltd. has licensed&nbsp; portions of the Software.&nbsp; Such suppliers</span><br ' & |
  !'style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">are expressly&nbsp; ' & |
  !'understood&nbsp; to be beneficiaries of the terms and provisions of</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">this&nbsp; Agreement.&nbsp; All rights ' & |
  !'that are not expressly&nbsp; granted are reserved by</span><br style="font-family: Tahoma;"></small>' & |
  !'<small><span style="font-family: Tahoma;">PC Control Systems Ltd. or its suppliers. You must treat ' & |
  !'the SOFTWARE like</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">any other copyrighted material (e.g. a book or musical recording) except that</span><br ' & |
  !'style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">you may either (a) ' & |
  !'make one copy of the SOFTWARE solely for backup</span><br style="font-family: Tahoma;"></small>' & |
  !'<small><span style="font-family: Tahoma;">or archival purposes, or (b) transfer the SOFTWARE to a ' & |
  !'single hard disk</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">provided you keep the original solely for backup or archival purposes.</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">You may not copy the ' & |
  !'written materials accompanying the SOFTWARE.</span><br style="font-family: Tahoma;"></small>' & |
  !'<br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-' & |
  !'weight: bold;">DUEL MEDIA SOFTWARE .</span> If the SOFTWARE&nbsp; package contains both 3.5"' & |
  !'</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">and ' & |
  !'5.25" or 3.5" and CD ROM disks, then you may use only the disks</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">appropriate for your single-user ' & |
  !'computer or your file server.&nbsp; You may not use</span><br style="font-family: Tahoma;">' & |
  !'</small><small><span style="font-family: Tahoma;">the other disks on any other computer or ' & |
  !'loan, rent, lease, or transfer them to</span><br style="font-family: Tahoma;"></small><small>' & |
  !'<span style="font-family: Tahoma;">another user except as part of the permanent transfer (as provided above)' & |
  !' of all</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">SOFTWARE and written materials.</span><br style="font-family: Tahoma;"></small><br style=' & |
  !'"font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;"' & |
  !'>LIMITED WARRANTY.</span>&nbsp; PC Control Systems Ltd. warrants that (a) the</span><br sty' & |
  !'le="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">SOFTWARE will perf' & |
  !'orm substantially in accordance with the accompanying</span><br style="font-family: Tahoma;"></sma' & |
  !'ll><small><span style="font-family: Tahoma;">written materials for a period of ninety (90) day' & |
  !'s from the date of receipt,</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">and (b) any hardware accompanying the SOFTWARE will be free of defects ' & |
  !'in</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">ma' & |
  !'terials and workmanship under normal use and service for a period of one</span><br style="font-' & |
  !'family: Tahoma;"></small><small><span style="font-family: Tahoma;">(1) year from the date of rec' & |
  !'eipt. Any implied warranties on the</span><br style="font-family: Tahoma;"></small><small><span ' & |
  !'style="font-family: Tahoma;">SOFTWARE and hardware are limited to ninety (90) days and one</span' & |
  !'><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">(1) year, re' & |
  !'spectively.&nbsp; Some jurisdictions do not allow limitations</span><br style="font-family: Taho' & |
  !'ma;"></small><small><span style="font-family: Tahoma;">duration of an implied warranty, so the a' & |
  !'bove limitation may not</span><br style="font-family: Tahoma;"></small><small><span style="font-' & |
  !'family: Tahoma;">apply to you.</span><br style="font-family: Tahoma;"></small><br style="font-fa' & |
  !'mily: Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;">CUSTOM' & |
  !'ER REMEDIES.</span>&nbsp; PC Control Systems Ltd. and its suppliers entire</span><br style="font' & |
  !'-family: Tahoma;"></small><small><span style="font-family: Tahoma;">liability and your exclusive' & |
  !' remedy shall be, at PC Control Systems Ltd.</span><br style="font-family: Tahoma;"></small><sma' & |
  !'ll><span style="font-family: Tahoma;">option, either (a) return of the price paid , or (b) repai' & |
  !'r or replacement of</span><br style="font-family: Tahoma;"></small><small><span style="font-fami' & |
  !'ly: Tahoma;">the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which</span><b' & |
  !'r style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">is returned to ' & |
  !'PC Control Systems Ltd. with a copy of your SOFTWARE</span><br style="font-family: Tahoma;"></s' & |
  !'mall><small><span style="font-family: Tahoma;">purchase invoice.&nbsp; This Limited Warranty is' & |
  !' void if failure of the SOFTWARE</span><br style="font-family: Tahoma;"></small><small><span st' & |
  !'yle="font-family: Tahoma;">has resulted from accident, abuse, or misapplication.&nbsp; Any repl' & |
  !'acement</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;' & |
  !'">SOFTWARE will be warranted for the remainder of the original warranty period</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">or thirty (30) days, w' & |
  !'hichever is the longer.</span><br style="font-family: Tahoma;"></small><br style="font-family: ' & |
  !'Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;">NO OTHER WA' & |
  !'RRANTIES.&nbsp;</span> To the maximum extent permitted by applicable</span><br style="font-fami' & |
  !'ly: Tahoma;"></small><small><span style="font-family: Tahoma;">law, PC Control Systems Ltd. and' & |
  !' its suppliers disclaim all other warranties,</span><br style="font-family: Tahoma;"></small><s' & |
  !'mall><span style="font-family: Tahoma;">either&nbsp; express or implied, including, but not lim' & |
  !'ited to implied warranties of</span><br style="font-family: Tahoma;"></small><small><span style' & |
  !'="font-family: Tahoma;">merchantability and fitness for a particular purpose, with regard to th' & |
  !'e</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">SOFT' & |
  !'WARE, the accompanying written materials, and any accompanying</span><br style="font-family: Ta' & |
  !'homa;"></small><small><span style="font-family: Tahoma;">hardware.&nbsp; This limited warranty ' & |
  !'gives you specific legal rights, and you may</span><br style="font-family: Tahoma;"></small><sm' & |
  !'all><span style="font-family: Tahoma;">also have other rights which vary from jurisdiction to j' & |
  !'urisdiction.</span><br style="font-family: Tahoma;"></small><br style="font-family: Tahoma;"><s' & |
  !'mall><span style="font-family: Tahoma;"><span style="font-weight: bold;">NO LIABILITY FOR CONSE' & |
  !'QUENTIAL DAMAGES.</span> To the maximum extent permitted</span><br style="font-family: Tahoma;"' & |
  !'></small><small><span style="font-family: Tahoma;">by applicable law, in no event shall PC Cont' & |
  !'rol Systems Ltd.&nbsp; or its suppliers</span><br style="font-family: Tahoma;"></small><small><' & |
  !'span style="font-family: Tahoma;">be liable for any damage&nbsp; whatsoever ( including without' & |
  !'&nbsp; limitation,</span><br style="font-family: Tahoma;"></small><small><span style="font-fami' & |
  !'ly: Tahoma;">damages for loss of business profits, business interruption, loss of business</spa' & |
  !'n><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">informatio' & |
  !'n, or any other pecuniary loss)&nbsp; arising&nbsp; out of the use of or</span><br style="font-' & |
  !'family: Tahoma;"></small><small><span style="font-family: Tahoma;">inability to use this produc' & |
  !'t,&nbsp; even if PC Control Systems Ltd. has been advised</span><br style="font-family: Tahoma;' & |
  !'"></small><small><span style="font-family: Tahoma;">of the possibility&nbsp; of such damages.&n' & |
  !'bsp; Because some jurisdictions do not allow the</span><br style="font-family: Tahoma;"></small' & |
  !'><small><span style="font-family: Tahoma;">exclusion or limitation of liability for consequenti' & |
  !'al or incidental damages, the</span><br style="font-family: Tahoma;"></small><small><span style' & |
  !'="font-family: Tahoma;">above limitation may not apply to you.</span><br style="font-family: Ta' & |
  !'homa;"></small><br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span' & |
  !' style="font-weight: bold;">SUPPORT.</span>&nbsp;&nbsp; PC Control Systems Ltd.&nbsp; will atte' & |
  !'mpt to answer&nbsp; your technical</span><br style="font-family: Tahoma;"></small><small><span ' & |
  !'style="font-family: Tahoma;">support requests concerning the SOFTWARE; however, this service is' & |
  !' offered</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma' & |
  !';">on a reasonable efforts basis only, and PC Control Systems Ltd. may not be</span><br style="' & |
  !'font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">able to resolve every s' & |
  !'upport request.&nbsp; PC Control Systems Ltd. supports</span><br style="font-family: Tahoma;"><' & |
  !'/small><small><span style="font-family: Tahoma;">the Software only if it is used under conditio' & |
  !'ns and on operating systems</span><br style="font-family: Tahoma;"></small><small><span style="' & |
  !'font-family: Tahoma;">for which the Software is designed.<br><br>By Logging in you are agreeing' & |
  !' to the above agreement.</span></small></small></div></body>')
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 loc:BranchID = p_web.RestoreValue('loc:BranchID')
 Loc:Password = p_web.RestoreValue('Loc:Password')
 locNewPassword = p_web.RestoreValue('locNewPassword')
 locConfirmNewPassword = p_web.RestoreValue('locConfirmNewPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('LoginForm_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('LoginForm_ChainTo')
    loc:formaction = p_web.GetSessionValue('LoginForm_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'LoginForm'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  loc:formaction = 'IndexPage'
  loc:formactiontarget = '_top'
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="LoginForm" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="LoginForm" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="LoginForm" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Login') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Login',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_LoginForm">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_LoginForm" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_LoginForm')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Licence Agreement') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ServiceBase Online Login') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_LoginForm')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_LoginForm'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_LoginForm')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Licence Agreement') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_LoginForm_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Licence Agreement')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Licence Agreement')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Licence Agreement')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Licence Agreement')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentreSmall')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::licence
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('ServiceBase Online Login') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_LoginForm_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentreSmall')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'10%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc:BranchID
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc:BranchID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'10%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Loc:Password
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Loc:Password
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:EnterNewPassword') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'10%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:EnterNewPassword') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'10%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locConfirmNewPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locConfirmNewPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::licence  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('licence',p_web.GetValue('NewValue'))
    do Value::licence
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::licence  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('licence') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('<iframe id="waitframe" src="la.htm" style="border: 0 solid #8888ff; height: 300px; width: 780px; margin-left: auto;margin-right: auto"></iframe>',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc:BranchID  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('loc:BranchID') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Branch ID')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc:BranchID  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc:BranchID',p_web.GetValue('NewValue'))
    loc:BranchID = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc:BranchID
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc:BranchID',p_web.GetValue('Value'))
    loc:BranchID = p_web.GetValue('Value')
  End
  If loc:BranchID = ''
    loc:Invalid = 'loc:BranchID'
    loc:alert = p_web.translate('Branch ID') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    loc:BranchID = Upper(loc:BranchID)
    p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  p_web.SSV('LoginMessage','')
  do Value::loc:BranchID
  do SendAlert

Value::loc:BranchID  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('loc:BranchID') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- loc:BranchID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:EnterNewPassword') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:EnterNewPassword') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('loc:BranchID')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If loc:BranchID = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''loc:BranchID'',''loginform_loc:branchid_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(loc:Password)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','loc:BranchID',p_web.GetSessionValueFormat('loc:BranchID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('LoginForm_' & p_web._nocolon('loc:BranchID') & '_value')


Prompt::Loc:Password  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('Loc:Password') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Loc:Password  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Loc:Password',p_web.GetValue('NewValue'))
    Loc:Password = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::Loc:Password
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('Loc:Password',p_web.GetValue('Value'))
    Loc:Password = p_web.GetValue('Value')
  End
  If Loc:Password = ''
    loc:Invalid = 'Loc:Password'
    loc:alert = p_web.translate('Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    Loc:Password = Upper(Loc:Password)
    p_web.SetSessionValue('Loc:Password',Loc:Password)
  p_web.SSV('LoginMessage','')
  do Value::Loc:Password
  do SendAlert

Value::Loc:Password  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('Loc:Password') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- Loc:Password
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:EnterNewPassword') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:EnterNewPassword') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('Loc:Password')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If Loc:Password = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''Loc:Password'',''loginform_loc:password_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','Loc:Password',p_web.GetSessionValueFormat('Loc:Password'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('LoginForm_' & p_web._nocolon('Loc:Password') & '_value')


Prompt::locNewPassword  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('locNewPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewPassword',p_web.GetValue('NewValue'))
    locNewPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewPassword',p_web.GetValue('Value'))
    locNewPassword = p_web.GetValue('Value')
  End
  If locNewPassword = ''
    loc:Invalid = 'locNewPassword'
    loc:alert = p_web.translate('New Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNewPassword = Upper(locNewPassword)
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  do Value::locNewPassword
  do SendAlert

Value::locNewPassword  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('locNewPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewPassword'',''loginform_locnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locNewPassword',p_web.GetSessionValueFormat('locNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('LoginForm_' & p_web._nocolon('locNewPassword') & '_value')


Prompt::locConfirmNewPassword  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('locConfirmNewPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm New Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locConfirmNewPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConfirmNewPassword',p_web.GetValue('NewValue'))
    locConfirmNewPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConfirmNewPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConfirmNewPassword',p_web.GetValue('Value'))
    locConfirmNewPassword = p_web.GetValue('Value')
  End
  If locConfirmNewPassword = ''
    loc:Invalid = 'locConfirmNewPassword'
    loc:alert = p_web.translate('Confirm New Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locConfirmNewPassword = Upper(locConfirmNewPassword)
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  do Value::locConfirmNewPassword
  do SendAlert

Value::locConfirmNewPassword  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('locConfirmNewPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locConfirmNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locConfirmNewPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locConfirmNewPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConfirmNewPassword'',''loginform_locconfirmnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locConfirmNewPassword',p_web.GetSessionValueFormat('locConfirmNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('LoginForm_' & p_web._nocolon('locConfirmNewPassword') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('LoginForm_loc:BranchID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::loc:BranchID
      else
        do Value::loc:BranchID
      end
  of lower('LoginForm_Loc:Password_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Loc:Password
      else
        do Value::Loc:Password
      end
  of lower('LoginForm_locNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewPassword
      else
        do Value::locNewPassword
      end
  of lower('LoginForm_locConfirmNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConfirmNewPassword
      else
        do Value::locConfirmNewPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_LoginForm',0)

PreCopy  Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_LoginForm',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('LoginForm:Primed',0)

PreDelete       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('LoginForm:Primed',0)
  p_web.setsessionvalue('showtab_LoginForm',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  ! Validation
  BHAddToDebugLog('LoginForm: ValidateUpdate')
  
  IF (p_web.GSV('Hide:EnterNewPassword') = 0)
      ! Asking for a new passowrd
      IF (p_web.GSV('locNewPassword') <> p_web.GSV('locConfirmNewPassword'))
          loc:Invalid = 'locConfirmNewPassword'
          loc:Alert = 'Passwords Do Not Match'
          EXIT
      END
      IF (p_web.GSV('locNewPassword') = p_web.GSV('loc:Password'))
          loc:Invalid = 'locNewPassword'
          loc:Alert = 'You must enter a NEW password'
          EXIT
      END
  END
  
  
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = Upper(loc:Password)
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
      If (use:Active <> 'YES')
          loc:Invalid = loc:Password
          loc:alert = 'Entered password is no longer active'
          p_web.SSV('loc:Password','')
          Exit
      ELSE
          Access:TRADEACC.ClearKey(tra:SiteLocationKey)
          tra:SiteLocation = use:Location
          If Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign
              !Found
              IF (tra:Account_Number <> loc:BranchID)
                  loc:Invalid = 'loc:BranchID'
                  loc:Alert = 'Branch ID And Password Do Not Match'
                  p_web.SSV('loc:Password','')
                  Exit
              ELSE
                  
                  IF (p_web.GSV('Hide:EnterNewPassword') = 0)
                      ! Entering A New Password
                      Access:USERS_ALIAS.Clearkey(use_ali:Password_Key)
                      use_ali:Password = p_web.GSV('locNewPassword')
                      IF (Access:USERS_ALIAS.Tryfetch(use_ali:Password_Key) = Level:Benign)
                          loc:Invalid = 'locNewPassword'
                          loc:Alert = 'The selected password is already in use. Enter another'
                          EXIT
                      END
                      IF (p_web.GSV('locNewPassword') = '' OR p_web.GSV('locConfirmNewPassword') = '')
                          ! Something has gone wrong
                          EXIT
                      END
                      use:Password    = p_web.GSV('locNewPassword')
                      use:PasswordLastChanged = Today()
                      If Access:USERS.Update() = Level:Benign
                      END
                      
                  ELSE
                      
                      IF (use:RenewPassword > 0)
                          IF (use:RenewPassword = '')
                              use:RenewPassword = TODAY()
                              Access:USERS.TryUpdate()
                          ELSE
                              If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today()
                                  !p_web.SetValue('retry','LoginForm')
                                  !p_web.SSV('LoginMessage','The password has expired. Enter a new one')
                                  p_web.SSV('Hide:EnterNewPassword',0)
                                  loc:Invalid = 'loc:Password'
                                  loc:alert = 'The password has expired. Enter a new one'
                                  exit
                              END
                          END
                      END
                  END ! IF (p_web.GSV('Hide:EnterNewPassword') = 0)
                  
                  p_web.ValidateLogin()
                  p_web.SSV('BookingBranchID',tra:BranchIdentification)
                  p_web.SSV('BookingAccount',Clip(tra:Account_Number))
                  p_web.SSV('BookingName',Clip(DoCaps(tra:Company_Name)))
                  p_web.SSV('BookingUserPassword',use:Password)
                  p_web.SSV('BookingUserCode',use:User_Code)
                  p_web.SSV('BookingSiteLocation',tra:SiteLocation)
                  p_web.SSV('BookingRestrictParts',use:RestrictParts)
                  p_web.SSV('BookingRestrictChargeable',use:RestrictChargeable)
                  p_web.SSV('BookingSkillLevel',use:SkillLevel)
                  If tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                      p_web.SSV('BookingSite','ARC')
                      !p_web.SSV('Filter:BrowseJobs','Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                      p_web.SSV('Filter:BrowseJobs','')
  
                  Else ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
                      p_web.SSV('BookingSite','RRC')
                      !p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)  And Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                      p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)')
                  End ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
                  ! Set Some Defaults
                  p_web.SSV('Default:InTransitPUP',GETINI('RRC','InTransitToPUPLocation',,Path() & '\SB2KDEF.INI'))
                  p_web.SSV('Default:PUPLocation',GETINI('RRC','AtPUPLocation',,Path() & '\SB2KDEF.INI'))
                  p_web.SSV('Default:ARCLocation',GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:StatusSendToRRC',GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:DespatchToCustomer',GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:InTransitARC',GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:RRCLocation',GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:StatusSendToARC',GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:InTransitRRC',GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
                  p_web.SSV('Default:StatusReceivedFromPUP',GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI'))
                  p_web.SSV('Default:AccountNumber',tra:Account_Number)
                  p_web.SSV('Default:SiteLocation',tra:SiteLocation)
                  
                  p_web.SSV('Default:TermsText',GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')) ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
                  p_web.DeleteSessionValue('loc:Password')
                  glo:Password = use:Password
  
                  p_web.SSV('ARC:AccountNumber',GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))
  
                  p_web.SSV('LoginDetails1','Site: ' & clip(tra:Company_Name))
                  p_web.SSV('LoginDetails2','User: ' & clip(use:forename) & ' ' & clip(use:Surname))
  
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number    = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                  if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                      ! Found
                      p_web.SSV('ARC:SiteLocation',tra:SiteLocation)
                  else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
          
                  IF (SecurityCheckFailed(use:Password,'JOBS - INSERT')) ! #11682 Check access. (Bryan: 07/09/2010)
                      p_web.SSV('Hide:ButtonCreateNewJob',1)
                  ELSE
                      p_web.SSV('Hide:ButtonCreateNewJob',0)
                  END
          
                  IF (SecurityCheckFailed(use:Password,'JOBS - CHANGE'))
                      p_web.SSV('Hide:ButtonJobSearch',1)
                  ELSE
                      p_web.SSV('Hide:ButtonJobSearch',0)
                  END
              END
          Else ! If Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign
              !Error
              loc:Invalid = 'loc:Password'
              loc:Alert = 'Incorrect Password'
              Exit
          End ! If Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign
      END
      
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
      loc:Invalid = 'loc:Password'
      loc:Alert = 'Incorrect Password'
      Exit
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  
  

ValidateDelete  Routine
  p_web.DeleteSessionValue('LoginForm_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('LoginForm_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
        If loc:BranchID = ''
          loc:Invalid = 'loc:BranchID'
          loc:alert = p_web.translate('Branch ID') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          loc:BranchID = Upper(loc:BranchID)
          p_web.SetSessionValue('loc:BranchID',loc:BranchID)
        If loc:Invalid <> '' then exit.
        If Loc:Password = ''
          loc:Invalid = 'Loc:Password'
          loc:alert = p_web.translate('Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          Loc:Password = Upper(Loc:Password)
          p_web.SetSessionValue('Loc:Password',Loc:Password)
        If loc:Invalid <> '' then exit.
    If p_web.GSV('Hide:EnterNewPassword') = 0
        If locNewPassword = ''
          loc:Invalid = 'locNewPassword'
          loc:alert = p_web.translate('New Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNewPassword = Upper(locNewPassword)
          p_web.SetSessionValue('locNewPassword',locNewPassword)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:EnterNewPassword') = 0
        If locConfirmNewPassword = ''
          loc:Invalid = 'locConfirmNewPassword'
          loc:alert = p_web.translate('Confirm New Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locConfirmNewPassword = Upper(locConfirmNewPassword)
          p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
        If loc:Invalid <> '' then exit.
    End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('LoginForm:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('loc:BranchID')
  p_web.StoreValue('Loc:Password')
  p_web.StoreValue('locNewPassword')
  p_web.StoreValue('locConfirmNewPassword')
DoCaps               PROCEDURE  (f_String)                 ! Declare Procedure
  CODE
    f_string = CLIP(LEFT(f_string))

    STR_LEN#  = LEN(f_string)


    STR_POS#  = 1

    f_string = UPPER(SUB(f_string,STR_POS#,1)) & SUB(f_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(f_string,STR_POS#,1) = ' ' OR SUB(f_string,STR_POS#,1) = '-'  |
        OR SUB(f_string,STR_POS#,1) = '.' OR SUB(f_string,STR_POS#,1) = '/' |
        OR SUB(f_string,STR_POS#,1) = '&' OR SUB(f_string,STR_POS#,1) = '(' OR SUB(f_string,STR_POS#,1) = CHR(39)
        f_string = SUB(f_string,1,STR_POS#) & UPPER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)
     ELSE
        f_string = SUB(f_string,1,STR_POS#) & LOWER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)

     .
    .

    RETURN(f_string)

BannerBlank          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('BannerBlank')
  loc:formname = 'BannerBlank_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerBlank','')
    p_web._DivHeader('BannerBlank',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerBlank',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerBlank',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerBlank',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerBlank',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerBlank',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerBlank',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerBlank_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BannerBlank_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerBlank')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerBlank_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerBlank_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerBlank_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BannerBlank" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BannerBlank" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BannerBlank" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BannerBlank">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BannerBlank">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BannerBlank')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate(' ') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BannerBlank')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BannerBlank'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BannerBlank')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate(' ') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BannerBlank_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(' ')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate(' ')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(' ')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(' ')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('bannerHeading')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::banner
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::banner  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('banner',p_web.GetValue('NewValue'))
    do Value::banner
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::banner  Routine
  p_web._DivHeader('BannerBlank_' & p_web._nocolon('banner') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('<img src="/images/topbanner.gif" width="800" height="40"/>',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BannerBlank_form:ready_',1)
  p_web.SetSessionValue('BannerBlank_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BannerBlank',0)

PreCopy  Routine
  p_web.SetValue('BannerBlank_form:ready_',1)
  p_web.SetSessionValue('BannerBlank_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerBlank',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BannerBlank_form:ready_',1)
  p_web.SetSessionValue('BannerBlank_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BannerBlank:Primed',0)

PreDelete       Routine
  p_web.SetValue('BannerBlank_form:ready_',1)
  p_web.SetSessionValue('BannerBlank_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BannerBlank:Primed',0)
  p_web.setsessionvalue('showtab_BannerBlank',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerBlank_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerBlank_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BannerBlank:Primed',0)
  p_web.StoreValue('')
WebHandler           PROCEDURE (String p_ReqString)

loc:file                              &FILE
!Local Data Classes
p_web                CLASS(NetWebServerWorker)             ! Generated by NetTalk Extension (Class Definition)
CallForm               PROCEDURE(FILE p_file,LONG p_Stage),LONG,DERIVED
ProcessLink            PROCEDURE(<string p_action>),DERIVED
ProcessTag             PROCEDURE(string p_TagString),DERIVED
SetPics                PROCEDURE(FILE p_file),DERIVED
SetSessionPics         PROCEDURE(FILE p_file),DERIVED
_AddFile               PROCEDURE(FILE p_file),LONG,PROC,DERIVED
_CloseFile             PROCEDURE(FILE p_file),LONG,PROC,DERIVED
_DeleteFile            PROCEDURE(FILE p_file),LONG,PROC,DERIVED
_GetFile               PROCEDURE(FILE p_file,KEY p_Key),LONG,PROC,DERIVED
_NetWebFileNamed       PROCEDURE(STRING p_Label),*FILE,DERIVED
_OpenFile              PROCEDURE(FILE p_file),LONG,PROC,DERIVED
_SendFile              PROCEDURE(string p_FileName,Long p_header=NET:SendHeader),DERIVED
_UpdateFile            PROCEDURE(FILE p_file),LONG,PROC,DERIVED

                     END


  CODE
  GlobalErrors.SetProcedureName('WebHandler')
  p_web.ProcessRequest(p_ReqString)
  GlobalErrors.SetProcedureName()

p_web.CallForm PROCEDURE(FILE p_file,LONG p_Stage)

ReturnValue          LONG,AUTO


  CODE
    If Band(p_Stage, NET:WEB:StagePost + NET:WEB:StageValidate + NET:WEB:Cancel)
      case lower(SELF.GetValue('FromForm'))
      Of 'finishbatch'
         ReturnValue = FinishBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'createcreditnote'
         ReturnValue = CreateCreditNote(Self,p_stage)
         RETURN ReturnValue
      Of 'individualdespatch'
         ReturnValue = IndividualDespatch(Self,p_stage)
         RETURN ReturnValue
      Of 'jobestimate'
         ReturnValue = JobEstimate(Self,p_stage)
         RETURN ReturnValue
      Of 'prenewjobbooking'
         ReturnValue = PreNewJobBooking(Self,p_stage)
         RETURN ReturnValue
      Of 'formcontacthistory'
         ReturnValue = FormContactHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'formbrowselocationhistory'
         ReturnValue = FormBrowseLocationHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'loginform'
         ReturnValue = LoginForm(Self,p_stage)
         RETURN ReturnValue
      Of 'formbrowsecontacthistory'
         ReturnValue = FormBrowseContactHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'multiplebatchdespatch'
         ReturnValue = MultipleBatchDespatch(Self,p_stage)
         RETURN ReturnValue
      Of 'indexpage'
         ReturnValue = IndexPage(Self,p_stage)
         RETURN ReturnValue
      Of 'viewjob'
         ReturnValue = ViewJob(Self,p_stage)
         RETURN ReturnValue
      Of 'jobaccessories'
         ReturnValue = JobAccessories(Self,p_stage)
         RETURN ReturnValue
      Of 'sendsms'
         ReturnValue = SendSMS(Self,p_stage)
         RETURN ReturnValue
      Of 'displayoutfaults'
         ReturnValue = DisplayOutFaults(Self,p_stage)
         RETURN ReturnValue
      Of 'pickengineersnotes'
         ReturnValue = PickEngineersNotes(Self,p_stage)
         RETURN ReturnValue
      Of 'formexchangeunitfilter'
         ReturnValue = FormExchangeUnitFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'formdeletepart'
         ReturnValue = FormDeletePart(Self,p_stage)
         RETURN ReturnValue
      Of 'proofofpurchase'
         ReturnValue = ProofOfPurchase(Self,p_stage)
         RETURN ReturnValue
      Of 'createnewbatch'
         ReturnValue = CreateNewBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'formwarrantyparts'
         ReturnValue = FormWarrantyParts(Self,p_stage)
         RETURN ReturnValue
      Of 'pickpaperwork'
         ReturnValue = PickPaperwork(Self,p_stage)
         RETURN ReturnValue
      Of 'bannernewjobbooking'
         ReturnValue = BannerNewJobBooking(Self,p_stage)
         RETURN ReturnValue
      Of 'addtobatch'
         ReturnValue = AddToBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'formrepairnotes'
         ReturnValue = FormRepairNotes(Self,p_stage)
         RETURN ReturnValue
      Of 'jobfaultcodes'
         ReturnValue = JobFaultCodes(Self,p_stage)
         RETURN ReturnValue
      Of 'amendaddress'
         ReturnValue = AmendAddress(Self,p_stage)
         RETURN ReturnValue
      Of 'browseauditfilter'
         ReturnValue = BrowseAuditFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'pickloanunit'
         ReturnValue = PickLoanUnit(Self,p_stage)
         RETURN ReturnValue
      Of 'jobsearch'
         ReturnValue = JobSearch(Self,p_stage)
         RETURN ReturnValue
      Of 'clearaddressdetails'
         ReturnValue = ClearAddressDetails(Self,p_stage)
         RETURN ReturnValue
      Of 'obfvalidation'
         ReturnValue = OBFValidation(Self,p_stage)
         RETURN ReturnValue
      Of 'sendanemail'
         ReturnValue = SendAnEmail(Self,p_stage)
         RETURN ReturnValue
      Of 'formpayments'
         ReturnValue = FormPayments(Self,p_stage)
         RETURN ReturnValue
      Of 'formaddtobatch'
         ReturnValue = FormAddToBatch(Self,p_stage)
         RETURN ReturnValue
      Of 'formpickmodelstocklocation'
         ReturnValue = FormPickModelStockLocation(Self,p_stage)
         RETURN ReturnValue
      Of 'insertjob_finished'
         ReturnValue = InsertJob_Finished(Self,p_stage)
         RETURN ReturnValue
      Of 'viewcosts'
         ReturnValue = ViewCosts(Self,p_stage)
         RETURN ReturnValue
      Of 'bannerblank'
         ReturnValue = BannerBlank(Self,p_stage)
         RETURN ReturnValue
      Of 'pickexchangeunit'
         ReturnValue = PickExchangeUnit(Self,p_stage)
         RETURN ReturnValue
      Of 'bannermainmenu'
         ReturnValue = BannerMainMenu(Self,p_stage)
         RETURN ReturnValue
      Of 'pagefooter'
         ReturnValue = PageFooter(Self,p_stage)
         RETURN ReturnValue
      Of 'pickaccessory'
         ReturnValue = PickAccessory(Self,p_stage)
         RETURN ReturnValue
      Of 'formbrowseengineerhistory'
         ReturnValue = FormBrowseEngineerHistory(Self,p_stage)
         RETURN ReturnValue
      Of 'invoicecreated'
         ReturnValue = InvoiceCreated(Self,p_stage)
         RETURN ReturnValue
      Of 'viewbouncerjob'
         ReturnValue = ViewBouncerJob(Self,p_stage)
         RETURN ReturnValue
      Of 'printroutines'
         ReturnValue = PrintRoutines(Self,p_stage)
         RETURN ReturnValue
      Of 'bannerengineeringdetails'
         ReturnValue = BannerEngineeringDetails(Self,p_stage)
         RETURN ReturnValue
      Of 'jobestimatequery'
         ReturnValue = JobEstimateQuery(Self,p_stage)
         RETURN ReturnValue
      Of 'newjobbooking'
         ReturnValue = NewJobBooking(Self,p_stage)
         RETURN ReturnValue
      Of 'browsestatuschangesfilter'
         ReturnValue = BrowseStatusChangesFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'setjobtype'
         ReturnValue = SetJobType(Self,p_stage)
         RETURN ReturnValue
      Of 'bannerbrowsejobs'
         ReturnValue = BannerBrowseJobs(Self,p_stage)
         RETURN ReturnValue
      Of 'createinvoice'
         ReturnValue = CreateInvoice(Self,p_stage)
         RETURN ReturnValue
      Of 'despatchconfirmation'
         ReturnValue = DespatchConfirmation(Self,p_stage)
         RETURN ReturnValue
      Of 'receiptfrompup'
         ReturnValue = ReceiptFromPUP(Self,p_stage)
         RETURN ReturnValue
      Of 'formchargeableparts'
         ReturnValue = FormChargeableParts(Self,p_stage)
         RETURN ReturnValue
      Of 'formaccessorynumbers'
         ReturnValue = FormAccessoryNumbers(Self,p_stage)
         RETURN ReturnValue
      Of 'bannervodacom'
         ReturnValue = BannerVodacom(Self,p_stage)
         RETURN ReturnValue
      Of 'formengineeringoption'
         ReturnValue = FormEngineeringOption(Self,p_stage)
         RETURN ReturnValue
      Of 'allocateengineer'
         ReturnValue = AllocateEngineer(Self,p_stage)
         RETURN ReturnValue
      Of 'formestimateparts'
         ReturnValue = FormEstimateParts(Self,p_stage)
         RETURN ReturnValue
      Of 'formfiltermodelnumber'
         ReturnValue = formFilterModelNumber(Self,p_stage)
         RETURN ReturnValue
      Of 'displaybrowsepayments'
         ReturnValue = DisplayBrowsePayments(Self,p_stage)
         RETURN ReturnValue
      Of 'formjoboutfaults'
         ReturnValue = FormJobOutFaults(Self,p_stage)
         RETURN ReturnValue
      Of 'formchangedop'
         ReturnValue = FormChangeDOP(Self,p_stage)
         RETURN ReturnValue
      Of 'browsestatusfilter'
         ReturnValue = BrowseStatusFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'billingconfirmation'
         ReturnValue = BillingConfirmation(Self,p_stage)
         RETURN ReturnValue
      Of 'formloanunitfilter'
         ReturnValue = FormLoanUnitFilter(Self,p_stage)
         RETURN ReturnValue
      Of 'pickcontacthistorynotes'
         ReturnValue = PickContactHistoryNotes(Self,p_stage)
         RETURN ReturnValue
      Of 'formjobsinbatch'
         ReturnValue = FormJobsInBatch(Self,p_stage)
         RETURN ReturnValue
      End
    Else
      case lower(SELF.PageName)
        Of 'finishbatch'
             ReturnValue = FinishBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'createcreditnote'
             ReturnValue = CreateCreditNote(Self,p_stage)
           RETURN ReturnValue
        Of 'individualdespatch'
             ReturnValue = IndividualDespatch(Self,p_stage)
           RETURN ReturnValue
        Of 'jobestimate'
             ReturnValue = JobEstimate(Self,p_stage)
           RETURN ReturnValue
        Of 'prenewjobbooking'
             ReturnValue = PreNewJobBooking(Self,p_stage)
           RETURN ReturnValue
        Of 'formcontacthistory'
             ReturnValue = FormContactHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'formbrowselocationhistory'
             ReturnValue = FormBrowseLocationHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'loginform'
             ReturnValue = LoginForm(Self,p_stage)
           RETURN ReturnValue
        Of 'formbrowsecontacthistory'
             ReturnValue = FormBrowseContactHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'multiplebatchdespatch'
             ReturnValue = MultipleBatchDespatch(Self,p_stage)
           RETURN ReturnValue
        Of 'indexpage'
             ReturnValue = IndexPage(Self,p_stage)
           RETURN ReturnValue
        Of 'viewjob'
             ReturnValue = ViewJob(Self,p_stage)
           RETURN ReturnValue
        Of 'jobaccessories'
             ReturnValue = JobAccessories(Self,p_stage)
           RETURN ReturnValue
        Of 'sendsms'
             ReturnValue = SendSMS(Self,p_stage)
           RETURN ReturnValue
        Of 'displayoutfaults'
             ReturnValue = DisplayOutFaults(Self,p_stage)
           RETURN ReturnValue
        Of 'pickengineersnotes'
             ReturnValue = PickEngineersNotes(Self,p_stage)
           RETURN ReturnValue
        Of 'formexchangeunitfilter'
             ReturnValue = FormExchangeUnitFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'formdeletepart'
             ReturnValue = FormDeletePart(Self,p_stage)
           RETURN ReturnValue
        Of 'proofofpurchase'
             ReturnValue = ProofOfPurchase(Self,p_stage)
           RETURN ReturnValue
        Of 'createnewbatch'
             ReturnValue = CreateNewBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'formwarrantyparts'
             ReturnValue = FormWarrantyParts(Self,p_stage)
           RETURN ReturnValue
        Of 'pickpaperwork'
             ReturnValue = PickPaperwork(Self,p_stage)
           RETURN ReturnValue
        Of 'bannernewjobbooking'
             ReturnValue = BannerNewJobBooking(Self,p_stage)
           RETURN ReturnValue
        Of 'addtobatch'
             ReturnValue = AddToBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'formrepairnotes'
             ReturnValue = FormRepairNotes(Self,p_stage)
           RETURN ReturnValue
        Of 'jobfaultcodes'
             ReturnValue = JobFaultCodes(Self,p_stage)
           RETURN ReturnValue
        Of 'amendaddress'
             ReturnValue = AmendAddress(Self,p_stage)
           RETURN ReturnValue
        Of 'browseauditfilter'
             ReturnValue = BrowseAuditFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'pickloanunit'
             ReturnValue = PickLoanUnit(Self,p_stage)
           RETURN ReturnValue
        Of 'jobsearch'
             ReturnValue = JobSearch(Self,p_stage)
           RETURN ReturnValue
        Of 'clearaddressdetails'
             ReturnValue = ClearAddressDetails(Self,p_stage)
           RETURN ReturnValue
        Of 'obfvalidation'
             ReturnValue = OBFValidation(Self,p_stage)
           RETURN ReturnValue
        Of 'sendanemail'
             ReturnValue = SendAnEmail(Self,p_stage)
           RETURN ReturnValue
        Of 'formpayments'
             ReturnValue = FormPayments(Self,p_stage)
           RETURN ReturnValue
        Of 'formaddtobatch'
             ReturnValue = FormAddToBatch(Self,p_stage)
           RETURN ReturnValue
        Of 'formpickmodelstocklocation'
             ReturnValue = FormPickModelStockLocation(Self,p_stage)
           RETURN ReturnValue
        Of 'insertjob_finished'
             ReturnValue = InsertJob_Finished(Self,p_stage)
           RETURN ReturnValue
        Of 'viewcosts'
             ReturnValue = ViewCosts(Self,p_stage)
           RETURN ReturnValue
        Of 'bannerblank'
             ReturnValue = BannerBlank(Self,p_stage)
           RETURN ReturnValue
        Of 'pickexchangeunit'
             ReturnValue = PickExchangeUnit(Self,p_stage)
           RETURN ReturnValue
        Of 'bannermainmenu'
             ReturnValue = BannerMainMenu(Self,p_stage)
           RETURN ReturnValue
        Of 'pagefooter'
             ReturnValue = PageFooter(Self,p_stage)
           RETURN ReturnValue
        Of 'pickaccessory'
             ReturnValue = PickAccessory(Self,p_stage)
           RETURN ReturnValue
        Of 'formbrowseengineerhistory'
             ReturnValue = FormBrowseEngineerHistory(Self,p_stage)
           RETURN ReturnValue
        Of 'invoicecreated'
             ReturnValue = InvoiceCreated(Self,p_stage)
           RETURN ReturnValue
        Of 'viewbouncerjob'
             ReturnValue = ViewBouncerJob(Self,p_stage)
           RETURN ReturnValue
        Of 'printroutines'
             ReturnValue = PrintRoutines(Self,p_stage)
           RETURN ReturnValue
        Of 'bannerengineeringdetails'
             ReturnValue = BannerEngineeringDetails(Self,p_stage)
           RETURN ReturnValue
        Of 'jobestimatequery'
             ReturnValue = JobEstimateQuery(Self,p_stage)
           RETURN ReturnValue
        Of 'newjobbooking'
             ReturnValue = NewJobBooking(Self,p_stage)
           RETURN ReturnValue
        Of 'browsestatuschangesfilter'
             ReturnValue = BrowseStatusChangesFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'setjobtype'
             ReturnValue = SetJobType(Self,p_stage)
           RETURN ReturnValue
        Of 'bannerbrowsejobs'
             ReturnValue = BannerBrowseJobs(Self,p_stage)
           RETURN ReturnValue
        Of 'createinvoice'
             ReturnValue = CreateInvoice(Self,p_stage)
           RETURN ReturnValue
        Of 'despatchconfirmation'
             ReturnValue = DespatchConfirmation(Self,p_stage)
           RETURN ReturnValue
        Of 'receiptfrompup'
             ReturnValue = ReceiptFromPUP(Self,p_stage)
           RETURN ReturnValue
        Of 'formchargeableparts'
             ReturnValue = FormChargeableParts(Self,p_stage)
           RETURN ReturnValue
        Of 'formaccessorynumbers'
             ReturnValue = FormAccessoryNumbers(Self,p_stage)
           RETURN ReturnValue
        Of 'bannervodacom'
             ReturnValue = BannerVodacom(Self,p_stage)
           RETURN ReturnValue
        Of 'formengineeringoption'
             ReturnValue = FormEngineeringOption(Self,p_stage)
           RETURN ReturnValue
        Of 'allocateengineer'
             ReturnValue = AllocateEngineer(Self,p_stage)
           RETURN ReturnValue
        Of 'formestimateparts'
             ReturnValue = FormEstimateParts(Self,p_stage)
           RETURN ReturnValue
        Of 'formfiltermodelnumber'
             ReturnValue = formFilterModelNumber(Self,p_stage)
           RETURN ReturnValue
        Of 'displaybrowsepayments'
             ReturnValue = DisplayBrowsePayments(Self,p_stage)
           RETURN ReturnValue
        Of 'formjoboutfaults'
             ReturnValue = FormJobOutFaults(Self,p_stage)
           RETURN ReturnValue
        Of 'formchangedop'
             ReturnValue = FormChangeDOP(Self,p_stage)
           RETURN ReturnValue
        Of 'browsestatusfilter'
             ReturnValue = BrowseStatusFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'billingconfirmation'
             ReturnValue = BillingConfirmation(Self,p_stage)
           RETURN ReturnValue
        Of 'formloanunitfilter'
             ReturnValue = FormLoanUnitFilter(Self,p_stage)
           RETURN ReturnValue
        Of 'pickcontacthistorynotes'
             ReturnValue = PickContactHistoryNotes(Self,p_stage)
           RETURN ReturnValue
        Of 'formjobsinbatch'
             ReturnValue = FormJobsInBatch(Self,p_stage)
           RETURN ReturnValue
      End
    End
    If p_File &= conthist
       ReturnValue = FormContactHistory(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= webjob
       ReturnValue = ViewJob(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= warparts
       ReturnValue = FormWarrantyParts(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobrpnot
       ReturnValue = FormRepairNotes(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobpaymt
       ReturnValue = FormPayments(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobs_alias
       ReturnValue = ViewBouncerJob(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobs
       ReturnValue = NewJobBooking(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= parts
       ReturnValue = FormChargeableParts(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= jobaccno
       ReturnValue = FormAccessoryNumbers(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= estparts
       ReturnValue = FormEstimateParts(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= joboutfl
       ReturnValue = FormJobOutFaults(Self,p_stage)
       RETURN ReturnValue
    End
    If p_File &= muldespj
       ReturnValue = FormJobsInBatch(Self,p_stage)
       RETURN ReturnValue
    End
    Case Lower(Self.GetValue('Retry'))
      Of 'finishbatch'
        FinishBatch(Self,p_Stage)
      Of 'createcreditnote'
        CreateCreditNote(Self,p_Stage)
      Of 'individualdespatch'
        IndividualDespatch(Self,p_Stage)
      Of 'jobestimate'
        JobEstimate(Self,p_Stage)
      Of 'prenewjobbooking'
        PreNewJobBooking(Self,p_Stage)
      Of 'formbrowselocationhistory'
        FormBrowseLocationHistory(Self,p_Stage)
      Of 'loginform'
        LoginForm(Self,p_Stage)
      Of 'formbrowsecontacthistory'
        FormBrowseContactHistory(Self,p_Stage)
      Of 'multiplebatchdespatch'
        MultipleBatchDespatch(Self,p_Stage)
      Of 'indexpage'
        IndexPage(Self,p_Stage)
      Of 'jobaccessories'
        JobAccessories(Self,p_Stage)
      Of 'sendsms'
        SendSMS(Self,p_Stage)
      Of 'displayoutfaults'
        DisplayOutFaults(Self,p_Stage)
      Of 'pickengineersnotes'
        PickEngineersNotes(Self,p_Stage)
      Of 'formexchangeunitfilter'
        FormExchangeUnitFilter(Self,p_Stage)
      Of 'formdeletepart'
        FormDeletePart(Self,p_Stage)
      Of 'proofofpurchase'
        ProofOfPurchase(Self,p_Stage)
      Of 'createnewbatch'
        CreateNewBatch(Self,p_Stage)
      Of 'pickpaperwork'
        PickPaperwork(Self,p_Stage)
      Of 'bannernewjobbooking'
        BannerNewJobBooking(Self,p_Stage)
      Of 'addtobatch'
        AddToBatch(Self,p_Stage)
      Of 'jobfaultcodes'
        JobFaultCodes(Self,p_Stage)
      Of 'amendaddress'
        AmendAddress(Self,p_Stage)
      Of 'browseauditfilter'
        BrowseAuditFilter(Self,p_Stage)
      Of 'pickloanunit'
        PickLoanUnit(Self,p_Stage)
      Of 'jobsearch'
        JobSearch(Self,p_Stage)
      Of 'clearaddressdetails'
        ClearAddressDetails(Self,p_Stage)
      Of 'obfvalidation'
        OBFValidation(Self,p_Stage)
      Of 'sendanemail'
        SendAnEmail(Self,p_Stage)
      Of 'formaddtobatch'
        FormAddToBatch(Self,p_Stage)
      Of 'formpickmodelstocklocation'
        FormPickModelStockLocation(Self,p_Stage)
      Of 'insertjob_finished'
        InsertJob_Finished(Self,p_Stage)
      Of 'viewcosts'
        ViewCosts(Self,p_Stage)
      Of 'bannerblank'
        BannerBlank(Self,p_Stage)
      Of 'pickexchangeunit'
        PickExchangeUnit(Self,p_Stage)
      Of 'bannermainmenu'
        BannerMainMenu(Self,p_Stage)
      Of 'pagefooter'
        PageFooter(Self,p_Stage)
      Of 'pickaccessory'
        PickAccessory(Self,p_Stage)
      Of 'formbrowseengineerhistory'
        FormBrowseEngineerHistory(Self,p_Stage)
      Of 'invoicecreated'
        InvoiceCreated(Self,p_Stage)
      Of 'printroutines'
        PrintRoutines(Self,p_Stage)
      Of 'bannerengineeringdetails'
        BannerEngineeringDetails(Self,p_Stage)
      Of 'jobestimatequery'
        JobEstimateQuery(Self,p_Stage)
      Of 'browsestatuschangesfilter'
        BrowseStatusChangesFilter(Self,p_Stage)
      Of 'setjobtype'
        SetJobType(Self,p_Stage)
      Of 'bannerbrowsejobs'
        BannerBrowseJobs(Self,p_Stage)
      Of 'createinvoice'
        CreateInvoice(Self,p_Stage)
      Of 'despatchconfirmation'
        DespatchConfirmation(Self,p_Stage)
      Of 'receiptfrompup'
        ReceiptFromPUP(Self,p_Stage)
      Of 'bannervodacom'
        BannerVodacom(Self,p_Stage)
      Of 'formengineeringoption'
        FormEngineeringOption(Self,p_Stage)
      Of 'allocateengineer'
        AllocateEngineer(Self,p_Stage)
      Of 'formfiltermodelnumber'
        formFilterModelNumber(Self,p_Stage)
      Of 'displaybrowsepayments'
        DisplayBrowsePayments(Self,p_Stage)
      Of 'formchangedop'
        FormChangeDOP(Self,p_Stage)
      Of 'browsestatusfilter'
        BrowseStatusFilter(Self,p_Stage)
      Of 'billingconfirmation'
        BillingConfirmation(Self,p_Stage)
      Of 'formloanunitfilter'
        FormLoanUnitFilter(Self,p_Stage)
      Of 'pickcontacthistorynotes'
        PickContactHistoryNotes(Self,p_Stage)
    End
  ReturnValue = PARENT.CallForm(p_file,p_Stage)
  RETURN ReturnValue


p_web.ProcessLink PROCEDURE(<string p_action>)


  CODE
  ! C5.5:  file{prop:name} = clip(self.site.appPath) & 'file.tps'
  PARENT.ProcessLink(p_action)


p_web.ProcessTag PROCEDURE(string p_TagString)

loc:tag      String(255)

  CODE
  PARENT.ProcessTag(p_TagString)
    loc:tag = lower(p_TagString)
  Case loc:tag
    of 'finishbatch'
      finishbatch(Self)
    of 'messagequestion'
      messagequestion(Self)
    of 'createcreditnote'
      createcreditnote(Self)
    of 'individualdespatch'
      individualdespatch(Self)
    of 'jobestimate'
      jobestimate(Self)
    of 'prenewjobbooking'
      prenewjobbooking(Self)
    of 'formcontacthistory'
      formcontacthistory(Self)
    of 'formbrowselocationhistory'
      formbrowselocationhistory(Self)
    of 'clearjobvariables'
      clearjobvariables(Self)
    of 'selectengineers'
      selectengineers(Self)
    of 'loginform'
      loginform(Self)
    of 'formbrowsecontacthistory'
      formbrowsecontacthistory(Self)
    of 'messagealert'
      messagealert(Self)
    of 'browsecontacthistory'
      browsecontacthistory(Self)
    of 'tagvalidateloanaccessories'
      tagvalidateloanaccessories(Self)
    of 'browsejobsinbatch'
      browsejobsinbatch(Self)
    of 'lookuprrcaccounts'
      lookuprrcaccounts(Self)
    of 'browseloanunits'
      browseloanunits(Self)
    of 'lookupsuburbs'
      lookupsuburbs(Self)
    of 'multiplebatchdespatch'
      multiplebatchdespatch(Self)
    of 'selectcolours'
      selectcolours(Self)
    of 'indexpage'
      indexpage(Self)
    of 'viewjob'
      viewjob(Self)
    of 'browsejobcredits'
      browsejobcredits(Self)
    of 'browsechargeableparts'
      browsechargeableparts(Self)
    of 'jobaccessories'
      jobaccessories(Self)
    of 'browseestimateparts'
      browseestimateparts(Self)
    of 'clearupdatejobvariables'
      clearupdatejobvariables(Self)
    of 'selectaccessjobstatus'
      selectaccessjobstatus(Self)
    of 'sendsms'
      sendsms(Self)
    of 'displayoutfaults'
      displayoutfaults(Self)
    of 'browsejoboutfaults'
      browsejoboutfaults(Self)
    of 'pickengineersnotes'
      pickengineersnotes(Self)
    of 'lookupgenericaccounts'
      lookupgenericaccounts(Self)
    of 'bouncerchargeableparts'
      bouncerchargeableparts(Self)
    of 'formexchangeunitfilter'
      formexchangeunitfilter(Self)
    of 'browsesubaddresses'
      browsesubaddresses(Self)
    of 'browsewarrantyparts'
      browsewarrantyparts(Self)
    of 'formdeletepart'
      formdeletepart(Self)
    of 'proofofpurchase'
      proofofpurchase(Self)
    of 'createnewbatch'
      createnewbatch(Self)
    of 'formwarrantyparts'
      formwarrantyparts(Self)
    of 'pickpaperwork'
      pickpaperwork(Self)
    of 'bannernewjobbooking'
      bannernewjobbooking(Self)
    of 'addtobatch'
      addtobatch(Self)
    of 'browsemodelnumbers'
      browsemodelnumbers(Self)
    of 'formrepairnotes'
      formrepairnotes(Self)
    of 'lookupproductcodes'
      lookupproductcodes(Self)
    of 'browsepayments'
      browsepayments(Self)
    of 'jobfaultcodes'
      jobfaultcodes(Self)
    of 'selectcouriers'
      selectcouriers(Self)
    of 'selectchargetypes'
      selectchargetypes(Self)
    of 'amendaddress'
      amendaddress(Self)
    of 'browseauditfilter'
      browseauditfilter(Self)
    of 'pickloanunit'
      pickloanunit(Self)
    of 'jobsearch'
      jobsearch(Self)
    of 'clearaddressdetails'
      clearaddressdetails(Self)
    of 'obfvalidation'
      obfvalidation(Self)
    of 'sendanemail'
      sendanemail(Self)
    of 'browsestockpartfaultcodelookup'
      browsestockpartfaultcodelookup(Self)
    of 'formpayments'
      formpayments(Self)
    of 'formaddtobatch'
      formaddtobatch(Self)
    of 'browselocationhistory'
      browselocationhistory(Self)
    of 'browsestatuschanges'
      browsestatuschanges(Self)
    of 'formpickmodelstocklocation'
      formpickmodelstocklocation(Self)
    of 'insertjob_finished'
      insertjob_finished(Self)
    of 'viewcosts'
      viewcosts(Self)
    of 'browseengineerhistory'
      browseengineerhistory(Self)
    of 'selectnetworks'
      selectnetworks(Self)
    of 'bannerblank'
      bannerblank(Self)
    of 'menutools'
      menutools(Self)
    of 'browseoutfaultcodes'
      browseoutfaultcodes(Self)
    of 'selectunittypes'
      selectunittypes(Self)
    of 'pickexchangeunit'
      pickexchangeunit(Self)
    of 'bannermainmenu'
      bannermainmenu(Self)
    of 'bouncerwarrantyparts'
      bouncerwarrantyparts(Self)
    of 'browsemodelstock'
      browsemodelstock(Self)
    of 'pagefooter'
      pagefooter(Self)
    of 'bounceroutfaults'
      bounceroutfaults(Self)
    of 'pickaccessory'
      pickaccessory(Self)
    of 'browsejobfaultcodelookup'
      browsejobfaultcodelookup(Self)
    of 'formbrowseengineerhistory'
      formbrowseengineerhistory(Self)
    of 'invoicecreated'
      invoicecreated(Self)
    of 'browseimeihistory'
      browseimeihistory(Self)
    of 'browseexchangeunits'
      browseexchangeunits(Self)
    of 'viewbouncerjob'
      viewbouncerjob(Self)
    of 'printroutines'
      printroutines(Self)
    of 'browsepartfaultcodelookup'
      browsepartfaultcodelookup(Self)
    of 'gotobottom'
      gotobottom(Self)
    of 'bannerengineeringdetails'
      bannerengineeringdetails(Self)
    of 'jobestimatequery'
      jobestimatequery(Self)
    of 'newjobbooking'
      newjobbooking(Self)
    of 'viewchargeableparts'
      viewchargeableparts(Self)
    of 'lookupjobstatus'
      lookupjobstatus(Self)
    of 'setbottom'
      setbottom(Self)
    of 'selectmodelnumbers'
      selectmodelnumbers(Self)
    of 'browsestatuschangesfilter'
      browsestatuschangesfilter(Self)
    of 'setjobtype'
      setjobtype(Self)
    of 'bannerbrowsejobs'
      bannerbrowsejobs(Self)
    of 'browseoutfaultschargeableparts'
      browseoutfaultschargeableparts(Self)
    of 'viewestimateparts'
      viewestimateparts(Self)
    of 'browsesmshistory'
      browsesmshistory(Self)
    of 'createinvoice'
      createinvoice(Self)
    of 'viewwarrantyparts'
      viewwarrantyparts(Self)
    of 'popupmessage'
      popupmessage(Self)
    of 'browsebatchesinprogress'
      browsebatchesinprogress(Self)
    of 'selectsuppliers'
      selectsuppliers(Self)
    of 'despatchconfirmation'
      despatchconfirmation(Self)
    of 'receiptfrompup'
      receiptfrompup(Self)
    of 'formchargeableparts'
      formchargeableparts(Self)
    of 'formaccessorynumbers'
      formaccessorynumbers(Self)
    of 'bannervodacom'
      bannervodacom(Self)
    of 'formengineeringoption'
      formengineeringoption(Self)
    of 'allocateengineer'
      allocateengineer(Self)
    of 'formestimateparts'
      formestimateparts(Self)
    of 'formfiltermodelnumber'
      formfiltermodelnumber(Self)
    of 'browserepairnotes'
      browserepairnotes(Self)
    of 'browseengineersonjob'
      browseengineersonjob(Self)
    of 'browseaccessorynumber'
      browseaccessorynumber(Self)
    of 'browseoutfaultswarrantyparts'
      browseoutfaultswarrantyparts(Self)
    of 'displaybrowsepayments'
      displaybrowsepayments(Self)
    of 'browseoutfaultsestimateparts'
      browseoutfaultsestimateparts(Self)
    of 'formjoboutfaults'
      formjoboutfaults(Self)
    of 'formchangedop'
      formchangedop(Self)
    of 'browsestatusfilter'
      browsestatusfilter(Self)
    of 'billingconfirmation'
      billingconfirmation(Self)
    of 'selectmanufacturers'
      selectmanufacturers(Self)
    of 'selecttransittypes'
      selecttransittypes(Self)
    of 'browseaudittrail'
      browseaudittrail(Self)
    of 'formloanunitfilter'
      formloanunitfilter(Self)
    of 'sethubrepair'
      sethubrepair(Self)
    of 'pickcontacthistorynotes'
      pickcontacthistorynotes(Self)
    of 'selectfaultcodes'
      selectfaultcodes(Self)
    of 'formjobsinbatch'
      formjobsinbatch(Self)
  End


p_web.SetPics PROCEDURE(FILE p_file)


  CODE
  PARENT.SetPics(p_file)
    ! If you're getting an Error here then you probably have forgotten to add the
    ! _2nd_ global extension, the one called "NetWebServerGlobal".
    NetWebSetPics(Self,p_File)


p_web.SetSessionPics PROCEDURE(FILE p_file)


  CODE
  PARENT.SetSessionPics(p_file)
    NetWebSetSessionPics(Self,p_File)


p_web._AddFile PROCEDURE(FILE p_file)

ReturnValue          LONG,AUTO

RM      &RelationManager
Loc:Err Long(-1)

  CODE
    RM &= NetWebRelationManager(p_File)
    If NOT RM &= Null
      Loc:Err = RM.Me.Insert()
      Self.FileToSessionQueue(p_File,Net:AlsoValueQueue)
    End
    Return Loc:Err
  ReturnValue = PARENT._AddFile(p_file)
  RETURN ReturnValue


p_web._CloseFile PROCEDURE(FILE p_file)

ReturnValue          LONG,AUTO

RM      &RelationManager
Loc:Err Long(-1)

  CODE
    RM &= NetWebRelationManager(p_File)
    If NOT RM &= Null
      Loc:Err = RM.Close()
    end
    Return Loc:Err
  ReturnValue = PARENT._CloseFile(p_file)
  RETURN ReturnValue


p_web._DeleteFile PROCEDURE(FILE p_file)

ReturnValue          LONG,AUTO

RM      &RelationManager
Loc:Err Long(-1)

  CODE
    RM &= NetWebRelationManager(p_File)
    If NOT RM &= Null
      Loc:Err = RM.Delete(False)
    End
    Return Loc:Err
  ReturnValue = PARENT._DeleteFile(p_file)
  RETURN ReturnValue


p_web._GetFile PROCEDURE(FILE p_file,KEY p_Key)

ReturnValue          LONG,AUTO

RM      &RelationManager
Loc:Err Long(-1)

  CODE
    RM &= NetWebRelationManager(p_File)
    If NOT RM &= Null
      Loc:Err = RM.Me.Fetch(p_Key)
      RM.Save()                    ! saves related file info so relational update will work.
    End
    Return Loc:Err
  ReturnValue = PARENT._GetFile(p_file,p_Key)
  RETURN ReturnValue


p_web._NetWebFileNamed PROCEDURE(STRING p_Label)

ReturnValue          &FILE


  CODE
    ReturnValue &= NetWebFileNamed(p_Label)
    Return ReturnValue
  ReturnValue &= PARENT._NetWebFileNamed(p_Label)
  RETURN ReturnValue


p_web._OpenFile PROCEDURE(FILE p_file)

ReturnValue          LONG,AUTO

RM      &RelationManager
Loc:Err Long(-1)

  CODE
    RM &= NetWebRelationManager(p_File)
    If NOT RM &= Null
      Loc:Err = RM.Open()
    End
    Return Loc:Err
  ReturnValue = PARENT._OpenFile(p_file)
  RETURN ReturnValue


p_web._SendFile PROCEDURE(string p_FileName,Long p_header=NET:SendHeader)

loc:parent  string(250)
loc:done        Long
loc:filename    string(255)

  CODE
  
    loc:parent = Lower(self.GetValue('_ParentProc'))
    loc:filename = SELF.GetPageName(Lower(p_FileName))
  
    do CaseStart:WebServer_Phase3
    If loc:Done then Return.
  
  PARENT._SendFile(p_FileName,p_header)

CallLoginPage  routine
  If self.site.LoginPageIsControl
    self.MakePage(self.site.LoginPage)
  else
    self._SendFile(self.site.LoginPage)
  End

CaseStart:WebServer_Phase3  routine
  do Case:FinishBatch
  do Case:CreateCreditNote
  do Case:IndividualDespatch
  do Case:JobEstimate
  do Case:ExchangeOrder
  do Case:PreNewJobBooking
  do Case:FormContactHistory
  do Case:FormBrowseLocationHistory
  do Case:SelectEngineers
  do Case:LoginForm
  do Case:FormBrowseContactHistory
  do Case:BrowseContactHistory
  do Case:TagValidateLoanAccessories
  do Case:BrowseJobsInBatch
  do Case:LookupRRCAccounts
  do Case:BrowseLoanUnits
  do Case:LookupSuburbs
  do Case:MultipleBatchDespatch
  do Case:SelectColours
  do Case:VodacomSingleInvoice
  do Case:IndexPage
  do Case:ViewJob
  do Case:BrowseJobCredits
  do Case:BrowseChargeableParts
  do Case:JobAccessories
  do Case:BrowseEstimateParts
  do Case:SelectAccessJobStatus
  do Case:SendSMS
  do Case:DisplayOutFaults
  do Case:BrowseJobOutFaults
  do Case:PickEngineersNotes
  do Case:LookupGenericAccounts
  do Case:BouncerChargeableParts
  do Case:FormExchangeUnitFilter
  do Case:BrowseSubAddresses
  do Case:BrowseWarrantyParts
  do Case:FormDeletePart
  do Case:ProofOfPurchase
  do Case:CreateNewBatch
  do Case:FormWarrantyParts
  do Case:PickPaperwork
  do Case:BannerNewJobBooking
  do Case:DespatchNote
  do Case:AddToBatch
  do Case:BrowseModelNumbers
  do Case:FormRepairNotes
  do Case:LookupProductCodes
  do Case:BrowsePayments
  do Case:Estimate
  do Case:JobFaultCodes
  do Case:SelectCouriers
  do Case:RefreshPage
  do Case:SelectChargeTypes
  do Case:AmendAddress
  do Case:BrowseAuditFilter
  do Case:PickLoanUnit
  do Case:JobSearch
  do Case:ClearAddressDetails
  do Case:OBFValidation
  do Case:SendAnEmail
  do Case:BrowseStockPartFaultCodeLookup
  do Case:FormPayments
  do Case:FormAddToBatch
  do Case:BrowseLocationHistory
  do Case:BrowseStatusChanges
  do Case:FormPickModelStockLocation
  do Case:InsertJob_Finished
  do Case:ViewCosts
  do Case:BrowseEngineerHistory
  do Case:SelectNetworks
  do Case:BannerBlank
  do Case:BrowseOutFaultCodes
  do Case:SelectUnitTypes
  do Case:PickExchangeUnit
  do Case:BannerMainMenu
  do Case:BouncerWarrantyParts
  do Case:BrowseModelStock
  do Case:PageFooter
  do Case:BouncerOutFaults
  do Case:PickAccessory
  do Case:BrowseJobFaultCodeLookup
  do Case:FormBrowseEngineerHistory
  do Case:InvoiceCreated
  do Case:BrowseIMEIHistory
  do Case:JobReceipt
  do Case:BrowseExchangeUnits
  do Case:ViewBouncerJob
  do Case:PrintRoutines
  do Case:BrowsePartFaultCodeLookup
  do Case:BannerEngineeringDetails
  do Case:JobEstimateQuery
  do Case:NewJobBooking
  do Case:ViewChargeableParts
  do Case:LookupJobStatus
  do Case:SelectModelNumbers
  do Case:BrowseStatusChangesFilter
  do Case:SetJobType
  do Case:BannerBrowseJobs
  do Case:BrowseOutFaultsChargeableParts
  do Case:InvoiceNote
  do Case:ViewEstimateParts
  do Case:Waybill
  do Case:BrowseSMSHistory
  do Case:CreateInvoice
  do Case:ViewWarrantyParts
  do Case:BrowseBatchesInProgress
  do Case:SelectSuppliers
  do Case:DespatchConfirmation
  do Case:ReceiptFromPUP
  do Case:FormChargeableParts
  do Case:FormAccessoryNumbers
  do Case:BannerVodacom
  do Case:FormEngineeringOption
  do Case:AllocateEngineer
  do Case:FormEstimateParts
  do Case:formFilterModelNumber
  do Case:BrowseRepairNotes
  do Case:BrowseEngineersOnJob
  do Case:BrowseAccessoryNumber
  do Case:CloseWebPage
  do Case:BrowseOutFaultsWarrantyParts
  do Case:DisplayBrowsePayments
  do Case:BrowseOutFaultsEstimateParts
  do Case:FormJobOutFaults
  do Case:FormChangeDOP
  do Case:BrowseStatusFilter
  do Case:BouncerHistory
  do Case:BillingConfirmation
  do Case:SelectManufacturers
  do Case:SelectTransitTypes
  do Case:BrowseAuditTrail
  do Case:FormLoanUnitFilter
  do Case:PageHeader
  do Case:JobCard
  do Case:SetHubRepair
  do Case:PickContactHistoryNotes
  do Case:SelectFaultCodes
  do Case:FormJobsInBatch

! - -  - - - - - - - - - - - - - -
Case:FinishBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'finishbatch'
  orof 'finish batch'
    Self.MakePage('FinishBatch',Net:Web:Form,Net:Login,'Finish Batch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('finishbatch_locwaybillnumber_value')
    FinishBatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('finishbatch_buttonfinishbatch_value')
    FinishBatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CreateCreditNote  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'createcreditnote'
  orof 'create credit note'
    Self.MakePage('CreateCreditNote',Net:Web:Form,Net:Login,'Create Credit Note',,)
    loc:Done = 1 ; Exit
  of self._nocolon('createcreditnote_loccredittype_value')
    CreateCreditNote(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('createcreditnote_loccreditamount_value')
    CreateCreditNote(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('createcreditnote_buttoncreatecreditnote_value')
    CreateCreditNote(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:IndividualDespatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'individualdespatch'
  orof 'individual despatch'
    Self.MakePage('IndividualDespatch',Net:Web:Form,Net:Login,'Individual Despatch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locjobnumber_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locimeinumber_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonvalidatejobdetails_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_tagvalidateloanaccessories_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonvalidateaccessories_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locaccessorypassword_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonconfirmmismatch_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonfailaccessory_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_cou:courier_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locconsignmentnumber_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_locsecuritypackid_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('individualdespatch_buttonconfirmdespatch_value')
    IndividualDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobEstimate  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobestimate'
  orof 'estimatedetails'
    Self.MakePage('JobEstimate',Net:Web:Form,0,'Estimate Details',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_if_over_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_ready_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_accepted_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_job:estimate_rejected_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestaccby_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestacccommunicationmethod_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestrejby_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestrejcommunicationmethod_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimate_locestrejreason_value')
    JobEstimate(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ExchangeOrder  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'exchangeorder'
  orof 'exchangeorder' & '.pdf'
    ExchangeOrder(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PreNewJobBooking  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'prenewjobbooking'
  orof 'newjobbooking.htm'
    Self.MakePage('PreNewJobBooking',Net:Web:Form,Net:Login,'New Job Booking','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:colour_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:ordernumber_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:faultcode_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_mj:engineersnotes_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:transittype_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:esn_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_buttonvalidateimei_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:manufacturer_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('prenewjobbooking_tmp:modelnumber_value')
    PreNewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormContactHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formcontacthistory'
  orof 'insert / amend contact history'
    Self.MakePage('FormContactHistory',Net:Web:Form,Net:Login,'Insert / Amend Contact History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formcontacthistory_cht:action_value')
    FormContactHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formcontacthistory_cht:notes_value')
    FormContactHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormBrowseLocationHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formbrowselocationhistory'
  orof 'location history'
    Self.MakePage('FormBrowseLocationHistory',Net:Web:Form,Net:Login,'Location History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formbrowselocationhistory_browselocationhistory_value')
    FormBrowseLocationHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectEngineers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectengineers'
  orof 'selectengineers' & '_' & loc:parent
  orof 'select engineer'
  orof 'select engineer' & '_' & loc:parent
    Self.MakePage('SelectEngineers',Net:Web:Browse,Net:Login,'Select Engineer',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LoginForm  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'loginform'
  orof 'sblogin.htm'
    Self.MakePage('LoginForm',Net:Web:Form,0,'ServiceBase Login','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_loc:branchid_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_loc:password_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_locnewpassword_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('loginform_locconfirmnewpassword_value')
    LoginForm(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormBrowseContactHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formbrowsecontacthistory'
  orof 'contact history'
    Self.MakePage('FormBrowseContactHistory',Net:Web:Form,Net:Login,'Contact History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formbrowsecontacthistory_browsecontacthistory_value')
    FormBrowseContactHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseContactHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsecontacthistory'
  orof 'browsecontacthistory' & '_' & loc:parent
  orof 'browse contact history'
  orof 'browse contact history' & '_' & loc:parent
    Self.MakePage('BrowseContactHistory',Net:Web:Browse,Net:Login,'Browse Contact History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:TagValidateLoanAccessories  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'tagvalidateloanaccessories'
  orof 'tagvalidateloanaccessories' & '_' & loc:parent
  orof 'tagvalidateloanaccessories' & '_' & loc:parent
    Self.MakePage('TagValidateLoanAccessories',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobsInBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejobsinbatch'
  orof 'browsejobsinbatch' & '_' & loc:parent
  orof 'browse jobs in batch'
  orof 'browse jobs in batch' & '_' & loc:parent
    Self.MakePage('BrowseJobsInBatch',Net:Web:Browse,Net:Login,'Browse Jobs In Batch',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupRRCAccounts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookuprrcaccounts'
  orof 'lookuprrcaccounts' & '_' & loc:parent
  orof 'new job booking'
  orof 'new job booking' & '_' & loc:parent
    Self.MakePage('LookupRRCAccounts',Net:Web:Browse,Net:Login,'New Job Booking',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseLoanUnits  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseloanunits'
  orof 'browseloanunits' & '_' & loc:parent
  orof 'browseloanunits' & '_' & loc:parent
    Self.MakePage('BrowseLoanUnits',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupSuburbs  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupsuburbs'
  orof 'lookupsuburbs' & '_' & loc:parent
  orof 'new job booking'
  orof 'new job booking' & '_' & loc:parent
    Self.MakePage('LookupSuburbs',Net:Web:Browse,Net:Login,'New Job Booking',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:MultipleBatchDespatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'multiplebatchdespatch'
  orof 'multbatchdespatch.htm'
    Self.MakePage('MultipleBatchDespatch',Net:Web:Form,Net:Login,'Multiple Batch Despatch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_browsebatchesinprogress_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locjobnumber_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locimeinumber_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locsecuritypacknumber_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonprocessjob_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_tagvalidateloanaccessories_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonvalidateaccessories_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonconfirmmismatch_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_buttonfailaccessory_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('multiplebatchdespatch_locaccessorypassword_value')
    MultipleBatchDespatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectColours  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectcolours'
  orof 'selectcolours' & '_' & loc:parent
  orof 'select colour'
  orof 'select colour' & '_' & loc:parent
    Self.MakePage('SelectColours',Net:Web:Browse,Net:Login,'Select Colour',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:VodacomSingleInvoice  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'vodacomsingleinvoice'
  orof 'vodacomsingleinvoice' & '.pdf'
    VodacomSingleInvoice(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:IndexPage  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'indexpage'
  orof 'servicebase 3g'
    Self.MakePage('IndexPage',Net:Web:Form,Net:Login,'ServiceBase 3g','<!-- Net:BannerBlank-->','')
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewJob  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewjob'
  orof 'amend job'
    Self.MakePage('ViewJob',Net:Web:Form,Net:Login,'Amend Job',,)
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_job:current_status_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_jbn:fault_description_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_jbn:engineers_notes_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_jobe:network_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_job:authority_number_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_job:unit_type_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_browseestimateparts_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_browsechargeableparts_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_browsewarrantyparts_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_buttonresendxml_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_loccchargetypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_loccrepairtypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_locwchargetypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_locwrepairtypereason_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewjob_buttonprintestimate_value')
    ViewJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobCredits  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejobcredits'
  orof 'browsejobcredits' & '_' & loc:parent
  orof 'browsejobcredits' & '_' & loc:parent
    Self.MakePage('BrowseJobCredits',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsechargeableparts'
  orof 'browsechargeableparts' & '_' & loc:parent
  orof 'browsechargeableparts' & '_' & loc:parent
    Self.MakePage('BrowseChargeableParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobAccessories  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobaccessories'
  orof 'jobaccessories.htm'
    Self.MakePage('JobAccessories',Net:Web:Form,Net:Login,'Job Accessories',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_tmp:showaccessory_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_tmp:theaccessory_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_buttonaddaccessories_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_buttonremoveaccessory_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_jobe:accessorynotes_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobaccessories_browseaccessorynumber_value')
    JobAccessories(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseestimateparts'
  orof 'browseestimateparts' & '_' & loc:parent
  orof 'browseestimateparts' & '_' & loc:parent
    Self.MakePage('BrowseEstimateParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectAccessJobStatus  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectaccessjobstatus'
  orof 'selectaccessjobstatus' & '_' & loc:parent
  orof 'select status'
  orof 'select status' & '_' & loc:parent
    Self.MakePage('SelectAccessJobStatus',Net:Web:Browse,Net:Login,'Select Status',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SendSMS  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'sendsms'
  orof 'send sms'
    Self.MakePage('SendSMS',Net:Web:Form,0,'Send SMS',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DisplayOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'displayoutfaults'
  orof 'out faults'
    Self.MakePage('DisplayOutFaults',Net:Web:Form,0,'Out Faults',,)
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browsejoboutfaults_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browseoutfaultschargeableparts_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browseoutfaultswarrantyparts_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('displayoutfaults_browseoutfaultsestimateparts_value')
    DisplayOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejoboutfaults'
  orof 'browsejoboutfaults' & '_' & loc:parent
  orof 'out faults'
  orof 'out faults' & '_' & loc:parent
    Self.MakePage('BrowseJobOutFaults',Net:Web:Browse,Net:Login,'Out Faults',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickEngineersNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickengineersnotes'
  orof 'engineers notes'
    Self.MakePage('PickEngineersNotes',Net:Web:Form,0,'Engineers Notes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_locpicklist_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_locfinallist_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_button:addselected_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_button:removeselected_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_gap_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickengineersnotes_button:removeall_value')
    PickEngineersNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupGenericAccounts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupgenericaccounts'
  orof 'lookupgenericaccounts' & '_' & loc:parent
  orof 'new job booking'
  orof 'new job booking' & '_' & loc:parent
    Self.MakePage('LookupGenericAccounts',Net:Web:Browse,Net:Login,'New Job Booking',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bouncerchargeableparts'
  orof 'bouncerchargeableparts' & '_' & loc:parent
  orof 'bouncerchargeableparts' & '_' & loc:parent
    Self.MakePage('BouncerChargeableParts',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormExchangeUnitFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formexchangeunitfilter'
  orof 'browse exchange unit'
    Self.MakePage('FormExchangeUnitFilter',Net:Web:Form,Net:Login,'Browse Exchange Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_tmp:exchangestocktype_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_tmp:exchangemanufacturer_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_tmp:exchangemodelnumber_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formexchangeunitfilter_browseexchangeunits_value')
    FormExchangeUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseSubAddresses  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsesubaddresses'
  orof 'browsesubaddresses' & '_' & loc:parent
  orof 'browse accounts'
  orof 'browse accounts' & '_' & loc:parent
    Self.MakePage('BrowseSubAddresses',Net:Web:Browse,0,'Browse Accounts',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsewarrantyparts'
  orof 'browsewarrantyparts' & '_' & loc:parent
  orof 'browsewarrantyparts' & '_' & loc:parent
    Self.MakePage('BrowseWarrantyParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormDeletePart  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formdeletepart'
  orof 'delete part'
    Self.MakePage('FormDeletePart',Net:Web:Form,Net:Login,'Delete Part',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formdeletepart_locpartnumber_value')
    FormDeletePart(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formdeletepart_locdescription_value')
    FormDeletePart(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formdeletepart_locscraprestock_value')
    FormDeletePart(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ProofOfPurchase  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'proofofpurchase'
  orof 'proof of purchase'
    Self.MakePage('ProofOfPurchase',Net:Web:Form,Net:Login,'Proof Of Purchase',,)
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:dop_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:pop_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_locpoptypepassword_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:poptype_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('proofofpurchase_tmp:warrantyrefno_value')
    ProofOfPurchase(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CreateNewBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'createnewbatch'
    Self.MakePage('CreateNewBatch',Net:Web:Form,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formwarrantyparts'
  orof 'insert / amend warranty parts'
    Self.MakePage('FormWarrantyParts',Net:Web:Form,Net:Login,'Insert / Amend Warranty Parts',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:correction_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:part_number_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:description_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:despatch_note_number_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:quantity_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:purchasecost_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:inwarrantycost_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:inwarrantymarkup_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:supplier_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:exclude_from_order_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_wpr:partallocated_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:unallocatepart_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:createorder_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcodeschecked_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcodes1_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode2_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode3_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode4_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode5_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode6_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode7_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode8_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode9_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode10_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode11_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formwarrantyparts_tmp:faultcode12_value')
    FormWarrantyParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickPaperwork  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickpaperwork'
    Self.MakePage('PickPaperwork',Net:Web:Form,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerNewJobBooking  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannernewjobbooking'
    Self.MakePage('BannerNewJobBooking',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DespatchNote  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'despatchnote'
  orof 'despatchnote' & '.pdf'
    DespatchNote(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:AddToBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'addtobatch'
    Self.MakePage('AddToBatch',Net:Web:Form,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseModelNumbers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsemodelnumbers'
  orof 'browsemodelnumbers' & '_' & loc:parent
  orof 'browsemodelnumbers' & '_' & loc:parent
    Self.MakePage('BrowseModelNumbers',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormRepairNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formrepairnotes'
  orof 'insert / amend repair notes'
    Self.MakePage('FormRepairNotes',Net:Web:Form,Net:Login,'Insert / Amend Repair Notes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formrepairnotes_jrn:thedate_value')
    FormRepairNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formrepairnotes_jrn:thetime_value')
    FormRepairNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formrepairnotes_jrn:notes_value')
    FormRepairNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupProductCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupproductcodes'
  orof 'lookupproductcodes' & '_' & loc:parent
  orof 'select product code'
  orof 'select product code' & '_' & loc:parent
    Self.MakePage('LookupProductCodes',Net:Web:Browse,Net:Login,'Select Product Code',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowsePayments  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsepayments'
  orof 'browsepayments' & '_' & loc:parent
  orof 'browse payments'
  orof 'browse payments' & '_' & loc:parent
    Self.MakePage('BrowsePayments',Net:Web:Browse,0,'Browse Payments',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:Estimate  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'estimate'
  orof 'estimate' & '.pdf'
    Estimate(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobFaultCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobfaultcodes'
  orof 'jobfaultcodes.htm'
    Self.MakePage('JobFaultCodes',Net:Web:Form,Net:Login,'Job Fault Codes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:msn_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_buttonverifymsn_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:verifymsn_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_buttonverifymsn2_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:confirmmsnchange_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_job:productcode_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode1_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode2_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode3_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode4_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode5_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode6_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode7_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode8_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode9_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode10_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode11_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode12_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode13_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode14_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode15_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode16_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode17_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode18_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode19_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:faultcode20_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobfaultcodes_tmp:processexchange_value')
    JobFaultCodes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectCouriers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectcouriers'
  orof 'selectcouriers' & '_' & loc:parent
  orof 'select courier'
  orof 'select courier' & '_' & loc:parent
    Self.MakePage('SelectCouriers',Net:Web:Browse,Net:Login,'Select Courier',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:RefreshPage  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'refreshpage'
    RefreshPage(self)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectChargeTypes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectchargetypes'
  orof 'selectchargetypes' & '_' & loc:parent
  orof 'select charge type'
  orof 'select charge type' & '_' & loc:parent
    Self.MakePage('SelectChargeTypes',Net:Web:Browse,Net:Login,'Select Charge Type',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:AmendAddress  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'amendaddress'
  orof 'amend addresses'
    Self.MakePage('AmendAddress',Net:Web:Form,Net:Login,'Amend Addresses',,)
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:company_name_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line1_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line2_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line3_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:hubcustomer_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:postcode_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:telephone_number_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:fax_number_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:enduseremailaddress_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:endusertelno_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:vatnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:idnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:smsnotification_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:smsalertnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:emailnotification_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:emailalertaddress_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:courierwaybillnumber_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:company_name_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:company_name_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line1_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line1_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line2_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line2_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line3_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:address_line3_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:hubdelivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe2:hubcollection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:postcode_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:postcode_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:telephone_delivery_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_job:telephone_collection_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_button:copyenduseraddress_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_button:copyenduseraddress1_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:delivery_text_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:collection_text_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:delcontactname_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:colcontatname_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:deldepartment_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jbn:coldepartment_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('amendaddress_jobe:sub_sub_account_value')
    AmendAddress(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseAuditFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseauditfilter'
  orof 'browse audit trail'
    Self.MakePage('BrowseAuditFilter',Net:Web:Form,Net:Login,'Browse Audit Trail',,)
    loc:Done = 1 ; Exit
  of self._nocolon('browseauditfilter_tmp:statustypefilter_value')
    BrowseAuditFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('browseauditfilter_browseaudittrail_value')
    BrowseAuditFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickLoanUnit  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickloanunit'
  orof 'pick loan unit'
    Self.MakePage('PickLoanUnit',Net:Web:Form,Net:Login,'Pick Loan Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tmp:loanimeinumber_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonpickloanunit_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_jobe2:loanidnumber_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_locloanidoption_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_button:amenddespatchdetails_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonremoveattachedunit_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_locremovalalertmessage_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tmp:removalreason_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tagvalidateloanaccessories_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonvalidateaccessories_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_tmp:lostloanfee_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_buttonconfirmloanremoval_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickloanunit_link:sendsms_value')
    PickLoanUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobSearch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobsearch'
  orof 'jobsearch'
    Self.MakePage('JobSearch',Net:Web:Form,Net:Login,'Job Search',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobsearch_locsearchjobnumber_value')
    JobSearch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('jobsearch_locjobpassword_value')
    JobSearch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ClearAddressDetails  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'clearaddressdetails'
  orof 'address options'
    Self.MakePage('ClearAddressDetails',Net:Web:Form,0,'Address Options',,)
    loc:Done = 1 ; Exit
  of self._nocolon('clearaddressdetails_locclearaddressoption_value')
    ClearAddressDetails(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:OBFValidation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'obfvalidation'
  orof 'obfvalidation.htm'
    Self.MakePage('OBFValidation',Net:Web:Form,Net:Login,'OBF Validation','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:imeinumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:boximeinumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:network_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:dateofpurchase_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:returndate_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:talktime_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originaldealer_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:branchofreturn_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:storereferencenumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:proofofpurchase_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originalpackaging_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originalaccessories_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:originalmanuals_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:physicaldamage_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:replacement_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:laccountnumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:replacementimeinumber_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_button:failvalidation_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_error:validation_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('obfvalidation_tmp:transittype_value')
    OBFValidation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SendAnEmail  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'sendanemail'
  orof 'send email'
    Self.MakePage('SendAnEmail',Net:Web:Form,0,'Send Email',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStockPartFaultCodeLookup  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestockpartfaultcodelookup'
  orof 'browsestockpartfaultcodelookup' & '_' & loc:parent
  orof 'select part fault codes'
  orof 'select part fault codes' & '_' & loc:parent
    Self.MakePage('BrowseStockPartFaultCodeLookup',Net:Web:Browse,Net:Login,'Select Part Fault Codes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormPayments  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formpayments'
  orof 'job payments'
    Self.MakePage('FormPayments',Net:Web:Form,Net:Login,'Job Payments',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:date_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:payment_type_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:credit_card_number_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:expiry_date_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:issue_number_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpayments_jpt:amount_value')
    FormPayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormAddToBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formaddtobatch'
  orof 'multiple despatch'
    Self.MakePage('FormAddToBatch',Net:Web:Form,Net:Login,'Multiple Despatch',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formaddtobatch_locwaybillnumber_value')
    FormAddToBatch(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseLocationHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browselocationhistory'
  orof 'browselocationhistory' & '_' & loc:parent
  orof 'browse location history'
  orof 'browse location history' & '_' & loc:parent
    Self.MakePage('BrowseLocationHistory',Net:Web:Browse,Net:Login,'Browse Location History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStatusChanges  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestatuschanges'
  orof 'browsestatuschanges' & '_' & loc:parent
  orof 'browsestatuschanges' & '_' & loc:parent
    Self.MakePage('BrowseStatusChanges',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormPickModelStockLocation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formpickmodelstocklocation'
  orof 'browse stock'
    Self.MakePage('FormPickModelStockLocation',Net:Web:Form,0,'Browse Stock',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formpickmodelstocklocation_locpicklocation_value')
    FormPickModelStockLocation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formpickmodelstocklocation_browsemodelstock_value')
    FormPickModelStockLocation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:InsertJob_Finished  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'insertjob_finished'
  orof 'jobbooked.htm'
    Self.MakePage('InsertJob_Finished',Net:Web:Form,Net:Login,'Job Booked','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewCosts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewcosts'
  orof 'view costs'
    Self.MakePage('ViewCosts',Net:Web:Form,Net:Login,'View Costs',,)
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arcviewcosttype_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arcignoredefaultcharges_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arcignorereason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttonacceptarcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttoncancelarcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost1_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost1_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost2_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost2_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost3_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost3_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost4_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost4_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost5_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost5_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost6_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:adjustmentcost6_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost7_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:arccost8_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrcviewcosttype_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrcignoredefaultcharges_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrcignorereason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttonacceptrrcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_buttoncancelrrcreason_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost0_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost1_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:originalinvoice_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost2_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost3_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_browsejobcredits_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost4_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost5_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost6_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost7_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_tmp:rrccost8_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewcosts_jobe:excreplcamentcharge_value')
    ViewCosts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseEngineerHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseengineerhistory'
  orof 'browseengineerhistory' & '_' & loc:parent
  orof 'browse engineer history'
  orof 'browse engineer history' & '_' & loc:parent
    Self.MakePage('BrowseEngineerHistory',Net:Web:Browse,Net:Login,'Browse Engineer History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectNetworks  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectnetworks'
  orof 'selectnetworks' & '_' & loc:parent
  orof 'select network'
  orof 'select network' & '_' & loc:parent
    Self.MakePage('SelectNetworks',Net:Web:Browse,Net:Login,'Select Network',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerBlank  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannerblank'
    Self.MakePage('BannerBlank',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultcodes'
  orof 'browseoutfaultcodes' & '_' & loc:parent
  orof 'browse out faults'
  orof 'browse out faults' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultCodes',Net:Web:Browse,Net:Login,'Browse Out Faults',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectUnitTypes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectunittypes'
  orof 'selectunittypes' & '_' & loc:parent
  orof 'select unit type'
  orof 'select unit type' & '_' & loc:parent
    Self.MakePage('SelectUnitTypes',Net:Web:Browse,Net:Login,'Select Unit Type',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickExchangeUnit  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickexchangeunit'
  orof 'pick exchange unit'
    Self.MakePage('PickExchangeUnit',Net:Web:Form,Net:Login,'Pick Exchange Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:exchangeimeinumber_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_buttonpickexchangeunit_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:handsetpartnumber_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:handsetreplacementvalue_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_job:exchange_courier_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_button:amenddespatchdetails_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_buttonremoveattachedunit_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_locremovalalertmessage_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_tmp:removalreason_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickexchangeunit_buttonconfirmexchangeremoval_value')
    PickExchangeUnit(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerMainMenu  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannermainmenu'
    Self.MakePage('BannerMainMenu',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bouncerwarrantyparts'
  orof 'bouncerwarrantyparts' & '_' & loc:parent
  orof 'bouncerwarrantyparts' & '_' & loc:parent
    Self.MakePage('BouncerWarrantyParts',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseModelStock  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsemodelstock'
  orof 'browsemodelstock' & '_' & loc:parent
  orof 'browse stock'
  orof 'browse stock' & '_' & loc:parent
    Self.MakePage('BrowseModelStock',Net:Web:Browse,Net:Login,'Browse Stock',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PageFooter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pagefooter'
    Self.MakePage('PageFooter',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bounceroutfaults'
  orof 'bounceroutfaults' & '_' & loc:parent
  orof 'bounceroutfaults' & '_' & loc:parent
    Self.MakePage('BouncerOutFaults',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickAccessory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickaccessory'
    Self.MakePage('PickAccessory',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_tmp:showaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_tmp:theaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:addaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:removeaccessory_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:addall_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickaccessory_button:removeall_value')
    PickAccessory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseJobFaultCodeLookup  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsejobfaultcodelookup'
  orof 'browsejobfaultcodelookup' & '_' & loc:parent
  orof 'select fault codes'
  orof 'select fault codes' & '_' & loc:parent
    Self.MakePage('BrowseJobFaultCodeLookup',Net:Web:Browse,Net:Login,'Select Fault Codes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormBrowseEngineerHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formbrowseengineerhistory'
  orof 'engineer history'
    Self.MakePage('FormBrowseEngineerHistory',Net:Web:Form,Net:Login,'Engineer History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formbrowseengineerhistory_browseengineerhistory_value')
    FormBrowseEngineerHistory(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:InvoiceCreated  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'invoicecreated'
  orof 'invoicecreated.htm'
    Self.MakePage('InvoiceCreated',Net:Web:Form,0,'Invoice Created',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseIMEIHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseimeihistory'
  orof 'browseimeihistory' & '_' & loc:parent
  orof 'previous job history'
  orof 'previous job history' & '_' & loc:parent
    Self.MakePage('BrowseIMEIHistory',Net:Web:Browse,Net:Login,'Previous Job History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobReceipt  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobreceipt'
  orof 'jobreceipt' & '.pdf'
    JobReceipt(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseExchangeUnits  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseexchangeunits'
  orof 'browseexchangeunits' & '_' & loc:parent
  orof 'browseexchangeunits' & '_' & loc:parent
    Self.MakePage('BrowseExchangeUnits',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewBouncerJob  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewbouncerjob'
  orof 'view job details'
    Self.MakePage('ViewBouncerJob',Net:Web:Form,0,'View Job Details',,)
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:account_number_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:company_name_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:address_line1_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:address_line2_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:address_line3_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:postcode_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:telephone_number_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:fax_number_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('viewbouncerjob_tra:emailaddress_value')
    ViewBouncerJob(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PrintRoutines  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'printroutines'
  orof 'printroutines.htm'
    Self.MakePage('PrintRoutines',Net:Web:Form,Net:Login,'Print Routines',,)
    loc:Done = 1 ; Exit
  of self._nocolon('printroutines_locjobnumber_value')
    PrintRoutines(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('printroutines_locwaybillnumber_value')
    PrintRoutines(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowsePartFaultCodeLookup  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsepartfaultcodelookup'
  orof 'browsepartfaultcodelookup' & '_' & loc:parent
  orof 'select part fault codes'
  orof 'select part fault codes' & '_' & loc:parent
    Self.MakePage('BrowsePartFaultCodeLookup',Net:Web:Browse,Net:Login,'Select Part Fault Codes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerEngineeringDetails  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannerengineeringdetails'
    Self.MakePage('BannerEngineeringDetails',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobEstimateQuery  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobestimatequery'
  orof 'estimatequery'
    Self.MakePage('JobEstimateQuery',Net:Web:Form,Net:Login,'Estimate Query',,)
    loc:Done = 1 ; Exit
  of self._nocolon('jobestimatequery_locestimatereadyoption_value')
    JobEstimateQuery(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:NewJobBooking  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'newjobbooking'
  orof 'newjobbooking.htm'
    Self.MakePage('NewJobBooking',Net:Web:Form,Net:Login,'New Job Booking','<!-- Net:BannerBlank -->',)
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:previousaddress_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:usepreviousaddress_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:network_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:mobile_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_buttonmsisdncheck_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:unit_type_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:productcode_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:colour_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:msn_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:dop_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:pop_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:popconfirmed_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_locpoptypepassword_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:poptype_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:warrantyrefno_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:chargeable_job_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:charge_type_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:warranty_job_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:warranty_charge_type_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:authority_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_franchiseaccount_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:account_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:account_number2_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:order_number_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:title_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:initial_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:surname_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:endusertelno_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:courier_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:courierwaybillnumber_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:amendaccessories_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:showaccessory_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:theaccessory_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_button:addaccessories_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_button:removeaccessory_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_button:removeallaccessories_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:amendfaultcodes_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_1_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_2_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_3_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_4_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_5_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:faultcode_6_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_job:engineer_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jbn:fault_description_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jbn:engineers_notes_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_tmp:externaldamagechecklist_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xnone_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xantenna_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xlens_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xfcover_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xbcover_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xkeypad_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xbattery_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xcharger_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xlcd_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xsimreader_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xsystemconnector_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:xnotes_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:booking48houroption_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe:vsacustomer_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:contract_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('newjobbooking_jobe2:prepaid_value')
    NewJobBooking(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewchargeableparts'
  orof 'viewchargeableparts' & '_' & loc:parent
  orof 'viewchargeableparts' & '_' & loc:parent
    Self.MakePage('ViewChargeableParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:LookupJobStatus  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'lookupjobstatus'
  orof 'lookupjobstatus' & '_' & loc:parent
  orof 'lookupjobstatus' & '_' & loc:parent
    Self.MakePage('LookupJobStatus',Net:Web:Browse,0,'Select Job Status',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectModelNumbers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectmodelnumbers'
  orof 'selectmodelnumbers' & '_' & loc:parent
  orof 'select model number'
  orof 'select model number' & '_' & loc:parent
    Self.MakePage('SelectModelNumbers',Net:Web:Browse,Net:Login,'Select Model Number',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStatusChangesFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestatuschangesfilter'
    Self.MakePage('BrowseStatusChangesFilter',Net:Web:Form,Net:Login,'Browse Status Change History',,)
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatuschangesfilter_tmp:statustypefilter_value')
    BrowseStatusChangesFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatuschangesfilter_browsestatuschanges_value')
    BrowseStatusChangesFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SetJobType  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'setjobtype'
  orof 'set job type(s)'
    Self.MakePage('SetJobType',Net:Web:Form,Net:Login,'Set Job Type(s)',,)
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locchargeablejob_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_loccchargetype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_loccrepairtype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locwarrantyjob_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locwchargetype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_locwrepairtype_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_buttonconfirm_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_buttonsplitjob_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('setjobtype_buttoncancel_value')
    SetJobType(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerBrowseJobs  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannerbrowsejobs'
    Self.MakePage('BannerBrowseJobs',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultsChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultschargeableparts'
  orof 'browseoutfaultschargeableparts' & '_' & loc:parent
  orof 'browseoutfaultschargeableparts' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultsChargeableParts',Net:Web:Browse,Net:Login,'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''C''',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:InvoiceNote  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'invoicenote'
  orof 'invoicenote' & '.pdf'
    InvoiceNote(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewestimateparts'
  orof 'viewestimateparts' & '_' & loc:parent
  orof 'viewestimateparts' & '_' & loc:parent
    Self.MakePage('ViewEstimateParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:Waybill  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'waybill'
  orof 'waybill' & '.pdf'
    Waybill(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseSMSHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsesmshistory'
  orof 'browsesmshistory' & '_' & loc:parent
  orof 'browse sms history'
  orof 'browse sms history' & '_' & loc:parent
    Self.MakePage('BrowseSMSHistory',Net:Web:Browse,0,'Browse SMS History',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CreateInvoice  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'createinvoice'
  orof 'create invoice'
    Self.MakePage('CreateInvoice',Net:Web:Form,0,'Create Invoice',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ViewWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'viewwarrantyparts'
  orof 'viewwarrantyparts' & '_' & loc:parent
  orof 'viewwarrantyparts' & '_' & loc:parent
    Self.MakePage('ViewWarrantyParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseBatchesInProgress  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsebatchesinprogress'
  orof 'browsebatchesinprogress' & '_' & loc:parent
  orof 'browsebatchesinprogress' & '_' & loc:parent
    Self.MakePage('BrowseBatchesInProgress',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectSuppliers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectsuppliers'
  orof 'selectsuppliers' & '_' & loc:parent
  orof 'select supplier'
  orof 'select supplier' & '_' & loc:parent
    Self.MakePage('SelectSuppliers',Net:Web:Browse,Net:Login,'Select Supplier',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DespatchConfirmation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'despatchconfirmation'
  orof 'despatch confirmation'
    Self.MakePage('DespatchConfirmation',Net:Web:Form,Net:Login,'Despatch Confirmation',,)
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_job:courier_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_buttoncreateinvoice_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_buttonprintwaybill_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('despatchconfirmation_buttonprintdespatchnote_value')
    DespatchConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:ReceiptFromPUP  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'receiptfrompup'
  orof 'pup validation'
    Self.MakePage('ReceiptFromPUP',Net:Web:Form,Net:Login,'PUP Validation',,)
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locimeinumber_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locpassword_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locmsn_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locnetwork_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_locinfault_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_tagvalidateloanaccessories_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('receiptfrompup_buttonvalidateaccessories_value')
    ReceiptFromPUP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormChargeableParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formchargeableparts'
  orof 'insert / amend chargeable parts'
    Self.MakePage('FormChargeableParts',Net:Web:Form,Net:Login,'Insert / Amend Chargeable Parts',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:part_number_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:description_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:despatch_note_number_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:quantity_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:purchasecost_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:outwarrantycost_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:outwarrantymarkup_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:supplier_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:exclude_from_order_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_par:partallocated_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:unallocatepart_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:createorder_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcodeschecked_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcodes1_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode2_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode3_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode4_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode5_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode6_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode7_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode8_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode9_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode10_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode11_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchargeableparts_tmp:faultcode12_value')
    FormChargeableParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormAccessoryNumbers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formaccessorynumbers'
  orof 'accessorynumber'
    Self.MakePage('FormAccessoryNumbers',Net:Web:Form,Net:Login,'Accessory Number',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formaccessorynumbers_joa:accessorynumber_value')
    FormAccessoryNumbers(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BannerVodacom  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bannervodacom'
    Self.MakePage('BannerVodacom',Net:Web:Form,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormEngineeringOption  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formengineeringoption'
  orof 'change engineering option'
    Self.MakePage('FormEngineeringOption',Net:Web:Form,Net:Login,'Change Engineering Option',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locuserpassword_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locnewengineeringoption_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_job:dop_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locexchangemanufacturer_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locexchangemodelnumber_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locexchangenotes_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_locsplitjob_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formengineeringoption_button:createorder_value')
    FormEngineeringOption(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:AllocateEngineer  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'allocateengineer'
  orof 'allocate engineer'
    Self.MakePage('AllocateEngineer',Net:Web:Form,Net:Login,'Allocate Engineer',,)
    loc:Done = 1 ; Exit
  of self._nocolon('allocateengineer_tmp:engineerpassword_value')
    AllocateEngineer(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formestimateparts'
  orof 'insert / amend estimate parts'
    Self.MakePage('FormEstimateParts',Net:Web:Form,Net:Login,'Insert / Amend Estimate Parts',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:part_number_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:description_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:despatch_note_number_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:quantity_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:purchasecost_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:outwarrantycost_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:outwarrantymarkup_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:supplier_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:exclude_from_order_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:partallocated_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:unallocatepart_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:createorder_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcodeschecked_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcodes1_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode2_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode3_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode4_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode5_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode6_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode7_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode8_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode9_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode10_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode11_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_tmp:faultcode12_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formestimateparts_epr:usedonrepair_value')
    FormEstimateParts(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:formFilterModelNumber  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formfiltermodelnumber'
  orof 'select model numbers'
    Self.MakePage('formFilterModelNumber',Net:Web:Form,0,'Select Model Numbers',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formfiltermodelnumber_filter:manufacturer_value')
    formFilterModelNumber(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formfiltermodelnumber_browsemodelnumbers_value')
    formFilterModelNumber(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseRepairNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browserepairnotes'
  orof 'browserepairnotes' & '_' & loc:parent
  orof 'job repair notes'
  orof 'job repair notes' & '_' & loc:parent
    Self.MakePage('BrowseRepairNotes',Net:Web:Browse,Net:Login,'Job Repair Notes',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseEngineersOnJob  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseengineersonjob'
  orof 'browseengineersonjob' & '_' & loc:parent
  orof 'browseengineersonjob' & '_' & loc:parent
    Self.MakePage('BrowseEngineersOnJob',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseAccessoryNumber  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseaccessorynumber'
  orof 'browseaccessorynumber' & '_' & loc:parent
  orof 'browseaccessorynumber' & '_' & loc:parent
    Self.MakePage('BrowseAccessoryNumber',Net:Web:Browse,0,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:CloseWebPage  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'closewebpage'
    CloseWebPage(self)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultsWarrantyParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultswarrantyparts'
  orof 'browseoutfaultswarrantyparts' & '_' & loc:parent
  orof 'browseoutfaultswarrantyparts' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultsWarrantyParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:DisplayBrowsePayments  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'displaybrowsepayments'
  orof 'browsepayments.htm'
    Self.MakePage('DisplayBrowsePayments',Net:Web:Form,Net:Login,'Browse Payments',,)
    loc:Done = 1 ; Exit
  of self._nocolon('displaybrowsepayments_browsepayments_value')
    DisplayBrowsePayments(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseOutFaultsEstimateParts  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseoutfaultsestimateparts'
  orof 'browseoutfaultsestimateparts' & '_' & loc:parent
  orof 'browseoutfaultsestimateparts' & '_' & loc:parent
    Self.MakePage('BrowseOutFaultsEstimateParts',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormJobOutFaults  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formjoboutfaults'
  orof 'insert out fault'
    Self.MakePage('FormJobOutFaults',Net:Web:Form,Net:Login,'Insert Out Fault',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_tmp:freetextoutfault_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_joo:faultcode_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_joo:description_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formjoboutfaults_joo:level_value')
    FormJobOutFaults(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormChangeDOP  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formchangedop'
  orof 'override dop'
    Self.MakePage('FormChangeDOP',Net:Web:Form,Net:Login,'Override DOP',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_tmp:userpassword_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_job:dop_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_tmp:newdop_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formchangedop_tmp:changereason_value')
    FormChangeDOP(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseStatusFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browsestatusfilter'
  orof 'browse status changes'
    Self.MakePage('BrowseStatusFilter',Net:Web:Form,Net:Login,'Browse Status Changes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatusfilter_tmp:statustypefilter_value')
    BrowseStatusFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('browsestatusfilter_browsestatuschanges_value')
    BrowseStatusFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BouncerHistory  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'bouncerhistory'
  orof 'bouncerhistory' & '.pdf'
    BouncerHistory(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BillingConfirmation  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'billingconfirmation'
  orof 'billingconfirmation'
    Self.MakePage('BillingConfirmation',Net:Web:Form,Net:Login,'Billing Confirmation',,)
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:cchargetype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:crepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:cconfirmrepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:wchargetype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:wrepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('billingconfirmation_tmp:wconfirmrepairtype_value')
    BillingConfirmation(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectManufacturers  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectmanufacturers'
  orof 'selectmanufacturers' & '_' & loc:parent
  orof 'select manufacturer'
  orof 'select manufacturer' & '_' & loc:parent
    Self.MakePage('SelectManufacturers',Net:Web:Browse,0,'Select Manufacturer',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectTransitTypes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selecttransittypes'
  orof 'selecttransittypes' & '_' & loc:parent
  orof 'select transit type'
  orof 'select transit type' & '_' & loc:parent
    Self.MakePage('SelectTransitTypes',Net:Web:Browse,0,'Select Transit Type',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:BrowseAuditTrail  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'browseaudittrail'
  orof 'browseaudittrail' & '_' & loc:parent
  orof 'browseaudittrail' & '_' & loc:parent
    Self.MakePage('BrowseAuditTrail',Net:Web:Browse,Net:Login,,,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormLoanUnitFilter  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formloanunitfilter'
  orof 'browse loan unit'
    Self.MakePage('FormLoanUnitFilter',Net:Web:Form,Net:Login,'Browse Loan Unit',,)
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_tmp:loanstocktype_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_tmp:loanmanufacturer_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_tmp:loanmodelnumber_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('formloanunitfilter_browseloanunits_value')
    FormLoanUnitFilter(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PageHeader  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pageheader'
    PageHeader(self)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:JobCard  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'jobcard'
  orof 'jobcard' & '.pdf'
    JobCard(self) ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SetHubRepair  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'sethubrepair'
  orof 'sethubrepair' & '_' & loc:parent
      self._requestajaxnow = 1
      SetHubRepair(self)
      self._Sendfooter()
      loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:PickContactHistoryNotes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'pickcontacthistorynotes'
  orof 'contact history notes'
    Self.MakePage('PickContactHistoryNotes',Net:Web:Form,0,'Contact History Notes',,)
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_locpicklist_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_locfinallist_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_button:addselected_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_button:removeselected_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_gap_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  of self._nocolon('pickcontacthistorynotes_button:removeall_value')
    PickContactHistoryNotes(self,Net:Web:Div)
    self._Sendfooter()
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:SelectFaultCodes  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'selectfaultcodes'
  orof 'selectfaultcodes' & '_' & loc:parent
  orof 'select fault code'
  orof 'select fault code' & '_' & loc:parent
    Self.MakePage('SelectFaultCodes',Net:Web:Browse,Net:Login,'Select Fault Code',,)
    loc:Done = 1 ; Exit
  End ! Case
! - -  - - - - - - - - - - - - - -
Case:FormJobsInBatch  Routine
  Case loc:filename !SELF.GetPageName(Lower(p_FileName))
  of 'formjobsinbatch'
    Self.MakePage('FormJobsInBatch',Net:Web:Form,Net:Login,'Update MULDESPJ',,)
    loc:Done = 1 ; Exit
  End ! Case

p_web._UpdateFile PROCEDURE(FILE p_file)

ReturnValue          LONG,AUTO

RM      &RelationManager
Loc:Err Long(-1)

  CODE
    RM &= NetWebRelationManager(p_File)
    If NOT RM &= Null
      Loc:Err = RM.Update()
      Self.FileToSessionQueue(p_File,Net:AlsoValueQueue)
    End
    Return Loc:Err
  ReturnValue = PARENT._UpdateFile(p_file)
  RETURN ReturnValue

ViewChargeableParts  PROCEDURE  (NetWebServerWorker p_web)
tmp:partStatus       STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(PARTS)
                      Project(par:Record_Number)
                      Project(par:Part_Number)
                      Project(par:Description)
                      Project(par:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('ViewChargeableParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('ViewChargeableParts:NoForm')
      loc:NoForm = p_web.GetValue('ViewChargeableParts:NoForm')
      loc:FormName = p_web.GetValue('ViewChargeableParts:FormName')
    else
      loc:FormName = 'ViewChargeableParts_frm'
    End
    p_web.SSV('ViewChargeableParts:NoForm',loc:NoForm)
    p_web.SSV('ViewChargeableParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('ViewChargeableParts:NoForm')
    loc:FormName = p_web.GSV('ViewChargeableParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('ViewChargeableParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('ViewChargeableParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(PARTS,par:recordnumberkey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'PAR:PART_NUMBER') then p_web.SetValue('ViewChargeableParts_sort','1')
    ElsIf (loc:vorder = 'PAR:DESCRIPTION') then p_web.SetValue('ViewChargeableParts_sort','2')
    ElsIf (loc:vorder = 'PAR:QUANTITY') then p_web.SetValue('ViewChargeableParts_sort','3')
    ElsIf (loc:vorder = 'TMP:PARTSTATUS') then p_web.SetValue('ViewChargeableParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('ViewChargeableParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('ViewChargeableParts:LookupFrom','LookupFrom')
    p_web.StoreValue('ViewChargeableParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('ViewChargeableParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('ViewChargeableParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  !Override browse buttons
  p_web.site.insertButton.class = 'BrowseButtonsSmall'
  p_web.site.changeButton.class = 'BrowseButtonsSmall'
  p_web.site.deletebButton.class = 'BrowseButtonsSmall'
  p_web.site.insertButton.image = ''
  p_web.site.changeButton.image = ''
  p_web.site.deletebButton.image = ''
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('ViewChargeableParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('ViewChargeableParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(par:Part_Number)','-UPPER(par:Part_Number)')
    Loc:LocateField = 'par:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(par:Description)','-UPPER(par:Description)')
    Loc:LocateField = 'par:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'par:Quantity','-par:Quantity')
    Loc:LocateField = 'par:Quantity'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:partStatus','-tmp:partStatus')
    Loc:LocateField = 'tmp:partStatus'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('par:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('ViewChargeableParts_LocatorPic','@s30')
  Of upper('par:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('ViewChargeableParts_LocatorPic','@s30')
  Of upper('par:Quantity')
    loc:SortHeader = p_web.Translate('Qty')
    p_web.SetSessionValue('ViewChargeableParts_LocatorPic','@n8')
  Of upper('tmp:partStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('ViewChargeableParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('ViewChargeableParts:LookupFrom')
  End!Else
  loc:formaction = 'ViewChargeableParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="ViewChargeableParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="ViewChargeableParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('ViewChargeableParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="PARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="par:recordnumberkey"></input><13,10>'
  end
  If p_web.Translate('Chargeable Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Chargeable Parts',0)&'</span>'&CRLF
  End
  If clip('Chargeable Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewChargeableParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2ViewChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="ViewChargeableParts.locate(''Locator2ViewChargeableParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'ViewChargeableParts.cl(''ViewChargeableParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="ViewChargeableParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="ViewChargeableParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','ViewChargeableParts','Part Number',,,,'20%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('20%')&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','ViewChargeableParts','Description',,,,'40%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('40%')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','ViewChargeableParts','Qty',,,,'10%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('10%')&'">'&p_web.Translate('Qty')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','ViewChargeableParts','Status',,,,'20%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('20%')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('par:record_number',lower(Thisview{prop:order}),1,1) = 0 !and PARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'par:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('par:Record_Number'),p_web.GetValue('par:Record_Number'),p_web.GetSessionValue('par:Record_Number'))
      loc:FilterWas = 'par:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewChargeableParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('ViewChargeableParts_Filter')
    p_web.SetSessionValue('ViewChargeableParts_FirstValue','')
    p_web.SetSessionValue('ViewChargeableParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,PARTS,par:recordnumberkey,loc:PageRows,'ViewChargeableParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If PARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(PARTS,loc:firstvalue)
              Reset(ThisView,PARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If PARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(PARTS,loc:lastvalue)
            Reset(ThisView,PARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:PartStatus = getPartStatus('C')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(par:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Chargeable Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'ViewChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'ViewChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'ViewChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'ViewChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewChargeableParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('ViewChargeableParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('ViewChargeableParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1ViewChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="ViewChargeableParts.locate(''Locator1ViewChargeableParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'ViewChargeableParts.cl(''ViewChargeableParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('ViewChargeableParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('ViewChargeableParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'ViewChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'ViewChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'ViewChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'ViewChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = par:Record_Number
    p_web._thisrow = p_web._nocolon('par:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('ViewChargeableParts:LookupField')) = par:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((par:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="ViewChargeableParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If PARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(PARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If PARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(PARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','par:Record_Number',clip(loc:field),,loc:checked,,,'onclick="ViewChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','par:Record_Number',clip(loc:field),,'checked',,,'onclick="ViewChargeableParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('40%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('10%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::par:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:partStatus = 'Requested'
              packet = clip(packet) & '<td class="'&clip('GreenRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Picked'
              packet = clip(packet) & '<td class="'&clip('BlueRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'On Order'
              packet = clip(packet) & '<td class="'&clip('PurpleRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&clip('PinkRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&clip('OrangeRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:partStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="ViewChargeableParts.omv(this);" onMouseOut="ViewChargeableParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var ViewChargeableParts=new browseTable(''ViewChargeableParts'','''&clip(loc:formname)&''','''&p_web._jsok('par:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('par:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'ViewChargeableParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'ViewChargeableParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2ViewChargeableParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1ViewChargeableParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1ViewChargeableParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2ViewChargeableParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  Clear(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('par:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::par:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Part_Number_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::par:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Description_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::par:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('par:Quantity_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(par:Quantity,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:partStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:partStatus = 'Requested'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'GreenRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Picked'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'BlueRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'On Order'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'PurpleRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Picking'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'PinkRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Return'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,'OrangeRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&par:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:partStatus,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = par:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('par:Record_Number',par:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('par:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('par:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('par:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ViewWarrantyParts    PROCEDURE  (NetWebServerWorker p_web)
tmp:partStatus       STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(WARPARTS)
                      Project(wpr:Record_Number)
                      Project(wpr:Part_Number)
                      Project(wpr:Description)
                      Project(wpr:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('ViewWarrantyParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('ViewWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('ViewWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('ViewWarrantyParts:FormName')
    else
      loc:FormName = 'ViewWarrantyParts_frm'
    End
    p_web.SSV('ViewWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('ViewWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('ViewWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('ViewWarrantyParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('ViewWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('ViewWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(WARPARTS,wpr:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'WPR:PART_NUMBER') then p_web.SetValue('ViewWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'WPR:DESCRIPTION') then p_web.SetValue('ViewWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'WPR:QUANTITY') then p_web.SetValue('ViewWarrantyParts_sort','3')
    ElsIf (loc:vorder = 'TMP:PARTSTATUS') then p_web.SetValue('ViewWarrantyParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('ViewWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('ViewWarrantyParts:LookupFrom','LookupFrom')
    p_web.StoreValue('ViewWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('ViewWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('ViewWarrantyParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  !Override browse buttons
  p_web.site.insertButton.class = 'BrowseButtonsSmall'
  p_web.site.changeButton.class = 'BrowseButtonsSmall'
  p_web.site.deletebButton.class = 'BrowseButtonsSmall'
  p_web.site.insertButton.image = ''
  p_web.site.changeButton.image = ''
  p_web.site.deletebButton.image = ''
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('ViewWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('ViewWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Part_Number)','-UPPER(wpr:Part_Number)')
    Loc:LocateField = 'wpr:Part_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Description)','-UPPER(wpr:Description)')
    Loc:LocateField = 'wpr:Description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'wpr:Quantity','-wpr:Quantity')
    Loc:LocateField = 'wpr:Quantity'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:partStatus','-tmp:partStatus')
    Loc:LocateField = 'tmp:partStatus'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('wpr:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('ViewWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('ViewWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Quantity')
    loc:SortHeader = p_web.Translate('Qty')
    p_web.SetSessionValue('ViewWarrantyParts_LocatorPic','@n8')
  Of upper('tmp:partStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('ViewWarrantyParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('ViewWarrantyParts:LookupFrom')
  End!Else
  loc:formaction = 'ViewWarrantyParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="ViewWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="ViewWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('ViewWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="WARPARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="wpr:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Warranty Parts',0)&'</span>'&CRLF
  End
  If clip('Warranty Parts') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2ViewWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="ViewWarrantyParts.locate(''Locator2ViewWarrantyParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'ViewWarrantyParts.cl(''ViewWarrantyParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="ViewWarrantyParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="ViewWarrantyParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','ViewWarrantyParts','Part Number',,,,'20%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('20%')&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','ViewWarrantyParts','Description',,,,'40%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('40%')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','ViewWarrantyParts','Qty',,,,'10%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('10%')&'">'&p_web.Translate('Qty')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','ViewWarrantyParts','Status',,,,'20%',1)
        Else
          packet = clip(packet) & '<th width="'&clip('20%')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('wpr:record_number',lower(Thisview{prop:order}),1,1) = 0 !and WARPARTS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'wpr:Record_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('wpr:Record_Number'),p_web.GetValue('wpr:Record_Number'),p_web.GetSessionValue('wpr:Record_Number'))
      loc:FilterWas = 'wpr:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('ViewWarrantyParts_Filter')
    p_web.SetSessionValue('ViewWarrantyParts_FirstValue','')
    p_web.SetSessionValue('ViewWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,WARPARTS,wpr:RecordNumberKey,loc:PageRows,'ViewWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If WARPARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(WARPARTS,loc:firstvalue)
              Reset(ThisView,WARPARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If WARPARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(WARPARTS,loc:lastvalue)
            Reset(ThisView,WARPARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:partStatus = getPartStatus('W')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Warranty Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'ViewWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'ViewWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'ViewWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'ViewWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'ViewWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('ViewWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('ViewWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1ViewWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="ViewWarrantyParts.locate(''Locator1ViewWarrantyParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'ViewWarrantyParts.cl(''ViewWarrantyParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('ViewWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('ViewWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'ViewWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'ViewWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'ViewWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'ViewWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = wpr:Record_Number
    p_web._thisrow = p_web._nocolon('wpr:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('ViewWarrantyParts:LookupField')) = wpr:Record_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((wpr:Record_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="ViewWarrantyParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If WARPARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(WARPARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If WARPARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(WARPARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wpr:Record_Number',clip(loc:field),,loc:checked,,,'onclick="ViewWarrantyParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wpr:Record_Number',clip(loc:field),,'checked',,,'onclick="ViewWarrantyParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('40%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip('10%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wpr:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:partStatus = 'Requested'
              packet = clip(packet) & '<td class="'&clip('GreenRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Picked'
              packet = clip(packet) & '<td class="'&clip('BlueRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'On Order'
              packet = clip(packet) & '<td class="'&clip('PurpleRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&clip('PinkRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif tmp:partStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&clip('OrangeRegular')&'" width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td width="'&clip('20%')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:partStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="ViewWarrantyParts.omv(this);" onMouseOut="ViewWarrantyParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var ViewWarrantyParts=new browseTable(''ViewWarrantyParts'','''&clip(loc:formname)&''','''&p_web._jsok('wpr:Record_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('wpr:Record_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'ViewWarrantyParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'ViewWarrantyParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2ViewWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1ViewWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1ViewWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2ViewWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(WARPARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  Clear(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('wpr:Record_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::wpr:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Part_Number_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Description_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wpr:Quantity_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wpr:Quantity,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:partStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:partStatus = 'Requested'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&wpr:Record_Number,'GreenRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Picked'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&wpr:Record_Number,'BlueRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'On Order'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&wpr:Record_Number,'PurpleRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Picking'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&wpr:Record_Number,'PinkRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    elsif tmp:partStatus = 'Awaiting Return'
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&wpr:Record_Number,'OrangeRegular',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:partStatus,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:partStatus_'&wpr:Record_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:partStatus,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = wpr:Record_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('wpr:Record_Number',wpr:Record_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('wpr:Record_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wpr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wpr:Record_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ClearUpdateJobVariables PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ClearUpdateJobVariables -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
FilesOpened     Long
JOBACC::State  USHORT
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('ClearUpdateJobVariables')
! Clear Variables
    p_web.SSV('FirstTime',1)
    p_web.SSV('Job:ViewOnly',0)
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBACC)
     FilesOpened = False
  END
