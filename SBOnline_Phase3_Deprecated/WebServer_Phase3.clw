   PROGRAM


NetTalk:TemplateVersion equate('4.57')
ActivateNetTalk   EQUATE(1)
  include('NetCrit.inc'),once
  include('NetMap.inc'),once
  include('NetAll.inc'),once
  include('NetTalk.inc'),once
  include('NetSimp.inc'),once
  include('NetWww.inc'),once
  include('NetWeb.inc'),once
  include('NetEmail.inc'),once
  include('NetFile.Inc'),once
Draw:TemplateVersion equate('3.02')
CapesoftMessageBox:TemplateVersion equate('2.15')

   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
  include('cwsynchc.inc'),once  ! added by NetTalk
    include('draw.inc'),once
    include('drawheader.inc'),once

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    INCLUDE('VodacomClass.inc','Declarations')
   INCLUDE('iQXML.INC','Equates')
   INCLUDE('PrnProp.clw'),ONCE

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('WEBSERVER_PHASE3_BC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('WEBSERVER_PHASE3001.CLW')
PageFooter             FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
Main                   PROCEDURE   !Don't Forget To Update Version Number
Waybill                PROCEDURE(<NetWebServerWorker p_web>)   !Report the JOBS File
InvoiceSubAccounts     FUNCTION(String),Byte   !
VATRate_OLD            FUNCTION(String,String),Real   !
JobCard                PROCEDURE(<NetWebServerWorker p_web>)   !
TotalPrice             PROCEDURE(NetWebServerWorker p_web)   !
Estimate               PROCEDURE(<NetWebServerWorker p_web>)   !Estimate
SentToHub              PROCEDURE(NetWebServerWorker p_web)   !
     END
     MODULE('WEBSERVER_PHASE3002.CLW')
GetStatus              PROCEDURE(NetWebServerWorker p_web)   !
AddToAudit             PROCEDURE(NetWebServerWorker p_web)   !
DespatchNote           PROCEDURE(<NetWebServerWorker p_web>)   !
CheckLength            FUNCTION(String,String,String),Byte   !
PassMobileNumberFormat FUNCTION(String f:MobileNumber),Byte   !
     END
     MODULE('WEBSERVER_PHASE3004.CLW')
OBFValidation          FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
LookupSuburbs          PROCEDURE(NetWebServerWorker p_Web)   !
LookupRRCAccounts      PROCEDURE(NetWebServerWorker p_Web)   !
LookupGenericAccounts  PROCEDURE(NetWebServerWorker p_Web)   !
LocationChange         PROCEDURE(NetWebServerWorker p_web)   !
InsertJob_Finished     FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
     END
     MODULE('WEBSERVER_PHASE3005.CLW')
JobReceipt             PROCEDURE(<NetWebServerWorker p_web>)   !
CreateTestRecord       FUNCTION(String f:Path),Byte   !
CID_XML                PROCEDURE(String f:Mobile,String f:Account,Byte f:Type)   !
AddEmailSMS            PROCEDURE(Long f:JobNumber, String f:AccountNumber, String f:MessageType, String f:SMSEmail, String f:MobileNumber, String f:EmailAddress, Real f:Cost, String f:EstimatePath)   !
     END
     MODULE('WEBSERVER_PHASE3006.CLW')
IsUnitLiquidDamaged    FUNCTION(String f:ESN, Long f:JobNumber,<Byte ignoreARCCheck>),Byte   !
IsOneYearWarranty      FUNCTION(STRING manufacturer,STRING modelNumber),BYTE   !
JobInUse               FUNCTION(Long),Byte   !
HubOutOfRegion         FUNCTION(String,String),Byte   !
     END
     MODULE('WEBSERVER_PHASE3007.CLW')
BannerEngineeringDetails FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
MessageQuestion        PROCEDURE(NetWebServerWorker p_web)   !
     END
     MODULE('WEBSERVER_PHASE3009.CLW')
SecurityCheckFailed    FUNCTION(String f:Password,String f:Area),Byte   !
MSNRequired            FUNCTION(String),Byte   !
PickLoanUnit           FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
     END
     MODULE('WEBSERVER_PHASE3010.CLW')
PickExchangeUnit       FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
PendingJob             PROCEDURE(NetWebServerWorker p_web)   !
JobPricingRoutine      PROCEDURE(NetWebServerWorker p_web)   !
     END
     MODULE('WEBSERVER_PHASE3011.CLW')
LoanAttachedToJob      PROCEDURE(NetWebServerWorker p_web)   !
BannerVodacom          FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
     END
     MODULE('WEBSERVER_PHASE3012.CLW')
ProductCodeRequired    FUNCTION(String),Byte   !
     END
     MODULE('WEBSERVER_PHASE3013.CLW')
CheckFaultFormat       FUNCTION(String,String),Byte   !
     END
     MODULE('WEBSERVER_PHASE3014.CLW')
VodacomSingleInvoice   PROCEDURE(<NetWebServerWorker p_web>)   !Single Invoice
InvoiceNote            PROCEDURE(<NetWebServerWorker p_web>)   !
Is48HourOrderProcessed FUNCTION(String,String),Byte   !
Is48HourOrderCreated   FUNCTION(String,String),Byte   !
ExchangeOrder          PROCEDURE(<NetWebServerWorker p_web>)   !Report the EXCHOR48 File
BannerNewJobBooking    FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
     END
     MODULE('WEBSERVER_PHASE3016.CLW')
Line500_XML            PROCEDURE(String)   !
     END
     MODULE('WEBSERVER_PHASE3017.CLW')
ForceUnitType          FUNCTION(String),Byte   !
ForceTransitType       FUNCTION(String),Byte   !
ForceRepairType        FUNCTION(String),Byte   !
ForceOrderNumber       FUNCTION(String),Byte   !
ForceNetwork           FUNCTION(String),Byte   !
ForceMSN               FUNCTION(String,String),Byte   !
ForceModelNumber       FUNCTION(String),Byte   !
ForceMobileNumber      FUNCTION(String),Byte   !
ForceLocation          FUNCTION(String,String,String),Byte   !
     END
     MODULE('WEBSERVER_PHASE3018.CLW')
ForceInvoiceText       FUNCTION(String),Byte   !
ForceIncomingCourier   FUNCTION(String),Byte   !
ForceIMEI              FUNCTION(String),Byte   !
ForceFaultDescription  FUNCTION(String),Byte   !
ForceFaultCodes        FUNCTION(String,String,String,String,String,String,String,String),Byte   !
ForceDOP               FUNCTION(String,String,String,String),Byte   !
ForceDeliveryPostcode  FUNCTION(String),Byte   !
ForceCustomerName      FUNCTION(String,String),Byte   !
ForceCourier           FUNCTION(String),Byte   !
ForceColour            FUNCTION(String),Byte   !
     END
     MODULE('WEBSERVER_PHASE3019.CLW')
ForceAuthorityNumber   FUNCTION(String),Byte   !
ForceAccessories       FUNCTION(String),Byte   !
BrowseAuditTrail       PROCEDURE(NetWebServerWorker p_Web)   !
     END
     MODULE('WEBSERVER_PHASE3020.CLW')
BrowseEstimateParts    PROCEDURE(NetWebServerWorker p_Web)   !
BrowseWarrantyParts    PROCEDURE(NetWebServerWorker p_Web)   !
     END
     MODULE('WEBSERVER_PHASE3021.CLW')
BrowseChargeableParts  PROCEDURE(NetWebServerWorker p_Web)   !
MessageAlert           PROCEDURE(NetWebServerWorker p_web)   !
     END
     MODULE('WEBSERVER_PHASE3022.CLW')
IndividualDespatch     FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
ClearJobVariables      PROCEDURE(NetWebServerWorker p_web)   !
LoginForm              FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !Defaults Set Here
DoCaps                 FUNCTION(String),String   !
BannerBlank            FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
ViewChargeableParts    PROCEDURE(NetWebServerWorker p_Web)   !
ClearUpdateJobVariables PROCEDURE(NetWebServerWorker p_web)   !
     END
     MODULE('WEBSERVER_PHASE3023.CLW')
PopupMessage           PROCEDURE(NetWebServerWorker p_web)   !
PageHeader             PROCEDURE(NetWebServerWorker p_web)   !
PreviousIMEI           FUNCTION(String),Byte   !
SetBottom              PROCEDURE(NetWebServerWorker p_web)   !
GoToBottom             PROCEDURE(NetWebServerWorker p_web)   !
     END
     MODULE('WEBSERVER_PHASE3024.CLW')
BannerBrowseJobs       FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
BannerMainMenu         FUNCTION(NetWebServerWorker p_web,long p_action=0),long,proc   !
SetHubRepair           PROCEDURE(NetWebServerWorker p_web)   !
IsThisModelAlternative FUNCTION(String,String),Byte   !
IsDateFormatInvalid    FUNCTION(String),Byte   !
BouncerHistory         PROCEDURE(<NetWebServerWorker p_web>)   !
PushPack               PROCEDURE(NetWebServerWorker p_web, string p_packet)   !
     END
     MODULE('WEBSERVER_PHASE3025.CLW')
ds_Stop                PROCEDURE(<string StopText>)   !
ds_Halt                PROCEDURE(UNSIGNED Level=0,<STRING HaltText>)   !
ds_Message             FUNCTION(STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE),UNSIGNED,PROC   !
RunDOS                 FUNCTION(string, byte=false),long   !
ForcePostcode          FUNCTION(String),Byte   !
CustomerNameRequired   FUNCTION(String),Byte   !
Pricing_Routine        PROCEDURE(String,*String,*String,*String,*String,*String,*String,*String,*String)   !
DateCodeValidation     FUNCTION(String,String,Date),Byte   !
SendEmail              PROCEDURE(string pEmailFrom, string pEmailTo, string pEmailSubject, string pEmailCC, string pEmailBcc, string pEmailFileList, string pEmailMessageText)   !
     END
     MODULE('WEBSERVER_PHASE3026.CLW')
ViewEstimateParts      PROCEDURE(NetWebServerWorker p_Web)   !
MenuTools              PROCEDURE(NetWebServerWorker p_web)   !
GetVATRate             FUNCTION(String,String),Real   !
     END
       Module('WebSe_nw.clw')
          NetWebRelationManager (FILE p_file),*RelationManager
          NetWebFileNamed (string p_file),*File
          NetWebSetSessionPics (NetWebServerWorker p_web, FILE p_File)
          NetWebSetPics (NetWebServerWorker p_web, FILE p_File)
       End
         Module('winapi')
           GetExitCodeProcess(unsigned, ulong),signed,pascal
           CreateProcess(ulong, ulong, ulong, ulong, signed, ulong, ulong, ulong, ulong, ulong ),signed,raw,pascal,name('CreateProcessA')
           Sleep(ulong),pascal,raw,name('Sleep')
           GetLastError(),ulong,pascal,raw
         End
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
     MODULE('SEQLIB32.LIB')
       SEQUENCE2(BYTE,BYTE,*CSTRING,*CSTRING),PASCAL,RAW
       !CHECK(BYTE,BYTE,*?,*?),PASCAL,NAME('CHECK'),RAW
     END
     
     
     Include('clib.clw'),ONCE ! To Create Folders
     INCLUDE('VodacomGlobalMaps.inc')
        INCLUDE('iQXML.INC','Modules')
          FillCpcsIniStrings(STRING,STRING,STRING,STRING)
          MODULE('WebSe_SF.CLW')
            CheckOpen(FILE File,<BYTE OverrideCreate>,<BYTE OverrideOpenMode>)
            StandardWarning(LONG WarningID),LONG,PROC
            StandardWarning(LONG WarningID,STRING WarningText1),LONG,PROC
            StandardWarning(LONG WarningID,STRING WarningText1,STRING WarningText2),LONG,PROC
            RISaveError
          END
       MODULE('CPCxxL32.LIB')
         Wmf2Ascii(QUEUE, STRING, LONG),NAME('Wmf2Ascii')
         PaperSupport(SHORT,<QUEUE>),STRING,NAME('PaperSupport')
         PrintPreview(QUEUE,SHORT,<STRING>,REPORT,LONG,<STRING>,<LONG>,<STRING>,<STRING>,<STRING>,<*?>,<BYTE>,<STRING>,<*?>,<BYTE>,<STRING>,<*?>,<BYTE>,<STRING>,<STRING>,<STRING>,<*QUEUE>),LONG,NAME('PrintPreview')
         Supported(LONG),SHORT,NAME('Supported')
         HandleCopies(QUEUE,LONG),NAME('HandleCopies')
         ShowPrinterStatus(STRING,SHORT,<STRING>),NAME('ShowPrinterStatus')
         AssgnPgOfPg(Queue,<STRING>,<BYTE>,<STRING>,<LONG>),NAME('AssgnPgOfPg')
         SaveClipToFile(<STRING>),STRING,NAME('SaveClipToFile')
         SetPrinterDraftMode(),LONG,NAME('SetPrinterDraftMode')
       END
   END

glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:DateModify       DATE
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:WebJob           BYTE(0)
glo:ARCAccountNumber STRING(30)
glo:ARCSiteLocation  STRING(30)
glo:FaultCodeGroup   GROUP,PRE(glo)
FaultCode1             STRING(30)
FaultCode2             STRING(30)
FaultCode3             STRING(30)
FaultCode4             STRING(30)
FaultCode5             STRING(30)
FaultCode6             STRING(30)
FaultCode7             STRING(30)
FaultCode8             STRING(30)
FaultCode9             STRING(30)
FaultCode10            STRING(255)
FaultCode11            STRING(255)
FaultCode12            STRING(255)
FaultCode13            STRING(30)
FaultCode14            STRING(30)
FaultCode15            STRING(30)
FaultCode16            STRING(30)
FaultCode17            STRING(30)
FaultCode18            STRING(30)
FaultCode19            STRING(30)
FaultCode20            STRING(30)
                     END
glo:IPAddress        STRING(30)
glo:HostName         STRING(60)
glo:ReportName       STRING(255)
glo:ExportReport     BYTE(0)
glo:EstimateReportName STRING(255)
glo:ReportCopies     BYTE
glo:Vetting          BYTE(0)
glo:TagFile          STRING(255)
glo:sbo_outfaultparts STRING(255)
glo:UserAccountNumber STRING(30)
glo:UserCode         STRING(3)
glo:Location         STRING(30)
glo:EDI_Reason       STRING(60)
glo:Insert_Global    STRING(3)
glo:sbo_outparts     STRING(255)
glo:StockReceiveTmp  STRING(255)
glo:SBO_GenericFile  STRING(255)
glo:file_name        STRING(255)
Glo:iQErrorMessageText STRING(128)
glo:WebserverPort    LONG
glo:DataPath         STRING(255)
glo:LocalPath        STRING(255)
glo:VersionNumber    STRING(30)
glo:FileName         STRING(255)
glo:RepairTypes      STRING(255)
glo:file_name2       STRING(255)
glo:file_name3       STRING(255)
glo:file_name4       STRING(255)
glo:tradetmp         STRING(255)
glo:Account_Number_Web STRING(30)
Glo:Default_Site_Location STRING(30)
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

BHQFileList         QUEUE,TYPE
qFilename               STRING(255)
                    END
    Map
BHStripReplace           Procedure(String func:String,String func:Strip,String func:Replace),String
BHStripNonAlphaNum           Procedure(String func:String,String func:Replace),String
BHStripForFilename           Procedure(String func:String),String
BHGetFileFromPath			Procedure(String fullPath),String
BHGetPathFromFile			Procedure(String fullPath,<Byte removeSlash>),String
BHGetFileNoExtension        Procedure(String fullPath),String
BHPutFileIntoQueue          Procedure(STRING fullPath,*bhQFileList Q)
BHAddBackSlash              Procedure(STRING fullPath),STRING
BHRunDOS                    Procedure(String f:Command,<Byte f:Wait>,<Byte f:NoTimeOut>),Long
    End
 MAP
BHDebugMessage		Procedure(String fMessage)
BHAddToDebugLog		Procedure(String fMessage,<String fOverrideFilename>)
 END
BHDebugFileName		String(255),STATIC
BHDebugExportFile   File,Driver('ASCII'),Pre(dbg),Name(BHDebugFileName),Create,Bindable,Thread
Record                  Record
DebugLine                   STRING(1000)
      End
     End
CPCSStartUpPrintDevice      CSTRING(128)    ! chgd to 128 (from 64) on 3/6/2008

CPCS:ProgWinTitlePrvw   CSTRING(64)
CPCS:ProgWinTitlePrnt   CSTRING(64)
CPCS:ProgWinPctText     CSTRING(64)
CPCS:ProgWinRecText     CSTRING(64)
CPCS:ProgWinUsrText     CSTRING(64)
CPCS:PrtrDlgTitle       CSTRING(64)
CPCS:AskPrvwDlgTitle    CSTRING(64)
CPCS:AskPrvwDlgText     CSTRING(64)
CPCS:AreYouSureTitle    CSTRING(64)
CPCS:AreYouSureText     CSTRING(64)
CPCS:PrvwPartialTitle   CSTRING(64)
CPCS:PrvwPartialText    CSTRING(128)
CPCS:NoDfltPrtrTitle    CSTRING(64)
CPCS:NoDfltPrtrText     CSTRING(255)
CPCS:NthgToPrvwTitle    CSTRING(64)
CPCS:NthgToPrvwText     CSTRING(64)
CPCS:NthgToPrntTitle    CSTRING(64)
CPCS:NthgToPrntText     CSTRING(64)
CPCS:AsciiOutTitle      CSTRING(64)
CPCS:AsciiOutmasks      CSTRING(64)
CPCS:DynLblErrTitle     CSTRING(64)
CPCS:DynLblErrText1     CSTRING(64)
CPCS:DynLblErrText2     CSTRING(64)

CPCS:ButtonYesNoCancel  CSTRING(64)
CPCS:ButtonYesNo        CSTRING(64)
CPCS:ButtonYesNoIgnore  CSTRING(64)
CPCS:ButtonYes          CSTRING(64)
CPCS:ButtonOK           CSTRING(64)

SaveErrorCode               LONG
SaveError                   CSTRING(255)
SaveFileErrorCode           LONG
SaveFileError               CSTRING(255)

Warn:InvalidFile         EQUATE (1)
Warn:InvalidKey          EQUATE (2)
Warn:RebuildError        EQUATE (3)
Warn:CreateError         EQUATE (4)
Warn:CreateOpenError     EQUATE (5)
Warn:ProcedureToDo       EQUATE (6)
Warn:BadKeyedRec         EQUATE (7)
Warn:OutOfRangeHigh      EQUATE (8)
Warn:OutOfRangeLow       EQUATE (9)
Warn:OutOfRange          EQUATE (10)
Warn:NotInFile           EQUATE (11)
Warn:RestrictUpdate      EQUATE (12)
Warn:RestrictDelete      EQUATE (13)
Warn:InsertError         EQUATE (14)
Warn:RIUpdateError       EQUATE (15)
Warn:UpdateError         EQUATE (16)
Warn:RIDeleteError       EQUATE (17)
Warn:DeleteError         EQUATE (18)
Warn:InsertDisabled      EQUATE (19)
Warn:UpdateDisabled      EQUATE (20)
Warn:DeleteDisabled      EQUATE (21)
Warn:NoCreate            EQUATE (22)
Warn:ConfirmCancel       EQUATE (23)
Warn:DuplicateKey        EQUATE (24)
Warn:AutoIncError        EQUATE (25)
Warn:FileLoadError       EQUATE (26)
Warn:ConfirmCancelLoad   EQUATE (27)
Warn:FileZeroLength      EQUATE (28)
Warn:EndOfAsciiQueue     EQUATE (29)
Warn:DiskError           EQUATE (30)
Warn:ProcessActionError  EQUATE (31)
Warn:StandardDelete      EQUATE (32)
Warn:SaveOnCancel        EQUATE (33)
Warn:LogoutError         EQUATE (34)
Warn:RecordFetchError    EQUATE (35)
Warn:ViewOpenError       EQUATE (36)
Warn:NewRecordAdded      EQUATE (37)
Warn:RIFormUpdateError   EQUATE (38)


!region File Declaration
STDCHRGE             FILE,DRIVER('Btrieve'),NAME('STDCHRGE.DAT'),PRE(sta),CREATE,BINDABLE,THREAD !Standard Charges    
Model_Number_Charge_Key  KEY(sta:Model_Number,sta:Charge_Type,sta:Unit_Type,sta:Repair_Type),NOCASE,PRIMARY !By Charge Type      
Charge_Type_Key          KEY(sta:Model_Number,sta:Charge_Type),DUP,NOCASE !By Charge Type      
Unit_Type_Key            KEY(sta:Model_Number,sta:Unit_Type),DUP,NOCASE !By Unit Type        
Repair_Type_Key          KEY(sta:Model_Number,sta:Repair_Type),DUP,NOCASE !By Repair Type      
Cost_Key                 KEY(sta:Model_Number,sta:Cost),DUP,NOCASE !By Cost             
Charge_Type_Only_Key     KEY(sta:Charge_Type),DUP,NOCASE   !                    
Repair_Type_Only_Key     KEY(sta:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(sta:Unit_Type),DUP,NOCASE     !                    
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
dummy                       BYTE                           !                    
WarrantyClaimRate           REAL                           !Warranty Claim Rate 
HandlingFee                 REAL                           !HandlingFee         
Exchange                    REAL                           !Exchange            
RRCRate                     REAL                           !R.R.C. Rate         
                         END
                     END                       

TagFile              FILE,DRIVER('TOPSPEED'),RECLAIM,OEM,NAME(glo:TagFile),PRE(tag),CREATE,BINDABLE,THREAD !Tag File For SB Online
keyTagged                KEY(tag:sessionID,tag:taggedValue),DUP,NOCASE,OPT !                    
keyID                    KEY(tag:taggedValue,tag:sessionID),DUP,NOCASE,OPT !                    
Record                   RECORD,PRE()
sessionID                   LONG                           !                    
taggedValue                 STRING(30)                     !                    
tagged                      BYTE                           !                    
                         END
                     END                       

STOFAULT             FILE,DRIVER('Btrieve'),OEM,NAME('STOFAULT.DAT'),PRE(stf),CREATE,BINDABLE,THREAD !Faulty Parts        
RecordNumberKey          KEY(stf:RecordNumber),NOCASE,PRIMARY !By Record Number    
PartNumberKey            KEY(stf:PartNumber),DUP,NOCASE    !By Part Number      
DescriptionKey           KEY(stf:Description),DUP,NOCASE   !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
PartNumber                  STRING(30)                     !Part Number         
Description                 STRING(30)                     !Description         
Quantity                    LONG                           !Quantity            
PurchaseCost                REAL                           !Purchase Cost       
ModelNumber                 STRING(30)                     !Model Number        
IMEI                        STRING(30)                     !I.M.E.I. Number     
Engineer                    STRING(3)                      !Engineer            
StoreUserCode               STRING(3)                      !Store Person        
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
PartType                    STRING(3)                      !Type                
                         END
                     END                       

JOBOUTFL             FILE,DRIVER('Btrieve'),OEM,NAME('JOBOUTFL.DAT'),PRE(joo),CREATE,BINDABLE,THREAD !Out Fault List      
RecordNumberKey          KEY(joo:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(joo:JobNumber,joo:FaultCode),DUP,NOCASE !By Code             
LevelKey                 KEY(joo:JobNumber,joo:Level),DUP,NOCASE !By Level            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JobNumber                   LONG                           !Job Number          
FaultCode                   STRING(30)                     !Fault Code          
Description                 STRING(255)                    !                    
Level                       LONG                           !Level               
                         END
                     END                       

ACCSTAT              FILE,DRIVER('Btrieve'),OEM,NAME('ACCSTAT.DAT'),PRE(acs),CREATE,BINDABLE,THREAD !Access Area Status Types
RecordNumberKey          KEY(acs:RecordNumber),NOCASE,PRIMARY !By Record Number    
StatusKey                KEY(acs:AccessArea,acs:Status),DUP,NOCASE !By Status           
StatusOnlyKey            KEY(acs:Status),DUP,NOCASE        !By Status           
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccessArea                  STRING(30)                     !AccessArea          
Status                      STRING(30)                     !Status              
                         END
                     END                       

EXCHAMF              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAMF.DAT'),PRE(emf),CREATE,BINDABLE,THREAD !                    
Audit_Number_Key         KEY(emf:Audit_Number),NOCASE,PRIMARY !                    
Secondary_Key            KEY(emf:Complete_Flag,emf:Audit_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Audit_Number                LONG                           !                    
Date                        DATE                           !                    
Time                        LONG                           !                    
User                        STRING(3)                      !                    
Status                      STRING(30)                     !                    
Site_location               STRING(30)                     !                    
Complete_Flag               BYTE                           !                    
                         END
                     END                       

AUDITE               FILE,DRIVER('Btrieve'),OEM,NAME('AUDITE.DAT'),PRE(aude),CREATE,BINDABLE,THREAD !AUDIT Extension     
RecordNumberKey          KEY(aude:RecordNumber),NOCASE,PRIMARY !Record Number Key   
RefNumberKey             KEY(aude:RefNumber),DUP,NOCASE    !By Ref Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to AUDIT Record Number
IPAddress                   STRING(30)                     !IP Address          
HostName                    STRING(60)                     !Host Name           
                         END
                     END                       

EXCHAUI              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAUI.DAT'),PRE(eau),CREATE,BINDABLE,THREAD !                    
Internal_No_Key          KEY(eau:Internal_No),NOCASE,PRIMARY !                    
Audit_Number_Key         KEY(eau:Audit_Number),DUP,NOCASE  !                    
Ref_Number_Key           KEY(eau:Ref_Number),DUP,NOCASE    !                    
Exch_Browse_Key          KEY(eau:Audit_Number,eau:Exists),DUP,NOCASE !                    
AuditIMEIKey             KEY(eau:Audit_Number,eau:New_IMEI),DUP,NOCASE !By I.M.E.I. Number  
Locate_IMEI_Key          KEY(eau:Audit_Number,eau:Site_Location,eau:IMEI_Number),DUP,NOCASE,OPT !                    
Main_Browse_Key          KEY(eau:Audit_Number,eau:Confirmed,eau:Site_Location,eau:Stock_Type,eau:Shelf_Location,eau:Internal_No),DUP,NOCASE !                    
Record                   RECORD,PRE()
Internal_No                 LONG                           !                    
Site_Location               STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Location                    STRING(30)                     !                    
Audit_Number                LONG                           !                    
Ref_Number                  LONG                           !Unit Number         
Exists                      BYTE                           !                    
New_IMEI                    BYTE                           !                    
IMEI_Number                 STRING(20)                     !                    
Confirmed                   BYTE                           !Confirmed           
Stock_Type                  STRING(30)                     !                    
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSENG.DAT'),PRE(joe),CREATE,BINDABLE,THREAD !Engineers Attached To Jobs
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY !By Record Number    
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE !By Date             
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE !By Date             
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE !By Date             
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE !By Date             
UserCodeStatusKey        KEY(joe:UserCode,joe:StatusDate,joe:Status),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JobNumber                   LONG                           !Job Number          
UserCode                    STRING(3)                      !User Code           
DateAllocated               DATE                           !Date Allocated      
TimeAllocated               STRING(20)                     !Time Allocated      
AllocatedBy                 STRING(3)                      !Allocated By        
EngSkillLevel               LONG                           !Engineer Skill Level
JobSkillLevel               STRING(30)                     !Job Skill Level     
Status                      STRING(30)                     !Status              
StatusDate                  DATE                           !Status Date         
StatusTime                  TIME                           !Status Time         
                         END
                     END                       

MULDESPJ             FILE,DRIVER('Btrieve'),OEM,NAME('MULDESPJ.DAT'),PRE(mulj),CREATE,BINDABLE,THREAD !Multiple Despatch Jobs
RecordNumberKey          KEY(mulj:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(mulj:RefNumber,mulj:JobNumber),DUP,NOCASE !By Job Number       
CurrentJobKey            KEY(mulj:RefNumber,mulj:Current,mulj:JobNumber),DUP,NOCASE !By Job Number       
JobNumberOnlyKey         KEY(mulj:JobNumber),DUP,NOCASE    !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !RefNumber           
JobNumber                   LONG                           !Job Number          
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
MSN                         STRING(30)                     !M.S.N.              
AccountNumber               STRING(30)                     !AccountNumber       
Courier                     STRING(30)                     !Courier             
Current                     BYTE                           !Current             
SecurityPackNumber          STRING(30)                     !Security Pack Number
                         END
                     END                       

MULDESP              FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(muld),CREATE,BINDABLE,THREAD !Multiple Despatch File
RecordNumberKey          KEY(muld:RecordNumber),NOCASE,PRIMARY !By Record Number    
BatchNumberKey           KEY(muld:BatchNumber),DUP,NOCASE  !By Batch Number     
AccountNumberKey         KEY(muld:AccountNumber),DUP,NOCASE !By Account Number   
CourierKey               KEY(muld:Courier),DUP,NOCASE      !By Courier          
HeadBatchNumberKey       KEY(muld:HeadAccountNumber,muld:BatchNumber),DUP,NOCASE !By Batch Number     
HeadAccountKey           KEY(muld:HeadAccountNumber,muld:AccountNumber),DUP,NOCASE !By Account Number   
HeadCourierKey           KEY(muld:HeadAccountNumber,muld:Courier),DUP,NOCASE !By Courier          
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
BatchNumber                 STRING(30)                     !Batch Number        
AccountNumber               STRING(30)                     !Account Number      
Courier                     STRING(30)                     !Courier             
BatchTotal                  LONG                           !Total In Batch      
BatchType                   STRING(3)                      !Batch Type          
HeadAccountNumber           STRING(30)                     !Head Account Number 
                         END
                     END                       

STMASAUD             FILE,DRIVER('Btrieve'),NAME('STMASAUD.DAT'),PRE(stom),CREATE,BINDABLE,THREAD !Stock Master Audit  
AutoIncrement_Key        KEY(-stom:Audit_No),NOCASE,PRIMARY !                    
Compeleted_Key           KEY(stom:Complete),DUP,NOCASE     !                    
Sent_Key                 KEY(stom:Send),DUP,NOCASE         !                    
Record                   RECORD,PRE()
Audit_No                    LONG                           !                    
branch                      STRING(30)                     !                    
branch_id                   LONG                           !                    
Audit_Date                  LONG                           !                    
Audit_Time                  LONG                           !                    
End_Date                    LONG                           !                    
End_Time                    LONG                           !                    
Audit_User                  STRING(3)                      !                    
Complete                    STRING(1)                      !                    
Send                        STRING(1)                      !                    
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTATS.DAT'),PRE(aus),CREATE,BINDABLE,THREAD !Audit For Status Changes
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE !By Date Changed     
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE !By New Status       
StatusTimeKey            KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged),DUP,NOCASE !By Time             
StatusDateKey            KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged),DUP,NOCASE !By Time             
StatusDateRecordKey      KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE !By Record Number    
StatusTypeRecordKey      KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE !By Record Number    
RefRecordNumberKey       KEY(aus:RefNumber,aus:RecordNumber),DUP,NOCASE !By Record Number    
RefDateRecordKey         KEY(aus:RefNumber,aus:DateChanged,aus:RecordNumber),DUP,NOCASE !By Record Number    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Job Number          
Type                        STRING(3)                      !Type                
DateChanged                 DATE                           !Date Changed        
TimeChanged                 TIME                           !Time Changed        
OldStatus                   STRING(30)                     !Old Status          
NewStatus                   STRING(30)                     !New Status          
UserCode                    STRING(3)                      !User Code           
                         END
                     END                       

NETWORKS             FILE,DRIVER('Btrieve'),OEM,NAME('NETWORKS.DAT'),PRE(net),CREATE,BINDABLE,THREAD !Networks            
RecordNumberKey          KEY(net:RecordNumber),NOCASE,PRIMARY !By Record Number    
NetworkKey               KEY(net:Network),NOCASE           !By Network          
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Network                     STRING(30)                     !Network             
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,NAME('JOBTHIRD.DAT'),PRE(jot),CREATE,BINDABLE,THREAD !Job Third Party Despatched
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE     !By Ref Number       
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE       !By Outgoing IMEI Number
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE        !By Incoming IMEI Number
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE !By Outgoing Date    
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE !                    
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE  !By Original I.M.E.I. No
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
RefNumber                   LONG                           !Link To Job's File  
OriginalIMEI                STRING(30)                     !Original IMEI Number
OutIMEI                     STRING(30)                     !Outgoing I.M.E.I. Number
InIMEI                      STRING(30)                     !Incoming I.M.E.I. Number
DateOut                     DATE                           !                    
DateDespatched              DATE                           !Date Despatched     
DateIn                      DATE                           !                    
ThirdPartyNumber            LONG                           !Link To Third Party Entry
OriginalMSN                 STRING(30)                     !Original MSN        
OutMSN                      STRING(30)                     !Outgoing M.S.N.     
InMSN                       STRING(30)                     !Incoming M.S.N.     
                         END
                     END                       

LOCATLOG             FILE,DRIVER('Btrieve'),OEM,NAME('LOCATLOG.DAT'),PRE(lot),CREATE,BINDABLE,THREAD !Location Change Log 
RecordNumberKey          KEY(lot:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(lot:RefNumber,lot:TheDate),DUP,NOCASE !By Date             
NewLocationKey           KEY(lot:RefNumber,lot:NewLocation),DUP,NOCASE !By Location         
DateNewLocationKey       KEY(lot:NewLocation,lot:TheDate,lot:RefNumber),DUP,NOCASE !By New Location     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
TheDate                     DATE                           !Date                
TheTime                     TIME                           !The Time            
UserCode                    STRING(30)                     !User Code           
PreviousLocation            STRING(30)                     !Previous Location   
NewLocation                 STRING(30)                     !New Location        
                         END
                     END                       

PRODCODE             FILE,DRIVER('Btrieve'),OEM,NAME('PRODCODE.DAT'),PRE(prd),CREATE,BINDABLE,THREAD !Handset Part Numbers
RecordNumberKey          KEY(prd:RecordNumber),NOCASE,PRIMARY !By Record Number    
ProductCodeKey           KEY(prd:ProductCode),DUP,NOCASE   !By Product Code     
ModelProductKey          KEY(prd:ModelNumber,prd:ProductCode),NOCASE !By Product Code     
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
ModelNumber                 STRING(30)                     !                    
ProductCode                 STRING(30)                     !                    
HandsetReplacementValue     REAL                           !Handset Replacement Value
                         END
                     END                       

ESNMODAL             FILE,DRIVER('Btrieve'),OEM,NAME('ESNMODAL.DAT'),PRE(esa),CREATE,BINDABLE,THREAD !Alternative TAC Codes
RecordNumberKey          KEY(esa:RecordNumber),NOCASE,PRIMARY !By Record Number    
TACCodeKey               KEY(esa:RefNumber,esa:TacCode),DUP,NOCASE !By TAC Code         
TacModelKey              KEY(esa:RefNumber,esa:TacCode,esa:ModelNumber),DUP,NOCASE !By TacCode          
RefModelNumberKey        KEY(esa:RefNumber,esa:ModelNumber),DUP,NOCASE !By Model Number     
ModelNumberOnlyKey       KEY(esa:ModelNumber),DUP,NOCASE   !By Model Number     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To ESNMODEL    
TacCode                     STRING(8)                      !TAC Code            
ModelNumber                 STRING(30)                     !Model Number        
                         END
                     END                       

STOAUDIT             FILE,DRIVER('Btrieve'),NAME('STOAUDIT.DAT'),PRE(stoa),CREATE,BINDABLE,THREAD !                    
Internal_AutoNumber_Key  KEY(stoa:Internal_AutoNumber),NOCASE,PRIMARY !                    
Audit_Ref_No_Key         KEY(stoa:Audit_Ref_No),DUP,NOCASE !                    
Cellular_Key             KEY(stoa:Site_Location,stoa:Stock_Ref_No),DUP,NOCASE !                    
Stock_Ref_No_Key         KEY(stoa:Stock_Ref_No),DUP,NOCASE !                    
Stock_Site_Number_Key    KEY(stoa:Audit_Ref_No,stoa:Site_Location),DUP,NOCASE !                    
Lock_Down_Key            KEY(stoa:Audit_Ref_No,stoa:Stock_Ref_No),DUP,NOCASE !                    
Main_Browse_Key          KEY(stoa:Audit_Ref_No,stoa:Confirmed,stoa:Site_Location,stoa:Shelf_Location,stoa:Second_Location,stoa:Internal_AutoNumber),DUP,NOCASE !                    
Report_Key               KEY(stoa:Audit_Ref_No,stoa:Shelf_Location),DUP,NOCASE !                    
Record                   RECORD,PRE()
Internal_AutoNumber         LONG                           !                    
Site_Location               STRING(30)                     !                    
Stock_Ref_No                LONG                           !                    
Original_Level              LONG                           !                    
New_Level                   LONG                           !                    
Audit_Reason                STRING(255)                    !                    
Audit_Ref_No                LONG                           !                    
Preliminary                 STRING(1)                      !                    
Shelf_Location              STRING(30)                     !                    
Second_Location             STRING(30)                     !                    
Confirmed                   STRING(1)                      !                    
                         END
                     END                       

DEFAULT2             FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULT2.DAT'),PRE(de2),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(de2:RecordNumber),NOCASE,PRIMARY !By Record Number    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
UserSkillLevel              BYTE                           !Allocate Skill Level To Users
Inv2CompanyName             STRING(30)                     !                    
Inv2AddressLine1            STRING(30)                     !                    
Inv2AddressLine2            STRING(30)                     !Address             
Inv2AddressLine3            STRING(30)                     !                    
Inv2Postcode                STRING(15)                     !                    
Inv2TelephoneNumber         STRING(15)                     !                    
Inv2FaxNumber               STRING(15)                     !                    
Inv2EmailAddress            STRING(255)                    !Email Address       
Inv2VATNumber               STRING(30)                     !                    
CurrencySymbol              STRING(3)                      !Currency Symbol     
UseRequisitionNumber        BYTE                           !Use Requisition Number
PickEngStockOnly            BYTE                           !Pick Stock From Engineer's Location Only
AllocateEngPassword         BYTE                           !Password Required To Allocate Engineer
PrintRetainedAccLabel       BYTE                           !Print Retained Accessory Label
OverdueBackOrderText        STRING(255)                    !Overdue Backorder Text
EmailUserName               STRING(255)                    !Email Username      
EmailPassword               STRING(60)                     !Email Password      
GlobalPrintText             STRING(255)                    !                    
                         END
                     END                       

ACTION               FILE,DRIVER('Btrieve'),OEM,NAME('ACTION.DAT'),PRE(act),CREATE,BINDABLE,THREAD !Action for Contact History
Record_Number_Key        KEY(act:Record_Number),NOCASE,PRIMARY !By Record Number    
Action_Key               KEY(act:Action),NOCASE            !By Action           
Record                   RECORD,PRE()
Record_Number               LONG                           !                    
Action                      STRING(30)                     !                    
                         END
                     END                       

RETSALES             FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(ret),CREATE,BINDABLE,THREAD !Retail Sales        
Ref_Number_Key           KEY(ret:Ref_Number),NOCASE,PRIMARY !By Sales Number     
Invoice_Number_Key       KEY(ret:Invoice_Number),DUP,NOCASE !By Invoice Number   
Despatched_Purchase_Key  KEY(ret:Despatched,ret:Purchase_Order_Number),DUP,NOCASE !By Purchase Order No
Despatched_Ref_Key       KEY(ret:Despatched,ret:Ref_Number),DUP,NOCASE !By Sales Number     
Despatched_Account_Key   KEY(ret:Despatched,ret:Account_Number,ret:Purchase_Order_Number),DUP,NOCASE !By Account No       
Account_Number_Key       KEY(ret:Account_Number,ret:Ref_Number),DUP,NOCASE !By Account Number   
Purchase_Number_Key      KEY(ret:Purchase_Order_Number),DUP,NOCASE !By Purchase Order No
Despatch_Number_Key      KEY(ret:Despatch_Number),DUP,NOCASE !By Despatch_Number  
Date_Booked_Key          KEY(ret:date_booked),DUP,NOCASE   !By Date Booked      
AccountInvoiceKey        KEY(ret:Account_Number,ret:Invoice_Number),DUP,NOCASE !By Invoice Number   
DespatchedPurchDateKey   KEY(ret:Despatched,ret:Purchase_Order_Number,ret:date_booked),DUP,NOCASE !By Date             
DespatchNoRefNoKey       KEY(ret:Despatch_Number,ret:Ref_Number),DUP,NOCASE !By Ref Number       
AccountPurchaseNumberKey KEY(ret:Account_Number,ret:Purchase_Order_Number),DUP,NOCASE !By Purchase Order No
WaybillNumberKey         KEY(ret:WaybillNumber,ret:Ref_Number),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
who_booked                  STRING(3)                      !                    
date_booked                 DATE                           !                    
time_booked                 TIME                           !                    
Account_Number              STRING(15)                     !                    
Contact_Name                STRING(30)                     !                    
Purchase_Order_Number       STRING(30)                     !                    
Delivery_Collection         STRING(3)                      !                    
Payment_Method              STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Parts_Cost                  REAL                           !                    
Sub_Total                   REAL                           !                    
Courier                     STRING(30)                     !                    
Consignment_Number          STRING(30)                     !                    
Date_Despatched             DATE                           !                    
Despatched                  STRING(3)                      !                    
Despatch_Number             LONG                           !                    
Invoice_Number              LONG                           !                    
Invoice_Date                DATE                           !                    
Invoice_Courier_Cost        REAL                           !                    
Invoice_Parts_Cost          REAL                           !                    
Invoice_Sub_Total           REAL                           !                    
WebOrderNumber              LONG                           !Web Order Number    
WebDateCreated              DATE                           !Date Created        
WebTimeCreated              STRING(20)                     !Time Created        
WebCreatedUser              STRING(30)                     !Web User            
Postcode                    STRING(10)                     !                    
Company_Name                STRING(30)                     !                    
Building_Name               STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Postcode_Delivery           STRING(10)                     !                    
Company_Name_Delivery       STRING(30)                     !                    
Building_Name_Delivery      STRING(30)                     !                    
Address_Line1_Delivery      STRING(30)                     !                    
Address_Line2_Delivery      STRING(30)                     !                    
Address_Line3_Delivery      STRING(30)                     !                    
Telephone_Delivery          STRING(15)                     !                    
Fax_Number_Delivery         STRING(15)                     !                    
Delivery_Text               STRING(255)                    !                    
Invoice_Text                STRING(255)                    !                    
WaybillNumber               LONG                           !Waybill Number      
DatePickingNotePrinted      DATE                           !Date Picking Note Printed
TimePickingNotePrinted      TIME                           !Time Picking Note Printed
ExchangeOrder               BYTE                           !                    
                         END
                     END                       

LOANAMF              FILE,DRIVER('Btrieve'),OEM,NAME('LOANAMF.DAT'),PRE(lmf),CREATE,BINDABLE,THREAD !                    
Audit_Number_Key         KEY(lmf:Audit_Number),NOCASE,PRIMARY !                    
Secondary_Key            KEY(lmf:Complete_Flag,lmf:Audit_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Audit_Number                LONG                           !                    
Date                        DATE                           !                    
Time                        LONG                           !                    
User                        STRING(3)                      !                    
Status                      STRING(30)                     !                    
Site_location               STRING(30)                     !                    
Complete_Flag               BYTE                           !                    
                         END
                     END                       

BOUNCER              FILE,DRIVER('Btrieve'),OEM,NAME('BOUNCER.DAT'),PRE(bou),CREATE,BINDABLE,THREAD !Bouncer Files       
Record_Number_Key        KEY(bou:Record_Number),NOCASE,PRIMARY !By Record Number    
Bouncer_Job_Number_Key   KEY(bou:Original_Ref_Number,bou:Bouncer_Job_Number),NOCASE !By Job Number       
Bouncer_Job_Only_Key     KEY(bou:Bouncer_Job_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Original_Ref_Number         REAL                           !                    
Bouncer_Job_Number          REAL                           !                    
                         END
                     END                       

EXCHOR48             FILE,DRIVER('Btrieve'),OEM,NAME('EXCHOR48.DAT'),PRE(ex4),CREATE,BINDABLE,THREAD !48 Hour Exchange Orders
RecordNumberKey          KEY(ex4:RecordNumber),NOCASE,PRIMARY !By Record Number    
LocationModelKey         KEY(ex4:Location,ex4:Manufacturer,ex4:ModelNumber),DUP,NOCASE !By Model Number     
ReturningModelKey        KEY(ex4:Received,ex4:Returning,ex4:Location,ex4:Manufacturer,ex4:ModelNumber),DUP,NOCASE !By Model Number     
LocationJobKey           KEY(ex4:Received,ex4:Returning,ex4:Location,ex4:JobNumber),DUP,NOCASE !By Job Number       
JobNumberKey             KEY(ex4:Location,ex4:JobNumber),DUP,NOCASE !By Job Number       
AttachedToJobKey         KEY(ex4:AttachedToJob,ex4:Location,ex4:JobNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !Location            
Manufacturer                STRING(30)                     !Manufacturer        
ModelNumber                 STRING(30)                     !Model Number        
Received                    BYTE                           !Received            
Notes                       STRING(255)                    !                    
DateCreated                 DATE                           !Date Created        
TimeCreated                 TIME                           !Time Created        
DateOrdered                 DATE                           !Date Ordered        
TimeOrdered                 TIME                           !Time Ordered        
JobNumber                   REAL                           !Job Number          
Returning                   BYTE                           !No Order Required   
OrderUnitNumber             LONG                           !Ref Number Of Exchange Unit Returning
AttachedToJob               BYTE                           !Attached To Job     
                         END
                     END                       

LOANAUI              FILE,DRIVER('Btrieve'),OEM,NAME('LOANAUI.DAT'),PRE(lau),CREATE,BINDABLE,THREAD !                    
Internal_No_Key          KEY(lau:Internal_No),NOCASE,PRIMARY !                    
Audit_Number_Key         KEY(lau:Audit_Number),DUP,NOCASE  !                    
Ref_Number_Key           KEY(lau:Ref_Number),DUP,NOCASE    !                    
Exch_Browse_Key          KEY(lau:Audit_Number,lau:Exists),DUP,NOCASE !                    
AuditIMEIKey             KEY(lau:Audit_Number,lau:New_IMEI),DUP,NOCASE !By I.M.E.I. Number  
Locate_IMEI_Key          KEY(lau:Audit_Number,lau:Site_Location,lau:IMEI_Number),DUP,NOCASE,OPT !                    
Main_Browse_Key          KEY(lau:Audit_Number,lau:Confirmed,lau:Site_Location,lau:Stock_Type,lau:Shelf_Location,lau:Internal_No),DUP,NOCASE !                    
Record                   RECORD,PRE()
Internal_No                 LONG                           !                    
Site_Location               STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Location                    STRING(30)                     !                    
Audit_Number                LONG                           !                    
Ref_Number                  LONG                           !Unit Number         
Exists                      BYTE                           !                    
New_IMEI                    BYTE                           !                    
IMEI_Number                 STRING(20)                     !                    
Confirmed                   BYTE                           !Confirmed           
Stock_Type                  STRING(30)                     !                    
                         END
                     END                       

WIPAMF               FILE,DRIVER('Btrieve'),OEM,NAME('WIPAMF.DAT'),PRE(wim),CREATE,BINDABLE,THREAD !                    
Audit_Number_Key         KEY(wim:Audit_Number),NOCASE,PRIMARY !                    
Secondary_Key            KEY(wim:Complete_Flag,wim:Audit_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Audit_Number                LONG                           !                    
Date                        DATE                           !                    
Time                        LONG                           !                    
User                        STRING(3)                      !                    
Status                      STRING(30)                     !                    
Site_location               STRING(30)                     !                    
Complete_Flag               BYTE                           !                    
                         END
                     END                       

TEAMS                FILE,DRIVER('Btrieve'),OEM,NAME('TEAMS.DAT'),PRE(tea),CREATE,BINDABLE,THREAD !Teams               
Record_Number_Key        KEY(tea:Record_Number),NOCASE,PRIMARY !By Record Number    
Team_Key                 KEY(tea:Team),NOCASE              !By Team             
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Team                        STRING(30)                     !                    
                         END
                     END                       

WIPAUI               FILE,DRIVER('Btrieve'),OEM,NAME('WIPAUI.DAT'),PRE(wia),CREATE,BINDABLE,THREAD !                    
Internal_No_Key          KEY(wia:Internal_No),NOCASE,PRIMARY !                    
Audit_Number_Key         KEY(wia:Audit_Number),DUP,NOCASE  !                    
Ref_Number_Key           KEY(wia:Ref_Number),DUP,NOCASE    !                    
Exch_Browse_Key          KEY(wia:Audit_Number),DUP,NOCASE  !                    
AuditIMEIKey             KEY(wia:Audit_Number,wia:New_In_Status),DUP,NOCASE !By I.M.E.I. Number  
Locate_IMEI_Key          KEY(wia:Audit_Number,wia:Site_Location,wia:Ref_Number),DUP,NOCASE !                    
Main_Browse_Key          KEY(wia:Audit_Number,wia:Confirmed,wia:Site_Location,wia:Status,wia:Ref_Number),DUP,NOCASE !                    
AuditRefNumberKey        KEY(wia:Audit_Number,wia:Ref_Number),DUP,NOCASE !By Ref Number       
AuditStatusRefNoKey      KEY(wia:Audit_Number,wia:Status,wia:Ref_Number),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Internal_No                 LONG                           !                    
Site_Location               STRING(30)                     !                    
Status                      STRING(30)                     !                    
Audit_Number                LONG                           !                    
Ref_Number                  LONG                           !Unit Number         
New_In_Status               BYTE                           !                    
Confirmed                   BYTE                           !Confirmed           
                         END
                     END                       

EXCAUDIT             FILE,DRIVER('Btrieve'),OEM,NAME('EXCAUDIT.DAT'),PRE(exa),CREATE,BINDABLE,THREAD !Exchange Unit Audit Number File
RecordNumberKey          KEY(exa:Record_Number),NOCASE,PRIMARY !By Record Number    
Audit_Number_Key         KEY(exa:Stock_Type,exa:Audit_Number),NOCASE !By Audit Number     
StockUnitNoKey           KEY(exa:Stock_Unit_Number),DUP,NOCASE !                    
ReplaceUnitNoKey         KEY(exa:Replacement_Unit_Number),DUP,NOCASE !                    
TypeStockNumber          KEY(exa:Stock_Type,exa:Stock_Unit_Number,exa:Audit_Number),DUP,NOCASE !By Audit Number     
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Stock_Type                  STRING(30)                     !                    
Audit_Number                REAL                           !                    
Stock_Unit_Number           REAL                           !                    
Replacement_Unit_Number     REAL                           !                    
                         END
                     END                       

JOBEXACC             FILE,DRIVER('Btrieve'),OEM,NAME('JOBEXACC.DAT'),PRE(jea),CREATE,BINDABLE,THREAD !Job Exchange Accessories
Record_Number_Key        KEY(jea:Record_Number),NOCASE,PRIMARY !                    
Part_Number_Key          KEY(jea:Job_Ref_Number,jea:Part_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Job_Ref_Number              REAL                           !                    
Stock_Ref_Number            REAL                           !                    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCOL.DAT'),PRE(moc),CREATE,BINDABLE,THREAD !Model Colours       
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY !                    
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE !By Colour           
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Model_Number                STRING(30)                     !                    
Colour                      STRING(30)                     !                    
                         END
                     END                       

PAYTYPES             FILE,DRIVER('Btrieve'),NAME('PAYTYPES.DAT'),PRE(pay),CREATE,BINDABLE,THREAD !Payment Types       
Payment_Type_Key         KEY(pay:Payment_Type),NOCASE,PRIMARY !By Payment Type     
Record                   RECORD,PRE()
Payment_Type                STRING(30)                     !                    
Credit_Card                 STRING(3)                      !                    
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(cht),CREATE,BINDABLE,THREAD !Contact History     
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE !By Date             
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE !By Action           
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE !By User             
Record_Number_Key        KEY(cht:Record_Number),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Record_Number               LONG                           !Record Number       
Ref_Number                  LONG                           !Ref Number          
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Action                      STRING(80)                     !                    
Notes                       STRING(2000)                   !                    
SystemHistory               BYTE                           !System History      
                         END
                     END                       

DEFPRINT             FILE,DRIVER('Btrieve'),NAME('DEFPRINT.DAT'),PRE(dep),CREATE,BINDABLE,THREAD !Default Printer List
Printer_Name_Key         KEY(dep:Printer_Name),NOCASE,PRIMARY !By Printer Name     
Record                   RECORD,PRE()
Printer_Name                STRING(60)                     !                    
Printer_Path                STRING(255)                    !                    
Background                  STRING(3)                      !                    
Copies                      LONG                           !Copies              
                         END
                     END                       

JOBSTAMP             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSTAMP.DAT'),PRE(jos),CREATE,BINDABLE,THREAD !Date/Time Stamp when "JOB" changes
RecordNumberKey          KEY(jos:RecordNumber),NOCASE,PRIMARY !By Record Number    
JOBSRefNumberKey         KEY(jos:JOBSRefNumber),DUP,NOCASE !By RefNumber        
DateTimeKey              KEY(jos:DateStamp,jos:TimeStamp),DUP,NOCASE !By Date Time        
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JOBSRefNumber               LONG                           !Link To JOBS File   
DateStamp                   DATE                           !Date Stamp          
TimeStamp                   TIME                           !Time Stamp          
                         END
                     END                       

ACCESDEF             FILE,DRIVER('Btrieve'),NAME('ACCESDEF.DAT'),PRE(acd),CREATE,BINDABLE,THREAD !Accessories - Default List
Accessory_Key            KEY(acd:Accessory),NOCASE,PRIMARY !By Accessory        
Record                   RECORD,PRE()
Accessory                   STRING(30)                     !                    
                         END
                     END                       

STANTEXT             FILE,DRIVER('Btrieve'),NAME('STANTEXT.DAT'),PRE(stt),CREATE,BINDABLE,THREAD !Standard Document Texts
Description_Key          KEY(stt:Description),NOCASE,PRIMARY !By Description      
Record                   RECORD,PRE()
Description                 STRING(30)                     !                    
TelephoneNumber             STRING(30)                     !                    
FaxNumber                   STRING(30)                     !                    
Text                        STRING(1000)                   !                    
                         END
                     END                       

NOTESENG             FILE,DRIVER('Btrieve'),NAME('NOTESENG.DAT'),PRE(noe),CREATE,BINDABLE,THREAD !Notes - Engineers   
Notes_Key                KEY(noe:Reference,noe:Notes),NOCASE,PRIMARY !By Notes            
Record                   RECORD,PRE()
Reference                   STRING(30)                     !                    
Notes                       STRING(80)                     !                    
                         END
                     END                       

JOBACCNO             FILE,DRIVER('Btrieve'),OEM,NAME('JOBACCNO.DAT'),PRE(joa),CREATE,BINDABLE,THREAD !Job Accessory Numbers
RecordNumberKey          KEY(joa:RecordNumber),NOCASE,PRIMARY !By Record Number    
AccessoryNumberKey       KEY(joa:RefNumber,joa:AccessoryNumber),DUP,NOCASE !By Accessory Number 
AccessoryNoOnlyKey       KEY(joa:AccessoryNumber),DUP,NOCASE !By Accessory Number 
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
AccessoryNumber             STRING(30)                     !Accessory Number    
                         END
                     END                       

JOBRPNOT             FILE,DRIVER('Btrieve'),OEM,NAME('JOBRPNOT.DAT'),PRE(jrn),CREATE,BINDABLE,THREAD !Job Repair Notes    
RecordNumberKey          KEY(jrn:RecordNumber),NOCASE,PRIMARY !By Record Number    
TheDateKey               KEY(jrn:RefNumber,jrn:TheDate,jrn:TheTime),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to JOBS Ref_Number
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
User                        STRING(3)                      !User                
Notes                       STRING(255)                    !Notes               
                         END
                     END                       

JOBSOBF              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jof),CREATE,BINDABLE,THREAD !Processing OBF Jobs 
RecordNumberKey          KEY(jof:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jof:RefNumber),DUP,NOCASE     !By Job Number       
StatusRefNumberKey       KEY(jof:Status,jof:RefNumber),DUP,NOCASE !By Job Number       
StatusIMEINumberKey      KEY(jof:Status,jof:IMEINumber),DUP,NOCASE !By I.M.E.I. Number  
HeadAccountCompletedKey  KEY(jof:HeadAccountNumber,jof:DateCompleted,jof:RefNumber),DUP,NOCASE !By Date Completed   
HeadAccountProcessedKey  KEY(jof:HeadAccountNumber,jof:DateProcessed,jof:RefNumber),DUP,NOCASE !By Date Processed   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Like to JOBS RefNumber
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
Status                      BYTE                           !Processed Status    
Replacement                 BYTE                           !Replacement         
StoreReferenceNumber        STRING(30)                     !Stock Reference Number
RNumber                     STRING(30)                     !R Number            
RejectionReason             STRING(255)                    !Rejection Reason    
ReplacementIMEI             STRING(30)                     !Replacement I.M.E.I. Number
LAccountNumber              STRING(30)                     !L/Account Number    
UserCode                    STRING(3)                      !User Code           
HeadAccountNumber           STRING(30)                     !Head Account Number 
DateCompleted               DATE                           !Date Completed      
TimeCompleted               TIME                           !Time Completed      
DateProcessed               DATE                           !Date Process        
TimeProcessed               TIME                           !Time Processed      
                         END
                     END                       

JOBSINV              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSINV.DAT'),PRE(jov),CREATE,BINDABLE,THREAD !Jobs Invoice/Credit Notes
RecordNumberKey          KEY(jov:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jov:RefNumber,jov:RecordNumber),DUP,NOCASE !By Record Number    
DateCreatedKey           KEY(jov:RefNumber,jov:DateCreated,jov:TimeCreated),DUP,NOCASE !By Date Created     
DateCreatedOnlyKey       KEY(jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE !By Job Number       
TypeRecordKey            KEY(jov:RefNumber,jov:Type,jov:RecordNumber),DUP,NOCASE !By Record Number    
TypeSuffixKey            KEY(jov:RefNumber,jov:Type,jov:Suffix),DUP,NOCASE !By Suffix           
InvoiceNumberKey         KEY(jov:InvoiceNumber,jov:RecordNumber),DUP,NOCASE !By Invoice Number   
InvoiceTypeKey           KEY(jov:InvoiceNumber,jov:Type,jov:RecordNumber),DUP,NOCASE !By Invoice Number   
BookingDateTypeKey       KEY(jov:BookingAccount,jov:Type,jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE !By Type             
TypeDateKey              KEY(jov:Type,jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE !By Type             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To JOBS RefNumber
InvoiceNumber               LONG                           !Link to INVOICE Invoice_Number
Type                        STRING(1)                      !Type (Invoice / Credit)
Suffix                      STRING(1)                      !Suffix              
DateCreated                 DATE                           !Date Created        
TimeCreated                 TIME                           !Time Created        
UserCode                    STRING(3)                      !User Code           
OriginalTotalCost           REAL                           !Original Total Cost 
NewTotalCost                REAL                           !New Total Cost      
CreditAmount                REAL                           !Credit Amount       
BookingAccount              STRING(30)                     !Booking Account Number
NewInvoiceNumber            STRING(30)                     !New Invoice Number  
ChargeType                  STRING(30)                     !Charge Type         
RepairType                  STRING(30)                     !Repair Type         
HandlingFee                 REAL                           !Handling Fee        
ExchangeRate                REAL                           !Exchange Rate       
ARCCharge                   REAL                           !ARC Charge          
RRCLostLoanCost             REAL                           !Lost Loan Cost      
RRCPartsCost                REAL                           !RRC Parts Cost      
RRCPartsSelling             REAL                           !RRC Parts Selling   
RRCLabour                   REAL                           !RRC Labour Cost     
ARCMarkUp                   REAL                           !ARC Markup          
RRCVAT                      REAL                           !RRCVAT              
Paid                        REAL                           !Paid                
Outstanding                 REAL                           !Outstanding         
Refund                      REAL                           !Refund              
                         END
                     END                       

JOBSCONS             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSCONS.DAT'),PRE(joc),CREATE,BINDABLE,THREAD !Jobs Consignment History
RecordNumberKey          KEY(joc:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(joc:RefNumber,joc:TheDate,joc:TheTime),DUP,NOCASE !By Date             
ConsignmentNumberKey     KEY(joc:ConsignmentNumber,joc:RefNumber),DUP,NOCASE !By Consignment Number
DespatchFromDateKey      KEY(joc:DespatchFrom,joc:TheDate,joc:RefNumber),DUP,NOCASE !By Date             
DateOnlyKey              KEY(joc:TheDate,joc:RefNumber),DUP,NOCASE !By Date             
CourierKey               KEY(joc:Courier,joc:RefNumber),DUP,NOCASE !By Courier          
DespatchFromCourierKey   KEY(joc:DespatchFrom,joc:Courier,joc:RefNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number (Link To Jobs)
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
UserCode                    STRING(3)                      !User Code           
DespatchFrom                STRING(30)                     !Despatch From       
DespatchTo                  STRING(30)                     !Despatch To         
Courier                     STRING(30)                     !Courier             
ConsignmentNumber           STRING(30)                     !Consignment Number  
DespatchType                STRING(3)                      !Despatch Type       
                         END
                     END                       

JOBSWARR             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSWARR.DAT'),PRE(jow),CREATE,BINDABLE,THREAD !Warranty Jobs List  
RecordNumberKey          KEY(jow:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jow:RefNumber),DUP,NOCASE     !By Ref Number       
StatusManFirstKey        KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:RefNumber),DUP,NOCASE !By Job Number       
StatusManKey             KEY(jow:Status,jow:Manufacturer,jow:RefNumber),DUP,NOCASE !By Job Number       
ClaimStatusManKey        KEY(jow:Status,jow:Manufacturer,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
ClaimStatusManFirstKey   KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCStatusKey             KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCStatusManKey          KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCReconciledManKey      KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCReconciledKey         KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCStatusKey     KEY(jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE !By RRC Status       
RepairedRRCStatusManKey  KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCReconciledKey KEY(jow:RepairedAt,jow:RRCStatus,jow:DateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCReconManKey   KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
SubmittedRepairedBranchKey KEY(jow:BranchID,jow:RepairedAt,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Branch           
SubmittedBranchKey       KEY(jow:BranchID,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
ClaimSubmittedKey        KEY(jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCAccManKey     KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:DateAccepted,jow:RefNumber),DUP,NOCASE !By Job Number       
AcceptedBranchKey        KEY(jow:BranchID,jow:DateAccepted,jow:RefNumber),DUP,NOCASE !By Job Number       
DateAcceptedKey          KEY(jow:DateAccepted,jow:RefNumber),DUP,NOCASE !By Job Number       
RejectedBranchKey        KEY(jow:BranchID,jow:DateRejected,jow:RefNumber),DUP,NOCASE !By Job Number       
RejectedKey              KEY(jow:DateRejected,jow:RefNumber),DUP,NOCASE !By Job Number       
FinalRejectionBranchKey  KEY(jow:BranchID,jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE !By JobNumber        
FinalRejectionKey        KEY(jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Job Number          
BranchID                    STRING(2)                      !BranchID            
RepairedAt                  STRING(3)                      !Repaired At         
Manufacturer                STRING(30)                     !Manufacturer        
FirstSecondYear             BYTE                           !First or Second Year
Status                      STRING(3)                      !Status              
Submitted                   LONG                           !Number Of Times Submitted
FromApproved                BYTE                           !Resubmitted From Approved Batch
ClaimSubmitted              DATE                           !Claim Submitted     
DateAccepted                DATE                           !Date Accepted       
DateReconciled              DATE                           !Date Reconciled     
RRCDateReconciled           DATE                           !Date Reconciled     
RRCStatus                   STRING(3)                      !RRC Status          
DateRejected                DATE                           !                    
DateFinalRejection          DATE                           !                    
                         END
                     END                       

JOBSE2               FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE2.DAT'),PRE(jobe2),CREATE,BINDABLE,THREAD !Jobs Extension (2)  
RecordNumberKey          KEY(jobe2:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jobe2:RefNumber),DUP,NOCASE   !By Ref Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To JOBS Ref_Number
IDNumber                    STRING(13)                     !I.D. Number         
InPendingDate               DATE                           !In Pending Date     
Contract                    BYTE                           !Contract            
Prepaid                     BYTE                           !Prepaid             
WarrantyRefNo               STRING(30)                     !Warranty Ref No     
SIDBookingName              STRING(60)                     !SID Booking Name    
XAntenna                    BYTE                           !Antenna             
XLens                       BYTE                           !Lens                
XFCover                     BYTE                           !F/Cover             
XBCover                     BYTE                           !B/Cover             
XKeypad                     BYTE                           !Keypad              
XBattery                    BYTE                           !Battery             
XCharger                    BYTE                           !Charger             
XLCD                        BYTE                           !LCD                 
XSimReader                  BYTE                           !Sim Reader          
XSystemConnector            BYTE                           !System Connector    
XNone                       BYTE                           !None                
XNotes                      STRING(255)                    !Notes               
ExchangeTerms               BYTE                           !Exchange Terms Explained To Customer
WaybillNoFromPUP            LONG                           !Waybill Number From PUP
WaybillNoToPUP              LONG                           !Waybill Number To PUP
SMSNotification             BYTE                           !SMS Notification    
EmailNotification           BYTE                           !Email Notification  
SMSAlertNumber              STRING(30)                     !SMS Alert Mobile Number
EmailAlertAddress           STRING(255)                    !Email Alert Address 
DateReceivedAtPUP           DATE                           !Date Received At PUP From RRC
TimeReceivedAtPUP           TIME                           !Time Received At PUP From RRC
DateDespatchFromPUP         DATE                           !Date Despatch From PUP
TimeDespatchFromPUP         TIME                           !Time Despatch From PUP
LoanIDNumber                STRING(13)                     !Loan ID Number      
CourierWaybillNumber        STRING(30)                     !Courier Waybill Number
HubCustomer                 STRING(30)                     !Hub                 
HubCollection               STRING(30)                     !Hub                 
HubDelivery                 STRING(30)                     !Hub                 
WLabourPaid                 REAL                           !Warranty Labour Paid
WPartsPaid                  REAL                           !Warranty Parts Paid 
WOtherCosts                 REAL                           !Warranty Other Costs
WSubTotal                   REAL                           !Warranty Sub Total  
WVAT                        REAL                           !Warranty VAT        
WTotal                      REAL                           !Warranty Total      
WarrantyReason              STRING(255)                    !Reason For Not Reconciling
WInvoiceRefNo               STRING(255)                    !Warranty Invoice Ref Number
SMSDate                     DATE                           !                    
JobDiscountAmnt             REAL                           !                    
InvDiscountAmnt             REAL                           !                    
POPConfirmed                BYTE                           !                    
                         END
                     END                       

TRDSPEC              FILE,DRIVER('Btrieve'),NAME('TRDSPEC.DAT'),PRE(tsp),CREATE,BINDABLE,THREAD !Third Party Special Instructions
Short_Description_Key    KEY(tsp:Short_Description),NOCASE,PRIMARY !By Short Description
Record                   RECORD,PRE()
Short_Description           STRING(30)                     !                    
Long_Description            STRING(255)                    !                    
                         END
                     END                       

ORDPEND              FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ope),CREATE,BINDABLE,THREAD !Orders - Pending Parts
Ref_Number_Key           KEY(ope:Ref_Number),NOCASE,PRIMARY !By Ref Number       
Supplier_Key             KEY(ope:Supplier,ope:Part_Number),DUP,NOCASE !By Supplier         
DescriptionKey           KEY(ope:Supplier,ope:Description),DUP,NOCASE !By Description      
Supplier_Name_Key        KEY(ope:Supplier),DUP,NOCASE      !By Supplier         
Part_Ref_Number_Key      KEY(ope:Part_Ref_Number),DUP,NOCASE !By Stock Ref Number 
Awaiting_Supplier_Key    KEY(ope:Awaiting_Stock,ope:Supplier,ope:Part_Number),DUP,NOCASE !By Part Number      
PartRecordNumberKey      KEY(ope:PartRecordNumber),DUP,NOCASE !By Record Number    
ReqPartNumber            KEY(ope:StockReqNumber,ope:Part_Number),DUP,NOCASE !By Part Number      
ReqDescriptionKey        KEY(ope:StockReqNumber,ope:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Part_Ref_Number             LONG                           !                    
Job_Number                  LONG                           !                    
Part_Type                   STRING(3)                      !                    
Supplier                    STRING(30)                     !                    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Quantity                    LONG                           !                    
Account_Number              STRING(15)                     !                    
Awaiting_Stock              STRING(3)                      !                    
Reason                      STRING(255)                    !Reason              
PartRecordNumber            LONG                           !Part Record Number  
StockReqNumber              LONG                           !Stock Requisition Number
                         END
                     END                       

STOHIST              FILE,DRIVER('Btrieve'),NAME('STOHIST.DAT'),PRE(shi),CREATE,BINDABLE,THREAD !Stock History       
Ref_Number_Key           KEY(shi:Ref_Number,shi:Date),DUP,NOCASE !By Ref_Number       
record_number_key        KEY(shi:Record_Number),NOCASE,PRIMARY !                    
Transaction_Type_Key     KEY(shi:Ref_Number,shi:Transaction_Type,shi:Date),DUP,NOCASE !By Transaction      
DateKey                  KEY(shi:Date),DUP,NOCASE          !By Date             
JobNumberKey             KEY(shi:Ref_Number,shi:Transaction_Type,shi:Job_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Record_Number               LONG                           !Record Number       
Ref_Number                  LONG                           !                    
User                        STRING(3)                      !                    
Transaction_Type            STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Job_Number                  LONG                           !                    
Sales_Number                LONG                           !                    
Quantity                    REAL                           !                    
Date                        DATE                           !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Notes                       STRING(255)                    !                    
Information                 STRING(255)                    !                    
StockOnHand                 LONG                           !Stock On Hand       
                         END
                     END                       

REPTYDEF             FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(rtd),CREATE,BINDABLE,THREAD !Manufacturer Repair Types
RecordNumberKey          KEY(rtd:RecordNumber),NOCASE,PRIMARY !By Record Number    
ManRepairTypeKey         KEY(rtd:Manufacturer,rtd:Repair_Type),NOCASE !By Repair Type      
Repair_Type_Key          KEY(rtd:Repair_Type),DUP,NOCASE   !By Repair Type      
Chargeable_Key           KEY(rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE !                    
Warranty_Key             KEY(rtd:Warranty,rtd:Repair_Type),DUP,NOCASE !                    
ChaManRepairTypeKey      KEY(rtd:Manufacturer,rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE !By Repair Type      
WarManRepairTypeKey      KEY(rtd:Manufacturer,rtd:Warranty,rtd:Repair_Type),DUP,NOCASE !By Repair Type      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Repair_Type                 STRING(30)                     !Repair Type         
Chargeable                  STRING(3)                      !Chargeable Repair Type
Warranty                    STRING(3)                      !Warranty Repair Type
WarrantyCode                STRING(5)                      !Warranty Code       
CompFaultCoding             BYTE                           !Force Fault Codes   
ExcludeFromEDI              BYTE                           !Exclude From EDI Process
ExcludeFromInvoicing        BYTE                           !Exclude From Invoicing
BER                         BYTE                           !Type Of Repair Type 
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table
PromptForExchange           BYTE                           !Prompt For Exchange 
JobWeighting                LONG                           !Job Weighting       
SkillLevel                  LONG                           !Skill Level         
Manufacturer                STRING(30)                     !Manufacturer        
RepairLevel                 LONG                           !Repair Index        
ForceAdjustment             BYTE                           !Force Adjustment If No Parts Used
ScrapExchange               BYTE                           !                    
ExcludeHandlingFee          BYTE                           !Exclude RRC Handling Fee
NotAvailable                BYTE                           !Not Available       
NoParts                     STRING(1)                      !                    
                         END
                     END                       

PRIORITY             FILE,DRIVER('Btrieve'),NAME('PRIORITY.DAT'),PRE(pri),CREATE,BINDABLE,THREAD !Priority            
Priority_Type_Key        KEY(pri:Priority_Type),NOCASE,PRIMARY !By Priority Type    
Record                   RECORD,PRE()
Priority_Type               STRING(30)                     !                    
Time                        REAL                           !                    
Book_Before                 TIME                           !                    
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe),CREATE,BINDABLE,THREAD !Jobs Extension      
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE    !By Ref Number       
WarrStatusDateKey        KEY(jobe:WarrantyStatusDate),DUP,NOCASE !By Warranty Status Date
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Refence To Job Number
JobMark                     BYTE                           !Job Mark            
TraFaultCode1               STRING(30)                     !Fault Code 1        
TraFaultCode2               STRING(30)                     !Fault Code 2        
TraFaultCode3               STRING(30)                     !Fault Code 3        
TraFaultCode4               STRING(30)                     !Fault Code 4        
TraFaultCode5               STRING(30)                     !Fault Code 5        
TraFaultCode6               STRING(30)                     !Fault Code 6        
TraFaultCode7               STRING(30)                     !Fault Code 7        
TraFaultCode8               STRING(30)                     !Fault Code 8        
TraFaultCode9               STRING(30)                     !Fault Code 9        
TraFaultCode10              STRING(30)                     !Fault Code 10       
TraFaultCode11              STRING(30)                     !Fault Code 11       
TraFaultCode12              STRING(30)                     !Fault Code 12       
SIMNumber                   STRING(30)                     !SIM Number          
JobReceived                 BYTE                           !Job Received        
SkillLevel                  LONG                           !Skill Level         
UPSFlagCode                 STRING(1)                      !UPS Flag Code       
FailedDelivery              BYTE                           !Failed Delivery     
CConfirmSecondEntry         STRING(3)                      !Chargeable Confirm Second Entry
WConfirmSecondEntry         STRING(3)                      !Warranty Confirm Second Entry
EndUserEmailAddress         STRING(255)                    !Email Address       
HubRepair                   BYTE                           !Hub Repair          
Network                     STRING(30)                     !Network             
POPConfirmed                BYTE                           !POPConfirmed        
HubRepairDate               DATE                           !HubRepairDate       
HubRepairTime               TIME                           !Hub Repair Time     
ClaimValue                  REAL                           !ClaimValue          
HandlingFee                 REAL                           !Handling Fee        
ExchangeRate                REAL                           !Exchange Rate       
InvoiceClaimValue           REAL                           !Invoice Claim Value 
InvoiceHandlingFee          REAL                           !Invoice Handling Fee
InvoiceExchangeRate         REAL                           !Invoice Exchange Rate
BoxESN                      STRING(20)                     !                    
ValidPOP                    STRING(3)                      !                    
ReturnDate                  DATE                           !                    
TalkTime                    REAL                           !                    
OriginalPackaging           BYTE                           !                    
OriginalBattery             BYTE                           !                    
OriginalCharger             BYTE                           !                    
OriginalAntenna             BYTE                           !                    
OriginalManuals             BYTE                           !                    
PhysicalDamage              BYTE                           !                    
OriginalDealer              CSTRING(255)                   !                    
BranchOfReturn              STRING(30)                     !                    
COverwriteRepairType        BYTE                           !Overwrite Repair Type
WOverwriteRepairType        BYTE                           !Overwrite Repair Type
LabourAdjustment            REAL                           !Labour Adjustment   
PartsAdjustment             REAL                           !Parts Adjustment    
SubTotalAdjustment          REAL                           !Sub Total Adjustment
IgnoreClaimCosts            BYTE                           !Ignore Claim Costs  
RRCCLabourCost              REAL                           !Labour Cost         
RRCCPartsCost               REAL                           !Parts Cost          
RRCCPartsSale               REAL                           !Parts Sale Cost     
RRCCSubTotal                REAL                           !RRC Sub Total       
InvRRCCLabourCost           REAL                           !Invoice Labour Cost 
InvRRCCPartsCost            REAL                           !Invoice Parts Cost  
InvRRCCPartsSale            REAL                           !Invoice Parts Sale Cost
InvRRCCSubTotal             REAL                           !Invoice Sub Total   
RRCWLabourCost              REAL                           !Labour Cost         
RRCWPartsCost               REAL                           !Parts Cost          
RRCWPartsSale               REAL                           !Parts Sale Cost     
RRCWSubTotal                REAL                           !RRC Sub Total       
InvRRCWLabourCost           REAL                           !Invoice Labour Cost 
InvRRCWPartsCost            REAL                           !Invoice Parts Cost  
InvRRCWPartsSale            REAL                           !Invoice Parts Sale Cost
InvRRCWSubTotal             REAL                           !Invoice Sub Total   
ARC3rdPartyCost             REAL                           !3rd Party Cost      
InvARC3rdPartCost           REAL                           !Invoice 3rd Party Cost
WebJob                      BYTE                           !Is this job a web job?
RRCELabourCost              REAL                           !RRC Estimate Labour Cost
RRCEPartsCost               REAL                           !RRC Estimate Parts Cost
RRCESubTotal                REAL                           !RRC Estimate Sub Total
IgnoreRRCChaCosts           REAL                           !Ignore Default Costs
IgnoreRRCWarCosts           REAL                           !Ignore Default Costs
IgnoreRRCEstCosts           REAL                           !Ignore Estimate Costs
OBFvalidated                BYTE                           !Out of Box Failure - Validation
OBFvalidateDate             DATE                           !OBF Validation Date 
OBFvalidateTime             TIME                           !OBF Validation Time 
DespatchType                STRING(3)                      !Despatch Type       
Despatched                  STRING(3)                      !Despatched          
WarrantyClaimStatus         STRING(30)                     !Warranty Claim Status
WarrantyStatusDate          DATE                           !Warranty Claim Status Date
InSecurityPackNo            STRING(30)                     !Incoming Security Pack Number
JobSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Job
ExcSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Exchange
LoaSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Loan
ExceedWarrantyRepairLimit   BYTE                           !Warranty Repair Authorised To Exceed Repair Limit
BouncerClaim                BYTE                           !Set true (1) if this was submitted from Bouncer Browse
Sub_Sub_Account             STRING(15)                     !                    
ConfirmClaimAdjustment      BYTE                           !Confirm Warranty Claim Adjustment Values
ARC3rdPartyVAT              REAL                           !3rd Party Vat       
ARC3rdPartyInvoiceNumber    STRING(30)                     !3rd Party Invoice Number
ExchangeAdjustment          REAL                           !Exchange Adjustment 
ExchangedATRRC              BYTE                           !Exchanged At RRC    
EndUserTelNo                STRING(15)                     !                    
ClaimColour                 BYTE                           !Colour Warranty Claim
ARC3rdPartyMarkup           REAL                           !3rd Party Markup    
Ignore3rdPartyCosts         BYTE                           !Ignore 3rd Party Costs
POPType                     STRING(30)                     !POP Type            
OBFProcessed                BYTE                           !OBF Routine Processed
LoanReplacementValue        REAL                           !Loan Replacement Value
PendingClaimColour          BYTE                           !Pending Claim Colour
AccessoryNotes              STRING(255)                    !Accessory Notes     
ClaimPartsCost              REAL                           !Claim Parts Cost    
InvClaimPartsCost           REAL                           !Invoice Parts Claim Cost
Booking48HourOption         BYTE                           !48 Hour Exchange (Booking)
Engineer48HourOption        BYTE                           !48 Hour Exchange (Engineering)
ExcReplcamentCharge         BYTE                           !Exchange Replacement Charge
SecondExchangeNumber        LONG                           !Second Exchange Unit Number
SecondExchangeStatus        STRING(30)                     !2nd Exchange Status 
VatNumber                   STRING(30)                     !Vat Number          
ExchangeProductCode         STRING(30)                     !Exchange Handset Part Number
SecondExcProdCode           STRING(30)                     !Second Exchange Handset Part Number
ARC3rdPartyInvoiceDate      DATE                           !                    
ARC3rdPartyWaybillNo        STRING(30)                     !                    
ARC3rdPartyRepairType       STRING(30)                     !                    
ARC3rdPartyRejectedReason   STRING(255)                    !                    
ARC3rdPartyRejectedAmount   REAL                           !                    
VSACustomer                 BYTE                           !VSA Customer        
HandsetReplacmentValue      REAL                           !Handset Replacement Value
SecondHandsetRepValue       REAL                           !Handset Replacement Value
t                           STRING(20)                     !                    
                         END
                     END                       

MANFAULT             FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(maf),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding On Jobs
Field_Number_Key         KEY(maf:Manufacturer,maf:Field_Number),NOCASE,PRIMARY !By Field Number     
MainFaultKey             KEY(maf:Manufacturer,maf:MainFault),DUP,NOCASE !By Main Fault       
InFaultKey               KEY(maf:Manufacturer,maf:InFault),DUP,NOCASE !By In Fault         
ScreenOrderKey           KEY(maf:Manufacturer,maf:ScreenOrder),DUP,NOCASE !By Screen Order     
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
Compulsory_At_Booking       STRING(3)                      !                    
ReplicateFault              STRING(3)                      !Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                      !Replicate To Fault Description Field
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
ForceFormat                 BYTE                           !Force Format        
FieldFormat                 STRING(30)                     !Field Format        
DateType                    STRING(30)                     !Date Type           
MainFault                   BYTE                           !Main Fault          
PromptForExchange           BYTE                           !Prompt For Exchange 
InFault                     BYTE                           !In Fault            
CompulsoryIfExchange        BYTE                           !Compulsory If Exchange Issued
NotAvailable                BYTE                           !Not Available       
CharCompulsory              BYTE                           !Compulsory At Completion
CharCompulsoryBooking       BYTE                           !Compulsory At Booking
ScreenOrder                 LONG                           !Screen Order        
RestrictAvailability        BYTE                           !Restrict Availability
RestrictServiceCentre       BYTE                           !Available For Service Centres
GenericFault                BYTE                           !Generic Fault       
NotCompulsoryThirdParty     BYTE                           !Not Compulsory For 3rd Party Job
HideThirdParty              BYTE                           !Hide For 3rd Party Jobs
HideRelatedCodeIfBlank      BYTE                           !Hide When Related Code Is Blank
BlankRelatedCode            LONG                           !Blank Related Code  
FillFromDOP                 BYTE                           !Fill From Date Of Purchase
CompulsoryForRepairType     BYTE                           !Compulsory For Repair Type
CompulsoryRepairType        STRING(30)                     !Repair Type         
                         END
                     END                       

TRACHAR              FILE,DRIVER('Btrieve'),NAME('TRACHAR.DAT'),PRE(tch),CREATE,BINDABLE,THREAD !Trade Charge Types For Priced Despatch
Account_Number_Key       KEY(tch:Account_Number,tch:Charge_Type),NOCASE,PRIMARY !By Charge Type      
Record                   RECORD,PRE()
Account_Number              STRING(15)                     !                    
Charge_Type                 STRING(30)                     !                    
                         END
                     END                       

DISCOUNT             FILE,DRIVER('Btrieve'),NAME('DISCOUNT.DAT'),PRE(dis),CREATE,BINDABLE,THREAD !Discount Code       
Discount_Code_Key        KEY(dis:Discount_Code),NOCASE,PRIMARY !By Discount Code    
Record                   RECORD,PRE()
Discount_Code               STRING(2)                      !                    
Discount_Rate               REAL                           !                    
                         END
                     END                       

LOCVALUE             FILE,DRIVER('Btrieve'),OEM,NAME('LOCVALUE.DAT'),PRE(lov),CREATE,BINDABLE,THREAD !Location, End Of Period Values
RecordNumberKey          KEY(lov:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(lov:Location,lov:TheDate),DUP,NOCASE !By Date             
DateOnly                 KEY(lov:TheDate),DUP,NOCASE,OPT   !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !Location            
TheDate                     DATE                           !The Date            
PurchaseTotal               REAL                           !Purchase Cost Total 
SaleCostTotal               REAL                           !SaleCostTotal       
RetailCostTotal             REAL                           !RetailCostTotal     
QuantityTotal               LONG                           !QuantityTotal       
                         END
                     END                       

STOCKTYP             FILE,DRIVER('Btrieve'),NAME('STOCKTYP.DAT'),PRE(stp),CREATE,BINDABLE,THREAD !Stock Type For Loan/Exhange Units
Stock_Type_Key           KEY(stp:Stock_Type),NOCASE,PRIMARY !By Stock_Type       
Use_Loan_Key             KEY(stp:Use_Loan,stp:Stock_Type),DUP,NOCASE !By Stock Type       
Use_Exchange_Key         KEY(stp:Use_Exchange,stp:Stock_Type),DUP,NOCASE !By Stock Type       
Record                   RECORD,PRE()
Use_Loan                    STRING(3)                      !                    
Use_Exchange                STRING(3)                      !                    
Stock_Type                  STRING(30)                     !                    
Available                   BYTE                           !Available           
FranchiseViewOnly           BYTE                           !Franchise View Only Stock Type
                         END
                     END                       

COURIER              FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(cou),CREATE,BINDABLE,THREAD !Couriers            
Courier_Key              KEY(cou:Courier),NOCASE,PRIMARY   !By Courier          
Courier_Type_Key         KEY(cou:Courier_Type,cou:Courier),DUP,NOCASE !By Courier_Type     
Record                   RECORD,PRE()
Courier                     STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Postcode                    STRING(10)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Contact_Name                STRING(60)                     !                    
Export_Path                 STRING(255)                    !                    
Include_Estimate            STRING(3)                      !                    
Include_Chargeable          STRING(3)                      !                    
Include_Warranty            STRING(3)                      !                    
Service                     STRING(15)                     !                    
Import_Path                 STRING(255)                    !                    
Courier_Type                STRING(30)                     !                    
Last_Despatch_Time          TIME                           !                    
ICOLSuffix                  STRING(3)                      !ANC ICOL File Suffix
ContractNumber              STRING(20)                     !ANC Contract Number 
ANCPath                     STRING(255)                    !Path to ANCPAPER.EXE  (I.e. s:\apps\service)
ANCCount                    LONG                           !Count Of Each ANC Export
DespatchClose               STRING(3)                      !Despatch At Closing 
LabGOptions                 STRING(60)                     !Label G Command Line Options
CustomerCollection          BYTE                           !Customer Collection 
NewStatus                   STRING(30)                     !On Despatch Change Status To
NoOfDespatchNotes           LONG                           !No Of Despatch Notes
AutoConsignmentNo           BYTE                           !Auto Allocate Consignment Number
LastConsignmentNo           LONG                           !Last Allocated Consignment Number
PrintLabel                  BYTE                           !Print Label         
PrintWaybill                BYTE                           !Print Waybill       
StartWorkHours              TIME                           !Start Time          
EndWorkHours                TIME                           !End Time            
IncludeSaturday             BYTE                           !Include Saturday    
IncludeSunday               BYTE                           !Include Sunday      
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
EmailAddress                STRING(255)                    !Email Address       
FromEmailAddress            STRING(255)                    !From Email Address  
                         END
                     END                       

TRDPARTY             FILE,DRIVER('Btrieve'),NAME('TRDPARTY.DAT'),PRE(trd),CREATE,BINDABLE,THREAD !Third Party Repairer
Company_Name_Key         KEY(trd:Company_Name),NOCASE,PRIMARY !By Company Name     
Account_Number_Key       KEY(trd:Account_Number),NOCASE    !By Account_Number   
Special_Instructions_Key KEY(trd:Special_Instructions),DUP,NOCASE !By Special Instructions
Courier_Only_Key         KEY(trd:Courier),DUP,NOCASE       !                    
DeactivateCompanyKey     KEY(trd:Deactivate,trd:Company_Name),DUP,NOCASE !By Company Name     
ASCIDKey                 KEY(trd:ASCID),DUP,NOCASE         !By ASC ID           
Record                   RECORD,PRE()
Company_Name                STRING(30)                     !                    
Account_Number              STRING(30)                     !                    
Contact_Name                STRING(60)                     !                    
Postcode                    STRING(15)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Turnaround_Time             REAL                           !                    
Courier_Direct              STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Print_Order                 STRING(15)                     !                    
Special_Instructions        STRING(30)                     !                    
Batch_Number                LONG                           !                    
BatchLimit                  LONG                           !                    
VATRate                     REAL                           !V.A.T. Rate         
Markup                      REAL                           !Markup              
MOPSupplierNumber           STRING(30)                     !MOP Supplier Number 
MOPItemID                   STRING(30)                     !MOP Item ID         
Deactivate                  BYTE                           !Deactivate Account  
LHubAccount                 BYTE                           !L-Hub Account       
ASCID                       STRING(30)                     !ASC ID              
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn),CREATE,BINDABLE,THREAD !Notes Attached To Jobs
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY !By Job Number       
Record                   RECORD,PRE()
RefNumber                   LONG                           !Related Job Number  
Fault_Description           STRING(255)                    !                    
Engineers_Notes             STRING(255)                    !                    
Invoice_Text                STRING(255)                    !                    
Collection_Text             STRING(255)                    !                    
Delivery_Text               STRING(255)                    !                    
ColContatName               STRING(30)                     !Contact Name        
ColDepartment               STRING(30)                     !Department          
DelContactName              STRING(30)                     !Contact Name        
DelDepartment               STRING(30)                     !Department          
                         END
                     END                       

TRANTYPE             FILE,DRIVER('Btrieve'),NAME('TRANTYPE.DAT'),PRE(trt),CREATE,BINDABLE,THREAD !Transit Types       
Transit_Type_Key         KEY(trt:Transit_Type),NOCASE,PRIMARY !By Transit_Type     
RRCKey                   KEY(trt:RRC,trt:Transit_Type),DUP,NOCASE !By Transit Type     
ARCKey                   KEY(trt:ARC,trt:Transit_Type),DUP,NOCASE !By Transit Type     
Record                   RECORD,PRE()
Transit_Type                STRING(30)                     !                    
Courier_Collection_Report   STRING(3)                      !                    
Collection_Address          STRING(3)                      !                    
Delivery_Address            STRING(3)                      !                    
Loan_Unit                   STRING(3)                      !                    
Exchange_Unit               STRING(3)                      !                    
Force_DOP                   STRING(3)                      !                    
Force_Location              STRING(3)                      !                    
Skip_Workshop               STRING(3)                      !                    
HideLocation                BYTE                           !Hide Location       
InternalLocation            STRING(30)                     !Location            
Workshop_Label              STRING(3)                      !                    
Job_Card                    STRING(3)                      !                    
JobReceipt                  STRING(3)                      !Print Job Receipt   
Initial_Status              STRING(30)                     !                    
Initial_Priority            STRING(30)                     !                    
ExchangeStatus              STRING(30)                     !                    
LoanStatus                  STRING(30)                     !                    
Location                    STRING(3)                      !                    
ANCCollNote                 STRING(3)                      !Print ANC Collection Note
InWorkshop                  STRING(3)                      !                    
HubRepair                   BYTE                           !Hub Repair          
ARC                         BYTE                           !A.R.C.              
RRC                         BYTE                           !R.R.C.              
OBF                         BYTE                           !                    
WaybillComBook              BYTE                           !Waybill Compulsory At Booking
WaybillComComp              BYTE                           !Waybill Compulsory At Completion
                         END
                     END                       

MANFAURL             FILE,DRIVER('Btrieve'),OEM,NAME('MANFAURL.DAT'),PRE(mnr),CREATE,BINDABLE,THREAD !Related Fault Code  
RecordNumberKey          KEY(mnr:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(mnr:MANFAULORecordNumber,mnr:PartFaultCode,mnr:FieldNumber),DUP,NOCASE !By Field Number     
LinkedRecordNumberKey    KEY(mnr:MANFAULORecordNumber,mnr:PartFaultCode,mnr:LinkedRecordNumber),DUP,NOCASE !By Linked Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANFAULORecordNumber        LONG                           !MANFPALO Record Number
FieldNumber                 LONG                           !Field Number        
LinkedRecordNumber          LONG                           !Linked Record Number
PartFaultCode               BYTE                           !Is This For A Part Fault Code?
RelatedPartFaultCode        LONG                           !Related Part Fault Code
                         END
                     END                       

MODELCCT             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCCT.DAT'),PRE(mcc),CREATE,BINDABLE,THREAD !CCT Reference / Model File
RecordNumberKey          KEY(mcc:RecordNumber),NOCASE,PRIMARY !By Record Number    
CCTRefKey                KEY(mcc:ModelNumber,mcc:CCTReferenceNumber),DUP,NOCASE !By CCT Reference Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
ModelNumber                 STRING(30)                     !Model Number        
CCTReferenceNumber          STRING(30)                     !CCT Reference Number
                         END
                     END                       

MANFAUEX             FILE,DRIVER('Btrieve'),OEM,NAME('MANFAUEX.DAT'),PRE(max),CREATE,BINDABLE,THREAD !Fault Code Model Exceptions
RecordNumberKey          KEY(max:RecordNumber),NOCASE,PRIMARY !By Record Number    
ModelNumberKey           KEY(max:MANFAULORecordNumber,max:ModelNumber),DUP,NOCASE !By Model Number..   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANFAULORecordNumber        LONG                           !Link To MANFAULO File
ModelNumber                 STRING(30)                     !Model Number        
                         END
                     END                       

MANFPARL             FILE,DRIVER('Btrieve'),OEM,NAME('MANFPARL.DAT'),PRE(mpr),CREATE,BINDABLE,THREAD !Related Part Fault Code
RecordNumberKey          KEY(mpr:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(mpr:MANFPALORecordNumber,mpr:JobFaultCode,mpr:FieldNumber),DUP,NOCASE !By Field Number     
LinkedRecordNumberKey    KEY(mpr:MANFPALORecordNumber,mpr:JobFaultCode,mpr:LinkedRecordNumber),DUP,NOCASE !By Linked Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANFPALORecordNumber        LONG                           !MANFAULO Record Number
FieldNumber                 LONG                           !Field Number        
LinkedRecordNumber          LONG                           !Linked Record Number
JobFaultCode                BYTE                           !Is This For A Job Fault Code?
                         END
                     END                       

ESNMODEL             FILE,DRIVER('Btrieve'),NAME('ESNMODEL.DAT'),PRE(esn),CREATE,BINDABLE,THREAD !ESN And Model Number File
Record_Number_Key        KEY(esn:Record_Number),NOCASE,PRIMARY !By Record Number    
ESN_Key                  KEY(esn:ESN,esn:Model_Number),DUP,NOCASE !By ESN              
ESN_Only_Key             KEY(esn:ESN),DUP,NOCASE           !By ESN              
Model_Number_Key         KEY(esn:Model_Number,esn:ESN),DUP,NOCASE !                    
Manufacturer_Key         KEY(esn:Manufacturer,esn:ESN),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
ESN                         STRING(8)                      !I.M.E.I. Number     
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
Include48Hour               BYTE                           !Include For 48 Hour Exchange
Active                      STRING(1)                      !                    
                         END
                     END                       

JOBPAYMT             FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt),CREATE,BINDABLE,THREAD !Job Payments        
Record_Number_Key        KEY(jpt:Record_Number),NOCASE,PRIMARY !By Record Number    
All_Date_Key             KEY(jpt:Ref_Number,jpt:Date),DUP,NOCASE !By Date             
Loan_Deposit_Key         KEY(jpt:Ref_Number,jpt:Loan_Deposit,jpt:Date),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Payment_Type                STRING(30)                     !                    
Credit_Card_Number          STRING(20)                     !                    
Expiry_Date                 STRING(5)                      !                    
Issue_Number                STRING(5)                      !                    
Amount                      REAL                           !                    
User_Code                   STRING(3)                      !                    
Loan_Deposit                BYTE                           !Loan Deposit        
                         END
                     END                       

MANREJR              FILE,DRIVER('Btrieve'),OEM,NAME('MANREJR.DAT'),PRE(mar),CREATE,BINDABLE,THREAD !Manufacturer Rejection Reasons
RecordNumberKey          KEY(mar:RecordNumber),NOCASE,PRIMARY !By Record Number    
CodeKey                  KEY(mar:MANRecordNumber,mar:CodeNumber),NOCASE !By Code             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANRecordNumber             LONG                           !MANUFACT Record Number
CodeNumber                  STRING(60)                     !CodeNumber          
Description                 STRING(255)                    !Description         
                         END
                     END                       

STOCK                FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto),CREATE,BINDABLE,THREAD !Stock Control       
Ref_Number_Key           KEY(sto:Ref_Number),NOCASE,PRIMARY !By Ref Number       
Sundry_Item_Key          KEY(sto:Sundry_Item),DUP,NOCASE   !By Sundry Item      
Description_Key          KEY(sto:Location,sto:Description),DUP,NOCASE !By Description      
Description_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Description),DUP,NOCASE !By Description      
Shelf_Location_Key       KEY(sto:Location,sto:Shelf_Location),DUP,NOCASE !By Shelf Location   
Shelf_Location_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE !By Shelf Location   
Supplier_Accessory_Key   KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE !By Supplier         
Manufacturer_Key         KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Manufacturer     
Supplier_Key             KEY(sto:Location,sto:Suspend,sto:Supplier),DUP,NOCASE !By Supplier         
Location_Key             KEY(sto:Location,sto:Part_Number),DUP,NOCASE !By Location         
Part_Number_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Part_Number),DUP,NOCASE !By Part Number      
Ref_Part_Description_Key KEY(sto:Location,sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE !Why?                
Location_Manufacturer_Key KEY(sto:Location,sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Manufacturer     
Manufacturer_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Part Number      
Location_Part_Description_Key KEY(sto:Location,sto:Part_Number,sto:Description),DUP,NOCASE !By Part Number      
Ref_Part_Description2_Key KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE !CASCADE Key         
Minimum_Part_Number_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE !By Part Number      
Minimum_Description_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE !                    
SecondLocKey             KEY(sto:Location,sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE !By Part Number      
RequestedKey             KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE !By Part Number      
ExchangeAccPartKey       KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE !By Part Number      
ExchangeAccDescKey       KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE !By Description      
DateBookedKey            KEY(sto:DateBooked),DUP,NOCASE    !By Date Booked      
Supplier_Only_Key        KEY(sto:Supplier),DUP,NOCASE      !                    
LocPartSuspendKey        KEY(sto:Location,sto:Suspend,sto:Part_Number),DUP,NOCASE !By Part Number      
LocDescSuspendKey        KEY(sto:Location,sto:Suspend,sto:Description),DUP,NOCASE !By Description      
LocShelfSuspendKey       KEY(sto:Location,sto:Suspend,sto:Shelf_Location),DUP,NOCASE !By Shelf Location   
LocManSuspendKey         KEY(sto:Location,sto:Suspend,sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Part Number      
ExchangeModelKey         KEY(sto:Location,sto:Manufacturer,sto:ExchangeModelNumber),DUP,NOCASE !                    
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)                      !                    
Ref_Number                  LONG                           !Reference Number    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
AccessoryCost               REAL                           !Accessory Cost      
Percentage_Mark_Up          REAL                           !                    
Shelf_Location              STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
Location                    STRING(30)                     !                    
Second_Location             STRING(30)                     !                    
Quantity_Stock              REAL                           !                    
Quantity_To_Order           REAL                           !                    
Quantity_On_Order           REAL                           !                    
Minimum_Level               REAL                           !                    
Reorder_Level               REAL                           !                    
Use_VAT_Code                STRING(3)                      !                    
Service_VAT_Code            STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Superceeded                 STRING(3)                      !                    
Superceeded_Ref_Number      REAL                           !                    
Pending_Ref_Number          REAL                           !                    
Accessory                   STRING(3)                      !                    
Minimum_Stock               STRING(3)                      !                    
Assign_Fault_Codes          STRING(3)                      !                    
Individual_Serial_Numbers   STRING(3)                      !                    
ExchangeUnit                STRING(3)                      !                    
QuantityRequested           LONG                           !Quantity Requested  
Suspend                     BYTE                           !Suspend Part        
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
ReturnFaultySpare           BYTE                           !Return Faulty Spare 
ChargeablePartOnly          BYTE                           !Chargeable Part Only
AttachBySolder              BYTE                           !Attach By Solder    
AllowDuplicate              BYTE                           !Allow Duplicate Part On Jobs
RetailMarkup                REAL                           !Percentage Mark Up  
VirtPurchaseCost            REAL                           !Purchase Cost       
VirtTradeCost               REAL                           !Trade Price         
VirtRetailCost              REAL                           !Retail Price        
VirtTradeMarkup             REAL                           !Percent Mark Up     
VirtRetailMarkup            REAL                           !Percentage Mark Up  
AveragePurchaseCost         REAL                           !Average Purchase Cost
PurchaseMarkUp              REAL                           !Percentage Mark Up  
RepairLevel                 LONG                           !Repair Index        
SkillLevel                  LONG                           !Skill Level         
DateBooked                  DATE                           !Date Booked         
AccWarrantyPeriod           LONG                           !Accessory Warranty Period
ExcludeLevel12Repair        BYTE                           !Exclude From Level 1 && 2 Repairs
ExchangeOrderCap            LONG                           !Exchange Order Cap  
ExchangeModelNumber         STRING(30)                     !                    
                         END
                     END                       

ORDWEBPR             FILE,DRIVER('Btrieve'),OEM,NAME('ORDWEBPR.DAT'),PRE(orw),CREATE,BINDABLE,THREAD !Pending Web Orders  
RecordNumberKey          KEY(orw:RecordNumber),NOCASE,PRIMARY !By Record Number.   
PartNumberKey            KEY(orw:AccountNumber,orw:PartNumber),DUP,NOCASE !By Part Number      
DescriptionKey           KEY(orw:AccountNumber,orw:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccountNumber               STRING(30)                     !AccountNumber       
PartNumber                  STRING(30)                     !Part Number         
Description                 STRING(30)                     !Description         
Quantity                    LONG                           !Quantity            
ItemCost                    REAL                           !Item Cost           
                         END
                     END                       

ESREJRES             FILE,DRIVER('Btrieve'),OEM,NAME('ESREJRES.DAT'),PRE(esr),CREATE,BINDABLE,THREAD !Estimate Rejection Reasons
RecordNumberKey          KEY(esr:RecordNumber),NOCASE,PRIMARY !By Record Number    
RejectionReasonKey       KEY(esr:RejectionReason),NOCASE   !By Rejection Reason 
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RejectionReason             STRING(60)                     !Rejection Reason    
                         END
                     END                       

MANMARK              FILE,DRIVER('Btrieve'),OEM,NAME('MANMARK.DAT'),PRE(mak),CREATE,BINDABLE,THREAD !Manufacturer In Warranty Markups
RecordNumberKey          KEY(mak:RecordNumber),NOCASE,PRIMARY !By Record Number    
SiteLocationKey          KEY(mak:RefNumber,mak:SiteLocation),NOCASE !By Site Location    
SiteLocationOnlyKey      KEY(mak:SiteLocation),DUP,NOCASE  !By Site Location    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
SiteLocation                STRING(30)                     !Site Location       
InWarrantyMarkup            LONG                           !In Warranty Markup  
                         END
                     END                       

MODPROD              FILE,DRIVER('Btrieve'),OEM,NAME('MODPROD.DAT'),PRE(mop),CREATE,BINDABLE,THREAD !Model Related Product Code
RecordNumberKey          KEY(mop:RecordNumber),NOCASE,PRIMARY !By Record Number    
ProductCodeKey           KEY(mop:ModelNumber,mop:ProductCode),NOCASE !By Model No/Product Code
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
ModelNumber                 STRING(30)                     !Model Number        
ProductCode                 STRING(30)                     !Product Code        
                         END
                     END                       

MANUDATE             FILE,DRIVER('Btrieve'),OEM,NAME('MANUDATE.DAT'),PRE(mad),CREATE,BINDABLE,THREAD !Manufacturer Date Codes
RecordNumberKey          KEY(mad:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateCodeKey              KEY(mad:Manufacturer,mad:DateCode),NOCASE !By Date Code        
AlcatelDateCodeKey       KEY(mad:Manufacturer,mad:AlcatelYearCode,mad:AlcatelMonthCode,mad:AlcatelDayCode),DUP,NOCASE !By Date Code        
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !Manufacturer        
DateCode                    STRING(6)                      !Date Code           
TheYear                     STRING(4)                      !Year                
TheMonth                    STRING(2)                      !Month               
AlcatelYearCode             STRING(1)                      !Year Code           
AlcatelYear                 STRING(4)                      !Year                
AlcatelMonthCode            STRING(1)                      !Month Code          
AlcatelMonth                STRING(2)                      !Month               
AlcatelDayCode              STRING(1)                      !Day Code            
AlcatelDay                  STRING(2)                      !Day                 
                         END
                     END                       

TRAFAULO             FILE,DRIVER('Btrieve'),NAME('TRAFAULO.DAT'),PRE(tfo),CREATE,BINDABLE,THREAD !Trade Account Fault Coding Lookup Table
Field_Key                KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Field,tfo:Description),NOCASE,PRIMARY !By Field            
DescriptionKey           KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccountNumber               STRING(30)                     !Account Number      
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
                         END
                     END                       

WEBJOBNO             FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOBNO.DAT'),PRE(wej),CREATE,BINDABLE,THREAD !Web Job Number Control File
RecordNumberKey          KEY(wej:RecordNumber),NOCASE,PRIMARY !By Record Number    
HeadAccountNumberKey     KEY(wej:HeadAccountNumber),DUP,NOCASE !By Head Account Number
JobNumberKey             KEY(wej:LastWEBJOBNumber),DUP,NOCASE !By Job Number       
HeadJobNumberKey         KEY(wej:HeadAccountNumber,wej:LastWEBJOBNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
HeadAccountNumber           STRING(30)                     !Head Account Number 
LastWEBJOBNumber            LONG                           !Last WEBJOB Number  
                         END
                     END                       

TRAFAULT             FILE,DRIVER('Btrieve'),NAME('TRAFAULT.DAT'),PRE(taf),CREATE,BINDABLE,THREAD !Trade Account Fault Coding On Jobs
RecordNumberKey          KEY(taf:RecordNumber),NOCASE,PRIMARY !                    
Field_Number_Key         KEY(taf:AccountNumber,taf:Field_Number),NOCASE !By Field Number     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccountNumber               STRING(30)                     !Account Number      
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
Compulsory_At_Booking       STRING(3)                      !                    
ReplicateFault              STRING(3)                      !Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                      !Replicate To Fault Description Field
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),NAME('JOBLOHIS.DAT'),PRE(jlh),CREATE,BINDABLE,THREAD !Job Loan History    
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE !By Date             
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
record_number               REAL                           !                    
Loan_Unit_Number            REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
                         END
                     END                       

LOAN                 FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa),CREATE,BINDABLE,THREAD !Loan Units          
Ref_Number_Key           KEY(loa:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
ESN_Only_Key             KEY(loa:ESN),DUP,NOCASE           !                    
MSN_Only_Key             KEY(loa:MSN),DUP,NOCASE           !                    
Ref_Number_Stock_Key     KEY(loa:Stock_Type,loa:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(loa:Stock_Type,loa:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(loa:Stock_Type,loa:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
Model_Available_Key      KEY(loa:Available,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(loa:Stock_Type),DUP,NOCASE    !By Stock Type       
ModelRefNoKey            KEY(loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(loa:Available,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(loa:Available,loa:MSN),DUP,NOCASE !By M.S.N            
AvailRefOnlyKey          KEY(loa:Available,loa:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(loa:Date_Booked),DUP,NOCASE   !By Date Booked      
LocStockAvailIMEIKey     KEY(loa:Location,loa:Stock_Type,loa:Available,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(loa:Location,loa:Stock_Type,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(loa:Location,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(loa:Location,loa:Stock_Type,loa:Available,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockRefKey           KEY(loa:Location,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocRefKey                KEY(loa:Location,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockAvailModelKey    KEY(loa:Location,loa:Stock_Type,loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockModelKey         KEY(loa:Location,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocModelKey              KEY(loa:Location,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockAvailMSNKey      KEY(loa:Location,loa:Stock_Type,loa:Available,loa:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(loa:Location,loa:Stock_Type,loa:MSN),DUP,NOCASE !By Loan Unit Number 
LocMSNKey                KEY(loa:Location,loa:MSN),DUP,NOCASE !By M.S.N.           
AvailLocIMEI             KEY(loa:Available,loa:Location,loa:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(loa:Available,loa:Location,loa:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(loa:Available,loa:Location,loa:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(loa:Available,loa:Location,loa:Model_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !                    
Stock_Type                  STRING(30)                     !                    
DummyField                  STRING(1)                      !For File Manager    
StatusChangeDate            DATE                           !Status Change Date  
                         END
                     END                       

NOTESCON             FILE,DRIVER('Btrieve'),NAME('NOTESCON.DAT'),PRE(noc),CREATE,BINDABLE,THREAD !Notes - Contact History
RecordNumberKey          KEY(noc:RecordNumber),NOCASE,PRIMARY !By Record Number    
Notes_Key                KEY(noc:Reference,noc:Notes),NOCASE !By Notes            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Reference                   STRING(30)                     !                    
Notes                       STRING(80)                     !                    
                         END
                     END                       

VATCODE              FILE,DRIVER('Btrieve'),NAME('VATCODE.DAT'),PRE(vat),CREATE,BINDABLE,THREAD !Vat Code            
Vat_code_Key             KEY(vat:VAT_Code),NOCASE,PRIMARY  !By V.A.T. Code      
Record                   RECORD,PRE()
VAT_Code                    STRING(2)                      !                    
VAT_Rate                    REAL                           !                    
                         END
                     END                       

SMSMAIL              FILE,DRIVER('Btrieve'),OEM,NAME('SMSMAIL.DAT'),PRE(sms),CREATE,BINDABLE,THREAD !SMS Email File      
RecordNumberKey          KEY(sms:RecordNumber),NOCASE,PRIMARY !By Record Number    
SMSSentKey               KEY(sms:SendToSMS,sms:SMSSent),DUP,NOCASE !SMS Sent Key        
EmailSentKey             KEY(sms:SendToEmail,sms:EmailSent),DUP,NOCASE !By Email Sent       
SMSSBUpdateKey           KEY(sms:SMSSent,sms:SBUpdated),DUP,NOCASE !                    
EmailSBUpdateKey         KEY(sms:EmailSent,sms:SBUpdated),DUP,NOCASE !                    
DateTimeInsertedKey      KEY(sms:RefNumber,sms:DateInserted,sms:TimeInserted),DUP,NOCASE !By Date Time        
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
MSISDN                      STRING(12)                     !Mobile Number       
MSG                         STRING(255)                    !Message             
EmailAddress                STRING(255)                    !EmailAddress        
SendToSMS                   STRING(1)                      !Send To SMS         
SendToEmail                 STRING(1)                      !Send To Email       
SMSSent                     STRING(1)                      !SMS Sent            
DateInserted                DATE                           !Date Inserted       
TimeInserted                TIME                           !Time Inserted       
EmailSent                   STRING(1)                      !Email Sent          
DateSMSSent                 DATE                           !Date SMS Sent       
TimeSMSSent                 TIME                           !Time SMS Sent       
DateEmailSent               DATE                           !Date Email Sent     
TimeEmailSent               TIME                           !Time Email Sent     
PathToEstimate              STRING(255)                    !Path To Estimate    
SBUpdated                   BYTE                           !ServiceBase Updated 
                         END
                     END                       

LOANHIST             FILE,DRIVER('Btrieve'),NAME('LOANHIST.DAT'),PRE(loh),CREATE,BINDABLE,THREAD !Loan History        
Record_Number_Key        KEY(loh:record_number),NOCASE,PRIMARY !By Record Number    
Ref_Number_Key           KEY(loh:Ref_Number,-loh:Date,-loh:Time),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
record_number               REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
                         END
                     END                       

EXCHHIST             FILE,DRIVER('Btrieve'),NAME('EXCHHIST.DAT'),PRE(exh),CREATE,BINDABLE,THREAD !Exchange History    
Record_Number_Key        KEY(exh:record_number),NOCASE,PRIMARY !By Record Number    
Ref_Number_Key           KEY(exh:Ref_Number,-exh:Date,-exh:Time),DUP,NOCASE !By Ref Number       
DateStatusKey            KEY(exh:Date,exh:Status,exh:Ref_Number),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
record_number               LONG                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
Notes                       STRING(255)                    !                    
                         END
                     END                       

ACCESSOR             FILE,DRIVER('Btrieve'),NAME('ACCESSOR.DAT'),PRE(acr),CREATE,BINDABLE,THREAD !Accessories         
Accesory_Key             KEY(acr:Model_Number,acr:Accessory),NOCASE,PRIMARY !By Accessory        
Model_Number_Key         KEY(acr:Accessory,acr:Model_Number),NOCASE !By Model Number     
AccessOnlyKey            KEY(acr:Accessory),DUP,NOCASE     !By Accessory        
Record                   RECORD,PRE()
Accessory                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
                         END
                     END                       

SUBACCAD             FILE,DRIVER('Btrieve'),OEM,NAME('SUBACCAD.DAT'),PRE(sua),CREATE,BINDABLE,THREAD !Sub Account Addresses
RecordNumberKey          KEY(sua:RecordNumber),NOCASE,PRIMARY !By Record Number    
AccountNumberKey         KEY(sua:RefNumber,sua:AccountNumber),DUP,NOCASE !By Account Number   
CompanyNameKey           KEY(sua:RefNumber,sua:CompanyName),DUP,NOCASE !By Company Name     
AccountNumberOnlyKey     KEY(sua:AccountNumber),NOCASE     !By Account Number   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !RefNumber           
AccountNumber               STRING(15)                     !                    
CompanyName                 STRING(30)                     !                    
Postcode                    STRING(15)                     !                    
AddressLine1                STRING(30)                     !                    
AddressLine2                STRING(30)                     !                    
AddressLine3                STRING(30)                     !                    
TelephoneNumber             STRING(15)                     !                    
FaxNumber                   STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
ContactName                 STRING(30)                     !                    
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(wpr),CREATE,BINDABLE,THREAD !Warrant Parts On A Job
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE !By Part Number      
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY !                    
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE  !By Date Ordered     
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE      !By Supplier         
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE !By Order Part Number
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE !By Date Ordered     
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE !By Date Received    
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE !By Part Number      
WebOrderKey              KEY(wpr:WebOrder,wpr:Part_Number),DUP,NOCASE !By Part Number      
PartAllocatedKey         KEY(wpr:PartAllocated,wpr:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(wpr:Status,wpr:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(wpr:PartAllocated,wpr:Status,wpr:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Job Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Stock Reference Number
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Quantity                    REAL                           !                    
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Reference Number
Order_Number                LONG                           !Order Number        
Order_Part_Number           REAL                           !                    
Date_Received               DATE                           !                    
Status_Date                 DATE                           !                    
Main_Part                   STRING(3)                      !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Requested For Order 
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
WebOrder                    BYTE                           !Web Order           
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
CostAdjustment              REAL                           !Cost Adjustment     
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
ExchangeUnit                BYTE                           !Exchange Unit Part  
SecondExchangeUnit          BYTE                           !Second Exchange Unit
Correction                  BYTE                           !Part Correction     
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par),CREATE,BINDABLE,THREAD !Parts On A Job      
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE !By Part Number      
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY !                    
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE !By Part_Ref_Number  
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE  !By Date Ordered     
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(par:Supplier),DUP,NOCASE      !By Supplier         
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE !By Order Part Number
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE !By Date Ordered     
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE !By Date Received    
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE !By Part Number      
WebOrderKey              KEY(par:WebOrder,par:Part_Number),DUP,NOCASE !By Part Number      
PartAllocatedKey         KEY(par:PartAllocated,par:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(par:Status,par:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(par:PartAllocated,par:Status,par:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Job Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Stock Reference Number
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !In Warranty Cost    
Sale_Cost                   REAL                           !Out Warranty Cost   
Retail_Cost                 REAL                           !                    
Quantity                    REAL                           !                    
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Reference Number
Order_Number                LONG                           !Order Number        
Order_Part_Number           LONG                           !Order Reference Number
Date_Received               DATE                           !                    
Status_Date                 DATE                           !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Parts Requested For Order
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
WebOrder                    BYTE                           !Web Order           
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
ExchangeUnit                BYTE                           !Exchange Unit       
SecondExchangeUnit          BYTE                           !Second Exchange Unit
Correction                  BYTE                           !Part Correction     
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),NAME('JOBACC.DAT'),PRE(jac),CREATE,BINDABLE,THREAD !Job Accesories      
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY !By Accessory        
DamagedKey               KEY(jac:Ref_Number,jac:Damaged,jac:Accessory),DUP,NOCASE !By Accessory        
DamagedPirateKey         KEY(jac:Ref_Number,jac:Damaged,jac:Pirate,jac:Accessory),DUP,NOCASE !By Accessory        
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Accessory                   STRING(30)                     !                    
Damaged                     BYTE                           !Damaged             
Pirate                      BYTE                           !Pirate              
Attached                    BYTE                           !Attached At RRC     
                         END
                     END                       

STOCKALL             FILE,DRIVER('Btrieve'),OEM,NAME('STOCKALL.DAT'),PRE(stl),CREATE,BINDABLE,THREAD !Stock Allocation    
RecordNumberKey          KEY(stl:RecordNumber),NOCASE,PRIMARY !By Record Number    
PartNumberKey            KEY(stl:Location,stl:PartNumber),DUP,NOCASE !By Part Number      
StatusPartNumberKey      KEY(stl:Location,stl:Status,stl:PartNumber),DUP,NOCASE !By Part Number      
DescriptionKey           KEY(stl:Location,stl:Description),DUP,NOCASE !By Description      
StatusDescriptionKey     KEY(stl:Location,stl:Status,stl:Description),DUP,NOCASE !By Description      
ShelfLocationKey         KEY(stl:Location,stl:ShelfLocation),DUP,NOCASE !By Shelf Location   
StatusShelfLocationKey   KEY(stl:Location,stl:Status,stl:ShelfLocation),DUP,NOCASE !By Shelf Location   
EngineerKey              KEY(stl:Location,stl:Engineer,stl:JobNumber),DUP,NOCASE !By Job Number       
StatusEngineerKey        KEY(stl:Location,stl:Status,stl:Engineer,stl:JobNumber),DUP,NOCASE !By Job Number       
JobNumberKey             KEY(stl:Location,stl:JobNumber),DUP,NOCASE !By Job Number       
StatusJobNumberKey       KEY(stl:Location,stl:Status,stl:JobNumber),DUP,NOCASE !By Job Number       
PartRecordNumberKey      KEY(stl:Location,stl:PartType,stl:PartRecordNumber),DUP,NOCASE !By Part Record Number
PartRecordTypeKey        KEY(stl:PartType,stl:PartRecordNumber),DUP,NOCASE !By Part Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !Location            
ShelfLocation               STRING(30)                     !Shelf Location      
PartNumber                  STRING(30)                     !Part Number         
Description                 STRING(30)                     !Description         
Engineer                    STRING(3)                      !Engineer            
Status                      STRING(3)                      !Status              
JobNumber                   LONG                           !Job Number          
PartRefNumber               LONG                           !Part Ref Number     
PartRecordNumber            LONG                           !Part Record Number  
PartType                    STRING(3)                      !Chargeable/Warranty 
Quantity                    LONG                           !Quantity            
                         END
                     END                       

USELEVEL             FILE,DRIVER('Btrieve'),NAME('USELEVEL.DAT'),PRE(lev),CREATE,BINDABLE,THREAD !User Levels         
User_Level_Key           KEY(lev:User_Level),NOCASE,PRIMARY !By User Level       
Record                   RECORD,PRE()
User_Level                  STRING(30)                     !                    
                         END
                     END                       

ALLLEVEL             FILE,DRIVER('Btrieve'),NAME('ALLLEVEL.DAT'),PRE(all),CREATE,BINDABLE,THREAD !All Access Levels   
Access_Level_Key         KEY(all:Access_Level),NOCASE,PRIMARY !By Access Level     
Record                   RECORD,PRE()
Access_Level                STRING(30)                     !                    
Description                 STRING(500)                    !                    
ExpertUser                  BYTE                           !Expert User         
Administrator               BYTE                           !Administrator       
Engineer                    BYTE                           !Engineer            
Miscellaneous               BYTE                           !Supervisor          
Accounts                    BYTE                           !Accounts            
StockControl                BYTE                           !Stock Control       
                         END
                     END                       

ACCAREAS             FILE,DRIVER('Btrieve'),NAME('ACCAREAS.DAT'),PRE(acc),CREATE,BINDABLE,THREAD !Access Areas        
Access_level_key         KEY(acc:User_Level,acc:Access_Area),NOCASE,PRIMARY !By Access Level     
AccessOnlyKey            KEY(acc:Access_Area),DUP,NOCASE   !                    
Record                   RECORD,PRE()
Access_Area                 STRING(30)                     !                    
User_Level                  STRING(30)                     !                    
DummyField                  STRING(1)                      !For File Manager    
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(aud),CREATE,BINDABLE,THREAD !Audit Trail         
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE !By Date             
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE !By Action           
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE !By User             
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY !                    
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE !By Action           
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE !By Date             
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE !By Action           
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE !By User             
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE !By Job Number       
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE !By Job Number       
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE !By Job Number       
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
record_number               REAL                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Action                      STRING(80)                     !                    
DummyField                  STRING(2)                      !For File Manager    
Notes                       STRING(255)                    !                    
Type                        STRING(3)                      !Job/Exchange/Loan   
                         END
                     END                       

USERS                FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use),CREATE,BINDABLE,THREAD !Users               
User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY !By User Code        
User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE !By Surname          
Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE !By Surname          
User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE !                    
User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE !By Surname          
surname_key              KEY(use:Surname),DUP,NOCASE       !                    
password_key             KEY(use:Password),NOCASE          !                    
Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE !By Name             
Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE !Surname             
Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE !By Surname          
LocationSurnameKey       KEY(use:Location,use:Surname),DUP,NOCASE !By Surname          
LocationForenameKey      KEY(use:Location,use:Forename),DUP,NOCASE !By Forename         
LocActiveSurnameKey      KEY(use:Active,use:Location,use:Surname),DUP,NOCASE !By Surname          
LocActiveForenameKey     KEY(use:Active,use:Location,use:Forename),DUP,NOCASE !By Forename         
TeamStatusKey            KEY(use:IncludeInEngStatus,use:Team,use:Surname),DUP,NOCASE !By Surname          
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Forename                    STRING(30)                     !                    
Surname                     STRING(30)                     !                    
Password                    STRING(20)                     !                    
User_Type                   STRING(15)                     !                    
Job_Assignment              STRING(15)                     !                    
Supervisor                  STRING(3)                      !                    
User_Level                  STRING(30)                     !                    
Logged_In                   STRING(1)                      !                    
Logged_in_date              DATE                           !                    
Logged_In_Time              TIME                           !                    
Restrict_Logins             STRING(3)                      !                    
Concurrent_Logins           REAL                           !                    
Active                      STRING(3)                      !                    
Team                        STRING(30)                     !                    
Location                    STRING(30)                     !                    
Repair_Target               REAL                           !                    
SkillLevel                  LONG                           !Skill Level         
StockFromLocationOnly       BYTE                           !Pick Stock From Users Location Only
EmailAddress                STRING(255)                    !Email Address       
RenewPassword               LONG                           !Renew Password      
PasswordLastChanged         DATE                           !Password Last Changed
RestrictParts               BYTE                           !Restrict Parts Usage To Skill Level
RestrictChargeable          BYTE                           !Chargeable          
RestrictWarranty            BYTE                           !Warranty            
IncludeInEngStatus          BYTE                           !Include In Engineer Status (True/False)
                         END
                     END                       

LOCSHELF             FILE,DRIVER('Btrieve'),NAME('LOCSHELF.DAT'),PRE(los),CREATE,BINDABLE,THREAD !Locations - Shelf   
Shelf_Location_Key       KEY(los:Site_Location,los:Shelf_Location),NOCASE,PRIMARY !By Shelf Location   
Record                   RECORD,PRE()
Site_Location               STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
                         END
                     END                       

DEFAULTS             FILE,DRIVER('Btrieve'),NAME('DEFAULTS.DAT'),PRE(def),CREATE,BINDABLE,THREAD !Defaults            
RecordNumberKey          KEY(def:record_number),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
User_Name                   STRING(30)                     !                    
record_number               REAL                           !                    
Version_Number              STRING(10)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Postcode                    STRING(15)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
VAT_Number                  STRING(30)                     !                    
Use_Invoice_Address         STRING(3)                      !                    
Use_For_Order               STRING(3)                      !                    
Invoice_Company_Name        STRING(30)                     !                    
Invoice_Address_Line1       STRING(30)                     !                    
Invoice_Address_Line2       STRING(30)                     !                    
Invoice_Address_Line3       STRING(30)                     !                    
Invoice_Postcode            STRING(15)                     !                    
Invoice_Telephone_Number    STRING(15)                     !                    
Invoice_Fax_Number          STRING(15)                     !                    
InvoiceEmailAddress         STRING(255)                    !Email Address       
Invoice_VAT_Number          STRING(30)                     !                    
OrderCompanyName            STRING(30)                     !Company Name        
OrderAddressLine1           STRING(30)                     !Address             
OrderAddressLine2           STRING(30)                     !Address             
OrderAddressLine3           STRING(30)                     !Address             
OrderPostcode               STRING(30)                     !Postcode            
OrderTelephoneNumber        STRING(30)                     !Telephone Number    
OrderFaxNumber              STRING(30)                     !Fax Number          
OrderEmailAddress           STRING(255)                    !Email Address       
Use_Postcode                STRING(3)                      !                    
Postcode_Path               STRING(255)                    !                    
PostcodeDll                 STRING(3)                      !Use DLL Version Of Postcode
Vat_Rate_Labour             STRING(2)                      !                    
Vat_Rate_Parts              STRING(2)                      !                    
License_Number              STRING(10)                     !                    
Maximum_Users               REAL                           !                    
Warranty_Period             REAL                           !                    
Automatic_Replicate         STRING(3)                      !                    
Automatic_Replicate_Field   STRING(15)                     !                    
Estimate_If_Over            REAL                           !                    
Start_Work_Hours            TIME                           !                    
End_Work_Hours              TIME                           !                    
Include_Saturday            STRING(3)                      !                    
Include_Sunday              STRING(3)                      !                    
Show_Mobile_Number          STRING(3)                      !                    
Show_Loan_Exchange_Details  STRING(3)                      !                    
Use_Credit_Limit            STRING(3)                      !                    
Hide_Physical_Damage        STRING(3)                      !                    
Hide_Insurance              STRING(3)                      !                    
Hide_Authority_Number       STRING(3)                      !                    
Hide_Advance_Payment        STRING(3)                      !                    
HideColour                  STRING(3)                      !                    
HideInCourier               STRING(3)                      !Hide Incoming Courier
HideIntFault                STRING(3)                      !Hide Intermittant Fault
HideProduct                 STRING(3)                      !Hide Product Code   
HideLocation                BYTE                           !Hide Internal Location
Allow_Bouncer               STRING(3)                      !                    
Job_Batch_Number            REAL                           !                    
QA_Required                 STRING(3)                      !                    
QA_Before_Complete          STRING(3)                      !                    
QAPreliminary               STRING(3)                      !Use Preliminary QA Check
QAExchLoan                  STRING(3)                      !QA Required For Exchange/Loan Units Before Despatch
CompleteAtQA                BYTE                           !Complete At QA      
RapidQA                     STRING(3)                      !Use Rapid QA        
ValidateESN                 STRING(3)                      !Validate ESN Before Rapid Job Update
ValidateDesp                STRING(3)                      !Validate ESN At Despatch
Force_Accessory_Check       STRING(3)                      !                    
Force_Initial_Transit_Type  STRING(1)                      !                    
Force_Mobile_Number         STRING(1)                      !                    
Force_Model_Number          STRING(1)                      !                    
Force_Unit_Type             STRING(1)                      !                    
Force_Fault_Description     STRING(1)                      !                    
Force_ESN                   STRING(1)                      !                    
Force_MSN                   STRING(1)                      !                    
Force_Job_Type              STRING(1)                      !                    
Force_Engineer              STRING(1)                      !                    
Force_Invoice_Text          STRING(1)                      !                    
Force_Repair_Type           STRING(1)                      !                    
Force_Authority_Number      STRING(1)                      !                    
Force_Fault_Coding          STRING(1)                      !                    
Force_Spares                STRING(1)                      !                    
Force_Incoming_Courier      STRING(1)                      !                    
Force_Outoing_Courier       STRING(1)                      !                    
Force_DOP                   STRING(1)                      !                    
Order_Number                STRING(1)                      !                    
Customer_Name               STRING(1)                      !                    
ForcePostcode               STRING(1)                      !                    
ForceDelPostcode            STRING(1)                      !                    
ForceCommonFault            STRING(1)                      !Force Common Fault  
Remove_Backgrounds          STRING(3)                      !                    
Use_Loan_Exchange_Label     STRING(3)                      !                    
Use_Job_Label               STRING(3)                      !                    
UseSmallLabel               BYTE                           !Use Small Label     
Job_Label                   STRING(255)                    !                    
add_stock_label             STRING(3)                      !                    
receive_stock_label         STRING(3)                      !                    
QA_Failed_Label             STRING(3)                      !                    
QA_Failed_Report            STRING(3)                      !                    
QAPassLabel                 BYTE                           !Print QA Pass Label 
ThirdPartyNote              STRING(3)                      !Print Single 3rd Party Despatch Note
Vodafone_Import_Path        STRING(255)                    !                    
Label_Printer_Type          STRING(30)                     !                    
Browse_Option               STRING(3)                      !Which Address To Show On Main Browse
ANCCollectionNo             LONG                           !Next ANC Collection Number
Use_Sage                    STRING(3)                      !                    
Global_Nominal_Code         STRING(30)                     !                    
Parts_Stock_Code            STRING(30)                     !                    
Labour_Stock_Code           STRING(30)                     !                    
Courier_Stock_Code          STRING(30)                     !                    
Parts_Description           STRING(60)                     !                    
Labour_Description          STRING(60)                     !                    
Courier_Description         STRING(60)                     !                    
Parts_Code                  STRING(30)                     !                    
Labour_Code                 STRING(30)                     !                    
Courier_Code                STRING(30)                     !                    
User_Name_Sage              STRING(30)                     !                    
Password_Sage               STRING(30)                     !                    
Company_Number_Sage         STRING(30)                     !                    
Path_Sage                   STRING(255)                    !                    
BouncerTime                 LONG                           !Bouncer Time        
ExportPath                  STRING(255)                    !Default Export Path 
PrintBarcode                BYTE                           !Print Barcode       
ChaChargeType               STRING(30)                     !Chargeable Charge Type
WarChargeType               STRING(30)                     !Warranty Charge Type
RetBackOrders               BYTE                           !Retail Back Orders  
RemoveWorkshopDespatch      BYTE                           !Remove From Workshop On Despatch
SummaryOrders               BYTE                           !Summary Parts Orders
EuroRate                    REAL                           !Euro Exchange Rate  
IrishVatRate                REAL                           !Irish V.A.T. Rate   
EmailServerAddress          STRING(255)                    !Email Server Address
EmailServerPort             LONG                           !Email Service Port Number
Job_Label_Accessories       STRING(3)                      !                    
ShowRepTypeCosts            BYTE                           !Show Repair Type Costs
                         END
                     END                       

REPTYCAT             FILE,DRIVER('Btrieve'),OEM,NAME('REPTYCAT.DAT'),PRE(repc),CREATE,BINDABLE,THREAD !Repair Type Categories
RecordNumberKey          KEY(repc:RecordNumber),NOCASE,PRIMARY !By Record Number    
RepairTypeKey            KEY(repc:RepairType),DUP,NOCASE   !By Repair Type      
CategoryKey              KEY(repc:Category,repc:RepairType),DUP,NOCASE !By Repair Type      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RepairType                  STRING(30)                     !Repair Type         
Category                    STRING(30)                     !Category            
                         END
                     END                       

MANFPALO             FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(mfp),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding Lookup Table
RecordNumberKey          KEY(mfp:RecordNumber),NOCASE,PRIMARY !By Record Number    
Field_Key                KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Field),NOCASE !By Field            
DescriptionKey           KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Description),DUP,NOCASE !By Description.     
AvailableFieldKey        KEY(mfp:NotAvailable,mfp:Manufacturer,mfp:Field_Number,mfp:Field),DUP,NOCASE !By Field            
AvailableDescriptionKey  KEY(mfp:Manufacturer,mfp:NotAvailable,mfp:Field_Number,mfp:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
RestrictLookup              BYTE                           !Restrict Lookup To  
RestrictLookupType          BYTE                           !Restrict Lookup Type
NotAvailable                BYTE                           !Not Available       
ForceJobFaultCode           BYTE                           !Force Job Fault Code
ForceFaultCodeNumber        LONG                           !Job Fault Code Number
SetPartFaultCode            BYTE                           !Set Part Fault Code 
SelectPartFaultCode         LONG                           !Select Part Fault Code
PartFaultCodeValue          STRING(30)                     !Part Fault Code Value
JobTypeAvailability         BYTE                           !Job Type Availability
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),NAME('JOBSTAGE.DAT'),PRE(jst),CREATE,BINDABLE,THREAD !Job Stage           
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE !By Ref Number       
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Job_Stage                   STRING(30)                     !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
                         END
                     END                       

MANFAUPA             FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(map),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding on Parts
Field_Number_Key         KEY(map:Manufacturer,map:Field_Number),NOCASE,PRIMARY !By Field Number     
MainFaultKey             KEY(map:Manufacturer,map:MainFault),DUP,NOCASE !By Main Fault       
ScreenOrderKey           KEY(map:Manufacturer,map:ScreenOrder),DUP,NOCASE !By Screen Order     
KeyRepairKey             KEY(map:Manufacturer,map:KeyRepair),DUP,NOCASE !By KeyRepair        
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
ForceFormat                 BYTE                           !Force Format        
FieldFormat                 STRING(30)                     !Field Format        
DateType                    STRING(30)                     !Date Type           
MainFault                   BYTE                           !Main Fault          
UseRelatedJobCode           BYTE                           !Use Related Job Code
NotAvailable                BYTE                           !Not Available       
ScreenOrder                 LONG                           !Screen Order        
CopyFromJobFaultCode        BYTE                           !Copy From Job Fault Code
CopyJobFaultCode            LONG                           !Job Fault Code      
NAForAccessory              BYTE                           !N/A For Accessory   
CompulsoryForAdjustment     BYTE                           !Compulsory For Adjustment
KeyRepair                   BYTE                           !Key Repair          
CCTReferenceFaultCode       BYTE                           !"CCT Reference Fault Code"
NAForSW                     BYTE                           !Fill With "N/A" For Software Upgrade
                         END
                     END                       

LOCINTER             FILE,DRIVER('Btrieve'),NAME('LOCINTER.DAT'),PRE(loi),CREATE,BINDABLE,THREAD !Locations - Internal
Location_Key             KEY(loi:Location),NOCASE,PRIMARY  !By Location         
Location_Available_Key   KEY(loi:Location_Available,loi:Location),DUP,NOCASE !By Location         
Record                   RECORD,PRE()
Location                    STRING(30)                     !                    
Location_Available          STRING(3)                      !                    
Allocate_Spaces             STRING(3)                      !                    
Total_Spaces                REAL                           !                    
Current_Spaces              STRING(6)                      !                    
                         END
                     END                       

STAHEAD              FILE,DRIVER('Btrieve'),NAME('STAHEAD.DAT'),PRE(sth),CREATE,BINDABLE,THREAD !Status Type Heading 
Heading_Key              KEY(sth:Ref_Number,sth:Heading),NOCASE,PRIMARY !By Heading          
Ref_Number_Key           KEY(sth:Ref_Number),DUP,NOCASE    !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Heading                     STRING(30)                     !                    
                         END
                     END                       

NOTESFAU             FILE,DRIVER('Btrieve'),NAME('NOTESFAU.DAT'),PRE(nof),CREATE,BINDABLE,THREAD !Notes - Fault Description
Notes_Key                KEY(nof:Manufacturer,nof:Reference,nof:Notes),NOCASE,PRIMARY !By Notes            
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Reference                   STRING(30)                     !                    
Notes                       STRING(80)                     !                    
                         END
                     END                       

MANFAULO             FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(mfo),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding Lookup Table O
RecordNumberKey          KEY(mfo:RecordNumber),NOCASE,PRIMARY !By Record Number    
RelatedFieldKey          KEY(mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
Field_Key                KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
DescriptionKey           KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
ManFieldKey              KEY(mfo:Manufacturer,mfo:Field),DUP,NOCASE !By Field            
HideFieldKey             KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
HideDescriptionKey       KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
RelatedDescriptionKey    KEY(mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
HideRelatedFieldKey      KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
HideRelatedDescKey       KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
FieldNumberKey           KEY(mfo:Manufacturer,mfo:Field_Number),DUP,NOCASE !By Field Number     
PrimaryLookupKey         KEY(mfo:Manufacturer,mfo:Field_Number,mfo:PrimaryLookup),DUP,NOCASE !By Primary Lookup   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
ImportanceLevel             LONG                           !Repair Index        
SkillLevel                  LONG                           !Skill Level         
RepairType                  STRING(30)                     !Repair Type         
RepairTypeWarranty          STRING(30)                     !Repair Type         
HideFromEngineer            BYTE                           !Hide From Engineer  
ForcePartCode               LONG                           !Force Related Part Code
RelatedPartCode             LONG                           !Related Part Fault Code
PromptForExchange           BYTE                           !                    
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table
ReturnToRRC                 BYTE                           !Description         
RestrictLookup              BYTE                           !Restrict Lookup To  
RestrictLookupType          BYTE                           !Restrict Lookup Type
NotAvailable                BYTE                           !Not Available       
PrimaryLookup               BYTE                           !Primary Lookup      
SetJobFaultCode             BYTE                           !Set Job Fault Code  
SelectJobFaultCode          LONG                           !Select Job Fault Code
JobFaultCodeValue           STRING(30)                     !Job Fault Code Value
JobTypeAvailability         BYTE                           !Job Type Availability
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),NAME('JOBEXHIS.DAT'),PRE(jxh),CREATE,BINDABLE,THREAD !Job Exchange History
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE !By Date             
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
record_number               REAL                           !                    
Loan_Unit_Number            REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
                         END
                     END                       

STARECIP             FILE,DRIVER('Btrieve'),OEM,NAME('STARECIP.DAT'),PRE(str),CREATE,BINDABLE,THREAD !Status Recipients   
RecordNumberKey          KEY(str:RecordNumber),NOCASE,PRIMARY !By Record Number    
RecipientTypeKey         KEY(str:RefNumber,str:RecipientType),NOCASE !By Recipient Type   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
RecipientType               STRING(30)                     !Recipient Type      
                         END
                     END                       

STOPARTS             FILE,DRIVER('Btrieve'),OEM,NAME('STOPARTS.DAT'),PRE(spt),CREATE,BINDABLE,THREAD !Stock Part History  
RecordNumberKey          KEY(spt:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateChangedKey           KEY(spt:STOCKRefNumber,spt:DateChanged,spt:RecordNumber),DUP,NOCASE !By Date Changed     
LocationDateKey          KEY(spt:Location,spt:DateChanged,spt:RecordNumber),DUP,NOCASE !By Date Changed     
LocationNewPartKey       KEY(spt:Location,spt:NewPartNumber,spt:NewDescription),DUP,NOCASE !By New Part Number  
LocationOldPartKey       KEY(spt:Location,spt:OldPartNumber,spt:OldDescription),DUP,NOCASE !By Old Part Number  
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
STOCKRefNumber              LONG                           !Link To STOCK Ref Number
Location                    STRING(30)                     !Location            
DateChanged                 DATE                           !Date Changed        
TimeChanged                 TIME                           !Time Changed        
UserCode                    STRING(3)                      !User Code           
OldPartNumber               STRING(30)                     !Old Part Number     
OldDescription              STRING(30)                     !Old Description     
NewPartNumber               STRING(30)                     !New Part Number     
NewDescription              STRING(30)                     !New Description     
Notes                       STRING(255)                    !Notes               
                         END
                     END                       

RETSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(res),CREATE,BINDABLE,THREAD !Retail Stock        
Record_Number_Key        KEY(res:Record_Number),NOCASE,PRIMARY !By Record Number    
Part_Number_Key          KEY(res:Ref_Number,res:Part_Number),DUP,NOCASE !By Part Number      
Despatched_Key           KEY(res:Ref_Number,res:Despatched,res:Despatch_Note_Number,res:Part_Number),DUP,NOCASE !By Part Number      
Despatched_Only_Key      KEY(res:Ref_Number,res:Despatched),DUP,NOCASE !By Despatched       
Pending_Ref_Number_Key   KEY(res:Ref_Number,res:Pending_Ref_Number),DUP,NOCASE !                    
Order_Part_Key           KEY(res:Ref_Number,res:Order_Part_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(res:Ref_Number,res:Order_Number),DUP,NOCASE !                    
DespatchedKey            KEY(res:Despatched),DUP,NOCASE    !By Despatched       
DespatchedPartKey        KEY(res:Ref_Number,res:Despatched,res:Part_Number),DUP,NOCASE !By Part Number      
Amended_Receipt_Key      KEY(res:Amend_Site_Loc,res:Amended,res:Ref_Number),DUP,NOCASE !                    
Delimit_Stock_Key        KEY(res:Ref_Number,res:Part_Ref_Number),DUP,NOCASE !                    
PartRefNumberKey         KEY(res:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PartNumberKey            KEY(res:Part_Number),DUP,NOCASE   !By Part Number      
DespatchPartKey          KEY(res:Despatched,res:Part_Number),DUP,NOCASE !By Part Number      
ExchangeRefNumberKey     KEY(res:Ref_Number,res:ExchangeRefNumber),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               LONG                           !Record Number       
Ref_Number                  REAL                           !                    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
AccessoryCost               REAL                           !Accesory Cost       
Item_Cost                   REAL                           !                    
Quantity                    REAL                           !                    
Despatched                  STRING(20)                     !                    
Order_Number                REAL                           !                    
Date_Ordered                DATE                           !                    
Date_Received               DATE                           !                    
Despatch_Note_Number        REAL                           !                    
Despatch_Date               DATE                           !                    
Part_Ref_Number             REAL                           !                    
Pending_Ref_Number          REAL                           !                    
Order_Part_Number           REAL                           !                    
Purchase_Order_Number       STRING(30)                     !                    
Previous_Sale_Number        LONG                           !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
QuantityReceived            LONG                           !Quantity Received   
Received                    BYTE                           !Received            
Amended                     BYTE                           !                    
Amend_Reason                CSTRING(255)                   !                    
Amend_Site_Loc              STRING(30)                     !Site Location       
GRNNumber                   REAL                           !                    
DateReceived                DATE                           !Date Received       
ScannedQty                  LONG                           !Scanned Quantity    
ExchangeRefNumber           LONG                           !                    
                         END
                     END                       

RETPAY               FILE,DRIVER('Btrieve'),OEM,NAME('RETPAY.DAT'),PRE(rtp),CREATE,BINDABLE,THREAD !Retail Sales Payment
Record_Number_Key        KEY(rtp:Record_Number),NOCASE,PRIMARY !By Record Number    
Date_Key                 KEY(rtp:Ref_Number,rtp:Date,rtp:Payment_Type),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
Record_Number               LONG                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Payment_Type                STRING(30)                     !                    
Credit_Card_Number          STRING(20)                     !                    
Expiry_Date                 STRING(5)                      !                    
Issue_Number                STRING(5)                      !                    
Amount                      REAL                           !                    
User_Code                   STRING(3)                      !                    
Authorisation_Code          STRING(30)                     !                    
Card_Holder                 STRING(30)                     !                    
                         END
                     END                       

RETDESNO             FILE,DRIVER('Btrieve'),OEM,NAME('RETDESNO.DAT'),PRE(rdn),CREATE,BINDABLE,THREAD !Retail Despatch Number File
Despatch_Number_Key      KEY(rdn:Despatch_Number),NOCASE,PRIMARY !By Despatch Number  
Record                   RECORD,PRE()
Despatch_Number             LONG                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
Consignment_Number          STRING(30)                     !                    
Courier                     STRING(30)                     !                    
Sale_Number                 LONG                           !                    
                         END
                     END                       

ORDPARTS             FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(orp),CREATE,BINDABLE,THREAD !Orders - Parts Attached
Order_Number_Key         KEY(orp:Order_Number,orp:Part_Ref_Number),DUP,NOCASE !By Order Number     
record_number_key        KEY(orp:Record_Number),NOCASE,PRIMARY !                    
Ref_Number_Key           KEY(orp:Part_Ref_Number,orp:Part_Number,orp:Description),DUP,NOCASE !By Ref Number       
Part_Number_Key          KEY(orp:Order_Number,orp:Part_Number,orp:Description),DUP,NOCASE !By Part Number      
Description_Key          KEY(orp:Order_Number,orp:Description,orp:Part_Number),DUP,NOCASE !By Description      
Received_Part_Number_Key KEY(orp:All_Received,orp:Part_Number),DUP,NOCASE !By Part Number      
Account_Number_Key       KEY(orp:Account_Number,orp:Part_Type,orp:All_Received,orp:Part_Number),DUP,NOCASE !                    
Allocated_Key            KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Part_Number),DUP,NOCASE !                    
Allocated_Account_Key    KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Account_Number,orp:Part_Number),DUP,NOCASE !                    
Order_Type_Received      KEY(orp:Order_Number,orp:Part_Type,orp:All_Received,orp:Part_Number,orp:Description),DUP,NOCASE !By Part Number      
PartRecordNumberKey      KEY(orp:PartRecordNumber),DUP,NOCASE !By Record Number    
Record                   RECORD,PRE()
Order_Number                LONG                           !Order Number        
Record_Number               LONG                           !Record Number       
Part_Ref_Number             LONG                           !Part Ref Number     
Quantity                    LONG                           !Quantity            
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Job_Number                  LONG                           !Job Number          
Part_Type                   STRING(3)                      !                    
Number_Received             LONG                           !Number Received     
Date_Received               DATE                           !                    
All_Received                STRING(3)                      !                    
Allocated_To_Sale           STRING(3)                      !                    
Account_Number              STRING(15)                     !                    
DespatchNoteNumber          STRING(30)                     !Despatch Note Number
Reason                      STRING(255)                    !Reason              
PartRecordNumber            LONG                           !Part Record Number  
GRN_Number                  LONG                           !Goods Received Number
OrderedCurrency             STRING(30)                     !Currency On Ordering
OrderedDailyRate            REAL                           !Daily Rate On Ordering
OrderedDivideMultiply       STRING(1)                      !Ordered Divide Multiply
ReceivedCurrency            STRING(30)                     !Currency On Order Received
ReceivedDailyRate           REAL                           !Daily Rate On Order Received
ReceivedDivideMultiply      STRING(1)                      !Divide Multiply Order Received
FreeExchangeStock           BYTE                           !Free Exchange Stock 
TimeReceived                TIME                           !Time Received       
DatePriceCaptured           DATE                           !                    
TimePriceCaptured           TIME                           !                    
                         END
                     END                       

WAYBILLS             FILE,DRIVER('Btrieve'),OEM,NAME('WAYBILLS.DAT'),PRE(way),CREATE,BINDABLE,THREAD !Waybill Numbers     
RecordNumberKey          KEY(way:RecordNumber),NOCASE,PRIMARY !By Record Number    
WayBillNumberKey         KEY(way:WayBillNumber),NOCASE     !By WayBill Number   
RecWayBillNoKey          KEY(way:Received,way:WayBillNumber),DUP,NOCASE !By WayBill Number   
TypeNumberKey            KEY(way:WayBillType,way:WayBillNumber),DUP,NOCASE !By Waybill Number   
TypeRecNumberKey         KEY(way:WayBillType,way:Received,way:WayBillNumber),DUP,NOCASE !By Waybill Number   
TypeAccountRecNumberKey  KEY(way:WayBillType,way:AccountNumber,way:Received,way:WayBillNumber),DUP,NOCASE !                    
DateKey                  KEY(way:TheDate,way:TheTime),DUP,NOCASE !By The Date         
WaybillTypeDateKey       KEY(way:WayBillType,way:TheDate,way:TheTime),DUP,NOCASE !By Date             
AccountDateKey           KEY(way:AccountNumber,way:TheDate,way:TheTime),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccountNumber               STRING(30)                     !Account Number      
WayBillNumber               LONG                           !Waybill Number      
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
Received                    BYTE                           !Received            
WayBillType                 BYTE                           !WayBill Type        
WaybillID                   STRING(3)                      !Waybill Type        
FromAccount                 STRING(30)                     !Account Number      
ToAccount                   STRING(30)                     !Account Number      
SecurityPackNumber          STRING(30)                     !Security Pack Number
UserNotes                   STRING(255)                    !User Notes          
Courier                     STRING(30)                     !Courier             
OtherAccountNumber          STRING(30)                     !Account Number      
OtherCompanyName            STRING(30)                     !Company Name        
OtherAddress1               STRING(30)                     !Address 1           
OtherAddress2               STRING(30)                     !Address 2           
OtherAddress3               STRING(30)                     !Address 3           
OtherPostcode               STRING(30)                     !Postcode            
OtherTelephoneNo            STRING(30)                     !Telephone Number    
OtherContactName            STRING(60)                     !Contact Name        
OtherEmailAddress           STRING(255)                    !Email Address       
OtherHub                    STRING(30)                     !Hub                 
                         END
                     END                       

LOCATION             FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc),CREATE,BINDABLE,THREAD !Locations - Site    
RecordNumberKey          KEY(loc:RecordNumber),NOCASE,PRIMARY !By Record Number    
Location_Key             KEY(loc:Location),NOCASE          !By Location         
Main_Store_Key           KEY(loc:Main_Store,loc:Location),DUP,NOCASE !By Location         
ActiveLocationKey        KEY(loc:Active,loc:Location),DUP,NOCASE !By Location         
ActiveMainStoreKey       KEY(loc:Active,loc:Main_Store,loc:Location),DUP,NOCASE !By Location         
VirtualLocationKey       KEY(loc:VirtualSite,loc:Location),DUP,NOCASE !By Location         
VirtualMainStoreKey      KEY(loc:VirtualSite,loc:Main_Store,loc:Location),DUP,NOCASE !By Location         
FaultyLocationKey        KEY(loc:FaultyPartsLocation),DUP,NOCASE !By Location         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !                    
Main_Store                  STRING(3)                      !                    
Active                      BYTE                           !Active              
VirtualSite                 BYTE                           !Virtual Site        
Level1                      BYTE                           !Level 1             
Level2                      BYTE                           !Level 2             
Level3                      BYTE                           !Level 3             
InWarrantyMarkUp            REAL                           !In Warranty Markup  
OutWarrantyMarkUp           REAL                           !Out Warranty Markup 
UseRapidStock               BYTE                           !Use Rapid Stock Allocation
FaultyPartsLocation         BYTE                           !                    
                         END
                     END                       

STOMPFAU             FILE,DRIVER('Btrieve'),OEM,NAME('STOMPFAU.DAT'),PRE(stu),CREATE,BINDABLE,THREAD !Model Stock Part Fault Codes
RecordNumberKey          KEY(stu:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(stu:RefNumber,stu:FieldNumber,stu:Field),DUP,NOCASE !By Field            
DescriptionKey           KEY(stu:RefNumber,stu:FieldNumber,stu:Description),DUP,NOCASE !By Description      
ManufacturerFieldKey     KEY(stu:Manufacturer,stu:FieldNumber,stu:Field),DUP,NOCASE !By Field            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to STOMODEL RecordNumber
Manufacturer                STRING(30)                     !Manufacturer        
FieldNumber                 LONG                           !Field Number        
Field                       STRING(30)                     !Field               
Description                 STRING(60)                     !                    
                         END
                     END                       

STOMJFAU             FILE,DRIVER('Btrieve'),OEM,NAME('STOMJFAU.DAT'),PRE(stj),CREATE,BINDABLE,THREAD !Model Stock Job Fault Codes
RecordNumberKey          KEY(stj:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(stj:RefNumber),DUP,NOCASE     !By Field            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to STOMODEL RecordNumber
FaultCode1                  STRING(30)                     !Fault Code 1        
FaultCode2                  STRING(30)                     !Fault Code 2        
FaultCode3                  STRING(30)                     !Fault Code 3        
FaultCode4                  STRING(30)                     !Fault Code 4        
FaultCode5                  STRING(30)                     !Fault Code 5        
FaultCode6                  STRING(30)                     !Fault Code 6        
FaultCode7                  STRING(30)                     !Fault Code 7        
FaultCode8                  STRING(30)                     !Faul Code 8         
FaultCode9                  STRING(30)                     !FaultCode9          
FaultCode10                 STRING(255)                    !Fault Code 10       
FaultCode11                 STRING(255)                    !Fault Code 11       
FaultCode12                 STRING(255)                    !FaultCode12         
FaultCode13                 STRING(30)                     !FaultCode13         
FaultCode14                 STRING(30)                     !FaultCode14         
FaultCode15                 STRING(30)                     !FaultCode15         
FaultCode16                 STRING(30)                     !FaultCode16         
FaultCode17                 STRING(30)                     !FaultCode17         
FaultCode18                 STRING(30)                     !FaultCode18         
FaultCode19                 STRING(30)                     !FaultCode19         
FaultCode20                 STRING(30)                     !FaultCode20         
                         END
                     END                       

EXCHANGE             FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch),CREATE,BINDABLE,THREAD !Exchange units      
Ref_Number_Key           KEY(xch:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
AvailLocIMEI             KEY(xch:Available,xch:Location,xch:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(xch:Available,xch:Location,xch:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(xch:Available,xch:Location,xch:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(xch:Available,xch:Location,xch:Model_Number),DUP,NOCASE !                    
ESN_Only_Key             KEY(xch:ESN),DUP,NOCASE           !                    
MSN_Only_Key             KEY(xch:MSN),DUP,NOCASE           !                    
Ref_Number_Stock_Key     KEY(xch:Stock_Type,xch:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(xch:Stock_Type,xch:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch:Stock_Type,xch:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
Model_Available_Key      KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(xch:Stock_Type),DUP,NOCASE    !By Stock Type       
ModelRefNoKey            KEY(xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(xch:Available,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(xch:Available,xch:MSN),DUP,NOCASE !By M.S.N.           
AvailRefOnlyKey          KEY(xch:Available,xch:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Unit Number      
StockBookedKey           KEY(xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE !By Unit Number      
AvailBookedKey           KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(xch:Date_Booked),DUP,NOCASE   !By Date Booked      
LocStockAvailIMEIKey     KEY(xch:Location,xch:Stock_Type,xch:Available,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(xch:Location,xch:Stock_Type,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(xch:Location,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(xch:Location,xch:Stock_Type,xch:Available,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockRefKey           KEY(xch:Location,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocRefKey                KEY(xch:Location,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailModelKey    KEY(xch:Location,xch:Stock_Type,xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockModelKey         KEY(xch:Location,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocModelKey              KEY(xch:Location,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailMSNKey      KEY(xch:Location,xch:Stock_Type,xch:Available,xch:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(xch:Location,xch:Stock_Type,xch:MSN),DUP,NOCASE !By Exchange Unit Number
LocMSNKey                KEY(xch:Location,xch:MSN),DUP,NOCASE !By M.S.N.           
InTransitLocationKey     KEY(xch:InTransit,xch:Location,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
InTransitKey             KEY(xch:InTransit,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
StatusChangeDateKey      KEY(xch:StatusChangeDate),DUP,NOCASE !                    
LocStatusChangeDateKey   KEY(xch:Location,xch:StatusChangeDate),DUP,NOCASE !                    
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Unit Number         
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !Job Number          
Stock_Type                  STRING(30)                     !                    
Audit_Number                REAL                           !                    
InTransit                   BYTE                           !In Transit Flag     
FreeStockPurchased          STRING(1)                      !                    
StatusChangeDate            DATE                           !                    
                         END
                     END                       

STOESN               FILE,DRIVER('Btrieve'),OEM,NAME('STOESN.DAT'),PRE(ste),CREATE,BINDABLE,THREAD !Stock Serial Number File
Record_Number_Key        KEY(ste:Record_Number),NOCASE,PRIMARY !By Record Number    
Sold_Key                 KEY(ste:Ref_Number,ste:Sold,ste:Serial_Number),DUP,NOCASE !By Serial Number    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Ref_Number                  REAL                           !                    
Serial_Number               STRING(16)                     !                    
Sold                        STRING(3)                      !                    
                         END
                     END                       

LOANACC              FILE,DRIVER('Btrieve'),NAME('LOANACC.DAT'),PRE(lac),CREATE,BINDABLE,THREAD !Loan Accesories     
Ref_Number_Key           KEY(lac:Ref_Number,lac:Accessory),NOCASE,PRIMARY !By Accessory        
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Accessory                   STRING(30)                     !                    
Accessory_Status            STRING(6)                      !                    
                         END
                     END                       

SUPPLIER             FILE,DRIVER('Btrieve'),NAME('SUPPLIER.DAT'),PRE(sup),CREATE,BINDABLE,THREAD !Suppliers           
RecordNumberKey          KEY(sup:RecordNumber),NOCASE,PRIMARY !                    
AccountNumberKey         KEY(sup:Account_Number),DUP,NOCASE !By Account Number   
Company_Name_Key         KEY(sup:Company_Name),NOCASE      !By Company Name     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Company_Name                STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Contact_Name                STRING(60)                     !                    
Account_Number              STRING(15)                     !                    
Order_Period                REAL                           !                    
Normal_Supply_Period        REAL                           !                    
Minimum_Order_Value         REAL                           !                    
HistoryUsage                LONG                           !History Usage       
Factor                      REAL                           !Multiply By Factor Of
Notes                       STRING(255)                    !                    
UseForeignCurrency          BYTE                           !                    
CurrencyCode                STRING(30)                     !                    
MOPSupplierNumber           STRING(30)                     !MOP Supplier Number 
MOPItemID                   STRING(30)                     !MOP Item ID         
UseFreeStock                BYTE                           !                    
                         END
                     END                       

SUBURB               FILE,DRIVER('Btrieve'),OEM,NAME('SUBURB.DAT'),PRE(sur),CREATE,BINDABLE,THREAD !List Of Suburbs     
RecordNumberKey          KEY(sur:RecordNumber),NOCASE,PRIMARY !By Record Number    
SuburbKey                KEY(sur:Suburb),DUP,NOCASE        !By Suburb           
PostcodeKey              KEY(sur:Postcode),DUP,NOCASE      !By Postcode         
HubKey                   KEY(sur:Hub),DUP,NOCASE           !By Hub              
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Suburb                      STRING(30)                     !Suburb              
Postcode                    STRING(30)                     !Postcode            
Hub                         STRING(30)                     !Hub                 
                         END
                     END                       

EXCHACC              FILE,DRIVER('Btrieve'),NAME('EXCHACC.DAT'),PRE(xca),CREATE,BINDABLE,THREAD !Exchange Accesories 
Ref_Number_Key           KEY(xca:Ref_Number,xca:Accessory),NOCASE,PRIMARY !By Accessory        
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Accessory                   STRING(30)                     !                    
                         END
                     END                       

COUBUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('COUBUSHR.DAT'),PRE(cbh),CREATE,BINDABLE,THREAD !Courier Hours Of Business Exceptions
RecordNumberKey          KEY(cbh:RecordNumber),NOCASE,PRIMARY !By Record Number    
TypeDateKey              KEY(cbh:Courier,cbh:TheDate),NOCASE !By Date             
EndTimeKey               KEY(cbh:Courier,cbh:TheDate,cbh:StartTime,cbh:EndTime),DUP,NOCASE !By End Time         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Courier                     STRING(30)                     !Courier             
TheDate                     DATE                           !Date                
ExceptionType               BYTE                           !Exception Type      
StartTime                   TIME                           !Start Time          
EndTime                     TIME                           !End Time            
                         END
                     END                       

SUPVALA              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALA.DAT'),PRE(suva),CREATE,BINDABLE,THREAD !Suppliers End Of Day Calcs A
RecordNumberKey          KEY(suva:RecordNumber),NOCASE,PRIMARY !                    
RunDateKey               KEY(suva:Supplier,suva:RunDate),DUP,NOCASE !By Run Date         
DateOnly                 KEY(suva:RunDate),DUP,NOCASE,OPT  !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Supplier                    STRING(30)                     !Supplier            
RunDate                     DATE                           !Date                
OldBackOrder                DATE                           !Date Of Oldest Back Order
OldOutOrder                 DATE                           !Date Of Oldest Outstanding Order
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),NAME('ESTPARTS.DAT'),PRE(epr),CREATE,BINDABLE,THREAD !Estimated Parts On A Job
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE !By Part Number      
record_number_key        KEY(epr:Record_Number),NOCASE,PRIMARY !                    
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE  !By Date Ordered     
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE      !                    
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE !By Order Part Number
PartAllocatedKey         KEY(epr:PartAllocated,epr:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(epr:Status,epr:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(epr:PartAllocated,epr:Status,epr:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Ref Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Part Ref Number     
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Quantity                    LONG                           !Quantity            
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Ref Number  
Order_Number                LONG                           !Order Number        
Date_Received               DATE                           !                    
Order_Part_Number           LONG                           !Order Part Number   
Status_Date                 DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Parts Requested For Order
UsedOnRepair                BYTE                           !Used On Repair      
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
                         END
                     END                       

SBO_OutFaultParts    FILE,DRIVER('TOPSPEED'),RECLAIM,OEM,NAME(glo:sbo_outfaultparts),PRE(sofp),CREATE,BINDABLE,THREAD !SBOnline - Out Fault Parts List
FaultKey                 KEY(sofp:sessionID,sofp:partType,sofp:fault),DUP,NOCASE,OPT !By SessionID        
Record                   RECORD,PRE()
sessionID                   LONG                           !                    
partType                    STRING(1)                      !                    
fault                       STRING(30)                     !                    
description                 STRING(60)                     !                    
level                       LONG                           !Repair Index        
                         END
                     END                       

TRAHUBS              FILE,DRIVER('Btrieve'),OEM,NAME('TRAHUBS.DAT'),PRE(trh),CREATE,BINDABLE,THREAD !Trade Account Hubs  
RecordNumberKey          KEY(trh:RecordNumber),NOCASE,PRIMARY !By Record Number    
HubKey                   KEY(trh:TRADEACCAccountNumber,trh:Hub),NOCASE !By Hub              
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
TRADEACCAccountNumber       STRING(30)                     !Account Number      
Hub                         STRING(30)                     !Hub                 
                         END
                     END                       

TRAEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('TRAEMAIL.DAT'),PRE(tre),CREATE,BINDABLE,THREAD !Trade Account Email Recipients
RecordNumberKey          KEY(tre:RecordNumber),NOCASE,PRIMARY !By Record Number    
RecipientKey             KEY(tre:RefNumber,tre:RecipientType),DUP,NOCASE !By Recipient Type   
ContactNameKey           KEY(tre:RefNumber,tre:ContactName),DUP,NOCASE !By Contact Name     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
RecipientType               STRING(30)                     !Recipient Type      
ContactName                 STRING(60)                     !Contact Name        
EmailAddress                STRING(255)                    !Email Address       
SendStatusEmails            BYTE                           !Send Status Emails  
SendReportEmails            BYTE                           !Send Report Emails  
                         END
                     END                       

SUPVALB              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALB.DAT'),PRE(suvb),CREATE,BINDABLE,THREAD !Suppliers End Of Date Calcs B
RecordNumberKey          KEY(suvb:RecordNumber),NOCASE,PRIMARY !By Record Number    
RunDateKey               KEY(suvb:Supplier,suvb:RunDate),DUP,NOCASE !By Run Date         
LocationKey              KEY(suvb:Supplier,suvb:RunDate,suvb:Location),DUP,NOCASE !By Location         
DateOnly                 KEY(suvb:RunDate),DUP,NOCASE,OPT  !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Supplier                    STRING(30)                     !Supplier            
RunDate                     DATE                           !Date                
Location                    STRING(30)                     !Location            
BackOrderValue              REAL                           !Total Cost Of All Oustanding Orders
                         END
                     END                       

SUBBUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('SUBBUSHR.DAT'),PRE(sbh),CREATE,BINDABLE,THREAD !Sub Account Hours Of Business Exceptions
RecordNumberKey          KEY(sbh:RecordNumber),NOCASE,PRIMARY !By Record Number    
TypeDateKey              KEY(sbh:RefNumber,sbh:TheDate),NOCASE !By Date             
EndTimeKey               KEY(sbh:RefNumber,sbh:TheDate,sbh:StartTime,sbh:EndTime),DUP,NOCASE !By End Time         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To Parent      
TheDate                     DATE                           !Date                
ExceptionType               BYTE                           !Exception Type      
StartTime                   TIME                           !Start Time          
EndTime                     TIME                           !End Time            
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job),BINDABLE,CREATE,THREAD !Jobs                
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY !By Job Number       
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE !By Job Number       
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE !By Job Number       
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE !By Job Number       
Surname_Key              KEY(job:Surname),DUP,NOCASE       !By Surname          
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE !By Mobile Number    
ESN_Key                  KEY(job:ESN),DUP,NOCASE           !By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job:MSN),DUP,NOCASE           !By M.S.N.           
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE !By Account Number   
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE !By Order Number     
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE  !By Model Number     
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE !By Engineer         
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE   !By Date Booked      
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE !By Date Completed   
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE !By Completed Date   
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE !By Job Number       
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE !By Job Number       
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE !By Location         
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE !By Third Party      
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE !By ESN              
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE !By MSN              
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE !By Priority         
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE     !By Unit Type        
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE !By Invoice_Number   
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE !By Invoice Number   
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE !By Ref Number       
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE !By Ref Number       
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE !By Job Number       
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE !                    
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE !By Job Number       
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE !By Cosignment Number
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE !By Consignment Number
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE !By Job Number       
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE   !By Chargeable Repair Type
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE !By Warranty Repair Type
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE   !                    
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE !                    
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE !By Job Number       
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE !By Date Completed   
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE !By Job Number       
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE !By Job Number       
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE !By Job Number       
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE !By Job Number       
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE !By Job Number       
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Batch_Number                LONG                           !                    
Internal_Status             STRING(10)                     !                    
Auto_Search                 STRING(30)                     !                    
who_booked                  STRING(3)                      !                    
date_booked                 DATE                           !                    
time_booked                 TIME                           !                    
Cancelled                   STRING(3)                      !                    
Bouncer                     STRING(8)                      !                    
Bouncer_Type                STRING(3)                      !Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                      !Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                      !                    
Chargeable_Job              STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(20)                     !                    
MSN                         STRING(20)                     !                    
ProductCode                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location_Type               STRING(10)                     !                    
Phone_Lock                  STRING(30)                     !                    
Workshop                    STRING(3)                      !                    
Location                    STRING(30)                     !                    
Authority_Number            STRING(30)                     !                    
Insurance_Reference_Number  STRING(30)                     !                    
DOP                         DATE                           !                    
Insurance                   STRING(3)                      !                    
Insurance_Type              STRING(30)                     !                    
Transit_Type                STRING(30)                     !                    
Physical_Damage             STRING(3)                      !                    
Intermittent_Fault          STRING(3)                      !                    
Loan_Status                 STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Job_Priority                STRING(30)                     !                    
Charge_Type                 STRING(30)                     !                    
Warranty_Charge_Type        STRING(30)                     !                    
Current_Status              STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Trade_Account_Name          STRING(30)                     !                    
Department_Name             STRING(30)                     !                    
Order_Number                STRING(30)                     !                    
POP                         STRING(3)                      !                    
In_Repair                   STRING(3)                      !                    
Date_In_Repair              DATE                           !                    
Time_In_Repair              TIME                           !                    
On_Test                     STRING(3)                      !                    
Date_On_Test                DATE                           !                    
Time_On_Test                TIME                           !                    
Estimate_Ready              STRING(3)                      !                    
QA_Passed                   STRING(3)                      !                    
Date_QA_Passed              DATE                           !                    
Time_QA_Passed              TIME                           !                    
QA_Rejected                 STRING(3)                      !                    
Date_QA_Rejected            DATE                           !                    
Time_QA_Rejected            TIME                           !                    
QA_Second_Passed            STRING(3)                      !                    
Date_QA_Second_Passed       DATE                           !                    
Time_QA_Second_Passed       TIME                           !                    
Title                       STRING(4)                      !                    
Initial                     STRING(1)                      !                    
Surname                     STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Mobile_Number               STRING(15)                     !                    
Postcode_Collection         STRING(10)                     !                    
Company_Name_Collection     STRING(30)                     !                    
Address_Line1_Collection    STRING(30)                     !                    
Address_Line2_Collection    STRING(30)                     !                    
Address_Line3_Collection    STRING(30)                     !                    
Telephone_Collection        STRING(15)                     !                    
Postcode_Delivery           STRING(10)                     !                    
Address_Line1_Delivery      STRING(30)                     !                    
Company_Name_Delivery       STRING(30)                     !                    
Address_Line2_Delivery      STRING(30)                     !                    
Address_Line3_Delivery      STRING(30)                     !                    
Telephone_Delivery          STRING(15)                     !                    
Date_Completed              DATE                           !                    
Time_Completed              TIME                           !                    
Completed                   STRING(3)                      !                    
Paid                        STRING(3)                      !                    
Paid_Warranty               STRING(3)                      !                    
Date_Paid                   DATE                           !                    
Repair_Type                 STRING(30)                     !                    
Repair_Type_Warranty        STRING(30)                     !                    
Engineer                    STRING(3)                      !                    
Ignore_Chargeable_Charges   STRING(3)                      !                    
Ignore_Warranty_Charges     STRING(3)                      !                    
Ignore_Estimate_Charges     STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Advance_Payment             REAL                           !                    
Labour_Cost                 REAL                           !                    
Parts_Cost                  REAL                           !                    
Sub_Total                   REAL                           !                    
Courier_Cost_Estimate       REAL                           !                    
Labour_Cost_Estimate        REAL                           !                    
Parts_Cost_Estimate         REAL                           !                    
Sub_Total_Estimate          REAL                           !                    
Courier_Cost_Warranty       REAL                           !                    
Labour_Cost_Warranty        REAL                           !                    
Parts_Cost_Warranty         REAL                           !                    
Sub_Total_Warranty          REAL                           !                    
Loan_Issued_Date            DATE                           !                    
Loan_Unit_Number            LONG                           !                    
Loan_accessory              STRING(3)                      !                    
Loan_User                   STRING(3)                      !                    
Loan_Courier                STRING(30)                     !                    
Loan_Consignment_Number     STRING(30)                     !                    
Loan_Despatched             DATE                           !                    
Loan_Despatched_User        STRING(3)                      !                    
Loan_Despatch_Number        LONG                           !                    
LoaService                  STRING(1)                      !Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                           !                    
Exchange_Authorised         STRING(3)                      !                    
Loan_Authorised             STRING(3)                      !                    
Exchange_Accessory          STRING(3)                      !                    
Exchange_Issued_Date        DATE                           !                    
Exchange_User               STRING(3)                      !                    
Exchange_Courier            STRING(30)                     !                    
Exchange_Consignment_Number STRING(30)                     !                    
Exchange_Despatched         DATE                           !                    
Exchange_Despatched_User    STRING(3)                      !                    
Exchange_Despatch_Number    LONG                           !                    
ExcService                  STRING(1)                      !Exchange Service For City Link / Label G
Date_Despatched             DATE                           !                    
Despatch_Number             LONG                           !                    
Despatch_User               STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Consignment_Number          STRING(30)                     !                    
Incoming_Courier            STRING(30)                     !                    
Incoming_Consignment_Number STRING(30)                     !                    
Incoming_Date               DATE                           !                    
Despatched                  STRING(3)                      !                    
Despatch_Type               STRING(3)                      !                    
Current_Courier             STRING(30)                     !                    
JobService                  STRING(1)                      !Job Service For City Link / Label G
Last_Repair_Days            REAL                           !                    
Third_Party_Site            STRING(30)                     !                    
Estimate                    STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Estimate_Accepted           STRING(3)                      !                    
Estimate_Rejected           STRING(3)                      !                    
Third_Party_Printed         STRING(3)                      !                    
ThirdPartyDateDesp          DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(255)                    !                    
Fault_Code11                STRING(255)                    !                    
Fault_Code12                STRING(255)                    !                    
PreviousStatus              STRING(30)                     !                    
StatusUser                  STRING(3)                      !User who last changed the status
Status_End_Date             DATE                           !                    
Status_End_Time             TIME                           !                    
Turnaround_End_Date         DATE                           !                    
Turnaround_End_Time         TIME                           !                    
Turnaround_Time             STRING(30)                     !                    
Special_Instructions        STRING(30)                     !                    
InvoiceAccount              STRING(15)                     !Invoice Batch Account Number
InvoiceStatus               STRING(3)                      !Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                           !Proforma Batch Number
InvoiceQuery                STRING(255)                    !Query Reason        
EDI                         STRING(3)                      !                    
EDI_Batch_Number            REAL                           !                    
Invoice_Exception           STRING(3)                      !                    
Invoice_Failure_Reason      STRING(80)                     !                    
Invoice_Number              LONG                           !                    
Invoice_Date                DATE                           !                    
Invoice_Date_Warranty       DATE                           !                    
Invoice_Courier_Cost        REAL                           !                    
Invoice_Labour_Cost         REAL                           !                    
Invoice_Parts_Cost          REAL                           !                    
Invoice_Sub_Total           REAL                           !                    
Invoice_Number_Warranty     LONG                           !                    
WInvoice_Courier_Cost       REAL                           !                    
WInvoice_Labour_Cost        REAL                           !                    
WInvoice_Parts_Cost         REAL                           !                    
WInvoice_Sub_Total          REAL                           !                    
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(mod),CREATE,BINDABLE,THREAD !Model Numbers       
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY !By Model Number     
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE !By Manufacturer     
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE !By Unit Type        
SalesModelKey            KEY(mod:Manufacturer,mod:SalesModel),DUP,NOCASE !By Sales Model      
Record                   RECORD,PRE()
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
Specify_Unit_Type           STRING(3)                      !                    
Product_Type                STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Nokia_Unit_Type             STRING(30)                     !                    
ESN_Length_From             REAL                           !                    
ESN_Length_To               REAL                           !                    
MSN_Length_From             REAL                           !                    
MSN_Length_To               REAL                           !                    
ExchangeUnitMinLevel        LONG                           !Exchange Unit Min Level
ReplacementValue            REAL                           !Replacement Value   
WarrantyRepairLimit         REAL                           !Warranty Repair Limit
LoanReplacementValue        REAL                           !Loan Replacement Value
ExcludedRRCRepair           BYTE                           !Excluded From RRC Repair
AllowIMEICharacters         BYTE                           !Allow IMEI Characters
UseReplenishmentProcess     BYTE                           !Use Replenishment Process
SalesModel                  STRING(30)                     !Sales Model         
ExcludeAutomaticRebookingProcess BYTE                      !Exclude Automatic Rebooking Process
OneYearWarrOnly             STRING(1)                      !                    
Active                      STRING(1)                      !                    
ExchReplaceValue            REAL                           !Exchange Replacement Value
                         END
                     END                       

TRABUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('TRABUSHR.DAT'),PRE(tbh),CREATE,BINDABLE,THREAD !Main Account Hours Of Business Exceptions
RecordNumberKey          KEY(tbh:RecordNumber),NOCASE,PRIMARY !By Record Number    
TypeDateKey              KEY(tbh:RefNumber,tbh:TheDate),NOCASE !By Date             
EndTimeKey               KEY(tbh:RefNumber,tbh:TheDate,tbh:StartTime,tbh:EndTime),DUP,NOCASE !By End Time         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To Parent      
TheDate                     DATE                           !Date                
ExceptionType               BYTE                           !Exception Type      
StartTime                   TIME                           !Start Time          
EndTime                     TIME                           !End Time            
                         END
                     END                       

SUBEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('SUBEMAIL.DAT'),PRE(sue),CREATE,BINDABLE,THREAD !Sub Account Email Recipients
RecordNumberKey          KEY(sue:RecordNumber),NOCASE,PRIMARY !By Record Number    
RecipientTypeKey         KEY(sue:RefNumber,sue:RecipientType),DUP,NOCASE !By Recipient Type   
ContactNameKey           KEY(sue:RefNumber,sue:ContactName),DUP,NOCASE !By Contact Name     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
RecipientType               STRING(30)                     !Recipient Type      
ContactName                 STRING(60)                     !Contact Name        
EmailAddress                STRING(255)                    !Email Address       
SendStatusEmails            BYTE                           !Send Status Emails  
SendReportEmails            BYTE                           !Send Report Emails  
                         END
                     END                       

ORDERS               FILE,DRIVER('Btrieve'),NAME('ORDERS.DAT'),PRE(ord),CREATE,BINDABLE,THREAD !Orders              
Order_Number_Key         KEY(ord:Order_Number),NOCASE,PRIMARY !By Order_Number     
Printed_Key              KEY(ord:Printed,ord:Order_Number),DUP,NOCASE !By Order Number     
Supplier_Printed_Key     KEY(ord:Printed,ord:Supplier,ord:Order_Number),DUP,NOCASE !By Order Number     
Received_Key             KEY(ord:All_Received,ord:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(ord:Supplier,ord:Order_Number),DUP,NOCASE !By Order Number     
Supplier_Received_Key    KEY(ord:Supplier,ord:All_Received,ord:Order_Number),DUP,NOCASE !By Order Number     
DateKey                  KEY(ord:Date),DUP,NOCASE          !By Date             
NotPrintedOrderKey       KEY(ord:BatchRunNotPrinted,ord:Order_Number),DUP,NOCASE !By Order Number     
Record                   RECORD,PRE()
Order_Number                LONG                           !Order Number        
Supplier                    STRING(30)                     !Supplier Name       
Date                        DATE                           !Date Ordered        
Printed                     STRING(3)                      !Order Printed       
All_Received                STRING(3)                      !All Parts On Order Received
User                        STRING(3)                      !Order Created By    
OrderedCurrency             STRING(30)                     !Currency On Ordering
OrderedDailyRate            REAL                           !Daily Rate On Ordering
OrderedDivideMultiply       STRING(1)                      !Ordered Divide Multiply
BatchRunNotPrinted          BYTE                           !Order Not Printed As Part Of Batch Run
                         END
                     END                       

UNITTYPE             FILE,DRIVER('Btrieve'),NAME('UNITTYPE.DAT'),PRE(uni),CREATE,BINDABLE,THREAD !Unit Types          
Unit_Type_Key            KEY(uni:Unit_Type),NOCASE,PRIMARY !By Unit Type        
ActiveKey                KEY(uni:Active,uni:Unit_Type),DUP,NOCASE !By Unit Type        
Record                   RECORD,PRE()
Unit_Type                   STRING(30)                     !                    
Active                      BYTE                           !Active              
                         END
                     END                       

STATUS               FILE,DRIVER('Btrieve'),NAME('STATUS.DAT'),PRE(sts),CREATE,BINDABLE,THREAD !Status Types        
Ref_Number_Only_Key      KEY(sts:Ref_Number),NOCASE,PRIMARY !                    
Status_Key               KEY(sts:Status),DUP,NOCASE        !By Status           
Heading_Key              KEY(sts:Heading_Ref_Number,sts:Status),NOCASE !By Status Type      
Ref_Number_Key           KEY(sts:Heading_Ref_Number,sts:Ref_Number),NOCASE !By Ref Number       
LoanKey                  KEY(sts:Loan,sts:Status),DUP,NOCASE !By Status           
ExchangeKey              KEY(sts:Exchange,sts:Status),DUP,NOCASE !By Status           
JobKey                   KEY(sts:Job,sts:Status),DUP,NOCASE !By Status           
TurnJobKey               KEY(sts:Use_Turnaround_Time,sts:Job,sts:Status),DUP,NOCASE !By Status           
Record                   RECORD,PRE()
Heading_Ref_Number          REAL                           !                    
Status                      STRING(30)                     !                    
Ref_Number                  REAL                           !                    
Use_Turnaround_Time         STRING(3)                      !                    
Use_Turnaround_Days         STRING(3)                      !                    
Turnaround_Days             LONG                           !                    
Use_Turnaround_Hours        STRING(3)                      !                    
Turnaround_Hours            LONG                           !                    
Notes                       STRING(1000)                   !                    
Loan                        STRING(3)                      !                    
Exchange                    STRING(3)                      !                    
Job                         STRING(3)                      !                    
SystemStatus                STRING(3)                      !This Status Cannot Be Changed Or Deleted
EngineerStatus              BYTE                           !Display In Engineer's Browse
EnableEmail                 BYTE                           !Enable Email        
SenderEmailAddress          STRING(255)                    !Sender Email Address
EmailSubject                STRING(255)                    !Email Subject       
EmailBody                   STRING(500)                    !Email Body          
EmailFooter                 STRING(255)                    !Email Footer        
RefManufacturer             BYTE                           !Manufacturer        
RefModelNumber              BYTE                           !Model Number        
RefLabourCost               BYTE                           !Labour Cost         
RefPartsCost                BYTE                           !Parts Cost          
RefTotalCost                BYTE                           !Total Cost          
RefEstCost                  BYTE                           !Estimated Cost      
TurnaroundTimeReport        BYTE                           !Include In Turnaround Time Report
                         END
                     END                       

REPAIRTY             FILE,DRIVER('Btrieve'),NAME('REPAIRTY.DAT'),PRE(rep),CREATE,BINDABLE,THREAD !NOT USED            
Repair_Type_Key          KEY(rep:Manufacturer,rep:Model_Number,rep:Repair_Type),NOCASE,PRIMARY !By Repair Type      
Manufacturer_Key         KEY(rep:Manufacturer,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Model_Number_Key         KEY(rep:Model_Number,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Repair_Type_Only_Key     KEY(rep:Repair_Type),DUP,NOCASE   !By Repair Type      
Model_Chargeable_Key     KEY(rep:Model_Number,rep:Chargeable,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Model_Warranty_Key       KEY(rep:Model_Number,rep:Warranty,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Chargeable                  STRING(3)                      !                    
Warranty                    STRING(3)                      !                    
CompFaultCoding             BYTE                           !Compulsory Fault Coding
ExcludeFromEDI              BYTE                           !Exclude From EDI    
ExcludeFromInvoicing        BYTE                           !Exclude From Invoicing
                         END
                     END                       

TRDACC               FILE,DRIVER('Btrieve'),OEM,NAME('TRDACC.DAT'),PRE(trr),CREATE,BINDABLE,THREAD !Third Party Returned Accessories
RecordNumberKey          KEY(trr:RecordNumber),NOCASE,PRIMARY !By Record Number    
AccessoryKey             KEY(trr:RefNumber,trr:Accessory),DUP,NOCASE !By Accessory        
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !TRDBATCH Record Number
EntryDate                   DATE                           !Entry Date          
EntryTime                   TIME                           !Entry Time          
Accessory                   STRING(30)                     !Accessory           
                         END
                     END                       

INVOICE              FILE,DRIVER('Btrieve'),NAME('INVOICE.DAT'),PRE(inv),CREATE,BINDABLE,THREAD !Invoices            
Invoice_Number_Key       KEY(inv:Invoice_Number),NOCASE,PRIMARY !By Invoice Number   
Invoice_Type_Key         KEY(inv:Invoice_Type,inv:Invoice_Number),DUP,NOCASE !By Invoice_Number   
Account_Number_Type_Key  KEY(inv:Invoice_Type,inv:Account_Number,inv:Invoice_Number),DUP,NOCASE !By Invoice Number   
Account_Number_Key       KEY(inv:Account_Number,inv:Invoice_Number),DUP,NOCASE !By Invoice Number   
Date_Created_Key         KEY(inv:Date_Created),DUP,NOCASE  !By Invoice Date     
Batch_Number_Key         KEY(inv:Manufacturer,inv:Batch_Number),DUP,NOCASE !By Batch Number     
OracleDateKey            KEY(inv:ExportedOracleDate,inv:Invoice_Number),DUP,NOCASE !By Invoice Number   
OracleNumberKey          KEY(inv:OracleNumber,inv:Invoice_Number),DUP,NOCASE !By Oracle Number    
Account_Date_Key         KEY(inv:Account_Number,inv:Date_Created),DUP,NOCASE !By Date Created     
RRCInvoiceDateKey        KEY(inv:RRCInvoiceDate),DUP,NOCASE !By RRC Invoice Date Key
ARCInvoiceDateKey        KEY(inv:ARCInvoiceDate),DUP,NOCASE !By ARC Invoice Date 
ReconciledDateKey        KEY(inv:Reconciled_Date),DUP,NOCASE !By Reconciled Date  
AccountReconciledKey     KEY(inv:Account_Number,inv:Reconciled_Date),DUP,NOCASE !By Reconciled Date  
Record                   RECORD,PRE()
Invoice_Number              REAL                           !                    
Invoice_Type                STRING(3)                      !                    
Job_Number                  REAL                           !                    
Date_Created                DATE                           !                    
Account_Number              STRING(15)                     !                    
AccountType                 STRING(3)                      !Main Or Sub (MAI/SUB)
Total                       REAL                           !                    
Vat_Rate_Labour             REAL                           !                    
Vat_Rate_Parts              REAL                           !                    
Vat_Rate_Retail             REAL                           !                    
VAT_Number                  STRING(30)                     !                    
Invoice_VAT_Number          STRING(30)                     !                    
Currency                    STRING(30)                     !                    
Batch_Number                REAL                           !                    
Manufacturer                STRING(30)                     !                    
Claim_Reference             STRING(30)                     !                    
Total_Claimed               REAL                           !                    
Courier_Paid                REAL                           !                    
Labour_Paid                 REAL                           !                    
Parts_Paid                  REAL                           !                    
Reconciled_Date             DATE                           !                    
jobs_count                  REAL                           !                    
PrevInvoiceNo               LONG                           !Link To Previous Invoice Number for Credit Notes
InvoiceCredit               STRING(3)                      !Invoice Type: Invoice or Credit
UseAlternativeAddress       BYTE                           !Use Alternative Address
EuroExhangeRate             REAL                           !Euro Exchange Rate  
RRCVatRateLabour            REAL                           !RRC Vat Rate Labour 
RRCVatRateParts             REAL                           !RRC Vat Rate Parts  
RRCVatRateRetail            REAL                           !RRC Vat Rate Retail 
ExportedRRCOracle           BYTE                           !Exported RRC To Oracle
ExportedARCOracle           BYTE                           !Exported ARC To Oracle
ExportedOracleDate          DATE                           !Exported Date       
OracleNumber                LONG                           !Oracle Number       
RRCInvoiceDate              DATE                           !RRC Invoice Date    
ARCInvoiceDate              DATE                           !ARC Invoice Date    
                         END
                     END                       

TRDBATCH             FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(trb),CREATE,BINDABLE,THREAD !Third Party Batch Number
RecordNumberKey          KEY(trb:RecordNumber),NOCASE,PRIMARY !By Record Number    
Batch_Number_Key         KEY(trb:Company_Name,trb:Batch_Number),DUP,NOCASE !By Job Number       
Batch_Number_Status_Key  KEY(trb:Status,trb:Batch_Number),DUP,NOCASE !By Batch Number     
BatchOnlyKey             KEY(trb:Batch_Number),DUP,NOCASE  !By Batch Number     
Batch_Company_Status_Key KEY(trb:Status,trb:Company_Name,trb:Batch_Number),DUP,NOCASE !By Batch Number     
ESN_Only_Key             KEY(trb:ESN),DUP,NOCASE           !By ESN              
ESN_Status_Key           KEY(trb:Status,trb:ESN),DUP,NOCASE !By ESN              
Company_Batch_ESN_Key    KEY(trb:Company_Name,trb:Batch_Number,trb:ESN),DUP,NOCASE !By ESN              
AuthorisationKey         KEY(trb:AuthorisationNo),DUP,NOCASE !By Authorisation Number
StatusAuthKey            KEY(trb:Status,trb:AuthorisationNo),DUP,NOCASE !By Status           
CompanyDateKey           KEY(trb:Company_Name,trb:Status,trb:DateReturn),DUP,NOCASE !By Date             
JobStatusKey             KEY(trb:Status,trb:Ref_Number),DUP,NOCASE !By Job Number       
JobNumberKey             KEY(trb:Ref_Number),DUP,NOCASE    !By Job Number       
CompanyDespatchedKey     KEY(trb:Company_Name,trb:DateDespatched),DUP,NOCASE !By Despatch Date    
ReturnDateKey            KEY(trb:DateReturn),DUP,NOCASE    !By Return Date      
ReturnCompanyKey         KEY(trb:DateReturn,trb:Company_Name),DUP,NOCASE !By Company Name     
NotPrintedManJobKey      KEY(trb:BatchRunNotPrinted,trb:Status,trb:Company_Name,trb:Ref_Number),DUP,NOCASE !By Job Number       
PurchaseOrderKey         KEY(trb:PurchaseOrderNumber,trb:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
Batch_Number                LONG                           !                    
Company_Name                STRING(30)                     !                    
Ref_Number                  LONG                           !Job Number          
ESN                         STRING(20)                     !                    
Status                      STRING(3)                      !Out or In           
Date                        DATE                           !                    
Time                        TIME                           !                    
AuthorisationNo             STRING(30)                     !Authorisation Number for the Despatched Batch
Exchanged                   STRING(3)                      !Has the Job got an Exchange Unit attached
DateReturn                  DATE                           !Date The Unit Was Returned
TimeReturn                  TIME                           !Time Unit Returned  
TurnTime                    LONG                           !Turnaround Time     
MSN                         STRING(30)                     !M.S.N.              
DateDespatched              DATE                           !Date Despatched     
TimeDespatched              TIME                           !Time Despatched     
ReturnUser                  STRING(3)                      !Return Batch User Code
ReturnWaybillNo             STRING(30)                     !Return Waybill Number
ReturnRepairType            STRING(30)                     !Return Batch Repair Type
ReturnRejectedAmount        REAL                           !Rejected Amount     
ReturnRejectedReason        STRING(255)                    !Rejected Reason     
ThirdPartyInvoiceNo         STRING(30)                     !3rd Party Invoice Number
ThirdPartyInvoiceDate       DATE                           !3rd Party Invoice Date
ThirdPartyInvoiceCharge     REAL                           !3rd Party Invoice Charge
ThirdPartyVAT               REAL                           !3rd Party V.A.T.    
ThirdPartyMarkup            REAL                           !3rd Party Markup    
Manufacturer                STRING(30)                     !Manufacturer        
ModelNumber                 STRING(30)                     !Model Number        
BatchRunNotPrinted          BYTE                           !Not Printed As Part Of Batch Run
PurchaseOrderNumber         LONG                           !Oracle Purchase Order Number
NewThirdPartySite           STRING(30)                     !New Third Party Site
NewThirdPartySiteID         STRING(30)                     !New Third Party Site ID
                         END
                     END                       

TRACHRGE             FILE,DRIVER('Btrieve'),NAME('TRACHRGE.DAT'),PRE(trc),CREATE,BINDABLE,THREAD !Trade Account Charges
Account_Charge_Key       KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type,trc:Unit_Type,trc:Repair_Type),NOCASE,PRIMARY !By Charge Type      
Model_Repair_Key         KEY(trc:Model_Number,trc:Repair_Type),DUP,NOCASE !                    
Charge_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type),DUP,NOCASE !By Charge Type      
Unit_Type_Key            KEY(trc:Account_Number,trc:Model_Number,trc:Unit_Type),DUP,NOCASE !By Unit Type        
Repair_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Repair_Type),DUP,NOCASE !By Repair Type      
Cost_Key                 KEY(trc:Account_Number,trc:Model_Number,trc:Cost),DUP,NOCASE !By Cost             
Charge_Type_Only_Key     KEY(trc:Charge_Type),DUP,NOCASE   !                    
Repair_Type_Only_Key     KEY(trc:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(trc:Unit_Type),DUP,NOCASE     !                    
Record                   RECORD,PRE()
Account_Number              STRING(15)                     !                    
Charge_Type                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
dummy                       BYTE                           !                    
WarrantyClaimRate           REAL                           !Warranty Claim Rate 
HandlingFee                 REAL                           !Handling Fee        
Exchange                    REAL                           !Exchange            
RRCRate                     REAL                           !R.R.C. Rate         
                         END
                     END                       

TRDMODEL             FILE,DRIVER('Btrieve'),NAME('TRDMODEL.DAT'),PRE(trm),CREATE,BINDABLE,THREAD !Third Party Models  
Model_Number_Key         KEY(trm:Company_Name,trm:Model_Number),NOCASE,PRIMARY !By Model Number     
Model_Number_Only_Key    KEY(trm:Model_Number),DUP,NOCASE  !By Model Number     
Record                   RECORD,PRE()
Company_Name                STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
                         END
                     END                       

STOMODEL             FILE,DRIVER('Btrieve'),NAME('STOMODEL.DAT'),PRE(stm),CREATE,BINDABLE,THREAD !Stock Model Numbers 
RecordNumberKey          KEY(stm:RecordNumber),NOCASE,PRIMARY !By Record Number    
Model_Number_Key         KEY(stm:Ref_Number,stm:Manufacturer,stm:Model_Number),NOCASE !By Model Number     
Model_Part_Number_Key    KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE !By Model Number     
Description_Key          KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE !By Description      
Manufacturer_Key         KEY(stm:Manufacturer,stm:Model_Number),DUP,NOCASE !By Manufacturer     
Ref_Part_Description     KEY(stm:Ref_Number,stm:Part_Number,stm:Location,stm:Description),DUP,NOCASE !Why?                
Location_Part_Number_Key KEY(stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE !                    
Location_Description_Key KEY(stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE !                    
Mode_Number_Only_Key     KEY(stm:Ref_Number,stm:Model_Number),DUP,NOCASE !By Model Number     
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Manufacturer                STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Location                    STRING(30)                     !                    
Accessory                   STRING(3)                      !                    
FaultCode1                  STRING(30)                     !Fault Code 1        
FaultCode2                  STRING(30)                     !Fault Code 2        
FaultCode3                  STRING(30)                     !Fault Code 3        
FaultCode4                  STRING(30)                     !Fault Code 4        
FaultCode5                  STRING(30)                     !Fault Code 5        
FaultCode6                  STRING(30)                     !Fault Code 6        
FaultCode7                  STRING(30)                     !Fault Code 7        
FaultCode8                  STRING(30)                     !Fault Code 8        
FaultCode9                  STRING(30)                     !Fault Code 9        
FaultCode10                 STRING(30)                     !Fault Code 10       
FaultCode11                 STRING(30)                     !Fault Code 11       
FaultCode12                 STRING(30)                     !Fault Code 12       
RecordNumber                LONG                           !Record Number       
                         END
                     END                       

TRADEACC             FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(tra),CREATE,BINDABLE,THREAD !Trade Accounts      
RecordNumberKey          KEY(tra:RecordNumber),NOCASE,PRIMARY !By Record Number    
Account_Number_Key       KEY(tra:Account_Number),NOCASE    !By Account Number   
Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE  !By Company Name     
ReplicateFromKey         KEY(tra:ReplicateAccount,tra:Account_Number),DUP,NOCASE !By Account Number   
StoresAccountKey         KEY(tra:StoresAccount),DUP,NOCASE !By Stores Account   
SiteLocationKey          KEY(tra:SiteLocation),DUP,NOCASE  !By Site Location    
RegionKey                KEY(tra:Region),DUP,NOCASE        !By Region           
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Account_Number              STRING(15)                     !                    
Postcode                    STRING(15)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Contact_Name                STRING(30)                     !                    
Labour_Discount_Code        STRING(2)                      !                    
Retail_Discount_Code        STRING(2)                      !                    
Parts_Discount_Code         STRING(2)                      !                    
Labour_VAT_Code             STRING(2)                      !                    
Parts_VAT_Code              STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Account_Type                STRING(6)                      !                    
Credit_Limit                REAL                           !                    
Account_Balance             REAL                           !                    
Use_Sub_Accounts            STRING(3)                      !                    
Invoice_Sub_Accounts        STRING(3)                      !                    
Stop_Account                STRING(3)                      !                    
Allow_Cash_Sales            STRING(3)                      !                    
Price_Despatch              STRING(3)                      !                    
Price_First_Copy_Only       BYTE                           !Display pricing details on first copy of despatch note
Num_Despatch_Note_Copies    LONG                           !Number of despatch notes to print
Price_Retail_Despatch       STRING(3)                      !                    
Print_Despatch_Complete     STRING(3)                      !Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                      !Print Despatch Note At Despatch
Standard_Repair_Type        STRING(15)                     !                    
Courier_Incoming            STRING(30)                     !                    
Courier_Outgoing            STRING(30)                     !                    
Use_Contact_Name            STRING(3)                      !                    
VAT_Number                  STRING(30)                     !                    
Use_Delivery_Address        STRING(3)                      !                    
Use_Collection_Address      STRING(3)                      !                    
UseCustDespAdd              STRING(3)                      !Use Customer Address As Despatch Address
Allow_Loan                  STRING(3)                      !                    
Allow_Exchange              STRING(3)                      !                    
Force_Fault_Codes           STRING(3)                      !                    
Despatch_Invoiced_Jobs      STRING(3)                      !                    
Despatch_Paid_Jobs          STRING(3)                      !                    
Print_Despatch_Notes        STRING(3)                      !                    
Print_Retail_Despatch_Note  STRING(3)                      !                    
Print_Retail_Picking_Note   STRING(3)                      !                    
Despatch_Note_Per_Item      STRING(3)                      !                    
Skip_Despatch               STRING(3)                      !Use Batch Despatch  
IgnoreDespatch              STRING(3)                      !Do not use Despatch Table
Summary_Despatch_Notes      STRING(3)                      !                    
Exchange_Stock_Type         STRING(30)                     !                    
Loan_Stock_Type             STRING(30)                     !                    
Courier_Cost                REAL                           !                    
Force_Estimate              STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Turnaround_Time             STRING(30)                     !                    
Password                    STRING(20)                     !                    
Retail_Payment_Type         STRING(3)                      !                    
Retail_Price_Structure      STRING(3)                      !                    
Use_Customer_Address        STRING(3)                      !                    
Invoice_Customer_Address    STRING(3)                      !                    
ZeroChargeable              STRING(3)                      !Don't show Chargeable Costs
RefurbCharge                STRING(3)                      !                    
ChargeType                  STRING(30)                     !                    
WarChargeType               STRING(30)                     !                    
ExchangeAcc                 STRING(3)                      !Account used for repairing exchange units
MultiInvoice                STRING(3)                      !Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                      !Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)                    !For ProForma Export 
ImportPath                  STRING(255)                    !For ProForma Export 
BatchNumber                 LONG                           !ProForma Batch Number
ExcludeBouncer              BYTE                           !Exclude From Bouncer
RetailZeroParts             BYTE                           !Zero Value Parts    
HideDespAdd                 BYTE                           !Hide Address On Despatch Note
EuroApplies                 BYTE                           !Apply Euro          
ForceCommonFault            BYTE                           !Force Common Fault  
ForceOrderNumber            BYTE                           !Force Order Number  
ShowRepairTypes             BYTE                           !Repair Type Display 
WebMemo                     STRING(255)                    !Web Memo            
WebPassword1                STRING(30)                     !Web Password1       
WebPassword2                STRING(30)                     !WebPassword2        
InvoiceAtDespatch           BYTE                           !Print Invoice At Despatch
InvoiceType                 BYTE                           !Invoice Type        
IndividualSummary           BYTE                           !Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                           !Use Trade Contact No
StopThirdParty              BYTE                           !Stop Unit For Going To Third Party
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
UseDespatchDetails          BYTE                           !Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                     !Telephone Number    
AltFaxNumber                STRING(30)                     !Fax Number          
AltEmailAddress             STRING(255)                    !Email Address       
AutoSendStatusEmails        BYTE                           !Auto Send Status Emails
EmailRecipientList          BYTE                           !Email Recipient List
EmailEndUser                BYTE                           !Email End User      
ForceEndUserName            BYTE                           !Force End User Name 
ChangeInvAddress            BYTE                           !Invoice Address     
ChangeCollAddress           BYTE                           !Collection Address  
ChangeDelAddress            BYTE                           !Delivery Address    
AllowMaximumDiscount        BYTE                           !Set Maximum Discount Level
MaximumDiscount             REAL                           !Maximum Discount    
SetInvoicedJobStatus        BYTE                           !Set Completed Status
SetDespatchJobStatus        BYTE                           !Set Completed Status
InvoicedJobStatus           STRING(30)                     !Completed Status    
DespatchedJobStatus         STRING(30)                     !Completed Status    
CCommissionInHouse          LONG                           !Chargeable          
WCommissionInHouse          LONG                           !Warranty            
CCommissionOutSource        LONG                           !Chargeable          
WCommissionOutSource        LONG                           !Warranty            
ReplicateAccount            STRING(30)                     !Account Replicated From
RemoteRepairCentre          BYTE                           !Remote Repair Centre
SiteLocation                STRING(30)                     !Site Location       
RRCFactor                   LONG                           !R.R.C. Chargeable (%)
ARCFactor                   LONG                           !A.R.C. Chargeable (%)
TransitType                 STRING(30)                     !Transit Type        
ForceMobileNumber           BYTE                           !Force Mobile Number 
BranchIdentification        STRING(2)                      !                    
AllocateQAEng               BYTE                           !                    
InvoiceAtCompletion         BYTE                           !Invoice At Completion
InvoiceTypeComplete         BYTE                           !Invoice Type        
UseTimingsFrom              STRING(30)                     !Use Timings From    
StartWorkHours              TIME                           !Start Work Hours    
EndWorkHours                TIME                           !End Work Hours      
IncludeSaturday             STRING(3)                      !Include Saturday    
IncludeSunday               STRING(3)                      !Include Sunday      
StoresAccount               STRING(30)                     !Stores Account      
RepairEngineerQA            BYTE                           !Repair Engineer To QA Job
SecondYearAccount           BYTE                           !Second Year Warranty Account (True/False)
Activate48Hour              BYTE                           !Activate 48 Hour Exchange Process
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
Line500AccountNumber        STRING(30)                     !Line 500 Account Number
CompanyOwned                BYTE                           !Company Ownder Franchise
OverrideMobileDefault       BYTE                           !Override Mandatory Mobile Number
Region                      STRING(30)                     !Region              
AllowCreditNotes            BYTE                           !Allow Credit Notes  
DoMSISDNCheck               BYTE                           !Do MSISDN Check     
Hub                         STRING(30)                     !Hub                 
UseSBOnline                 BYTE                           !Use SB Online       
IgnoreReplenishmentProcess  BYTE                           !Ignore Replenishment Process
VCPWaybillPrefix            STRING(30)                     !VCP Waybill Prefix  
RRCWaybillPrefix            STRING(30)                     !RRC Waybill Prefix  
OBFCompanyName              STRING(30)                     !                    
OBFAddress1                 STRING(30)                     !                    
OBFAddress2                 STRING(30)                     !                    
OBFSuburb                   STRING(30)                     !                    
OBFContactName              STRING(30)                     !                    
OBFContactNumber            STRING(15)                     !                    
OBFEmailAddress             STRING(255)                    !                    
SBOnlineJobProgress         BYTE                           !Job Progress        
coTradingName               STRING(40)                     !                    
coTradingName2              STRING(40)                     !                    
coLocation                  STRING(40)                     !                    
coRegistrationNo            STRING(30)                     !                    
coVATNumber                 STRING(30)                     !                    
coAddressLine1              STRING(40)                     !                    
coAddressLine2              STRING(40)                     !                    
coAddressLine3              STRING(40)                     !                    
coAddressLine4              STRING(40)                     !                    
coTelephoneNumber           STRING(30)                     !                    
coFaxNumber                 STRING(30)                     !                    
coEmailAddress              STRING(255)                    !                    
RtnExchangeAccount          STRING(30)                     !                    
                         END
                     END                       

SUBCHRGE             FILE,DRIVER('Btrieve'),NAME('SUBCHRGE.DAT'),PRE(suc),CREATE,BINDABLE,THREAD !Sub Trade Account Charges
Model_Repair_Type_Key    KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type,suc:Unit_Type,suc:Repair_Type),NOCASE,PRIMARY !By Repair Type      
Account_Charge_Key       KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type),DUP,NOCASE !By Charge Type      
Unit_Type_Key            KEY(suc:Account_Number,suc:Model_Number,suc:Unit_Type),DUP,NOCASE !By Unit Type        
Repair_Type_Key          KEY(suc:Account_Number,suc:Model_Number,suc:Repair_Type),DUP,NOCASE !By Repair Type      
Cost_Key                 KEY(suc:Account_Number,suc:Model_Number,suc:Cost),DUP,NOCASE !By Cost             
Charge_Type_Only_Key     KEY(suc:Charge_Type),DUP,NOCASE   !                    
Repair_Type_Only_Key     KEY(suc:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(suc:Unit_Type),DUP,NOCASE     !                    
Model_Repair_Key         KEY(suc:Model_Number,suc:Repair_Type),DUP,NOCASE !                    
Record                   RECORD,PRE()
Account_Number              STRING(15)                     !                    
Charge_Type                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
dummy                       BYTE                           !                    
WarrantyClaimRate           REAL                           !Warranty Claim Rate 
HandlingFee                 REAL                           !Handling Fee        
Exchange                    REAL                           !Exchange            
RRCRate                     REAL                           !R.R.C. Rate         
                         END
                     END                       

DEFCHRGE             FILE,DRIVER('Btrieve'),NAME('DEFCHRGE.DAT'),PRE(dec),CREATE,BINDABLE,THREAD !Default Charges     
Charge_Type_Key          KEY(dec:Charge_Type),NOCASE,PRIMARY !By Charge Type      
Repair_Type_Only_Key     KEY(dec:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(dec:Unit_Type),DUP,NOCASE     !                    
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
                         END
                     END                       

SUBTRACC             FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(sub),CREATE,BINDABLE,THREAD !Sub Trade Accounts  
RecordNumberKey          KEY(sub:RecordNumber),NOCASE,PRIMARY !By Record Number    
Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE !By Account Number   
Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE !By Company Name     
Account_Number_Key       KEY(sub:Account_Number),NOCASE    !By Account Number   
Branch_Key               KEY(sub:Branch),DUP,NOCASE        !By Branch           
Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE !By Branch           
Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE  !By Company Name     
ReplicateFromKey         KEY(sub:ReplicateAccount,sub:Account_Number),DUP,NOCASE !By Account Number   
GenericAccountKey        KEY(sub:Generic_Account,sub:Account_Number),DUP,NOCASE !                    
GenericCompanyKey        KEY(sub:Generic_Account,sub:Company_Name),DUP,NOCASE !                    
GenericBranchKey         KEY(sub:Generic_Account,sub:Branch),DUP,NOCASE !                    
RegionKey                KEY(sub:Region),DUP,NOCASE        !By Region           
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Main_Account_Number         STRING(15)                     !                    
Account_Number              STRING(15)                     !                    
Postcode                    STRING(15)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Branch                      STRING(30)                     !                    
Contact_Name                STRING(30)                     !                    
Enquiry_Source              STRING(30)                     !                    
Labour_Discount_Code        STRING(2)                      !                    
Retail_Discount_Code        STRING(2)                      !                    
Parts_Discount_Code         STRING(2)                      !                    
Labour_VAT_Code             STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Parts_VAT_Code              STRING(2)                      !                    
Account_Type                STRING(6)                      !                    
Credit_Limit                REAL                           !                    
Account_Balance             REAL                           !                    
Stop_Account                STRING(3)                      !                    
Allow_Cash_Sales            STRING(3)                      !                    
Use_Delivery_Address        STRING(3)                      !                    
Use_Collection_Address      STRING(3)                      !                    
UseCustDespAdd              STRING(3)                      !Use Customer Address As Despatch Address
Courier_Incoming            STRING(30)                     !                    
Courier_Outgoing            STRING(30)                     !                    
VAT_Number                  STRING(30)                     !                    
Despatch_Invoiced_Jobs      STRING(3)                      !                    
Despatch_Paid_Jobs          STRING(3)                      !                    
Print_Despatch_Notes        STRING(3)                      !                    
PriceDespatchNotes          BYTE                           !Price Despatch Notes
Price_First_Copy_Only       BYTE                           !Display pricing details on first copy of despatch note
Num_Despatch_Note_Copies    LONG                           !Number of despatch notes to print
Print_Despatch_Complete     STRING(3)                      !Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                      !Print Despatch Note At Despatch
Print_Retail_Despatch_Note  STRING(3)                      !                    
Print_Retail_Picking_Note   STRING(3)                      !                    
Despatch_Note_Per_Item      STRING(3)                      !                    
Summary_Despatch_Notes      STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Password                    STRING(20)                     !                    
Retail_Payment_Type         STRING(3)                      !                    
Retail_Price_Structure      STRING(3)                      !                    
Use_Customer_Address        STRING(3)                      !                    
Invoice_Customer_Address    STRING(3)                      !                    
ZeroChargeable              STRING(3)                      !Don't show Chargeable Costs
MultiInvoice                STRING(3)                      !Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                      !Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)                    !For ProForma Export 
ImportPath                  STRING(255)                    !For ProForma Export 
BatchNumber                 LONG                           !ProForma Batch Number
HideDespAdd                 BYTE                           !Hide Address On Despatch Note
EuroApplies                 BYTE                           !Apply Euro          
ForceCommonFault            BYTE                           !Force Common Fault  
ForceOrderNumber            BYTE                           !Force Order Number  
WebPassword1                STRING(30)                     !Web Password1       
WebPassword2                STRING(30)                     !WebPassword2        
InvoiceAtDespatch           BYTE                           !Print Invoice At Despatch
InvoiceType                 BYTE                           !Invoice Type        
IndividualSummary           BYTE                           !Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                           !Use Trade Contact No
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
UseDespatchDetails          BYTE                           !Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                     !Telephone Number    
AltFaxNumber                STRING(30)                     !Fax Number          
AltEmailAddress             STRING(255)                    !Email Address       
UseAlternativeAdd           BYTE                           !Use Alternative Delivery Addresses
AutoSendStatusEmails        BYTE                           !Auto Send Status Emails
EmailRecipientList          BYTE                           !Email Recipient List
EmailEndUser                BYTE                           !Email End User      
ForceEndUserName            BYTE                           !Force End User Name 
ChangeInvAddress            BYTE                           !Invoice Address     
ChangeCollAddress           BYTE                           !Collection Address  
ChangeDelAddress            BYTE                           !Delivery Address    
AllowMaximumDiscount        BYTE                           !Set Maximum Discount Level
MaximumDiscount             REAL                           !Maximum Discount    
SetInvoicedJobStatus        BYTE                           !Set Completed Status
SetDespatchJobStatus        BYTE                           !Set Completed Status
InvoicedJobStatus           STRING(30)                     !Completed Status    
DespatchedJobStatus         STRING(30)                     !Completed Status    
ReplicateAccount            STRING(30)                     !Account Replicated From
FactorAccount               BYTE                           !Factor Account      
ForceEstimate               BYTE                           !Force Estimate      
EstimateIfOver              REAL                           !Estimate If Over    
InvoiceAtCompletion         BYTE                           !Invoice At Completion
InvoiceTypeComplete         BYTE                           !Invoice Type        
Generic_Account             BYTE                           !                    
StoresAccountNumber         STRING(30)                     !* NOT USED *        
Force_Customer_Name         BYTE                           !                    
FinanceContactName          STRING(30)                     !Contact Name        
FinanceTelephoneNo          STRING(30)                     !Contact Tel Number  
FinanceEmailAddress         STRING(255)                    !Email Address       
FinanceAddress1             STRING(30)                     !Address             
FinanceAddress2             STRING(30)                     !Address             
FinanceAddress3             STRING(30)                     !Finance             
FinancePostcode             STRING(30)                     !Postcode            
AccountLimit                REAL                           !Account Limit       
ExcludeBouncer              BYTE                           !Exclude From Bouncer
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
ExcludeFromTATReport        BYTE                           !Exclude From TAT Report
OverrideHeadVATNo           BYTE                           !Override Head Account V.A.T. No
Line500AccountNumber        STRING(30)                     !Line 500 Account Number
OverrideHeadMobile          BYTE                           !Override Head Account Mandatory Mobile Check
OverrideMobileDefault       BYTE                           !Override Mandatory Mobile Number
SIDJobBooking               BYTE                           !SID Job Booking     
SIDJobEnquiry               BYTE                           !SID Job Enquiry     
Region                      STRING(30)                     !Region              
AllowVCPLoanUnits           BYTE                           !Allow VCP Loan Units
Hub                         STRING(30)                     !Hub                 
RefurbishmentAccount        BYTE                           !Refurbishment Account
VCPWaybillPrefix            STRING(30)                     !VCP Waybill Prefix  
PrintOOWVCPFee              BYTE                           !Print OOW VCP Fee   
OOWVCPFeeLabel              STRING(30)                     !OOW VCP Fee Label   
OOWVCPFeeAmount             REAL                           !OOW VCP Fee Amount  
PrintWarrantyVCPFee         BYTE                           !Print Warranty VCP Fee
WarrantyVCPFeeAmount        REAL                           !Warranty VCP Fee Amount
DealerID                    STRING(30)                     !Dealer ID           
                         END
                     END                       

CHARTYPE             FILE,DRIVER('Btrieve'),NAME('CHARTYPE.DAT'),PRE(cha),CREATE,BINDABLE,THREAD !Charge Types        
Charge_Type_Key          KEY(cha:Charge_Type),NOCASE,PRIMARY !By Charge Type      
Ref_Number_Key           KEY(cha:Ref_Number,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Physical_Damage_Key      KEY(cha:Allow_Physical_Damage,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Warranty_Key             KEY(cha:Warranty,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Warranty_Ref_Number_Key  KEY(cha:Warranty,cha:Ref_Number,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                     !Charge Type         
Force_Warranty              STRING(3)                      !Force Fault Codes (YES/NO)
Allow_Physical_Damage       STRING(3)                      !Allow Physical Damage (YES/NO)
Allow_Estimate              STRING(3)                      !Allow Estimate (YES/NO)
Force_Estimate              STRING(3)                      !Force Estimate (YES/NO)
Invoice_Customer            STRING(3)                      !Invoice Customer (YES/NO)
Invoice_Trade_Customer      STRING(3)                      !Invoice Trade Customer (YES/NO)
Invoice_Manufacturer        STRING(3)                      !Invoice Manufacturer (YES/NO)
No_Charge                   STRING(3)                      !NOT USED            
Zero_Parts                  STRING(3)                      !Fixed Price (RRC) (YES/NO)
Warranty                    STRING(3)                      !Warranty Charge Type (YES/NO)
Ref_Number                  REAL                           !                    
Exclude_EDI                 STRING(3)                      !Exclude From EDI Process (YES/NO)
ExcludeInvoice              STRING(3)                      !Exclude From Invoicing (YES/NO)
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table (True/False)
ForceAuthorisation          BYTE                           !Force Authorisation Code (True/False)
Zero_Parts_ARC              BYTE                           !Fixed Price ARC (True/False)
SecondYearWarranty          BYTE                           !Second Year Warranty (True/False)
                         END
                     END                       

WAYAUDIT             FILE,DRIVER('Btrieve'),OEM,NAME('WAYAUDIT.DAT'),PRE(waa),CREATE,BINDABLE,THREAD !Waybill Audit       
RecordNumberKey          KEY(waa:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(waa:WAYBILLSRecordNumber,waa:TheDate,waa:TheTime),DUP,NOCASE !By Date             
DateOnlyKey              KEY(waa:TheDate,waa:TheTime),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
WAYBILLSRecordNumber        LONG                           !Linke to WAYBILLS Record Number
UserCode                    STRING(3)                      !User Code           
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
Action                      STRING(60)                     !Action              
                         END
                     END                       

WAYBAWT              FILE,DRIVER('Btrieve'),OEM,NAME('WAYBAWT.DAT'),PRE(wya),CREATE,BINDABLE,THREAD !Waybill Generation - Awaiting Processing
WAYBAWTIDKey             KEY(wya:WAYBAWTID),NOCASE,PRIMARY !                    
AccountJobNumberKey      KEY(wya:AccountNumber,wya:JobNumber),DUP,NOCASE !                    
JobNumberKey             KEY(wya:JobNumber),DUP,NOCASE     !                    
Record                   RECORD,PRE()
WAYBAWTID                   LONG                           !                    
JobNumber                   LONG                           !                    
AccountNumber               STRING(30)                     !Header Account Number
Manufacturer                STRING(30)                     !                    
ModelNumber                 STRING(30)                     !                    
IMEINumber                  STRING(20)                     !                    
                         END
                     END                       

WAYSUND              FILE,DRIVER('Btrieve'),OEM,NAME('WAYSUND.DAT'),PRE(was),CREATE,BINDABLE,THREAD !Waybill Sundry Items
RecordNumberKey          KEY(was:RecordNumber),NOCASE,PRIMARY !By Record Number    
EnteredKey               KEY(was:WAYBILLSRecordNumber,was:RecordNumber),DUP,NOCASE !By Record Number    
DescriptionKey           KEY(was:WAYBILLSRecordNumber,was:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
WAYBILLSRecordNumber        LONG                           !Link to WAYBILLS Record Number
Description                 STRING(30)                     !Description         
Quantity                    STRING(30)                     !Quantity            
                         END
                     END                       

WAYBILLJ             FILE,DRIVER('Btrieve'),OEM,NAME('WAYBILLJ.DAT'),PRE(waj),CREATE,BINDABLE,THREAD !Jobs attached to waybills
RecordNumberKey          KEY(waj:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(waj:WayBillNumber,waj:JobNumber),DUP,NOCASE !By Job Number       
DescRecordNoKey          KEY(-waj:RecordNumber),DUP,NOCASE !By Record Number    
DescWaybillNoKey         KEY(-waj:WayBillNumber),DUP,NOCASE !By Waybill Number   
DescJobNumberKey         KEY(-waj:JobNumber),DUP,NOCASE    !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
WayBillNumber               LONG                           !WayBill Number      
JobNumber                   LONG                           !Job Number          
ReportJobNumber             STRING(30)                     !Report Job Number   
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
OrderNumber                 STRING(30)                     !Order Number        
SecurityPackNumber          STRING(30)                     !Security Pack Number
JobType                     STRING(3)                      !Job Type            
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(man),CREATE,BINDABLE,THREAD !Manufacturers       
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY !By Record Number    
Manufacturer_Key         KEY(man:Manufacturer),NOCASE      !By Manufacturer     
EDIFileTypeKey           KEY(man:EDIFileType,man:Manufacturer),DUP,NOCASE !By Manufacturer     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !Manufacturer        
Account_Number              STRING(30)                     !Account Number      
Postcode                    STRING(30)                     !Postcode            
Address_Line1               STRING(30)                     !Address Line 1      
Address_Line2               STRING(30)                     !Address Line 2      
Address_Line3               STRING(30)                     !Address Line 3      
Telephone_Number            STRING(30)                     !Telephone Number    
Fax_Number                  STRING(30)                     !Fax Number          
EmailAddress                STRING(30)                     !Email Address       
Use_MSN                     STRING(3)                      !Models Use MSN (YES/NO)
Contact_Name1               STRING(60)                     !Contact Name 1      
Contact_Name2               STRING(60)                     !Contact Name 2      
Head_Office_Telephone       STRING(15)                     !Head Office Telephone Number
Head_Office_Fax             STRING(15)                     !Head Office Fax Number
Technical_Support_Telephone STRING(15)                     !Technical Support Telephone Number
Technical_Support_Fax       STRING(15)                     !Technical Support Fax Number
Technical_Support_Hours     STRING(30)                     !Technical Support Hours
Batch_Number                LONG                           !Last EDI Batch Number
EDI_Account_Number          STRING(30)                     !EDI Account Number  
EDI_Path                    STRING(255)                    !EDI File Export Path
Trade_Account               STRING(30)                     !Warranty Account    
Supplier                    STRING(30)                     !Supplier            
Warranty_Period             REAL                           !Warranty Period From DOP
SamsungCount                LONG                           !Count Of Samsung Claims
IncludeAdjustment           STRING(3)                      !EDI Warranty Adjustments
AdjustPart                  BYTE                           !Assign Part Number To Warranty Adjustment
ForceParts                  BYTE                           !Force Warranty Adjustment If No Parts Used
NokiaType                   STRING(20)                     !Nokia Type (COMMUNICAID/NSH)
RemAccCosts                 BYTE                           !Remove Accessory Claims Costs
SiemensNewEDI               BYTE                           !Use New EDI Format  
DOPCompulsory               BYTE                           !D.O.P. Compulsory for Warranty
Notes                       STRING(255)                    !                    
UseQA                       BYTE                           !Use QA (True/False) 
UseElectronicQA             BYTE                           !Use Electronic QA (True/False)
QALoanExchange              BYTE                           !QA Loan/Exchange Units (True/False)
QAAtCompletion              BYTE                           !QA At Completion (True/False)
SiemensNumber               LONG                           !Siemens Number      
SiemensDate                 DATE                           !Date                
UseProductCode              BYTE                           !Use Product Code (True/False)
ApplyMSNFormat              BYTE                           !Apply M.S.N. Format (True/False)
MSNFormat                   STRING(30)                     !M.S.N. Format       
ValidateDateCode            BYTE                           !Validate Date Code (True/False)
ForceStatus                 BYTE                           !Force Status (True/False)
StatusRequired              STRING(30)                     !Status Required     
POPPeriod                   LONG                           !P.O.P. Period       
ClaimPeriod                 LONG                           !Claim Period        
QAParts                     BYTE                           !Validate Parts At QA (True/False)
QANetwork                   BYTE                           !Validate Network At QA (True/False)
POPRequired                 BYTE                           !POP Required For Claim (True/False)
UseInvTextForFaults         BYTE                           !Use Main Fault For Invoice Text (True/False)
ForceAccessoryCode          BYTE                           !Force Accessory Fault Codes Only (True/False)
UseInternetValidation       BYTE                           !Use Internet Validation (True/False)
AutoRepairType              BYTE                           !AutoRepairType (True/False)
BillingConfirmation         BYTE                           !Display Billing Confirmation Screen (True/False)
CreateEDIReport             BYTE                           !Create Service History Report (True/False)
EDIFileType                 STRING(30)                     !E.D.I. File Type    
CreateEDIFile               BYTE                           !Create EDI Export File
ExchangeFee                 REAL                           !Exchange Fee        
QALoan                      BYTE                           !QA Loans (True/False)
ForceCharFaultCodes         BYTE                           !Force Out Of Warranty Fault Codes (True/False)
IncludeCharJobs             BYTE                           !Include Out Of Warranty Jobs (True/False)
SecondYrExchangeFee         REAL                           !Second Year Exchange Fee
SecondYrTradeAccount        STRING(30)                     !Second Year Warranty Account
VATNumber                   STRING(30)                     !VAT Number          
UseProdCodesForEXC          BYTE                           !Use Handset Part Number Of Exchanges
UseFaultCodesForOBF         BYTE                           !Use Fault Code For OBF Jobs
KeyRepairRequired           BYTE                           !Force "Key Repair" Part
UseReplenishmentProcess     BYTE                           !Use Replenishment Process
UseResubmissionLimit        BYTE                           !Use Warranty Claim Resubmission Limit
ResubmissionLimit           LONG                           !Days To Resubmit By 
AutoTechnicalReports        BYTE                           !Auto-create Technical Reports
AlertEmailAddress           STRING(255)                    !Alert Email Address 
LimitResubmissions          BYTE                           !Limit Resubmissions 
TimesToResubmit             LONG                           !Times Allowed To Resubmit
CopyDOPFromBouncer          BYTE                           !Copy D.O.P. From Bouncer Job
ExcludeAutomaticRebookingProcess BYTE                      !Exclude Automatic Rebooking Process
OneYearWarrOnly             STRING(1)                      !                    
EDItransportFee             REAL                           !                    
SagemVersionNumber          STRING(30)                     !                    
UseBouncerRules             BYTE                           !                    
BouncerPeriod               LONG                           !                    
BouncerType                 BYTE                           !                    
BouncerInFault              BYTE                           !                    
BouncerOutFault             BYTE                           !                    
BouncerSpares               BYTE                           !                    
BouncerPeriodIMEI           LONG                           !                    
DoNotBounceWarranty         BYTE                           !                    
BounceRulesType             BYTE                           !Bounce Rules Type   
                         END
                     END                       

USMASSIG             FILE,DRIVER('Btrieve'),NAME('USMASSIG.DAT'),PRE(usm),CREATE,BINDABLE,THREAD !Users Model Number Assignments
Model_Number_Key         KEY(usm:User_Code,usm:Model_Number),NOCASE,PRIMARY !By Model Number     
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
                         END
                     END                       

WEBJOB               FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOB.DAT'),PRE(wob),CREATE,BINDABLE,THREAD !Web Job             
RecordNumberKey          KEY(wob:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(wob:RefNumber),DUP,NOCASE     !By Ref Number       
HeadJobNumberKey         KEY(wob:HeadAccountNumber,wob:JobNumber),DUP,NOCASE !By Job Number       
HeadOrderNumberKey       KEY(wob:HeadAccountNumber,wob:OrderNumber,wob:RefNumber),DUP,NOCASE !By Order Number     
HeadMobileNumberKey      KEY(wob:HeadAccountNumber,wob:MobileNumber,wob:RefNumber),DUP,NOCASE !By Mobile Number    
HeadModelNumberKey       KEY(wob:HeadAccountNumber,wob:ModelNumber,wob:RefNumber),DUP,NOCASE !By Job Number       
HeadIMEINumberKey        KEY(wob:HeadAccountNumber,wob:IMEINumber,wob:RefNumber),DUP,NOCASE !By I.M.E.I. Number  
HeadMSNKey               KEY(wob:HeadAccountNumber,wob:MSN),DUP,NOCASE !By M.S.N.           
HeadEDIKey               KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:JobNumber),DUP,NOCASE !By Job Number       
ValidationIMEIKey        KEY(wob:HeadAccountNumber,wob:Validation,wob:IMEINumber),DUP,NOCASE !By I.M.E.I. Number  
HeadSubKey               KEY(wob:HeadAccountNumber,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE !                    
HeadRefNumberKey         KEY(wob:HeadAccountNumber,wob:RefNumber),DUP,NOCASE !                    
OracleNumberKey          KEY(wob:OracleExportNumber),DUP,NOCASE !By Oracle Number    
HeadCurrentStatusKey     KEY(wob:HeadAccountNumber,wob:Current_Status,wob:RefNumber),DUP,NOCASE !By Job Status       
HeadExchangeStatus       KEY(wob:HeadAccountNumber,wob:Exchange_Status,wob:RefNumber),DUP,NOCASE !By Exchange Status  
HeadLoanStatusKey        KEY(wob:HeadAccountNumber,wob:Loan_Status,wob:RefNumber),DUP,NOCASE !By Loan Status      
ExcWayBillNoKey          KEY(wob:ExcWayBillNumber),DUP,NOCASE !By Exchange Waybill Number
LoaWayBillNoKey          KEY(wob:LoaWayBillNumber),DUP,NOCASE !By Loan Waybill Number
JobWayBillNoKey          KEY(wob:JobWayBillNumber),DUP,NOCASE !By Job WayBill Number
RRCWInvoiceNumberKey     KEY(wob:RRCWInvoiceNumber,wob:RefNumber),DUP,NOCASE !By Job Number       
DateBookedKey            KEY(wob:HeadAccountNumber,wob:DateBooked),DUP,NOCASE !By Date Booked      
DateCompletedKey         KEY(wob:HeadAccountNumber,wob:DateCompleted),DUP,NOCASE !By Date Completed   
CompletedKey             KEY(wob:HeadAccountNumber,wob:Completed),DUP,NOCASE !By Completed        
DespatchKey              KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:RefNumber),DUP,NOCASE !By Job Number       
DespatchSubKey           KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE !By Job Number       
DespatchCourierKey       KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE !By Job Number       
DespatchSubCourierKey    KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE !By Job Number       
EDIKey                   KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:RefNumber),DUP,NOCASE !By Job Number       
EDIRefNumberKey          KEY(wob:HeadAccountNumber,wob:EDI,wob:RefNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JobNumber                   LONG                           !Job Number          
RefNumber                   LONG                           !Link to JOBS RefNumber
HeadAccountNumber           STRING(30)                     !Header Account Number
SubAcountNumber             STRING(30)                     !Sub Account Number  
OrderNumber                 STRING(30)                     !Order Number        
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
MSN                         STRING(30)                     !M.S.N.              
Manufacturer                STRING(30)                     !Manufacturer        
ModelNumber                 STRING(30)                     !Model Number        
MobileNumber                STRING(30)                     !Mobile Number       
EDI                         STRING(3)                      !EDI                 
Validation                  STRING(3)                      !Validation          
OracleExportNumber          LONG                           !Oracle Export Number
Current_Status              STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Loan_Status                 STRING(30)                     !                    
Current_Status_Date         DATE                           !                    
Exchange_Status_Date        DATE                           !                    
Loan_Status_Date            DATE                           !                    
ExcWayBillNumber            LONG                           !Exchange Waybill Number
LoaWayBillNumber            LONG                           !Loan Waybill Number 
JobWayBillNumber            LONG                           !Job Waybill Number  
DateJobDespatched           DATE                           !Date Despatched     
RRCEngineer                 STRING(3)                      !RRC Engineer        
ReconciledMarker            BYTE                           !Marker              
RRCWInvoiceNumber           LONG                           !Warranty Invoice Number
DateBooked                  DATE                           !Copy Of JOBS Date Booked
TimeBooked                  TIME                           !Copy Of JOBS Time Booked
DateCompleted               DATE                           !Copy Of JOBS Date Completed
TimeCompleted               TIME                           !Copy Of JOBS Time Completed
Completed                   STRING(3)                      !Copy Of JOBS Completed
ReadyToDespatch             BYTE                           !Ready To Despatch   
DespatchCourier             STRING(30)                     !Courier To Despatch 
FaultCode13                 STRING(30)                     !Fault Code 13       
FaultCode14                 STRING(30)                     !Fault Code 14       
FaultCode15                 STRING(30)                     !Fault Code 15       
FaultCode16                 STRING(30)                     !Fault Code 16       
FaultCode17                 STRING(30)                     !Fault Code 17       
FaultCode18                 STRING(30)                     !Fault Code 18       
FaultCode19                 STRING(30)                     !Fault Code 19       
FaultCode20                 STRING(30)                     !Fault Code 20       
                         END
                     END                       

USUASSIG             FILE,DRIVER('Btrieve'),NAME('USUASSIG.DAT'),PRE(usu),CREATE,BINDABLE,THREAD !Users Unit Type Assignments
Unit_Type_Key            KEY(usu:User_Code,usu:Unit_Type),NOCASE,PRIMARY !By Unit Type        
Unit_Type_Only_Key       KEY(usu:Unit_Type),DUP,NOCASE     !By Unit Type        
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Unit_Type                   STRING(30)                     !                    
                         END
                     END                       

JOBSLOCK             FILE,DRIVER('Btrieve'),NAME('JOBSLOCK.DAT'),PRE(lock),CREATE,BINDABLE,THREAD !Table To Hold Locked Jobs
RecordNumberKey          KEY(lock:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(lock:JobNumber),NOCASE        !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
JobNumber                   LONG                           !                    
DateLocked                  DATE                           !                    
TimeLocked                  TIME                           !                    
SessionValue                LONG                           !                    
                         END
                     END                       

TRAHUBAC             FILE,DRIVER('Btrieve'),NAME('TRAHUBAC.DAT'),PRE(TRA1),CREATE,BINDABLE,THREAD !Trade Hub Accounts  
RecordNoKey              KEY(TRA1:RecordNo),NOCASE,PRIMARY !                    
HeadAccSubAccKey         KEY(TRA1:HeadAcc,TRA1:SubAcc),DUP,NOCASE !                    
HeadAccSubAccNameKey     KEY(TRA1:HeadAcc,TRA1:SubAccName),DUP,NOCASE !                    
HeadAccSubAccBranchKey   KEY(TRA1:HeadAcc,TRA1:SubAccBranch),DUP,NOCASE !                    
Record                   RECORD,PRE()
RecordNo                    LONG                           !                    
HeadAcc                     STRING(15)                     !                    
SubAcc                      STRING(15)                     !                    
SubAccName                  STRING(30)                     !                    
SubAccBranch                STRING(30)                     !                    
                         END
                     END                       

STOHISTE             FILE,DRIVER('Btrieve'),NAME('STOHISTE.DAT'),PRE(stoe),CREATE,BINDABLE,THREAD !Stock History Extension
RecordNumberKey          KEY(stoe:RecordNumber),NOCASE,PRIMARY !By Record Number    
SHIRecordNumberKey       KEY(stoe:SHIRecordNumber),DUP,NOCASE !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
SHIRecordNumber             LONG                           !                    
PreviousAveragePurchaseCost REAL                           !                    
PurchaseCost                REAL                           !                    
SaleCost                    REAL                           !                    
RetailCost                  REAL                           !                    
                         END
                     END                       

RTNORDER             FILE,DRIVER('Btrieve'),NAME('RTNORDER.DAT'),PRE(rtn),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(rtn:RecordNumber),NOCASE,PRIMARY !                    
LocationPartNumberKey    KEY(rtn:Location,rtn:ExchangeOrder,rtn:PartNumber),DUP,NOCASE !By Part Number      
LocationStatusPartKey    KEY(rtn:Location,rtn:ExchangeOrder,rtn:Status,rtn:PartNumber),DUP,NOCASE !By Part Number      
OrderStatusPartNumberKey KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:Status,rtn:PartNumber),DUP,NOCASE !By Part Number      
OrderPartNumberKey       KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:PartNumber),DUP,NOCASE !By Part Number      
CreditNoteNumberKey      KEY(rtn:CreditNoteRequestNumber),DUP,NOCASE !By Credit Note Number
WaybillNumberKey         KEY(rtn:WaybillNumber),DUP,NOCASE !By Waybill Number   
RefNumberKey             KEY(rtn:ExchangeOrder,rtn:RefNumber),DUP,NOCASE !By Ref Number       
OrderStatusReturnRefNoKey KEY(rtn:OrderNumber,rtn:ExchangeOrder,rtn:ReturnType,rtn:Status,rtn:RefNumber),DUP,NOCASE !By Part Number      
LocArcOrdWaybillKey      KEY(rtn:Archived,rtn:Ordered,rtn:Location,rtn:WaybillNumber),DUP,NOCASE !By Waybill Number   
ArcOrdWaybillKey         KEY(rtn:Archived,rtn:Ordered,rtn:WaybillNumber),DUP,NOCASE !By Waybill Number   
ArcLocDateKey            KEY(rtn:Archived,rtn:Location,rtn:DateCreated),DUP,NOCASE !                    
ArcDateKey               KEY(rtn:Archived,rtn:DateCreated),DUP,NOCASE !                    
DateOrderedKey           KEY(rtn:Location,rtn:DateOrdered,rtn:TimeOrdered),DUP,NOCASE !                    
DateOrderedReceivedKey   KEY(rtn:Location,rtn:DateOrdered,rtn:TimeOrdered),DUP,NOCASE !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
Archived                    BYTE                           !Don't show on the Tracking Screen
DateCreated                 DATE                           !                    
TimeCreated                 TIME                           !                    
Location                    STRING(30)                     !                    
UserCode                    STRING(3)                      !                    
RefNumber                   LONG                           !Exchange Ref Number/Stock Ref Number
OrderNumber                 LONG                           !                    
InvoiceNumber               LONG                           !                    
PartNumber                  STRING(30)                     !                    
Description                 STRING(30)                     !                    
QuantityReturned            LONG                           !                    
ExchangeOrder               BYTE                           !                    
Status                      STRING(3)                      !                    
Notes                       STRING(255)                    !                    
ReturnType                  STRING(30)                     !                    
PurchaseCost                REAL                           !                    
SaleCost                    REAL                           !                    
ExchangePrice               REAL                           !                    
CreditNoteRequestNumber     LONG                           !                    
WaybillNumber               LONG                           !                    
Ordered                     BYTE                           !Has the order been raised?
DateOrdered                 DATE                           !                    
TimeOrdered                 TIME                           !                    
WhoOrdered                  STRING(3)                      !                    
Received                    BYTE                           !                    
DateReceived                DATE                           !                    
TimeReceived                TIME                           !                    
WhoReceived                 STRING(3)                      !                    
WhoProcessed                STRING(3)                      !                    
                         END
                     END                       

WAYCNR               FILE,DRIVER('Btrieve'),NAME('WAYCNR.DAT'),PRE(wcr),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(wcr:RecordNumber),NOCASE,PRIMARY !By Record Number    
EnteredKey               KEY(wcr:WAYBILLSRecordNumber,wcr:RecordNumber),DUP,NOCASE !                    
PartNumberKey            KEY(wcr:WAYBILLSRecordNumber,wcr:ExchangeOrder,wcr:PartNumber),DUP,NOCASE !By Part Number      
ProcessedKey             KEY(wcr:WAYBILLSRecordNumber,wcr:Processed,wcr:PartNumber),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
WAYBILLSRecordNumber        LONG                           !                    
PartNumber                  STRING(30)                     !                    
Description                 STRING(30)                     !                    
ExchangeOrder               BYTE                           !                    
Quantity                    LONG                           !                    
QuantityReceived            LONG                           !                    
Processed                   BYTE                           !                    
RefNumber                   LONG                           !Link To Unit        
                         END
                     END                       

MODEXCHA             FILE,DRIVER('Btrieve'),NAME('MODEXCHA.DAT'),PRE(moa),CREATE,BINDABLE,THREAD !Exchange Order Model
RecordNumberKey          KEY(moa:RecordNumber),NOCASE,PRIMARY !                    
AccountNumberKey         KEY(moa:Manufacturer,moa:ModelNumber,moa:AccountNumber),NOCASE !By Account Number   
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
Manufacturer                STRING(30)                     !                    
ModelNumber                 STRING(30)                     !                    
AccountNumber               STRING(30)                     !                    
CompanyName                 STRING(30)                     !                    
                         END
                     END                       

MULDESPJ_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('MULDESPJ.DAT'),PRE(mulj_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(mulj_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(mulj_ali:RefNumber,mulj_ali:JobNumber),DUP,NOCASE !By Job Number       
CurrentJobKey            KEY(mulj_ali:RefNumber,mulj_ali:Current,mulj_ali:JobNumber),DUP,NOCASE !By Job Number       
JobNumberOnlyKey         KEY(mulj_ali:JobNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !RefNumber           
JobNumber                   LONG                           !Job Number          
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
MSN                         STRING(30)                     !M.S.N.              
AccountNumber               STRING(30)                     !AccountNumber       
Courier                     STRING(30)                     !Courier             
Current                     BYTE                           !Current             
SecurityPackNumber          STRING(30)                     !Security Pack Number
                         END
                     END                       

MULDESP_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(muld_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(muld_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
BatchNumberKey           KEY(muld_ali:BatchNumber),DUP,NOCASE !By Batch Number     
AccountNumberKey         KEY(muld_ali:AccountNumber),DUP,NOCASE !By Account Number   
CourierKey               KEY(muld_ali:Courier),DUP,NOCASE  !By Courier          
HeadBatchNumberKey       KEY(muld_ali:HeadAccountNumber,muld_ali:BatchNumber),DUP,NOCASE !By Batch Number     
HeadAccountKey           KEY(muld_ali:HeadAccountNumber,muld_ali:AccountNumber),DUP,NOCASE !By Account Number   
HeadCourierKey           KEY(muld_ali:HeadAccountNumber,muld_ali:Courier),DUP,NOCASE !By Courier          
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
BatchNumber                 STRING(30)                     !Batch Number        
AccountNumber               STRING(30)                     !Account Number      
Courier                     STRING(30)                     !Courier             
BatchTotal                  LONG                           !Total In Batch      
BatchType                   STRING(3)                      !Batch Type          
HeadAccountNumber           STRING(30)                     !Head Account Number 
                         END
                     END                       

JOBSOBF_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jofali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(jofali:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jofali:RefNumber),DUP,NOCASE  !By Job Number       
StatusRefNumberKey       KEY(jofali:Status,jofali:RefNumber),DUP,NOCASE !By Job Number       
StatusIMEINumberKey      KEY(jofali:Status,jofali:IMEINumber),DUP,NOCASE !By I.M.E.I. Number  
HeadAccountCompletedKey  KEY(jofali:HeadAccountNumber,jofali:DateCompleted,jofali:RefNumber),DUP,NOCASE !By Date Completed   
HeadAccountProcessedKey  KEY(jofali:HeadAccountNumber,jofali:DateProcessed,jofali:RefNumber),DUP,NOCASE !By Date Processed   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Like to JOBS RefNumber
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
Status                      BYTE                           !Processed Status    
Replacement                 BYTE                           !Replacement         
StoreReferenceNumber        STRING(30)                     !Stock Reference Number
RNumber                     STRING(30)                     !R Number            
RejectionReason             STRING(255)                    !Rejection Reason    
ReplacementIMEI             STRING(30)                     !Replacement I.M.E.I. Number
LAccountNumber              STRING(30)                     !L/Account Number    
UserCode                    STRING(3)                      !User Code           
HeadAccountNumber           STRING(30)                     !Head Account Number 
DateCompleted               DATE                           !Date Completed      
TimeCompleted               TIME                           !Time Completed      
DateProcessed               DATE                           !Date Process        
TimeProcessed               TIME                           !Time Processed      
                         END
                     END                       

JOBSE_ALIAS          FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(jobe_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jobe_ali:RefNumber),DUP,NOCASE !By Ref Number       
WarrStatusDateKey        KEY(jobe_ali:WarrantyStatusDate),DUP,NOCASE !By Warranty Status Date
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Refence To Job Number
JobMark                     BYTE                           !Job Mark            
TraFaultCode1               STRING(30)                     !Fault Code 1        
TraFaultCode2               STRING(30)                     !Fault Code 2        
TraFaultCode3               STRING(30)                     !Fault Code 3        
TraFaultCode4               STRING(30)                     !Fault Code 4        
TraFaultCode5               STRING(30)                     !Fault Code 5        
TraFaultCode6               STRING(30)                     !Fault Code 6        
TraFaultCode7               STRING(30)                     !Fault Code 7        
TraFaultCode8               STRING(30)                     !Fault Code 8        
TraFaultCode9               STRING(30)                     !Fault Code 9        
TraFaultCode10              STRING(30)                     !Fault Code 10       
TraFaultCode11              STRING(30)                     !Fault Code 11       
TraFaultCode12              STRING(30)                     !Fault Code 12       
SIMNumber                   STRING(30)                     !SIM Number          
JobReceived                 BYTE                           !Job Received        
SkillLevel                  LONG                           !Skill Level         
UPSFlagCode                 STRING(1)                      !UPS Flag Code       
FailedDelivery              BYTE                           !Failed Delivery     
CConfirmSecondEntry         STRING(3)                      !Chargeable Confirm Second Entry
WConfirmSecondEntry         STRING(3)                      !Warranty Confirm Second Entry
EndUserEmailAddress         STRING(255)                    !Email Address       
HubRepair                   BYTE                           !Hub Repair          
Network                     STRING(30)                     !Network             
POPConfirmed                BYTE                           !POPConfirmed        
HubRepairDate               DATE                           !HubRepairDate       
HubRepairTime               TIME                           !Hub Repair Time     
ClaimValue                  REAL                           !ClaimValue          
HandlingFee                 REAL                           !Handling Fee        
ExchangeRate                REAL                           !Exchange Rate       
InvoiceClaimValue           REAL                           !Invoice Claim Value 
InvoiceHandlingFee          REAL                           !Invoice Handling Fee
InvoiceExchangeRate         REAL                           !Invoice Exchange Rate
BoxESN                      STRING(20)                     !                    
ValidPOP                    STRING(3)                      !                    
ReturnDate                  DATE                           !                    
TalkTime                    REAL                           !                    
OriginalPackaging           BYTE                           !                    
OriginalBattery             BYTE                           !                    
OriginalCharger             BYTE                           !                    
OriginalAntenna             BYTE                           !                    
OriginalManuals             BYTE                           !                    
PhysicalDamage              BYTE                           !                    
OriginalDealer              CSTRING(255)                   !                    
BranchOfReturn              STRING(30)                     !                    
COverwriteRepairType        BYTE                           !Overwrite Repair Type
WOverwriteRepairType        BYTE                           !Overwrite Repair Type
LabourAdjustment            REAL                           !Labour Adjustment   
PartsAdjustment             REAL                           !Parts Adjustment    
SubTotalAdjustment          REAL                           !Sub Total Adjustment
IgnoreClaimCosts            BYTE                           !Ignore Claim Costs  
RRCCLabourCost              REAL                           !Labour Cost         
RRCCPartsCost               REAL                           !Parts Cost          
RRCCPartsSale               REAL                           !Parts Sale Cost     
RRCCSubTotal                REAL                           !RRC Sub Total       
InvRRCCLabourCost           REAL                           !Invoice Labour Cost 
InvRRCCPartsCost            REAL                           !Invoice Parts Cost  
InvRRCCPartsSale            REAL                           !Invoice Parts Sale Cost
InvRRCCSubTotal             REAL                           !Invoice Sub Total   
RRCWLabourCost              REAL                           !Labour Cost         
RRCWPartsCost               REAL                           !Parts Cost          
RRCWPartsSale               REAL                           !Parts Sale Cost     
RRCWSubTotal                REAL                           !RRC Sub Total       
InvRRCWLabourCost           REAL                           !Invoice Labour Cost 
InvRRCWPartsCost            REAL                           !Invoice Parts Cost  
InvRRCWPartsSale            REAL                           !Invoice Parts Sale Cost
InvRRCWSubTotal             REAL                           !Invoice Sub Total   
ARC3rdPartyCost             REAL                           !3rd Party Cost      
InvARC3rdPartCost           REAL                           !Invoice 3rd Party Cost
WebJob                      BYTE                           !Is this job a web job?
RRCELabourCost              REAL                           !RRC Estimate Labour Cost
RRCEPartsCost               REAL                           !RRC Estimate Parts Cost
RRCESubTotal                REAL                           !RRC Estimate Sub Total
IgnoreRRCChaCosts           REAL                           !Ignore Default Costs
IgnoreRRCWarCosts           REAL                           !Ignore Default Costs
IgnoreRRCEstCosts           REAL                           !Ignore Estimate Costs
OBFvalidated                BYTE                           !Out of Box Failure - Validation
OBFvalidateDate             DATE                           !OBF Validation Date 
OBFvalidateTime             TIME                           !OBF Validation Time 
DespatchType                STRING(3)                      !Despatch Type       
Despatched                  STRING(3)                      !Despatched          
WarrantyClaimStatus         STRING(30)                     !Warranty Claim Status
WarrantyStatusDate          DATE                           !Warranty Claim Status Date
InSecurityPackNo            STRING(30)                     !Incoming Security Pack Number
JobSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Job
ExcSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Exchange
LoaSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Loan
ExceedWarrantyRepairLimit   BYTE                           !Warranty Repair Authorised To Exceed Repair Limit
BouncerClaim                BYTE                           !Set true (1) if this was submitted from Bouncer Browse
Sub_Sub_Account             STRING(15)                     !                    
ConfirmClaimAdjustment      BYTE                           !Confirm Warranty Claim Adjustment Values
ARC3rdPartyVAT              REAL                           !3rd Party Vat       
ARC3rdPartyInvoiceNumber    STRING(30)                     !3rd Party Invoice Number
ExchangeAdjustment          REAL                           !Exchange Adjustment 
ExchangedATRRC              BYTE                           !Exchanged At RRC    
EndUserTelNo                STRING(15)                     !                    
ClaimColour                 BYTE                           !Colour Warranty Claim
ARC3rdPartyMarkup           REAL                           !3rd Party Markup    
Ignore3rdPartyCosts         BYTE                           !Ignore 3rd Party Costs
POPType                     STRING(30)                     !POP Type            
OBFProcessed                BYTE                           !OBF Routine Processed
LoanReplacementValue        REAL                           !Loan Replacement Value
PendingClaimColour          BYTE                           !Pending Claim Colour
AccessoryNotes              STRING(255)                    !Accessory Notes     
ClaimPartsCost              REAL                           !Claim Parts Cost    
InvClaimPartsCost           REAL                           !Invoice Parts Claim Cost
Booking48HourOption         BYTE                           !48 Hour Exchange (Booking)
Engineer48HourOption        BYTE                           !48 Hour Exchange (Engineering)
ExcReplcamentCharge         BYTE                           !Exchange Replacement Charge
SecondExchangeNumber        LONG                           !Second Exchange Unit Number
SecondExchangeStatus        STRING(30)                     !2nd Exchange Status 
VatNumber                   STRING(30)                     !Vat Number          
ExchangeProductCode         STRING(30)                     !Exchange Handset Part Number
SecondExcProdCode           STRING(30)                     !Second Exchange Handset Part Number
ARC3rdPartyInvoiceDate      DATE                           !                    
ARC3rdPartyWaybillNo        STRING(30)                     !                    
ARC3rdPartyRepairType       STRING(30)                     !                    
ARC3rdPartyRejectedReason   STRING(255)                    !                    
ARC3rdPartyRejectedAmount   REAL                           !                    
VSACustomer                 BYTE                           !VSA Customer        
HandsetReplacmentValue      REAL                           !Handset Replacement Value
SecondHandsetRepValue       REAL                           !Handset Replacement Value
t                           STRING(20)                     !                    
                         END
                     END                       

MANFAULT_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(maf_ali),CREATE,BINDABLE,THREAD !                    
Field_Number_Key         KEY(maf_ali:Manufacturer,maf_ali:Field_Number),NOCASE,PRIMARY !By Field Number     
MainFaultKey             KEY(maf_ali:Manufacturer,maf_ali:MainFault),DUP,NOCASE !By Main Fault       
InFaultKey               KEY(maf_ali:Manufacturer,maf_ali:InFault),DUP,NOCASE !By In Fault         
ScreenOrderKey           KEY(maf_ali:Manufacturer,maf_ali:ScreenOrder),DUP,NOCASE !By Screen Order     
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
Compulsory_At_Booking       STRING(3)                      !                    
ReplicateFault              STRING(3)                      !Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                      !Replicate To Fault Description Field
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
ForceFormat                 BYTE                           !Force Format        
FieldFormat                 STRING(30)                     !Field Format        
DateType                    STRING(30)                     !Date Type           
MainFault                   BYTE                           !Main Fault          
PromptForExchange           BYTE                           !Prompt For Exchange 
InFault                     BYTE                           !In Fault            
CompulsoryIfExchange        BYTE                           !Compulsory If Exchange Issued
NotAvailable                BYTE                           !Not Available       
CharCompulsory              BYTE                           !Compulsory At Completion
CharCompulsoryBooking       BYTE                           !Compulsory At Booking
ScreenOrder                 LONG                           !Screen Order        
RestrictAvailability        BYTE                           !Restrict Availability
RestrictServiceCentre       BYTE                           !Available For Service Centres
GenericFault                BYTE                           !Generic Fault       
NotCompulsoryThirdParty     BYTE                           !Not Compulsory For 3rd Party Job
HideThirdParty              BYTE                           !Hide For 3rd Party Jobs
HideRelatedCodeIfBlank      BYTE                           !Hide When Related Code Is Blank
BlankRelatedCode            LONG                           !Blank Related Code  
FillFromDOP                 BYTE                           !Fill From Date Of Purchase
CompulsoryForRepairType     BYTE                           !Compulsory For Repair Type
CompulsoryRepairType        STRING(30)                     !Repair Type         
                         END
                     END                       

JOBNOTES_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn_ali),CREATE,BINDABLE,THREAD !                    
RefNumberKey             KEY(jbn_ali:RefNumber),NOCASE,PRIMARY !By Job Number       
Record                   RECORD,PRE()
RefNumber                   LONG                           !Related Job Number  
Fault_Description           STRING(255)                    !                    
Engineers_Notes             STRING(255)                    !                    
Invoice_Text                STRING(255)                    !                    
Collection_Text             STRING(255)                    !                    
Delivery_Text               STRING(255)                    !                    
ColContatName               STRING(30)                     !Contact Name        
ColDepartment               STRING(30)                     !Department          
DelContactName              STRING(30)                     !Contact Name        
DelDepartment               STRING(30)                     !Department          
                         END
                     END                       

JOBPAYMT_ALIAS       FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt_ali),CREATE,BINDABLE,THREAD !                    
Record_Number_Key        KEY(jpt_ali:Record_Number),NOCASE,PRIMARY !By Record Number    
All_Date_Key             KEY(jpt_ali:Ref_Number,jpt_ali:Date),DUP,NOCASE !By Date             
Loan_Deposit_Key         KEY(jpt_ali:Ref_Number,jpt_ali:Loan_Deposit,jpt_ali:Date),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Payment_Type                STRING(30)                     !                    
Credit_Card_Number          STRING(20)                     !                    
Expiry_Date                 STRING(5)                      !                    
Issue_Number                STRING(5)                      !                    
Amount                      REAL                           !                    
User_Code                   STRING(3)                      !                    
Loan_Deposit                BYTE                           !Loan Deposit        
                         END
                     END                       

STOCK_ALIAS          FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto_ali),CREATE,BINDABLE,THREAD !                    
Ref_Number_Key           KEY(sto_ali:Ref_Number),NOCASE,PRIMARY !By Ref Number       
Sundry_Item_Key          KEY(sto_ali:Sundry_Item),DUP,NOCASE !By Sundry Item      
Description_Key          KEY(sto_ali:Location,sto_ali:Description),DUP,NOCASE !By Description      
Description_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Description),DUP,NOCASE !By Description      
Shelf_Location_Key       KEY(sto_ali:Location,sto_ali:Shelf_Location),DUP,NOCASE !By Shelf Location   
Shelf_Location_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE !By Shelf Location   
Supplier_Accessory_Key   KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE !By Supplier         
Manufacturer_Key         KEY(sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE !By Manufacturer     
Supplier_Key             KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Supplier),DUP,NOCASE !By Supplier         
Location_Key             KEY(sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE !By Location         
Part_Number_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
Ref_Part_Description_Key KEY(sto_ali:Location,sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE !Why?                
Location_Manufacturer_Key KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE !By Manufacturer     
Manufacturer_Accessory_Key KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Accessory,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
Location_Part_Description_Key KEY(sto_ali:Location,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE !By Part Number      
Ref_Part_Description2_Key KEY(sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE !CASCADE Key         
Minimum_Part_Number_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
Minimum_Description_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Description),DUP,NOCASE !                    
SecondLocKey             KEY(sto_ali:Location,sto_ali:Shelf_Location,sto_ali:Second_Location,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
RequestedKey             KEY(sto_ali:QuantityRequested,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
ExchangeAccPartKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
ExchangeAccDescKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Description),DUP,NOCASE !By Description      
DateBookedKey            KEY(sto_ali:DateBooked),DUP,NOCASE !By Date Booked      
Supplier_Only_Key        KEY(sto_ali:Supplier),DUP,NOCASE  !                    
LocPartSuspendKey        KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
LocDescSuspendKey        KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Description),DUP,NOCASE !By Description      
LocShelfSuspendKey       KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Shelf_Location),DUP,NOCASE !By Shelf Location   
LocManSuspendKey         KEY(sto_ali:Location,sto_ali:Suspend,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE !By Part Number      
ExchangeModelKey         KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:ExchangeModelNumber),DUP,NOCASE !                    
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)                      !                    
Ref_Number                  LONG                           !Reference Number    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
AccessoryCost               REAL                           !Accessory Cost      
Percentage_Mark_Up          REAL                           !                    
Shelf_Location              STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
Location                    STRING(30)                     !                    
Second_Location             STRING(30)                     !                    
Quantity_Stock              REAL                           !                    
Quantity_To_Order           REAL                           !                    
Quantity_On_Order           REAL                           !                    
Minimum_Level               REAL                           !                    
Reorder_Level               REAL                           !                    
Use_VAT_Code                STRING(3)                      !                    
Service_VAT_Code            STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Superceeded                 STRING(3)                      !                    
Superceeded_Ref_Number      REAL                           !                    
Pending_Ref_Number          REAL                           !                    
Accessory                   STRING(3)                      !                    
Minimum_Stock               STRING(3)                      !                    
Assign_Fault_Codes          STRING(3)                      !                    
Individual_Serial_Numbers   STRING(3)                      !                    
ExchangeUnit                STRING(3)                      !                    
QuantityRequested           LONG                           !Quantity Requested  
Suspend                     BYTE                           !Suspend Part        
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
ReturnFaultySpare           BYTE                           !Return Faulty Spare 
ChargeablePartOnly          BYTE                           !Chargeable Part Only
AttachBySolder              BYTE                           !Attach By Solder    
AllowDuplicate              BYTE                           !Allow Duplicate Part On Jobs
RetailMarkup                REAL                           !Percentage Mark Up  
VirtPurchaseCost            REAL                           !Purchase Cost       
VirtTradeCost               REAL                           !Trade Price         
VirtRetailCost              REAL                           !Retail Price        
VirtTradeMarkup             REAL                           !Percent Mark Up     
VirtRetailMarkup            REAL                           !Percentage Mark Up  
AveragePurchaseCost         REAL                           !Average Purchase Cost
PurchaseMarkUp              REAL                           !Percentage Mark Up  
RepairLevel                 LONG                           !Repair Index        
SkillLevel                  LONG                           !Skill Level         
DateBooked                  DATE                           !Date Booked         
AccWarrantyPeriod           LONG                           !Accessory Warranty Period
ExcludeLevel12Repair        BYTE                           !Exclude From Level 1 && 2 Repairs
ExchangeOrderCap            LONG                           !Exchange Order Cap  
ExchangeModelNumber         STRING(30)                     !                    
                         END
                     END                       

LOAN_ALIAS           FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa_ali),CREATE,BINDABLE,THREAD !                    
Ref_Number_Key           KEY(loa_ali:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
ESN_Only_Key             KEY(loa_ali:ESN),DUP,NOCASE       !                    
MSN_Only_Key             KEY(loa_ali:MSN),DUP,NOCASE       !                    
Ref_Number_Stock_Key     KEY(loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
Model_Available_Key      KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(loa_ali:Stock_Type),DUP,NOCASE !By Stock Type       
ModelRefNoKey            KEY(loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(loa_ali:Available,loa_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(loa_ali:Available,loa_ali:MSN),DUP,NOCASE !By M.S.N            
AvailRefOnlyKey          KEY(loa_ali:Available,loa_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(loa_ali:Available,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(loa_ali:Date_Booked),DUP,NOCASE !By Date Booked      
LocStockAvailIMEIKey     KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(loa_ali:Location,loa_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockRefKey           KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocRefKey                KEY(loa_ali:Location,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockAvailModelKey    KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockModelKey         KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocModelKey              KEY(loa_ali:Location,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockAvailMSNKey      KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:Available,loa_ali:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(loa_ali:Location,loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE !By Loan Unit Number 
LocMSNKey                KEY(loa_ali:Location,loa_ali:MSN),DUP,NOCASE !By M.S.N.           
AvailLocIMEI             KEY(loa_ali:Available,loa_ali:Location,loa_ali:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(loa_ali:Available,loa_ali:Location,loa_ali:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(loa_ali:Available,loa_ali:Location,loa_ali:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(loa_ali:Available,loa_ali:Location,loa_ali:Model_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !                    
Stock_Type                  STRING(30)                     !                    
DummyField                  STRING(1)                      !For File Manager    
StatusChangeDate            DATE                           !Status Change Date  
                         END
                     END                       

WARPARTS_ALIAS       FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(war_ali),CREATE,BINDABLE,THREAD !                    
Part_Number_Key          KEY(war_ali:Ref_Number,war_ali:Part_Number),DUP,NOCASE !By Part Number      
RecordNumberKey          KEY(war_ali:Record_Number),NOCASE,PRIMARY !                    
PartRefNoKey             KEY(war_ali:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Date_Ordered_Key         KEY(war_ali:Date_Ordered),DUP,NOCASE !By Date Ordered     
RefPartRefNoKey          KEY(war_ali:Ref_Number,war_ali:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PendingRefNoKey          KEY(war_ali:Ref_Number,war_ali:Pending_Ref_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(war_ali:Ref_Number,war_ali:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(war_ali:Supplier),DUP,NOCASE  !By Supplier         
Order_Part_Key           KEY(war_ali:Ref_Number,war_ali:Order_Part_Number),DUP,NOCASE !By Order Part Number
SuppDateOrdKey           KEY(war_ali:Supplier,war_ali:Date_Ordered),DUP,NOCASE !By Date Ordered     
SuppDateRecKey           KEY(war_ali:Supplier,war_ali:Date_Received),DUP,NOCASE !By Date Received    
RequestedKey             KEY(war_ali:Requested,war_ali:Part_Number),DUP,NOCASE !By Part Number      
WebOrderKey              KEY(war_ali:WebOrder,war_ali:Part_Number),DUP,NOCASE !By Part Number      
PartAllocatedKey         KEY(war_ali:PartAllocated,war_ali:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(war_ali:Status,war_ali:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(war_ali:PartAllocated,war_ali:Status,war_ali:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Job Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Stock Reference Number
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Quantity                    REAL                           !                    
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Reference Number
Order_Number                LONG                           !Order Number        
Order_Part_Number           REAL                           !                    
Date_Received               DATE                           !                    
Status_Date                 DATE                           !                    
Main_Part                   STRING(3)                      !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Requested For Order 
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
WebOrder                    BYTE                           !Web Order           
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
CostAdjustment              REAL                           !Cost Adjustment     
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
ExchangeUnit                BYTE                           !Exchange Unit Part  
SecondExchangeUnit          BYTE                           !Second Exchange Unit
Correction                  BYTE                           !Part Correction     
                         END
                     END                       

PARTS_ALIAS          FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par_ali),CREATE,BINDABLE,THREAD !                    
Part_Number_Key          KEY(par_ali:Ref_Number,par_ali:Part_Number),DUP,NOCASE !By Part Number      
recordnumberkey          KEY(par_ali:Record_Number),NOCASE,PRIMARY !                    
PartRefNoKey             KEY(par_ali:Part_Ref_Number),DUP,NOCASE !By Part_Ref_Number  
Date_Ordered_Key         KEY(par_ali:Date_Ordered),DUP,NOCASE !By Date Ordered     
RefPartRefNoKey          KEY(par_ali:Ref_Number,par_ali:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PendingRefNoKey          KEY(par_ali:Ref_Number,par_ali:Pending_Ref_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(par_ali:Ref_Number,par_ali:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(par_ali:Supplier),DUP,NOCASE  !By Supplier         
Order_Part_Key           KEY(par_ali:Ref_Number,par_ali:Order_Part_Number),DUP,NOCASE !By Order Part Number
SuppDateOrdKey           KEY(par_ali:Supplier,par_ali:Date_Ordered),DUP,NOCASE !By Date Ordered     
SuppDateRecKey           KEY(par_ali:Supplier,par_ali:Date_Received),DUP,NOCASE !By Date Received    
RequestedKey             KEY(par_ali:Requested,par_ali:Part_Number),DUP,NOCASE !By Part Number      
WebOrderKey              KEY(par_ali:WebOrder,par_ali:Part_Number),DUP,NOCASE !By Part Number      
PartAllocatedKey         KEY(par_ali:PartAllocated,par_ali:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(par_ali:Status,par_ali:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(par_ali:PartAllocated,par_ali:Status,par_ali:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Job Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Stock Reference Number
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !In Warranty Cost    
Sale_Cost                   REAL                           !Out Warranty Cost   
Retail_Cost                 REAL                           !                    
Quantity                    REAL                           !                    
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Reference Number
Order_Number                LONG                           !Order Number        
Order_Part_Number           LONG                           !Order Reference Number
Date_Received               DATE                           !                    
Status_Date                 DATE                           !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Parts Requested For Order
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
WebOrder                    BYTE                           !Web Order           
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
ExchangeUnit                BYTE                           !Exchange Unit       
SecondExchangeUnit          BYTE                           !Second Exchange Unit
Correction                  BYTE                           !Part Correction     
                         END
                     END                       

USERS_ALIAS          FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use_ali),CREATE,BINDABLE,THREAD !                    
User_Code_Key            KEY(use_ali:User_Code),NOCASE,PRIMARY !By User Code        
User_Type_Active_Key     KEY(use_ali:User_Type,use_ali:Active,use_ali:Surname),DUP,NOCASE !By Surname          
Surname_Active_Key       KEY(use_ali:Active,use_ali:Surname),DUP,NOCASE !By Surname          
User_Code_Active_Key     KEY(use_ali:Active,use_ali:User_Code),DUP,NOCASE !                    
User_Type_Key            KEY(use_ali:User_Type,use_ali:Surname),DUP,NOCASE !By Surname          
surname_key              KEY(use_ali:Surname),DUP,NOCASE   !                    
password_key             KEY(use_ali:Password),NOCASE      !                    
Logged_In_Key            KEY(use_ali:Logged_In,use_ali:Surname),DUP,NOCASE !By Name             
Team_Surname             KEY(use_ali:Team,use_ali:Surname),DUP,NOCASE !Surname             
Active_Team_Surname      KEY(use_ali:Team,use_ali:Active,use_ali:Surname),DUP,NOCASE !By Surname          
LocationSurnameKey       KEY(use_ali:Location,use_ali:Surname),DUP,NOCASE !By Surname          
LocationForenameKey      KEY(use_ali:Location,use_ali:Forename),DUP,NOCASE !By Forename         
LocActiveSurnameKey      KEY(use_ali:Active,use_ali:Location,use_ali:Surname),DUP,NOCASE !By Surname          
LocActiveForenameKey     KEY(use_ali:Active,use_ali:Location,use_ali:Forename),DUP,NOCASE !By Forename         
TeamStatusKey            KEY(use_ali:IncludeInEngStatus,use_ali:Team,use_ali:Surname),DUP,NOCASE !By Surname          
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Forename                    STRING(30)                     !                    
Surname                     STRING(30)                     !                    
Password                    STRING(20)                     !                    
User_Type                   STRING(15)                     !                    
Job_Assignment              STRING(15)                     !                    
Supervisor                  STRING(3)                      !                    
User_Level                  STRING(30)                     !                    
Logged_In                   STRING(1)                      !                    
Logged_in_date              DATE                           !                    
Logged_In_Time              TIME                           !                    
Restrict_Logins             STRING(3)                      !                    
Concurrent_Logins           REAL                           !                    
Active                      STRING(3)                      !                    
Team                        STRING(30)                     !                    
Location                    STRING(30)                     !                    
Repair_Target               REAL                           !                    
SkillLevel                  LONG                           !Skill Level         
StockFromLocationOnly       BYTE                           !Pick Stock From Users Location Only
EmailAddress                STRING(255)                    !Email Address       
RenewPassword               LONG                           !Renew Password      
PasswordLastChanged         DATE                           !Password Last Changed
RestrictParts               BYTE                           !Restrict Parts Usage To Skill Level
RestrictChargeable          BYTE                           !Chargeable          
RestrictWarranty            BYTE                           !Warranty            
IncludeInEngStatus          BYTE                           !Include In Engineer Status (True/False)
                         END
                     END                       

MANFPALO_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(mfp_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(mfp_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
Field_Key                KEY(mfp_ali:Manufacturer,mfp_ali:Field_Number,mfp_ali:Field),NOCASE !By Field            
DescriptionKey           KEY(mfp_ali:Manufacturer,mfp_ali:Field_Number,mfp_ali:Description),DUP,NOCASE !By Description.     
AvailableFieldKey        KEY(mfp_ali:NotAvailable,mfp_ali:Manufacturer,mfp_ali:Field_Number,mfp_ali:Field),DUP,NOCASE !By Field            
AvailableDescriptionKey  KEY(mfp_ali:Manufacturer,mfp_ali:NotAvailable,mfp_ali:Field_Number,mfp_ali:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
RestrictLookup              BYTE                           !Restrict Lookup To  
RestrictLookupType          BYTE                           !Restrict Lookup Type
NotAvailable                BYTE                           !Not Available       
ForceJobFaultCode           BYTE                           !Force Job Fault Code
ForceFaultCodeNumber        LONG                           !Job Fault Code Number
SetPartFaultCode            BYTE                           !Set Part Fault Code 
SelectPartFaultCode         LONG                           !Select Part Fault Code
PartFaultCodeValue          STRING(30)                     !Part Fault Code Value
JobTypeAvailability         BYTE                           !Job Type Availability
                         END
                     END                       

MANFAUPA_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(map_ali),CREATE,BINDABLE,THREAD !                    
Field_Number_Key         KEY(map_ali:Manufacturer,map_ali:Field_Number),NOCASE,PRIMARY !By Field Number     
MainFaultKey             KEY(map_ali:Manufacturer,map_ali:MainFault),DUP,NOCASE !By Main Fault       
ScreenOrderKey           KEY(map_ali:Manufacturer,map_ali:ScreenOrder),DUP,NOCASE !By Screen Order     
KeyRepairKey             KEY(map_ali:Manufacturer,map_ali:KeyRepair),DUP,NOCASE !By KeyRepair        
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
ForceFormat                 BYTE                           !Force Format        
FieldFormat                 STRING(30)                     !Field Format        
DateType                    STRING(30)                     !Date Type           
MainFault                   BYTE                           !Main Fault          
UseRelatedJobCode           BYTE                           !Use Related Job Code
NotAvailable                BYTE                           !Not Available       
ScreenOrder                 LONG                           !Screen Order        
CopyFromJobFaultCode        BYTE                           !Copy From Job Fault Code
CopyJobFaultCode            LONG                           !Job Fault Code      
NAForAccessory              BYTE                           !N/A For Accessory   
CompulsoryForAdjustment     BYTE                           !Compulsory For Adjustment
KeyRepair                   BYTE                           !Key Repair          
CCTReferenceFaultCode       BYTE                           !"CCT Reference Fault Code"
NAForSW                     BYTE                           !Fill With "N/A" For Software Upgrade
                         END
                     END                       

MANFAULO_ALIAS       FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(mfo_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(mfo_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
RelatedFieldKey          KEY(mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE !By Field            
Field_Key                KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE !By Field            
DescriptionKey           KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE !By Description      
ManFieldKey              KEY(mfo_ali:Manufacturer,mfo_ali:Field),DUP,NOCASE !By Field            
HideFieldKey             KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE !By Field            
HideDescriptionKey       KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE !By Description      
RelatedDescriptionKey    KEY(mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE !By Description      
HideRelatedFieldKey      KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Field),DUP,NOCASE !By Field            
HideRelatedDescKey       KEY(mfo_ali:NotAvailable,mfo_ali:Manufacturer,mfo_ali:RelatedPartCode,mfo_ali:Field_Number,mfo_ali:Description),DUP,NOCASE !By Description      
FieldNumberKey           KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number),DUP,NOCASE !By Field Number     
PrimaryLookupKey         KEY(mfo_ali:Manufacturer,mfo_ali:Field_Number,mfo_ali:PrimaryLookup),DUP,NOCASE !By Primary Lookup   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
ImportanceLevel             LONG                           !Repair Index        
SkillLevel                  LONG                           !Skill Level         
RepairType                  STRING(30)                     !Repair Type         
RepairTypeWarranty          STRING(30)                     !Repair Type         
HideFromEngineer            BYTE                           !Hide From Engineer  
ForcePartCode               LONG                           !Force Related Part Code
RelatedPartCode             LONG                           !Related Part Fault Code
PromptForExchange           BYTE                           !                    
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table
ReturnToRRC                 BYTE                           !Description         
RestrictLookup              BYTE                           !Restrict Lookup To  
RestrictLookupType          BYTE                           !Restrict Lookup Type
NotAvailable                BYTE                           !Not Available       
PrimaryLookup               BYTE                           !Primary Lookup      
SetJobFaultCode             BYTE                           !Set Job Fault Code  
SelectJobFaultCode          LONG                           !Select Job Fault Code
JobFaultCodeValue           STRING(30)                     !Job Fault Code Value
JobTypeAvailability         BYTE                           !Job Type Availability
                         END
                     END                       

LOCATION_ALIAS       FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(loc_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
Location_Key             KEY(loc_ali:Location),NOCASE      !By Location         
Main_Store_Key           KEY(loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE !By Location         
ActiveLocationKey        KEY(loc_ali:Active,loc_ali:Location),DUP,NOCASE !By Location         
ActiveMainStoreKey       KEY(loc_ali:Active,loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE !By Location         
VirtualLocationKey       KEY(loc_ali:VirtualSite,loc_ali:Location),DUP,NOCASE !By Location         
VirtualMainStoreKey      KEY(loc_ali:VirtualSite,loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE !By Location         
FaultyLocationKey        KEY(loc_ali:FaultyPartsLocation),DUP,NOCASE !By Location         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !                    
Main_Store                  STRING(3)                      !                    
Active                      BYTE                           !Active              
VirtualSite                 BYTE                           !Virtual Site        
Level1                      BYTE                           !Level 1             
Level2                      BYTE                           !Level 2             
Level3                      BYTE                           !Level 3             
InWarrantyMarkUp            REAL                           !In Warranty Markup  
OutWarrantyMarkUp           REAL                           !Out Warranty Markup 
UseRapidStock               BYTE                           !Use Rapid Stock Allocation
FaultyPartsLocation         BYTE                           !                    
                         END
                     END                       

EXCHANGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch_ali),CREATE,BINDABLE,THREAD !                    
Ref_Number_Key           KEY(xch_ali:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
AvailLocIMEI             KEY(xch_ali:Available,xch_ali:Location,xch_ali:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(xch_ali:Available,xch_ali:Location,xch_ali:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(xch_ali:Available,xch_ali:Location,xch_ali:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(xch_ali:Available,xch_ali:Location,xch_ali:Model_Number),DUP,NOCASE !                    
ESN_Only_Key             KEY(xch_ali:ESN),DUP,NOCASE       !                    
MSN_Only_Key             KEY(xch_ali:MSN),DUP,NOCASE       !                    
Ref_Number_Stock_Key     KEY(xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
Model_Available_Key      KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(xch_ali:Stock_Type),DUP,NOCASE !By Stock Type       
ModelRefNoKey            KEY(xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(xch_ali:Available,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(xch_ali:Available,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
AvailRefOnlyKey          KEY(xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
StockBookedKey           KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailBookedKey           KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(xch_ali:Date_Booked),DUP,NOCASE !By Date Booked      
LocStockAvailIMEIKey     KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(xch_ali:Location,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockRefKey           KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocRefKey                KEY(xch_ali:Location,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailModelKey    KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockModelKey         KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocModelKey              KEY(xch_ali:Location,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailMSNKey      KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE !By Exchange Unit Number
LocMSNKey                KEY(xch_ali:Location,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
InTransitLocationKey     KEY(xch_ali:InTransit,xch_ali:Location,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
InTransitKey             KEY(xch_ali:InTransit,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
StatusChangeDateKey      KEY(xch_ali:StatusChangeDate),DUP,NOCASE !                    
LocStatusChangeDateKey   KEY(xch_ali:Location,xch_ali:StatusChangeDate),DUP,NOCASE !                    
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Unit Number         
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !Job Number          
Stock_Type                  STRING(30)                     !                    
Audit_Number                REAL                           !                    
InTransit                   BYTE                           !In Transit Flag     
FreeStockPurchased          STRING(1)                      !                    
StatusChangeDate            DATE                           !                    
                         END
                     END                       

JOBS2_ALIAS          FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job2),BINDABLE,CREATE,THREAD !                    
Ref_Number_Key           KEY(job2:Ref_Number),NOCASE,PRIMARY !By Job Number       
Model_Unit_Key           KEY(job2:Model_Number,job2:Unit_Type),DUP,NOCASE !By Job Number       
EngCompKey               KEY(job2:Engineer,job2:Completed,job2:Ref_Number),DUP,NOCASE !By Job Number       
EngWorkKey               KEY(job2:Engineer,job2:Workshop,job2:Ref_Number),DUP,NOCASE !By Job Number       
Surname_Key              KEY(job2:Surname),DUP,NOCASE      !By Surname          
MobileNumberKey          KEY(job2:Mobile_Number),DUP,NOCASE !By Mobile Number    
ESN_Key                  KEY(job2:ESN),DUP,NOCASE          !By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job2:MSN),DUP,NOCASE          !By M.S.N.           
AccountNumberKey         KEY(job2:Account_Number,job2:Ref_Number),DUP,NOCASE !By Account Number   
AccOrdNoKey              KEY(job2:Account_Number,job2:Order_Number),DUP,NOCASE !By Order Number     
Model_Number_Key         KEY(job2:Model_Number),DUP,NOCASE !By Model Number     
Engineer_Key             KEY(job2:Engineer,job2:Model_Number),DUP,NOCASE !By Engineer         
Date_Booked_Key          KEY(job2:date_booked),DUP,NOCASE  !By Date Booked      
DateCompletedKey         KEY(job2:Date_Completed),DUP,NOCASE !By Date Completed   
ModelCompKey             KEY(job2:Model_Number,job2:Date_Completed),DUP,NOCASE !By Completed Date   
By_Status                KEY(job2:Current_Status,job2:Ref_Number),DUP,NOCASE !By Job Number       
StatusLocKey             KEY(job2:Current_Status,job2:Location,job2:Ref_Number),DUP,NOCASE !By Job Number       
Location_Key             KEY(job2:Location,job2:Ref_Number),DUP,NOCASE !By Location         
Third_Party_Key          KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:Ref_Number),DUP,NOCASE !By Third Party      
ThirdEsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:ESN),DUP,NOCASE !By ESN              
ThirdMsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:MSN),DUP,NOCASE !By MSN              
PriorityTypeKey          KEY(job2:Job_Priority,job2:Ref_Number),DUP,NOCASE !By Priority         
Unit_Type_Key            KEY(job2:Unit_Type),DUP,NOCASE    !By Unit Type        
EDI_Key                  KEY(job2:Manufacturer,job2:EDI,job2:EDI_Batch_Number,job2:Ref_Number),DUP,NOCASE !By Job Number       
InvoiceNumberKey         KEY(job2:Invoice_Number),DUP,NOCASE !By Invoice_Number   
WarInvoiceNoKey          KEY(job2:Invoice_Number_Warranty),DUP,NOCASE !By Invoice Number   
Batch_Number_Key         KEY(job2:Batch_Number,job2:Ref_Number),DUP,NOCASE !By Job Number       
Batch_Status_Key         KEY(job2:Batch_Number,job2:Current_Status,job2:Ref_Number),DUP,NOCASE !By Ref Number       
BatchModelNoKey          KEY(job2:Batch_Number,job2:Model_Number,job2:Ref_Number),DUP,NOCASE !By Job Number       
BatchInvoicedKey         KEY(job2:Batch_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE !By Ref Number       
BatchCompKey             KEY(job2:Batch_Number,job2:Completed,job2:Ref_Number),DUP,NOCASE !By Job Number       
ChaInvoiceKey            KEY(job2:Chargeable_Job,job2:Account_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE !                    
InvoiceExceptKey         KEY(job2:Invoice_Exception,job2:Ref_Number),DUP,NOCASE !By Job Number       
ConsignmentNoKey         KEY(job2:Consignment_Number),DUP,NOCASE !By Cosignment Number
InConsignKey             KEY(job2:Incoming_Consignment_Number),DUP,NOCASE !By Consignment Number
ReadyToDespKey           KEY(job2:Despatched,job2:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToTradeKey          KEY(job2:Despatched,job2:Account_Number,job2:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToCouKey            KEY(job2:Despatched,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToAllKey            KEY(job2:Despatched,job2:Account_Number,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE !By Job Number       
DespJobNumberKey         KEY(job2:Despatch_Number,job2:Ref_Number),DUP,NOCASE !By Job Number       
DateDespatchKey          KEY(job2:Courier,job2:Date_Despatched,job2:Ref_Number),DUP,NOCASE !By Job Number       
DateDespLoaKey           KEY(job2:Loan_Courier,job2:Loan_Despatched,job2:Ref_Number),DUP,NOCASE !By Job Number       
DateDespExcKey           KEY(job2:Exchange_Courier,job2:Exchange_Despatched,job2:Ref_Number),DUP,NOCASE !By Job Number       
ChaRepTypeKey            KEY(job2:Repair_Type),DUP,NOCASE  !By Chargeable Repair Type
WarRepTypeKey            KEY(job2:Repair_Type_Warranty),DUP,NOCASE !By Warranty Repair Type
ChaTypeKey               KEY(job2:Charge_Type),DUP,NOCASE  !                    
WarChaTypeKey            KEY(job2:Warranty_Charge_Type),DUP,NOCASE !                    
Bouncer_Key              KEY(job2:Bouncer,job2:Ref_Number),DUP,NOCASE !By Job Number       
EngDateCompKey           KEY(job2:Engineer,job2:Date_Completed),DUP,NOCASE !By Date Completed   
ExcStatusKey             KEY(job2:Exchange_Status,job2:Ref_Number),DUP,NOCASE !By Job Number       
LoanStatusKey            KEY(job2:Loan_Status,job2:Ref_Number),DUP,NOCASE !By Job Number       
ExchangeLocKey           KEY(job2:Exchange_Status,job2:Location,job2:Ref_Number),DUP,NOCASE !By Job Number       
LoanLocKey               KEY(job2:Loan_Status,job2:Location,job2:Ref_Number),DUP,NOCASE !By Job Number       
BatchJobKey              KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:Ref_Number),DUP,NOCASE !By Job Number       
BatchStatusKey           KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:InvoiceStatus,job2:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Batch_Number                LONG                           !                    
Internal_Status             STRING(10)                     !                    
Auto_Search                 STRING(30)                     !                    
who_booked                  STRING(3)                      !                    
date_booked                 DATE                           !                    
time_booked                 TIME                           !                    
Cancelled                   STRING(3)                      !                    
Bouncer                     STRING(8)                      !                    
Bouncer_Type                STRING(3)                      !Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                      !Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                      !                    
Chargeable_Job              STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(20)                     !                    
MSN                         STRING(20)                     !                    
ProductCode                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location_Type               STRING(10)                     !                    
Phone_Lock                  STRING(30)                     !                    
Workshop                    STRING(3)                      !                    
Location                    STRING(30)                     !                    
Authority_Number            STRING(30)                     !                    
Insurance_Reference_Number  STRING(30)                     !                    
DOP                         DATE                           !                    
Insurance                   STRING(3)                      !                    
Insurance_Type              STRING(30)                     !                    
Transit_Type                STRING(30)                     !                    
Physical_Damage             STRING(3)                      !                    
Intermittent_Fault          STRING(3)                      !                    
Loan_Status                 STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Job_Priority                STRING(30)                     !                    
Charge_Type                 STRING(30)                     !                    
Warranty_Charge_Type        STRING(30)                     !                    
Current_Status              STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Trade_Account_Name          STRING(30)                     !                    
Department_Name             STRING(30)                     !                    
Order_Number                STRING(30)                     !                    
POP                         STRING(3)                      !                    
In_Repair                   STRING(3)                      !                    
Date_In_Repair              DATE                           !                    
Time_In_Repair              TIME                           !                    
On_Test                     STRING(3)                      !                    
Date_On_Test                DATE                           !                    
Time_On_Test                TIME                           !                    
Estimate_Ready              STRING(3)                      !                    
QA_Passed                   STRING(3)                      !                    
Date_QA_Passed              DATE                           !                    
Time_QA_Passed              TIME                           !                    
QA_Rejected                 STRING(3)                      !                    
Date_QA_Rejected            DATE                           !                    
Time_QA_Rejected            TIME                           !                    
QA_Second_Passed            STRING(3)                      !                    
Date_QA_Second_Passed       DATE                           !                    
Time_QA_Second_Passed       TIME                           !                    
Title                       STRING(4)                      !                    
Initial                     STRING(1)                      !                    
Surname                     STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Mobile_Number               STRING(15)                     !                    
Postcode_Collection         STRING(10)                     !                    
Company_Name_Collection     STRING(30)                     !                    
Address_Line1_Collection    STRING(30)                     !                    
Address_Line2_Collection    STRING(30)                     !                    
Address_Line3_Collection    STRING(30)                     !                    
Telephone_Collection        STRING(15)                     !                    
Postcode_Delivery           STRING(10)                     !                    
Address_Line1_Delivery      STRING(30)                     !                    
Company_Name_Delivery       STRING(30)                     !                    
Address_Line2_Delivery      STRING(30)                     !                    
Address_Line3_Delivery      STRING(30)                     !                    
Telephone_Delivery          STRING(15)                     !                    
Date_Completed              DATE                           !                    
Time_Completed              TIME                           !                    
Completed                   STRING(3)                      !                    
Paid                        STRING(3)                      !                    
Paid_Warranty               STRING(3)                      !                    
Date_Paid                   DATE                           !                    
Repair_Type                 STRING(30)                     !                    
Repair_Type_Warranty        STRING(30)                     !                    
Engineer                    STRING(3)                      !                    
Ignore_Chargeable_Charges   STRING(3)                      !                    
Ignore_Warranty_Charges     STRING(3)                      !                    
Ignore_Estimate_Charges     STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Advance_Payment             REAL                           !                    
Labour_Cost                 REAL                           !                    
Parts_Cost                  REAL                           !                    
Sub_Total                   REAL                           !                    
Courier_Cost_Estimate       REAL                           !                    
Labour_Cost_Estimate        REAL                           !                    
Parts_Cost_Estimate         REAL                           !                    
Sub_Total_Estimate          REAL                           !                    
Courier_Cost_Warranty       REAL                           !                    
Labour_Cost_Warranty        REAL                           !                    
Parts_Cost_Warranty         REAL                           !                    
Sub_Total_Warranty          REAL                           !                    
Loan_Issued_Date            DATE                           !                    
Loan_Unit_Number            LONG                           !                    
Loan_accessory              STRING(3)                      !                    
Loan_User                   STRING(3)                      !                    
Loan_Courier                STRING(30)                     !                    
Loan_Consignment_Number     STRING(30)                     !                    
Loan_Despatched             DATE                           !                    
Loan_Despatched_User        STRING(3)                      !                    
Loan_Despatch_Number        LONG                           !                    
LoaService                  STRING(1)                      !Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                           !                    
Exchange_Authorised         STRING(3)                      !                    
Loan_Authorised             STRING(3)                      !                    
Exchange_Accessory          STRING(3)                      !                    
Exchange_Issued_Date        DATE                           !                    
Exchange_User               STRING(3)                      !                    
Exchange_Courier            STRING(30)                     !                    
Exchange_Consignment_Number STRING(30)                     !                    
Exchange_Despatched         DATE                           !                    
Exchange_Despatched_User    STRING(3)                      !                    
Exchange_Despatch_Number    LONG                           !                    
ExcService                  STRING(1)                      !Exchange Service For City Link / Label G
Date_Despatched             DATE                           !                    
Despatch_Number             LONG                           !                    
Despatch_User               STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Consignment_Number          STRING(30)                     !                    
Incoming_Courier            STRING(30)                     !                    
Incoming_Consignment_Number STRING(30)                     !                    
Incoming_Date               DATE                           !                    
Despatched                  STRING(3)                      !                    
Despatch_Type               STRING(3)                      !                    
Current_Courier             STRING(30)                     !                    
JobService                  STRING(1)                      !Job Service For City Link / Label G
Last_Repair_Days            REAL                           !                    
Third_Party_Site            STRING(30)                     !                    
Estimate                    STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Estimate_Accepted           STRING(3)                      !                    
Estimate_Rejected           STRING(3)                      !                    
Third_Party_Printed         STRING(3)                      !                    
ThirdPartyDateDesp          DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(255)                    !                    
Fault_Code11                STRING(255)                    !                    
Fault_Code12                STRING(255)                    !                    
PreviousStatus              STRING(30)                     !                    
StatusUser                  STRING(3)                      !User who last changed the status
Status_End_Date             DATE                           !                    
Status_End_Time             TIME                           !                    
Turnaround_End_Date         DATE                           !                    
Turnaround_End_Time         TIME                           !                    
Turnaround_Time             STRING(30)                     !                    
Special_Instructions        STRING(30)                     !                    
InvoiceAccount              STRING(15)                     !Invoice Batch Account Number
InvoiceStatus               STRING(3)                      !Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                           !Proforma Batch Number
InvoiceQuery                STRING(255)                    !Query Reason        
EDI                         STRING(3)                      !                    
EDI_Batch_Number            REAL                           !                    
Invoice_Exception           STRING(3)                      !                    
Invoice_Failure_Reason      STRING(80)                     !                    
Invoice_Number              LONG                           !                    
Invoice_Date                DATE                           !                    
Invoice_Date_Warranty       DATE                           !                    
Invoice_Courier_Cost        REAL                           !                    
Invoice_Labour_Cost         REAL                           !                    
Invoice_Parts_Cost          REAL                           !                    
Invoice_Sub_Total           REAL                           !                    
Invoice_Number_Warranty     LONG                           !                    
WInvoice_Courier_Cost       REAL                           !                    
WInvoice_Labour_Cost        REAL                           !                    
WInvoice_Parts_Cost         REAL                           !                    
WInvoice_Sub_Total          REAL                           !                    
                         END
                     END                       

JOBS_ALIAS           FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job_ali),BINDABLE,CREATE,THREAD !                    
Ref_Number_Key           KEY(job_ali:Ref_Number),NOCASE,PRIMARY !By Job Number       
Model_Unit_Key           KEY(job_ali:Model_Number,job_ali:Unit_Type),DUP,NOCASE !By Job Number       
EngCompKey               KEY(job_ali:Engineer,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
EngWorkKey               KEY(job_ali:Engineer,job_ali:Workshop,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Surname_Key              KEY(job_ali:Surname),DUP,NOCASE   !By Surname          
MobileNumberKey          KEY(job_ali:Mobile_Number),DUP,NOCASE !By Mobile Number    
ESN_Key                  KEY(job_ali:ESN),DUP,NOCASE       !By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job_ali:MSN),DUP,NOCASE       !By M.S.N.           
AccountNumberKey         KEY(job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE !By Account Number   
AccOrdNoKey              KEY(job_ali:Account_Number,job_ali:Order_Number),DUP,NOCASE !By Order Number     
Model_Number_Key         KEY(job_ali:Model_Number),DUP,NOCASE !By Model Number     
Engineer_Key             KEY(job_ali:Engineer,job_ali:Model_Number),DUP,NOCASE !By Engineer         
Date_Booked_Key          KEY(job_ali:date_booked),DUP,NOCASE !By Date Booked      
DateCompletedKey         KEY(job_ali:Date_Completed),DUP,NOCASE !By Date Completed   
ModelCompKey             KEY(job_ali:Model_Number,job_ali:Date_Completed),DUP,NOCASE !By Completed Date   
By_Status                KEY(job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
StatusLocKey             KEY(job_ali:Current_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Location_Key             KEY(job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Location         
Third_Party_Key          KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:Ref_Number),DUP,NOCASE !By Third Party      
ThirdEsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:ESN),DUP,NOCASE !By ESN              
ThirdMsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:MSN),DUP,NOCASE !By MSN              
PriorityTypeKey          KEY(job_ali:Job_Priority,job_ali:Ref_Number),DUP,NOCASE !By Priority         
Unit_Type_Key            KEY(job_ali:Unit_Type),DUP,NOCASE !By Unit Type        
EDI_Key                  KEY(job_ali:Manufacturer,job_ali:EDI,job_ali:EDI_Batch_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
InvoiceNumberKey         KEY(job_ali:Invoice_Number),DUP,NOCASE !By Invoice_Number   
WarInvoiceNoKey          KEY(job_ali:Invoice_Number_Warranty),DUP,NOCASE !By Invoice Number   
Batch_Number_Key         KEY(job_ali:Batch_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Batch_Status_Key         KEY(job_ali:Batch_Number,job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE !By Ref Number       
BatchModelNoKey          KEY(job_ali:Batch_Number,job_ali:Model_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
BatchInvoicedKey         KEY(job_ali:Batch_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE !By Ref Number       
BatchCompKey             KEY(job_ali:Batch_Number,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ChaInvoiceKey            KEY(job_ali:Chargeable_Job,job_ali:Account_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE !                    
InvoiceExceptKey         KEY(job_ali:Invoice_Exception,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ConsignmentNoKey         KEY(job_ali:Consignment_Number),DUP,NOCASE !By Cosignment Number
InConsignKey             KEY(job_ali:Incoming_Consignment_Number),DUP,NOCASE !By Consignment Number
ReadyToDespKey           KEY(job_ali:Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToTradeKey          KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToCouKey            KEY(job_ali:Despatched,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToAllKey            KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DespJobNumberKey         KEY(job_ali:Despatch_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DateDespatchKey          KEY(job_ali:Courier,job_ali:Date_Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DateDespLoaKey           KEY(job_ali:Loan_Courier,job_ali:Loan_Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DateDespExcKey           KEY(job_ali:Exchange_Courier,job_ali:Exchange_Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ChaRepTypeKey            KEY(job_ali:Repair_Type),DUP,NOCASE !By Chargeable Repair Type
WarRepTypeKey            KEY(job_ali:Repair_Type_Warranty),DUP,NOCASE !By Warranty Repair Type
ChaTypeKey               KEY(job_ali:Charge_Type),DUP,NOCASE !                    
WarChaTypeKey            KEY(job_ali:Warranty_Charge_Type),DUP,NOCASE !                    
Bouncer_Key              KEY(job_ali:Bouncer,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
EngDateCompKey           KEY(job_ali:Engineer,job_ali:Date_Completed),DUP,NOCASE !By Date Completed   
ExcStatusKey             KEY(job_ali:Exchange_Status,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
LoanStatusKey            KEY(job_ali:Loan_Status,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ExchangeLocKey           KEY(job_ali:Exchange_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
LoanLocKey               KEY(job_ali:Loan_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
BatchJobKey              KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
BatchStatusKey           KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:InvoiceStatus,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Batch_Number                LONG                           !                    
Internal_Status             STRING(10)                     !                    
Auto_Search                 STRING(30)                     !                    
who_booked                  STRING(3)                      !                    
date_booked                 DATE                           !                    
time_booked                 TIME                           !                    
Cancelled                   STRING(3)                      !                    
Bouncer                     STRING(8)                      !                    
Bouncer_Type                STRING(3)                      !Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                      !Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                      !                    
Chargeable_Job              STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(20)                     !                    
MSN                         STRING(20)                     !                    
ProductCode                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location_Type               STRING(10)                     !                    
Phone_Lock                  STRING(30)                     !                    
Workshop                    STRING(3)                      !                    
Location                    STRING(30)                     !                    
Authority_Number            STRING(30)                     !                    
Insurance_Reference_Number  STRING(30)                     !                    
DOP                         DATE                           !                    
Insurance                   STRING(3)                      !                    
Insurance_Type              STRING(30)                     !                    
Transit_Type                STRING(30)                     !                    
Physical_Damage             STRING(3)                      !                    
Intermittent_Fault          STRING(3)                      !                    
Loan_Status                 STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Job_Priority                STRING(30)                     !                    
Charge_Type                 STRING(30)                     !                    
Warranty_Charge_Type        STRING(30)                     !                    
Current_Status              STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Trade_Account_Name          STRING(30)                     !                    
Department_Name             STRING(30)                     !                    
Order_Number                STRING(30)                     !                    
POP                         STRING(3)                      !                    
In_Repair                   STRING(3)                      !                    
Date_In_Repair              DATE                           !                    
Time_In_Repair              TIME                           !                    
On_Test                     STRING(3)                      !                    
Date_On_Test                DATE                           !                    
Time_On_Test                TIME                           !                    
Estimate_Ready              STRING(3)                      !                    
QA_Passed                   STRING(3)                      !                    
Date_QA_Passed              DATE                           !                    
Time_QA_Passed              TIME                           !                    
QA_Rejected                 STRING(3)                      !                    
Date_QA_Rejected            DATE                           !                    
Time_QA_Rejected            TIME                           !                    
QA_Second_Passed            STRING(3)                      !                    
Date_QA_Second_Passed       DATE                           !                    
Time_QA_Second_Passed       TIME                           !                    
Title                       STRING(4)                      !                    
Initial                     STRING(1)                      !                    
Surname                     STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Mobile_Number               STRING(15)                     !                    
Postcode_Collection         STRING(10)                     !                    
Company_Name_Collection     STRING(30)                     !                    
Address_Line1_Collection    STRING(30)                     !                    
Address_Line2_Collection    STRING(30)                     !                    
Address_Line3_Collection    STRING(30)                     !                    
Telephone_Collection        STRING(15)                     !                    
Postcode_Delivery           STRING(10)                     !                    
Address_Line1_Delivery      STRING(30)                     !                    
Company_Name_Delivery       STRING(30)                     !                    
Address_Line2_Delivery      STRING(30)                     !                    
Address_Line3_Delivery      STRING(30)                     !                    
Telephone_Delivery          STRING(15)                     !                    
Date_Completed              DATE                           !                    
Time_Completed              TIME                           !                    
Completed                   STRING(3)                      !                    
Paid                        STRING(3)                      !                    
Paid_Warranty               STRING(3)                      !                    
Date_Paid                   DATE                           !                    
Repair_Type                 STRING(30)                     !                    
Repair_Type_Warranty        STRING(30)                     !                    
Engineer                    STRING(3)                      !                    
Ignore_Chargeable_Charges   STRING(3)                      !                    
Ignore_Warranty_Charges     STRING(3)                      !                    
Ignore_Estimate_Charges     STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Advance_Payment             REAL                           !                    
Labour_Cost                 REAL                           !                    
Parts_Cost                  REAL                           !                    
Sub_Total                   REAL                           !                    
Courier_Cost_Estimate       REAL                           !                    
Labour_Cost_Estimate        REAL                           !                    
Parts_Cost_Estimate         REAL                           !                    
Sub_Total_Estimate          REAL                           !                    
Courier_Cost_Warranty       REAL                           !                    
Labour_Cost_Warranty        REAL                           !                    
Parts_Cost_Warranty         REAL                           !                    
Sub_Total_Warranty          REAL                           !                    
Loan_Issued_Date            DATE                           !                    
Loan_Unit_Number            LONG                           !                    
Loan_accessory              STRING(3)                      !                    
Loan_User                   STRING(3)                      !                    
Loan_Courier                STRING(30)                     !                    
Loan_Consignment_Number     STRING(30)                     !                    
Loan_Despatched             DATE                           !                    
Loan_Despatched_User        STRING(3)                      !                    
Loan_Despatch_Number        LONG                           !                    
LoaService                  STRING(1)                      !Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                           !                    
Exchange_Authorised         STRING(3)                      !                    
Loan_Authorised             STRING(3)                      !                    
Exchange_Accessory          STRING(3)                      !                    
Exchange_Issued_Date        DATE                           !                    
Exchange_User               STRING(3)                      !                    
Exchange_Courier            STRING(30)                     !                    
Exchange_Consignment_Number STRING(30)                     !                    
Exchange_Despatched         DATE                           !                    
Exchange_Despatched_User    STRING(3)                      !                    
Exchange_Despatch_Number    LONG                           !                    
ExcService                  STRING(1)                      !Exchange Service For City Link / Label G
Date_Despatched             DATE                           !                    
Despatch_Number             LONG                           !                    
Despatch_User               STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Consignment_Number          STRING(30)                     !                    
Incoming_Courier            STRING(30)                     !                    
Incoming_Consignment_Number STRING(30)                     !                    
Incoming_Date               DATE                           !                    
Despatched                  STRING(3)                      !                    
Despatch_Type               STRING(3)                      !                    
Current_Courier             STRING(30)                     !                    
JobService                  STRING(1)                      !Job Service For City Link / Label G
Last_Repair_Days            REAL                           !                    
Third_Party_Site            STRING(30)                     !                    
Estimate                    STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Estimate_Accepted           STRING(3)                      !                    
Estimate_Rejected           STRING(3)                      !                    
Third_Party_Printed         STRING(3)                      !                    
ThirdPartyDateDesp          DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(255)                    !                    
Fault_Code11                STRING(255)                    !                    
Fault_Code12                STRING(255)                    !                    
PreviousStatus              STRING(30)                     !                    
StatusUser                  STRING(3)                      !User who last changed the status
Status_End_Date             DATE                           !                    
Status_End_Time             TIME                           !                    
Turnaround_End_Date         DATE                           !                    
Turnaround_End_Time         TIME                           !                    
Turnaround_Time             STRING(30)                     !                    
Special_Instructions        STRING(30)                     !                    
InvoiceAccount              STRING(15)                     !Invoice Batch Account Number
InvoiceStatus               STRING(3)                      !Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                           !Proforma Batch Number
InvoiceQuery                STRING(255)                    !Query Reason        
EDI                         STRING(3)                      !                    
EDI_Batch_Number            REAL                           !                    
Invoice_Exception           STRING(3)                      !                    
Invoice_Failure_Reason      STRING(80)                     !                    
Invoice_Number              LONG                           !                    
Invoice_Date                DATE                           !                    
Invoice_Date_Warranty       DATE                           !                    
Invoice_Courier_Cost        REAL                           !                    
Invoice_Labour_Cost         REAL                           !                    
Invoice_Parts_Cost          REAL                           !                    
Invoice_Sub_Total           REAL                           !                    
Invoice_Number_Warranty     LONG                           !                    
WInvoice_Courier_Cost       REAL                           !                    
WInvoice_Labour_Cost        REAL                           !                    
WInvoice_Parts_Cost         REAL                           !                    
WInvoice_Sub_Total          REAL                           !                    
                         END
                     END                       

TRADEACC_ALIAS       FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(tra_ali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(tra_ali:RecordNumber),NOCASE,PRIMARY !By Record Number    
Account_Number_Key       KEY(tra_ali:Account_Number),NOCASE !By Account Number   
Company_Name_Key         KEY(tra_ali:Company_Name),DUP,NOCASE !By Company Name     
ReplicateFromKey         KEY(tra_ali:ReplicateAccount,tra_ali:Account_Number),DUP,NOCASE !By Account Number   
StoresAccountKey         KEY(tra_ali:StoresAccount),DUP,NOCASE !By Stores Account   
SiteLocationKey          KEY(tra_ali:SiteLocation),DUP,NOCASE !By Site Location    
RegionKey                KEY(tra_ali:Region),DUP,NOCASE    !By Region           
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Account_Number              STRING(15)                     !                    
Postcode                    STRING(15)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Contact_Name                STRING(30)                     !                    
Labour_Discount_Code        STRING(2)                      !                    
Retail_Discount_Code        STRING(2)                      !                    
Parts_Discount_Code         STRING(2)                      !                    
Labour_VAT_Code             STRING(2)                      !                    
Parts_VAT_Code              STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Account_Type                STRING(6)                      !                    
Credit_Limit                REAL                           !                    
Account_Balance             REAL                           !                    
Use_Sub_Accounts            STRING(3)                      !                    
Invoice_Sub_Accounts        STRING(3)                      !                    
Stop_Account                STRING(3)                      !                    
Allow_Cash_Sales            STRING(3)                      !                    
Price_Despatch              STRING(3)                      !                    
Price_First_Copy_Only       BYTE                           !Display pricing details on first copy of despatch note
Num_Despatch_Note_Copies    LONG                           !Number of despatch notes to print
Price_Retail_Despatch       STRING(3)                      !                    
Print_Despatch_Complete     STRING(3)                      !Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                      !Print Despatch Note At Despatch
Standard_Repair_Type        STRING(15)                     !                    
Courier_Incoming            STRING(30)                     !                    
Courier_Outgoing            STRING(30)                     !                    
Use_Contact_Name            STRING(3)                      !                    
VAT_Number                  STRING(30)                     !                    
Use_Delivery_Address        STRING(3)                      !                    
Use_Collection_Address      STRING(3)                      !                    
UseCustDespAdd              STRING(3)                      !Use Customer Address As Despatch Address
Allow_Loan                  STRING(3)                      !                    
Allow_Exchange              STRING(3)                      !                    
Force_Fault_Codes           STRING(3)                      !                    
Despatch_Invoiced_Jobs      STRING(3)                      !                    
Despatch_Paid_Jobs          STRING(3)                      !                    
Print_Despatch_Notes        STRING(3)                      !                    
Print_Retail_Despatch_Note  STRING(3)                      !                    
Print_Retail_Picking_Note   STRING(3)                      !                    
Despatch_Note_Per_Item      STRING(3)                      !                    
Skip_Despatch               STRING(3)                      !Use Batch Despatch  
IgnoreDespatch              STRING(3)                      !Do not use Despatch Table
Summary_Despatch_Notes      STRING(3)                      !                    
Exchange_Stock_Type         STRING(30)                     !                    
Loan_Stock_Type             STRING(30)                     !                    
Courier_Cost                REAL                           !                    
Force_Estimate              STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Turnaround_Time             STRING(30)                     !                    
Password                    STRING(20)                     !                    
Retail_Payment_Type         STRING(3)                      !                    
Retail_Price_Structure      STRING(3)                      !                    
Use_Customer_Address        STRING(3)                      !                    
Invoice_Customer_Address    STRING(3)                      !                    
ZeroChargeable              STRING(3)                      !Don't show Chargeable Costs
RefurbCharge                STRING(3)                      !                    
ChargeType                  STRING(30)                     !                    
WarChargeType               STRING(30)                     !                    
ExchangeAcc                 STRING(3)                      !Account used for repairing exchange units
MultiInvoice                STRING(3)                      !Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                      !Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)                    !For ProForma Export 
ImportPath                  STRING(255)                    !For ProForma Export 
BatchNumber                 LONG                           !ProForma Batch Number
ExcludeBouncer              BYTE                           !Exclude From Bouncer
RetailZeroParts             BYTE                           !Zero Value Parts    
HideDespAdd                 BYTE                           !Hide Address On Despatch Note
EuroApplies                 BYTE                           !Apply Euro          
ForceCommonFault            BYTE                           !Force Common Fault  
ForceOrderNumber            BYTE                           !Force Order Number  
ShowRepairTypes             BYTE                           !Repair Type Display 
WebMemo                     STRING(255)                    !Web Memo            
WebPassword1                STRING(30)                     !Web Password1       
WebPassword2                STRING(30)                     !WebPassword2        
InvoiceAtDespatch           BYTE                           !Print Invoice At Despatch
InvoiceType                 BYTE                           !Invoice Type        
IndividualSummary           BYTE                           !Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                           !Use Trade Contact No
StopThirdParty              BYTE                           !Stop Unit For Going To Third Party
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
UseDespatchDetails          BYTE                           !Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                     !Telephone Number    
AltFaxNumber                STRING(30)                     !Fax Number          
AltEmailAddress             STRING(255)                    !Email Address       
AutoSendStatusEmails        BYTE                           !Auto Send Status Emails
EmailRecipientList          BYTE                           !Email Recipient List
EmailEndUser                BYTE                           !Email End User      
ForceEndUserName            BYTE                           !Force End User Name 
ChangeInvAddress            BYTE                           !Invoice Address     
ChangeCollAddress           BYTE                           !Collection Address  
ChangeDelAddress            BYTE                           !Delivery Address    
AllowMaximumDiscount        BYTE                           !Set Maximum Discount Level
MaximumDiscount             REAL                           !Maximum Discount    
SetInvoicedJobStatus        BYTE                           !Set Completed Status
SetDespatchJobStatus        BYTE                           !Set Completed Status
InvoicedJobStatus           STRING(30)                     !Completed Status    
DespatchedJobStatus         STRING(30)                     !Completed Status    
CCommissionInHouse          LONG                           !Chargeable          
WCommissionInHouse          LONG                           !Warranty            
CCommissionOutSource        LONG                           !Chargeable          
WCommissionOutSource        LONG                           !Warranty            
ReplicateAccount            STRING(30)                     !Account Replicated From
RemoteRepairCentre          BYTE                           !Remote Repair Centre
SiteLocation                STRING(30)                     !Site Location       
RRCFactor                   LONG                           !R.R.C. Chargeable (%)
ARCFactor                   LONG                           !A.R.C. Chargeable (%)
TransitType                 STRING(30)                     !Transit Type        
ForceMobileNumber           BYTE                           !Force Mobile Number 
BranchIdentification        STRING(2)                      !                    
AllocateQAEng               BYTE                           !                    
InvoiceAtCompletion         BYTE                           !Invoice At Completion
InvoiceTypeComplete         BYTE                           !Invoice Type        
UseTimingsFrom              STRING(30)                     !Use Timings From    
StartWorkHours              TIME                           !Start Work Hours    
EndWorkHours                TIME                           !End Work Hours      
IncludeSaturday             STRING(3)                      !Include Saturday    
IncludeSunday               STRING(3)                      !Include Sunday      
StoresAccount               STRING(30)                     !Stores Account      
RepairEngineerQA            BYTE                           !Repair Engineer To QA Job
SecondYearAccount           BYTE                           !Second Year Warranty Account (True/False)
Activate48Hour              BYTE                           !Activate 48 Hour Exchange Process
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
Line500AccountNumber        STRING(30)                     !Line 500 Account Number
CompanyOwned                BYTE                           !Company Ownder Franchise
OverrideMobileDefault       BYTE                           !Override Mandatory Mobile Number
Region                      STRING(30)                     !Region              
AllowCreditNotes            BYTE                           !Allow Credit Notes  
DoMSISDNCheck               BYTE                           !Do MSISDN Check     
Hub                         STRING(30)                     !Hub                 
UseSBOnline                 BYTE                           !Use SB Online       
IgnoreReplenishmentProcess  BYTE                           !Ignore Replenishment Process
VCPWaybillPrefix            STRING(30)                     !VCP Waybill Prefix  
RRCWaybillPrefix            STRING(30)                     !RRC Waybill Prefix  
OBFCompanyName              STRING(30)                     !                    
OBFAddress1                 STRING(30)                     !                    
OBFAddress2                 STRING(30)                     !                    
OBFSuburb                   STRING(30)                     !                    
OBFContactName              STRING(30)                     !                    
OBFContactNumber            STRING(15)                     !                    
OBFEmailAddress             STRING(255)                    !                    
SBOnlineJobProgress         BYTE                           !Job Progress        
coTradingName               STRING(40)                     !                    
coTradingName2              STRING(40)                     !                    
coLocation                  STRING(40)                     !                    
coRegistrationNo            STRING(30)                     !                    
coVATNumber                 STRING(30)                     !                    
coAddressLine1              STRING(40)                     !                    
coAddressLine2              STRING(40)                     !                    
coAddressLine3              STRING(40)                     !                    
coAddressLine4              STRING(40)                     !                    
coTelephoneNumber           STRING(30)                     !                    
coFaxNumber                 STRING(30)                     !                    
coEmailAddress              STRING(255)                    !                    
RtnExchangeAccount          STRING(30)                     !                    
                         END
                     END                       

!endregion

         include('MessageBox.inc'),once

ThisMessageBox       class(csEnhancedMessageClass) 
Init                     procedure  (long UseABCClasses=0,long UseDefaultFile=0)  ,virtual
PrimeLog                 procedure  (<string ExtraDetails>)  ,virtual
                      end
StartSearchEXEName          long(1)
EndSearchEXEName            long(0)
glo:TempFaultCodes  String(255),STATIC
glo:TempAuditQueue      String(255),STATIC

TempFaultCodes    File,Driver('TOPSPEED'),Pre(tmpfau),Name(glo:TempFaultCodes),Create,Bindable,Thread
keySessionID         Key(tmpfau:SessionID),NOCASE,DUP
Record                  Record
SessionID                  Long()
RecordNumber               Long()
FaultType                  String(1)
                        End
                    End

TempAuditQueue    File,Driver('TOPSPEED'),Pre(tmpaud),Name(glo:TempAuditQueue),Create,Bindable
keySessionID         Key(tmpaud:SessionID),NOCASE,DUP
keyField             Key(tmpaud:SessionID,tmpaud:Field),NOCASE
Record                  Record
SessionID                  Long()
Field                      String(255)
Reason                     String(255)
SaveCost                   Byte(0)
Cost                       Real
                        End
                    End
INCLUDE('VodacomDeclarations.inc')
! ===============================================================
! XmlExport Class
XmlExport   class,type
ExportFile     &file,protected
Rec            &group,protected
indent         long,protected
FirstTimeWrite bool,protected
FInit          procedure(FILE aFile)
FOpen          procedure(string aFilename, byte aIncludeXmlHeader=true),long
FClose         procedure()
FWrite         procedure(string aStr),protected
OpenTag        procedure(string aTag, <string aAttributeString>)
CloseTag       procedure(string aTag)
WriteTag       procedure(string aTag, string aStr, <string aAttributeString>)
            end
! ===============================================================
vod                 VodacomClass

Access:STDCHRGE      &FileManager,THREAD                   ! FileManager for STDCHRGE
Relate:STDCHRGE      &RelationManager,THREAD               ! RelationManager for STDCHRGE
Access:TagFile       &FileManager,THREAD                   ! FileManager for TagFile
Relate:TagFile       &RelationManager,THREAD               ! RelationManager for TagFile
Access:STOFAULT      &FileManager,THREAD                   ! FileManager for STOFAULT
Relate:STOFAULT      &RelationManager,THREAD               ! RelationManager for STOFAULT
Access:JOBOUTFL      &FileManager,THREAD                   ! FileManager for JOBOUTFL
Relate:JOBOUTFL      &RelationManager,THREAD               ! RelationManager for JOBOUTFL
Access:ACCSTAT       &FileManager,THREAD                   ! FileManager for ACCSTAT
Relate:ACCSTAT       &RelationManager,THREAD               ! RelationManager for ACCSTAT
Access:EXCHAMF       &FileManager,THREAD                   ! FileManager for EXCHAMF
Relate:EXCHAMF       &RelationManager,THREAD               ! RelationManager for EXCHAMF
Access:AUDITE        &FileManager,THREAD                   ! FileManager for AUDITE
Relate:AUDITE        &RelationManager,THREAD               ! RelationManager for AUDITE
Access:EXCHAUI       &FileManager,THREAD                   ! FileManager for EXCHAUI
Relate:EXCHAUI       &RelationManager,THREAD               ! RelationManager for EXCHAUI
Access:JOBSENG       &FileManager,THREAD                   ! FileManager for JOBSENG
Relate:JOBSENG       &RelationManager,THREAD               ! RelationManager for JOBSENG
Access:MULDESPJ      &FileManager,THREAD                   ! FileManager for MULDESPJ
Relate:MULDESPJ      &RelationManager,THREAD               ! RelationManager for MULDESPJ
Access:MULDESP       &FileManager,THREAD                   ! FileManager for MULDESP
Relate:MULDESP       &RelationManager,THREAD               ! RelationManager for MULDESP
Access:STMASAUD      &FileManager,THREAD                   ! FileManager for STMASAUD
Relate:STMASAUD      &RelationManager,THREAD               ! RelationManager for STMASAUD
Access:AUDSTATS      &FileManager,THREAD                   ! FileManager for AUDSTATS
Relate:AUDSTATS      &RelationManager,THREAD               ! RelationManager for AUDSTATS
Access:NETWORKS      &FileManager,THREAD                   ! FileManager for NETWORKS
Relate:NETWORKS      &RelationManager,THREAD               ! RelationManager for NETWORKS
Access:JOBTHIRD      &FileManager,THREAD                   ! FileManager for JOBTHIRD
Relate:JOBTHIRD      &RelationManager,THREAD               ! RelationManager for JOBTHIRD
Access:LOCATLOG      &FileManager,THREAD                   ! FileManager for LOCATLOG
Relate:LOCATLOG      &RelationManager,THREAD               ! RelationManager for LOCATLOG
Access:PRODCODE      &FileManager,THREAD                   ! FileManager for PRODCODE
Relate:PRODCODE      &RelationManager,THREAD               ! RelationManager for PRODCODE
Access:ESNMODAL      &FileManager,THREAD                   ! FileManager for ESNMODAL
Relate:ESNMODAL      &RelationManager,THREAD               ! RelationManager for ESNMODAL
Access:STOAUDIT      &FileManager,THREAD                   ! FileManager for STOAUDIT
Relate:STOAUDIT      &RelationManager,THREAD               ! RelationManager for STOAUDIT
Access:DEFAULT2      &FileManager,THREAD                   ! FileManager for DEFAULT2
Relate:DEFAULT2      &RelationManager,THREAD               ! RelationManager for DEFAULT2
Access:ACTION        &FileManager,THREAD                   ! FileManager for ACTION
Relate:ACTION        &RelationManager,THREAD               ! RelationManager for ACTION
Access:RETSALES      &FileManager,THREAD                   ! FileManager for RETSALES
Relate:RETSALES      &RelationManager,THREAD               ! RelationManager for RETSALES
Access:LOANAMF       &FileManager,THREAD                   ! FileManager for LOANAMF
Relate:LOANAMF       &RelationManager,THREAD               ! RelationManager for LOANAMF
Access:BOUNCER       &FileManager,THREAD                   ! FileManager for BOUNCER
Relate:BOUNCER       &RelationManager,THREAD               ! RelationManager for BOUNCER
Access:EXCHOR48      &FileManager,THREAD                   ! FileManager for EXCHOR48
Relate:EXCHOR48      &RelationManager,THREAD               ! RelationManager for EXCHOR48
Access:LOANAUI       &FileManager,THREAD                   ! FileManager for LOANAUI
Relate:LOANAUI       &RelationManager,THREAD               ! RelationManager for LOANAUI
Access:WIPAMF        &FileManager,THREAD                   ! FileManager for WIPAMF
Relate:WIPAMF        &RelationManager,THREAD               ! RelationManager for WIPAMF
Access:TEAMS         &FileManager,THREAD                   ! FileManager for TEAMS
Relate:TEAMS         &RelationManager,THREAD               ! RelationManager for TEAMS
Access:WIPAUI        &FileManager,THREAD                   ! FileManager for WIPAUI
Relate:WIPAUI        &RelationManager,THREAD               ! RelationManager for WIPAUI
Access:EXCAUDIT      &FileManager,THREAD                   ! FileManager for EXCAUDIT
Relate:EXCAUDIT      &RelationManager,THREAD               ! RelationManager for EXCAUDIT
Access:JOBEXACC      &FileManager,THREAD                   ! FileManager for JOBEXACC
Relate:JOBEXACC      &RelationManager,THREAD               ! RelationManager for JOBEXACC
Access:MODELCOL      &FileManager,THREAD                   ! FileManager for MODELCOL
Relate:MODELCOL      &RelationManager,THREAD               ! RelationManager for MODELCOL
Access:PAYTYPES      &FileManager,THREAD                   ! FileManager for PAYTYPES
Relate:PAYTYPES      &RelationManager,THREAD               ! RelationManager for PAYTYPES
Access:CONTHIST      &FileManager,THREAD                   ! FileManager for CONTHIST
Relate:CONTHIST      &RelationManager,THREAD               ! RelationManager for CONTHIST
Access:DEFPRINT      &FileManager,THREAD                   ! FileManager for DEFPRINT
Relate:DEFPRINT      &RelationManager,THREAD               ! RelationManager for DEFPRINT
Access:JOBSTAMP      &FileManager,THREAD                   ! FileManager for JOBSTAMP
Relate:JOBSTAMP      &RelationManager,THREAD               ! RelationManager for JOBSTAMP
Access:ACCESDEF      &FileManager,THREAD                   ! FileManager for ACCESDEF
Relate:ACCESDEF      &RelationManager,THREAD               ! RelationManager for ACCESDEF
Access:STANTEXT      &FileManager,THREAD                   ! FileManager for STANTEXT
Relate:STANTEXT      &RelationManager,THREAD               ! RelationManager for STANTEXT
Access:NOTESENG      &FileManager,THREAD                   ! FileManager for NOTESENG
Relate:NOTESENG      &RelationManager,THREAD               ! RelationManager for NOTESENG
Access:JOBACCNO      &FileManager,THREAD                   ! FileManager for JOBACCNO
Relate:JOBACCNO      &RelationManager,THREAD               ! RelationManager for JOBACCNO
Access:JOBRPNOT      &FileManager,THREAD                   ! FileManager for JOBRPNOT
Relate:JOBRPNOT      &RelationManager,THREAD               ! RelationManager for JOBRPNOT
Access:JOBSOBF       &FileManager,THREAD                   ! FileManager for JOBSOBF
Relate:JOBSOBF       &RelationManager,THREAD               ! RelationManager for JOBSOBF
Access:JOBSINV       &FileManager,THREAD                   ! FileManager for JOBSINV
Relate:JOBSINV       &RelationManager,THREAD               ! RelationManager for JOBSINV
Access:JOBSCONS      &FileManager,THREAD                   ! FileManager for JOBSCONS
Relate:JOBSCONS      &RelationManager,THREAD               ! RelationManager for JOBSCONS
Access:JOBSWARR      &FileManager,THREAD                   ! FileManager for JOBSWARR
Relate:JOBSWARR      &RelationManager,THREAD               ! RelationManager for JOBSWARR
Access:JOBSE2        &FileManager,THREAD                   ! FileManager for JOBSE2
Relate:JOBSE2        &RelationManager,THREAD               ! RelationManager for JOBSE2
Access:TRDSPEC       &FileManager,THREAD                   ! FileManager for TRDSPEC
Relate:TRDSPEC       &RelationManager,THREAD               ! RelationManager for TRDSPEC
Access:ORDPEND       &FileManager,THREAD                   ! FileManager for ORDPEND
Relate:ORDPEND       &RelationManager,THREAD               ! RelationManager for ORDPEND
Access:STOHIST       &FileManager,THREAD                   ! FileManager for STOHIST
Relate:STOHIST       &RelationManager,THREAD               ! RelationManager for STOHIST
Access:REPTYDEF      &FileManager,THREAD                   ! FileManager for REPTYDEF
Relate:REPTYDEF      &RelationManager,THREAD               ! RelationManager for REPTYDEF
Access:PRIORITY      &FileManager,THREAD                   ! FileManager for PRIORITY
Relate:PRIORITY      &RelationManager,THREAD               ! RelationManager for PRIORITY
Access:JOBSE         &FileManager,THREAD                   ! FileManager for JOBSE
Relate:JOBSE         &RelationManager,THREAD               ! RelationManager for JOBSE
Access:MANFAULT      &FileManager,THREAD                   ! FileManager for MANFAULT
Relate:MANFAULT      &RelationManager,THREAD               ! RelationManager for MANFAULT
Access:TRACHAR       &FileManager,THREAD                   ! FileManager for TRACHAR
Relate:TRACHAR       &RelationManager,THREAD               ! RelationManager for TRACHAR
Access:DISCOUNT      &FileManager,THREAD                   ! FileManager for DISCOUNT
Relate:DISCOUNT      &RelationManager,THREAD               ! RelationManager for DISCOUNT
Access:LOCVALUE      &FileManager,THREAD                   ! FileManager for LOCVALUE
Relate:LOCVALUE      &RelationManager,THREAD               ! RelationManager for LOCVALUE
Access:STOCKTYP      &FileManager,THREAD                   ! FileManager for STOCKTYP
Relate:STOCKTYP      &RelationManager,THREAD               ! RelationManager for STOCKTYP
Access:COURIER       &FileManager,THREAD                   ! FileManager for COURIER
Relate:COURIER       &RelationManager,THREAD               ! RelationManager for COURIER
Access:TRDPARTY      &FileManager,THREAD                   ! FileManager for TRDPARTY
Relate:TRDPARTY      &RelationManager,THREAD               ! RelationManager for TRDPARTY
Access:JOBNOTES      &FileManager,THREAD                   ! FileManager for JOBNOTES
Relate:JOBNOTES      &RelationManager,THREAD               ! RelationManager for JOBNOTES
Access:TRANTYPE      &FileManager,THREAD                   ! FileManager for TRANTYPE
Relate:TRANTYPE      &RelationManager,THREAD               ! RelationManager for TRANTYPE
Access:MANFAURL      &FileManager,THREAD                   ! FileManager for MANFAURL
Relate:MANFAURL      &RelationManager,THREAD               ! RelationManager for MANFAURL
Access:MODELCCT      &FileManager,THREAD                   ! FileManager for MODELCCT
Relate:MODELCCT      &RelationManager,THREAD               ! RelationManager for MODELCCT
Access:MANFAUEX      &FileManager,THREAD                   ! FileManager for MANFAUEX
Relate:MANFAUEX      &RelationManager,THREAD               ! RelationManager for MANFAUEX
Access:MANFPARL      &FileManager,THREAD                   ! FileManager for MANFPARL
Relate:MANFPARL      &RelationManager,THREAD               ! RelationManager for MANFPARL
Access:ESNMODEL      &FileManager,THREAD                   ! FileManager for ESNMODEL
Relate:ESNMODEL      &RelationManager,THREAD               ! RelationManager for ESNMODEL
Access:JOBPAYMT      &FileManager,THREAD                   ! FileManager for JOBPAYMT
Relate:JOBPAYMT      &RelationManager,THREAD               ! RelationManager for JOBPAYMT
Access:MANREJR       &FileManager,THREAD                   ! FileManager for MANREJR
Relate:MANREJR       &RelationManager,THREAD               ! RelationManager for MANREJR
Access:STOCK         &FileManager,THREAD                   ! FileManager for STOCK
Relate:STOCK         &RelationManager,THREAD               ! RelationManager for STOCK
Access:ORDWEBPR      &FileManager,THREAD                   ! FileManager for ORDWEBPR
Relate:ORDWEBPR      &RelationManager,THREAD               ! RelationManager for ORDWEBPR
Access:ESREJRES      &FileManager,THREAD                   ! FileManager for ESREJRES
Relate:ESREJRES      &RelationManager,THREAD               ! RelationManager for ESREJRES
Access:MANMARK       &FileManager,THREAD                   ! FileManager for MANMARK
Relate:MANMARK       &RelationManager,THREAD               ! RelationManager for MANMARK
Access:MODPROD       &FileManager,THREAD                   ! FileManager for MODPROD
Relate:MODPROD       &RelationManager,THREAD               ! RelationManager for MODPROD
Access:MANUDATE      &FileManager,THREAD                   ! FileManager for MANUDATE
Relate:MANUDATE      &RelationManager,THREAD               ! RelationManager for MANUDATE
Access:TRAFAULO      &FileManager,THREAD                   ! FileManager for TRAFAULO
Relate:TRAFAULO      &RelationManager,THREAD               ! RelationManager for TRAFAULO
Access:WEBJOBNO      &FileManager,THREAD                   ! FileManager for WEBJOBNO
Relate:WEBJOBNO      &RelationManager,THREAD               ! RelationManager for WEBJOBNO
Access:TRAFAULT      &FileManager,THREAD                   ! FileManager for TRAFAULT
Relate:TRAFAULT      &RelationManager,THREAD               ! RelationManager for TRAFAULT
Access:JOBLOHIS      &FileManager,THREAD                   ! FileManager for JOBLOHIS
Relate:JOBLOHIS      &RelationManager,THREAD               ! RelationManager for JOBLOHIS
Access:LOAN          &FileManager,THREAD                   ! FileManager for LOAN
Relate:LOAN          &RelationManager,THREAD               ! RelationManager for LOAN
Access:NOTESCON      &FileManager,THREAD                   ! FileManager for NOTESCON
Relate:NOTESCON      &RelationManager,THREAD               ! RelationManager for NOTESCON
Access:VATCODE       &FileManager,THREAD                   ! FileManager for VATCODE
Relate:VATCODE       &RelationManager,THREAD               ! RelationManager for VATCODE
Access:SMSMAIL       &FileManager,THREAD                   ! FileManager for SMSMAIL
Relate:SMSMAIL       &RelationManager,THREAD               ! RelationManager for SMSMAIL
Access:LOANHIST      &FileManager,THREAD                   ! FileManager for LOANHIST
Relate:LOANHIST      &RelationManager,THREAD               ! RelationManager for LOANHIST
Access:EXCHHIST      &FileManager,THREAD                   ! FileManager for EXCHHIST
Relate:EXCHHIST      &RelationManager,THREAD               ! RelationManager for EXCHHIST
Access:ACCESSOR      &FileManager,THREAD                   ! FileManager for ACCESSOR
Relate:ACCESSOR      &RelationManager,THREAD               ! RelationManager for ACCESSOR
Access:SUBACCAD      &FileManager,THREAD                   ! FileManager for SUBACCAD
Relate:SUBACCAD      &RelationManager,THREAD               ! RelationManager for SUBACCAD
Access:WARPARTS      &FileManager,THREAD                   ! FileManager for WARPARTS
Relate:WARPARTS      &RelationManager,THREAD               ! RelationManager for WARPARTS
Access:PARTS         &FileManager,THREAD                   ! FileManager for PARTS
Relate:PARTS         &RelationManager,THREAD               ! RelationManager for PARTS
Access:JOBACC        &FileManager,THREAD                   ! FileManager for JOBACC
Relate:JOBACC        &RelationManager,THREAD               ! RelationManager for JOBACC
Access:STOCKALL      &FileManager,THREAD                   ! FileManager for STOCKALL
Relate:STOCKALL      &RelationManager,THREAD               ! RelationManager for STOCKALL
Access:USELEVEL      &FileManager,THREAD                   ! FileManager for USELEVEL
Relate:USELEVEL      &RelationManager,THREAD               ! RelationManager for USELEVEL
Access:ALLLEVEL      &FileManager,THREAD                   ! FileManager for ALLLEVEL
Relate:ALLLEVEL      &RelationManager,THREAD               ! RelationManager for ALLLEVEL
Access:ACCAREAS      &FileManager,THREAD                   ! FileManager for ACCAREAS
Relate:ACCAREAS      &RelationManager,THREAD               ! RelationManager for ACCAREAS
Access:AUDIT         &FileManager,THREAD                   ! FileManager for AUDIT
Relate:AUDIT         &RelationManager,THREAD               ! RelationManager for AUDIT
Access:USERS         &FileManager,THREAD                   ! FileManager for USERS
Relate:USERS         &RelationManager,THREAD               ! RelationManager for USERS
Access:LOCSHELF      &FileManager,THREAD                   ! FileManager for LOCSHELF
Relate:LOCSHELF      &RelationManager,THREAD               ! RelationManager for LOCSHELF
Access:DEFAULTS      &FileManager,THREAD                   ! FileManager for DEFAULTS
Relate:DEFAULTS      &RelationManager,THREAD               ! RelationManager for DEFAULTS
Access:REPTYCAT      &FileManager,THREAD                   ! FileManager for REPTYCAT
Relate:REPTYCAT      &RelationManager,THREAD               ! RelationManager for REPTYCAT
Access:MANFPALO      &FileManager,THREAD                   ! FileManager for MANFPALO
Relate:MANFPALO      &RelationManager,THREAD               ! RelationManager for MANFPALO
Access:JOBSTAGE      &FileManager,THREAD                   ! FileManager for JOBSTAGE
Relate:JOBSTAGE      &RelationManager,THREAD               ! RelationManager for JOBSTAGE
Access:MANFAUPA      &FileManager,THREAD                   ! FileManager for MANFAUPA
Relate:MANFAUPA      &RelationManager,THREAD               ! RelationManager for MANFAUPA
Access:LOCINTER      &FileManager,THREAD                   ! FileManager for LOCINTER
Relate:LOCINTER      &RelationManager,THREAD               ! RelationManager for LOCINTER
Access:STAHEAD       &FileManager,THREAD                   ! FileManager for STAHEAD
Relate:STAHEAD       &RelationManager,THREAD               ! RelationManager for STAHEAD
Access:NOTESFAU      &FileManager,THREAD                   ! FileManager for NOTESFAU
Relate:NOTESFAU      &RelationManager,THREAD               ! RelationManager for NOTESFAU
Access:MANFAULO      &FileManager,THREAD                   ! FileManager for MANFAULO
Relate:MANFAULO      &RelationManager,THREAD               ! RelationManager for MANFAULO
Access:JOBEXHIS      &FileManager,THREAD                   ! FileManager for JOBEXHIS
Relate:JOBEXHIS      &RelationManager,THREAD               ! RelationManager for JOBEXHIS
Access:STARECIP      &FileManager,THREAD                   ! FileManager for STARECIP
Relate:STARECIP      &RelationManager,THREAD               ! RelationManager for STARECIP
Access:STOPARTS      &FileManager,THREAD                   ! FileManager for STOPARTS
Relate:STOPARTS      &RelationManager,THREAD               ! RelationManager for STOPARTS
Access:RETSTOCK      &FileManager,THREAD                   ! FileManager for RETSTOCK
Relate:RETSTOCK      &RelationManager,THREAD               ! RelationManager for RETSTOCK
Access:RETPAY        &FileManager,THREAD                   ! FileManager for RETPAY
Relate:RETPAY        &RelationManager,THREAD               ! RelationManager for RETPAY
Access:RETDESNO      &FileManager,THREAD                   ! FileManager for RETDESNO
Relate:RETDESNO      &RelationManager,THREAD               ! RelationManager for RETDESNO
Access:ORDPARTS      &FileManager,THREAD                   ! FileManager for ORDPARTS
Relate:ORDPARTS      &RelationManager,THREAD               ! RelationManager for ORDPARTS
Access:WAYBILLS      &FileManager,THREAD                   ! FileManager for WAYBILLS
Relate:WAYBILLS      &RelationManager,THREAD               ! RelationManager for WAYBILLS
Access:LOCATION      &FileManager,THREAD                   ! FileManager for LOCATION
Relate:LOCATION      &RelationManager,THREAD               ! RelationManager for LOCATION
Access:STOMPFAU      &FileManager,THREAD                   ! FileManager for STOMPFAU
Relate:STOMPFAU      &RelationManager,THREAD               ! RelationManager for STOMPFAU
Access:STOMJFAU      &FileManager,THREAD                   ! FileManager for STOMJFAU
Relate:STOMJFAU      &RelationManager,THREAD               ! RelationManager for STOMJFAU
Access:EXCHANGE      &FileManager,THREAD                   ! FileManager for EXCHANGE
Relate:EXCHANGE      &RelationManager,THREAD               ! RelationManager for EXCHANGE
Access:STOESN        &FileManager,THREAD                   ! FileManager for STOESN
Relate:STOESN        &RelationManager,THREAD               ! RelationManager for STOESN
Access:LOANACC       &FileManager,THREAD                   ! FileManager for LOANACC
Relate:LOANACC       &RelationManager,THREAD               ! RelationManager for LOANACC
Access:SUPPLIER      &FileManager,THREAD                   ! FileManager for SUPPLIER
Relate:SUPPLIER      &RelationManager,THREAD               ! RelationManager for SUPPLIER
Access:SUBURB        &FileManager,THREAD                   ! FileManager for SUBURB
Relate:SUBURB        &RelationManager,THREAD               ! RelationManager for SUBURB
Access:EXCHACC       &FileManager,THREAD                   ! FileManager for EXCHACC
Relate:EXCHACC       &RelationManager,THREAD               ! RelationManager for EXCHACC
Access:COUBUSHR      &FileManager,THREAD                   ! FileManager for COUBUSHR
Relate:COUBUSHR      &RelationManager,THREAD               ! RelationManager for COUBUSHR
Access:SUPVALA       &FileManager,THREAD                   ! FileManager for SUPVALA
Relate:SUPVALA       &RelationManager,THREAD               ! RelationManager for SUPVALA
Access:ESTPARTS      &FileManager,THREAD                   ! FileManager for ESTPARTS
Relate:ESTPARTS      &RelationManager,THREAD               ! RelationManager for ESTPARTS
Access:SBO_OutFaultParts &FileManager,THREAD               ! FileManager for SBO_OutFaultParts
Relate:SBO_OutFaultParts &RelationManager,THREAD           ! RelationManager for SBO_OutFaultParts
Access:TRAHUBS       &FileManager,THREAD                   ! FileManager for TRAHUBS
Relate:TRAHUBS       &RelationManager,THREAD               ! RelationManager for TRAHUBS
Access:TRAEMAIL      &FileManager,THREAD                   ! FileManager for TRAEMAIL
Relate:TRAEMAIL      &RelationManager,THREAD               ! RelationManager for TRAEMAIL
Access:SUPVALB       &FileManager,THREAD                   ! FileManager for SUPVALB
Relate:SUPVALB       &RelationManager,THREAD               ! RelationManager for SUPVALB
Access:SUBBUSHR      &FileManager,THREAD                   ! FileManager for SUBBUSHR
Relate:SUBBUSHR      &RelationManager,THREAD               ! RelationManager for SUBBUSHR
Access:JOBS          &FileManager,THREAD                   ! FileManager for JOBS
Relate:JOBS          &RelationManager,THREAD               ! RelationManager for JOBS
Access:MODELNUM      &FileManager,THREAD                   ! FileManager for MODELNUM
Relate:MODELNUM      &RelationManager,THREAD               ! RelationManager for MODELNUM
Access:TRABUSHR      &FileManager,THREAD                   ! FileManager for TRABUSHR
Relate:TRABUSHR      &RelationManager,THREAD               ! RelationManager for TRABUSHR
Access:SUBEMAIL      &FileManager,THREAD                   ! FileManager for SUBEMAIL
Relate:SUBEMAIL      &RelationManager,THREAD               ! RelationManager for SUBEMAIL
Access:ORDERS        &FileManager,THREAD                   ! FileManager for ORDERS
Relate:ORDERS        &RelationManager,THREAD               ! RelationManager for ORDERS
Access:UNITTYPE      &FileManager,THREAD                   ! FileManager for UNITTYPE
Relate:UNITTYPE      &RelationManager,THREAD               ! RelationManager for UNITTYPE
Access:STATUS        &FileManager,THREAD                   ! FileManager for STATUS
Relate:STATUS        &RelationManager,THREAD               ! RelationManager for STATUS
Access:REPAIRTY      &FileManager,THREAD                   ! FileManager for REPAIRTY
Relate:REPAIRTY      &RelationManager,THREAD               ! RelationManager for REPAIRTY
Access:TRDACC        &FileManager,THREAD                   ! FileManager for TRDACC
Relate:TRDACC        &RelationManager,THREAD               ! RelationManager for TRDACC
Access:INVOICE       &FileManager,THREAD                   ! FileManager for INVOICE
Relate:INVOICE       &RelationManager,THREAD               ! RelationManager for INVOICE
Access:TRDBATCH      &FileManager,THREAD                   ! FileManager for TRDBATCH
Relate:TRDBATCH      &RelationManager,THREAD               ! RelationManager for TRDBATCH
Access:TRACHRGE      &FileManager,THREAD                   ! FileManager for TRACHRGE
Relate:TRACHRGE      &RelationManager,THREAD               ! RelationManager for TRACHRGE
Access:TRDMODEL      &FileManager,THREAD                   ! FileManager for TRDMODEL
Relate:TRDMODEL      &RelationManager,THREAD               ! RelationManager for TRDMODEL
Access:STOMODEL      &FileManager,THREAD                   ! FileManager for STOMODEL
Relate:STOMODEL      &RelationManager,THREAD               ! RelationManager for STOMODEL
Access:TRADEACC      &FileManager,THREAD                   ! FileManager for TRADEACC
Relate:TRADEACC      &RelationManager,THREAD               ! RelationManager for TRADEACC
Access:SUBCHRGE      &FileManager,THREAD                   ! FileManager for SUBCHRGE
Relate:SUBCHRGE      &RelationManager,THREAD               ! RelationManager for SUBCHRGE
Access:DEFCHRGE      &FileManager,THREAD                   ! FileManager for DEFCHRGE
Relate:DEFCHRGE      &RelationManager,THREAD               ! RelationManager for DEFCHRGE
Access:SUBTRACC      &FileManager,THREAD                   ! FileManager for SUBTRACC
Relate:SUBTRACC      &RelationManager,THREAD               ! RelationManager for SUBTRACC
Access:CHARTYPE      &FileManager,THREAD                   ! FileManager for CHARTYPE
Relate:CHARTYPE      &RelationManager,THREAD               ! RelationManager for CHARTYPE
Access:WAYAUDIT      &FileManager,THREAD                   ! FileManager for WAYAUDIT
Relate:WAYAUDIT      &RelationManager,THREAD               ! RelationManager for WAYAUDIT
Access:WAYBAWT       &FileManager,THREAD                   ! FileManager for WAYBAWT
Relate:WAYBAWT       &RelationManager,THREAD               ! RelationManager for WAYBAWT
Access:WAYSUND       &FileManager,THREAD                   ! FileManager for WAYSUND
Relate:WAYSUND       &RelationManager,THREAD               ! RelationManager for WAYSUND
Access:WAYBILLJ      &FileManager,THREAD                   ! FileManager for WAYBILLJ
Relate:WAYBILLJ      &RelationManager,THREAD               ! RelationManager for WAYBILLJ
Access:MANUFACT      &FileManager,THREAD                   ! FileManager for MANUFACT
Relate:MANUFACT      &RelationManager,THREAD               ! RelationManager for MANUFACT
Access:USMASSIG      &FileManager,THREAD                   ! FileManager for USMASSIG
Relate:USMASSIG      &RelationManager,THREAD               ! RelationManager for USMASSIG
Access:WEBJOB        &FileManager,THREAD                   ! FileManager for WEBJOB
Relate:WEBJOB        &RelationManager,THREAD               ! RelationManager for WEBJOB
Access:USUASSIG      &FileManager,THREAD                   ! FileManager for USUASSIG
Relate:USUASSIG      &RelationManager,THREAD               ! RelationManager for USUASSIG
Access:JOBSLOCK      &FileManager,THREAD                   ! FileManager for JOBSLOCK
Relate:JOBSLOCK      &RelationManager,THREAD               ! RelationManager for JOBSLOCK
Access:TRAHUBAC      &FileManager,THREAD                   ! FileManager for TRAHUBAC
Relate:TRAHUBAC      &RelationManager,THREAD               ! RelationManager for TRAHUBAC
Access:STOHISTE      &FileManager,THREAD                   ! FileManager for STOHISTE
Relate:STOHISTE      &RelationManager,THREAD               ! RelationManager for STOHISTE
Access:RTNORDER      &FileManager,THREAD                   ! FileManager for RTNORDER
Relate:RTNORDER      &RelationManager,THREAD               ! RelationManager for RTNORDER
Access:WAYCNR        &FileManager,THREAD                   ! FileManager for WAYCNR
Relate:WAYCNR        &RelationManager,THREAD               ! RelationManager for WAYCNR
Access:MODEXCHA      &FileManager,THREAD                   ! FileManager for MODEXCHA
Relate:MODEXCHA      &RelationManager,THREAD               ! RelationManager for MODEXCHA
Access:MULDESPJ_ALIAS &FileManager,THREAD                  ! FileManager for MULDESPJ_ALIAS
Relate:MULDESPJ_ALIAS &RelationManager,THREAD              ! RelationManager for MULDESPJ_ALIAS
Access:MULDESP_ALIAS &FileManager,THREAD                   ! FileManager for MULDESP_ALIAS
Relate:MULDESP_ALIAS &RelationManager,THREAD               ! RelationManager for MULDESP_ALIAS
Access:JOBSOBF_ALIAS &FileManager,THREAD                   ! FileManager for JOBSOBF_ALIAS
Relate:JOBSOBF_ALIAS &RelationManager,THREAD               ! RelationManager for JOBSOBF_ALIAS
Access:JOBSE_ALIAS   &FileManager,THREAD                   ! FileManager for JOBSE_ALIAS
Relate:JOBSE_ALIAS   &RelationManager,THREAD               ! RelationManager for JOBSE_ALIAS
Access:MANFAULT_ALIAS &FileManager,THREAD                  ! FileManager for MANFAULT_ALIAS
Relate:MANFAULT_ALIAS &RelationManager,THREAD              ! RelationManager for MANFAULT_ALIAS
Access:JOBNOTES_ALIAS &FileManager,THREAD                  ! FileManager for JOBNOTES_ALIAS
Relate:JOBNOTES_ALIAS &RelationManager,THREAD              ! RelationManager for JOBNOTES_ALIAS
Access:JOBPAYMT_ALIAS &FileManager,THREAD                  ! FileManager for JOBPAYMT_ALIAS
Relate:JOBPAYMT_ALIAS &RelationManager,THREAD              ! RelationManager for JOBPAYMT_ALIAS
Access:STOCK_ALIAS   &FileManager,THREAD                   ! FileManager for STOCK_ALIAS
Relate:STOCK_ALIAS   &RelationManager,THREAD               ! RelationManager for STOCK_ALIAS
Access:LOAN_ALIAS    &FileManager,THREAD                   ! FileManager for LOAN_ALIAS
Relate:LOAN_ALIAS    &RelationManager,THREAD               ! RelationManager for LOAN_ALIAS
Access:WARPARTS_ALIAS &FileManager,THREAD                  ! FileManager for WARPARTS_ALIAS
Relate:WARPARTS_ALIAS &RelationManager,THREAD              ! RelationManager for WARPARTS_ALIAS
Access:PARTS_ALIAS   &FileManager,THREAD                   ! FileManager for PARTS_ALIAS
Relate:PARTS_ALIAS   &RelationManager,THREAD               ! RelationManager for PARTS_ALIAS
Access:USERS_ALIAS   &FileManager,THREAD                   ! FileManager for USERS_ALIAS
Relate:USERS_ALIAS   &RelationManager,THREAD               ! RelationManager for USERS_ALIAS
Access:MANFPALO_ALIAS &FileManager,THREAD                  ! FileManager for MANFPALO_ALIAS
Relate:MANFPALO_ALIAS &RelationManager,THREAD              ! RelationManager for MANFPALO_ALIAS
Access:MANFAUPA_ALIAS &FileManager,THREAD                  ! FileManager for MANFAUPA_ALIAS
Relate:MANFAUPA_ALIAS &RelationManager,THREAD              ! RelationManager for MANFAUPA_ALIAS
Access:MANFAULO_ALIAS &FileManager,THREAD                  ! FileManager for MANFAULO_ALIAS
Relate:MANFAULO_ALIAS &RelationManager,THREAD              ! RelationManager for MANFAULO_ALIAS
Access:LOCATION_ALIAS &FileManager,THREAD                  ! FileManager for LOCATION_ALIAS
Relate:LOCATION_ALIAS &RelationManager,THREAD              ! RelationManager for LOCATION_ALIAS
Access:EXCHANGE_ALIAS &FileManager,THREAD                  ! FileManager for EXCHANGE_ALIAS
Relate:EXCHANGE_ALIAS &RelationManager,THREAD              ! RelationManager for EXCHANGE_ALIAS
Access:JOBS2_ALIAS   &FileManager,THREAD                   ! FileManager for JOBS2_ALIAS
Relate:JOBS2_ALIAS   &RelationManager,THREAD               ! RelationManager for JOBS2_ALIAS
Access:JOBS_ALIAS    &FileManager,THREAD                   ! FileManager for JOBS_ALIAS
Relate:JOBS_ALIAS    &RelationManager,THREAD               ! RelationManager for JOBS_ALIAS
Access:TRADEACC_ALIAS &FileManager,THREAD                  ! FileManager for TRADEACC_ALIAS
Relate:TRADEACC_ALIAS &RelationManager,THREAD              ! RelationManager for TRADEACC_ALIAS

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END

lCurrentFDSetting    LONG                                  ! Used by window frame dragging
lAdjFDSetting        LONG                                  ! ditto

  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('.\WebServer_Phase3.INI', NVD_INI)           ! Configure INIManager to use INI file
  DctInit
                             ! Begin Generated by NetTalk Extension Template
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 4.57')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & sub (7300,1,1) & '.' & sub (7300,2,1))
      NetDebugTrace ('[Nettalk Template] NetTalk Object version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
      NetDebugTrace ('[Nettalk Template] ABC Template Chain')
    end
                             ! End Generated by Extension Template
  SYSTEM{PROP:Icon} = 'cellular3g.ico'
                 !CapeSoft MessageBox init code
  ThisMessageBox.init(1,1)
                 !End of CapeSoft MessageBox init code
  CPCSStartUpPrintDevice = PRINTER{PROPPRINT:DEVICE}
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)       ! Configure frame dragging
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
                             ! Begin Generated by NetTalk Extension Template
    NetCloseCallBackWindow() ! Tell NetTalk DLL to shutdown it's WinSock Call Back Window
  
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 4.57')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & sub (7300,1,1) & '.' & sub (7300,2,1))
      NetDebugTrace ('[Nettalk Template] Closing Down NetTalk (Object) version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
    end
                             ! End Generated by Extension Template
      ThisMessageBox.Kill()                     !CapeSoft MessageBox template generated code
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher
    
! XmlExport Methods
! ===============================================================
XmlExport.FInit procedure(aFile)
    code
    self.ExportFile &= aFile
    self.Rec &= aFile{prop:record}
    return
! ===============================================================
XmlExport.FWrite procedure(aStr)
    code
    if (self.FirstTimeWrite) then
        self.FirstTimeWrite = false
        clear(self.ExportFile)
        self.rec = '<?xml version="1.0" encoding="UTF-8"?>'
        add(self.ExportFile)
    end
    clear(self.ExportFile)
    self.Rec = all(' ', self.indent) & clip(aStr)
    add(self.ExportFile)
    return
! ===============================================================
XmlExport.FOpen procedure(aFilename, aIncludeXmlHeader)
    code
    self.ExportFile{PROP:Name} = clip(aFilename)
    open(self.ExportFile)
    if (errorcode()) then
        create(self.ExportFile)
        open(self.ExportFile)
        if (errorcode()) then
            return level:fatal
        end
    else
        !empty(self.ExportFile)
    end
    self.indent = 0
    self.FirstTimeWrite = aIncludeXmlHeader
    return level:benign
! ================================================================
XmlExport.FClose procedure()
    code
    close(self.ExportFile)
    return
! ================================================================
XmlExport.OpenTag procedure(aTag, aAttributeString)
    code
    if (omitted(3)) then
        self.FWrite('<' & clip(aTag) & '>')
    else
        self.FWrite('<' & clip(aTag) & ' ' & clip(aAttributeString) & '>')
    end
    self.indent += 4
    return
! ================================================================
XmlExport.CloseTag procedure(aTag)
    code
    self.indent -= 4
    if (self.indent < 0) then
        self.indent = 0
    end
    self.FWrite('</' & clip(aTag) & '>')
    return
! ================================================================
XmlExport.WriteTag procedure(aTag, aStr, aAttributeString)
xmldata    string(255)
    code
    len# = len(clip(aStr))
    p# = 1
    xmldata = ''
    loop ix# = 1 TO len#
        if (aStr[ix#] = '&') then
            xmldata[p# : p#+4] = '&amp;'
            p# += 5
        elsif (aStr[ix#] = '<') then
            xmldata[p# : p#+3] = '&lt;'
            p# += 4
        elsif (aStr[ix#] = '>') then
            xmldata[p# : p#+3] = '&gt;'
            p# += 4
        elsif (aStr[ix#] = '"') then
            xmldata[p# : p#+5] = '&quot;'
            p# += 6
        elsif (aStr[ix#] = '''') then
            xmldata[p# : p#+5] = '&apos;'
            p# += 6
        elsif (val(aStr[ix#]) > 07Fh) then
            xmldata[p# : p#+5] = '&#'&FORMAT(val(aStr[ix#]), @P###P)&';'
            p# += 6
        else
            xmldata[p# : p#] = aStr[ix#]
            p# += 1
       end
   end
   if (omitted(4)) then
       self.FWrite('<' & clip(aTag) & '>' & clip(xmldata) & '</' & clip(aTag) & '>')
   else
       self.FWrite('<' & clip(aTag) & ' ' & clip(aAttributeString) & '>' & clip(xmldata) & '</' & clip(aTag) & '>')
   end
   return
! ================================================================
BHStripReplace            Procedure(String func:String,String func:Strip,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1
    StripLength#    = Len(func:Strip)

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

        IF SUB(func:String,STR_POS#,StripLength#) = func:Strip
            If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            Else !If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) &  SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            End !If func:Replace <> ''

        End
    End
    RETURN(func:String)
BHStripNonAlphaNum      Procedure(String func:String,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

     IF VAL(SUB(func:string,STR_POS#,1)) < 32 Or VAL(SUB(func:string,STR_POS#,1)) > 126 Or |
        VAL(SUB(func:string,STR_POS#,1)) = 34 Or VAL(SUB(func:string,STR_POS#,1)) = 44
           func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    End
    RETURN(func:String)
BHStripForFilename      Procedure(String func:String)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    !func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

        IF SUB(func:String,STR_POS#,1) = '\' OR |
           SUB(func:String,STR_POS#,1) = '/' OR |
           SUB(func:String,STR_POS#,1) = ':' OR |
           SUB(func:String,STR_POS#,1) = '*' OR |
           SUB(func:String,STR_POS#,1) = '?' OR |
           SUB(func:String,STR_POS#,1) = '"' OR |
           SUB(func:String,STR_POS#,1) = '<<' OR |
           SUB(func:String,STR_POS#,1) = '>' OR |
           SUB(func:String,STR_POS#,1) = '|'

           func:String = SUB(func:String,1,STR_POS#-1) & ' ' & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    END
    RETURN(func:String)
BHGetFileFromPath		Procedure(String fullPath)
count                                           LONG()
locFilename                                     STRING(255)
    CODE
        locFilename = fullPath
        LOOP count = LEN(CLIP(fullPath)) TO 1 BY -1
            IF (SUB(fullPath,count,1) = '\')
                locFilename = CLIP(SUB(fullPath,count + 1,255))
                BREAK
            END
        END
        RETURN CLIP(locFilename)

BHGetPathFromFile			Procedure(String fullPath,<Byte removeSlash>)
count                                           LONG()
locPath                                         STRING(255)
    CODE
        locPath = fullPath
        LOOP count = LEN(CLIP(fullPath)) TO 1 BY -1
            IF (SUB(fullPath,count,1) = '\')
                IF (removeSlash)
                    locPath = CLIP(SUB(fullPath,1,count - 1))
                ELSE
                    locPath = CLIP(SUB(fullPath,1,count))
                END

                BREAK
            END
        END
        RETURN CLIP(locPath)
BHGetFileNoExtension    PROCEDURE(STRING fullPath)
count LONG()
locFilename                                             STRING(255)
    code
        locFilename = BHGetFileFromPath(fullPath)
        LOOP count = LEN(CLIP(locFilename)) TO 1 BY -1
            IF (SUB(locFilename,count,1) = '.')
                locFilename = CLIP(SUB(locFilename,1,count - 1))
                BREAK
            END
        END
        RETURN CLIP(locFilename)
BHPutFileIntoQueue          Procedure(STRING fullPath,*bhQFileList Q)
locBreakPoint                                   LONG()
locStartPoint                                   LONG()
locFilePath                                     STRING(255)
    code
        locBreakPoint = INSTRING('<124>',fullPath,1,1)

        IF (locBreakPoint > 0)
            locFilePath = SUB(fullPath,1,locBreakPoint - 1) & '\'

            locStartPoint = locBreakPoint + 1
            LOOP xx# = (locBreakPoint + 1) TO LEN(CLIP(fullPath))
                IF (SUB(fullPath,xx#,1) = '<124>')
                    Q.qFilename = CLIP(locFilePath) & CLIP(SUB(fullPath,locStartPoint,(xx# - locStartPoint)))
                    ADD(Q)
                    locStartPoint = xx# + 1
                END
            END
            Q.qFilename = CLIP(locFilePath) & CLIP(SUB(fullPath,locStartPoint,(xx# - locStartPoint)))
            ADD(Q)
        ELSE
            Q.qFilename = CLIP(fullPath)
            ADD(Q)
        end
BHAddBackSlash              Procedure(STRING fullPath)
ReturnPath  STRING(255)
    code
        ReturnPath = fullPath
        IF (SUB(CLIP(fullPath),-1,1) <> '\')
            ReturnPath = CLIP(fullPath) & '\'
        END
        RETURN CLIP(ReturnPath)
BHRunDOS                Procedure(String f:Command,<f:Wait>,<f:NoTimeOut>)
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

!STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
Code
    CommandLine = clip(f:Command)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = CreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = clock() + (f:Wait * 100)
    setcursor(cursor:wait)

    if f:Wait > 0 or f:NoTimeOut > 0 then
        loop
            Sleep(100)
            ! check for when process is finished
            ReturnCode# = GetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
            if (ProcessExitCode <> 259)  ! 259 = Still Active
                break
            end
            if (f:NoTimeOut <> 1)
                if (clock() < timeout#)
                    break
                end
            end
        end
    end
    setcursor
    return(true)
ThisMessageBox.Init                     procedure  (long UseABCClasses=0,long UseDefaultFile=0)   !CapeSoft MessageBox Object Procedure
TempVar         long,dim(2)
TMPLogFileName  string(252)

  Code
    parent.Init (UseABCClasses,UseDefaultFile)
    system{prop:MessageHook} = address(ds_Message)
    system{prop:StopHook} = address(ds_Stop)
    system{prop:HaltHook} = address(ds_Halt)
    self.StaticFeatures = Glo:Set_MsgLogging
    self.SetGlobalSetting('ShowTimeOut',0)
    self.SetGlobalSetting('GPFHotKey', CtrlAltG)
    self.SetGlobalSetting('CopyKey', CtrlC)
    self.SetGlobalSetting('INIFile','CSMesBox.INI')
    self.SetGlobalSetting('INISection', 'CS_Messages')
    self.SetGlobalSetting('LogFileName','c:\MessageBox.Log')
    self.SetGlobalSetting('DateFormat', '@d17')

ThisMessageBox.PrimeLog                 procedure  (<string ExtraDetails>)   !CapeSoft MessageBox Object Procedure

  Code
              !CapeSoft MessageBox Template Generated code to prime the logging records
                 !Because there is no File selected, record priming is handled in the object and
                 !the default ASCII file is used to log the records.
    parent.PrimeLog (ExtraDetails)


!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    INCLUDE('VodacomClass.inc','Procedures')
BHDebugMessage		Procedure(String fMessage)
 CODE
?       Beep(Beep:SystemExclamation)  ;  Yield()
?       Case Message('== DEBUG MESSAGE =='&|
?            '|'&|
?            '|' & Clip(fMessage) & ''&|
?            '|'&|
?            '|== END ==','Debug Message',|
?            Icon:Exclamation,'&OK',1)
?       Of 1 ! &OK Button
?       End!Case Message
RETURN
BHAddToDebugLog		Procedure(String fMessage,<String fOverrideFilename>)
 CODE
?   If (fOverrideFilename = '')
?       BHDebugFileName = CLIP('d:\SBOnline-debug.log')
?   ELSE
?       BHDebugFileName = CLIP(fOverrideFilename)
?   END
?   IF (NOT EXISTS(BHDebugFilename))
?       CREATE(BHDebugExportFile)
?       SHARE(BHDebugExportFile)
?   END
?   SHARE(BHDebugExportFile)
?   dbg:DebugLine = '"' & FORMAT(TODAY(),@d06) & '","' & FORMAT(CLOCK(),@T01) & '","' & CLIP(fMessage) & '"'
?   ADD(BHDebugExportFile)
?   CLOSE(BHDebugExportFile)
FillCpcsIniStrings      PROCEDURE(CpcsIniFile,WindowMessage,PreviewDialogTitle,PreviewDialogText)
IniToUse      CSTRING(63)
  CODE
  IF CLIP(CpcsIniFile) <> ''
    IniToUse = CLIP(CpcsIniFile)
  ELSE
    IniToUse = 'WIN.INI'
  END
  CPCS:ProgWinTitlePrvw =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinTitlePrvw',       'Generating Report', IniToUse)
  CPCS:ProgWinTitlePrnt =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinTitlePrnt',       'Printing Report', IniToUse)
  CPCS:ProgWinPctText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinPctText',         '% Completed', IniToUse)
  CPCS:ProgWinRecText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinRecText',         'Records Read', IniToUse)
  CPCS:ProgWinUsrText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinUsrText',         WindowMessage, IniToUse)
  CPCS:PrtrDlgTitle     =  GETINI('DISPLAY STRINGS 2.0', 'PrinterDlgTitle',        'Report Destination', IniToUse)
  CPCS:AskPrvwDlgTitle  =  GETINI('DISPLAY STRINGS 2.0', 'AskPrvwDialogTitle',     PreviewDialogTitle, IniToUse)
  CPCS:AskPrvwDlgText   =  GETINI('DISPLAY STRINGS 2.0', 'AskPrvwDialogText',      PreviewDialogText, IniToUse)
  CPCS:AreYouSureTitle  =  GETINI('DISPLAY STRINGS 2.0', 'CancelAreYouSureTitle',  'Question', IniToUse)
  CPCS:AreYouSureText   =  GETINI('DISPLAY STRINGS 2.0', 'CancelAreYouSureText',   'Are you sure you want to CANCEL this Report?', IniToUse)
  CPCS:PrvwPartialTitle =  GETINI('DISPLAY STRINGS 2.0', 'CancelPrvwPartialTitle', 'Question', IniToUse)
  CPCS:PrvwPartialText  =  GETINI('DISPLAY STRINGS 2.0', 'CancelPrvwPartialText',  'Report Cancellation Requested!||Preview Report generated so far?', IniToUse)
  CPCS:NoDfltPrtrTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NoDfltPrtrTitle',        'WARNING!', IniToUse)
  CPCS:NoDfltPrtrText   =  GETINI('DISPLAY STRINGS 2.0', 'NoDfltPrtrText',         'Warning:  No DEFAULT Printer Driver Found!||Printing may not occur correctly (or at all)|without a Default printer assigned.||Use CONTROL PANEL (PRINTERS) to set a|printer as your Default.||Continue with report anyway?', IniToUse)
  CPCS:NthgToPrvwTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrvwTitle',     'NOTE...', IniToUse)
  CPCS:NthgToPrvwText   =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrvwText',      'Nothing to Preview.', IniToUse)
  CPCS:NthgToPrntTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrntTitle',     'NOTE...', IniToUse)
  CPCS:NthgToPrntText   =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrntText',      'Nothing to Print.', IniToUse)
  CPCS:AsciiOutTitle    =  GETINI('DISPLAY STRINGS 2.0', 'AsciiOutTitle',          'Save to what Filename?', IniToUse)
  CPCS:AsciiOutMasks    =  GETINI('DISPLAY STRINGS 2.0', 'AsciiOutMasks',          'Text|*.TXT|All Files|*.*', IniToUse)
  CPCS:DynLblErrTitle   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrTitle',         'Problem!', IniToUse)
  CPCS:DynLblErrText1   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrText1',         'Sorry!||', IniToUse)
  CPCS:DynLblErrText2   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrText2',         '||Does not support paper type:', IniToUse)

  CPCS:ButtonYesNoCancel = GETINI('DISPLAY STRINGS 2.0', 'ButtonYesNoCancel',      '&Yes|&No|&Cancel', IniToUse)
  CPCS:ButtonYesNo       = GETINI('DISPLAY STRINGS 2.0', 'ButtonYesNo',            '&Yes|&No', IniToUse)
  CPCS:ButtonYes         = GETINI('DISPLAY STRINGS 2.0', 'ButtonYes',              '&Yes', IniToUse)
  CPCS:ButtonYesNoIgnore = GETINI('DISPLAY STRINGS 2.0', 'ButtonYesNoIgnore',      '&Yes|&No|&Ignore', IniToUse)
  CPCS:ButtonOK          = GETINI('DISPLAY STRINGS 2.0', 'ButtonOk',               '&Ok', IniToUse)




Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

