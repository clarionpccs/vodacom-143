

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3019.INC'),ONCE        !Local module procedure declarations
                     END


ForceAuthorityNumber PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Authority Number
    If (def:Force_Authority_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Authority_Number <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Authority_Number = 'B'
    Return Level:Benign
ForceAccessories     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    If (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B' And func:Type = 'B') Or |
        (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
        Return Level:Fatal
    End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
    Return Level:Benign
CheckParts           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locReturnValue       BYTE                                  !
FilesOpened     BYTE(0)
  CODE
    p_web.SSV('CheckParts:ReturnValue',0)

    do openFiles

    do CheckParts

    do closeFiles

    p_web.SSV('CheckParts:ReturnValue',locReturnValue)
checkParts    routine
    Data
tmp:FoundReceived   Byte()
    Code
!Return (1) = Pending Order
!Return (2) = On Order
!Return (3) = Back Order Spares
!Return (4) = Parts Recevied
!Return (0) = Received Order Or No Parts With Anything To Do WIth Orders
    tmp:FoundReceived = False
    locReturnValue = 0
    Case p_web.GSV('CheckParts:Type')
        Of 'C'
            If p_web.GSV('job:Chargeable_Job') = 'YES'
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = p_web.GSV('job:ref_number')
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = par:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = par:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If par:order_number <> ''
                        If par:date_received = ''
                            locReturnValue = 2
                            exit

                        Else
                            tmp:FoundReceived = True
                        End!If par:date_received = ''
                    End!If par:order_number <> ''
                end !loop
            End ! If p_web.GSV('job:Chargeable_Job = 'YES'

        Of 'W'
            If p_web.GSV('job:Warranty_Job') = 'YES'
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = p_web.GSV('job:ref_number')
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If wpr:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = wpr:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = wpr:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)

                    If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If wpr:order_number <> ''
                        If wpr:date_received = ''
                            locReturnValue = 2
                            exit
                        Else!If wpr:date_recieved = ''
                            tmp:FoundReceived = True
                        End!If wpr:date_recieved = ''
                    End!If wpr:order_number <> ''
                end !loop

            End ! If p_web.GSV('job:Warranty_Job = 'YES'

    End!Case f_type

    If tmp:FoundReceived = True
        !All parts received - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 4
        exit
    Else ! If tmp:FoundReceived = True
        !No parts attached - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 0
    End ! If tmp:FoundReceived = True


!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:STOCK.Close
     FilesOpened = False
  END
BrowseStatusChanges  PROCEDURE  (NetWebServerWorker p_web)
tmp:StatusTypeFilter STRING(3)                             !Status Type Filer
tmp:UserName         STRING(60)                            !User Name
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(AUDSTATS)
                      Project(aus:RecordNumber)
                      Project(aus:DateChanged)
                      Project(aus:TimeChanged)
                      Project(aus:OldStatus)
                      Project(aus:NewStatus)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseStatusChanges')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStatusChanges:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStatusChanges:NoForm')
      loc:FormName = p_web.GetValue('BrowseStatusChanges:FormName')
    else
      loc:FormName = 'BrowseStatusChanges_frm'
    End
    p_web.SSV('BrowseStatusChanges:NoForm',loc:NoForm)
    p_web.SSV('BrowseStatusChanges:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStatusChanges:NoForm')
    loc:FormName = p_web.GSV('BrowseStatusChanges:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStatusChanges') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStatusChanges')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(AUDSTATS,aus:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'AUS:DATECHANGED') then p_web.SetValue('BrowseStatusChanges_sort','1')
    ElsIf (loc:vorder = 'AUS:TIMECHANGED') then p_web.SetValue('BrowseStatusChanges_sort','2')
    ElsIf (loc:vorder = 'TMP:USERNAME') then p_web.SetValue('BrowseStatusChanges_sort','4')
    ElsIf (loc:vorder = 'AUS:OLDSTATUS') then p_web.SetValue('BrowseStatusChanges_sort','5')
    ElsIf (loc:vorder = 'AUS:NEWSTATUS') then p_web.SetValue('BrowseStatusChanges_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStatusChanges:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStatusChanges:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseStatusChanges:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStatusChanges:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStatusChanges:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStatusChanges_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStatusChanges_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'aus:DateChanged','-aus:DateChanged')
    Loc:LocateField = 'aus:DateChanged'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'aus:TimeChanged','-aus:TimeChanged')
    Loc:LocateField = 'aus:TimeChanged'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:UserName','-tmp:UserName')
    Loc:LocateField = 'tmp:UserName'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aus:OldStatus)','-UPPER(aus:OldStatus)')
    Loc:LocateField = 'aus:OldStatus'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aus:NewStatus)','-UPPER(aus:NewStatus)')
    Loc:LocateField = 'aus:NewStatus'
  end
  if loc:vorder = ''
    loc:vorder = '-aus:RecordNumber'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
    If Instring('AUS:TYPE',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'UPPER(aus:Type),' & loc:vorder
    End
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') = '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('aus:DateChanged')
    loc:SortHeader = p_web.Translate('Date Changed')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@d6')
  Of upper('aus:TimeChanged')
    loc:SortHeader = p_web.Translate('Time Changed')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@t1b')
  Of upper('tmp:UserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s60')
  Of upper('aus:OldStatus')
    loc:SortHeader = p_web.Translate('Old Status')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s30')
  Of upper('aus:NewStatus')
    loc:SortHeader = p_web.Translate('New Status')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseStatusChanges:LookupFrom')
  End!Else
  loc:formaction = 'BrowseStatusChanges'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStatusChanges:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStatusChanges:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStatusChanges:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="AUDSTATS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="aus:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseStatusChanges',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStatusChanges.locate(''Locator2BrowseStatusChanges'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStatusChanges.cl(''BrowseStatusChanges'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseStatusChanges_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseStatusChanges_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseStatusChanges','Date Changed','Click here to sort by Date Changed',,'RightJustify',100,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Date Changed')&'">'&p_web.Translate('Date Changed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseStatusChanges','Time Changed','Click here to sort by Time Changed',,'RightJustify',100,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Time Changed')&'">'&p_web.Translate('Time Changed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseStatusChanges','Username',,,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'">'&p_web.Translate('Username')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseStatusChanges','Old Status','Click here to sort by Old Status',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Old Status')&'">'&p_web.Translate('Old Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseStatusChanges','New Status','Click here to sort by New Status',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by New Status')&'">'&p_web.Translate('New Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'aus:DateChanged' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('aus:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and AUDSTATS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'aus:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('aus:RecordNumber'),p_web.GetValue('aus:RecordNumber'),p_web.GetSessionValue('aus:RecordNumber'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
      tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
      loc:FilterWas = 'aus:Type = ''' & clip(tmp:StatusTypeFilter) &''''
      If 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number') <> ''
        loc:FilterWas = clip(loc:FilterWas) & ' AND ' & 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number')
      End
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') = '')
      loc:FilterWas = 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number')
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStatusChanges_Filter')
    p_web.SetSessionValue('BrowseStatusChanges_FirstValue','')
    p_web.SetSessionValue('BrowseStatusChanges_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,AUDSTATS,aus:RecordNumberKey,loc:PageRows,'BrowseStatusChanges',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If AUDSTATS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(AUDSTATS,loc:firstvalue)
              Reset(ThisView,AUDSTATS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If AUDSTATS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(AUDSTATS,loc:lastvalue)
            Reset(ThisView,AUDSTATS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(aus:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStatusChanges.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStatusChanges.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStatusChanges.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStatusChanges.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStatusChanges_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStatusChanges_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseStatusChanges',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStatusChanges.locate(''Locator1BrowseStatusChanges'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStatusChanges.cl(''BrowseStatusChanges'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseStatusChanges_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStatusChanges_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStatusChanges.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStatusChanges.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStatusChanges.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStatusChanges.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = aus:UserCode
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        tmp:UserName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
    Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        tmp:UserName = '* Unknown User Code: ' & Clip(aus:UserCode) & ' *'
    End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    
    loc:field = aus:RecordNumber
    p_web._thisrow = p_web._nocolon('aus:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStatusChanges:LookupField')) = aus:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((aus:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseStatusChanges.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If AUDSTATS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(AUDSTATS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If AUDSTATS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(AUDSTATS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aus:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseStatusChanges.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aus:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseStatusChanges.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:DateChanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:TimeChanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:UserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:OldStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:NewStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseStatusChanges.omv(this);" onMouseOut="BrowseStatusChanges.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseStatusChanges=new browseTable(''BrowseStatusChanges'','''&clip(loc:formname)&''','''&p_web._jsok('aus:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('aus:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseStatusChanges.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseStatusChanges.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStatusChanges')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStatusChanges')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStatusChanges')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStatusChanges')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(AUDSTATS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(AUDSTATS)
  Bind(aus:Record)
  Clear(aus:Record)
  NetWebSetSessionPics(p_web,AUDSTATS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('aus:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::aus:DateChanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:DateChanged_'&aus:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:DateChanged,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:TimeChanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:TimeChanged_'&aus:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:TimeChanged,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:UserName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:UserName_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:UserName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:OldStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:OldStatus_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:OldStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:NewStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:NewStatus_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:NewStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = aus:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('aus:RecordNumber',aus:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('aus:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aus:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aus:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseStatusFilter   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:StatusTypeFilter STRING(3)                             !Status Type Filter
tmp:JOB              STRING('JOB')                         !
tmp:EXC              STRING('EXC')                         !
tmp:2NE              STRING('2NE')                         !
tmp:LOA              STRING('LOA')                         !
FilesOpened     Long
AUDSTATS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BrowseStatusFilter')
  loc:formname = 'BrowseStatusFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BrowseStatusFilter','None')
    p_web._DivHeader('BrowseStatusFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BrowseStatusFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BrowseStatusFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('tmp:StatusTypeFilter','JOB')
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  
  p_web.SetValue('BrowseStatusFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:StatusTypeFilter')
    tmp:StatusTypeFilter = p_web.GetValue('tmp:StatusTypeFilter')
    p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BrowseStatusFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BrowseStatusFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BrowseStatusFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('BrowseStatusFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BrowseStatusFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BrowseStatusFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BrowseStatusFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Status Changes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Status Changes',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BrowseStatusFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Filter') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BrowseStatusFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('BrowseStatusFilter_BrowseStatusChanges_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:StatusTypeFilter')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Filter') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StatusTypeFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browseStatusChanges
      do Comment::browseStatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Status Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StatusTypeFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('NewValue'))
    tmp:StatusTypeFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StatusTypeFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('Value'))
    tmp:StatusTypeFilter = p_web.GetValue('Value')
  End
  do Value::tmp:StatusTypeFilter
  do SendAlert
  do Value::browseStatusChanges  !1

Value::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:StatusTypeFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatusfilter_tmp:statustypefilter_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:StatusTypeFilter',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:StatusTypeFilter') = 0
    p_web.SetSessionValue('tmp:StatusTypeFilter','JOB')
  end
    packet = clip(packet) & p_web.CreateOption('Job','JOB',choose('JOB' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Exchange','EXC',choose('EXC' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Loan','LOA',choose('LOA' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('2nd Exchange','2NE',choose('2NE' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('All','',choose('' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value')

Comment::tmp:StatusTypeFilter  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browseStatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browseStatusChanges',p_web.GetValue('NewValue'))
    do Value::browseStatusChanges
  Else
    p_web.StoreValue('aus:RecordNumber')
  End

Value::browseStatusChanges  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseStatusChanges --
  p_web.SetValue('BrowseStatusChanges:NoForm',1)
  p_web.SetValue('BrowseStatusChanges:FormName',loc:formname)
  p_web.SetValue('BrowseStatusChanges:parentIs','Form')
  p_web.SetValue('_parentProc','BrowseStatusFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('BrowseStatusFilter_BrowseStatusChanges_embedded_div')&'"><!-- Net:BrowseStatusChanges --></div><13,10>'
    p_web._DivHeader('BrowseStatusFilter_' & lower('BrowseStatusChanges') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('BrowseStatusFilter_' & lower('BrowseStatusChanges') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseStatusChanges --><13,10>'
  end
  do SendPacket

Comment::browseStatusChanges  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('browseStatusChanges') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BrowseStatusFilter_tmp:StatusTypeFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StatusTypeFilter
      else
        do Value::tmp:StatusTypeFilter
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)

PreCopy  Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BrowseStatusFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BrowseStatusFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)
  p_web.StoreValue('tmp:StatusTypeFilter')
  p_web.StoreValue('')
BrowseAuditFilter    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:StatusTypeFilter STRING(3)                             !Status Type Filter
tmp:JOB              STRING('JOB')                         !
tmp:EXC              STRING('EXC')                         !
tmp:2NE              STRING('2NE')                         !
tmp:LOA              STRING('LOA')                         !
FilesOpened     Long
AUDSTATS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BrowseAuditFilter')
  loc:formname = 'BrowseAuditFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BrowseAuditFilter','None')
    p_web._DivHeader('BrowseAuditFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BrowseAuditFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseAuditFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BrowseAuditFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('tmp:StatusTypeFilter','JOB')
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  
  p_web.SetValue('BrowseAuditFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:StatusTypeFilter')
    tmp:StatusTypeFilter = p_web.GetValue('tmp:StatusTypeFilter')
    p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BrowseAuditFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BrowseAuditFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BrowseAuditFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('BrowseAuditFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BrowseAuditFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BrowseAuditFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BrowseAuditFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Audit Trail') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Audit Trail',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BrowseAuditFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BrowseAuditFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BrowseAuditFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Filter') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseAuditFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BrowseAuditFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('BrowseAuditFilter_BrowseAuditTrail_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:StatusTypeFilter')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseAuditFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Filter') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseAuditFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StatusTypeFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseAuditFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browseAuditTrail
      do Comment::browseAuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Audit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StatusTypeFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('NewValue'))
    tmp:StatusTypeFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StatusTypeFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('Value'))
    tmp:StatusTypeFilter = p_web.GetValue('Value')
  End
  do Value::tmp:StatusTypeFilter
  do SendAlert
  do Value::browseAuditTrail  !1

Value::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:StatusTypeFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browseauditfilter_tmp:statustypefilter_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:StatusTypeFilter',loc:fieldclass,loc:readonly,,178,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:StatusTypeFilter') = 0
    p_web.SetSessionValue('tmp:StatusTypeFilter','JOB')
  end
    packet = clip(packet) & p_web.CreateOption('Job','JOB',choose('JOB' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Exchange','EXC',choose('EXC' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Loan','LOA',choose('LOA' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('2nd Exchange','2NE',choose('2NE' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('All','',choose('' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value')

Comment::tmp:StatusTypeFilter  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browseAuditTrail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browseAuditTrail',p_web.GetValue('NewValue'))
    do Value::browseAuditTrail
  Else
    p_web.StoreValue('aud:record_number')
  End

Value::browseAuditTrail  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAuditTrail --
  p_web.SetValue('BrowseAuditTrail:NoForm',1)
  p_web.SetValue('BrowseAuditTrail:FormName',loc:formname)
  p_web.SetValue('BrowseAuditTrail:parentIs','Form')
  p_web.SetValue('_parentProc','BrowseAuditFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('BrowseAuditFilter_BrowseAuditTrail_embedded_div')&'"><!-- Net:BrowseAuditTrail --></div><13,10>'
    p_web._DivHeader('BrowseAuditFilter_' & lower('BrowseAuditTrail') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('BrowseAuditFilter_' & lower('BrowseAuditTrail') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAuditTrail --><13,10>'
  end
  do SendPacket

Comment::browseAuditTrail  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseAuditFilter_' & p_web._nocolon('browseAuditTrail') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BrowseAuditFilter_tmp:StatusTypeFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StatusTypeFilter
      else
        do Value::tmp:StatusTypeFilter
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BrowseAuditFilter',0)

PreCopy  Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BrowseAuditFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BrowseAuditFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('BrowseAuditFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseAuditFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BrowseAuditFilter:Primed',0)
  p_web.setsessionvalue('showtab_BrowseAuditFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BrowseAuditFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BrowseAuditFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BrowseAuditFilter:Primed',0)
  p_web.StoreValue('tmp:StatusTypeFilter')
  p_web.StoreValue('')
BrowseAuditTrail     PROCEDURE  (NetWebServerWorker p_web)
tmp:StatusTypeFilter STRING(3)                             !Status Type Filer
tmp:UserName         STRING(60)                            !User Name
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(AUDIT)
                      Project(aud:record_number)
                      Project(aud:Date)
                      Project(aud:Time)
                      Project(aud:Action)
                      Project(aud:Notes)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseAuditTrail')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAuditTrail:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAuditTrail:NoForm')
      loc:FormName = p_web.GetValue('BrowseAuditTrail:FormName')
    else
      loc:FormName = 'BrowseAuditTrail_frm'
    End
    p_web.SSV('BrowseAuditTrail:NoForm',loc:NoForm)
    p_web.SSV('BrowseAuditTrail:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAuditTrail:NoForm')
    loc:FormName = p_web.GSV('BrowseAuditTrail:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAuditTrail') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAuditTrail')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(AUDIT,aud:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'AUD:DATE') then p_web.SetValue('BrowseAuditTrail_sort','1')
    ElsIf (loc:vorder = 'AUD:TIME') then p_web.SetValue('BrowseAuditTrail_sort','2')
    ElsIf (loc:vorder = 'TMP:USERNAME') then p_web.SetValue('BrowseAuditTrail_sort','4')
    ElsIf (loc:vorder = 'AUD:ACTION') then p_web.SetValue('BrowseAuditTrail_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAuditTrail:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAuditTrail:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseAuditTrail:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAuditTrail:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAuditTrail:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Both
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:None
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAuditTrail_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAuditTrail_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'aud:Date','-aud:Date')
    Loc:LocateField = 'aud:Date'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'aud:Time','-aud:Time')
    Loc:LocateField = 'aud:Time'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:UserName','-tmp:UserName')
    Loc:LocateField = 'tmp:UserName'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aud:Action)','-UPPER(aud:Action)')
    Loc:LocateField = 'aud:Action'
  end
  if loc:vorder = ''
    loc:vorder = '-aud:record_number'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
  ElsIf (loc:Parent = 'BrowseAuditFilter')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('aud:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@d6')
  Of upper('aud:Time')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@t1b')
  Of upper('tmp:UserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@s50')
  Of upper('aud:Action')
    loc:SortHeader = p_web.Translate('Action')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@s80')
  Of upper('aud:Notes')
    loc:SortHeader = p_web.Translate('Notes')
    p_web.SetSessionValue('BrowseAuditTrail_LocatorPic','@s255')
  End
  If loc:selecting = 1
    loc:selectaction = 'ViewJob'
  End!Else
  loc:formaction = 'BrowseAuditTrail'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAuditTrail:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAuditTrail:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAuditTrail:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="AUDIT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="aud:Record_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditTrail',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseAuditTrail',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAuditTrail.locate(''Locator2BrowseAuditTrail'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAuditTrail.cl(''BrowseAuditTrail'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseAuditTrail_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseAuditTrail_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseAuditTrail','Date','Click here to sort by Date Changed',,,50,1)
        Else
          packet = clip(packet) & '<th width="'&clip(50)&'" Title="'&p_web.Translate('Click here to sort by Date Changed')&'">'&p_web.Translate('Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseAuditTrail','Time','Click here to sort by Time Changed',,,50,1)
        Else
          packet = clip(packet) & '<th width="'&clip(50)&'" Title="'&p_web.Translate('Click here to sort by Time Changed')&'">'&p_web.Translate('Time')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseAuditTrail','Username',,,'LeftJustify',150,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(150)&'">'&p_web.Translate('Username')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseAuditTrail','Action','Click here to sort by Action',,'LeftJustify',300,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(300)&'" Title="'&p_web.Translate('Click here to sort by Action')&'">'&p_web.Translate('Action')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th class="'&clip('SmallText')&'" width="'&clip(350)&'" Title="'&p_web.Translate('Click here to sort by New Status')&'">'&p_web.Translate('Notes')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'aud:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('aud:record_number',lower(Thisview{prop:order}),1,1) = 0 !and AUDIT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'aud:record_number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('aud:record_number'),p_web.GetValue('aud:record_number'),p_web.GetSessionValue('aud:record_number'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
      loc:FilterWas = 'aud:Ref_Number = ' & p_web.GSV('job:Ref_Number') & ' AND Upper(aud:Type) = Upper(''' & p_web.GSV('tmp:StatusTypeFilter') & ''')'
  ElsIf (loc:Parent = 'BrowseAuditFilter')
      loc:FilterWas = 'aud:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditTrail',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAuditTrail_Filter')
    p_web.SetSessionValue('BrowseAuditTrail_FirstValue','')
    p_web.SetSessionValue('BrowseAuditTrail_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,AUDIT,aud:Record_Number_Key,loc:PageRows,'BrowseAuditTrail',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If AUDIT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(AUDIT,loc:firstvalue)
              Reset(ThisView,AUDIT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If AUDIT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(AUDIT,loc:lastvalue)
            Reset(ThisView,AUDIT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(aud:record_number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAuditTrail.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAuditTrail.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAuditTrail.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAuditTrail.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditTrail',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAuditTrail_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAuditTrail_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseAuditTrail',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAuditTrail.locate(''Locator1BrowseAuditTrail'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAuditTrail.cl(''BrowseAuditTrail'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseAuditTrail_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAuditTrail_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAuditTrail.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAuditTrail.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAuditTrail.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAuditTrail.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = aud:User
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        tmp:UserName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
    Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        tmp:UserName = '* Unknown User Code: ' & Clip(aus:UserCode) & ' *'
    End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    
    loc:field = aud:record_number
    p_web._thisrow = p_web._nocolon('aud:record_number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAuditTrail:LookupField')) = aud:record_number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((aud:record_number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseAuditTrail.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If AUDIT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(AUDIT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If AUDIT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(AUDIT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aud:record_number',clip(loc:field),,loc:checked,,,'onclick="BrowseAuditTrail.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aud:record_number',clip(loc:field),,'checked',,,'onclick="BrowseAuditTrail.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(50)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(50)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Time
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(150)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:UserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(300)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Action
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&clip('SmallText')&'" width="'&clip(350)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aud:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If Loc:Eip = 0
        do StartRowHTML
      End
      If(loc:SelectionMethod  = Net:Radio)
        packet = clip(packet) & '<td width="1">&#160;</td>' ! first column is the select column
      End
      loc:RowsIn += 1
      if loc:RowCount = 1 then loc:RowsHigh += 1.
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseAuditTrail.omv(this);" onMouseOut="BrowseAuditTrail.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseAuditTrail=new browseTable(''BrowseAuditTrail'','''&clip(loc:formname)&''','''&p_web._jsok('aud:record_number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('aud:record_number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseAuditTrail.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseAuditTrail.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAuditTrail')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAuditTrail')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAuditTrail')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAuditTrail')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(AUDIT)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(AUDIT)
  Bind(aud:Record)
  Clear(aud:Record)
  NetWebSetSessionPics(p_web,AUDIT)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('aud:record_number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::aud:Date   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Date_'&aud:record_number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Date,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aud:Time   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Time_'&aud:record_number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Time,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:UserName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:UserName_'&aud:record_number,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:UserName,'@s50')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aud:Action   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Action_'&aud:record_number,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Action,'@s80')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aud:Notes   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aud:Notes_'&aud:record_number,'SmallText',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aud:Notes,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = aud:record_number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('aud:record_number',aud:record_number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('aud:record_number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aud:record_number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aud:record_number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
AllocateEngineer     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:EngineerPassword STRING(30)                            !Engineer Password
FilesOpened     Long
USERS::State  USHORT
LOCATLOG::State  USHORT
JOBSENG::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AllocateEngineer')
  loc:formname = 'AllocateEngineer_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AllocateEngineer','')
    p_web._DivHeader('AllocateEngineer',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AllocateEngineer',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AllocateEngineer',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(LOCATLOG)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(LOCATLOG)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      p_web.SSV('comment:EngineerPassword','Enter password and press [TAB]')
      p_web.SSV('tmp:NewEngineer','')
      p_web.SSV('tmp:EngineerPassword','')
      p_web.SSV('locEngResults','')
  
      IF (p_web.GSV('jobe:Booking48HourOption') = 4 AND p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('job:Third_Party_Site') = '')
          p_web.SSV('Force3rdParty',1)
          p_web.SSV('locEngResults','Liquid Damage Handset! Send to 3rd Party for repairs.')
          ! DBH #10544 - Change Stauts to 3rd party.
          IF (SUB(p_web.GSV('job:Current_Status'),1,3) <> '405')
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','STATUS CHANGED TO SEND TO 3RD PARTY')
              AddToAudit(p_web)
              
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber','405')
              GetStatus(p_web)
          
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number = p_web.GSV('wob:RefNumber')
              If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  p_web.SessionQueueToFile(JOBS)
                  Access:JOBS.TryUpdate()
              End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber    = p_web.GSV('wob:RefNumber')
              if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Found
                  p_web.SessionQueueToFile(WEBJOB)
                  Access:WEBJOB.TryUpdate()
              else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)        
          END !IF (SUB(p_web.GSV('job:Current_Status'),1,3) <> '405')        
      ELSE
          p_web.SSV('Force3rdParty',0)
      END
  p_web.SetValue('AllocateEngineer_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Force3rdParty') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:EngineerPassword',tmp:EngineerPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:EngineerPassword')
    tmp:EngineerPassword = p_web.GetValue('tmp:EngineerPassword')
    p_web.SetSessionValue('tmp:EngineerPassword',tmp:EngineerPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('AllocateEngineer_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:EngineerPassword = p_web.RestoreValue('tmp:EngineerPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AllocateEngineer_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AllocateEngineer_ChainTo')
    loc:formaction = p_web.GetSessionValue('AllocateEngineer_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="AllocateEngineer" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="AllocateEngineer" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="AllocateEngineer" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Allocate Engineer') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Allocate Engineer',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_AllocateEngineer">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_AllocateEngineer" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_AllocateEngineer')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Force3rdParty') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Engineer Password') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_AllocateEngineer')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_AllocateEngineer'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Force3rdParty') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:EngineerPassword')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Force3rdParty') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_AllocateEngineer')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Force3rdParty') <> 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Force3rdParty') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Engineer Password') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AllocateEngineer_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:EngineerPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:EngineerPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:EngineerPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AllocateEngineer_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::SaveMessage
      do Comment::SaveMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:EngineerPassword  Routine
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineer Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:EngineerPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:EngineerPassword',p_web.GetValue('NewValue'))
    tmp:EngineerPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:EngineerPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:EngineerPassword',p_web.GetValue('Value'))
    tmp:EngineerPassword = p_web.GetValue('Value')
  End
  If tmp:EngineerPassword = ''
    loc:Invalid = 'tmp:EngineerPassword'
    loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
      p_web.SSV('tmp:NewEngineer','')
      error# = 0
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('tmp:EngineerPassword')
      If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
          If use:User_Code <> p_web.GSV('job:Engineer')
              If use:User_Type <> 'ENGINEER'
                  p_web.SSV('comment:EngineerPassword','The selected password is not an Engineer.')
                  error# = 1
                  
              Else ! If use:User_Type <> 'ENGINEER'
                  ! Does the engineer match the site of where the job was booked (DBH: 14/12/2007)
                  If use:Location <> p_web.GSV('JobBookingSiteLocation')
                      If p_web.GSV('BookingSite') = 'RRC' Or (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('jobe:HubRepair') = 0)
                          p_web.SSV('comment:EngineerPassword','The selected engineer cannot be allocated to this job.')
                          error# = 1
                      End ! If p_web.GSV('BookingSite') <> 'ARC'
                  Else ! If use:Location <> p_web.GSV('BookingSiteLocation')
                      If use:Location <> p_web.GSV('BookingSiteLocation')
                          p_web.SSV('comment:EngineerPassword','The selected engineer is not available for this site.')
                          error# = 1
                      Else ! If use:Location <> p_web.GSV('BookingSiteLocation')
                          !If at ARC, check if job has gone through waybill generation
                          If p_web.GSV('BookingSite') = 'ARC'
                              Access:LOCATLOG.Clearkey(lot:NewLocationKey)
                              lot:RefNumber = job:Ref_Number
                              lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI'))
                              If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                                  p_web.SSV('comment:EngineerPassword','This job is not at the ARC')
                                  error# = 1
                              End ! If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                          End ! If p_web.GSV('BookingSite') = 'ARC'
                      End ! If use:Location <> p_web.GSV('BookingSiteLocation')
                  End ! If use:Location <> p_web.GSV('BookingSiteLocation')
  
                  if (error# = 0)
                      p_web.SSV('tmp:NewEngineer',use:User_Code)
                      p_web.SSV('locEngResults','Engineer ' & clip(use:Forename) & ' ' & clip(use:Surname) & ' will be allocated to this job when you click ''Save''')
                  else
                      p_web.SSV('tmp:NewEngineer','')
                      p_web.SSV('locEngResults','')
                  end ! if (error# = 0)
              End !
          Else ! If use:User_Code <> p_web.GSV('job:Engineer')
              ! Picked the same engineer. Do nothing (DBH: 14/12/2007)
              p_web.SSV('tmp:NewEngineer','')
              p_web.SSV('comment:EngineerPassword','Engineer already attached to this job')
          End ! If use:User_Code <> p_web.GSV('job:Engineer')
      Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
          p_web.SSV('tmp:NewEngineer','')
          p_web.SSV('comment:EngineerPassword','Invalid password')
      End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  do Value::tmp:EngineerPassword
  do SendAlert
  do Comment::tmp:EngineerPassword
  do Value::SaveMessage  !1

Value::tmp:EngineerPassword  Routine
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:EngineerPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:EngineerPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:EngineerPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:EngineerPassword'',''allocateengineer_tmp:engineerpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:EngineerPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','tmp:EngineerPassword',p_web.GetSessionValueFormat('tmp:EngineerPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_value')

Comment::tmp:EngineerPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:EngineerPassword'))
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_comment')

Validate::SaveMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('SaveMessage',p_web.GetValue('NewValue'))
    do Value::SaveMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::SaveMessage  Routine
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('SaveMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locEngResults'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AllocateEngineer_' & p_web._nocolon('SaveMessage') & '_value')

Comment::SaveMessage  Routine
    loc:comment = ''
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('SaveMessage') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('AllocateEngineer_tmp:EngineerPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:EngineerPassword
      else
        do Value::tmp:EngineerPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_AllocateEngineer',0)

PreCopy  Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AllocateEngineer',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('AllocateEngineer:Primed',0)

PreDelete       Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('AllocateEngineer:Primed',0)
  p_web.setsessionvalue('showtab_AllocateEngineer',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Force3rdParty') <> 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
      IF (p_web.GSV('Force3rdParty') = 1)
  
      ELSE
          
  
          if (p_web.GSV('tmp:NewEngineer') = '')
              loc:Alert = 'You must select a new engineer'
              loc:Invalid = 'tmp:EngineerPassword'
              Exit
          else ! if (p_web.GSV('tmp:NewEngineer') = '')
          
              Access:USERS.Clearkey(use:User_Code_Key)
              use:User_Code    = p_web.GSV('tmp:NewEngineer')
              if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
              ! Found
  
                  engChanged# = TRUE    
                  IF (p_web.GSV('job:Engineer') = '')
                      p_web.SSV('AddToAudit:Type','JOB')
                      p_web.SSV('AddToAudit:Action','ENGINEER ALLOCATED: ' & p_web.GSV('tmp:NewEngineer'))
                      p_web.SSV('AddToAudit:Notes',Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' | 
                          & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
                      AddToAudit(p_web)                
                      engChanged# = FALSE
                  END
              
                  p_web.SSV('job:Engineer',use:User_Code)
                  p_web.SSV('EngineerAllocated',Today())
  
                  p_web.SSV('ChangesSaved',1)
  
                  If p_web.GSV('BookingSite') = 'RRC' Or |
                      (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('jobe:WebJob') = 0)
                      If p_web.GSV('jobe2:SMSNotification')
                          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'2ENG','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
                      End ! If p_web.GSV('jobe2:SMSNotification')
                      If p_web.GSV('jobe2:EmailNotification')
                          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'2ENG','EMAIL',p_web.GSV('jobe2:EMailAlertAddress'),'',0,'')
                      End ! If p_web.GSV('jobe2:EmailNotification')
  
          !                      If AddToAudit(p_web.GSV('job:Ref_Number'),'JOB','ENGINEER ALLOCATION: ' & Clip(p_web.GSV('job:Engineer')), Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
          !
          !                      End ! AddToAudit
                      If Access:JOBSENG.PrimeRecord() = Level:Benign
                          joe:JobNumber     = p_web.GSV('job:Ref_Number')
                          joe:UserCode      = p_web.GSV('job:Engineer')
                          joe:DateAllocated = Today()
                          joe:TimeAllocated = Clock()
                          joe:AllocatedBy   = p_web.GSV('BookingUserCode')
                          joe:Status        = 'ALLOCATED'
                          joe:StatusDate    = Today()
                          joe:StatusTime    = Clock()
                          joe:EngSkillLevel = use:SkillLevel
                          joe:JobSkillLevel = p_web.GSV('jobe:SkillLevel')
                          If Access:JOBSENG.TryInsert() = Level:Benign
  
                          Else ! If Access:JOBSENG.TryInsert() = Level:Benign
                              Access:JOBSENG.CancelAutoInc()
                          End ! If Access:JOBSENG.TryInsert() = Level:Benign
                      End ! If Access:JOBSENG.PrimeRecord() = Level:Benign
                      p_web.SSV('GetStatus:Type','JOB')
                      p_web.SSV('GetStatus:StatusNumber','310')
  
                      GetStatus(p_web)
                  End ! p_web.GSV('jobe:WebJob') = 0)
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number = p_web.GSV('wob:RefNumber')
                  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  ! #11300 DBH - Record if the engineer has changed.
                      IF (engChanged# = TRUE)
                          p_web.SSV('AddToAudit:Type','JOB')
                          p_web.SSV('AddToAudit:Action','ENGINEER CHANGED TO ' & p_web.GSV('job:Engineer'))
                          p_web.SSV('AddToAudit:Notes','NEW ENGINEER: ' & p_web.GSV('job:Engineer') & |
                              '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                              '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(job:Engineer))
                          AddToAudit(p_web)
                      END                
                      p_web.SessionQueueToFile(JOBS)
                      Access:JOBS.TryUpdate()
                  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber    = p_web.GSV('wob:RefNumber')
                  if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Found
                      p_web.SessionQueueToFile(WEBJOB)
                      Access:WEBJOB.TryUpdate()
                  else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Error
                  end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
           
              else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
              ! Error
                  loc:Alert = 'An error occurred retrieving the selected engineer''s details.'
                  loc:Invalid = 'tmp:EngineerPassword'
                  Exit
              end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
          end !if (p_web.GSV('tmp:NewEngineer') = '')
      END
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AllocateEngineer_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AllocateEngineer_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Force3rdParty') <> 1
    loc:InvalidTab += 1
        If tmp:EngineerPassword = ''
          loc:Invalid = 'tmp:EngineerPassword'
          loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('AllocateEngineer:Primed',0)
  p_web.StoreValue('tmp:EngineerPassword')
  p_web.StoreValue('')
RapidLocation        PROCEDURE  (STRING fLocation)         ! Declare Procedure
LOCATION::State  USHORT
FilesOpened     BYTE(0)
  CODE
    RETURN vod.IsRapidLocation(fLocation)
SaveFiles  ROUTINE
  LOCATION::State = Access:LOCATION.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF LOCATION::State <> 0
    Access:LOCATION.RestoreFile(LOCATION::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
getPartStatus        PROCEDURE  (fType)                    ! Declare Procedure
  CODE
!Assume in part, or warpart record
    colourFlag# = 0

    case fType
    of 'C' ! Chargeable part

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = par:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
        !    tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        !    tmp:CShelfLocation  = ''
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


        If par:Part_Number = 'EXCH'
            Case par:Status
                Of 'ORD'    !Order
                    colourFlag# = 6
                Of 'REQ'    !Allocate
                    colourFlag# = 1
                Of 'RTS'
                    colourFlag# = 7
            End !Case par:Status
        Else !par:Part_Number = 'EXCH'
            If par:WebOrder = 1
                colourFlag# = 6
            Else

                If par:PartAllocated Or ~(rapidLocation(sto:Location) And sto:Location <> '')
                    If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
                        colourFlag# = 1 !Green
                    End !If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''

                    If par:order_number <> '' And par:date_received =''
                        colourFlag# = 3 !Red
                    End !If par:order_number <> '' And par:date_received =''

                    If par:Order_Number <> '' And par:Date_Received <> ''
                        colourFlag# = 4 !Gray
                    End !If par:Order_Number <> '' And par:Date_Received <> ''
                Else !par:PartAllocated
                    If par:Status = ''
                        colourFlag# = 1 !Green
                    End !If par:Status = ''

                    Case par:Status
                        Of 'PRO'
                            colourFlag# = 5 !Yellow
                        Of 'PIK'
                            colourFlag# = 2 !Navy
                        Of 'RET'
                            colourFlag# = 7 !Pink?
                    End !Case par:Status
                End !par:PartAllocated

            End !par:WebOrder = 1
        End !par:Part_Number = 'EXCH'

    of 'W'
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = wpr:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
        !    tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        !    tmp:CShelfLocation  = ''
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


        If wpr:Part_Number = 'EXCH'
            Case wpr:Status
                Of 'ORD'    !Order
                    colourFlag# = 6
                Of 'REQ'    !Allocate
                    colourFlag# = 1
                Of 'RTS'
                    colourFlag# = 7
            End !Case wpr:Status
        Else !wpr:Part_Number = 'EXCH'
            If wpr:WebOrder = 1
                colourFlag# = 6
            Else

                If wpr:PartAllocated Or ~(rapidLocation(sto:Location) And sto:Location <> '')
                    If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
                        colourFlag# = 1 !Green
                    End !If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''

                    If wpr:order_number <> '' And wpr:date_received =''
                        colourFlag# = 3 !Red
                    End !If wpr:order_number <> '' And wpr:date_received =''

                    If wpr:Order_Number <> '' And wpr:Date_Received <> ''
                        colourFlag# = 4 !Gray
                    End !If wpr:Order_Number <> '' And wpr:Date_Received <> ''
                Else !wpr:PartAllocated
                    If wpr:Status = ''
                        colourFlag# = 1 !Green
                    End !If wpr:Status = ''

                    Case wpr:Status
                        Of 'PRO'
                            colourFlag# = 5 !Yellow
                        Of 'PIK'
                            colourFlag# = 2 !Navy
                        Of 'RET'
                            colourFlag# = 7 !Pink?
                    End !Case wpr:Status
                End !wpr:PartAllocated

            End !wpr:WebOrder = 1
        End !wpr:Part_Number = 'EXCH'


    !1 = green
    !2 = navy
    !3 = red
    !4 = gray
    !5 = fuschia
    !6 = purple
    !7 = 08080FFH

    end ! case fType


    case colourFlag#
    of 1
        return 'Requested'
    of 2
        return 'Picked'
    of 3
        return 'On Order'
    of 4
        !tmp:partStatus = 'Requested'
    of 5
        return 'Awaiting Picking'
    of 6
        return 'On Order'
    of 7
        return 'Awaiting Return'
    else
        return 'Allocated'
    end ! case colourFlag#
    return ''
