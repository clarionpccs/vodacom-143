

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3009.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3010.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateDateTimeStamp  PROCEDURE  (f:RefNumber)              ! Declare Procedure
JOBSTAMP::State  USHORT
FilesOpened     BYTE(0)
  CODE
    ! Inserting (DBH 05/08/2008) # 10253 - Update date/time stamp everytime a job changes
    do openfiles
    do savefiles
    Access:JOBSTAMP.ClearKey(jos:JOBSRefNumberKey)
    jos:JOBSRefNumber = f:RefNumber
    If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Found
        jos:DateStamp = Today()
        jos:TimeStamp = Clock()
        Access:JOBSTAMP.TryUpdate()
    Else ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
        !Error
        If Access:JOBSTAMP.PrimeRecord() = Level:Benign
            jos:JOBSRefNumber = f:RefNumber
            jos:DateStamp = Today()
            jos:TimeStamp = Clock()
            If Access:JOBSTAMP.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:JOBSTAMP.TryInsert() = Level:Benign
                Access:JOBSTAMP.CancelAutoInc()
            End ! If Access:JOBSTAMP.TryInsert() = Level:Benign
        End ! If Access.JOBSTAMP.PrimeRecord() = Level:Benign
    End ! If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign

    do restorefiles
    do closefiles
    ! End (DBH 05/08/2008) #10253
SaveFiles  ROUTINE
  JOBSTAMP::State = Access:JOBSTAMP.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBSTAMP::State <> 0
    Access:JOBSTAMP.RestoreFile(JOBSTAMP::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSTAMP.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSTAMP.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSTAMP.Close
     FilesOpened = False
  END
SetJobType           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locChargeableJob     STRING(3)                             !
locWarrantyJob       STRING(3)                             !
locCChargeType       STRING(30)                            !
locWChargeType       STRING(30)                            !
locCRepairType       STRING(30)                            !
locWRepairType       STRING(30)                            !
locConfirmationText  STRING(100)                           !
locErrorText         STRING(100)                           !
FilesOpened     Long
STOCKALL::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
REPTYDEF::State  USHORT
CHARTYPE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locCChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
locCRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
locWChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
locWRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
locClass                    CLASS
validateWarrantyParts           PROCEDURE(),BYTE
validateChargeableParts         PROCEDURE(),BYTE
                            END
  CODE


  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('SetJobType')
  loc:formname = 'SetJobType_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SetJobType','')
    p_web._DivHeader('SetJobType',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SetJobType',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SetJobType',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
deleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')
    p_web.DeleteSessionValue('ReadOnly:CRepairType')
    p_web.DeleteSessionValue('ReadOnly:CChargeType')
    p_web.DeleteSessionValue('ReadOnly:WRepairType')
    p_web.DeleteSessionValue('ReadOnly:WChargeType')
    p_web.DeleteSessionValue('locConfirmationText')
    p_web.DeleteSessionValue('locCChargeType')
    p_web.DeleteSessionValue('locChargeableJob')
    p_web.DeleteSessionValue('locCRepairType')
    p_web.DeleteSessionValue('locWarrantyJob')
    p_web.DeleteSessionValue('locWChargeType')
    p_web.DeleteSessionValue('locWRepairType')
enableFields        ROUTINE  
    p_web.SSV('ReadOnly:ChargeableJOb',0)
    p_web.SSV('ReadOnly:WarrantyJob',0)
    p_web.SSV('ReadOnly:CRepairType',0)
    p_web.SSV('ReadOnly:CChargeType',0)
    p_web.SSV('ReadOnly:WRepairType',0)
    p_web.SSV('ReadOnly:WChargeType',0)  
disableFields       ROUTINE
    p_web.SSV('ReadOnly:ChargeableJOb',1)
    p_web.SSV('ReadOnly:WarrantyJob',1)
    p_web.SSV('ReadOnly:CRepairType',1)
    p_web.SSV('ReadOnly:CChargeType',1)
    p_web.SSV('ReadOnly:WRepairType',1)
    p_web.SSV('ReadOnly:WChargeType',1)    
    

    
  
loadJobData         ROUTINE
    p_web.SSV('locChargeableJob',p_web.GSV('job:Chargeable_Job'))
    p_web.SSV('locWarrantyJob',p_web.GSV('job:Warranty_Job'))
    p_web.SSV('locCChargeType',p_web.GSV('job:Charge_Type'))
    p_web.SSV('locCRepairType',p_web.GSV('job:Repair_Type'))
    p_web.SSV('locWChargeType',p_web.GSV('job:Warranty_Charge_Type'))
    p_web.SSV('locWRepairType',p_web.GSV('job:Repair_Type_Warranty'))
resetScreen         ROUTINE
    DO enablefields
    DO loadJobData

    p_web.SSV('Hide:ConfirmButton',1)
    p_web.SSV('Hide:CancelButton',1)
    p_web.SSV('Hide:SplitJobButton',1)
    p_web.SSV('locConfirmationText','')
    p_web.SSV('locErrorText','')
    
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(CHARTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('SetJobType_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locChargeableJob',locChargeableJob)
  p_web.SetSessionValue('locCChargeType',locCChargeType)
  p_web.SetSessionValue('locCRepairType',locCRepairType)
  p_web.SetSessionValue('locWarrantyJob',locWarrantyJob)
  p_web.SetSessionValue('locWChargeType',locWChargeType)
  p_web.SetSessionValue('locWRepairType',locWRepairType)
  p_web.SetSessionValue('locErrorText',locErrorText)
  p_web.SetSessionValue('locConfirmationText',locConfirmationText)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locChargeableJob')
    locChargeableJob = p_web.GetValue('locChargeableJob')
    p_web.SetSessionValue('locChargeableJob',locChargeableJob)
  End
  if p_web.IfExistsValue('locCChargeType')
    locCChargeType = p_web.GetValue('locCChargeType')
    p_web.SetSessionValue('locCChargeType',locCChargeType)
  End
  if p_web.IfExistsValue('locCRepairType')
    locCRepairType = p_web.GetValue('locCRepairType')
    p_web.SetSessionValue('locCRepairType',locCRepairType)
  End
  if p_web.IfExistsValue('locWarrantyJob')
    locWarrantyJob = p_web.GetValue('locWarrantyJob')
    p_web.SetSessionValue('locWarrantyJob',locWarrantyJob)
  End
  if p_web.IfExistsValue('locWChargeType')
    locWChargeType = p_web.GetValue('locWChargeType')
    p_web.SetSessionValue('locWChargeType',locWChargeType)
  End
  if p_web.IfExistsValue('locWRepairType')
    locWRepairType = p_web.GetValue('locWRepairType')
    p_web.SetSessionValue('locWRepairType',locWRepairType)
  End
  if p_web.IfExistsValue('locErrorText')
    locErrorText = p_web.GetValue('locErrorText')
    p_web.SetSessionValue('locErrorText',locErrorText)
  End
  if p_web.IfExistsValue('locConfirmationText')
    locConfirmationText = p_web.GetValue('locConfirmationText')
    p_web.SetSessionValue('locConfirmationText',locConfirmationText)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('SetJobType_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      ! Generate Form
      DO loadJobData
      p_web.SSV('Hide:ConfirmButton',1)
      p_web.SSV('Hide:CancelButton',1)
      p_web.SSV('Hide:SplitJobButton',1)
      DO enableFields
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locChargeableJob = p_web.RestoreValue('locChargeableJob')
 locCChargeType = p_web.RestoreValue('locCChargeType')
 locCRepairType = p_web.RestoreValue('locCRepairType')
 locWarrantyJob = p_web.RestoreValue('locWarrantyJob')
 locWChargeType = p_web.RestoreValue('locWChargeType')
 locWRepairType = p_web.RestoreValue('locWRepairType')
 locErrorText = p_web.RestoreValue('locErrorText')
 locConfirmationText = p_web.RestoreValue('locConfirmationText')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BillingConfirmation?passedURL=ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SetJobType_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SetJobType_ChainTo')
    loc:formaction = p_web.GetSessionValue('SetJobType_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="SetJobType" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="SetJobType" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="SetJobType" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Set Job Type') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Set Job Type',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_SetJobType">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_SetJobType" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_SetJobType')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_SetJobType')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_SetJobType'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locChargeableJob')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_SetJobType')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Chargeable Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SetJobType_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locChargeableJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locChargeableJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locChargeableJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Warranty Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SetJobType_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarrantyJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SetJobType_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorText
      do Comment::locErrorText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locConfirmationText
      do Comment::locConfirmationText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonConfirm
      do Comment::buttonConfirm
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonSplitJob
      do Comment::buttonSplitJob
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCancel
      do Comment::buttonCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locChargeableJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Chargeable Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locChargeableJob') & '_prompt')

Validate::locChargeableJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locChargeableJob',p_web.GetValue('NewValue'))
    locChargeableJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locChargeableJob
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('locChargeableJob',p_web.GetValue('Value'))
    locChargeableJob = p_web.GetValue('Value')
  End
      ! Chargeable Job
      DO disableFields
      p_web.SSV('Hide:ConfirmButton',0)
      p_web.SSV('Hide:CancelButton',0)
      p_web.SSV('Hide:SplitJobButton',1)
      p_web.SSV('JobTypeAction','')
  
  
      IF ( p_web.GSV('locChargeableJob') = 'YES')
          IF (p_web.GSV('locWarrantyJob') = 'YES')
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job a Split Warranty/Chargeable Job')
              p_web.SSV('Hide:SplitJobButton',0)
              p_web.SSV('JobTypeAction','CHATOSPLIT')
          ELSE
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job Chargeable')
              p_web.SSV('JobTypeAction','CHA')
          END
          p_web.SSV('ConfirmButtonText','Chargeable')
      ELSE
  
          CASE locClass.validateChargeableParts()
          OF 1
              DO resetScreen
              p_web.ssv('locErrorText','Cannot make job Warranty Only. There are chargeable parts attached.')
          OF 2
              DO resetScreen
              p_web.ssv('locErrorText','Cannot make job Warranty Only. This job is an estimate.')
          ELSE
              
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job Warranty Only')
              p_web.SSV('ConfirmButtonText','Warranty')
              p_web.SSV('JobTypeAction','WAR')            
          END
          
      END
  do Value::locChargeableJob
  do SendAlert
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Comment::locCChargeType
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Comment::locCRepairType
  do Value::buttonConfirm  !1
  do Value::locConfirmationText  !1
  do Value::locWChargeType  !1
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1
  do Value::buttonCancel  !1
  do Value::buttonSplitJob  !1
  do Value::locErrorText  !1

Value::locChargeableJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locChargeableJob
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locChargeableJob'',''setjobtype_locchargeablejob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locChargeableJob')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ChargeableJob')= 1,'disabled','')
  If p_web.GetSessionValue('locChargeableJob') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locChargeableJob',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locChargeableJob') & '_value')

Comment::locChargeableJob  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_prompt',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Chargeable Type')
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCChargeType') & '_prompt')

Validate::locCChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCChargeType',p_web.GetValue('NewValue'))
    locCChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCChargeType',p_web.GetValue('Value'))
    locCChargeType = p_web.GetValue('Value')
  End
  do Value::locCChargeType
  do SendAlert
  do Value::locCRepairType  !1

Value::locCChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_value',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChargeableJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCChargeType'',''setjobtype_loccchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locCChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locCChargeType') = 0
    p_web.SetSessionValue('locCChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locCChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locCChargeType_OptionView)
  locCChargeType_OptionView{prop:filter} = 'Upper(cha:Warranty) = <39>NO<39>'
  locCChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(locCChargeType_OptionView)
  Loop
    Next(locCChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locCChargeType') = 0
      p_web.SetSessionValue('locCChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('locCChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locCChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCChargeType') & '_value')

Comment::locCChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_comment',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCChargeType') & '_comment')

Prompt::locCRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_prompt',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Repair Type')
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCRepairType') & '_prompt')

Validate::locCRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCRepairType',p_web.GetValue('NewValue'))
    locCRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCRepairType',p_web.GetValue('Value'))
    locCRepairType = p_web.GetValue('Value')
  End
  do Value::locCRepairType
  do SendAlert

Value::locCRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_value',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChargeableJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCRepairType'',''setjobtype_loccrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locCRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locCRepairType') = 0
    p_web.SetSessionValue('locCRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locCRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locCRepairType_OptionView)
  locCRepairType_OptionView{prop:filter} = 'Upper(rtd:Chargeable) = <39>YES<39> AND Upper(rtd:Manufacturer) = Upper(<39>' & p_web.GSV('job:manufacturer') & '<39>) AND rtd:NotAvailable = 0'
  locCRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(locCRepairType_OptionView)
  Loop
    Next(locCRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locCRepairType') = 0
      p_web.SetSessionValue('locCRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('locCRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locCRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCRepairType') & '_value')

Comment::locCRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_comment',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCRepairType') & '_comment')

Prompt::locWarrantyJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarrantyJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyJob',p_web.GetValue('NewValue'))
    locWarrantyJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyJob
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('locWarrantyJob',p_web.GetValue('Value'))
    locWarrantyJob = p_web.GetValue('Value')
  End
      ! Warranty Job
  DO disableFields
  p_web.SSV('JobTypeAction','')
  
  p_web.SSV('Hide:ConfirmButton',0)
  p_web.SSV('Hide:CancelButton',0)
  p_web.SSV('Hide:SplitJobButton',1)
  
  p_web.SSV('locErrorText','')
  
  IF ( p_web.GSV('locWarrantyJob') = 'YES')
      IF (p_web.GSV('locChargeableJob') = 'YES')
          p_web.SSV('locConfirmationText','Confirm that you wish to make this job a Split Warranty/Chargeable Job')
          p_web.SSV('Hide:SplitJobButton',0)
          p_web.SSV('JobTypeAction','WARTOSPLIT')
              
              ! Can't make warranty only.
          Case locClass.validateChargeableParts()
          of 1 ! Chargeable Parts
              p_web.SSV('Hide:ConfirmButton',1)
              p_web.SSV('locConfirmationText','Cannot make Warranty Only job as there are still Chargeable Parts attached. Please confirm if you wish to make this a split job.')
          of 2 ! Estimate
              p_web.SSV('Hide:ConfirmButton',1)
                          p_web.SSV('locConfirmationText','Cannot make Warranty Only job as it is an Estimate. Please confirm if you wish to make this a split job.')
          END
              
      ELSE
          p_web.SSV('locConfirmationText','Confirm that you wish to make this job Warranty')
          p_web.SSV('JobTypeAction','WAR')
      END
      p_web.SSV('ConfirmButtonText','Warranty')
  ELSE
      IF (locClass.validateWarrantyParts())
          p_web.SSV('locErrorText','Warning! There are still warranty parts attached. They will be transferred to Chargeable')
      END
          
      p_web.SSV('locConfirmationText','Confirm that you wish to make this job Chargeable Only')
      p_web.SSV('JobTypeAction','CHA')
      p_web.SSV('ConfirmButtonText','Chargeable')
  END
  do Value::locWarrantyJob
  do SendAlert
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Comment::locWChargeType
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Comment::locWRepairType
  do Value::buttonConfirm  !1
  do Value::buttonCancel  !1
  do Value::locConfirmationText  !1
  do Value::locCChargeType  !1
  do Value::locCRepairType  !1
  do Value::locChargeableJob  !1
  do Value::buttonSplitJob  !1
  do Value::locErrorText  !1

Value::locWarrantyJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locWarrantyJob
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWarrantyJob'',''setjobtype_locwarrantyjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyJob')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:WarrantyJob') = 1,'disabled','')
  If p_web.GetSessionValue('locWarrantyJob') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWarrantyJob',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_value')

Comment::locWarrantyJob  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_prompt',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWChargeType') & '_prompt')

Validate::locWChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWChargeType',p_web.GetValue('NewValue'))
    locWChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWChargeType',p_web.GetValue('Value'))
    locWChargeType = p_web.GetValue('Value')
  End
  do Value::locWChargeType
  do SendAlert

Value::locWChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_value',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWChargeType'',''setjobtype_locwchargetype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locWChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWChargeType') = 0
    p_web.SetSessionValue('locWChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locWChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWChargeType_OptionView)
  locWChargeType_OptionView{prop:filter} = 'Upper(cha:Warranty) = <39>YES<39>'
  locWChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(locWChargeType_OptionView)
  Loop
    Next(locWChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWChargeType') = 0
      p_web.SetSessionValue('locWChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('locWChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWChargeType') & '_value')

Comment::locWChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_comment',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWChargeType') & '_comment')

Prompt::locWRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_prompt',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Repair Type')
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWRepairType') & '_prompt')

Validate::locWRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWRepairType',p_web.GetValue('NewValue'))
    locWRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWRepairType',p_web.GetValue('Value'))
    locWRepairType = p_web.GetValue('Value')
  End
  do Value::locWRepairType
  do SendAlert

Value::locWRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_value',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWRepairType'',''setjobtype_locwrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locWRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWRepairType') = 0
    p_web.SetSessionValue('locWRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locWRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWRepairType_OptionView)
  locWRepairType_OptionView{prop:filter} = 'Upper(rtd:Warranty) = <39>YES<39> AND Upper(rtd:Manufacturer) = Upper(<39>' & p_web.GSV('job:manufacturer') & '<39>) AND rtd:NotAvailable = 0'
  locWRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(locWRepairType_OptionView)
  Loop
    Next(locWRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWRepairType') = 0
      p_web.SetSessionValue('locWRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('locWRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWRepairType') & '_value')

Comment::locWRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_comment',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWRepairType') & '_comment')

Validate::locErrorText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorText',p_web.GetValue('NewValue'))
    locErrorText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorText',p_web.GetValue('Value'))
    locErrorText = p_web.GetValue('Value')
  End

Value::locErrorText  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locErrorText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locErrorText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locErrorText') & '_value')

Comment::locErrorText  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locErrorText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locConfirmationText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConfirmationText',p_web.GetValue('NewValue'))
    locConfirmationText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConfirmationText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConfirmationText',p_web.GetValue('Value'))
    locConfirmationText = p_web.GetValue('Value')
  End

Value::locConfirmationText  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locConfirmationText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locConfirmationText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locConfirmationText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locConfirmationText') & '_value')

Comment::locConfirmationText  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locConfirmationText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirm  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirm',p_web.GetValue('NewValue'))
    do Value::buttonConfirm
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Confirm change
  CASE p_web.GSV('JobTypeAction')
  OF 'CHA' ! Make Chargeable Job
      p_web.SSV('locChargeableJob','YES')
      p_web.SSV('locWarrantyJob','NO')
  OF 'WAR' ! Make Warranty Job
      p_web.SSV('locWarrantyJob','YES')
      p_web.SSV('locChargeableJob','NO')
  OF 'CHATOSPLIT' !Make Chargeable From Warranty
          ! Transfer Warranty Parts
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = p_web.GSV('job:Ref_Number')
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP
          IF (Access:WARPARTS.Next())
              BREAK
          END
          IF (wpr:Ref_Number <> p_web.GSV('job:ref_Number'))
              BREAK
          END
          IF (Access:PARTS.PrimeRecord() = Level:Benign)
              recordNo# = par:Record_Number
              par:Record :=: wpr:Record
              par:Record_Number = recordNo#
              IF (Access:PARTS.TryInsert())
                  Access:PARTS.CancelAutoInc()
              END
          END
              ! Is this part in stock allocation. 
          Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
          stl:PartType = 'WAR'
          stl:PartRecordNumber = wpr:Record_Number
          IF (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
              stl:PartRecordNumber = par:Record_Number
              stl:parttype = 'CHA'
              Access:STOCK.TryUpdate()
          END
          Access:WARPARTS.DeleteRecord(0)
      END
          
      p_web.SSV('locWarrantyJob','NO')
      p_web.SSV('locChargeableJob','YES')
              
  OF 'WARTOSPLIT' !Make Warranty From Chargeable 
          ! Transfer Chargeable Parts
      Access:PARTS.ClearKey(par:Part_Number_Key)
      par:Ref_Number = p_web.GSV('job:ref_number')
      SET(par:Part_Number_Key,par:Part_Number_Key)
      LOOP
          IF (Access:PARTS.Next())
              BREAK
          END
          IF (par:Ref_Number <> p_web.GSV('job:ref_number'))
              BREAK
          END
          IF (Access:WARPARTS.PrimeRecord() = Level:Benign)
              recordNo# = wpr:Record_Number
              wpr:Record :=: par:Record
              wpr:Record_Number = recordNo#
              IF (Access:WARPARTS.TryInsert())
                  Access:WARPARTS.CancelAutoInc()
              END
          END
              ! Is this part in stock allocation?
          Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
          stl:PartType = 'CHA'
          stl:PartRecordNumber = par:Record_Number
          IF (Access:STOCKALL.Tryfetch(stl:PartRecordTypeKey) = Level:Benign)
              stl:PartRecordNumber = wpr:Record_Number
              stl:PartType = 'WAR'
              Access:STOCKALL.TryUpdate()
          END
          Access:PARTS.DeleteRecord()
      END
      p_web.SSV('locChargeableJob','NO')
      p_web.SSV('locWarrantyJob','YES')
                  
  END
  
  p_web.SSV('job:Chargeable_Job',p_web.GSV('locChargeableJob'))
  p_web.SSV('job:Warranty_Job',p_web.GSV('locWarrantyJob'))
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END!
  
  do resetScreen
  
  
  
  do Value::buttonConfirm
  do SendAlert
  do Value::buttonCancel  !1
  do Value::locConfirmationText  !1
  do Value::buttonCancel  !1
  do Value::buttonSplitJob  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1

Value::buttonConfirm  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonConfirm') & '_value',Choose(p_web.GSV('Hide:ConfirmButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ConfirmButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirm'',''setjobtype_buttonconfirm_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmChange',p_web.GSV('ConfirmButtonText'),'MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('buttonConfirm') & '_value')

Comment::buttonConfirm  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonConfirm') & '_comment',Choose(p_web.GSV('Hide:ConfirmButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ConfirmButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonSplitJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonSplitJob',p_web.GetValue('NewValue'))
    do Value::buttonSplitJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Split Job
      ! Confirm change
  p_web.SSV('locChargeableJob','YES')
  p_web.SSV('locWarrantyJob','YES')
  p_web.SSV('job:chargeable_job','YES')
  p_web.SSV('job:warranty_Job','YES')
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END!
  
  do resetScreen
  do Value::buttonSplitJob
  do SendAlert
  do Value::buttonConfirm  !1
  do Value::buttonCancel  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1

Value::buttonSplitJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_value',Choose(p_web.GSV('Hide:SplitJobButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJobButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonSplitJob'',''setjobtype_buttonsplitjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','SplitJob','Split Job','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_value')

Comment::buttonSplitJob  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_comment',Choose(p_web.GSV('Hide:SplitJobButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SplitJobButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCancel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCancel',p_web.GetValue('NewValue'))
    do Value::buttonCancel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Cancel Change
      DO resetScreen
  do Value::buttonCancel
  do SendAlert
  do Value::buttonConfirm  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Prompt::locChargeableJob
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1
  do Value::buttonSplitJob  !1

Value::buttonCancel  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonCancel') & '_value',Choose(p_web.GSV('Hide:CancelButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CancelButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancel'',''setjobtype_buttoncancel_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Cancel','Cancel','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('buttonCancel') & '_value')

Comment::buttonCancel  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonCancel') & '_comment',Choose(p_web.GSV('Hide:CancelButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CancelButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('SetJobType_locChargeableJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locChargeableJob
      else
        do Value::locChargeableJob
      end
  of lower('SetJobType_locCChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCChargeType
      else
        do Value::locCChargeType
      end
  of lower('SetJobType_locCRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCRepairType
      else
        do Value::locCRepairType
      end
  of lower('SetJobType_locWarrantyJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyJob
      else
        do Value::locWarrantyJob
      end
  of lower('SetJobType_locWChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWChargeType
      else
        do Value::locWChargeType
      end
  of lower('SetJobType_locWRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWRepairType
      else
        do Value::locWRepairType
      end
  of lower('SetJobType_buttonConfirm_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirm
      else
        do Value::buttonConfirm
      end
  of lower('SetJobType_buttonSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonSplitJob
      else
        do Value::buttonSplitJob
      end
  of lower('SetJobType_buttonCancel_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancel
      else
        do Value::buttonCancel
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_SetJobType',0)

PreCopy  Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SetJobType',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('SetJobType:Primed',0)

PreDelete       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('SetJobType:Primed',0)
  p_web.setsessionvalue('showtab_SetJobType',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
        If (p_web.GSV('ReadOnly:ChargeableJob')= 1)
          If p_web.IfExistsValue('locChargeableJob') = 0
            p_web.SetValue('locChargeableJob','NO')
            locChargeableJob = 'NO'
          End
        End
        If (p_web.GSV('ReadOnly:WarrantyJob') = 1)
          If p_web.IfExistsValue('locWarrantyJob') = 0
            p_web.SetValue('locWarrantyJob','NO')
            locWarrantyJob = 'NO'
          End
        End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SetJobType_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      IF (p_web.GSV('job:Warranty_Job') <> 'YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Repair_Type_Warranty','')
      END
      IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
          p_web.SSV('job:Charge_Type','')
          p_web.SSV('job:Repair_Type','')
      END
  
      DO deleteSessionValues
  p_web.DeleteSessionValue('SetJobType_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('SetJobType:Primed',0)
  p_web.StoreValue('locChargeableJob')
  p_web.StoreValue('locCChargeType')
  p_web.StoreValue('locCRepairType')
  p_web.StoreValue('locWarrantyJob')
  p_web.StoreValue('locWChargeType')
  p_web.StoreValue('locWRepairType')
  p_web.StoreValue('locErrorText')
  p_web.StoreValue('locConfirmationText')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
locClass.validateWarrantyParts      PROCEDURE()
    CODE
            
        found# = 0
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = p_web.GSV('job:ref_Number')
        SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
        LOOP
            IF (Access:WARPARTS.Next())
                BREAK
            END
            IF (wpr:Ref_Number <> p_web.GSV('job:ref_number'))
                BREAK
            END
            found# = 1
            BREAK
        END
        
        return found#
        
locClass.validateChargeableParts    PROCEDURE()
    CODE
        found# = 0
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = p_web.gsv('job:Ref_number')
        SET(par:Part_Number_Key,par:Part_Number_Key)
        LOOP
            IF (Access:PARTS.Next())
                BREAK
            END
            IF (par:Ref_Number <> p_web.gsv('job:ref_Number'))
                BREAK
            END
            found# = 1
            BREAK
        END

        IF (p_web.gsv('job:estimate') = 'YES')
            found# = 2
        END
        
        RETURN found#
                
SelectAccessJobStatus PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(ACCSTAT)
                      Project(acs:RecordNumber)
                      Project(acs:Status)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('SelectAccessJobStatus')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('SelectAccessJobStatus:NoForm')
      loc:NoForm = p_web.GetValue('SelectAccessJobStatus:NoForm')
      loc:FormName = p_web.GetValue('SelectAccessJobStatus:FormName')
    else
      loc:FormName = 'SelectAccessJobStatus_frm'
    End
    p_web.SSV('SelectAccessJobStatus:NoForm',loc:NoForm)
    p_web.SSV('SelectAccessJobStatus:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('SelectAccessJobStatus:NoForm')
    loc:FormName = p_web.GSV('SelectAccessJobStatus:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('SelectAccessJobStatus') & '_' & lower(loc:parent)
  else
    loc:divname = lower('SelectAccessJobStatus')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ACCSTAT,acs:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'ACS:STATUS') then p_web.SetValue('SelectAccessJobStatus_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('SelectAccessJobStatus:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('SelectAccessJobStatus:LookupFrom','LookupFrom')
    p_web.StoreValue('SelectAccessJobStatus:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('SelectAccessJobStatus:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('SelectAccessJobStatus:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('SelectAccessJobStatus_sort',net:DontEvaluate)
  p_web.SetSessionValue('SelectAccessJobStatus_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    Loc:LocateField = ''
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(acs:Status)','-UPPER(acs:Status)')
    Loc:LocateField = 'acs:Status'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(acs:AccessArea),+UPPER(acs:Status)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('acs:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('SelectAccessJobStatus_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('SelectAccessJobStatus:LookupFrom')
  End!Else
  loc:formaction = 'SelectAccessJobStatus'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="SelectAccessJobStatus:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="SelectAccessJobStatus:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('SelectAccessJobStatus:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ACCSTAT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="acs:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Status') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Status',0)&'</span>'&CRLF
  End
  If clip('Select Status') <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2SelectAccessJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectAccessJobStatus.locate(''Locator2SelectAccessJobStatus'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectAccessJobStatus.cl(''SelectAccessJobStatus'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="SelectAccessJobStatus_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="SelectAccessJobStatus_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','SelectAccessJobStatus','Status','Click here to sort by Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('acs:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and ACCSTAT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'acs:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('acs:RecordNumber'),p_web.GetValue('acs:RecordNumber'),p_web.GetSessionValue('acs:RecordNumber'))
      loc:FilterWas = 'Upper(acs:AccessArea) = Upper(<39>' & p_web.GSV('filter:AccessLevel') & '<39>)'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('SelectAccessJobStatus_Filter')
    p_web.SetSessionValue('SelectAccessJobStatus_FirstValue','')
    p_web.SetSessionValue('SelectAccessJobStatus_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ACCSTAT,acs:RecordNumberKey,loc:PageRows,'SelectAccessJobStatus',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ACCSTAT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ACCSTAT,loc:firstvalue)
              Reset(ThisView,ACCSTAT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ACCSTAT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ACCSTAT,loc:lastvalue)
            Reset(ThisView,ACCSTAT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acs:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectAccessJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectAccessJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectAccessJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectAccessJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'SelectAccessJobStatus',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('SelectAccessJobStatus_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('SelectAccessJobStatus_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1SelectAccessJobStatus',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="SelectAccessJobStatus.locate(''Locator1SelectAccessJobStatus'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'SelectAccessJobStatus.cl(''SelectAccessJobStatus'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('SelectAccessJobStatus_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('SelectAccessJobStatus_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'SelectAccessJobStatus.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'SelectAccessJobStatus.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'SelectAccessJobStatus.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'SelectAccessJobStatus.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = acs:RecordNumber
    p_web._thisrow = p_web._nocolon('acs:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('SelectAccessJobStatus:LookupField')) = acs:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((acs:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="SelectAccessJobStatus.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ACCSTAT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ACCSTAT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ACCSTAT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ACCSTAT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acs:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="SelectAccessJobStatus.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acs:RecordNumber',clip(loc:field),,'checked',,,'onclick="SelectAccessJobStatus.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::acs:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="SelectAccessJobStatus.omv(this);" onMouseOut="SelectAccessJobStatus.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var SelectAccessJobStatus=new browseTable(''SelectAccessJobStatus'','''&clip(loc:formname)&''','''&p_web._jsok('acs:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('acs:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'SelectAccessJobStatus.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'SelectAccessJobStatus.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectAccessJobStatus')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectAccessJobStatus')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1SelectAccessJobStatus')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2SelectAccessJobStatus')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ACCSTAT)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ACCSTAT)
  Bind(acs:Record)
  Clear(acs:Record)
  NetWebSetSessionPics(p_web,ACCSTAT)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('acs:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&acs:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'SelectAccessJobStatus',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::acs:Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('acs:Status_'&acs:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(acs:Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = acs:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('acs:RecordNumber',acs:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('acs:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acs:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acs:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SecurityCheckFailed  PROCEDURE  (f:Password,f:Area)        ! Declare Procedure
USERS::State  USHORT
ACCAREAS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 1

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = f:Password
    If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
        acc:User_Level = use:User_Level
        acc:Access_Area = f:Area
        If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
            Return# = 0
        End ! If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
    End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  USERS::State = Access:USERS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  ACCAREAS::State = Access:ACCAREAS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF USERS::State <> 0
    Access:USERS.RestoreFile(USERS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ACCAREAS::State <> 0
    Access:ACCAREAS.RestoreFile(ACCAREAS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:ACCAREAS.Close
     FilesOpened = False
  END
saveJob              PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    Access:WEBJOB.Clearkey(wob:refNumberKey)
    wob:refNumber    = p_web.GSV('wob:ref_Number')
    if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(WEBJOB)
        access:WEBJOB.tryUpdate()
    else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

    Access:JOBS.Clearkey(job:ref_Number_Key)
    job:ref_Number    = p_web.GSV('wob:RefNumber')
    if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBS)
        access:JOBS.tryupdate()
    else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE)
        access:JOBSE.tryupdate()
    else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)


    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBSE2)
        access:JOBSE2.tryupdate()
    else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)


    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber    = p_web.GSV('wob:RefNumber')
    if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Found
        p_web.SessionQueueToFile(JOBNOTES)
        if access:JOBNOTES.tryupdate()
        end
    else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Error
    end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS.Close
     Access:JOBSE.Close
     Access:JOBNOTES.Close
     Access:JOBSE2.Close
     Access:WEBJOB.Close
     FilesOpened = False
  END
ReceiptFromPUP       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locIMEINumber        STRING(30)                            !
locMSN               STRING(30)                            !
locNetwork           STRING(30)                            !
locInFault           STRING(30)                            !
locIMEIMessage       STRING(100)                           !
locPassword          STRING(30)                            !
locValidationMessage STRING(100)                           !
FilesOpened     Long
WEBJOB::State  USHORT
NETWORKS::State  USHORT
MANFAULT::State  USHORT
TagFile::State  USHORT
JOBACC::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ReceiptFromPUP')
  loc:formname = 'ReceiptFromPUP_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ReceiptFromPUP','')
    p_web._DivHeader('ReceiptFromPUP',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
deleteVariables         routine
    p_web.deleteSessionValue('locIMEINumber')
    p_web.deleteSessionValue('locIMEIMessage')
    p_web.deleteSessionValue('locIMEIAccepted')
    p_web.deleteSessionValue('locMSN')
    p_web.deleteSessionValue('locNetwork')
    p_web.deleteSessionValue('locInFault')
    p_web.deleteSessionValue('locPassword')
    p_web.deleteSessionValue('PasswordRequired')
    p_web.deleteSessionValue('Comment:IMEINumber')
    p_web.deleteSessionValue('Comment:Password')
    p_web.deleteSessionValue('tmp:LoanModelNumber')
    p_web.deleteSessionValue('locValidationMessage')
saveChanges         routine
    data
locNotes        String(255)
    code
        if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
            locNotes = '<13,10>OLD I.M.E.I.: ' & p_web.GSV('job:ESN') & |
                '<13,10>NEW I.M.E.I.: ' & p_web.GSV('locIMEINumber')
            p_web.SSV('job:ESN',p_web.GSV('locIMEINumber'))
        end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))

        if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD M.S.N.: ' & p_web.GSV('job:MSN') & |
                '<13,10>NEW M.S.N.: ' & p_web.GSV('locMSN')
            p_web.SSV('job:MSN',p_web.GSV('locMSN'))
        end ! if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))

        if (p_web.GSV('locNetwork') <> p_web.GSV('jobe:Network'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD NETWORK: ' & p_web.GSV('jobe:Network') & |
                '<13,10>NEW NETWORK: ' & p_web.GSV('locNetwork')
            p_web.SSV('jobe:Network',p_web.GSV('locNetwork'))
        end ! if (p_web.GSV('locNetwork') <> p_web.GSV('job:Network'))

        Access:MANFAULT.Clearkey(maf:inFaultKey)
        maf:manufacturer    = p_web.GSV('job:manufacturer')
        maf:inFault    = 1
        if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)

        if (maf:Field_Number < 13)
            if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('job:Fault_Code' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        else ! if (maf:Field_Number < 13)
            if (p_web.GSV('wob:FaultCode' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('wob:FaultCode' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        end ! if (maf:Field_Number < 13)

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Notes','DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','PUP VALIDATION')
            addToAudit(p_web)
        end ! if (clip(locNotes) <> '')

        locNotes = ''
        if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1))
            locNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'
            Access:JOBACC.Clearkey(jac:ref_Number_Key)
            jac:ref_Number    = p_web.GSV('job:Ref_Number')
            set(jac:ref_Number_Key,jac:ref_Number_Key)
            loop
                if (Access:JOBACC.Next())
                    Break
                end ! if (Access:JOBACC.Next())
                if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                    Break
                end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(jac:accessory)
            end ! loop

            locNotes = clip(locNotes) & |
                '<13,10,13,0>ACCESSORIES RECEIVED:-'

            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(tag:taggedValue)
            end ! loop

        end ! if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1)

        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Notes',clip(locNotes))
        p_web.SSV('AddToAudit:Action','UNIT RECEIVED AT ' & p_web.GSV('BookingSite') & ' FROM PUP')
        addToAudit(p_web)

        p_web.SSV('LocationChange:Location',p_web.GSV('Default:' & p_web.GSV('BookingSite') & 'Location'))
        locationChange(p_web)

        p_web.SSV('job:Workshop','YES')

        p_web.SSV('GetStatus:Type','JOB')
        p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('Default:StatusReceivedFromPUP'),1,3))
        getStatus(p_web)

        Access:JOBS.Clearkey(job:ref_Number_Key)
        job:ref_Number    = p_web.GSV('job:Ref_Number')
        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBS)
            access:JOBS.tryUpdate()
            updateDateTimeStamp(job:Ref_Number)
        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = p_web.GSV('job:Ref_Number')
        if (Access:WEBJOB.TryFetch(wob:RefNumberKey)= Level:Benign)
            p_web.SessionQueueToFile(WEBJOB)
            access:WEBJOB.TryUpdate()
        END
        
        Access:JOBSE.Clearkey(jobe:refNumberKey)
        jobe:refNumber    = p_web.GSV('job:Ref_Number')
        if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBSE)
            access:JOBSE.tryUpdate()
        else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ReceiptFromPUP_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do DeleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locNetwork'
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  p_web.SetSessionValue('locPassword',locPassword)
  p_web.SetSessionValue('locMSN',locMSN)
  p_web.SetSessionValue('locNetwork',locNetwork)
  p_web.SetSessionValue('locInFault',locInFault)
  p_web.SetSessionValue('locValidationMessage',locValidationMessage)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locIMEIMessage')
    locIMEIMessage = p_web.GetValue('locIMEIMessage')
    p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  End
  if p_web.IfExistsValue('locPassword')
    locPassword = p_web.GetValue('locPassword')
    p_web.SetSessionValue('locPassword',locPassword)
  End
  if p_web.IfExistsValue('locMSN')
    locMSN = p_web.GetValue('locMSN')
    p_web.SetSessionValue('locMSN',locMSN)
  End
  if p_web.IfExistsValue('locNetwork')
    locNetwork = p_web.GetValue('locNetwork')
    p_web.SetSessionValue('locNetwork',locNetwork)
  End
  if p_web.IfExistsValue('locInFault')
    locInFault = p_web.GetValue('locInFault')
    p_web.SetSessionValue('locInFault',locInFault)
  End
  if p_web.IfExistsValue('locValidationMessage')
    locValidationMessage = p_web.GetValue('locValidationMessage')
    p_web.SetSessionValue('locValidationMessage',locValidationMessage)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ReceiptFromPUP_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !INIT
      if (MSNRequired(p_web.GSV('job:Manufacturer')))
          p_web.SSV('Hide:MSN',0)
      else
          p_web.SSV('Hide:MSN',1)
      end ! if (MSNRequired(p_web.GSV('job:Manufacturer')))
  
      p_web.SSV('Comment:IMEINumber','Enter I.M.E.I. and press [TAB] to accept')
  
      ! Used for the tag browse
      p_web.SSV('tmp:LoanModelNumber',p_web.GSV('job:Model_Number'))
  
      clearTagFile(p_web)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locIMEIMessage = p_web.RestoreValue('locIMEIMessage')
 locPassword = p_web.RestoreValue('locPassword')
 locMSN = p_web.RestoreValue('locMSN')
 locNetwork = p_web.RestoreValue('locNetwork')
 locInFault = p_web.RestoreValue('locInFault')
 locValidationMessage = p_web.RestoreValue('locValidationMessage')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ReceiptFromPUP_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ReceiptFromPUP_ChainTo')
    loc:formaction = p_web.GetSessionValue('ReceiptFromPUP_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ReceiptFromPUP" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ReceiptFromPUP" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ReceiptFromPUP" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Receipt From PUP') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Receipt From PUP',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ReceiptFromPUP">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ReceiptFromPUP" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ReceiptFromPUP')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('PUP Unit Validation') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ReceiptFromPUP')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ReceiptFromPUP'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ReceiptFromPUP')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('PUP Unit Validation') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEIMessage
      do Value::locIMEIMessage
      do Comment::locIMEIMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:MSN') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNetwork
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInFault
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonValidateAccessories
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locValidationMessage
      do Comment::locValidationMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locIMEINumber  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
      if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
          p_web.SSV('locIMEIMessage','IMEI Number does not match IMEI from booking. Enter password to accept change and continue.')
          p_web.SSV('PasswordRequired',1)
  !        p_web.SSV('locIMEIAccepted',0)
      else!
  !        p_web.SSV('locIMEIAccepted',1)
          p_web.SSV('PasswordRequired',0)
          p_web.SSV('locIMEIMessage','')
      end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Value::locPassword  !1
  do Comment::locPassword
  do Prompt::locInFault
  do Value::locInFault  !1
  do Prompt::locMSN
  do Value::locMSN  !1
  do Prompt::locNetwork
  do Value::locNetwork  !1
  do Value::locIMEIMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''receiptfrompup_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:IMEINumber'))
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment')

Prompt::locIMEIMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_prompt',Choose(p_web.GSV('locIMEIMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locIMEIMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEIMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEIMessage',p_web.GetValue('NewValue'))
    locIMEIMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEIMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEIMessage',p_web.GetValue('Value'))
    locIMEIMessage = p_web.GetValue('Value')
  End

Value::locIMEIMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value',Choose(p_web.GSV('locIMEIMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locIMEIMessage') = '')
  ! --- DISPLAY --- locIMEIMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIMEIMessage'),0) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value')

Comment::locIMEIMessage  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_comment',Choose(p_web.GSV('locIMEIMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locIMEIMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPassword  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('PasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt')

Validate::locPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPassword',p_web.GetValue('NewValue'))
    locPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPassword',p_web.GetValue('Value'))
    locPassword = p_web.GetValue('Value')
  End
  If locPassword = ''
    loc:Invalid = 'locPassword'
    loc:alert = p_web.translate('Enter Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locPassword = Upper(locPassword)
    p_web.SetSessionValue('locPassword',locPassword)
  Access:users.Clearkey(use:Password_Key)
  use:Password    = p_web.GSV('locPassword')
  if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Found
      Access:ACCAREAS.Clearkey(acc:access_Level_Key)
      acc:user_Level    = use:user_Level
      acc:access_Area    = 'PUP RECEIPT - CHANGE IMEI'
      if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Found
          p_web.SSV('Comment:Password','Password Accepted')
      else ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Error
          p_web.SSV('Comment:Password','You do not have access to change the IMEI')
          p_web.SSV('locPassword','')
      end ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
  else ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Error
      p_web.SSV('Comment:Password','Invalid Password')
      p_web.SSV('locPassword','')
  
  end ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locPassword
  do SendAlert
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Comment::locPassword

Value::locPassword  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('PasswordRequired') <> 1)
  ! --- STRING --- locPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPassword'',''receiptfrompup_locpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPassword',p_web.GetSessionValueFormat('locPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value')

Comment::locPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:Password'))
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('PasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment')

Prompt::locMSN  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt')

Validate::locMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMSN',p_web.GetValue('NewValue'))
    locMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMSN',p_web.GetValue('Value'))
    locMSN = p_web.GetValue('Value')
  End
  If locMSN = ''
    loc:Invalid = 'locMSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locMSN = Upper(locMSN)
    p_web.SetSessionValue('locMSN',locMSN)
  do Value::locMSN
  do SendAlert

Value::locMSN  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMSN'',''receiptfrompup_locmsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMSN',p_web.GetSessionValueFormat('locMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value')

Comment::locMSN  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNetwork  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt')

Validate::locNetwork  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('NewValue'))
    locNetwork = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNetwork
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('Value'))
    locNetwork = p_web.GetValue('Value')
  End
  If locNetwork = ''
    loc:Invalid = 'locNetwork'
    loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNetwork = Upper(locNetwork)
    p_web.SetSessionValue('locNetwork',locNetwork)
  p_Web.SetValue('lookupfield','locNetwork')
  do AfterLookup
  do Value::locNetwork
  do SendAlert
  do Comment::locNetwork

Value::locNetwork  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNetwork
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNetwork')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNetwork = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNetwork'',''receiptfrompup_locnetwork_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNetwork')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locNetwork',p_web.GetSessionValueFormat('locNetwork'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=locNetwork&Tab=2&ForeignField=net:Network&_sort=net:Network&Refresh=sort&LookupFrom=ReceiptFromPUP&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value')

Comment::locNetwork  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment')

Prompt::locInFault  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('In Fault')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt')

Validate::locInFault  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInFault',p_web.GetValue('NewValue'))
    locInFault = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInFault
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInFault',p_web.GetValue('Value'))
    locInFault = p_web.GetValue('Value')
  End
  If locInFault = ''
    loc:Invalid = 'locInFault'
    loc:alert = p_web.translate('In Fault') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locInFault = Upper(locInFault)
    p_web.SetSessionValue('locInFault',locInFault)
  do Value::locInFault
  do SendAlert

Value::locInFault  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locInFault
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locInFault')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locInFault = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInFault'',''receiptfrompup_locinfault_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInFault',p_web.GetSessionValueFormat('locInFault'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
          Access:MANFAULT.Clearkey(maf:InFaultKey)
          maf:manufacturer    = p_web.GSV('job:manufacturer')
          maf:inFault    = 1
          if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Found
              packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                          '?LookupField=locInFault&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                          'sort&LookupFrom=ReceiptFromPUP&' & |
                          'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
  
          else ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
  
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value')

Comment::locInFault  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','ReceiptFromPUP')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !Validate Accessories
      error# = 0
  
      Access:JOBACC.Clearkey(jac:ref_Number_Key)
      jac:ref_Number    = p_web.GSV('job:Ref_Number')
      set(jac:ref_Number_Key,jac:ref_Number_Key)
      loop
          if (Access:JOBACC.Next())
              Break
          end ! if (Access:JOBACC.Next())
          if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
          Access:TAGFILE.Clearkey(tag:keyTagged)
          tag:sessionID    = p_web.sessionID
          tag:taggedValue  = jac:accessory
          if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
              ! Found
              if (tag:tagged <> 1)
                  error# = 1
                  break
              end ! if (tag:tagged <> 1)
          else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
              ! Error
              error# = 1
              break
          end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  
      end ! loop
  
      if (error# = 0)
          Access:TAGFILE.Clearkey(tag:keyTagged)
          tag:sessionID    = p_web.sessionID
          set(tag:keyTagged,tag:keyTagged)
          loop
              if (Access:TAGFILE.Next())
                  Break
              end ! if (Access:TAGFILE.Next())
              if (tag:sessionID    <> p_web.sessionID)
                  Break
              end ! if (tag:sessionID    <> p_web.sessionID)
              if (tag:Tagged = 0)
                  cycle
              end ! if (tag:Tagged = 0)
  
              Access:JOBACC.Clearkey(jac:ref_Number_Key)
              jac:ref_Number    = p_web.GSV('job:Ref_Number')
              jac:accessory    = tag:taggedValue
              if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Found
              else ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Error
                  error# = 1
                  break
              end ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
  
          end ! loop
      end ! if (error# = 0)
  
      case error#
      of 1
          p_web.SSV('locValidationMessage','<span class="RedBold">Accessories Validated. There is a mismatch.</span>')
      else
          p_web.SSV('locValidationMessage','<span class="GreenBold">Accessories Validated</span>')
      end
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::locValidationMessage  !1
  do Value::locValidationMessage  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''receiptfrompup_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locValidationMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locValidationMessage',p_web.GetValue('NewValue'))
    locValidationMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locValidationMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locValidationMessage',p_web.GetValue('Value'))
    locValidationMessage = p_web.GetValue('Value')
  End

Value::locValidationMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value',Choose(p_web.GSV('locValidationMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locValidationMessage') = '')
  ! --- DISPLAY --- locValidationMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locValidationMessage'),1) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value')

Comment::locValidationMessage  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_comment',Choose(p_web.GSV('locValidationMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locValidationMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ReceiptFromPUP_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('ReceiptFromPUP_locPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPassword
      else
        do Value::locPassword
      end
  of lower('ReceiptFromPUP_locMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMSN
      else
        do Value::locMSN
      end
  of lower('ReceiptFromPUP_locNetwork_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNetwork
      else
        do Value::locNetwork
      end
  of lower('ReceiptFromPUP_locInFault_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInFault
      else
        do Value::locInFault
      end
  of lower('ReceiptFromPUP_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)

PreCopy  Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)

PreDelete       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validation
      if (p_web.GSV('locValidationMessage') = '')
          loc:Alert = 'You must validate the accessories'
          loc:invalid = 'buttonValidateAccessories'
          exit
      end ! if (p_web.GSV('locValidationMessage') = '')
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('PasswordRequired') <> 1)
        If locPassword = ''
          loc:Invalid = 'locPassword'
          loc:alert = p_web.translate('Enter Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locPassword = Upper(locPassword)
          p_web.SetSessionValue('locPassword',locPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 1
    loc:InvalidTab += 1
    If p_web.GSV('Hide:MSN') <> 1
        If locMSN = ''
          loc:Invalid = 'locMSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locMSN = Upper(locMSN)
          p_web.SetSessionValue('locMSN',locMSN)
        If loc:Invalid <> '' then exit.
    End
        If locNetwork = ''
          loc:Invalid = 'locNetwork'
          loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNetwork = Upper(locNetwork)
          p_web.SetSessionValue('locNetwork',locNetwork)
        If loc:Invalid <> '' then exit.
        If locInFault = ''
          loc:Invalid = 'locInFault'
          loc:alert = p_web.translate('In Fault') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locInFault = Upper(locInFault)
          p_web.SetSessionValue('locInFault',locInFault)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locIMEIMessage')
  p_web.StoreValue('locPassword')
  p_web.StoreValue('locMSN')
  p_web.StoreValue('locNetwork')
  p_web.StoreValue('locInFault')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidationMessage')
  !Post Update
  do saveChanges


  do deleteVariables
MSNRequired          PROCEDURE  (func:Manufacturer)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:Use_MSN = 'YES'
            Return# = 1
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles

    Return Return#
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     FilesOpened = False
  END
PickLoanUnit         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:LoanUnitNumber   LONG                                  !
tmp:UnitDetails      STRING(100)                           !
tmp:LoanIMEINumber   STRING(30)                            !
tmp:LoanModelNumber  STRING(30)                            !
tmp:MSN              STRING(30)                            !
tmp:LoanUnitDetails  STRING(60)                            !
tmp:LoanAccessories  STRING(100)                           !
tmp:LoanLocation     STRING(100)                           !
tmp:ReplacementValue REAL                                  !
locLoanAlertMessage  STRING(255)                           !
tmp:RemovalReason    BYTE                                  !
locRemovalAlertMessage STRING(255)                         !
tmp:LostLoanFee      REAL                                  !
locValidateMessage   STRING(255)                           !
locLoanIDOption      BYTE                                  !
locLoanIDAlert       STRING(100)                           !
locSMSDate           STRING(20)                            !
FilesOpened     Long
JOBSE2::State  USHORT
COURIER::State  USHORT
JOBS::State  USHORT
LOAN::State  USHORT
JOBEXACC::State  USHORT
LOAN_ALIAS::State  USHORT
EXCHOR48::State  USHORT
MANUFACT::State  USHORT
PARTS::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
STOCKTYP::State  USHORT
EXCHHIST::State  USHORT
PRODCODE::State  USHORT
JOBSE::State  USHORT
TagFile::State  USHORT
LOANACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
job:Loan_Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
local       class
AllocateLoanPart    Procedure(String func:Status,Byte func:SecondUnit)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickLoanUnit')
  loc:formname = 'PickLoanUnit_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickLoanUnit','')
    p_web._DivHeader('PickLoanUnit',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
addLoanUnit         Routine
    data
locAuditNotes   String(255)
    code
        Access:Loan.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
        if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:QALoan)
                    loa:Available = 'QA1'
                else ! if (man:QALoanLoan)
                    loa:Available = 'EXC'
                end !if (man:QALoanLoan)
                loa:Job_Number = p_web.GSV('job:Ref_Number')
                access:Loan.tryUpdate()

                if (Access:loanhist.PrimeRecord() = Level:Benign)
                    loh:Ref_Number    = tmp:LoanUnitNumber
                    loh:Date    = Today()
                    loh:Time    = Clock()
                    loh:User    = p_web.GSV('BookingUserCode')
                    if (man:QALoan)
                        loh:Status    = 'AWAITING QA. LOANED ON JOB NO: ' & p_web.GSV('job:ref_number')
                    else ! if (man:QALoanLoan)
                        loh:status = 'UNIT LOANED ON JOB NO: ' & p_web.GSV('job:Ref_number')
                    end !

                    if (Access:loanhist.TryInsert() = Level:Benign)
                        ! Inserted
                    else ! if (Access:loanhist.TryInsert() = Level:Benign)
                        ! Error
                        Access:loanhist.CancelAutoInc()
                    end ! if (Access:loanhist.TryInsert() = Level:Benign)
                end ! if (Access:loanhist.PrimeRecord() = Level:Benign)

                locAuditNotes = 'UNIT NUMBER: ' & CLip(loa:ref_number) & |
                    '<13,10>MODEL NUMBER: ' & CLip(loa:model_number) & |
                    '<13,10>I.M.E.I.: ' & CLip(loa:esn)

                if (MSNRequired(loa:Manufacturer))
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(loa:MSN)
                end ! if (MSNRequired(loa:Manufacturer))

                locAuditNotes = clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(loa:Stock_Type)

                p_web.SSV('AddToAudit:Type','LOA')
                p_web.SSV('AddToAudit:Action','LOAN UNIT ATTACHED TO JOB')
                p_web.SSV('AddToAudit:Notes',clip(locAuditNotes))
                addToAudit(p_web)

                if (man:QALoan)
                    p_web.SSV('job:Despatched','')
                    p_web.SSV('job:DespatchType','')
                    p_web.SSV('GetStatus:StatusNumber',605)
                    p_web.SSV('GetStatus:Type','LOA')
                    getStatus(p_web)
                else ! if (man:QALoanLoan)

                    if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('jobe:DespatchType','LOA')
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('wob:ReadyToDespatch',1)
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Loan_Courier'))

                    else ! if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('job:Loan_User',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Loan_Despatched','')
                        p_web.SSV('job:Despatched','LAT')
                        p_web.SSV('job:DespatchType','LOA')
                    end ! if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('GetStatus:StatusNumber',105)
                    p_web.SSV('GetStatus:Type','LOA')
                    getStatus(p_web)

                end !if (man:QALoanLoan)

                p_web.SSV('job:Loan_unit_Number',p_web.GSV('tmp:LoanUnitNumber'))

                !               saveJob(p_web)

                loc:Alert = 'Loan Unit Added'

            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        else ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
clearLoanDetails        routine
    p_web.SSV('tmp:MSN','')
    p_web.SSV('tmp:LoanUnitDetails','')
    p_web.SSV('tmp:LoanLocation','')
    p_web.SSV('tmp:ReplacementValue','')
    p_web.SSV('tmp:LoanIMEINumber','')
    p_web.SSV('tmp:LoanUnitNumber','')
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('tmp:LoanAccessories','')
    p_web.SSV('tmp:LoanLocation','')


clearTagFile        routine

    clearTagFile(p_web)
deleteVariables     routine
    p_web.deleteSessionValue('PickLoan:FirstTime')
    p_web.deleteSessionValue('locLoanIDAlert')
    p_web.deleteSessionValue('locLoanIDOption')
    p_web.DeleteSessionValue('Hide:LoadIDAlert')
    p_web.DeleteSessionValue('ReadOnly:LoanDespatchDetails')
getLoanDetails      Routine
    p_web.SSV('locLoanAlertMessage','')

    do lookupLoanDetails


    Access:LOAN.Clearkey(loa:Ref_Number_Key)
    loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
    if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)

    if (loa:Model_Number <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locLoanAlertMessage','Warning! The selected Loan Unit has a different Model Number!')
    end ! if (xch:Model_Number <> p_web.GSV('job:Model_Number')

lookupLoanDetails       Routine
    Access:Loan.Clearkey(loa:Ref_Number_Key)
    loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
    if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign and loa:Ref_Number > 0)
        ! Found
        p_web.SSV('tmp:LoanIMEINumber',loa:ESN)
        p_web.SSV('tmp:MSN',loa:MSN)
        p_web.SSV('tmp:LoanUnitDetails',Clip(loa:Ref_Number) & ': ' & Clip(loa:Manufacturer) & ' ' & Clip(loa:Model_Number))
        p_web.SSV('tmp:LoanLocation',Clip(loa:Location) & ' / ' & Clip(loa:Stock_Type))
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)

    else ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Error
        p_web.SSV('tmp:LoanIMEINumber','')
        p_web.SSV('tmp:MSN','')
        p_web.SSV('tmp:LoanUnitDetails','')
        p_web.SSV('tmp:LoanLocation','')
    end ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBEXACC)
  p_web._OpenFile(LOAN_ALIAS)
  p_web._OpenFile(EXCHOR48)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(PRODCODE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(LOANACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(LOAN_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(LOANACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickLoanUnit_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do deleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ReplacementValue')
    p_web.SetPicture('tmp:ReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ReplacementValue','@n14.2')
  If p_web.IfExistsValue('job:Loan_Despatched')
    p_web.SetPicture('job:Loan_Despatched','@d06b')
  End
  p_web.SetSessionPicture('job:Loan_Despatched','@d06b')
  If p_web.IfExistsValue('tmp:LostLoanFee')
    p_web.SetPicture('tmp:LostLoanFee','@n14.2')
  End
  p_web.SetSessionPicture('tmp:LostLoanFee','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:LoanIMEINumber'
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOAN)
        p_web.setsessionvalue('tmp:LoanUnitNumber',loa:Ref_Number)
      do getLoanDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locLoanAlertMessage')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Loan_Courier'
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Loan_Consignment_Number')
  End
  If p_web.GSV('job:Loan_Unit_Number') > 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage)
  p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories)
  p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation)
  p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
  p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert)
  p_web.SetSessionValue('locLoanIDOption',locLoanIDOption)
  p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier)
  p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
  p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
  p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User)
  p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
  p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  p_web.SetSessionValue('locValidateMessage',locValidateMessage)
  p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('tmp:LoanIMEINumber')
    tmp:LoanIMEINumber = p_web.GetValue('tmp:LoanIMEINumber')
    p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  End
  if p_web.IfExistsValue('locLoanAlertMessage')
    locLoanAlertMessage = p_web.GetValue('locLoanAlertMessage')
    p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage)
  End
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  End
  if p_web.IfExistsValue('tmp:LoanUnitDetails')
    tmp:LoanUnitDetails = p_web.GetValue('tmp:LoanUnitDetails')
    p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  End
  if p_web.IfExistsValue('tmp:LoanAccessories')
    tmp:LoanAccessories = p_web.GetValue('tmp:LoanAccessories')
    p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories)
  End
  if p_web.IfExistsValue('tmp:LoanLocation')
    tmp:LoanLocation = p_web.GetValue('tmp:LoanLocation')
    p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation)
  End
  if p_web.IfExistsValue('tmp:ReplacementValue')
    tmp:ReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:ReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  End
  if p_web.IfExistsValue('jobe2:LoanIDNumber')
    jobe2:LoanIDNumber = p_web.GetValue('jobe2:LoanIDNumber')
    p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
  End
  if p_web.IfExistsValue('locLoanIDAlert')
    locLoanIDAlert = p_web.GetValue('locLoanIDAlert')
    p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert)
  End
  if p_web.IfExistsValue('locLoanIDOption')
    locLoanIDOption = p_web.GetValue('locLoanIDOption')
    p_web.SetSessionValue('locLoanIDOption',locLoanIDOption)
  End
  if p_web.IfExistsValue('job:Loan_Courier')
    job:Loan_Courier = p_web.GetValue('job:Loan_Courier')
    p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier)
  End
  if p_web.IfExistsValue('job:Loan_Consignment_Number')
    job:Loan_Consignment_Number = p_web.GetValue('job:Loan_Consignment_Number')
    p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
  End
  if p_web.IfExistsValue('job:Loan_Despatched')
    job:Loan_Despatched = p_web.dformat(clip(p_web.GetValue('job:Loan_Despatched')),'@d06b')
    p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
  End
  if p_web.IfExistsValue('job:Loan_Despatched_User')
    job:Loan_Despatched_User = p_web.GetValue('job:Loan_Despatched_User')
    p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User)
  End
  if p_web.IfExistsValue('job:Loan_Despatch_Number')
    job:Loan_Despatch_Number = p_web.GetValue('job:Loan_Despatch_Number')
    p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
  End
  if p_web.IfExistsValue('locRemovalAlertMessage')
    locRemovalAlertMessage = p_web.GetValue('locRemovalAlertMessage')
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  End
  if p_web.IfExistsValue('tmp:RemovalReason')
    tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  End
  if p_web.IfExistsValue('locValidateMessage')
    locValidateMessage = p_web.GetValue('locValidateMessage')
    p_web.SetSessionValue('locValidateMessage',locValidateMessage)
  End
  if p_web.IfExistsValue('tmp:LostLoanFee')
    tmp:LostLoanFee = p_web.dformat(clip(p_web.GetValue('tmp:LostLoanFee')),'@n14.2')
    p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickLoanUnit_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Security Checks
  
      if (p_web.GSV('PickLoan:FirstTime') = 0)
          if (p_web.GSV('Job:ViewOnly') = 1)
              p_web.SSV('ReadOnly:LoanIMEINumber',1)
          else !if (p_web.GSV('Job:ViewOnly') = 1)
              if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND LOAN UNIT'))
                  p_web.SSV('ReadOnly:LoanIMEINumber',1)
              else
                  p_web.SSV('ReadOnly:LoanIMEINumber',0)
              end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),' '))
          end !if (p_web.GSV('Job:ViewOnly') = 1)
  
          p_web.SSV('tmp:LoanUnitNumber',p_web.GSV('job:Loan_Unit_Number'))
  
          if (p_web.GSV('job:Loan_Unit_Number') > 0)
              do lookupLoanDetails
          else
              do clearLoanDetails
          end
  
          p_web.SSV('locLoanAlertMessage','')
          p_web.SSV('locRemoveAlertMessage','')
          p_web.SSV('Hide:RemovalReason',1)
          p_web.SSV('tmp:RemovalReason',0)
          p_web.SSV('tmp:NoUnitAvailable',0)
          p_web.SSV('locValidateMessage','')
          p_web.SSV('Hide:LostLoanFee',1)
          p_web.SSV('Hide:ConfirmRemovalButton',1)
          p_web.SSV('Hide:ValidateAccessoriesButton',0)
          p_web.SSV('locLoanIDOption',0)
          p_web.SSV('locLoanIDAlert','')
          p_web.SSV('Hide:LoanIDAlert',1)
  
          do clearTagFile
          p_web.SSV('PickLoan:FirstTime',1)
      end !if (p_web.GSV('PickLoan:FirstTime') = 0)
  
      if (p_web.GSV('job:Loan_Unit_Number') > 0 And ((p_web.GSV('BookingSite') = 'RRC' And p_web.GSV('jobe:Despatched') = 'REA' And p_web.GSV('jobe:DespatchedType') = 'EXC') Or |
          (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('job:Despatched') = 'REA' And p_web.GSV('job:Despatch_Type') = 'EXC')))
          packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("You cannot attached a Loan Unit until the Exchange Unit has been despatched.")</script>'
          do sendPacket
          p_web.SSV('ReadOnly:LoanIMEINumber',1)
      End !
  
      p_web.SSV('ReadOnly:LoanDespatchDetails',1)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:LoanIMEINumber = p_web.RestoreValue('tmp:LoanIMEINumber')
 locLoanAlertMessage = p_web.RestoreValue('locLoanAlertMessage')
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:LoanUnitDetails = p_web.RestoreValue('tmp:LoanUnitDetails')
 tmp:LoanAccessories = p_web.RestoreValue('tmp:LoanAccessories')
 tmp:LoanLocation = p_web.RestoreValue('tmp:LoanLocation')
 tmp:ReplacementValue = p_web.RestoreValue('tmp:ReplacementValue')
 locLoanIDAlert = p_web.RestoreValue('locLoanIDAlert')
 locLoanIDOption = p_web.RestoreValue('locLoanIDOption')
 locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
 tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
 locValidateMessage = p_web.RestoreValue('locValidateMessage')
 tmp:LostLoanFee = p_web.RestoreValue('tmp:LostLoanFee')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickLoanUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickLoanUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickLoanUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickLoanUnit" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickLoanUnit" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickLoanUnit" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Pick Loan Unit') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Pick Loan Unit',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickLoanUnit">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickLoanUnit" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickLoanUnit')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan ID Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Details') & ''''
        If p_web.GSV('job:Loan_Unit_Number') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Remove Loan Unit') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickLoanUnit')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickLoanUnit'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='LOAN'
            p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:LoanIDNumber')
    End
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          If p_web.GSV('job:Loan_Unit_Number') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab7'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickLoanUnit')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    if p_web.GSV('job:Loan_Unit_Number') > 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab7'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Loan Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLoanAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('ReadOnly:LoanIMEINumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPickLoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPickLoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('tmp:MSN') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ReplacementValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Loan ID Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:LoanIDNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:LoanIDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:LoanIDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanIDAlert
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanIDAlert
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLoanIDOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanIDOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanIDOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Consignment_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Despatched
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Despatched_User
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Despatch_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
  If p_web.GSV('job:Loan_Unit_Number') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Remove Loan Unit') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locRemoveText
      do Comment::locRemoveText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonRemoveAttachedUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRemovalAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="5" valign="top">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RemovalReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TagValidateLoanAccessories
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locValidateMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locValidateMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locValidateMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LostLoanFee
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LostLoanFee
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LostLoanFee
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonConfirmLoanRemoval
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmLoanRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmLoanRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab5  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel7">'&CRLF &|
                                    '  <div id="panel7Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel7Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_7">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab7" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab7">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab7">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab7">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:SMSHistory
      do Comment::link:SMSHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:SendSMS
      do Comment::link:SendSMS
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:ESN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUnitDetails',p_web.GetValue('NewValue'))
    do Value::locUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type')
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Warranty_Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanIMEINumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan IMEI No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanIMEINumber',p_web.GetValue('NewValue'))
    tmp:LoanIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanIMEINumber',p_web.GetValue('Value'))
    tmp:LoanIMEINumber = p_web.GetValue('Value')
  End
    tmp:LoanIMEINumber = Upper(tmp:LoanIMEINumber)
    p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  ! Validate IMEI
  p_web.SSV('locLoanAlertMessage','')
  Access:Loan.Clearkey(loa:ESN_Only_Key)
  loa:ESN    = p_web.GSV('tmp:LoanIMEINumber')
  if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
      ! Found
      if (loa:Location <> p_web.GSV('BookingSiteLocation'))
          p_web.SSV('locLoanAlertMessage','Selected IMEI is from a different location.')
      else !  !if (loa:Location <> p_web.GSV('BookingSiteLocation'))
          if (loa:Available = 'AVL')
              Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
              stp:Stock_Type    = loa:Stock_Type
              if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                  ! Found
                  if (stp:Available <> 1)
                      p_web.SSV('locLoanAlertMessage','Selected Stock Type is not available')
                  else ! if (stp:Available <> 1)
                      p_web.SSV('tmp:LoanUnitNumber',loa:Ref_Number)
                  end ! if (stp:Available <> 1)
              else ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                  ! Error
              end ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
          else ! if (loa:Available = 'AVL')
              p_web.SSV('locLoanAlertMessage','Selected IMEI is not available')
          end ! if (loa:Available = 'AVL')
      end !if (loa:Location <> p_web.GSV('BookingSiteLocation'))
  else ! if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
      ! Error
      p_web.SSV('locLoanAlertMessage','Cannot find the selected IMEI Number')
  end ! if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
  
  if (p_web.GSV('locLoanAlertMessage') = '')
      do getLoanDetails
  else
      do clearLoanDetails
  end ! if (p_web.GSV('locLoanAlertMessage') = '')
  p_Web.SetValue('lookupfield','tmp:LoanIMEINumber')
  do AfterLookup
  do Value::tmp:LoanIMEINumber
  do SendAlert
  do Comment::tmp:LoanIMEINumber
  do Prompt::locLoanAlertMessage
  do Value::locLoanAlertMessage  !1
  do Value::tmp:LoanLocation  !1
  do Value::tmp:LoanUnitDetails  !1
  do Value::tmp:ReplacementValue  !1

Value::tmp:LoanIMEINumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:LoanIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:LoanIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanIMEINumber'',''pickloanunit_tmp:loanimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanIMEINumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:LoanIMEINumber',p_web.GetSessionValueFormat('tmp:LoanIMEINumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('FormLoanUnitFilter')&'?LookupField=tmp:LoanIMEINumber&Tab=5&ForeignField=loa:ESN&_sort=&Refresh=sort&LookupFrom=PickLoanUnit&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_value')

Comment::tmp:LoanIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_comment')

Prompt::locLoanAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_prompt',Choose(p_web.GSV('locLoanAlertMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locLoanAlertMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_prompt')

Validate::locLoanAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanAlertMessage',p_web.GetValue('NewValue'))
    locLoanAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanAlertMessage',p_web.GetValue('Value'))
    locLoanAlertMessage = p_web.GetValue('Value')
  End

Value::locLoanAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_value',Choose(p_web.GSV('locLoanAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locLoanAlertMessage') = '')
  ! --- DISPLAY --- locLoanAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate(p_web.GSV('locLoanAlertMessage'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_value')

Comment::locLoanAlertMessage  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_comment',Choose(p_web.GSV('locLoanAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locLoanAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPickLoanUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPickLoanUnit',p_web.GetValue('NewValue'))
    do Value::buttonPickLoanUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPickLoanUnit
  do SendAlert

Value::buttonPickLoanUnit  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_value',Choose(1 or p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (1 or p_web.GSV('job:Loan_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPickLoanUnit'',''pickloanunit_buttonpickloanunit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PickLoanUnit','Pick Loan Unit','SmallButtonFixedIcon',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormLoanUnitFilter?LookupField=tmp:LoanIMEINumber&Tab=2&ForeignField=loa:ESN&_sort=loa:ESN&Refresh=sort&LookupFrom=PickLoanUnit&')) & ''','''&clip('_self')&''')',loc:javascript,0,'images\packinsert.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_value')

Comment::buttonPickLoanUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_comment',Choose(1 or p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If 1 or p_web.GSV('job:Loan_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('NewValue'))
    tmp:MSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('Value'))
    tmp:MSN = p_web.GetValue('Value')
  End

Value::tmp:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:MSN'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_value')

Comment::tmp:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanUnitDetails',p_web.GetValue('NewValue'))
    tmp:LoanUnitDetails = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanUnitDetails',p_web.GetValue('Value'))
    tmp:LoanUnitDetails = p_web.GetValue('Value')
  End

Value::tmp:LoanUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:LoanUnitDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanUnitDetails'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_value')

Comment::tmp:LoanUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanAccessories',p_web.GetValue('NewValue'))
    tmp:LoanAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanAccessories',p_web.GetValue('Value'))
    tmp:LoanAccessories = p_web.GetValue('Value')
  End

Value::tmp:LoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:LoanAccessories
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanAccessories'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_value')

Comment::tmp:LoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanLocation  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Location / Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanLocation',p_web.GetValue('NewValue'))
    tmp:LoanLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanLocation',p_web.GetValue('Value'))
    tmp:LoanLocation = p_web.GetValue('Value')
  End

Value::tmp:LoanLocation  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:LoanLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanLocation'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_value')

Comment::tmp:LoanLocation  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ReplacementValue  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Replacement Value')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ReplacementValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ReplacementValue',p_web.GetValue('NewValue'))
    tmp:ReplacementValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ReplacementValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ReplacementValue',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::tmp:ReplacementValue  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ReplacementValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('tmp:ReplacementValue'),'@n14.2')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value')

Comment::tmp:ReplacementValue  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:LoanIDNumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:LoanIDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:LoanIDNumber',p_web.GetValue('NewValue'))
    jobe2:LoanIDNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:LoanIDNumber
    do Value::jobe2:LoanIDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:LoanIDNumber',p_web.dFormat(p_web.GetValue('Value'),'@s13'))
    jobe2:LoanIDNumber = p_web.GetValue('Value')
  End
      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
      jobe2:RefNumber = p_web.GSV('job:Ref_Number')
      IF (access:jobse2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
          IF (jobe2:LoanIDNumber <> p_web.GSV('jobe2:LoanIDNumber') AND jobe2:LoanIDNumber <> '')
              p_web.SSV('Hide:LoanIDAlert',0)
              p_web.SSV('locLoanIDAlert','You have changed the Loan ID. Please select one of the following options:')
              p_web.SSV('locLoanIDOption',0)
          ELSE
              p_web.SSV('Hide:LoanIDAlert',1)
          END
      END
          
  do Value::jobe2:LoanIDNumber
  do SendAlert
  do Prompt::locLoanIDOption
  do Value::locLoanIDOption  !1
  do Value::locLoanIDAlert  !1

Value::jobe2:LoanIDNumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:LoanIDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe2:LoanIDNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:LoanIDNumber'',''pickloanunit_jobe2:loanidnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:LoanIDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:LoanIDNumber',p_web.GetSessionValueFormat('jobe2:LoanIDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'Loan ID Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_value')

Comment::jobe2:LoanIDNumber  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locLoanIDAlert  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanIDAlert',p_web.GetValue('NewValue'))
    locLoanIDAlert = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanIDAlert
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanIDAlert',p_web.GetValue('Value'))
    locLoanIDAlert = p_web.GetValue('Value')
  End

Value::locLoanIDAlert  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_value',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanIDAlert') = 1)
  ! --- DISPLAY --- locLoanIDAlert
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locLoanIDAlert'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_value')

Comment::locLoanIDAlert  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_comment',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locLoanIDOption  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_prompt',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Select Option')
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_prompt')

Validate::locLoanIDOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanIDOption',p_web.GetValue('NewValue'))
    locLoanIDOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanIDOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanIDOption',p_web.GetValue('Value'))
    locLoanIDOption = p_web.GetValue('Value')
  End
      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
      jobe2:RefNumber = p_web.GSV('job:Ref_Number')
      IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
          CASE p_web.GSV('locLoanIDOption')
          OF 1
          OF 2
          OF 9
              p_web.SSV('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
              p_web.SSV('Hide:LoanIDAlert',1)
          END
      END
  
          
  do Value::locLoanIDOption
  do SendAlert
  do Value::jobe2:LoanIDNumber  !1
  do Prompt::locLoanIDOption
  do Value::locLoanIDAlert  !1

Value::locLoanIDOption  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_value',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanIDAlert') = 1)
  ! --- RADIO --- locLoanIDOption
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locLoanIDOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Change Loan ID Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Change Loan And Customer ID') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 9
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&clip(9)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(9),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Do Not Change Loan ID') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_value')

Comment::locLoanIDOption  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_comment',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Courier  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Courier',p_web.GetValue('NewValue'))
    job:Loan_Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Courier
    do Value::job:Loan_Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Courier = p_web.GetValue('Value')
  End

Value::job:Loan_Courier  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:Loan_Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Loan_Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('job:Loan_Courier') = 0
    p_web.SetSessionValue('job:Loan_Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('job:Loan_Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(JOBSE2)
  bind(jobe2:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(LOAN_ALIAS)
  bind(loa_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Loan_Courier_OptionView)
  job:Loan_Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Loan_Courier_OptionView)
  Loop
    Next(job:Loan_Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Loan_Courier') = 0
      p_web.SetSessionValue('job:Loan_Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Loan_Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Loan_Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(LOAN_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(LOANACC)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()

Comment::job:Loan_Courier  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Consignment_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Consignment_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Consignment_Number',p_web.GetValue('NewValue'))
    job:Loan_Consignment_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Consignment_Number
    do Value::job:Loan_Consignment_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Consignment_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Consignment_Number = p_web.GetValue('Value')
  End

Value::job:Loan_Consignment_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Consignment_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Consignment_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Consignment_Number',p_web.GetSessionValueFormat('job:Loan_Consignment_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_value')

Comment::job:Loan_Consignment_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Despatched  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Despatched  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Despatched',p_web.GetValue('NewValue'))
    job:Loan_Despatched = p_web.GetValue('NewValue') !FieldType= DATE Field = job:Loan_Despatched
    do Value::job:Loan_Despatched
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Despatched',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:Loan_Despatched = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::job:Loan_Despatched  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Despatched
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Despatched')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatched',p_web.GetSessionValue('job:Loan_Despatched'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_value')

Comment::job:Loan_Despatched  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Despatched_User  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('User')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Despatched_User  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Despatched_User',p_web.GetValue('NewValue'))
    job:Loan_Despatched_User = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Despatched_User
    do Value::job:Loan_Despatched_User
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Despatched_User',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Loan_Despatched_User = p_web.GetValue('Value')
  End

Value::job:Loan_Despatched_User  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Despatched_User
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Despatched_User')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatched_User',p_web.GetSessionValueFormat('job:Loan_Despatched_User'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s3'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_value')

Comment::job:Loan_Despatched_User  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Despatch_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Despatch_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Despatch_Number',p_web.GetValue('NewValue'))
    job:Loan_Despatch_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = job:Loan_Despatch_Number
    do Value::job:Loan_Despatch_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Despatch_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    job:Loan_Despatch_Number = p_web.GetValue('Value')
  End

Value::job:Loan_Despatch_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Despatch_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Despatch_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatch_Number',p_web.GetSessionValueFormat('job:Loan_Despatch_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_value')

Comment::job:Loan_Despatch_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:AmendDespatchDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AmendDespatchDetails',p_web.GetValue('NewValue'))
    do Value::button:AmendDespatchDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      if (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          p_web.SSV('ReadOnly:LoanDespatchDetails',0)
      ELSE
          p_web.SSV('ReadOnly:LoanDespatchDetails',1)
      END
  do Value::button:AmendDespatchDetails
  do SendAlert
  do Value::job:Loan_Consignment_Number  !1
  do Value::job:Loan_Despatch_Number  !1
  do Value::job:Loan_Despatched  !1
  do Value::job:Loan_Despatched_User  !1

Value::button:AmendDespatchDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value',Choose(p_web.GSV('job:Loan_Consignment_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Consignment_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AmendDespatchDetails'',''pickloanunit_button:amenddespatchdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AmendDespatchDetails','Amend Despatch Details','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value')

Comment::button:AmendDespatchDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_comment',Choose(p_web.GSV('job:Loan_Consignment_Number') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Loan_Consignment_Number') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locRemoveText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemoveText',p_web.GetValue('NewValue'))
    do Value::locRemoveText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locRemoveText  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_value',Choose(p_web.GSV('job:Loan_unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('You must remove the existing Loan before you can add a new one',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_value')

Comment::locRemoveText  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_comment',Choose(p_web.GSV('job:Loan_unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Loan_unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonRemoveAttachedUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRemoveAttachedUnit',p_web.GetValue('NewValue'))
    do Value::buttonRemoveAttachedUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('Hide:RemovalReason',0)
  
  if (p_web.GSV('job:Loan_Consignment_Number') <> '')
      p_web.SSV('locRemoveAlertMessage','Warning!<br>The Loan Unit has already been despatched. '&|
                          'If the continue the unit will be removed and returned to Loan Stock.')
  end ! if (p_web.GSV('job:Loan_Consignment_Number') <> '')
  do SendAlert
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::buttonConfirmLoanRemoval  !1
  do Value::buttonRemoveAttachedUnit  !1
  do Prompt::locRemovalAlertMessage
  do Value::locRemovalAlertMessage  !1
  do Value::TagValidateLoanAccessories  !1

Value::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAttachedUnit'',''pickloanunit_buttonremoveattachedunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveLoanUnit','Remove Loan Unit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value')

Comment::buttonRemoveAttachedUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Alert')
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt')

Validate::locRemovalAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('NewValue'))
    locRemovalAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRemovalAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('Value'))
    locRemovalAlertMessage = p_web.GetValue('Value')
  End
  do Value::locRemovalAlertMessage
  do SendAlert

Value::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locRemovalAlertMessage') = '')
  ! --- TEXT --- locRemovalAlertMessage
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locRemovalAlertMessage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRemovalAlertMessage'',''pickloanunit_locremovalalertmessage_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locRemovalAlertMessage',p_web.GetSessionValue('locRemovalAlertMessage'),5,25,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locRemovalAlertMessage),,Net:Web:Frame) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value')

Comment::locRemovalAlertMessage  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_comment',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RemovalReason  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Removal Reason')
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt')

Validate::tmp:RemovalReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('NewValue'))
    tmp:RemovalReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RemovalReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('Value'))
    tmp:RemovalReason = p_web.GetValue('Value')
  End
  if p_web.GSV('tmp:RemovalReason') = 3 or p_web.GSV('tmp:RemovalReason') = 0
      p_web.SSV('Hide:ConfirmRemovalButton',1)
  else ! if p_web.GSV('tmp:RemovalReason') = 3
      p_web.SSV('Hide:ConfirmRemovalButton',0)
  end ! if p_web.GSV('tmp:RemovalReason') = 3
  do Value::tmp:RemovalReason
  do SendAlert
  do Value::buttonConfirmLoanRemoval  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1

Value::tmp:RemovalReason  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- RADIO --- tmp:RemovalReason
  loc:fieldclass = Choose(sub(' noButton',1,1) = ' ',clip('FormEntry') & ' noButton',' noButton')
  If lower(loc:invalid) = lower('tmp:RemovalReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replace Faulty Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Alternative Model Required') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(4)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Unit Lost') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value')

Comment::tmp:RemovalReason  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TagValidateLoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Validate Accessories')
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','PickLoanUnit')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('PickLoanUnit_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('PickLoanUnit_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = p_web.Translate('Do not tag if no accessories attached')
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !Validate Accessories
  p_web.SSV('AccessoryCheck:Type','LOA')
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('tmp:LoanUnitNumber'))
  AccessoryCheck(p_web)
  
  case p_web.GSV('AccessoryCheck:Return')
  of 1
      p_web.SSV('locValidateMessage','<span class="RedBold">There is a missing accessory. If required, enter a Lost Loan Charge below or revalidate the accessories.</span>')
      p_web.SSV('Hide:LostLoanFee',0)
  of 2
      p_web.SSV('locValidateMessage','<span class="RedBold">There is an accessory mismatch. If required, enter a Lost Loan Charge below or revalidate the accessories.</span>')
      p_web.SSV('Hide:LostLoanFee',0)
  else
      p_web.SSV('locValidateMessage','<span class="GreenBold">Accessories Validated</span>')
      p_web.SSV('Hide:LostLoanFee',1)
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
          
  end
  
  p_web.SSV('Hide:ConfirmRemovalButton',0)
  
  
  !    error# = 0
  !    Access:LOANACC.Clearkey(lac:Ref_Number_Key)
  !    lac:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
  !    set(lac:Ref_Number_Key,lac:Ref_Number_Key)
  !    loop
  !        if (Access:LOANACC.Next())
  !            Break
  !        end ! if (Access:LOANACC.Next())
  !        if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
  !            Break
  !        end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
  !
  !
  !        Access:TAGFILE.Clearkey(tag:keyTagged)
  !        tag:sessionID    = p_web.sessionID
  !        tag:taggedValue  = lac:accessory
  !        if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  !            ! Found
  !            if (tag:tagged <> 1)
  !               error# = 1
  !               break
  !            end ! if (tag:tagged <> 1)
  !        else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  !            ! Error
  !            error# = 1
  !            break
  !        end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  !    end ! loop
  !
  !    if (error# = 0)
  !        Access:TAGFILE.Clearkey(tag:keyTagged)
  !        tag:sessionID    = p_web.sessionID
  !        set(tag:keyTagged,tag:keyTagged)
  !        loop
  !            if (Access:TAGFILE.Next())
  !                Break
  !            end ! if (Access:TAGFILE.Next())
  !            if (tag:sessionID    <> p_web.sessionID)
  !                Break
  !            end ! if (tag:sessionID    <> p_web.sessionID)
  !            if (tag:Tagged = 0)
  !                cycle
  !            end ! if (tag:Tagged = 0)
  !            Access:LOANACC.Clearkey(lac:ref_number_Key)
  !            lac:ref_number    = p_web.GSV('tmp:loanunitnumber')
  !            lac:accessory    = tag:taggedValue
  !            if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
  !                ! Found
  !            else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
  !                ! Error
  !                error# = 2
  !                break
  !            end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
  !        end ! loop
  !    end ! if (error# = 0)
  
  do Value::buttonValidateAccessories
  do SendAlert
  do Prompt::locValidateMessage
  do Value::locValidateMessage  !1
  do Prompt::tmp:LostLoanFee
  do Value::tmp:LostLoanFee  !1
  do Value::buttonConfirmLoanRemoval  !1
  do Value::TagValidateLoanAccessories  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''pickloanunit_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locValidateMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_prompt',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Message')
  If p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_prompt')

Validate::locValidateMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locValidateMessage',p_web.GetValue('NewValue'))
    locValidateMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locValidateMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locValidateMessage',p_web.GetValue('Value'))
    locValidateMessage = p_web.GetValue('Value')
  End

Value::locValidateMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_value',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1)
  ! --- TEXT --- locValidateMessage
  loc:fieldclass = Choose(sub('RedBoldSmall',1,1) = ' ',clip('FormEntry') & 'RedBoldSmall','RedBoldSmall')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locValidateMessage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locValidateMessage',p_web.GetSessionValue('locValidateMessage'),5,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locValidateMessage),,Net:Web:Frame) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_value')

Comment::locValidateMessage  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_comment',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LostLoanFee  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_prompt',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Lost Loan Fee')
  If p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_prompt')

Validate::tmp:LostLoanFee  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LostLoanFee',p_web.GetValue('NewValue'))
    tmp:LostLoanFee = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LostLoanFee
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LostLoanFee',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:LostLoanFee = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:LostLoanFee
  do SendAlert

Value::tmp:LostLoanFee  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_value',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1)
  ! --- STRING --- tmp:LostLoanFee
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:LostLoanFee')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LostLoanFee'',''pickloanunit_tmp:lostloanfee_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:LostLoanFee',p_web.GetSessionValue('tmp:LostLoanFee'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_value')

Comment::tmp:LostLoanFee  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_comment',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonConfirmLoanRemoval  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmLoanRemoval  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmLoanRemoval',p_web.GetValue('NewValue'))
    do Value::buttonConfirmLoanRemoval
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Remove Loan Unit
      p_web.SSV('AddToAudit:Type','LOA')
      if p_web.GSV('job:Loan_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT HAD BEEN DESPATCHED: ' & |
              '<13,10>UNIT NUMBER: ' & p_web.GSV('job:Loan_unit_number') & |
              '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
              '<13,10>CONSIGNMENT NUMBER: ' & p_web.GSV('job:Loan_consignment_number') & |
              '<13,10>DATE DESPATCHED: ' & Format(p_web.GSV('job:Loan_despatched'),@d6) &|
              '<13,10>DESPATCH USER: ' & p_web.GSV('job:Loan_despatched_user') &|
              '<13,10>DESPATCH NUMBER: ' & p_web.GSV('job:Loan_despatch_number'))
      else ! if p_web.GSV('job:Loan_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT NUMBER: ' & p_web.GSV('job:Loan_unit_Number'))
      end !if p_web.GSV('job:Loan_Consignemnt_Number') > 0
      case p_web.GSV('tmp:RemovalReason')
      of 1
          p_web.SSV('AddToAudit:Action','REPLACED FAULTY LOAN')
      of 2
          p_web.SSV('AddToAudit:Action','ALTERNATIVE MODEL REQUIRED')
      of 3
          p_web.SSV('AddToAudit:Action','RETURN LOAN: RE-STOCKED')
      of 4
          p_web.SSV('AddToAudit:Action','RETURN LOAN: LOAN LOST')
      end ! case p_web.GSV('tmp:RemovalReason')
  
      addToAudit(p_web)
  
      !p_web.SSV('job:Loan_Unit_Number','')
      p_web.SSV('job:Loan_Accessory','')
      p_web.SSV('job:Loan_Consignment_Number','')
      p_web.SSV('job:Loan_Despatched','')
      p_web.SSV('job:Loan_Despatched_User','')
      p_web.SSV('job:Loan_Issued_Date','')
      p_web.SSV('job:Loan_User','')
      if (p_web.GSV('job:Despatch_Type') = 'LOA')
          p_web.SSV('job:Despatched','NO')
          p_web.SSV('job:Despatch_type','')
      else ! if (p_web.GSV('job:Despatch_Type') = 'EXC')
          if (p_web.GSV('jobe:DespatchType') = 'LOA')
              p_web.SSV('jobe:DespatchType','')
              p_web.SSV('wob:ReadyToDespatch',0)
          end ! if (p_web.GSV('jobe:DespatchType') = 'EXC')
      end !
  
      p_web.SSV('GetStatus:Type','LOA')
      p_web.SSV('GetSTatus:StatusNumber',101) !Not Issued
      getStatus(p_web)
  
      IF (SUB(p_web.GSV('job:Current_Status'),1,3) = '811')
          p_web.SSV('GetStatus:Type','JOB')
          p_web.SSV('GetStatus:StatusNumber','810')
          GetStatus(p_web)
      END
  
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = p_web.GSV('job:loan_unit_number')
      IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
          IF (p_web.GSV('tmp:RemovalReason') = 4)
              loa:Available = 'LOS'
          ELSE
              loa:Available = 'AVL'
          END
          loa:StatusChangeDate = TODAY()
          loa:Job_Number = 0
          IF (Access:LOAN.TryUpdate() = Level:Benign)
              IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                  loh:Ref_Number = loa:Ref_Number
                  loh:Date = TODAY()
                  loh:Time = CLOCK()
                  loh:User = p_web.GSV('BookingUserCode')
                  loh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
                  IF (Access:LOANHIST.TryInsert())
                      Access:LOANHIST.CancelAutoInc()
                  END
              END
          END
      END
  
      p_web.SSV('job:Loan_Unit_Number','') ! Remove Loan Unit
  
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number    = p_web.GSV('job:Ref_Number')
      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBS)
          access:JOBS.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = p_web.GSV('job:Ref_Number')
      if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBSE)
          access:JOBSE.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      p_web.SessionQueueToFile(WEBJOB)
      access:WEBJOB.TryUpdate()
  
      p_web.SSV('tmp:LoanUnitNumber',0)
  
      do clearLoanDetails
  
      p_web.SSV('Hide:RemovalReason',1)
      p_web.SSV('Hide:ConfirmLoanRemoval',1)
  do Value::buttonConfirmLoanRemoval
  do SendAlert
  do Value::buttonPickLoanUnit  !1
  do Value::locLoanAlertMessage  !1
  do Value::locRemovalAlertMessage  !1
  do Value::tmp:LoanAccessories  !1
  do Value::tmp:LoanIMEINumber  !1
  do Value::tmp:LoanLocation  !1
  do Value::tmp:LoanUnitDetails  !1
  do Value::tmp:MSN  !1
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::locRemoveText  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1
  do Prompt::locValidateMessage
  do Value::locValidateMessage  !1
  do Prompt::tmp:LostLoanFee
  do Value::tmp:LostLoanFee  !1

Value::buttonConfirmLoanRemoval  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmLoanRemoval'',''pickloanunit_buttonconfirmloanremoval_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmLoanRemoval','Confirm Loan Removal','button-entryfield',loc:formname,,,,loc:javascript,0,'images\packdelete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_value')

Comment::buttonConfirmLoanRemoval  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:SMSHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:SMSHistory',p_web.GetValue('NewValue'))
    do Value::link:SMSHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:SMSHistory  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('link:SMSHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','SMSHistory','SMS History','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseSMSHistory')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::link:SMSHistory  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('link:SMSHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:SendSMS  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:SendSMS',p_web.GetValue('NewValue'))
    do Value::link:SendSMS
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      IF (p_web.GSV('jobe2:SMSDate') = 0)
          p_web.SSV('jobe2:SMSDate',TODAY() + 30)
          
          Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
          jobe2:RefNumber = p_web.GSV('job:Ref_Number')
          IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE2)
              Access:JOBSE2.TryUpdate()
          END
          
      END
  
  !Format the date before sending
      locSMSDate = Format(p_web.GSV('jobe2:SMSDate'),@d10)
  
  !now send the SMS message
      If p_web.GSV('jobe2:SMSNotification')
          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'LOAN','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'','',clip(locSMSDate))
      End ! If jobe2:SMSNotification
      If p_web.GSV('jobe2:EmailNotification')
          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'LOAN','EMAIL','',p_web.GSV('jobe2:EmailAlertAddress'),'',clip(locSMSDate))
      End ! If jobe2:EmailNotification
  do Value::link:SendSMS
  do SendAlert

Value::link:SendSMS  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('link:SendSMS') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''link:SendSMS'',''pickloanunit_link:sendsms_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Send Return SMS','Send Return SMS','DoubleButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('link:SendSMS') & '_value')

Comment::link:SendSMS  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('link:SendSMS') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickLoanUnit_tmp:LoanIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanIMEINumber
      else
        do Value::tmp:LoanIMEINumber
      end
  of lower('PickLoanUnit_buttonPickLoanUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPickLoanUnit
      else
        do Value::buttonPickLoanUnit
      end
  of lower('PickLoanUnit_jobe2:LoanIDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:LoanIDNumber
      else
        do Value::jobe2:LoanIDNumber
      end
  of lower('PickLoanUnit_locLoanIDOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locLoanIDOption
      else
        do Value::locLoanIDOption
      end
  of lower('PickLoanUnit_button:AmendDespatchDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AmendDespatchDetails
      else
        do Value::button:AmendDespatchDetails
      end
  of lower('PickLoanUnit_buttonRemoveAttachedUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAttachedUnit
      else
        do Value::buttonRemoveAttachedUnit
      end
  of lower('PickLoanUnit_locRemovalAlertMessage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRemovalAlertMessage
      else
        do Value::locRemovalAlertMessage
      end
  of lower('PickLoanUnit_tmp:RemovalReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RemovalReason
      else
        do Value::tmp:RemovalReason
      end
  of lower('PickLoanUnit_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('PickLoanUnit_tmp:LostLoanFee_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LostLoanFee
      else
        do Value::tmp:LostLoanFee
      end
  of lower('PickLoanUnit_buttonConfirmLoanRemoval_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmLoanRemoval
      else
        do Value::buttonConfirmLoanRemoval
      end
  of lower('PickLoanUnit_link:SendSMS_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::link:SendSMS
      else
        do Value::link:SendSMS
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)

PreCopy  Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickLoanUnit:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('job:Loan_Unit_Number') > 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickLoanUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      IF (p_web.GSV('Hide:LoanIDAlert') = 0)
          IF (p_web.GSV('locLoanIDOption') = 0)
              loc:alert = 'You must select an Loan ID option'
              loc:invalid = 'jobe2:LoanIDNumber'
              exit
          END
          
          Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
          jobe2:RefNumber = p_web.GSV('job:Ref_Number')
          IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              CASE(p_web.GSV('locLoanIDOption'))
              OF 1
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','LOAN ID NUMBER CHANGED')
                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))    
                  AddToAudit(p_web)
                  jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
                  IF (Access:JOBSE2.TryUpdate() = Level:Benign)
  
                  END
                  
              OF 2
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','LOAN ID NUMBER CHANGED')
                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))    
                  AddToAudit(p_web)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','CUSTOMER ID NUMBER CHANGED')
                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:IDNumber) & |
                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))    
                  AddToAudit(p_web)                
                  jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
                  p_web.SSV('jobe2:IDNumber',p_web.GSV('jobe2:LoanIDNumber'))
                  jobe2:IDNumber = p_web.GSV('jobe2:IDNumber')
                  IF (Access:JOBSE2.TryUpdate() = Level:Benign)
  
                  END
              END
              
          END
      END
  
      if (p_web.GSV('tmp:LoanUnitNumber') > 0)
          if (p_web.GSV('tmp:LoanUnitNumber') <> p_web.GSV('job:Loan_Unit_Number'))
              do addLoanUnit
          end ! if (p_web.GSV('tmp:LoanUnitNumber') <> p_web.GSV('job:Loan_Unit_Number'))
      else ! if (p_web.GSV('tmp:LoanUnitNumber') > 0)
          if (p_web.GSV('tmp:NoUnitAvailable') = 1)
              p_web.SSV('GetStatus:Type','EXC')
              p_web.SSV('GetStatus:StatusNumber',350)
              getStatus(p_web)
  
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber',355)
              getStatus(p_web)
  
              local.AllocateLoanPart('ORD',0)
          end ! if (p_web.GSV('tmp:NoUnitAvailable') = 1)
      end ! if (p_web.GSV('tmp:LoanUnitNumber') > 0)
  
      do clearTagFile
  p_web.DeleteSessionValue('PickLoanUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
          tmp:LoanIMEINumber = Upper(tmp:LoanIMEINumber)
          p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
        If loc:Invalid <> '' then exit.
  ! tab = 6
    loc:InvalidTab += 1
  ! tab = 5
    loc:InvalidTab += 1
          job:Loan_Consignment_Number = Upper(job:Loan_Consignment_Number)
          p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
        If loc:Invalid <> '' then exit.
          job:Loan_Despatched = Upper(job:Loan_Despatched)
          p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
        If loc:Invalid <> '' then exit.
          job:Loan_Despatch_Number = Upper(job:Loan_Despatch_Number)
          p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('job:Loan_Unit_Number') > 0
    loc:InvalidTab += 1
  End
  ! tab = 7
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      do deleteVariables
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('tmp:LoanIMEINumber')
  p_web.StoreValue('locLoanAlertMessage')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('tmp:LoanUnitDetails')
  p_web.StoreValue('tmp:LoanAccessories')
  p_web.StoreValue('tmp:LoanLocation')
  p_web.StoreValue('tmp:ReplacementValue')
  p_web.StoreValue('jobe2:LoanIDNumber')
  p_web.StoreValue('locLoanIDAlert')
  p_web.StoreValue('locLoanIDOption')
  p_web.StoreValue('job:Loan_Courier')
  p_web.StoreValue('job:Loan_Consignment_Number')
  p_web.StoreValue('job:Loan_Despatched')
  p_web.StoreValue('job:Loan_Despatched_User')
  p_web.StoreValue('job:Loan_Despatch_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locRemovalAlertMessage')
  p_web.StoreValue('tmp:RemovalReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidateMessage')
  p_web.StoreValue('tmp:LostLoanFee')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
Local.AllocateLoanPart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Loan isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If p_web.GSV('job:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Loan isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' Loan UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
!                   wpr:LoanUnit          = True
!                   wpr:SecondLoanUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' Loan UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
!                        par:LoanUnit          = True
!                        par:SecondLoanUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
!            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
RemoveFromStockAllocation PROCEDURE  (func:PartRecordNumber,func:PartType) ! Declare Procedure
save_sto_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_epr_id          USHORT,AUTO                           !
save_tra_id          USHORT,AUTO                           !
tmp:CurrentAccount   STRING(30)                            !Current Account Number
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
STOCKALL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    !Part Record Number
    !Part Type
    !Quantity

    DO openfiles
    DO Savefiles
    Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
    stl:PartType         = func:PartType
    stl:PartRecordNumber = func:PartRecordNumber
    If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Found
        Relate:STOCKALL.Delete(0)
    Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Error
    End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign

    DO restorefiles
    DO closefiles
SaveFiles  ROUTINE
  STOCKALL::State = Access:STOCKALL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF STOCKALL::State <> 0
    Access:STOCKALL.RestoreFile(STOCKALL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKALL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKALL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKALL.Close
     FilesOpened = False
  END
BrowseLoanUnits      PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(LOAN)
                      Project(loa:Ref_Number)
                      Project(loa:ESN)
                      Project(loa:Ref_Number)
                      Project(loa:Manufacturer)
                      Project(loa:Model_Number)
                      Project(loa:MSN)
                      Project(loa:Colour)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseLoanUnits')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseLoanUnits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseLoanUnits:NoForm')
      loc:FormName = p_web.GetValue('BrowseLoanUnits:FormName')
    else
      loc:FormName = 'BrowseLoanUnits_frm'
    End
    p_web.SSV('BrowseLoanUnits:NoForm',loc:NoForm)
    p_web.SSV('BrowseLoanUnits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseLoanUnits:NoForm')
    loc:FormName = p_web.GSV('BrowseLoanUnits:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseLoanUnits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseLoanUnits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(LOAN,loa:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOA:ESN') then p_web.SetValue('BrowseLoanUnits_sort','1')
    ElsIf (loc:vorder = 'LOA:REF_NUMBER') then p_web.SetValue('BrowseLoanUnits_sort','3')
    ElsIf (loc:vorder = 'LOA:MANUFACTURER') then p_web.SetValue('BrowseLoanUnits_sort','7')
    ElsIf (loc:vorder = 'LOA:MODEL_NUMBER') then p_web.SetValue('BrowseLoanUnits_sort','4')
    ElsIf (loc:vorder = 'LOA:MSN') then p_web.SetValue('BrowseLoanUnits_sort','5')
    ElsIf (loc:vorder = 'LOA:COLOUR') then p_web.SetValue('BrowseLoanUnits_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseLoanUnits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseLoanUnits:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseLoanUnits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseLoanUnits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseLoanUnits:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseLoanUnits_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseLoanUnits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:ESN)','-UPPER(loa:ESN)')
    Loc:LocateField = 'loa:ESN'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'loa:Ref_Number','-loa:Ref_Number')
    Loc:LocateField = 'loa:Ref_Number'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:Manufacturer)','-UPPER(loa:Manufacturer)')
    Loc:LocateField = 'loa:Manufacturer'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:Model_Number)','-UPPER(loa:Model_Number)')
    Loc:LocateField = 'loa:Model_Number'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:MSN)','-UPPER(loa:MSN)')
    Loc:LocateField = 'loa:MSN'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:Colour)','-UPPER(loa:Colour)')
    Loc:LocateField = 'loa:Colour'
  of 2
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(loa:Available),+UPPER(loa:Location),+UPPER(loa:ESN)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND (p_web.GSV('tmp:LoanModelNumber') = '' OR p_web.GSV('tmp:LoanManufacturer') = ''))
  ElsIf (p_web.GSV('tmp:LoanStockType') = '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('loa:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:Ref_Number')
    loc:SortHeader = p_web.Translate('Unit Number')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s8')
  Of upper('loa:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:MSN')
    loc:SortHeader = p_web.Translate('M.S.N.')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:Colour')
    loc:SortHeader = p_web.Translate('Colour')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseLoanUnits:LookupFrom')
  End!Else
  loc:formaction = 'BrowseLoanUnits'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseLoanUnits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseLoanUnits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseLoanUnits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="LOAN"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="loa:Ref_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLoanUnits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseLoanUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseLoanUnits.locate(''Locator2BrowseLoanUnits'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLoanUnits.cl(''BrowseLoanUnits'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseLoanUnits_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseLoanUnits_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseLoanUnits','I.M.E.I. Number','Click here to sort by I.M.E.I.',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by I.M.E.I.')&'">'&p_web.Translate('I.M.E.I. Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseLoanUnits','Unit Number','Click here to sort by Unit Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Unit Number')&'">'&p_web.Translate('Unit Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseLoanUnits','Manufacturer','Click here to sort by Manufacturer',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Manufacturer')&'">'&p_web.Translate('Manufacturer')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseLoanUnits','Model Number','Click here to sort by Model Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Model Number')&'">'&p_web.Translate('Model Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseLoanUnits','M.S.N.','Click here to sort by M.S.N.',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by M.S.N.')&'">'&p_web.Translate('M.S.N.')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseLoanUnits','Colour','Click here to sort by Colour',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Colour')&'">'&p_web.Translate('Colour')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Select
        do AddPacket
        loc:columns += 1
    End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('loa:ref_number',lower(Thisview{prop:order}),1,1) = 0 !and LOAN{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'loa:Ref_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('loa:Ref_Number'),p_web.GetValue('loa:Ref_Number'),p_web.GetSessionValue('loa:Ref_Number'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
      loc:FilterWas = 'Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(loa:Stock_Type) = Upper(''' & p_web.GSV('tmp:LoanStockType') & ''') AND Upper(loa:Available) = ''AVL'' AND Upper(loa:Model_Number) = Upper(''' & p_web.GSV('tmp:LoanModelNumber') & ''')'
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND (p_web.GSV('tmp:LoanModelNumber') = '' OR p_web.GSV('tmp:LoanManufacturer') = ''))
      loc:FilterWas = 'Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(loa:Stock_Type) = Upper(''' & p_web.GSV('tmp:LoanStockType') & ''') AND Upper(loa:Available) = ''AVL'''
  ElsIf (p_web.GSV('tmp:LoanStockType') = '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
      loc:FilterWas = 'Upper(loa:Available) = ''AVL'' AND Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(loa:Model_Number) = Upper(''' & p_web.GSV('tmp:LoanModelNumber') & ''')'
  Else
        loc:FilterWas = 'Upper(loa:Available) = ''AVL'' AND Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLoanUnits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseLoanUnits_Filter')
    p_web.SetSessionValue('BrowseLoanUnits_FirstValue','')
    p_web.SetSessionValue('BrowseLoanUnits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,LOAN,loa:Ref_Number_Key,loc:PageRows,'BrowseLoanUnits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If LOAN{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(LOAN,loc:firstvalue)
              Reset(ThisView,LOAN)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If LOAN{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(LOAN,loc:lastvalue)
            Reset(ThisView,LOAN)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(loa:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLoanUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLoanUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLoanUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLoanUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLoanUnits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseLoanUnits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseLoanUnits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseLoanUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseLoanUnits.locate(''Locator1BrowseLoanUnits'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLoanUnits.cl(''BrowseLoanUnits'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseLoanUnits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseLoanUnits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLoanUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLoanUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLoanUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLoanUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = loa:Ref_Number
    p_web._thisrow = p_web._nocolon('loa:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseLoanUnits:LookupField')) = loa:Ref_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((loa:Ref_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseLoanUnits.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If LOAN{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(LOAN)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If LOAN{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(LOAN)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','loa:Ref_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseLoanUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','loa:Ref_Number',clip(loc:field),,'checked',,,'onclick="BrowseLoanUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loa:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loa:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loa:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loa:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loa:MSN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::loa:Colour
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseLoanUnits.omv(this);" onMouseOut="BrowseLoanUnits.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseLoanUnits=new browseTable(''BrowseLoanUnits'','''&clip(loc:formname)&''','''&p_web._jsok('loa:Ref_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('loa:Ref_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseLoanUnits.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseLoanUnits.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLoanUnits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLoanUnits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLoanUnits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLoanUnits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(LOAN)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(LOAN)
  Bind(loa:Record)
  Clear(loa:Record)
  NetWebSetSessionPics(p_web,LOAN)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('loa:Ref_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::loa:ESN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loa:ESN_'&loa:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loa:ESN,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Ref_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loa:Ref_Number_'&loa:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loa:Ref_Number,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Manufacturer   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loa:Manufacturer_'&loa:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loa:Manufacturer,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Model_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loa:Model_Number_'&loa:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loa:Model_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:MSN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loa:MSN_'&loa:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loa:MSN,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Colour   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('loa:Colour_'&loa:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(loa:Colour,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&loa:Ref_Number,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseLoanUnits',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseLoanUnits',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseLoanUnits',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = loa:Ref_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('loa:Ref_Number',loa:Ref_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('loa:Ref_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('loa:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('loa:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
