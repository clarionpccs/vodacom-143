

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3010.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3009.INC'),ONCE        !Req'd for module callout resolution
                     END


FormLoanUnitFilter   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:LoanStockType    STRING(30)                            !
tmp:LoanModelNumber  STRING(30)                            !
tmp:LoanManufacturer STRING(30)                            !
locAllStockTypes     BYTE                                  !
FilesOpened     Long
STOCKTYP::State  USHORT
MODELNUM::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
tmp:LoanStockType_OptionView   View(STOCKTYP)
                          Project(stp:Stock_Type)
                        End
tmp:LoanManufacturer_OptionView   View(MANUFACT)
                          Project(man:Manufacturer)
                        End
tmp:LoanModelNumber_OptionView   View(MODELNUM)
                          Project(mod:Model_Number)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormLoanUnitFilter')
  loc:formname = 'FormLoanUnitFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormLoanUnitFilter','')
    p_web._DivHeader('FormLoanUnitFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormLoanUnitFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:LoanStockType'
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOCKTYP)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:LoanManufacturer')
  Of 'tmp:LoanModelNumber'
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType)
  p_web.SetSessionValue('tmp:LoanManufacturer',tmp:LoanManufacturer)
  p_web.SetSessionValue('tmp:LoanModelNumber',tmp:LoanModelNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:LoanStockType')
    tmp:LoanStockType = p_web.GetValue('tmp:LoanStockType')
    p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType)
  End
  if p_web.IfExistsValue('tmp:LoanManufacturer')
    tmp:LoanManufacturer = p_web.GetValue('tmp:LoanManufacturer')
    p_web.SetSessionValue('tmp:LoanManufacturer',tmp:LoanManufacturer)
  End
  if p_web.IfExistsValue('tmp:LoanModelNumber')
    tmp:LoanModelNumber = p_web.GetValue('tmp:LoanModelNumber')
    p_web.SetSessionValue('tmp:LoanModelNumber',tmp:LoanModelNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormLoanUnitFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('filterStockType','LOAN')
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:LoanStockType = p_web.RestoreValue('tmp:LoanStockType')
 tmp:LoanManufacturer = p_web.RestoreValue('tmp:LoanManufacturer')
 tmp:LoanModelNumber = p_web.RestoreValue('tmp:LoanModelNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormLoanUnitFilter')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormLoanUnitFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormLoanUnitFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormLoanUnitFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormLoanUnitFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormLoanUnitFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormLoanUnitFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Loan Units') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Loan Units',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormLoanUnitFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormLoanUnitFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormLoanUnitFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Filter Loan Units') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Available Loan Units') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormLoanUnitFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormLoanUnitFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:LoanStockType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormLoanUnitFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Filter Loan Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormLoanUnitFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Loan Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Loan Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Loan Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Loan Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanStockType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&290&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&290&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&290&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Available Loan Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormLoanUnitFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Available Loan Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Available Loan Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Available Loan Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Available Loan Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseLoanUnits
      do Comment::BrowseLoanUnits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:LoanStockType  Routine
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanStockType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanStockType',p_web.GetValue('NewValue'))
    tmp:LoanStockType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanStockType',p_web.GetValue('Value'))
    tmp:LoanStockType = p_web.GetValue('Value')
  End
    tmp:LoanStockType = Upper(tmp:LoanStockType)
    p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType)
  do Value::tmp:LoanStockType
  do SendAlert
  do Value::BrowseLoanUnits  !1
  do Value::tmp:LoanManufacturer  !1
  do Value::tmp:LoanModelNumber  !1

Value::tmp:LoanStockType  Routine
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:LoanStockType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanStockType'',''formloanunitfilter_tmp:loanstocktype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanStockType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('tmp:AllStockTypes') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:LoanStockType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:LoanStockType') = 0
    p_web.SetSessionValue('tmp:LoanStockType','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Stock Types ==','',choose('' = p_web.getsessionvalue('tmp:LoanStockType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:LoanStockType_OptionView)
  tmp:LoanStockType_OptionView{prop:filter} = 'stp:Available = 1 AND Upper(stp:Use_Loan) = ''YES'''
  tmp:LoanStockType_OptionView{prop:order} = 'UPPER(stp:Stock_Type)'
  Set(tmp:LoanStockType_OptionView)
  Loop
    Next(tmp:LoanStockType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:LoanStockType') = 0
      p_web.SetSessionValue('tmp:LoanStockType',stp:Stock_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(Upper(stp:Stock_Type),stp:Stock_Type,choose(stp:Stock_Type = p_web.getsessionvalue('tmp:LoanStockType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:LoanStockType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_value')

Comment::tmp:LoanStockType  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanManufacturer  Routine
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanManufacturer',p_web.GetValue('NewValue'))
    tmp:LoanManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanManufacturer',p_web.GetValue('Value'))
    tmp:LoanManufacturer = p_web.GetValue('Value')
  End
  do Value::tmp:LoanManufacturer
  do SendAlert
  do Value::tmp:LoanModelNumber  !1
  do Value::BrowseLoanUnits  !1

Value::tmp:LoanManufacturer  Routine
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:LoanManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanManufacturer'',''formloanunitfilter_tmp:loanmanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanManufacturer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:LoanManufacturer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:LoanManufacturer') = 0
    p_web.SetSessionValue('tmp:LoanManufacturer','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Manufacturers ==','',choose('' = p_web.getsessionvalue('tmp:LoanManufacturer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:LoanManufacturer_OptionView)
  tmp:LoanManufacturer_OptionView{prop:order} = 'UPPER(man:Manufacturer)'
  Set(tmp:LoanManufacturer_OptionView)
  Loop
    Next(tmp:LoanManufacturer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:LoanManufacturer') = 0
      p_web.SetSessionValue('tmp:LoanManufacturer',man:Manufacturer)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,man:Manufacturer,choose(man:Manufacturer = p_web.getsessionvalue('tmp:LoanManufacturer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:LoanManufacturer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_value')

Comment::tmp:LoanManufacturer  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanModelNumber  Routine
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanModelNumber',p_web.GetValue('NewValue'))
    tmp:LoanModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanModelNumber',p_web.GetValue('Value'))
    tmp:LoanModelNumber = p_web.GetValue('Value')
  End
  do Value::tmp:LoanModelNumber
  do SendAlert
  do Value::BrowseLoanUnits  !1

Value::tmp:LoanModelNumber  Routine
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:LoanModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanModelNumber'',''formloanunitfilter_tmp:loanmodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanModelNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:LoanModelNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:LoanModelNumber') = 0
    p_web.SetSessionValue('tmp:LoanModelNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Model Numbers ==','',choose('' = p_web.getsessionvalue('tmp:LoanModelNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:LoanModelNumber_OptionView)
  tmp:LoanModelNumber_OptionView{prop:filter} = 'Upper(mod:Manufacturer ) = Upper(''' & p_web.GSV('tmp:LoanManufacturer') & ''')'
  tmp:LoanModelNumber_OptionView{prop:order} = 'UPPER(mod:Model_Number)'
  Set(tmp:LoanModelNumber_OptionView)
  Loop
    Next(tmp:LoanModelNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:LoanModelNumber') = 0
      p_web.SetSessionValue('tmp:LoanModelNumber',mod:Model_Number)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,mod:Model_Number,choose(mod:Model_Number = p_web.getsessionvalue('tmp:LoanModelNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:LoanModelNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_value')

Comment::tmp:LoanModelNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseLoanUnits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseLoanUnits',p_web.GetValue('NewValue'))
    do Value::BrowseLoanUnits
  Else
    p_web.StoreValue('loa:Ref_Number')
  End

Value::BrowseLoanUnits  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseLoanUnits --
  p_web.SetValue('BrowseLoanUnits:NoForm',1)
  p_web.SetValue('BrowseLoanUnits:FormName',loc:formname)
  p_web.SetValue('BrowseLoanUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormLoanUnitFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormLoanUnitFilter_BrowseLoanUnits_embedded_div')&'"><!-- Net:BrowseLoanUnits --></div><13,10>'
    p_web._DivHeader('FormLoanUnitFilter_' & lower('BrowseLoanUnits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormLoanUnitFilter_' & lower('BrowseLoanUnits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseLoanUnits --><13,10>'
  end
  do SendPacket

Comment::BrowseLoanUnits  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanUnitFilter_' & p_web._nocolon('BrowseLoanUnits') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormLoanUnitFilter_tmp:LoanStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanStockType
      else
        do Value::tmp:LoanStockType
      end
  of lower('FormLoanUnitFilter_tmp:LoanManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanManufacturer
      else
        do Value::tmp:LoanManufacturer
      end
  of lower('FormLoanUnitFilter_tmp:LoanModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanModelNumber
      else
        do Value::tmp:LoanModelNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)

PreCopy  Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormLoanUnitFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormLoanUnitFilter:Primed',0)
  p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormLoanUnitFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormLoanUnitFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          tmp:LoanStockType = Upper(tmp:LoanStockType)
          p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType)
        If loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormLoanUnitFilter:Primed',0)
  p_web.StoreValue('tmp:LoanStockType')
  p_web.StoreValue('tmp:LoanManufacturer')
  p_web.StoreValue('tmp:LoanModelNumber')
  p_web.StoreValue('')
BrowseSMSHistory     PROCEDURE  (NetWebServerWorker p_web)
locSMSSent           STRING(5)                             !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SMSMAIL)
                      Project(sms:RecordNumber)
                      Project(sms:DateInserted)
                      Project(sms:TimeInserted)
                      Project(sms:MSISDN)
                      Project(sms:MSG)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('BrowseSMSHistory')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseSMSHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseSMSHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseSMSHistory:FormName')
    else
      loc:FormName = 'BrowseSMSHistory_frm'
    End
    p_web.SSV('BrowseSMSHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseSMSHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseSMSHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseSMSHistory:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseSMSHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseSMSHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SMSMAIL,sms:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SMS:DATEINSERTED') then p_web.SetValue('BrowseSMSHistory_sort','1')
    ElsIf (loc:vorder = 'SMS:TIMEINSERTED') then p_web.SetValue('BrowseSMSHistory_sort','2')
    ElsIf (loc:vorder = 'LOCSMSSENT') then p_web.SetValue('BrowseSMSHistory_sort','3')
    ElsIf (loc:vorder = 'SMS:MSISDN') then p_web.SetValue('BrowseSMSHistory_sort','4')
    ElsIf (loc:vorder = 'SMS:MSG') then p_web.SetValue('BrowseSMSHistory_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseSMSHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseSMSHistory:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseSMSHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseSMSHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseSMSHistory:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseSMSHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseSMSHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'sms:DateInserted','-sms:DateInserted')
    Loc:LocateField = 'sms:DateInserted'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'sms:TimeInserted','-sms:TimeInserted')
    Loc:LocateField = 'sms:TimeInserted'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'locSMSSent','-locSMSSent')
    Loc:LocateField = 'locSMSSent'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sms:MSISDN)','-UPPER(sms:MSISDN)')
    Loc:LocateField = 'sms:MSISDN'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sms:MSG)','-UPPER(sms:MSG)')
    Loc:LocateField = 'sms:MSG'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(sms:SendToSMS),+UPPER(sms:SMSSent)'
  end
  If False ! add range fields to sort order
  Else
    If Instring('SMS:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'sms:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sms:DateInserted')
    loc:SortHeader = p_web.Translate('Date Inserted')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@d6')
  Of upper('sms:TimeInserted')
    loc:SortHeader = p_web.Translate('Time Inserted')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@t1b')
  Of upper('locSMSSent')
    loc:SortHeader = p_web.Translate('Sent')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@s5')
  Of upper('sms:MSISDN')
    loc:SortHeader = p_web.Translate('Receipient')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@s12')
  Of upper('sms:MSG')
    loc:SortHeader = p_web.Translate('Message')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@s160')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseSMSHistory:LookupFrom')
  End!Else
  loc:CloseAction = 'PickLoanUnit'
  loc:formaction = 'BrowseSMSHistory'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseSMSHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseSMSHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseSMSHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SMSMAIL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sms:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse SMS History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse SMS History',0)&'</span>'&CRLF
  End
  If clip('Browse SMS History') <> ''
    packet = clip(packet) & p_web.br
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSMSHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseSMSHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseSMSHistory.locate(''Locator2BrowseSMSHistory'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSMSHistory.cl(''BrowseSMSHistory'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseSMSHistory_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseSMSHistory_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseSMSHistory','Date Inserted','Click here to sort by Date Inserted',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Inserted')&'">'&p_web.Translate('Date Inserted')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseSMSHistory','Time Inserted','Click here to sort by Time Inserted',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Time Inserted')&'">'&p_web.Translate('Time Inserted')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseSMSHistory','Sent',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Sent')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseSMSHistory','Receipient','Click here to sort by Receipient',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Receipient')&'">'&p_web.Translate('Receipient')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseSMSHistory','Message','Click here to sort by Message',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Message')&'">'&p_web.Translate('Message')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'sms:DateInserted' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sms:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SMSMAIL{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sms:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sms:RecordNumber'),p_web.GetValue('sms:RecordNumber'),p_web.GetSessionValue('sms:RecordNumber'))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'sms:RefNumber = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSMSHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseSMSHistory_Filter')
    p_web.SetSessionValue('BrowseSMSHistory_FirstValue','')
    p_web.SetSessionValue('BrowseSMSHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SMSMAIL,sms:RecordNumberKey,loc:PageRows,'BrowseSMSHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SMSMAIL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SMSMAIL,loc:firstvalue)
              Reset(ThisView,SMSMAIL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SMSMAIL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SMSMAIL,loc:lastvalue)
            Reset(ThisView,SMSMAIL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sms:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSMSHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSMSHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSMSHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSMSHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSMSHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseSMSHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseSMSHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseSMSHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseSMSHistory.locate(''Locator1BrowseSMSHistory'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSMSHistory.cl(''BrowseSMSHistory'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseSMSHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseSMSHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSMSHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSMSHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSMSHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSMSHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sms:RecordNumber
    p_web._thisrow = p_web._nocolon('sms:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseSMSHistory:LookupField')) = sms:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sms:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseSMSHistory.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SMSMAIL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SMSMAIL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SMSMAIL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SMSMAIL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sms:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseSMSHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sms:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseSMSHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sms:DateInserted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sms:TimeInserted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif sms:SMSSent = 'T'
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif sms:SMSSent <> 'T'
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::locSMSSent
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sms:MSISDN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sms:MSG
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseSMSHistory.omv(this);" onMouseOut="BrowseSMSHistory.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseSMSHistory=new browseTable(''BrowseSMSHistory'','''&clip(loc:formname)&''','''&p_web._jsok('sms:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sms:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseSMSHistory.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseSMSHistory.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSMSHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSMSHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSMSHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSMSHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SMSMAIL)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SMSMAIL)
  Bind(sms:Record)
  Clear(sms:Record)
  NetWebSetSessionPics(p_web,SMSMAIL)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sms:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::sms:DateInserted   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sms:DateInserted_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sms:DateInserted,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sms:TimeInserted   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sms:TimeInserted_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sms:TimeInserted,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locSMSSent   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif sms:SMSSent = 'T'
      packet = clip(packet) & p_web._DivHeader('locSMSSent_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('YES',0))
    elsif sms:SMSSent <> 'T'
      packet = clip(packet) & p_web._DivHeader('locSMSSent_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('NO',0))
    else
      packet = clip(packet) & p_web._DivHeader('locSMSSent_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locSMSSent,'@s5')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sms:MSISDN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sms:MSISDN_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sms:MSISDN,'@s12')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sms:MSG   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sms:MSG_'&sms:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sms:MSG,'@s160')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sms:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sms:RecordNumber',sms:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sms:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sms:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sms:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
PickExchangeUnit     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ExchangeUnitNumber LONG                                !
tmp:UnitDetails      STRING(100)                           !
tmp:ExchangeIMEINumber STRING(30)                          !
tmp:exchangeModelNumber STRING(30)                         !
tmp:MSN              STRING(30)                            !
tmp:ExchangeUnitDetails STRING(60)                         !
tmp:ExchangeAccessories STRING(100)                        !
tmp:ExchangeLocation STRING(100)                           !
tmp:ReplacementValue REAL                                  !
locExchangeAlertMessage STRING(255)                        !
tmp:RemovalReason    BYTE                                  !
locRemovalAlertMessage STRING(255)                         !
tmp:HandsetPartNumber STRING(30)                           !
tmp:HandsetReplacementValue REAL                           !
tmp:NoUnitAvailable  BYTE                                  !
FilesOpened     Long
COURIER::State  USHORT
JOBS::State  USHORT
EXCHANGE::State  USHORT
JOBEXACC::State  USHORT
EXCHANGE_ALIAS::State  USHORT
EXCHOR48::State  USHORT
MANUFACT::State  USHORT
PARTS::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
STOCKTYP::State  USHORT
EXCHHIST::State  USHORT
PRODCODE::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
TRDBATCH::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
tmp:HandsetPartNumber_OptionView   View(PRODCODE)
                          Project(prd:ProductCode)
                        End
job:Exchange_Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
local       class
AllocateExchangePart    Procedure(String func:Status,Byte func:SecondUnit)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickExchangeUnit')
  loc:formname = 'PickExchangeUnit_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickExchangeUnit','')
    p_web._DivHeader('PickExchangeUnit',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
addExchangeUnit     Routine
    data
locAuditNotes   String(255)
    code
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:QALoanExchange)
                    xch:Available = 'QA1'
                    
                else ! if (man:QALoanExchange)
                    xch:Available = 'EXC'
                end !if (man:QALoanExchange)
                xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
                xch:Job_Number = p_web.GSV('job:Ref_Number')
                access:EXCHANGE.tryUpdate()

                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number    = tmp:ExchangeUnitNumber
                    exh:Date    = Today()
                    exh:Time    = Clock()
                    exh:User    = p_web.GSV('BookingUserCode')
                    if (man:QALoanExchange)
                        exh:Status    = 'AWAITING QA. EXCHANGED ON JOB NO: ' & p_web.GSV('job:ref_number')
                    else ! if (man:QALoanExchange)
                        exh:status = 'UNIT EXCHANGED ON JOB NO: ' & p_web.GSV('job:Ref_number')
                    end !
                    
                    if (Access:EXCHHIST.TryInsert() = Level:Benign)
                        ! Inserted
                    else ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                        ! Error
                        Access:EXCHHIST.CancelAutoInc()
                    end ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)

                locAuditNotes = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                    '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                    '<13,10>I.M.E.I.: ' & CLip(xch:esn)

                if (MSNRequired(xch:Manufacturer))
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(xch:MSN)
                end ! if (MSNRequired(xch:Manufacturer))

                locAuditNotes = clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(xch:Stock_Type)

                p_web.SSV('AddToAudit:Type','EXC')
                p_web.SSV('AddToAudit:Action','EXCHANGE UNIT ATTACHED TO JOB')
                p_web.SSV('AddToAudit:Notes',clip(locAuditNotes))
                addToAudit(p_web)

                if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('jobe:ExchangeATRRC',1)
                else ! if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('jobe:ExchangeATRRC',0)
                end !if (p_web.GSV('BookingSite') = 'RRC')

                local.AllocateExchangePart('PIK',0)

                ! Create a new exchange unit for incoming unit

                Access:EXCHANGE_ALIAS.Clearkey(xch_ali:ESN_Only_Key)
                xch_ali:ESN    = xch:ESN
                if (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign)
                    ! Found
                    ! Unit already exists, use that one
                    if (p_web.GSV('job:Date_Completed') <> '')
                        xch_ali:Available = 'RTS'
                    else ! if (p_web.GSV('job:Date_Completed') <> '')
                        if (p_web.GSV('job:Workshop') = 'YES')
                            xch_ali:Available = 'REP'
                        else ! if (p_web.GSV('job:Workshop') = 'YES')
                            xch_ali:Available = 'INC'
                        end !if (p_web.GSV('job:Workshop') = 'YES')
                    end !if (p_web.GSV('job:Date_Completed') <> '')
                    xch_ali:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
                    xch_ali:Job_Number = p_web.GSV('job:Ref_Number')
                    access:EXCHANGE_ALIAS.tryUpdate()
                else ! if (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign)
                    ! Error
                    ! IMEI doesn't exist. Create a new one
                    if (Access:EXCHANGE_ALIAS.PrimeRecord() = Level:Benign)
                        refNo# = xch_ali:Ref_Number
                        xch_ali:Record :=: xch:Record
                        xch_ali:Ref_Number = refNo#

                        if (p_web.GSV('job:Date_Completed') <> '')
                            xch_ali:Available = 'RTS'
                        else ! if (p_web.GSV('job:Date_Completed') <> '')
                            if (p_web.GSV('job:Workshop') = 'YES')
                                xch_ali:Available = 'REP'
                            else ! if (p_web.GSV('job:Workshop') = 'YES')
                                xch_ali:Available = 'INC'
                            end !if (p_web.GSV('job:Workshop') = 'YES')
                        end !if (p_web.GSV('job:Date_Completed') <> '')
                        xch_ali:Job_Number = p_web.GSV('job:Ref_Number')
                        xch_ali:ESN = p_web.GSV('job:ESN')
                        xch_ali:MSN = p_web.GSV('job:MSN')
                        xch_ali:Model_Number = p_web.GSV('job:Model_Number')
                        xch_ali:Manufacturer = p_web.GSV('job:Manufacturer')

                        if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                            ! Inserted
                        else ! if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                            ! Error
                            Access:EXCHANGE_ALIAS.CancelAutoInc()
                        end ! if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                    end ! if (Access:EXCHANGE_ALIAS.PrimeRecord() = Level:Benign)
                End

                ! Mark exchange order as fulfilled
                if (p_web.GSV('jobe:Engineer48HourOption') = 1)

                    Access:EXCHOR48.Clearkey(ex4:AttachedToJobKey)
                    ex4:AttachedToJob    = 0
                    ex4:Location    = p_web.GSV('BookingSiteLocation')
                    ex4:JobNumber    = p_web.GSV('job:Ref_Number')
                    if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                        ! Found
                        ex4:attachedToJob = 1
                        access:EXCHOR48.tryUpdate()
                    else ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                        ! Error
                    end ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                end ! if (p_web.GSV('jobe:Engineer48HourOption') = 1)

                if (man:QALoanExchange)
                    p_web.SSV('job:Despatched','')
                    p_web.SSV('job:DespatchType','')
                    p_web.SSV('job:Exchange_Status','QA REQUIRED')
                    p_web.SSV('job:Exchange_Unit',p_web.GSV('BookingUserCode'))
                    p_web.SSV('GetStatus:StatusNumber',605)
                    p_web.SSV('GetStatus:Type','EXC')
                    getStatus(p_web)
                else ! if (man:QALoanExchange)

                    if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('jobe:DespatchType','EXC')
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('wob:ReadyToDespatch',1)
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:exchange_Courier'))
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('jobe:DespatchType','EXC')
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Exchange_Courier'))

                    else ! if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('job:Exchange_Status','AWAITING DESPATCH')
                        p_web.SSV('job:Exchange_User',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Exchange_Despatched','')

                        p_web.SSV('job:Despatched','REA')
                        p_web.SSV('job:Despatch_Type','EXC')
                    end ! if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('GetStatus:StatusNumber',110)
                    p_web.SSV('GetStatus:Type','EXC')
                    getStatus(p_web)

                end !if (man:QALoanExchange)

                if (p_web.GSV('job:Third_Party_Site') <> '')
                    Access:TRDBATCH.Clearkey(trb:ESN_Only_Key)
                    trb:ESN    = p_web.GSV('job:ESN')
                    set(trb:ESN_Only_Key,trb:ESN_Only_Key)
                    loop
                        if (Access:TRDBATCH.Next())
                            Break
                        end ! if (Access:TRDBATCH.Next())
                        if (trb:ESN    <> p_web.GSV('job:ESN'))
                            Break
                        end ! if (trb:ESN    <> p_web.GSV('job:ESN'))
                        if (trb:Ref_Number = p_web.GSV('job:Ref_Number'))
                            trb:Exchanged = 'YES'
                            access:TRDBATCH.tryUpdate()
                        end ! if (trb:Ref_Number = p_web.GSV('job:Ref_Number'))
                    end ! loop
                end ! if (p_web.GSV('job:Third_Party_Site'))

                if (p_web.GSV('BookingSite') = 'RRC')
                    count# = 0
                    if (p_web.GSV('job:Chargeable_Job') = 'YES')
                        
                        Access:PARTS.Clearkey(par:Part_Number_Key)
                        par:Ref_Number    = job:Ref_Number
                        set(par:Part_Number_Key,par:Part_Number_Key)
                        loop
                            if (Access:PARTS.Next())
                                Break
                            end ! if (Access:PARTS.Next())
                            if (par:Ref_Number    <> job:Ref_Number)
                                Break
                            end ! if (par:Ref_Number    <> job:Ref_Number)
                            if (par:Part_Number <> 'EXCH')
                                count# += 1
                            end ! if (par:Part_Number <> 'EXCH')
                        end ! loop
                    end ! end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
                    if (count# > 0)
                        loc:alert = 'This job has parts attached. These must be removed before you can send it to the ARC.'
                    else
                        p_web.SSV('jobe:HubRepair',1)
                        p_web.SSV('jobe:HubRepairDate',Today())
                        p_web.SSV('jobe:HubRepairTime',Clock())
                        p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
                        p_web.SSV('GetStatus:Type','JOB')
                        getStatus(p_web)

                        Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
                        rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                        set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
                        loop
                            if (Access:REPTYDEF.Next())
                                Break
                            end ! if (Access:REPTYDEF.Next())
                            if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                                Break
                            end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                            if (rtd:BER = 10)
                                if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                                    p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                                end
                                if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                                    p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                                end
                                break
                            end ! if (rtd:BER = 10)
                        end ! loop

                        Access:JOBSENG.Clearkey(joe:UserCodeKey)
                        joe:JobNumber    = p_web.GSV('job:Ref_Number')
                        joe:UserCode    = p_web.GSV('job:Engineer')
                        joe:DateAllocated    = Today()
                        set(joe:UserCodeKey,joe:UserCodeKey)
                        loop
                            if (Access:JOBSENG.Previous())
                                Break
                            end ! if (Access:JOBSEND.Next())
                            if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
                                Break
                            end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
                            if (joe:UserCode    <> p_web.GSV('job:Engineer'))
                                Break
                            end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
                            if (joe:DateAllocated    > Today())
                                Break
                            end ! if (joe:DateAllocated    <> Today())
                            joe:Status = 'HUB'
                            joe:StatusDate = Today()
                            joe:StatusTime = Clock()
                            access:JOBSENG.tryUpdate()
                            break
                        end ! loop                                                     
                    end ! if (count# > 0)
                end ! if (p_web.GSV('BookingSite') = 'RRC')                    

                p_web.SSV('jobe:ExchangePRoductCode','')
                p_web.SSV('jobe:HandsetReplacmentValue',0)
                p_web.SSV('job:Exchange_Unit_Number',p_web.GSV('tmp:ExchangeUnitNumber'))

                if (p_web.GSV('Hide:HandsetPartNumber') = 0)
                    p_web.SSV('jobe:ExchangeProductCode',p_web.GSV('tmp:HandsetPartNumber'))
                    p_web.SSV('jobe:HandsetReplacmentValue',p_web.GSV('tmp:HandsetReplacementValue'))
                end ! if (p_web.GSV('BookingSite') = 'ARC')

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber    = p_web.GSV('job:Ref_Number')
                if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    ! Found
                    p_web.SessionQueueToFile(JOBSE)
                    access:JOBSE.tryUpdate()
                else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    ! Error
                end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

                !Access:WEBJOB.Clearkey(wob:RefNumberKey)
                !wob:RefNumber    = p_web.GSV('job:Ref_Number')
                !if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(WEBJOB)
                access:WEBJOB.tryUpdate()
                !else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                !    ! Error
                !end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)

                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number    = p_web.GSV('job:Ref_Number')
                if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    ! Found
                    p_web.SessionQueueToFile(JOBS)
                    access:JOBS.tryUpdate()
                else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)

                loc:Alert = 'Exchange Unit Added'
                
            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
clearExchangeDetails        routine
    p_web.SSV('tmp:MSN','')
    p_web.SSV('tmp:ExchangeUnitDetails','')
    p_web.SSV('tmp:ExchangeLocation','')
    p_web.SSV('tmp:ReplacementValue','')
    p_web.SSV('tmp:ExchangeIMEINumber','')
    p_web.SSV('tmp:ExchangeUnitNumber','')
    p_web.SSV('tmp:ExchangeModelNumber','')
    p_web.SSV('tmp:ExchangeAccessories','')
    p_web.SSV('tmp:ExchangeLocation','')
    p_web.SSV('tmp:HandsetPartNumber','')
    p_web.SSV('tmp:HandsetReplacementValue','')


getExchangeDetails      Routine
    p_web.SSV('locExchangeAlertMessage','')

    do lookupExchangeDetails

    if (p_web.GSV('jobe:Engineer48HourOption') = 1)
        Access:EXCHOR48.Clearkey(ex4:AttachedToJobKey)
        ex4:AttachedToJob    = 0
        ex4:Location    = p_web.GSV('BookingSiteLocation')
        ex4:JobNumber    = p_web.GSV('job:Ref_Number')
        if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
            ! Found
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number    = ex4:orderUnitNumber
            if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                ! Found
                if (p_web.GSV('tmp:exchangeIMEINumber') <> xch:ESN)
                    p_web.SSV('locExchangeAlertMessage','Warning! An Exchange Unit has been ordered for this job under the 48 Hour Exchange Process. The I.M.E.I. Number you have selected is NOT the unit that has been ordered.')
                end ! if (p_web.GSV('tmp:exchangeIMEINumber') <> xch:ESN)
            else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
            ! Error
        end ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
    end ! if (p_web.GSV('jobe:Engineer48HourOption') = 1)

    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
    if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
    if (xch:Model_Number <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locExchangeAlertMessage','Warning! The selected Exchange Unit has a different Model Number!')
    end ! if (xch:Model_Number <> p_web.GSV('job:Model_Number')
lookupExchangeDetails       Routine
    p_web.SSV('Hide:HandsetPartNumber',1)
    p_web.SSV('tmp:HandsetPartNumber','')
    p_web.SSV('tmp:HandsetReplacementValue','')

    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
    if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign and xch:Ref_Number > 0)
        ! Found
        p_web.SSV('tmp:ExchangeIMEINumber',xch:ESN)
        p_web.SSV('tmp:MSN',xch:MSN)
        p_web.SSV('tmp:ExchangeUnitDetails',Clip(xch:Ref_Number) & ': ' & Clip(xch:Manufacturer) & ' ' & Clip(xch:Model_Number))
        p_web.SSV('tmp:ExchangeLocation',Clip(xch:Location) & ' / ' & Clip(xch:Stock_Type))
        p_web.SSV('tmp:exchangeModelNumber',xch:Model_Number)

        if (p_web.GSV('BookingSite') = 'ARC')
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = xch:Manufacturer
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            if (man:UseProdCodesForEXC = 1)
                p_web.SSV('Hide:HandsetPartNumber',0)
                p_web.SSV('tmp:HandsetPartNumber',p_web.GSV('jobe:ExchangeProductCode'))
                p_web.SSV('tmp:HandsetReplacementValue',p_web.GSV('jobe:HandsetReplacmentValue'))
            end ! if (man:UseProdCodesForEXC = 1)
        end ! if (p_web.GSV('job:Exchange_unit_Number') > 0 and p_web.GSV('BookingSite') = 'ARC')

    else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Error
        p_web.SSV('tmp:ExchangeIMEINumber','')
        p_web.SSV('tmp:MSN','')
        p_web.SSV('tmp:ExchangeUnitDetails','')
        p_web.SSV('tmp:ExchangeLocation','')
    end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
clearVariables      ROUTINE
    p_web.DeleteSessionValue('ReadOnly:ExchangeDespatchDetails')
OpenFiles  ROUTINE
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBEXACC)
  p_web._OpenFile(EXCHANGE_ALIAS)
  p_web._OpenFile(EXCHOR48)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(PRODCODE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(TRDBATCH)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Security Checks
      if (p_web.GSV('Job:ViewOnly') = 1)
          p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
      else !if (p_web.GSV('Job:ViewOnly') = 1)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND EXCHANGE UNIT'))
              p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
          else
              p_web.SSV('ReadOnly:ExchangeIMEINumber',0)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),' '))
      end !if (p_web.GSV('Job:ViewOnly') = 1)
  
      p_web.SSV('tmp:ExchangeUnitNumber',p_web.GSV('job:Exchange_Unit_Number'))
  
      if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          do lookupExchangeDetails
      else
          do clearExchangeDetails
          
      end
  
      p_web.SSV('locExchangeAlertMessage','')
      p_web.SSV('locRemoveAlertMessage','')
      p_web.SSV('Hide:RemovalReason',1)
      p_web.SSV('tmp:RemovalReason',0)
      p_web.SSV('tmp:NoUnitAvailable',0)
      
      
  p_web.SetValue('PickExchangeUnit_form:inited_',1)
  do RestoreMem

CancelForm  Routine
      do clearVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ReplacementValue')
    p_web.SetPicture('tmp:ReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ReplacementValue','@n14.2')
  If p_web.IfExistsValue('tmp:HandsetReplacementValue')
    p_web.SetPicture('tmp:HandsetReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:HandsetReplacementValue','@n14.2')
  If p_web.IfExistsValue('job:Exchange_Despatched')
    p_web.SetPicture('job:Exchange_Despatched','@d06b')
  End
  p_web.SetSessionPicture('job:Exchange_Despatched','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:ExchangeIMEINumber'
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(EXCHANGE)
        p_web.setsessionvalue('tmp:ExchangeUnitNumber',xch:Ref_Number)
        do getExchangeDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeAlertMessage')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Exchange_Courier'
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Exchange_Consignment_Number')
  End
  If p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
  p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage)
  p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories)
  p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation)
  p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber)
  p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue)
  p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable)
  p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number)
  p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched)
  p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User)
  p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number)
  p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('tmp:ExchangeIMEINumber')
    tmp:ExchangeIMEINumber = p_web.GetValue('tmp:ExchangeIMEINumber')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
  End
  if p_web.IfExistsValue('locExchangeAlertMessage')
    locExchangeAlertMessage = p_web.GetValue('locExchangeAlertMessage')
    p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage)
  End
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  End
  if p_web.IfExistsValue('tmp:ExchangeUnitDetails')
    tmp:ExchangeUnitDetails = p_web.GetValue('tmp:ExchangeUnitDetails')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  End
  if p_web.IfExistsValue('tmp:ExchangeAccessories')
    tmp:ExchangeAccessories = p_web.GetValue('tmp:ExchangeAccessories')
    p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories)
  End
  if p_web.IfExistsValue('tmp:ExchangeLocation')
    tmp:ExchangeLocation = p_web.GetValue('tmp:ExchangeLocation')
    p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation)
  End
  if p_web.IfExistsValue('tmp:HandsetPartNumber')
    tmp:HandsetPartNumber = p_web.GetValue('tmp:HandsetPartNumber')
    p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber)
  End
  if p_web.IfExistsValue('tmp:ReplacementValue')
    tmp:ReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:ReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  End
  if p_web.IfExistsValue('tmp:HandsetReplacementValue')
    tmp:HandsetReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:HandsetReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue)
  End
  if p_web.IfExistsValue('tmp:NoUnitAvailable')
    tmp:NoUnitAvailable = p_web.GetValue('tmp:NoUnitAvailable')
    p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable)
  End
  if p_web.IfExistsValue('job:Exchange_Courier')
    job:Exchange_Courier = p_web.GetValue('job:Exchange_Courier')
    p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  End
  if p_web.IfExistsValue('job:Exchange_Consignment_Number')
    job:Exchange_Consignment_Number = p_web.GetValue('job:Exchange_Consignment_Number')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number)
  End
  if p_web.IfExistsValue('job:Exchange_Despatched')
    job:Exchange_Despatched = p_web.dformat(clip(p_web.GetValue('job:Exchange_Despatched')),'@d06b')
    p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched)
  End
  if p_web.IfExistsValue('job:Exchange_Despatched_User')
    job:Exchange_Despatched_User = p_web.GetValue('job:Exchange_Despatched_User')
    p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User)
  End
  if p_web.IfExistsValue('job:Exchange_Despatch_Number')
    job:Exchange_Despatch_Number = p_web.GetValue('job:Exchange_Despatch_Number')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number)
  End
  if p_web.IfExistsValue('locRemovalAlertMessage')
    locRemovalAlertMessage = p_web.GetValue('locRemovalAlertMessage')
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  End
  if p_web.IfExistsValue('tmp:RemovalReason')
    tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickExchangeUnit_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('job:Loan_Unit_Number') > 0 And ((p_web.GSV('BookingSite') = 'RRC' And p_web.GSV('jobe:Despatched') = 'REA' And p_web.GSV('jobe:DespatchedType') = 'LOA') Or |
          (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('job:Despatched') = 'REA' And p_web.GSV('job:Despatch_Type') = 'LOA')))
          packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("You cannot attached an Exchange Unit until the Loan Unit has been despatched.")</script>'
          do sendPacket
          p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
      End !
  
      p_web.SSV('ReadOnly:ExchangeDespatchDetails',1)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ExchangeIMEINumber = p_web.RestoreValue('tmp:ExchangeIMEINumber')
 locExchangeAlertMessage = p_web.RestoreValue('locExchangeAlertMessage')
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:ExchangeUnitDetails = p_web.RestoreValue('tmp:ExchangeUnitDetails')
 tmp:ExchangeAccessories = p_web.RestoreValue('tmp:ExchangeAccessories')
 tmp:ExchangeLocation = p_web.RestoreValue('tmp:ExchangeLocation')
 tmp:HandsetPartNumber = p_web.RestoreValue('tmp:HandsetPartNumber')
 tmp:ReplacementValue = p_web.RestoreValue('tmp:ReplacementValue')
 tmp:HandsetReplacementValue = p_web.RestoreValue('tmp:HandsetReplacementValue')
 tmp:NoUnitAvailable = p_web.RestoreValue('tmp:NoUnitAvailable')
 locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
 tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickExchangeUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickExchangeUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickExchangeUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickExchangeUnit" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickExchangeUnit" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickExchangeUnit" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Pick Exchange Unit') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Pick Exchange Unit',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickExchangeUnit">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickExchangeUnit" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickExchangeUnit')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Exchange Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Details') & ''''
        If p_web.GSV('job:Exchange_Unit_Number') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Remove Exchange Unit') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickExchangeUnit')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickExchangeUnit'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='EXCHANGE'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Exchange_Consignment_Number')
    End
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          If p_web.GSV('job:Exchange_Unit_Number') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickExchangeUnit')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    if p_web.GSV('job:Exchange_Unit_Number') > 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Exchange Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('ReadOnly:ExchangeIMEINumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPickExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPickExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('tmp:MSN') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:HandsetPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:HandsetPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:HandsetPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ReplacementValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:HandsetReplacementValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:HandsetReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:HandsetReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:NoUnitAvailable
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:NoUnitAvailable
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:NoUnitAvailable
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Consignment_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Despatched
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Despatched_User
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Despatch_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
  If p_web.GSV('job:Exchange_Unit_Number') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Remove Exchange Unit') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locRemoveText
      do Comment::locRemoveText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonRemoveAttachedUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRemovalAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="5" valign="top">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RemovalReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonConfirmExchangeRemoval
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmExchangeRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmExchangeRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::job:ESN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUnitDetails',p_web.GetValue('NewValue'))
    do Value::locUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type')
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Warranty_Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeIMEINumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange IMEI No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',p_web.GetValue('NewValue'))
    tmp:ExchangeIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',p_web.GetValue('Value'))
    tmp:ExchangeIMEINumber = p_web.GetValue('Value')
  End
    tmp:ExchangeIMEINumber = Upper(tmp:ExchangeIMEINumber)
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
      ! Validate IMEI
      p_web.SSV('locExchangeAlertMessage','')
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN    = p_web.GSV('tmp:ExchangeIMEINumber')
      if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
          ! Found
          if (xch:Location <> p_web.GSV('BookingSiteLocation'))
              p_web.SSV('locExchangeAlertMessage','Selected IMEI is from a different location.')
          else !  !if (xch:Location <> p_web.GSV('BookingSiteLocation'))
              if (xch:Available = 'AVL')
                  Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
                  stp:Stock_Type    = xch:Stock_Type
                  if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                      ! Found
                      if (stp:Available <> 1)
                          p_web.SSV('locExchangeAlertMessage','Selected Stock Type is not available')
                      else ! if (stp:Available <> 1)
                          p_web.SSV('tmp:ExchangeUnitNumber',xch:Ref_Number)
                      end ! if (stp:Available <> 1)
                  else ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
              else ! if (xch:Available = 'AVL')
                  p_web.SSV('locExchangeAlertMessage','Selected IMEI is not available')
              end ! if (xch:Available = 'AVL')
          end !if (xch:Location <> p_web.GSV('BookingSiteLocation'))
      else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
          ! Error
          p_web.SSV('locExchangeAlertMessage','Cannot find the selected IMEI Number')
      end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
  
      if (p_web.GSV('locExchangeAlertMessage') = '')
          do getExchangeDetails
      else
          do clearExchangeDetails
      end ! if (p_web.GSV('locExchangeAlertMessage') = '')
  p_Web.SetValue('lookupfield','tmp:ExchangeIMEINumber')
  do AfterLookup
  do Value::tmp:ExchangeIMEINumber
  do SendAlert
  do Comment::tmp:ExchangeIMEINumber
  do Value::tmp:ExchangeLocation  !1
  do Value::tmp:ExchangeUnitDetails  !1
  do Value::tmp:ReplacementValue  !1
  do Prompt::tmp:HandsetPartNumber
  do Value::tmp:HandsetPartNumber  !1
  do Prompt::tmp:HandsetReplacementValue
  do Value::tmp:HandsetReplacementValue  !1
  do Prompt::tmp:ReplacementValue
  do Value::tmp:ReplacementValue  !1
  do Prompt::tmp:NoUnitAvailable
  do Value::tmp:NoUnitAvailable  !1
  do Prompt::locExchangeAlertMessage
  do Value::locExchangeAlertMessage  !1

Value::tmp:ExchangeIMEINumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ExchangeIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ExchangeIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeIMEINumber'',''pickexchangeunit_tmp:exchangeimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeIMEINumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:ExchangeIMEINumber',p_web.GetSessionValueFormat('tmp:ExchangeIMEINumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('FormExchangeUnitFilter')&'?LookupField=tmp:ExchangeIMEINumber&Tab=3&ForeignField=xch:ESN&_sort=xch:ESN&Refresh=sort&LookupFrom=PickExchangeUnit&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_value')

Comment::tmp:ExchangeIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_comment')

Prompt::locExchangeAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_prompt')

Validate::locExchangeAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeAlertMessage',p_web.GetValue('NewValue'))
    locExchangeAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeAlertMessage',p_web.GetValue('Value'))
    locExchangeAlertMessage = p_web.GetValue('Value')
  End

Value::locExchangeAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locExchangeAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate(p_web.GSV('locExchangeAlertMessage'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_value')

Comment::locExchangeAlertMessage  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPickExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPickExchangeUnit',p_web.GetValue('NewValue'))
    do Value::buttonPickExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPickExchangeUnit
  do SendAlert
  do Value::tmp:HandsetPartNumber  !1

Value::buttonPickExchangeUnit  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_value',Choose(1 or p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (1 or p_web.GSV('job:Exchange_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPickExchangeUnit'',''pickexchangeunit_buttonpickexchangeunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PickExchangeUnit','Pick Exchange Unit','SmallButtonFixedIcon',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormExchangeUnitFilter?LookupField=tmp:ExchangeIMEINumber&Tab=2&ForeignField=xch:ESN&_sort=xch:ESN&Refresh=sort&LookupFrom=PickExchangeUnit&')) & ''','''&clip('_self')&''')',loc:javascript,0,'images\packinsert.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_value')

Comment::buttonPickExchangeUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_comment',Choose(1 or p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If 1 or p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('NewValue'))
    tmp:MSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('Value'))
    tmp:MSN = p_web.GetValue('Value')
  End

Value::tmp:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:MSN'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_value')

Comment::tmp:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',p_web.GetValue('NewValue'))
    tmp:ExchangeUnitDetails = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',p_web.GetValue('Value'))
    tmp:ExchangeUnitDetails = p_web.GetValue('Value')
  End

Value::tmp:ExchangeUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeUnitDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeUnitDetails'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_value')

Comment::tmp:ExchangeUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeAccessories  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeAccessories',p_web.GetValue('NewValue'))
    tmp:ExchangeAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeAccessories',p_web.GetValue('Value'))
    tmp:ExchangeAccessories = p_web.GetValue('Value')
  End

Value::tmp:ExchangeAccessories  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeAccessories
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeAccessories'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_value')

Comment::tmp:ExchangeAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeLocation  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Location / Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeLocation',p_web.GetValue('NewValue'))
    tmp:ExchangeLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeLocation',p_web.GetValue('Value'))
    tmp:ExchangeLocation = p_web.GetValue('Value')
  End

Value::tmp:ExchangeLocation  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeLocation'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_value')

Comment::tmp:ExchangeLocation  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:HandsetPartNumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_prompt',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Handset Part No')
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_prompt')

Validate::tmp:HandsetPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:HandsetPartNumber',p_web.GetValue('NewValue'))
    tmp:HandsetPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:HandsetPartNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:HandsetPartNumber',p_web.GetValue('Value'))
    tmp:HandsetPartNumber = p_web.GetValue('Value')
  End
  Access:PRODCODE.Clearkey(prd:ModelProductKey)
  prd:ModelNumber    = p_web.GSV('tmp:ExchangeModelNumber')
  prd:ProductCode    = p_web.GSV('tmp:HandsetPartNumber')
  if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
      ! Found
      p_web.SSV('tmp:HandsetReplacementValue',prd:HandsetReplacementValue)
  else ! if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
      ! Error
      p_web.SSV('tmp:HandsetReplacementValue','')
  end ! if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
  do Value::tmp:HandsetPartNumber
  do SendAlert
  do Value::tmp:HandsetReplacementValue  !1

Value::tmp:HandsetPartNumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_value',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:HandsetPartNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:HandsetPartNumber'',''pickexchangeunit_tmp:handsetpartnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:HandsetPartNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:HandsetPartNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:HandsetPartNumber') = 0
    p_web.SetSessionValue('tmp:HandsetPartNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:HandsetPartNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(EXCHANGE_ALIAS)
  bind(xch_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(TRDBATCH)
  bind(trb:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:HandsetPartNumber_OptionView)
  tmp:HandsetPartNumber_OptionView{prop:filter} = 'Upper(prd:ModelNumber) = Upper(<39>' & p_web.GSV('tmp:ExchangeModelNumber') & '<39>)'
  tmp:HandsetPartNumber_OptionView{prop:order} = 'UPPER(prd:ProductCode)'
  Set(tmp:HandsetPartNumber_OptionView)
  Loop
    Next(tmp:HandsetPartNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:HandsetPartNumber') = 0
      p_web.SetSessionValue('tmp:HandsetPartNumber',prd:ProductCode)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(clip(prd:ProductCode) & '  (' & format(prd:HandsetReplacementValue,@n_14.2) & '  )',prd:ProductCode,choose(prd:ProductCode = p_web.getsessionvalue('tmp:HandsetPartNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:HandsetPartNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_value')

Comment::tmp:HandsetPartNumber  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_comment',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Replacement Value')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt')

Validate::tmp:ReplacementValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ReplacementValue',p_web.GetValue('NewValue'))
    tmp:ReplacementValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ReplacementValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ReplacementValue',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::tmp:ReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ReplacementValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('tmp:ReplacementValue'),'@n14.2')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value')

Comment::tmp:ReplacementValue  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:HandsetReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_prompt',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Replacement Value')
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_prompt')

Validate::tmp:HandsetReplacementValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',p_web.GetValue('NewValue'))
    tmp:HandsetReplacementValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:HandsetReplacementValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:HandsetReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:HandsetReplacementValue
  do SendAlert

Value::tmp:HandsetReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_value',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
  ! --- STRING --- tmp:HandsetReplacementValue
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:HandsetReplacementValue')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:HandsetReplacementValue'',''pickexchangeunit_tmp:handsetreplacementvalue_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:HandsetReplacementValue',p_web.GetSessionValue('tmp:HandsetReplacementValue'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_value')

Comment::tmp:HandsetReplacementValue  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_comment',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:NoUnitAvailable  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_prompt',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('No Unit Available')
  If p_web.GSV('tmp:ExchangeUnitNumber') > 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_prompt')

Validate::tmp:NoUnitAvailable  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:NoUnitAvailable',p_web.GetValue('NewValue'))
    tmp:NoUnitAvailable = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:NoUnitAvailable
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:NoUnitAvailable',p_web.GetValue('Value'))
    tmp:NoUnitAvailable = p_web.GetValue('Value')
  End

Value::tmp:NoUnitAvailable  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_value',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
  ! --- CHECKBOX --- tmp:NoUnitAvailable
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0,'disabled','')
  If p_web.GetSessionValue('tmp:NoUnitAvailable') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:NoUnitAvailable',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_value')

Comment::tmp:NoUnitAvailable  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_comment',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ExchangeUnitNumber') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Courier  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Courier',p_web.GetValue('NewValue'))
    job:Exchange_Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Courier
    do Value::job:Exchange_Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Courier = p_web.GetValue('Value')
  End
    job:Exchange_Courier = Upper(job:Exchange_Courier)
    p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  do Value::job:Exchange_Courier
  do SendAlert

Value::job:Exchange_Courier  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Exchange_Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Exchange_Courier'',''pickexchangeunit_job:exchange_courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Exchange_Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('job:Exchange_Courier') = 0
    p_web.SetSessionValue('job:Exchange_Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('job:Exchange_Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(EXCHANGE_ALIAS)
  bind(xch_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(TRDBATCH)
  bind(trb:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Exchange_Courier_OptionView)
  job:Exchange_Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Exchange_Courier_OptionView)
  Loop
    Next(job:Exchange_Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Exchange_Courier') = 0
      p_web.SetSessionValue('job:Exchange_Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Exchange_Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Exchange_Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_value')

Comment::job:Exchange_Courier  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Consignment_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Consignment_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',p_web.GetValue('NewValue'))
    job:Exchange_Consignment_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Consignment_Number
    do Value::job:Exchange_Consignment_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Consignment_Number = p_web.GetValue('Value')
  End

Value::job:Exchange_Consignment_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Consignment_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Consignment_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Consignment_Number',p_web.GetSessionValueFormat('job:Exchange_Consignment_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_value')

Comment::job:Exchange_Consignment_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Despatched  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Despatched  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Despatched',p_web.GetValue('NewValue'))
    job:Exchange_Despatched = p_web.GetValue('NewValue') !FieldType= DATE Field = job:Exchange_Despatched
    do Value::job:Exchange_Despatched
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Despatched',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:Exchange_Despatched = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::job:Exchange_Despatched  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Despatched
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Despatched')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatched',p_web.GetSessionValue('job:Exchange_Despatched'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_value')

Comment::job:Exchange_Despatched  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Despatched_User  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('User')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Despatched_User  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Despatched_User',p_web.GetValue('NewValue'))
    job:Exchange_Despatched_User = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Despatched_User
    do Value::job:Exchange_Despatched_User
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Despatched_User',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Exchange_Despatched_User = p_web.GetValue('Value')
  End

Value::job:Exchange_Despatched_User  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Despatched_User
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Despatched_User')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatched_User',p_web.GetSessionValueFormat('job:Exchange_Despatched_User'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s3'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_value')

Comment::job:Exchange_Despatched_User  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Despatch_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Despatch_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',p_web.GetValue('NewValue'))
    job:Exchange_Despatch_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = job:Exchange_Despatch_Number
    do Value::job:Exchange_Despatch_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    job:Exchange_Despatch_Number = p_web.GetValue('Value')
  End

Value::job:Exchange_Despatch_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Despatch_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Despatch_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatch_Number',p_web.GetSessionValueFormat('job:Exchange_Despatch_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_value')

Comment::job:Exchange_Despatch_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:AmendDespatchDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AmendDespatchDetails',p_web.GetValue('NewValue'))
    do Value::button:AmendDespatchDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      if (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          p_web.SSV('ReadOnly:ExchangeDespatchDetails',0)
      ELSE
          p_web.SSV('ReadOnly:ExchangeDespatchDetails',1)
      END
  do Value::button:AmendDespatchDetails
  do SendAlert
  do Value::job:Exchange_Consignment_Number  !1
  do Value::job:Exchange_Despatch_Number  !1
  do Value::job:Exchange_Despatched  !1
  do Value::job:Exchange_Despatched_User  !1

Value::button:AmendDespatchDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value',Choose(p_web.GSV('job:Exchange_Consignment_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Consignment_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AmendDespatchDetails'',''pickexchangeunit_button:amenddespatchdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AmendDespatchDetails','Amend Despatch Details','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value')

Comment::button:AmendDespatchDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_comment',Choose(p_web.GSV('job:Exchange_Consignment_Number') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Exchange_Consignment_Number') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locRemoveText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemoveText',p_web.GetValue('NewValue'))
    do Value::locRemoveText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locRemoveText  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_value',Choose(p_web.GSV('job:exchange_unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:exchange_unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('You must remove the existing exchange before you can add a new one',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_value')

Comment::locRemoveText  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_comment',Choose(p_web.GSV('job:exchange_unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:exchange_unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonRemoveAttachedUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRemoveAttachedUnit',p_web.GetValue('NewValue'))
    do Value::buttonRemoveAttachedUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('Hide:RemovalReason',0)
  
  if (p_web.GSV('job:exchange_Consignment_Number') <> '')
      p_web.SSV('locRemoveAlertMessage','Warning!<br>The Exchange Unit has already been despatched. '&|
                          'If the continue the unit will be removed and returned to Exchange Stock.')
  end ! if (p_web.GSV('job:exchange_Consignment_Number') <> '')
  do SendAlert
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::buttonConfirmExchangeRemoval  !1
  do Value::buttonRemoveAttachedUnit  !1
  do Prompt::locRemovalAlertMessage
  do Value::locRemovalAlertMessage  !1

Value::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAttachedUnit'',''pickexchangeunit_buttonremoveattachedunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveExchangeUnit','Remove Exchange Unit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value')

Comment::buttonRemoveAttachedUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Alert')
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt')

Validate::locRemovalAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('NewValue'))
    locRemovalAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRemovalAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('Value'))
    locRemovalAlertMessage = p_web.GetValue('Value')
  End
  do Value::locRemovalAlertMessage
  do SendAlert

Value::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locRemovalAlertMessage') = '')
  ! --- TEXT --- locRemovalAlertMessage
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locRemovalAlertMessage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRemovalAlertMessage'',''pickexchangeunit_locremovalalertmessage_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locRemovalAlertMessage',p_web.GetSessionValue('locRemovalAlertMessage'),5,25,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locRemovalAlertMessage),,Net:Web:Frame) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value')

Comment::locRemovalAlertMessage  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_comment',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RemovalReason  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Removal Reason')
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt')

Validate::tmp:RemovalReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('NewValue'))
    tmp:RemovalReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RemovalReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('Value'))
    tmp:RemovalReason = p_web.GetValue('Value')
  End
  do Value::tmp:RemovalReason
  do SendAlert
  do Value::buttonConfirmExchangeRemoval  !1

Value::tmp:RemovalReason  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- RADIO --- tmp:RemovalReason
  loc:fieldclass = Choose(sub(' noButton',1,1) = ' ',clip('FormEntry') & ' noButton',' noButton')
  If lower(loc:invalid) = lower('tmp:RemovalReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('jobe:Engineer48HourOption') = 1) then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('tmp:RemovalReason') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replace Faulty Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('jobe:Engineer48HourOption') = 1) then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('tmp:RemovalReason') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Alternative Model Required') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value')

Comment::tmp:RemovalReason  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonConfirmExchangeRemoval  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmExchangeRemoval  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmExchangeRemoval',p_web.GetValue('NewValue'))
    do Value::buttonConfirmExchangeRemoval
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Remove Exchange Unit
      p_web.SSV('AddToAudit:Type','EXC')
      if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT HAD BEEN DESPATCHED: ' & |
              '<13,10>UNIT NUMBER: ' & p_web.GSV('job:Exchange_unit_number') & |
              '<13,10>COURIER: ' & p_web.GSV('job:exchange_Courier') & |
              '<13,10>CONSIGNMENT NUMBER: ' & p_web.GSV('job:exchange_consignment_number') & |
              '<13,10>DATE DESPATCHED: ' & Format(p_web.GSV('job:Exchange_despatched'),@d6) &|
              '<13,10>DESPATCH USER: ' & p_web.GSV('job:Exchange_despatched_user') &|
              '<13,10>DESPATCH NUMBER: ' & p_web.GSV('job:exchange_despatch_number'))
      else ! if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT NUMBER: ' & p_web.GSV('job:exchange_unit_Number'))
      end !if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
      case p_web.GSV('tmp:RemovalReason')
      of 1
          p_web.SSV('AddToAudit:Action','REPLACED FAULTY EXCHANGE UNIT')
      of 2
          p_web.SSV('AddToAudit:Action','ALTERNATIVE EXCHANGE MODEL REQUIRED')
      of 3
          p_web.SSV('AddToAudit:Action','EXCHANGE UNIT NOT REQUIRED: RE-STOCKED')
      end ! case p_web.GSV('tmp:RemovalReason')
  
      addToAudit(p_web)
  
      !p_web.SSV('job:Exchange_Unit_Number','')
      p_web.SSV('job:Exchange_Accessory','')
      p_web.SSV('job:Exchange_Consignment_Number','')
      p_web.SSV('job:Exchange_Despatched','')
      p_web.SSV('job:exchange_Despatched_User','')
      p_web.SSV('job:Exchange_Issued_Date','')
      p_web.SSV('job:Exchange_User','')
      if (p_web.GSV('job:Despatch_Type') = 'EXC')
          p_web.SSV('job:Despatched','NO')
          p_web.SSV('job:Despatch_type','')
      else ! if (p_web.GSV('job:Despatch_Type') = 'EXC')
          if (p_web.GSV('jobe:DespatchType') = 'EXC')
              p_web.SSV('jobe:DespatchType','')
              p_web.SSV('wob:ReadyToDespatch',0)
          end ! if (p_web.GSV('jobe:DespatchType') = 'EXC')
      end !
  
      p_web.SSV('GetStatus:Type','EXC')
      p_web.SSV('GetStatus:StatusNumber',101)
      getStatus(p_web)
  
      restock# = 0
      Access:PARTS.Clearkey(par:Part_Number_Key)
      par:Ref_Number    = p_web.GSV('job:Ref_Number')
      par:Part_Number    = 'EXCH'
      SET(par:Part_Number_Key,par:part_Number_key)
      loop
          if (Access:parts.Next())
              break
          end
          if (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
              break
          end
          if (par:Part_Number <> 'EXCH')
              break
          end
          ! Found
          removeFromStockAllocation(par:Record_Number,'CHA')
          access:PARTS.deleterecord(0)
          restock# = 1
      end ! if (Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign)
  
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = p_web.GSV('job:Ref_Number')
      wpr:Part_Number  = 'EXCH'
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      loop
          if (Access:WARPARTS.Next())
              break
          end
          if (wpr:Ref_Number <> p_web.GSV('job:Ref_Number'))
              break
          end
          if (wpr:Part_Number <> 'EXCH')
              break
          end
          
          RemoveFromStockAllocation(wpr:Record_Number,'WAR')
          Access:WARPARTS.Deleterecord(0)
          restock# = 1
      end
  
      ! Remove incoming unit
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN    = p_web.GSV('job:ESN')
      set(xch:ESN_Only_Key,xch:ESN_Only_Key)
      loop
          if (Access:EXCHANGE.Next())
              Break
          end ! if (Access:EXCHANGE.Next())
          if (xch:ESN    <> p_web.GSV('job:ESN'))
              Break
          end ! if (xch:ESN    <> p_web.GSV('job:ESN'))
          if (xch:Job_Number = p_web.GSV('job:Ref_Number'))
              relate:EXCHANGE.delete(0)
          end ! if (xch:Job_Number = p_web.GSV('job:Ref_Number'))
      end ! loop
  
  !    ! Dont think this is necessary?!
  !    if (restock# = 1)
  !        local.AllocateExchangePart('RTS',0)
  !    end ! if (restock# = 1)
  
  
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number    = p_web.GSV('job:Exchange_Unit_Number')
      if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ! Found
          if (p_web.GSV('tmp:RemovalReason') = 1)
                xch:Available = 'INR'
                xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
              p_web.SSV('locExchangeAlertMessage','Please book the replaced Exchange Unit as a new job')
          else ! if (p_web.GSV('tmp:RemovalReason') = 1)
              if (xch:Available <> 'QAF')
                    xch:available = 'AVL'
                    xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
              end ! if (xch:Available <> 'QAF')
          end ! if (p_web.GSV('tmp:RemovalReason') = 1)
          xch:Job_Number = ''
          access:EXCHANGE.tryUpdate()
          if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
              exh:Ref_Number    = p_web.GSV('job:Exchange_Unit_Number')
              exh:Date = today()
              exh:Time = clock()
              exh:User = p_web.GSV('BookingUserCode')
              if (p_web.GSV('tmp:RemovalReason') = 1)
                  exh:Status = 'FAULTY UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
              else ! if (p_web.GSV('tmp:RemovalReason') = 1)
                  exh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
              end !if (p_web.GSV('tmp:RemovalReason') = 1)
              if (Access:EXCHHIST.TryInsert() = Level:Benign)
                  ! Inserted
              else ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                  ! Error
                  Access:EXCHHIST.CancelAutoInc()
              end ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
          end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
      else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
  
      p_web.SSV('job:Exchange_Unit_Number','')
  
  
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number    = p_web.GSV('job:Ref_Number')
      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBS)
          access:JOBS.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = p_web.GSV('job:Ref_Number')
      if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBSE)
          access:JOBSE.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      p_web.SessionQueueToFile(WEBJOB)
      access:WEBJOB.TryUpdate()
  
      p_web.SSV('tmp:ExchangeUnitNumber',0)
  
      do clearExchangeDetails
  
      p_web.SSV('Hide:RemovalReason',1)
  do Value::buttonConfirmExchangeRemoval
  do SendAlert
  do Value::buttonPickExchangeUnit  !1
  do Value::locExchangeAlertMessage  !1
  do Value::locRemovalAlertMessage  !1
  do Value::tmp:ExchangeAccessories  !1
  do Value::tmp:ExchangeIMEINumber  !1
  do Value::tmp:ExchangeLocation  !1
  do Value::tmp:ExchangeUnitDetails  !1
  do Value::tmp:MSN  !1
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::locRemoveText  !1

Value::buttonConfirmExchangeRemoval  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmExchangeRemoval'',''pickexchangeunit_buttonconfirmexchangeremoval_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmExchangeRemoval','Confirm Exchange Removal','button-entryfield',loc:formname,,,,loc:javascript,0,'images\packdelete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_value')

Comment::buttonConfirmExchangeRemoval  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickExchangeUnit_tmp:ExchangeIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeIMEINumber
      else
        do Value::tmp:ExchangeIMEINumber
      end
  of lower('PickExchangeUnit_buttonPickExchangeUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPickExchangeUnit
      else
        do Value::buttonPickExchangeUnit
      end
  of lower('PickExchangeUnit_tmp:HandsetPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:HandsetPartNumber
      else
        do Value::tmp:HandsetPartNumber
      end
  of lower('PickExchangeUnit_tmp:HandsetReplacementValue_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:HandsetReplacementValue
      else
        do Value::tmp:HandsetReplacementValue
      end
  of lower('PickExchangeUnit_job:Exchange_Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Exchange_Courier
      else
        do Value::job:Exchange_Courier
      end
  of lower('PickExchangeUnit_button:AmendDespatchDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AmendDespatchDetails
      else
        do Value::button:AmendDespatchDetails
      end
  of lower('PickExchangeUnit_buttonRemoveAttachedUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAttachedUnit
      else
        do Value::buttonRemoveAttachedUnit
      end
  of lower('PickExchangeUnit_locRemovalAlertMessage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRemovalAlertMessage
      else
        do Value::locRemovalAlertMessage
      end
  of lower('PickExchangeUnit_tmp:RemovalReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RemovalReason
      else
        do Value::tmp:RemovalReason
      end
  of lower('PickExchangeUnit_buttonConfirmExchangeRemoval_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmExchangeRemoval
      else
        do Value::buttonConfirmExchangeRemoval
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)

PreCopy  Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
      If(p_web.GSV('tmp:ExchangeUnitNumber') > 0)
        If (p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0)
          If p_web.IfExistsValue('tmp:NoUnitAvailable') = 0
            p_web.SetValue('tmp:NoUnitAvailable',0)
            tmp:NoUnitAvailable = 0
          End
        End
      End
  If p_web.GSV('job:Exchange_Unit_Number') > 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickExchangeUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
          if (p_web.GSV('tmp:ExchangeUnitNumber') <> p_web.GSV('job:Exchange_Unit_Number'))
              do addExchangeUnit
          end ! if (p_web.GSV('tmp:ExchangeUnitNumber') <> p_web.GSV('job:Exchange_Unit_Number'))
      else ! if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
          if (p_web.GSV('tmp:NoUnitAvailable') = 1)
              p_web.SSV('GetStatus:Type','EXC')
              p_web.SSV('GetStatus:StatusNumber',350)
              getStatus(p_web)
  
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber',355)
              getStatus(p_web)
  
              local.AllocateExchangePart('ORD',0)
          end ! if (p_web.GSV('tmp:NoUnitAvailable') = 1)
      end ! if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
      do clearVariables
  p_web.DeleteSessionValue('PickExchangeUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
          tmp:ExchangeIMEINumber = Upper(tmp:ExchangeIMEINumber)
          p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
        If loc:Invalid <> '' then exit.
  ! tab = 5
    loc:InvalidTab += 1
          job:Exchange_Courier = Upper(job:Exchange_Courier)
          p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('tmp:ExchangeIMEINumber')
  p_web.StoreValue('locExchangeAlertMessage')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('tmp:ExchangeUnitDetails')
  p_web.StoreValue('tmp:ExchangeAccessories')
  p_web.StoreValue('tmp:ExchangeLocation')
  p_web.StoreValue('tmp:HandsetPartNumber')
  p_web.StoreValue('tmp:ReplacementValue')
  p_web.StoreValue('tmp:HandsetReplacementValue')
  p_web.StoreValue('tmp:NoUnitAvailable')
  p_web.StoreValue('job:Exchange_Courier')
  p_web.StoreValue('job:Exchange_Consignment_Number')
  p_web.StoreValue('job:Exchange_Despatched')
  p_web.StoreValue('job:Exchange_Despatched_User')
  p_web.StoreValue('job:Exchange_Despatch_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locRemovalAlertMessage')
  p_web.StoreValue('tmp:RemovalReason')
  p_web.StoreValue('')
Local.AllocateExchangePart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
                If wpr:SecondExchangeUnit
                    local:FoundPart = True
                End !If wpr:SecondExchangeUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If p_web.GSV('job:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                    If par:SecondExchangeUnit
                        local:FoundPart = True
                    End !If wpr:SecondExchangeUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
!            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
BrowseExchangeUnits  PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(EXCHANGE)
                      Project(xch:Ref_Number)
                      Project(xch:ESN)
                      Project(xch:Ref_Number)
                      Project(xch:Manufacturer)
                      Project(xch:Model_Number)
                      Project(xch:MSN)
                      Project(xch:Colour)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseExchangeUnits')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseExchangeUnits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseExchangeUnits:NoForm')
      loc:FormName = p_web.GetValue('BrowseExchangeUnits:FormName')
    else
      loc:FormName = 'BrowseExchangeUnits_frm'
    End
    p_web.SSV('BrowseExchangeUnits:NoForm',loc:NoForm)
    p_web.SSV('BrowseExchangeUnits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseExchangeUnits:NoForm')
    loc:FormName = p_web.GSV('BrowseExchangeUnits:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseExchangeUnits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseExchangeUnits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(EXCHANGE,xch:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'XCH:ESN') then p_web.SetValue('BrowseExchangeUnits_sort','1')
    ElsIf (loc:vorder = 'XCH:REF_NUMBER') then p_web.SetValue('BrowseExchangeUnits_sort','3')
    ElsIf (loc:vorder = 'XCH:MANUFACTURER') then p_web.SetValue('BrowseExchangeUnits_sort','7')
    ElsIf (loc:vorder = 'XCH:MODEL_NUMBER') then p_web.SetValue('BrowseExchangeUnits_sort','4')
    ElsIf (loc:vorder = 'XCH:MSN') then p_web.SetValue('BrowseExchangeUnits_sort','5')
    ElsIf (loc:vorder = 'XCH:COLOUR') then p_web.SetValue('BrowseExchangeUnits_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseExchangeUnits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseExchangeUnits:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseExchangeUnits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseExchangeUnits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseExchangeUnits:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseExchangeUnits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseExchangeUnits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:ESN)','-UPPER(xch:ESN)')
    Loc:LocateField = 'xch:ESN'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'xch:Ref_Number','-xch:Ref_Number')
    Loc:LocateField = 'xch:Ref_Number'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Manufacturer)','-UPPER(xch:Manufacturer)')
    Loc:LocateField = 'xch:Manufacturer'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Model_Number)','-UPPER(xch:Model_Number)')
    Loc:LocateField = 'xch:Model_Number'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:MSN)','-UPPER(xch:MSN)')
    Loc:LocateField = 'xch:MSN'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Colour)','-UPPER(xch:Colour)')
    Loc:LocateField = 'xch:Colour'
  of 2
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(xch:Available),+UPPER(xch:Location),+UPPER(xch:ESN)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND (p_web.GSV('tmp:ExchangeModelNumber') = '' OR p_web.GSV('tmp:ExchangeManufacturer') = ''))
  ElsIf (p_web.GSV('tmp:ExchangeStockType') = '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('xch:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Ref_Number')
    loc:SortHeader = p_web.Translate('Unit Number')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s8')
  Of upper('xch:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:MSN')
    loc:SortHeader = p_web.Translate('M.S.N.')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Colour')
    loc:SortHeader = p_web.Translate('Colour')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseExchangeUnits:LookupFrom')
  End!Else
  loc:formaction = 'BrowseExchangeUnits'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseExchangeUnits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseExchangeUnits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseExchangeUnits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="EXCHANGE"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="xch:Ref_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeUnits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseExchangeUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseExchangeUnits.locate(''Locator2BrowseExchangeUnits'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseExchangeUnits.cl(''BrowseExchangeUnits'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseExchangeUnits_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseExchangeUnits_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseExchangeUnits','I.M.E.I. Number','Click here to sort by I.M.E.I.',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by I.M.E.I.')&'">'&p_web.Translate('I.M.E.I. Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseExchangeUnits','Unit Number','Click here to sort by Unit Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Unit Number')&'">'&p_web.Translate('Unit Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseExchangeUnits','Manufacturer','Click here to sort by Manufacturer',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Manufacturer')&'">'&p_web.Translate('Manufacturer')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseExchangeUnits','Model Number','Click here to sort by Model Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Model Number')&'">'&p_web.Translate('Model Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseExchangeUnits','M.S.N.','Click here to sort by M.S.N.',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by M.S.N.')&'">'&p_web.Translate('M.S.N.')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseExchangeUnits','Colour','Click here to sort by Colour',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Colour')&'">'&p_web.Translate('Colour')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Select
        do AddPacket
        loc:columns += 1
    End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('xch:ref_number',lower(Thisview{prop:order}),1,1) = 0 !and EXCHANGE{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'xch:Ref_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('xch:Ref_Number'),p_web.GetValue('xch:Ref_Number'),p_web.GetSessionValue('xch:Ref_Number'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
      loc:FilterWas = 'Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(xch:Stock_Type) = Upper(''' & p_web.GSV('tmp:ExchangeStockType') & ''') AND Upper(xch:Available) = ''AVL'' AND Upper(xch:Model_Number) = Upper(''' & p_web.GSV('tmp:ExchangeModelNumber') & ''')'
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND (p_web.GSV('tmp:ExchangeModelNumber') = '' OR p_web.GSV('tmp:ExchangeManufacturer') = ''))
      loc:FilterWas = 'Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(xch:Stock_Type) = Upper(''' & p_web.GSV('tmp:ExchangeStockType') & ''') AND Upper(xch:Available) = ''AVL'''
  ElsIf (p_web.GSV('tmp:ExchangeStockType') = '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
      loc:FilterWas = 'Upper(xch:Available) = ''AVL'' AND Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(xch:Model_Number) = Upper(''' & p_web.GSV('tmp:ExchangeModelNumber') & ''')'
  Else
        loc:FilterWas = 'Upper(xch:Available) = ''AVL'' AND Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeUnits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseExchangeUnits_Filter')
    p_web.SetSessionValue('BrowseExchangeUnits_FirstValue','')
    p_web.SetSessionValue('BrowseExchangeUnits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,EXCHANGE,xch:Ref_Number_Key,loc:PageRows,'BrowseExchangeUnits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If EXCHANGE{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(EXCHANGE,loc:firstvalue)
              Reset(ThisView,EXCHANGE)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If EXCHANGE{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(EXCHANGE,loc:lastvalue)
            Reset(ThisView,EXCHANGE)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(xch:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseExchangeUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseExchangeUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseExchangeUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseExchangeUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeUnits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseExchangeUnits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseExchangeUnits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseExchangeUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseExchangeUnits.locate(''Locator1BrowseExchangeUnits'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseExchangeUnits.cl(''BrowseExchangeUnits'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseExchangeUnits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseExchangeUnits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseExchangeUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseExchangeUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseExchangeUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseExchangeUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = xch:Ref_Number
    p_web._thisrow = p_web._nocolon('xch:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseExchangeUnits:LookupField')) = xch:Ref_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((xch:Ref_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseExchangeUnits.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If EXCHANGE{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(EXCHANGE)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If EXCHANGE{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(EXCHANGE)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','xch:Ref_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseExchangeUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','xch:Ref_Number',clip(loc:field),,'checked',,,'onclick="BrowseExchangeUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:MSN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:Colour
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseExchangeUnits.omv(this);" onMouseOut="BrowseExchangeUnits.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseExchangeUnits=new browseTable(''BrowseExchangeUnits'','''&clip(loc:formname)&''','''&p_web._jsok('xch:Ref_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('xch:Ref_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseExchangeUnits.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseExchangeUnits.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseExchangeUnits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseExchangeUnits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseExchangeUnits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseExchangeUnits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(EXCHANGE)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(EXCHANGE)
  Bind(xch:Record)
  Clear(xch:Record)
  NetWebSetSessionPics(p_web,EXCHANGE)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('xch:Ref_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::xch:ESN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:ESN_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:ESN,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Ref_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:Ref_Number_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:Ref_Number,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Manufacturer   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:Manufacturer_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:Manufacturer,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Model_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:Model_Number_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:Model_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:MSN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:MSN_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:MSN,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Colour   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:Colour_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:Colour,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&xch:Ref_Number,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseExchangeUnits',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseExchangeUnits',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseExchangeUnits',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = xch:Ref_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('xch:Ref_Number',xch:Ref_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('xch:Ref_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('xch:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('xch:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormExchangeUnitFilter PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ExchangeStockType STRING(30)                           !
tmp:ExchangeModelNumber STRING(30)                         !
tmp:ExchangeManufacturer STRING(30)                        !
FilesOpened     Long
STOCKTYP::State  USHORT
MODELNUM::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
tmp:ExchangeStockType_OptionView   View(STOCKTYP)
                          Project(stp:Stock_Type)
                        End
tmp:ExchangeManufacturer_OptionView   View(MANUFACT)
                          Project(man:Manufacturer)
                        End
tmp:ExchangeModelNumber_OptionView   View(MODELNUM)
                          Project(mod:Model_Number)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormExchangeUnitFilter')
  loc:formname = 'FormExchangeUnitFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormExchangeUnitFilter','')
    p_web._DivHeader('FormExchangeUnitFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormExchangeUnitFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:ExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:ExchangeStockType',tmp:ExchangeStockType)
  p_web.SetSessionValue('tmp:ExchangeManufacturer',tmp:ExchangeManufacturer)
  p_web.SetSessionValue('tmp:ExchangeModelNumber',tmp:ExchangeModelNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ExchangeStockType')
    tmp:ExchangeStockType = p_web.GetValue('tmp:ExchangeStockType')
    p_web.SetSessionValue('tmp:ExchangeStockType',tmp:ExchangeStockType)
  End
  if p_web.IfExistsValue('tmp:ExchangeManufacturer')
    tmp:ExchangeManufacturer = p_web.GetValue('tmp:ExchangeManufacturer')
    p_web.SetSessionValue('tmp:ExchangeManufacturer',tmp:ExchangeManufacturer)
  End
  if p_web.IfExistsValue('tmp:ExchangeModelNumber')
    tmp:ExchangeModelNumber = p_web.GetValue('tmp:ExchangeModelNumber')
    p_web.SetSessionValue('tmp:ExchangeModelNumber',tmp:ExchangeModelNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormExchangeUnitFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ExchangeStockType = p_web.RestoreValue('tmp:ExchangeStockType')
 tmp:ExchangeManufacturer = p_web.RestoreValue('tmp:ExchangeManufacturer')
 tmp:ExchangeModelNumber = p_web.RestoreValue('tmp:ExchangeModelNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormExchangeUnitFilter')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormExchangeUnitFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormExchangeUnitFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormExchangeUnitFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormExchangeUnitFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormExchangeUnitFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormExchangeUnitFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Exchange Units') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Exchange Units',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormExchangeUnitFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormExchangeUnitFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormExchangeUnitFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Filter Exchange Units') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Available Exchange Units') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormExchangeUnitFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormExchangeUnitFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ExchangeStockType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormExchangeUnitFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Filter Exchange Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangeUnitFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Exchange Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Exchange Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Exchange Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Filter Exchange Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeStockType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&290&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&290&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&290&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Available Exchange Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangeUnitFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Available Exchange Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Available Exchange Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Available Exchange Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Available Exchange Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseExchangeUnits
      do Comment::BrowseExchangeUnits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:ExchangeStockType  Routine
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeStockType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeStockType',p_web.GetValue('NewValue'))
    tmp:ExchangeStockType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeStockType',p_web.GetValue('Value'))
    tmp:ExchangeStockType = p_web.GetValue('Value')
  End
  do Value::tmp:ExchangeStockType
  do SendAlert
  do Value::BrowseExchangeUnits  !1
  do Value::tmp:ExchangeManufacturer  !1
  do Value::tmp:ExchangeModelNumber  !1

Value::tmp:ExchangeStockType  Routine
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ExchangeStockType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeStockType'',''formexchangeunitfilter_tmp:exchangestocktype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeStockType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('tmp:AllStockTypes') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ExchangeStockType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ExchangeStockType') = 0
    p_web.SetSessionValue('tmp:ExchangeStockType','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Stock Types ==','',choose('' = p_web.getsessionvalue('tmp:ExchangeStockType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:ExchangeStockType_OptionView)
  tmp:ExchangeStockType_OptionView{prop:filter} = 'stp:Available = 1 AND Upper(stp:Use_Exchange) = <39>YES<39>'
  tmp:ExchangeStockType_OptionView{prop:order} = 'UPPER(stp:Stock_Type)'
  Set(tmp:ExchangeStockType_OptionView)
  Loop
    Next(tmp:ExchangeStockType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:ExchangeStockType') = 0
      p_web.SetSessionValue('tmp:ExchangeStockType',stp:Stock_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,stp:Stock_Type,choose(stp:Stock_Type = p_web.getsessionvalue('tmp:ExchangeStockType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:ExchangeStockType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_value')

Comment::tmp:ExchangeStockType  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeManufacturer  Routine
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeManufacturer',p_web.GetValue('NewValue'))
    tmp:ExchangeManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeManufacturer',p_web.GetValue('Value'))
    tmp:ExchangeManufacturer = p_web.GetValue('Value')
  End
  do Value::tmp:ExchangeManufacturer
  do SendAlert
  do Value::tmp:ExchangeModelNumber  !1
  do Value::BrowseExchangeUnits  !1

Value::tmp:ExchangeManufacturer  Routine
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ExchangeManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeManufacturer'',''formexchangeunitfilter_tmp:exchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeManufacturer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ExchangeManufacturer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ExchangeManufacturer') = 0
    p_web.SetSessionValue('tmp:ExchangeManufacturer','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Manufacturers ==','',choose('' = p_web.getsessionvalue('tmp:ExchangeManufacturer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:ExchangeManufacturer_OptionView)
  tmp:ExchangeManufacturer_OptionView{prop:order} = 'UPPER(man:Manufacturer)'
  Set(tmp:ExchangeManufacturer_OptionView)
  Loop
    Next(tmp:ExchangeManufacturer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:ExchangeManufacturer') = 0
      p_web.SetSessionValue('tmp:ExchangeManufacturer',man:Manufacturer)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,man:Manufacturer,choose(man:Manufacturer = p_web.getsessionvalue('tmp:ExchangeManufacturer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:ExchangeManufacturer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_value')

Comment::tmp:ExchangeManufacturer  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeModelNumber  Routine
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeModelNumber',p_web.GetValue('NewValue'))
    tmp:ExchangeModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeModelNumber',p_web.GetValue('Value'))
    tmp:ExchangeModelNumber = p_web.GetValue('Value')
  End
  do Value::tmp:ExchangeModelNumber
  do SendAlert
  do Value::BrowseExchangeUnits  !1

Value::tmp:ExchangeModelNumber  Routine
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ExchangeModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeModelNumber'',''formexchangeunitfilter_tmp:exchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeModelNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ExchangeModelNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ExchangeModelNumber') = 0
    p_web.SetSessionValue('tmp:ExchangeModelNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Model Numbers ==','',choose('' = p_web.getsessionvalue('tmp:ExchangeModelNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:ExchangeModelNumber_OptionView)
  tmp:ExchangeModelNumber_OptionView{prop:filter} = 'Upper(mod:Manufacturer ) = Upper(<39>' & p_web.GSV('tmp:ExchangeManufacturer') & '<39>)'
  tmp:ExchangeModelNumber_OptionView{prop:order} = 'UPPER(mod:Model_Number)'
  Set(tmp:ExchangeModelNumber_OptionView)
  Loop
    Next(tmp:ExchangeModelNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:ExchangeModelNumber') = 0
      p_web.SetSessionValue('tmp:ExchangeModelNumber',mod:Model_Number)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,mod:Model_Number,choose(mod:Model_Number = p_web.getsessionvalue('tmp:ExchangeModelNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:ExchangeModelNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_value')

Comment::tmp:ExchangeModelNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseExchangeUnits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseExchangeUnits',p_web.GetValue('NewValue'))
    do Value::BrowseExchangeUnits
  Else
    p_web.StoreValue('xch:Ref_Number')
  End

Value::BrowseExchangeUnits  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseExchangeUnits --
  p_web.SetValue('BrowseExchangeUnits:NoForm',1)
  p_web.SetValue('BrowseExchangeUnits:FormName',loc:formname)
  p_web.SetValue('BrowseExchangeUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormExchangeUnitFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormExchangeUnitFilter_BrowseExchangeUnits_embedded_div')&'"><!-- Net:BrowseExchangeUnits --></div><13,10>'
    p_web._DivHeader('FormExchangeUnitFilter_' & lower('BrowseExchangeUnits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormExchangeUnitFilter_' & lower('BrowseExchangeUnits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseExchangeUnits --><13,10>'
  end
  do SendPacket

Comment::BrowseExchangeUnits  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('BrowseExchangeUnits') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormExchangeUnitFilter_tmp:ExchangeStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeStockType
      else
        do Value::tmp:ExchangeStockType
      end
  of lower('FormExchangeUnitFilter_tmp:ExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeManufacturer
      else
        do Value::tmp:ExchangeManufacturer
      end
  of lower('FormExchangeUnitFilter_tmp:ExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeModelNumber
      else
        do Value::tmp:ExchangeModelNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)

PreCopy  Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormExchangeUnitFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormExchangeUnitFilter:Primed',0)
  p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormExchangeUnitFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormExchangeUnitFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormExchangeUnitFilter:Primed',0)
  p_web.StoreValue('tmp:ExchangeStockType')
  p_web.StoreValue('tmp:ExchangeManufacturer')
  p_web.StoreValue('tmp:ExchangeModelNumber')
  p_web.StoreValue('')
PickEngineersNotes   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPickList          STRING(10000)                         !
locFinalList         STRING(10000)                         !
locFinalListField    STRING(10000)                         !
locFoundField        STRING(10000)                         !
FilesOpened     Long
JOBNOTES::State  USHORT
NOTESENG::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('PickEngineersNotes')
  loc:formname = 'PickEngineersNotes_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickEngineersNotes','')
    p_web._DivHeader('PickEngineersNotes',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickEngineersNotes',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
clearVariables      ROUTINE
    p_web.DeleteSessionValue('locPickList')
    p_web.DeleteSessionValue('locFinalList')
    p_web.DeleteSessionValue('locFinalListField')
    p_web.DeleteSessionValue('locFoundField')
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(NOTESENG)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(NOTESENG)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickEngineersNotes_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do clearVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locPickList',locPickList)
  p_web.SetSessionValue('locFinalList',locFinalList)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPickList')
    locPickList = p_web.GetValue('locPickList')
    p_web.SetSessionValue('locPickList',locPickList)
  End
  if p_web.IfExistsValue('locFinalList')
    locFinalList = p_web.GetValue('locFinalList')
    p_web.SetSessionValue('locFinalList',locFinalList)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickEngineersNotes_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locPickList = p_web.RestoreValue('locPickList')
 locFinalList = p_web.RestoreValue('locFinalList')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickEngineersNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickEngineersNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickEngineersNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickEngineersNotes" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickEngineersNotes" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickEngineersNotes" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Engineers Notes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Engineers Notes',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickEngineersNotes">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickEngineersNotes" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickEngineersNotes')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickEngineersNotes')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickEngineersNotes'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPickList')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickEngineersNotes')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickEngineersNotes_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPickList
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPickList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFinalList
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFinalList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::button:AddSelected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AddSelected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::button:RemoveSelected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:RemoveSelected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::gap
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:RemoveAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::title  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('title',p_web.GetValue('NewValue'))
    do Value::title
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::title  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('title') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one entry.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locPickList  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPickList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPickList',p_web.GetValue('NewValue'))
    locPickList = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPickList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPickList',p_web.GetValue('Value'))
    locPickList = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::locFinalList  !1

Value::locPickList  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locPickList')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPickList'',''pickengineersnotes_locpicklist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPickList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locPickList',loc:fieldclass,loc:readonly,30,420,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locPickList') = 0
    p_web.SetSessionValue('locPickList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Select Engineers Note -----','',choose('' = p_web.getsessionvalue('locPickList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      access:noteseng.clearkey(noe:Notes_Key)
      noe:Reference = ''
      SET(noe:Notes_Key,noe:Notes_Key)
      LOOP
          IF (access:noteseng.next())
              BREAK
          END
          If Instring(';' & Clip(noe:notes),p_web.GSV('locFinalListField'),1,1)
              Cycle
          End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('locFinalListField'),1,1)
  
          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
          packet = clip(packet) & p_web.CreateOption(CLIP(noe:Reference) & ' - ' & Clip(noe:Notes),noe:Notes,choose(noe:Notes = p_web.GSV('locPickList')),clip(loc:rowstyle),,)&CRLF
          loc:even = Choose(loc:even=1,2,1)
      END
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_value')


Prompt::locFinalList  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFinalList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFinalList',p_web.GetValue('NewValue'))
    locFinalList = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFinalList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFinalList',p_web.GetValue('Value'))
    locFinalList = p_web.GetValue('Value')
  End
  do Value::locFinalList
  do SendAlert
  do Value::locPickList  !1

Value::locFinalList  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locFinalList')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFinalList'',''pickengineersnotes_locfinallist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFinalList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locFinalList',loc:fieldclass,loc:readonly,30,350,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Selected Engineers Note ----','',choose('' = p_web.getsessionvalue('locFinalList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('locFinalListField') <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                          packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                          loc:even = Choose(loc:even=1,2,1)
                      End ! If tmp:FoundAccessory <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                  packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                  loc:even = Choose(loc:even=1,2,1)
              End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('locFinalListField') <> ''
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_value')


Prompt::button:AddSelected  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::button:AddSelected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AddSelected',p_web.GetValue('NewValue'))
    do Value::button:AddSelected
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      p_web.SSV('locFinalListField',p_web.GSV('locFinalListField') & p_web.GSV('locPickList'))
      p_web.SSV('locPickList','')
  do Value::button:AddSelected
  do SendAlert
  do Value::locFinalList  !1
  do Value::locPickList  !1

Value::button:AddSelected  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AddSelected'',''pickengineersnotes_button:addselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddSelected','Add Selected','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_value')


Prompt::button:RemoveSelected  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::button:RemoveSelected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:RemoveSelected',p_web.GetValue('NewValue'))
    do Value::button:RemoveSelected
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      locFinalListField = p_web.GSV('locFinalListField') & ';'
      locFinalList = '|;' & Clip(p_web.GSV('locFinalList')) & ';'
      Loop x# = 1 To 10000
          pos# = Instring(Clip(locFinalList),locFinalListField,1,1)
          If pos# > 0
              locFinalListField = Sub(locFinalListField,1,pos# - 1) & Sub(locFinalListField,pos# + Len(Clip(locFinalList)),10000)
              Break
          End ! If pos# > 0#
      End ! Loop x# = 1 To 1000
      p_web.SSV('locFinalListField',Sub(locFinalListField,1,Len(Clip(locFinalListField)) - 1))
      p_web.SSV('locFinalList','')
  do Value::button:RemoveSelected
  do SendAlert
  do Value::locFinalList  !1
  do Value::locPickList  !1

Value::button:RemoveSelected  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveSelected'',''pickengineersnotes_button:removeselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','DeleteSelected','Delete Selected','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_value')


Validate::gap  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('gap',p_web.GetValue('NewValue'))
    do Value::gap
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::gap
  do SendAlert

Value::gap  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('gap') & '_value',Choose(1,'hdiv','adiv'))
  loc:extra = ''
  If Not (1)
  ! --- STRING --- 
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''gap'',''pickengineersnotes_gap_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','',p_web.GetSessionValueFormat(''),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickEngineersNotes_' & p_web._nocolon('gap') & '_value')


Validate::button:RemoveAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:RemoveAll',p_web.GetValue('NewValue'))
    do Value::button:RemoveAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      p_web.SetSessionValue('locFinalListField','')
      p_web.SetSessionValue('locFinalList','')
      p_web.SetSessionValue('locPickList','')
  do Value::button:RemoveAll
  do SendAlert
  do Value::locFinalList  !1
  do Value::locPickList  !1

Value::button:RemoveAll  Routine
  p_web._DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveAll'',''pickengineersnotes_button:removeall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAll','Remove All','SmallButtonFixed',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickEngineersNotes_' & p_web._nocolon('button:RemoveAll') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickEngineersNotes_locPickList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPickList
      else
        do Value::locPickList
      end
  of lower('PickEngineersNotes_locFinalList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFinalList
      else
        do Value::locFinalList
      end
  of lower('PickEngineersNotes_button:AddSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AddSelected
      else
        do Value::button:AddSelected
      end
  of lower('PickEngineersNotes_button:RemoveSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveSelected
      else
        do Value::button:RemoveSelected
      end
  of lower('PickEngineersNotes_gap_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::gap
      else
        do Value::gap
      end
  of lower('PickEngineersNotes_button:RemoveAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveAll
      else
        do Value::button:RemoveAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)

PreCopy  Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickEngineersNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      If Clip(p_web.GSV('locFinalListField')) <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          p_web.SSV('jbn:Engineers_Notes',p_web.GSV('jbn:Engineers_Notes') & '<13,10>' & | 
                              CLIP(locFoundField))
                      End ! If locFoundField <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  p_web.SSV('jbn:Engineers_Notes',p_web.GSV('jbn:Engineers_Notes') & '<13,10>' & | 
                      CLIP(locFoundField))
              End ! If locFoundField <> ''
          End ! If Start# > 0
      End ! If Clip(p_web.GSV('locFinalListField') <> ''
  p_web.DeleteSessionValue('PickEngineersNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  do clearVariables
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locPickList')
  p_web.StoreValue('locFinalList')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
PendingJob           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:EDIFileType    = 'ALCATEL' Or  |
            man:EDIFileType   = 'BOSCH' Or    |
            man:EDIFileType   = 'ERICSSON' Or |
            man:EDIFileType   = 'LG' Or    |
            man:EDIFileType   = 'MAXON' Or    |
            man:EDIFileType   = 'MOTOROLA' Or |
            man:EDIFileType   = 'NEC' Or      |
            man:EDIFileType   = 'NOKIA' Or    |
            man:EDIFileType   = 'SIEMENS' Or  |
            man:EDIFileType   = 'SAMSUNG' Or  |
            man:EDIFileType   = 'SAMSUNG SA' Or  |
            man:EDIFileType   = 'MITSUBISHI' Or   |
            man:EDIFileType   = 'TELITAL' Or  |
            man:EDIFileType   = 'SAGEM' Or    |
            man:EDIFileType   = 'SONY' Or     |
            man:EDIFileType   = 'PHILIPS' Or     |
            man:EDIFileType   = 'PANASONIC'

            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

            !Job should never becaome an EDI exception by mistake
            !So no need to double check if it's edi is set to exception

            case p_web.GSV('job:EDI')
            of 'EXC' orof 'REJ'
                p_web.SSV('wob:EDI',p_web.GSV('job:EDI'))
                return
            of 'EXM' orof 'AAJ'
                return
            end ! case p_web.GSV('job:EDI')

            !Do the calculation to check if a job should appear in the Pending Claims Table
            ! Job must be completed, a warranty job, and not already have a batch number. Not be an exception, a bouncer and not be excluded (by Charge TYpe or Repair Type)

            found# = 0
            If p_web.GSV('job:Date_Completed') <> ''
!Stop(2)
                If p_web.GSV('job:Warranty_Job') = 'YES'
!Stop(3)
                    If p_web.GSV('job:EDI_Batch_Number') = 0
!Stop(4)
                        If p_web.GSV('job:EDI') <> 'AAJ'
!Stop(5)
                            If p_web.GSV('job:EDI') <> 'EXC'
!Stop(6)
                                If ~(p_web.GSV('job:Bouncer') <> '' And (p_web.GSV('job:Bouncer_Type') = 'BOT' Or p_web.GSV('job:Bouncer_Type') = 'WAR'))
!Stop(7)
                                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                                    cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
                                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Found
                                        If cha:Exclude_EDI <> 'YES'
!Stop(8)
                                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                                            rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                                            rtd:Warranty = 'YES'
                                            rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
                                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Found
                                                If ~rtd:ExcludeFromEDI
!Stop(9)
                                                    If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                                        p_web.SSV('wob:EDI','NO')
                                                    End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'

                                                    !Check if the job is a 48 Hour Scrap/RTM with only one exchange - 4449
                                                    ScrapCheck# = 0
                                                    ! Is this a 48 hour job? (DBH: 05/02/2007)
                                                    If p_web.GSV('jobe:Engineer48HourOption') = 1
                                                        ! Is there only one exchange attached? (DBH: 05/02/2007)
                                                        If p_web.GSV('job:Exchange_Unit_Number') > 0
                                                            If p_web.GSV('jobe:SecondExchangeNumber') = 0
                                                                If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.' Or p_web.GSV('job:Repair_Type_Warranty') = 'SCRAP'
                                                                    ScrapCheck# = 1
                                                                    ! Do not claim for this job (DBH: 05/02/2007)
                                                                End ! If job:Repair_Type_Warranty = 'R.T.M.' Or job:Repair_Type_Warranty = 'SCRAP'
                                                            End ! If jobe:SecondExchangeNumber = 0
                                                        End ! If job:Exchange_Unit_Number > 0
                                                    End ! If jobe:Engineer48HourOption = 1

                                                    If ScrapCheck# = 0
!Stop(10)
                                                        If p_web.GSV('job:EDI') <> 'NO'
                                                            p_web.SSV('jobe2:InPendingDate',Today())
                                                        End ! If job:EDI <> 'NO'
                                                        p_web.SSV('job:EDI','NO')
                                                        found# = 1
                                                    End ! If ScrapCheck# = 0
                                                End ! If ~rtd:ExcludeFromEDI
                                            Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Error
                                            End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign

                                        End ! If cha:Exclude_EDI <> 'YES'
                                    Else ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Error
                                    End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign

                                End ! If (~job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR'))
                            Else ! If job:EDI <> 'EXC'
                                if (p_web.GSV('BookingSite') = 'RRC')
                                    p_web.SSV('wob:EDI','AAJ')
                                    p_web.SSV('job:EDI','AAJ')
                                Else ! If glo:WebJob
                                    p_web.SSV('job:EDI','EXC')
                                End ! If glo:WebJob
                                found# = 1
                            End ! If job:EDI <> 'EXC'
                        Else ! If job:EDI <> 'AAJ'
                        End ! If job:EDI <> 'AAJ'
                    Else ! If job:EDI_Batch_Number = 0
                        ! This is an error check (DBH: 05/02/2007)
                        ! If the job has a batch number then it can only be 3 things. Invoice, Reconciled or exception (DBH: 05/02/2007)
                        If p_web.GSV('job:Invoice_Number_Warranty') <> ''
                            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                p_web.SSV('wob:EDI','FIN')
                            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                            p_web.SSV('job:EDI','FIN')
                        Else ! If job:Invoice_Number_Warranty <> ''
                            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                p_web.SSV('wob:EDI','YES')
                            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                            p_web.SSV('job:EDI','YES')
                        End ! If job:Invoice_Number_Warranty <> ''
                        found# = 1
                    End ! If job:EDI_Batch_Number = 0
                End ! If job:Warranty_Job = 'YES'
            End ! If job:Date_Completed <> ''
        End ! man:EDIFileType   = 'PANASONIC'

        ! Make sure the EDI value doesn't change once it's set (DBH: 05/02/2007)

        ! Reconciled (DBH: 05/02/2007)
        If (found# = 0)
            If (p_web.GSV('job:EDI') = 'YES')
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') = 'APP'
                    p_web.SSV('wob:EDI','YES')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI = 'APP'
                p_web.SSV('job:EDI','YES')
                found# = 1
            End ! If job:EDI = 'YES'
        End ! If Clip(tmp:Return) = ''

        ! Invoiced (DBH: 05/02/2007)
        If (found# = 0)
            If p_web.GSV('job:EDI') = 'FIN'
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                    p_web.SSV('wob:EDI','FIN')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                p_web.SSV('job:EDI','FIN')
                found# = 1
            End ! If job:EDI = 'FIN'
        End ! If Clip(tmp:Return) = ''


        ! If all else failes, then this shouldn't be a warranty claim (DBH: 05/02/2007)
        If (found# = 0)
            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                p_web.SSV('wob:EDI','XXX')
            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
            p_web.SSV('job:EDI','XXX')
            found# = 1
        End ! If Clip(tmp:Return) = ''

        ! Inserting (DBH 05/02/2007) # 8707 - Write warranty information to new file
        Access:JOBSWARR.ClearKey(jow:RefNumberKey)
        jow:RefNumber = p_web.GSV('job:Ref_Number')
        If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Error
            If Access:JOBSWARR.PrimeRecord() = Level:Benign
                jow:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:JOBSWARR.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:JOBSWARR.TryInsert() = Level:Benign
                    Access:JOBSWARR.CancelAutoInc()
                End ! If Access:JOBSWARR.TryInsert() = Level:Benign
            End ! If Access.JOBSWARR.PrimeRecord() = Level:Benign
        End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign


        If (p_web.GSV('job:EDI') = 'XXX' Or found#= 0)
            Relate:JOBSWARR.Delete(0)
        Else ! If tmp:Return = 'XXX'
            jow:Status = p_web.GSV('job:EDI')
            jow:BranchID = tra:BranchIdentification
!            If SentToHub(job:Ref_Number)
!                jow:RepairedAt = 'ARC'
!            Else ! If SentToHub(job:Ref_Number)
!                jow:RepairedAt = 'RRC'
!            End ! If SentToHub(job:Ref_Number)
            jow:Manufacturer = p_web.GSV('job:Manufacturer')

            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty = 'YES'
            cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
            Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
            End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            If cha:SecondYearWarranty
                jow:FirstSecondYear = 1
            Else ! If cha:SecondYearWarranty
                jow:FirstSecondYear = 0
            End ! If cha:SecondYearWarranty

            If (p_web.GSV('job:EDI') = 'NO')
                If jow:Submitted = 0
                    jow:Submitted = 1
                End ! If jow:Submitted = 0
            End ! If tmp:Return = 'NO'
            Access:JOBSWARR.Update()
        End ! If tmp:Return = 'XXX'
        ! End (DBH 05/02/2007) #8707

    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:CHARTYPE.Close
     Access:REPTYDEF.Close
     Access:MANUFACT.Close
     Access:JOBSWARR.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
LockRecord           PROCEDURE  (fJobNumber,fSessionValue,fType) ! Declare Procedure
JOBSLOCK::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles

    access:JOBSLOCK.clearkey(lock:jobnumberkey)
    lock:jobNumber = fjobnumber
    if (access:jobslock.tryfetch(lock:jobnumberkey) = level:benign)
        if (fType = 0) ! Add Lock
            lock:datelocked = today()
            lock:timelocked = clock()
            lock:SessionValue = fsessionvalue
            access:JOBSLOCK.tryUpdate()
        else
            access:jobslock.deleterecord(0)
        end
        
    else
        if (fType = 0) ! Add Lock
            if (access:jobslock.primerecord() = level:benign)
                lock:jobNumber = fjobnumber
                lock:sessionValue = fsessionvalue
                if (access:jobslock.tryinsert() = level:benign)
                else
                    access:jobslock.cancelautoinc()
                end
            end
        end
    end
    do closefiles
SaveFiles  ROUTINE
  JOBSLOCK::State = Access:JOBSLOCK.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBSLOCK::State <> 0
    Access:JOBSLOCK.RestoreFile(JOBSLOCK::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSLOCK.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSLOCK.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSLOCK.Close
     FilesOpened = False
  END
ExcludeHandlingFee   PROCEDURE  (func:Type,func:Manufacturer,func:RepairType) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    rtnValue# = 0
    !Return Fatal if the Handling Fee should be excluded
    Case func:Type
        Of 'C'
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Chargeable   = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Found
                IF rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !IF rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
        Of 'W'
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Warranty     = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Found
                If rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !If rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
    End !Case func:Type

    do closeFiles
    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:REPTYDEF.Close
     FilesOpened = False
  END
JobPricingRoutine    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_epr_id          USHORT,AUTO                           !
tmp:ARCCLabourCost   REAL                                  !ARC Labour Cost
tmp:RRCCLabourCost   REAL                                  !RRC Labour Cost
tmp:ARCCPartsCost    REAL                                  !ARCC Parts Cost
tmp:RRCCPartsCost    REAL                                  !RRCC Parts Cost
tmp:ARCWLabourCost   REAL                                  !ARCW Labour Cost
tmp:RRCWLabourCost   REAL                                  !RRCW Labour Cost
tmp:ARCWPartsCost    REAL                                  !ARCW Parts Cost
tmp:RRCWPartsCost    REAL                                  !RRCW Parts Cost
tmp:ARCELabourCost   REAL                                  !ARCE Labour Cost
tmp:RRCELabourCost   REAL                                  !ARCELabour Cost
tmp:ARCEPartsCost    REAL                                  !ARCE Parts Cost
tmp:RRCEPartsCost    REAL                                  !RRCE Parts Cost
tmp:ClaimPartsCost   REAL                                  !Claim PartsCost
tmp:UseStandardRate  BYTE(0)                               !Use Standard Rate
tmp:ClaimValue       REAL                                  !Claim Value
tmp:FixedCharge      BYTE(0)                               !Fixed Charge
tmp:ExcludeHandlingFee BYTE(0)                             !Exclude Handling Fee
tmp:ManufacturerExchangeFee REAL                           !Manufacturer Exchange Fee
tmp:FixedChargeARC   BYTE(0)                               !Fixed Charge ARC
tmp:FixedChargeRRC   BYTE(0)                               !Fixed Charge RRC
tmp:ARCLocation      STRING(30)                            !ARC Location
tmp:RRCLocation      STRING(30)                            !RRCLocation
tmp:SecondYearWarranty BYTE(0)                             !Second Year Warranty (True/False)
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    sentToHub(p_web)

    tmp:FixedCharge = 0
    tmp:ExcludeHandlingFee = 0
    tmp:ManufacturerExchangeFee = 0
    tmp:FixedChargeARC = 0
    tmp:FixedChargeRRC = 0
    tmp:ClaimPartsCost = 0
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        tmp:ARCLocation = tra:SiteLocation
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber  = p_web.GSV('job:Ref_Number')
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            tmp:RRCLocation = tra:SiteLocation
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign

    If p_web.GSV('job:Chargeable_Job') <> 'YES'
        !If not chargeable job, blank chargeable costs - L874 (DBH: 16-07-2003)
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        p_web.SSV('job:Ignore_Chargeable_Charges','NO')
        p_web.SSV('jobe:IgnoreRRCChaCosts',0)

    Else !If p_web.GSV('job:Chargeable_Job <> 'YES'
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        p_web.SSV('jobe:HandlingFee',0)
        p_web.SSV('jobe:ExchangeRate',0)

        If ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))
            tmp:ExcludeHandlingFee = 1
        End !If ExcludeHandingFee('C',p_web.GSV('job:Manfacturer,p_web.GSV('job:Repair_Type)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type = p_web.GSV('job:Charge_Type')
        If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Found
            If cha:Zero_Parts_ARC
                tmp:FixedChargeARC = 1
            End !If cha:Zero_Parts_ARC = 'YES'
            If cha:Zero_Parts = 'YES'
                tmp:FixedChargeRRC = 1
            End !If cha:Zero_Parts = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If


                If ~cha:Zero_Parts_ARC
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            If p_web.GSV('job:Estimate') = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:ARCCPartsCost += epr:Sale_Cost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If p_web.GSV('job:Estimate = 'YES'
                                tmp:ARCCPartsCost += par:Sale_Cost * par:Quantity
                            End !If p_web.GSV('job:Estimate = 'YES'
                        End !If sto:Location = tmp:ARCLocatin
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts_ARC <> 'YES'
                    If p_web.GSV('job:Invoice_Number') = 0
                        par:Sale_Cost = 0
                        Access:PARTS.Update()
                    End !If p_web.GSV('job:Invoice_Number = 0
                End !If cha:Zero_Parts_ARC <> 'YES'
                If cha:Zero_Parts <> 'YES'
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            If p_web.GSV('job:Estimate') = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:RRCCPartsCost += epr:RRCSaleCost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If p_web.GSV('job:Estimate = 'YES'
                                tmp:RRCCPartsCost += par:RRCSaleCost * par:Quantity
                            End !If p_web.GSV('job:Estimate = 'YES'

                        End !If sto:Location <> tmp:RRCLocation
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts <> 'YES'
                    If p_web.GSV('job:Invoice_Number') = 0
                        par:RRCSaleCost = 0
                        Access:PARTS.Update()
                    End !If p_web.GSV('job:Invoice_Number = 0
                End !If cha:Zero_Parts <> 'YES'
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = p_web.GSV('job:account_number')
                suc:model_number   = p_web.GSV('job:model_number')
                suc:charge_type    = p_web.GSV('job:charge_type')
                suc:unit_type      = p_web.GSV('job:unit_type')
                suc:repair_type    = p_web.GSV('job:repair_type')
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCCLabourCost      = trc:Cost
                        tmp:RRCCLabourCost      = trc:RRCRate
                        p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCCLabourCost      = suc:Cost
                    tmp:RRCCLabourCost      = suc:RRCRate
                    p_web.SSV('jobe:HandlingFee',suc:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',suc:Exchange)

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = p_web.GSV('job:model_number')
                trc:charge_type    = p_web.GSV('job:charge_type')
                trc:unit_type      = p_web.GSV('job:unit_type')
                trc:repair_type    = p_web.GSV('job:repair_type')
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCCLabourCost      = trc:Cost
                    tmp:RRCCLabourCost      = trc:RRCRate
                    p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = p_web.GSV('job:model_number')
                sta:charge_type  = p_web.GSV('job:charge_type')
                sta:unit_type    = p_web.GSV('job:unit_type')
                sta:repair_type  = p_web.GSV('job:repair_type')
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCCLabourCost      = sta:Cost
                    tmp:RRCCLabourCost      = sta:RRCRate
                    p_web.SSV('jobe:HandlingFee',sta:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',sta:Exchange)
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Error
        End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Dont blank the cost because of their cock up .
        !They used this field for other costs, and now want to see it
        !even if it's not a loan
!        If ~LoanAttachedToJob(p_web.GSV('job:Ref_Number)
!            p_web.GSV('job:Courier_Cost = 0
!            p_web.GSV('job:Courier_Cost_Estimate = 0
!        End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
    End !p_web.GSV('job:Chargeable_Job = 'YES'

    tmp:SecondYearWarranty = 0
    If p_web.GSV('job:Warranty_Job') <> 'YES'
        !Blank costs if not a Warranty Job - L874 (DBH: 16-07-2003)
        tmp:ARCWLabourCost  = 0
        tmp:ARCWPartsCost   = 0
        tmp:RRCWLabourCost  = 0
        tmp:RRCWPartsCost   = 0
        tmp:ClaimValue      = 0
        p_web.SSV('job:Ignore_Warranty_Charges','NO')
        p_web.SSV('jobe:IgnoreRRCWarCosts',0)
        p_web.SSV('jobe:IgnoreClaimCosts',0)
    Else !If p_web.GSV('job:Warranty_Job = 'YES'
        !Do not reprice Warranty Completed Jobs - L945  (DBH: 03-09-2003)
        !Allow to force the reprice if the job conditions change - L945 (DBH: 04-09-2003)
        If p_web.GSV('job:Date_Completed') = 0 Or p_web.GSV('JobPricingRoutine:ForceWarranty') = 1
            tmp:ARCWLabourCost  = 0
            tmp:ARCWPartsCost   = 0
            tmp:RRCWLabourCost  = 0
            tmp:RRCWPartsCost   = 0
            p_web.SSV('jobe:HandlingFee',0)
            p_web.SSV('jobe:ExchangeRate',0)

            If ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))
                tmp:ExcludeHandlingFee = 1
            End !If ExcludeHandingFee('C',p_web.GSV('job:Manfacturer,p_web.GSV('job:Repair_Type)

            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                tmp:SecondYearWarranty = cha:SecondYearWarranty


                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                        Then Break.  ! End If

                    tmp:ClaimPartsCost += wpr:Purchase_Cost * wpr:Quantity

                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            tmp:ARCWPartsCost += wpr:Purchase_Cost * wpr:Quantity
                        End !If sto:Location = tmp:ARCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            tmp:RRCWPartsCost += wpr:RRCPurchaseCost * wpr:Quantity
                        End !If sto:Location = tmp:RRCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)

                tmp:UseStandardRate = 1
                If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                    access:subchrge.clearkey(suc:model_repair_type_key)
                    suc:account_number = p_web.GSV('job:account_number')
                    suc:model_number   = p_web.GSV('job:model_number')
                    suc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                    suc:unit_type      = p_web.GSV('job:unit_type')
                    suc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                    if access:subchrge.fetch(suc:model_repair_type_key)
                        access:trachrge.clearkey(trc:account_charge_key)
                        trc:account_number = sub:main_account_number
                        trc:model_number   = p_web.GSV('job:model_number')
                        trc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                        trc:unit_type      = p_web.GSV('job:unit_type')
                        trc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                            tmp:ARCWLabourCost      = trc:Cost
                            tmp:RRCWLabourCost      = trc:RRCRate
                            tmp:ClaimValue          = trc:WarrantyClaimRate
                            p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                            p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                            tmp:useStandardRate = 0
                        End!if access:trachrge.fetch(trc:account_charge_key)
                    Else
                        tmp:ARCWLabourCost      = suc:Cost
                        tmp:RRCWLabourCost      = suc:RRCRate
                        tmp:ClaimValue          = suc:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',suc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',suc:Exchange)

                        tmp:useStandardRate = 0
                    End!if access:subchrge.fetch(suc:model_repair_type_key)
                Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCWLabourCost      = trc:Cost
                        tmp:RRCWLabourCost      = trc:RRCRate
                        tmp:ClaimValue          = trc:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                        tmp:useStandardRate = 0

                    End!if access:trachrge.fetch(trc:account_charge_key)

                End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

                If tmp:useStandardRate
                    access:stdchrge.clearkey(sta:model_number_charge_key)
                    sta:model_number = p_web.GSV('job:model_number')
                    sta:charge_type  = p_web.GSV('job:Warranty_charge_type')
                    sta:unit_type    = p_web.GSV('job:unit_type')
                    sta:repair_type  = p_web.GSV('job:repair_type_Warranty')
                    if access:stdchrge.fetch(sta:model_number_charge_key)
                    Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                        tmp:ARCWLabourCost      = sta:Cost
                        tmp:RRCWLabourCost      = sta:RRCRate
                        tmp:ClaimValue          = sta:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',sta:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',sta:Exchange)
                    end !if access:stdchrge.fetch(sta:model_number_charge_key)
                End !If tmp:useStandardRate
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Found
                If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:SecondYrExchangeFee
                Else !If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:ExchangeFee
                End !If tmp:SecondYearWarranty

            Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Error
            End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        End !If p_web.GSV('job:Date_Completed = 0
    End !p_web.GSV('job:Warranty_Job = 'YES'

    tmp:ARCELabourCost = 0
    tmp:ARCEPartsCost  = 0
    tmp:RRCELabourCost = 0
    tmp:RRCEPartsCost   = 0
    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type = p_web.GSV('job:Charge_Type')
    If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Found
        Save_epr_ID = Access:ESTPARTS.SaveFile()
        Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
        epr:Ref_Number  = p_web.GSV('job:Ref_Number')
        Set(epr:Part_Number_Key,epr:Part_Number_Key)
        Loop
            If Access:ESTPARTS.NEXT()
               Break
            End !If
            If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Then Break.  ! End If
            If ~cha:Zero_Parts_ARC
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:ARCLocation
                        tmp:ARCEPartsCost += epr:Sale_Cost * epr:Quantity
                    End !If sto:Location = tmp:ARCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts_ARC <> 'YES'
                If p_web.GSV('job:Invoice_Number') = 0
                    epr:Sale_Cost = 0
                    Access:ESTPARTS.Update()
                End !If p_web.GSV('job:Invoice_Number = 0
            End !If cha:Zero_Parts_ARC <> 'YES'

            If cha:Zero_Parts <> 'YES'
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:RRCLocation
                        tmp:RRCEPartsCost += epr:RRCSaleCost * epr:Quantity
                    End !If sto:Location = tmp:RRCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts <> 'YES'
                If p_web.GSV('job:Invoice_Number') = 0
                    epr:RRCSaleCost = 0
                    Access:ESTPARTS.Update()
                End !If p_web.GSV('job:Invoice_Number = 0
            End !If cha:Zero_Parts <> 'YES'
        End !Loop
        Access:ESTPARTS.RestoreFile(Save_epr_ID)
        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Estimate_Accepted') = 'YES'
                !If estimate, then use the estimate cost.
                !it doesn't matter what the current costs are
                tmp:ARCELabourCost  = p_web.GSV('job:Labour_Cost_Estimate')
                tmp:RRCELabourCost  = p_web.GSV('jobe:RRCELabourCost')
                tmp:ARCCLabourCost  = p_web.GSV('job:Labour_Cost_Estimate')
                tmp:RRCCLabourCost  = p_web.GSV('jobe:RRCELabourCost')

                ! Inserting (DBH 03/12/2007) # 8218 - For RRC-ARC estimate, the RRC costs must mirror the ARC costs
                
                if (p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob'))
                    tmp:RRCCLabourCost = tmp:ARCCLabourCost
                    tmp:RRCCPartsCost = tmp:ARCCPartsCost
                End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
                ! End (DBH 03/12/2007) #8218
        Else !If p_web.GSV('job:Estimate = 'YES' And p_web.GSV('job:Estimate_Accepted = 'YES'

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = p_web.GSV('job:account_number')
                suc:model_number   = p_web.GSV('job:model_number')
                suc:charge_type    = p_web.GSV('job:charge_type')
                suc:unit_type      = p_web.GSV('job:unit_type')
                suc:repair_type    = p_web.GSV('job:repair_type')
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCELabourCost = trc:Cost
                        tmp:RRCELabourCost  = trc:RRCRate
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCELabourCost = suc:Cost
                    tmp:RRCELabourCost = suc:RRCRate

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = p_web.GSV('job:model_number')
                trc:charge_type    = p_web.GSV('job:charge_type')
                trc:unit_type      = p_web.GSV('job:unit_type')
                trc:repair_type    = p_web.GSV('job:repair_type')
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCELabourCost     = trc:Cost
                    tmp:RRCELabourCost  = trc:RRCRate
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = p_web.GSV('job:model_number')
                sta:charge_type  = p_web.GSV('job:charge_type')
                sta:unit_type    = p_web.GSV('job:unit_type')
                sta:repair_type  = p_web.GSV('job:repair_type')
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCELabourCost = sta:Cost
                    tmp:RRCELabourCost = sta:RRCRate
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        End !If p_web.GSV('job:Estimate = 'YES' And p_web.GSV('job:Estimate_Accepted = 'YES'
    Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Error
    End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

    If p_web.GSV('job:Exchange_Unit_Number') <> 0
        If p_web.GSV('jobe:ExchangedAtRRC')
            p_web.SSV('jobe:HandlingFee',0)
        Else !If p_web.GSV('jobe:ExchangedAtRRC
            p_web.SSV('jobe:ExchangeRate',0)
        End !If p_web.GSV('jobe:ExchangedAtRRC
    Else !p_web.GSV('job:Exchange_Unit_Number <> 0
        If (p_web.GSV('SentToHub') = 0)
            p_web.SSV('jobe:HandlingFee',0)
        End !If ~SentToHub(p_web.GSV('job:Ref_Number)
        p_web.SSV('jobe:ExchangeRate',0)
    End !p_web.GSV('job:Exchange_Unit_Number <> 0

    If tmp:ExcludeHandlingFee
        p_web.SSV('jobe:HandlingFee',0)
    End !If tmp:ExcludeHandlingFee

    If p_web.GSV('job:Ignore_Chargeable_Charges') <> 'YES'
        p_web.SSV('job:Labour_Cost',tmp:ARCCLabourCost)
        p_web.SSV('job:Parts_Cost', tmp:ARCCPartsCost)
        If p_web.GSV('job:Third_Party_Site') <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = p_web.GSV('job:Third_Party_Site')
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If p_web.GSV('jobe:Ignore3rdPartyCosts') <> 1
                    If p_web.GSV('jobe:ARC3rdPartyMarkup') = 0
                        p_web.SSV('jobe:ARC3rdPartyMarkup',trd:Markup)
                    End !If p_web.GSV('jobe:ARC3rdPartyMarkup = 0
                End !If ~p_web.GSV('jobe:Ignore3rdPartyCosts
                p_web.SSV('jobe:ARC3rdPartyVAT',p_web.GSV('jobe:ARC3rdPartyCost') * (trd:VatRate/100))
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            If tmp:FixedChargeARC = 0
                p_web.SSV('job:Labour_Cost',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('jobe:ARC3rdPartyMarkup'))
            End !If tmp:FixedChargeARC = 0
        End !If p_web.GSV('job:Third_Party_Site <> ''
    End !p_web.GSV('job:Ignore_Chargeable_Charges <> 'YES'

    If p_web.GSV('jobe:IgnoreRRCChaCosts') <> 1
        p_web.SSV('jobe:RRCCLabourCost',tmp:RRCCLabourCost)

        If p_web.GSV('job:Third_Party_Site') <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = p_web.GSV('job:Third_Party_Site')
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If p_web.GSV('jobe:Ignore3rdPartyCosts') <> 1
                    If p_web.GSV('jobe:ARC3rdPartyMarkup') = 0
                        p_web.SSV('jobe:ARC3rdPartyMarkup',trd:Markup)
                    End !If p_web.GSV('jobe:ARC3rdPartyMarkup = 0
                End !If ~p_web.GSV('jobe:Ignore3rdPartyCosts
                p_web.SSV('jobe:ARC3rdPartyVAT',p_web.GSV('jobe:ARC3rdPartyCost') * (trd:VatRate/100))
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

            If tmp:FixedChargeRRC = 0
                p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('jobe:ARC3rdPartyMarkup'))
            End !If tmp:FixedCharge = 0
        End !If p_web.GSV('job:Third_Party_Site <> ''
    End !~p_web.GSV('jobe:IgnoreRRCChaCosts
    p_web.SSV('jobe:RRCCPartsCost',tmp:RRCCPartsCost)

    !Only update prices on incomplete jobs - L945 (DBH: 03-09-2003)
    !Allow to reprice if the job conditions change - L945 (DBH: 04-09-2003)
    If p_web.GSV('job:Date_Completed') = 0 Or p_web.GSV('JobPricingRoutine:ForceWarranty') = 1
        If p_web.GSV('job:Ignore_Warranty_Charges') <> 'YES'
            p_web.SSV('job:Labour_Cost_Warranty',tmp:ARCWLabourCost)
            p_web.SSV('job:Courier_Cost_Warranty',0)

            If p_web.GSV('jobe:ExchangedAtRRC') = True
                If p_web.GSV('jobe:SecondExchangeNumber') <> 0
                    !Exchange attached at RRC
                    !Don't claim, unless the ARC has added a second exchange - 3788 (DBH: 07-04-2004)
                    p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                Else !If p_web.GSV('jobe:SecondExchangeNumber <> 0
                    !Exchange attached at RRC.
                    !Do not claim, unless sent to ARC, and job is RTM - 3788 (DBH: 07-04-2004)
                    If p_web.GSV('jobe:Engineer48HourOption') <> 1
                        If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.'
                            p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                        End !If p_web.GSV('job:Repair_Type_Warranty = 'R.T.M.'
                    End !If p_web.GSV('jobe:Engineer48HourOption = 1
                End !If p_web.GSV('jobe:SecondExchangeNumber <> 0
            Else !If p_web.GSV('jobe:ExchangedAtRRC = True
                !Exchange attached at ARC. Claim for it - 3788 (DBH: 07-04-2004)
                If p_web.GSV('job:Exchange_Unit_Number') <> 0
                    p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                End !If p_web.GSV('job:Exchange_Unit_Number <> 0
            End !If p_web.GSV('jobe:ExchangedAtRRC = True
        End !p_web.GSV('job:Invoice_Warranty_Charges <> 'YES'

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If p_web.GSV('job:Exchange_Unit_Number') > 0
            If p_web.GSV('jobe:HandsetReplacmentValue') > 0
                tmp:ARCWPartsCost += p_web.GSV('jobe:HandsetReplacmentValue')
            End ! If p_web.GSV('jobe:HandsetReplacmentValue > 0
        End ! If p_web.GSV('job:Exchange_Unit_Number > 0
        If p_web.GSV('jobe:SecondExchangeNumber') > 0
            If p_web.GSV('jobe:SecondHandsetRepValue') > 0
                tmp:ARCWPartsCost += p_web.GSV('jobe:SecondHandsetRepValue')
            End ! If p_web.GSV('jobe:SecondHandsetReplacementValue > 0
        End ! If p_web.GSV('jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        p_web.SSV('job:Parts_Cost_Warranty',tmp:ARCWPartsCost)

        If p_web.GSV('jobe:IgnoreRRCWarCosts') <> 1
            p_web.SSV('jobe:RRCWLabourCost',tmp:RRCWLabourCost)
        End !~p_web.GSV('jobe:IgnoreRRCWarCosts

        p_web.SSV('jobe:RRCWPartsCost',tmp:RRCWPartsCost)

        If p_web.GSV('jobe:IgnoreClaimCosts') <> 1
            p_web.SSV('jobe:ClaimValue',tmp:ClaimValue)
        End !If ~p_web.GSV('jobe:IgnoreClaimCosts

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If p_web.GSV('job:Exchange_Unit_Number') > 0
            If p_web.GSV('jobe:HandsetReplacmentValue') > 0
                tmp:ClaimPartsCost += p_web.GSV('jobe:HandsetReplacmentValue')
            End ! If p_web.GSV('jobe:HandsetReplacmentValue > 0
        End ! If p_web.GSV('job:Exchange_Unit_Number > 0
        If p_web.GSV('jobe:SecondExchangeNumber') > 0
            If p_web.GSV('jobe:SecondHandsetRepValue') > 0
                tmp:ClaimPartsCost += p_web.GSV('jobe:SecondHandsetRepValue')
            End ! If p_web.GSV('jobe:SecondHandsetReplacementValue > 0
        End ! If p_web.GSV('jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        p_web.SSV('jobe:ClaimPartsCost',tmp:ClaimPartsCost)
    End !If p_web.GSV('job:Date_Completed = 0

    If p_web.GSV('job:Ignore_Estimate_Charges') <> 'YES'
        p_web.SSV('job:Labour_Cost_Estimate',tmp:ARCELabourCost)
    End !p_web.GSV('job:Ignore_Estimate_Charges <> 'YES'
    p_web.SSV('job:Parts_Cost_Estimate',tmp:ARCEPartsCost)

    If p_web.GSV('jobe:IgnoreRRCEstCosts') <> 1
        p_web.SSV('jobe:RRCELabourCost',tmp:RRCELabourCost)
    End !~p_web.GSV('jobe:IgnoreRRCEstCosts

    p_web.SSV('jobe:RRCEPartsCost',tmp:RRCEPartsCost)

    !Fixed Pricing

    If (p_web.GSV('SentToHub') = 0)
        !Not a hub job
        If tmp:FixedChargeRRC
            p_web.SSV('jobe:RRCCPartsCost',0)
            p_web.SSV('jobe:RRCEPartsCost',0)
        End !If tmp:FixedChargeRRC
    Else !If ~SentToHub(p_web.GSV('job:Ref_Number)
        If p_web.GSV('jobe:WebJob') = 1
            If tmp:FixedChargeARC
                p_web.SSV('job:Parts_Cost',0)
                p_web.SSV('job:Parts_Cost_Estimate',0)
            End !If tmp:FixedChargeRRC

            If tmp:FixedChargeRRC
                p_web.SSV('jobe:RRCCPartsCost',0)
                p_web.SSV('jobe:RRCEPartsCost',0)
            Else !If tmp:FixedChargeRRC
                If p_web.GSV('jobe:IgnoreRRCChaCosts') <> 1
                    p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('job:Labour_Cost'))
                End !If ~p_web.GSV('jobe:IgnoreRRCChaCosts
            End !If tmp:FixedChargeRRC
        Else !If p_web.GSV('jobe:WebJob = 1
            If tmp:FixedChargeARC
                p_web.SSV('job:Parts_Cost',0)
                p_web.SSV('job:Parts_Cost_Estimate',0)
            End !If tmp:FixedChargeARC
        End !If p_web.GSV('jobe:WebJob = 1
    End !If ~SentToHub(p_web.GSV('job:Ref_Number)

    !Totals
! Inserting (DBH 03/12/2007) # 8218 - RRC has the same costs as the ARC for an estimate
    If (p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob'))
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('job:Parts_Cost_Estimate'))
    End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
! End (DBH 03/12/2007) #8218

    p_web.SSV('job:Sub_Total',p_web.GSV('job:Courier_Cost') + p_web.GSV('job:Labour_Cost') + p_web.GSV('job:Parts_Cost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('jobe:RRCCLabourCost') + p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:Courier_Cost'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('job:Labour_Cost_Warranty') + p_web.GSV('job:Parts_Cost_Warranty') + p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('jobe:RRCWLabourCost') + p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('job:Labour_Cost_Estimate') + p_web.GSV('job:Parts_Cost_Estimate') + p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate'))


    do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:WARPARTS.Close
     Access:CHARTYPE.Close
     Access:SUBCHRGE.Close
     Access:TRACHRGE.Close
     Access:STDCHRGE.Close
     Access:MANUFACT.Close
     Access:TRDPARTY.Close
     Access:WEBJOB.Close
     Access:JOBS.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
