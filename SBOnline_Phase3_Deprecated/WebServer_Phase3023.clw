

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3023.INC'),ONCE        !Local module procedure declarations
                     END


PopupMessage         PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:PopupMessage -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('PopupMessage')
    If p_web.GetSessionValue('PopupMessageText') <> ''
        packet = Clip(Packet) & '<script>alert("' & p_web.GetSessionValue('PopupMessageText') & '");</script>'
        Do SendPacket
        p_web.SetSessionValue('PopupMessageText','')
    End ! If p_web.GetSessionValue('PopupMessage') <> ''
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
PageHeader           PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')
loc:BorderStyle  Long

  CODE
  GlobalErrors.SetProcedureName('PageHeader')
  p_web.SetValue('_parentPage','PageHeader')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody',,'PageBodyDiv')
  Loc:BorderStyle = Net:Web:Round
  do BorderHeader:2
  do SendPacket
  do Heading
  do BorderFooter:2
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
BorderHeader:2 Routine
  Case loc:BorderStyle
  of Net:Web:Rounded
    packet = clip(packet) & |
    '  <div id="oPageHeader2" class="'&clip('headingouter')&'"><13,10>' & |
    '    <div id="iPageHeader2" class="'&clip('headinginner')&'"><13,10>'
  of Net:Web:Plain
      packet = clip(packet) & |
      '  <div id="oPageHeader2" class="'&clip('headingouter')&'"><13,10>'
  End

BorderFooter:2 Routine
  Case loc:BorderStyle
  of Net:Web:Rounded
    packet = clip(packet) & |
    '    </div><13,10>' & |
    '  </div><13,10>' & |
    '  <script type="text/javascript"><13,10>' & |
    '    var roundCorners = Rico.Corner.round.bind(Rico.Corner);<13,10>' & |
    '    roundCorners(''oPageHeader2'');<13,10>' & |
    '    roundCorners(''iPageHeader2'');<13,10>' & |
    '  </script><13,10>'
  of Net:Web:Plain
    packet = clip(packet) & '</div><13,10>'
  End
Heading  Routine
  packet = clip(packet) & |
    '<<table class="headingtable"><13,10>'&|
    ' <<tr><13,10>'&|
    '  <<td width="10%"><<img border="0" src="images/heading.png" /><</td><13,10>'&|
    '  <<td width="80%">CapeSoft Email Server - Configuration<</td><13,10>'&|
    '  <<td width="10%"><<a href="javascript:top.close()"><<img border="0" src="images/close.png" /><</a><</td><13,10>'&|
    ' <</tr><13,10>'&|
    '<</table><13,10>'&|
    ''
CloseWebPage         PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('CloseWebPage')
    p_web.SetSessionValue('tmp:Manufacturer','')
    p_web.SetSessionValue('tmp:ModelNumber','')
    p_web.SetSessionValue('tmp:TransitType','')
    p_web.SetSessionValue('tmp:IMEINumber','')
    p_web.SetSessionValue('tmp:ProductCode','')
    p_web.SetSessionValue('tmp:UnitType','')
    p_web.SetSessionValue('tmp:Network','')
    p_web.SetSessionValue('tmp:MobileNumber','')
    p_web.SetSessionValue('tmp:ChargeableJob','')
    p_web.SetSessionValue('tmp:ChargeableChargeType','')
    p_web.SetSessionValue('tmp:WarrantyJob','')
    p_web.SetSessionValue('tmp:WarrantyChargeType','')
    p_web.SetSessionValue('tmp:Colour','')
    p_web.SetSessionValue('tmp:DOP','')
    p_web.SetSessionValue('tmp:AccountNumber','')
    p_web.SetSessionValue('tmp:OrderNumber','')
    p_web.SetSessionValue('tmp:Initial','')
    p_web.SetSessionValue('tmp:Title','')
    p_web.SetSessionValue('tmp:Surname','')
    p_web.SetSessionValue('tmp:EndUserTelephoneNumber','')
    p_web.SetSessionValue('tmp:AuthorityNumber','')
    p_web.SetSessionValue('tmp:BookingOption','')
    p_web.SetSessionValue('tmp:VSACustomer','')
    p_web.SetSessionValue('tmp:HubRepair','')
    p_web.SetSessionValue('tmp:PrintDuplicateJobCard','')
    p_web.SetSessionValue('tmp:Contract','')
    p_web.SetSessionValue('tmp:PrePaid','')
    p_web.SetSessionValue('tmp:CustomerName','')
    p_web.SetSessionValue('tmp:AddressLine1','')
    p_web.SetSessionValue('tmp:AddressLine2','')
    p_web.SetSessionValue('tmp:Suburb','')
    p_web.SetSessionValue('tmp:Postcode','')
    p_web.SetSessionValue('tmp:TelephoneNumber','')
    p_web.SetSessionValue('tmp:FaxNumber','')
    p_web.SetSessionValue('tmp:CustomerNameCollection','')
    p_web.SetSessionValue('tmp:AddressLine1Collection','')
    p_web.SetSessionValue('tmp:AddressLine2Collection','')
    p_web.SetSessionValue('tmp:SuburbCollection','')
    p_web.SetSessionValue('tmp:PostcodeCollection','')
    p_web.SetSessionValue('tmp:TelephoneNumberCollection','')
    p_web.SetSessionValue('tmp:CompanyNameDelivery','')
    p_web.SetSessionValue('tmp:AddressLine1Delivery','')
    p_web.SetSessionValue('tmp:AddressLine2Delivery','')
    p_web.SetSessionValue('tmp:SuburbDelivery','')
    p_web.SetSessionValue('tmp:PostcodeDelivery','')
    p_web.SetSessionValue('tmp:TelephoneNumberDelivery','')
    p_web.SetSessionValue('tmp:EmailAddress','')
    p_web.SetSessionValue('tmp:OutgoingCourier','')
    p_web.SetSessionValue('tmp:VATNumber','')
    p_web.SetSessionValue('tmp:IDNumber','')
    p_web.SetSessionValue('tmp:SMSNotification','')
    p_web.SetSessionValue('tmp:SMSMobileNumber','')
    p_web.SetSessionValue('tmp:EmailNotification','')
    p_web.SetSessionValue('tmp:EmailAlertAddress','')
    p_web.SetSessionValue('tmp:CollectionText','')
    p_web.SetSessionValue('tmp:DeliveryText','')
    p_web.SetSessionValue('tmp:SubSubAccountNumber','')
    p_web.SetSessionValue('tmp:CheckAntenna','')
    p_web.SetSessionValue('tmp:CheckLens','')
    p_web.SetSessionValue('tmp:CheckFCover','')
    p_web.SetSessionValue('tmp:CheckBCover','')
    p_web.SetSessionValue('tmp:CheckKeypad','')
    p_web.SetSessionValue('tmp:CheckBattery','')
    p_web.SetSessionValue('tmp:CheckCharger','')
    p_web.SetSessionValue('tmp:CheckLCD','')
    p_web.SetSessionValue('tmp:CheckSimReader','')
    p_web.SetSessionValue('tmp:CheckSystemConnector','')
    p_web.SetSessionValue('tmp:CheckNone','')
    p_web.SetSessionValue('tmp:CheckNotes','')
    p_web.SetSessionValue('tmp:FaultDescription','')
    p_web.SetSessionValue('tmp:EngineerNotes','')
    p_web.SetSessionValue('tmp:InvoiceText','')
    p_web.SetSessionValue('tmp:OBF','')
    p_web.SetSessionValue('tmp:Location','')
    p_web.SetSessionValue('tmp:Workshop','')
    p_web.SetSessionValue('tmp:TurnaroundTime','')
    p_web.SetSessionValue('tmp:MSN','')
    p_web.SetSessionValue('tmp:IMEIValidation','')
    p_web.SetSessionValue('tmp:POP','')
    p_web.SetSessionValue('tmp:OBFValidated','')
    p_web.SetSessionValue('tmp:OBFValidateDate','')
    p_web.SetSessionValue('tmp:OBFValidateTime','')

    p_web.SetSessionValue('ReadOnly:Manufacturer','')
    p_web.SetSessionValue('ReadOnly:ModelNumber','')
    p_web.SetSessionValue('ReadOnly:TransitType','')
    p_web.SetSessionValue('ReadOnly:IMEINumber','')
    p_web.SetSessionValue('ReadOnly:ProductCode','')
    p_web.SetSessionValue('ReadOnly:UnitType','')
    p_web.SetSessionValue('ReadOnly:Network','')
    p_web.SetSessionValue('ReadOnly:MobileNumber','')
    p_web.SetSessionValue('ReadOnly:ChargeableJob','')
    p_web.SetSessionValue('ReadOnly:ChargeableChargeType','')
    p_web.SetSessionValue('ReadOnly:WarrantyJob','')
    p_web.SetSessionValue('ReadOnly:WarrantyChargeType','')
    p_web.SetSessionValue('ReadOnly:Colour','')
    p_web.SetSessionValue('ReadOnly:DOP','')
    p_web.SetSessionValue('ReadOnly:AccountNumber','')
    p_web.SetSessionValue('ReadOnly:OrderNumber','')
    p_web.SetSessionValue('ReadOnly:Initial','')
    p_web.SetSessionValue('ReadOnly:Title','')
    p_web.SetSessionValue('ReadOnly:Surname','')
    p_web.SetSessionValue('ReadOnly:EndUserTelephoneNumber','')
    p_web.SetSessionValue('ReadOnly:AuthorityNumber','')
    p_web.SetSessionValue('ReadOnly:BookingOption','')
    p_web.SetSessionValue('ReadOnly:VSACustomer','')
    p_web.SetSessionValue('ReadOnly:HubRepair','')
    p_web.SetSessionValue('ReadOnly:PrintDuplicateJobCard','')
    p_web.SetSessionValue('ReadOnly:Contract','')
    p_web.SetSessionValue('ReadOnly:PrePaid','')
    p_web.SetSessionValue('ReadOnly:CustomerName','')
    p_web.SetSessionValue('ReadOnly:AddressLine1','')
    p_web.SetSessionValue('ReadOnly:AddressLine2','')
    p_web.SetSessionValue('ReadOnly:Suburb','')
    p_web.SetSessionValue('ReadOnly:Postcode','')
    p_web.SetSessionValue('ReadOnly:TelephoneNumber','')
    p_web.SetSessionValue('ReadOnly:FaxNumber','')
    p_web.SetSessionValue('ReadOnly:CustomerNameCollection','')
    p_web.SetSessionValue('ReadOnly:AddressLine1Collection','')
    p_web.SetSessionValue('ReadOnly:AddressLine2Collection','')
    p_web.SetSessionValue('ReadOnly:SuburbCollection','')
    p_web.SetSessionValue('ReadOnly:PostcodeCollection','')
    p_web.SetSessionValue('ReadOnly:TelephoneNumberCollection','')
    p_web.SetSessionValue('ReadOnly:CompanyNameDelivery','')
    p_web.SetSessionValue('ReadOnly:AddressLine1Delivery','')
    p_web.SetSessionValue('ReadOnly:AddressLine2Delivery','')
    p_web.SetSessionValue('ReadOnly:SuburbDelivery','')
    p_web.SetSessionValue('ReadOnly:PostcodeDelivery','')
    p_web.SetSessionValue('ReadOnly:TelephoneNumberDelivery','')
    p_web.SetSessionValue('ReadOnly:EmailAddress','')
    p_web.SetSessionValue('ReadOnly:OutgoingCourier','')
    p_web.SetSessionValue('ReadOnly:VATNumber','')
    p_web.SetSessionValue('ReadOnly:IDNumber','')
    p_web.SetSessionValue('ReadOnly:SMSNotification','')
    p_web.SetSessionValue('ReadOnly:SMSMobileNumber','')
    p_web.SetSessionValue('ReadOnly:EmailNotification','')
    p_web.SetSessionValue('ReadOnly:EmailAlertAddress','')
    p_web.SetSessionValue('ReadOnly:CollectionText','')
    p_web.SetSessionValue('ReadOnly:DeliveryText','')
    p_web.SetSessionValue('ReadOnly:SubSubAccountNumber','')
    p_web.SetSessionValue('ReadOnly:CheckAntenna','')
    p_web.SetSessionValue('ReadOnly:CheckLens','')
    p_web.SetSessionValue('ReadOnly:CheckFCover','')
    p_web.SetSessionValue('ReadOnly:CheckBCover','')
    p_web.SetSessionValue('ReadOnly:CheckKeypad','')
    p_web.SetSessionValue('ReadOnly:CheckBattery','')
    p_web.SetSessionValue('ReadOnly:CheckCharger','')
    p_web.SetSessionValue('ReadOnly:CheckLCD','')
    p_web.SetSessionValue('ReadOnly:CheckSimReader','')
    p_web.SetSessionValue('ReadOnly:CheckSystemConnector','')
    p_web.SetSessionValue('ReadOnly:CheckNone','')
    p_web.SetSessionValue('ReadOnly:CheckNotes','')
    p_web.SetSessionValue('ReadOnly:FaultDescription','')
    p_web.SetSessionValue('ReadOnly:EngineerNotes','')
    p_web.SetSessionValue('ReadOnly:InvoiceText','')
    p_web.SetSessionValue('ReadOnly:OBF','')
    p_web.SetSessionValue('ReadOnly:Location','')
    p_web.SetSessionValue('ReadOnly:Workshop','')
    p_web.SetSessionValue('ReadOnly:TurnaroundTime','')
    p_web.SetSessionValue('ReadOnly:MSN','')
    p_web.SetSessionValue('ReadOnly:IMEIValidation','')
    p_web.SetSessionValue('ReadOnly:POP','')
    p_web.SetSessionValue('ReadOnly:OBFValidated','')
    p_web.SetSessionValue('ReadOnly:OBFValidateDate','')
    p_web.SetSessionValue('ReadOnly:OBFValidateTime','')

    p_web.SetSessionValue('Hide:Manufacturer','')
    p_web.SetSessionValue('Hide:ModelNumber','')
    p_web.SetSessionValue('Hide:TransitType','')
    p_web.SetSessionValue('Hide:IMEINumber','')
    p_web.SetSessionValue('Hide:ProductCode','')
    p_web.SetSessionValue('Hide:UnitType','')
    p_web.SetSessionValue('Hide:Network','')
    p_web.SetSessionValue('Hide:MobileNumber','')
    p_web.SetSessionValue('Hide:ChargeableJob','')
    p_web.SetSessionValue('Hide:ChargeableChargeType','')
    p_web.SetSessionValue('Hide:WarrantyJob','')
    p_web.SetSessionValue('Hide:WarrantyChargeType','')
    p_web.SetSessionValue('Hide:Colour','')
    p_web.SetSessionValue('Hide:DOP','')
    p_web.SetSessionValue('Hide:AccountNumber','')
    p_web.SetSessionValue('Hide:OrderNumber','')
    p_web.SetSessionValue('Hide:Initial','')
    p_web.SetSessionValue('Hide:Title','')
    p_web.SetSessionValue('Hide:Surname','')
    p_web.SetSessionValue('Hide:EndUserTelephoneNumber','')
    p_web.SetSessionValue('Hide:AuthorityNumber','')
    p_web.SetSessionValue('Hide:BookingOption','')
    p_web.SetSessionValue('Hide:VSACustomer','')
    p_web.SetSessionValue('Hide:HubRepair','')
    p_web.SetSessionValue('Hide:PrintDuplicateJobCard','')
    p_web.SetSessionValue('Hide:Contract','')
    p_web.SetSessionValue('Hide:PrePaid','')
    p_web.SetSessionValue('Hide:CustomerName','')
    p_web.SetSessionValue('Hide:AddressLine1','')
    p_web.SetSessionValue('Hide:AddressLine2','')
    p_web.SetSessionValue('Hide:Suburb','')
    p_web.SetSessionValue('Hide:Postcode','')
    p_web.SetSessionValue('Hide:TelephoneNumber','')
    p_web.SetSessionValue('Hide:FaxNumber','')
    p_web.SetSessionValue('Hide:CustomerNameCollection','')
    p_web.SetSessionValue('Hide:AddressLine1Collection','')
    p_web.SetSessionValue('Hide:AddressLine2Collection','')
    p_web.SetSessionValue('Hide:SuburbCollection','')
    p_web.SetSessionValue('Hide:PostcodeCollection','')
    p_web.SetSessionValue('Hide:TelephoneNumberCollection','')
    p_web.SetSessionValue('Hide:CompanyNameDelivery','')
    p_web.SetSessionValue('Hide:AddressLine1Delivery','')
    p_web.SetSessionValue('Hide:AddressLine2Delivery','')
    p_web.SetSessionValue('Hide:SuburbDelivery','')
    p_web.SetSessionValue('Hide:PostcodeDelivery','')
    p_web.SetSessionValue('Hide:TelephoneNumberDelivery','')
    p_web.SetSessionValue('Hide:EmailAddress','')
    p_web.SetSessionValue('Hide:OutgoingCourier','')
    p_web.SetSessionValue('Hide:VATNumber','')
    p_web.SetSessionValue('Hide:IDNumber','')
    p_web.SetSessionValue('Hide:SMSNotification','')
    p_web.SetSessionValue('Hide:SMSMobileNumber','')
    p_web.SetSessionValue('Hide:EmailNotification','')
    p_web.SetSessionValue('Hide:EmailAlertAddress','')
    p_web.SetSessionValue('Hide:CollectionText','')
    p_web.SetSessionValue('Hide:DeliveryText','')
    p_web.SetSessionValue('Hide:SubSubAccountNumber','')
    p_web.SetSessionValue('Hide:CheckAntenna','')
    p_web.SetSessionValue('Hide:CheckLens','')
    p_web.SetSessionValue('Hide:CheckFCover','')
    p_web.SetSessionValue('Hide:CheckBCover','')
    p_web.SetSessionValue('Hide:CheckKeypad','')
    p_web.SetSessionValue('Hide:CheckBattery','')
    p_web.SetSessionValue('Hide:CheckCharger','')
    p_web.SetSessionValue('Hide:CheckLCD','')
    p_web.SetSessionValue('Hide:CheckSimReader','')
    p_web.SetSessionValue('Hide:CheckSystemConnector','')
    p_web.SetSessionValue('Hide:CheckNone','')
    p_web.SetSessionValue('Hide:CheckNotes','')
    p_web.SetSessionValue('Hide:FaultDescription','')
    p_web.SetSessionValue('Hide:EngineerNotes','')
    p_web.SetSessionValue('Hide:InvoiceText','')
    p_web.SetSessionValue('Hide:OBF','')
    p_web.SetSessionValue('Hide:Location','')
    p_web.SetSessionValue('Hide:Workshop','')
    p_web.SetSessionValue('Hide:TurnaroundTime','')
    p_web.SetSessionValue('Hide:MSN','')
    p_web.SetSessionValue('Hide:IMEIValidation','')
    p_web.SetSessionValue('Hide:POP','')
    p_web.SetSessionValue('Hide:OBFValidated','')
    p_web.SetSessionValue('Hide:OBFValidateDate','')
    p_web.SetSessionValue('Hide:OBFValidateTime','')

    p_web.SetSessionValue('Comment:Manufacturer','')
    p_web.SetSessionValue('Comment:ModelNumber','')
    p_web.SetSessionValue('Comment:TransitType','')
    p_web.SetSessionValue('Comment:IMEINumber','')
    p_web.SetSessionValue('Comment:ProductCode','')
    p_web.SetSessionValue('Comment:UnitType','')
    p_web.SetSessionValue('Comment:Network','')
    p_web.SetSessionValue('Comment:MobileNumber','')
    p_web.SetSessionValue('Comment:ChargeableJob','')
    p_web.SetSessionValue('Comment:ChargeableChargeType','')
    p_web.SetSessionValue('Comment:WarrantyJob','')
    p_web.SetSessionValue('Comment:WarrantyChargeType','')
    p_web.SetSessionValue('Comment:Colour','')
    p_web.SetSessionValue('Comment:DOP','')
    p_web.SetSessionValue('Comment:AccountNumber','')
    p_web.SetSessionValue('Comment:OrderNumber','')
    p_web.SetSessionValue('Comment:Initial','')
    p_web.SetSessionValue('Comment:Title','')
    p_web.SetSessionValue('Comment:Surname','')
    p_web.SetSessionValue('Comment:EndUserTelephoneNumber','')
    p_web.SetSessionValue('Comment:AuthorityNumber','')
    p_web.SetSessionValue('Comment:BookingOption','')
    p_web.SetSessionValue('Comment:VSACustomer','')
    p_web.SetSessionValue('Comment:HubRepair','')
    p_web.SetSessionValue('Comment:PrintDuplicateJobCard','')
    p_web.SetSessionValue('Comment:Contract','')
    p_web.SetSessionValue('Comment:PrePaid','')
    p_web.SetSessionValue('Comment:CustomerName','')
    p_web.SetSessionValue('Comment:AddressLine1','')
    p_web.SetSessionValue('Comment:AddressLine2','')
    p_web.SetSessionValue('Comment:Suburb','')
    p_web.SetSessionValue('Comment:Postcode','')
    p_web.SetSessionValue('Comment:TelephoneNumber','')
    p_web.SetSessionValue('Comment:FaxNumber','')
    p_web.SetSessionValue('Comment:CustomerNameCollection','')
    p_web.SetSessionValue('Comment:AddressLine1Collection','')
    p_web.SetSessionValue('Comment:AddressLine2Collection','')
    p_web.SetSessionValue('Comment:SuburbCollection','')
    p_web.SetSessionValue('Comment:PostcodeCollection','')
    p_web.SetSessionValue('Comment:TelephoneNumberCollection','')
    p_web.SetSessionValue('Comment:CompanyNameDelivery','')
    p_web.SetSessionValue('Comment:AddressLine1Delivery','')
    p_web.SetSessionValue('Comment:AddressLine2Delivery','')
    p_web.SetSessionValue('Comment:SuburbDelivery','')
    p_web.SetSessionValue('Comment:PostcodeDelivery','')
    p_web.SetSessionValue('Comment:TelephoneNumberDelivery','')
    p_web.SetSessionValue('Comment:EmailAddress','')
    p_web.SetSessionValue('Comment:OutgoingCourier','')
    p_web.SetSessionValue('Comment:VATNumber','')
    p_web.SetSessionValue('Comment:IDNumber','')
    p_web.SetSessionValue('Comment:SMSNotification','')
    p_web.SetSessionValue('Comment:SMSMobileNumber','')
    p_web.SetSessionValue('Comment:EmailNotification','')
    p_web.SetSessionValue('Comment:EmailAlertAddress','')
    p_web.SetSessionValue('Comment:CollectionText','')
    p_web.SetSessionValue('Comment:DeliveryText','')
    p_web.SetSessionValue('Comment:SubSubAccountNumber','')
    p_web.SetSessionValue('Comment:CheckAntenna','')
    p_web.SetSessionValue('Comment:CheckLens','')
    p_web.SetSessionValue('Comment:CheckFCover','')
    p_web.SetSessionValue('Comment:CheckBCover','')
    p_web.SetSessionValue('Comment:CheckKeypad','')
    p_web.SetSessionValue('Comment:CheckBattery','')
    p_web.SetSessionValue('Comment:CheckCharger','')
    p_web.SetSessionValue('Comment:CheckLCD','')
    p_web.SetSessionValue('Comment:CheckSimReader','')
    p_web.SetSessionValue('Comment:CheckSystemConnector','')
    p_web.SetSessionValue('Comment:CheckNone','')
    p_web.SetSessionValue('Comment:CheckNotes','')
    p_web.SetSessionValue('Comment:FaultDescription','')
    p_web.SetSessionValue('Comment:EngineerNotes','')
    p_web.SetSessionValue('Comment:InvoiceText','')
    p_web.SetSessionValue('Comment:OBF','')
    p_web.SetSessionValue('Comment:Location','')
    p_web.SetSessionValue('Comment:Workshop','')
    p_web.SetSessionValue('Comment:TurnaroundTime','')
    p_web.SetSessionValue('Comment:MSN','')
    p_web.SetSessionValue('Comment:IMEIValidation','')
    p_web.SetSessionValue('Comment:POP','')
    p_web.SetSessionValue('Comment:OBFValidated','')
    p_web.SetSessionValue('Comment:OBFValidateDate','')
    p_web.SetSessionValue('Comment:OBFValidateTime','')

    p_web.SetSessionValue('Save:Manufacturer','')
    p_web.SetSessionValue('Save:ModelNumber','')
    p_web.SetSessionValue('Save:TransitType','')
    p_web.SetSessionValue('Save:IMEINumber','')
    p_web.SetSessionValue('Save:ProductCode','')
    p_web.SetSessionValue('Save:UnitType','')
    p_web.SetSessionValue('Save:Network','')
    p_web.SetSessionValue('Save:MobileNumber','')
    p_web.SetSessionValue('Save:ChargeableJob','')
    p_web.SetSessionValue('Save:ChargeableChargeType','')
    p_web.SetSessionValue('Save:WarrantyJob','')
    p_web.SetSessionValue('Save:WarrantyChargeType','')
    p_web.SetSessionValue('Save:Colour','')
    p_web.SetSessionValue('Save:DOP','')
    p_web.SetSessionValue('Save:AccountNumber','')
    p_web.SetSessionValue('Save:OrderNumber','')
    p_web.SetSessionValue('Save:Initial','')
    p_web.SetSessionValue('Save:Title','')
    p_web.SetSessionValue('Save:Surname','')
    p_web.SetSessionValue('Save:EndUserTelephoneNumber','')
    p_web.SetSessionValue('Save:AuthorityNumber','')
    p_web.SetSessionValue('Save:BookingOption','')
    p_web.SetSessionValue('Save:VSACustomer','')
    p_web.SetSessionValue('Save:HubRepair','')
    p_web.SetSessionValue('Save:PrintDuplicateJobCard','')
    p_web.SetSessionValue('Save:Contract','')
    p_web.SetSessionValue('Save:PrePaid','')
    p_web.SetSessionValue('Save:CustomerName','')
    p_web.SetSessionValue('Save:AddressLine1','')
    p_web.SetSessionValue('Save:AddressLine2','')
    p_web.SetSessionValue('Save:Suburb','')
    p_web.SetSessionValue('Save:Postcode','')
    p_web.SetSessionValue('Save:TelephoneNumber','')
    p_web.SetSessionValue('Save:FaxNumber','')
    p_web.SetSessionValue('Save:CustomerNameCollection','')
    p_web.SetSessionValue('Save:AddressLine1Collection','')
    p_web.SetSessionValue('Save:AddressLine2Collection','')
    p_web.SetSessionValue('Save:SuburbCollection','')
    p_web.SetSessionValue('Save:PostcodeCollection','')
    p_web.SetSessionValue('Save:TelephoneNumberCollection','')
    p_web.SetSessionValue('Save:CompanyNameDelivery','')
    p_web.SetSessionValue('Save:AddressLine1Delivery','')
    p_web.SetSessionValue('Save:AddressLine2Delivery','')
    p_web.SetSessionValue('Save:SuburbDelivery','')
    p_web.SetSessionValue('Save:PostcodeDelivery','')
    p_web.SetSessionValue('Save:TelephoneNumberDelivery','')
    p_web.SetSessionValue('Save:EmailAddress','')
    p_web.SetSessionValue('Save:OutgoingCourier','')
    p_web.SetSessionValue('Save:VATNumber','')
    p_web.SetSessionValue('Save:IDNumber','')
    p_web.SetSessionValue('Save:SMSNotification','')
    p_web.SetSessionValue('Save:SMSMobileNumber','')
    p_web.SetSessionValue('Save:EmailNotification','')
    p_web.SetSessionValue('Save:EmailAlertAddress','')
    p_web.SetSessionValue('Save:CollectionText','')
    p_web.SetSessionValue('Save:DeliveryText','')
    p_web.SetSessionValue('Save:SubSubAccountNumber','')
    p_web.SetSessionValue('Save:CheckAntenna','')
    p_web.SetSessionValue('Save:CheckLens','')
    p_web.SetSessionValue('Save:CheckFCover','')
    p_web.SetSessionValue('Save:CheckBCover','')
    p_web.SetSessionValue('Save:CheckKeypad','')
    p_web.SetSessionValue('Save:CheckBattery','')
    p_web.SetSessionValue('Save:CheckCharger','')
    p_web.SetSessionValue('Save:CheckLCD','')
    p_web.SetSessionValue('Save:CheckSimReader','')
    p_web.SetSessionValue('Save:CheckSystemConnector','')
    p_web.SetSessionValue('Save:CheckNone','')
    p_web.SetSessionValue('Save:CheckNotes','')
    p_web.SetSessionValue('Save:FaultDescription','')
    p_web.SetSessionValue('Save:EngineerNotes','')
    p_web.SetSessionValue('Save:InvoiceText','')
    p_web.SetSessionValue('Save:OBF','')
    p_web.SetSessionValue('Save:Location','')
    p_web.SetSessionValue('Save:Workshop','')
    p_web.SetSessionValue('Save:TurnaroundTime','')
    p_web.SetSessionValue('Save:MSN','')
    p_web.SetSessionValue('Save:IMEIValidation','')
    p_web.SetSessionValue('Save:POP','')
    p_web.SetSessionValue('Save:OBFValidated','')
    p_web.SetSessionValue('Save:OBFValidateDate','')
    p_web.SetSessionValue('Save:OBFValidateTime','')

    Loop x# = 1 To 20
        p_web.SetSessionValue('ReadOnly:FaultCode' & x#,'')
        p_web.SetSessionValue('tmp:FaultCode' & x#,'')
        p_web.SetSessionValue('Hide:FaultCode' & x#,'')
        p_web.SetSessionValue('Hide:FaultCodeLookup' & x#,'')
        p_web.SetSessionValue('Comment:FaultCode' & x#,'')
        p_web.SetSessionValue('Comment:FaultCodeLookup' & x#,'')
        p_web.SetSessionValue('Save:FaultCode' & x#,'')
    End ! Loop x# = 1 To 20

    p_web.SetSessionValue('Error:Generic','')
  p_web.SetValue('_parentPage','CloseWebPage')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody',,'PageBodyDiv')
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
    Do CloseWindow
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
CloseWindow  Routine
  packet = clip(packet) & |
    '<<script>top.close()<</script> <13,10>'&|
    ''
CountBouncer         PROCEDURE  (f_RefNumber,f_DateBooked,f_IMEI,func:Cjob,func:CCharge,func:CRepair,func:WJob,func:WCharge,func:WRepair) ! Declare Procedure
tmp:DateBooked       DATE                                  !
tmp:count            LONG                                  !
save_job2_id         USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
save_mfo_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wob_id          USHORT,AUTO                           !
tmp:IMEI             STRING(30)                            !
tmp:Manufacturer     STRING(30)                            !
tmp:IgnoreChargeable BYTE(0)                               !Ignore Chargeable
tmp:IgnoreWarranty   BYTE(0)                               !Ignore Warranty
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
local       Class
OutFaultExcluded    Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob),Byte
            End
  CODE
    RETURN vod.CountJobBouncers()

!!Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
!!From that count how many times the IMEI number has been booked in before
!tmp:IgnoreChargeable = GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI')
!tmp:IgnoreWarranty  = GETINI('BOUNCER','IgnoreWarranty',,CLIP(PATH())&'\SB2KDEF.INI')
!
!CheckBouncer# = 1
!If func:CJob = 'YES'
!    If tmp:IgnoreChargeable
!        CheckBouncer# = 0
!    Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!        Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!        cha:Charge_Type = func:CCharge
!        If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            !Found
!            If cha:ExcludeFromBouncer
!                CheckBouncer# = 0
!            Else !If cha:ExcludeFromBouncer
!                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                rtd:Manufacturer = job:Manufacturer
!                rtd:Chargeable   = 'YES'
!                rtd:Repair_Type  = func:CRepair
!                If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                    !Found
!                    If rtd:ExcludeFromBouncer
!                        CheckBouncer# = 0
!                    End !If rtd:ExcludeFromBouncer
!                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!            End !If cha:ExcludeFromBouncer
!        Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!End !If func:CJob = 'YES'
!
!!Is the job's outfault excluded?
!If CheckBouncer#
!    If Local.OutFaultExcluded(job:Ref_Number,job:Warranty_job,job:Chargeable_Job)
!        CheckBouncer# = 0
!    End !If Local.OutFaultExcluded()
!End !If CheckBouncer#
!
!!Is the job's infault excluded?
!If CheckBouncer#
!    Access:MANFAULT.ClearKey(maf:InFaultKey)
!    maf:Manufacturer = job:Manufacturer
!    maf:InFault      = 1
!    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!        Access:MANFAULO.ClearKey(mfo:Field_Key)
!        mfo:Manufacturer = job:Manufacturer
!        mfo:Field_Number = maf:Field_Number
!        Case maf:Field_Number
!        Of 1
!            mfo:Field        = job:Fault_Code1
!        Of 2
!            mfo:Field        = job:Fault_Code2
!        Of 3
!            mfo:Field        = job:Fault_Code3
!        Of 4
!            mfo:Field        = job:Fault_Code4
!        Of 5
!            mfo:Field        = job:Fault_Code5
!        Of 6
!            mfo:Field        = job:Fault_Code6
!        Of 7
!            mfo:Field        = job:Fault_Code7
!        Of 8
!            mfo:Field        = job:Fault_Code8
!        Of 9
!            mfo:Field        = job:Fault_Code9
!        Of 10
!            mfo:Field        = job:Fault_Code10
!        Of 11
!            mfo:Field        = job:Fault_Code11
!        Of 12
!            mfo:Field        = job:Fault_Code12
!        End !Case maf:Field_Number
!        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!            !Found
!            If mfo:ExcludeFromBouncer
!                CheckBouncer# = 0
!            End !If mfo:ExcludeBouncer
!        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!End !If CheckBouncer#
!
!!Lets write the bouncers into a memory queue
!Free(glo:Queue20)
!Clear(glo:Queue20)
!
!
!If CheckBouncer#
!    If func:WJob = 'YES'
!        If tmp:IgnoreWarranty
!            CheckBouncer# = 0
!        Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!            Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!            cha:Charge_Type = func:WCharge
!            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Found
!                If cha:ExcludeFromBouncer
!                    CheckBouncer# = 0
!                Else !If cha:ExcludeFromBouncer
!                    Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                    rtd:Manufacturer = job:Manufacturer
!                    rtd:Warranty     = 'YES'
!                    rtd:Repair_Type  = func:WRepair
!                    If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                        !Found
!                        If rtd:ExcludeFromBouncer
!                            CheckBouncer# = 0
!                        End !If rtd:ExcludeFromBouncer
!                    Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                End !If cha:ExcludeFromBouncer
!            Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!        End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!    End !If func:CJob = 'YES'
!End !If CheckBouncer#
!
!If CheckBouncer#
!    tmp:count    = 0
!    If f_IMEI <> 'N/A' And f_IMEI <> ''
!        Set(defaults)
!        Access:Defaults.Next()
!
!        setcursor(cursor:wait)
!        save_job2_id = access:jobs2_alias.savefile()
!        access:jobs2_alias.clearkey(job2:esn_key)
!        job2:esn = f_imei
!        set(job2:esn_key,job2:esn_key)
!        loop
!            if access:jobs2_alias.next()
!                break
!            end !if
!            if job2:esn <> f_imei      |
!                then break.  ! end if
!            yldcnt# += 1
!            if yldcnt# > 25
!                yield() ; yldcnt# = 0
!            end !if
!
!            If job2:Cancelled = 'YES'
!                Cycle
!            End !If job2:Cancelled = 'YES'
!
!            If job2:Exchange_Unit_Number <> ''
!                Cycle
!            End !If job2:Exchange_Unit_Number <> ''
!
!            If job2:Chargeable_Job = 'YES'
!                If tmp:IgnoreChargeable
!                    Cycle
!                End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job2:Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job2:Repair_Type <> ''
!                            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Chargeable   = 'YES'
!                            rtd:Repair_Type  = job2:Repair_Type
!                            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!
!                        End !If job2:Repair_Type <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!
!            If job2:Warranty_Job = 'YES'
!                If tmp:IgnoreWarranty
!                    Cycle
!                End !If tmp:IgnoreWarranty
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job2:Warranty_Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job2:Repair_Type_Warranty <> ''
!                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Warranty     = 'YES'
!                            rtd:Repair_Type  = job2:Repair_Type_Warranty
!                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        End !If job2:Repair_Type_Warranty <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!            !Has job got the same In Fault?
!
!            Access:MANFAULT.ClearKey(maf:InFaultKey)
!            maf:Manufacturer = job:Manufacturer
!            maf:InFault      = 1
!            If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Found
!                Case maf:Field_Number
!                Of 1
!                    If job2:Fault_Code1 <> job:Fault_Code1
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 2
!                    If job2:Fault_Code2 <> job:Fault_Code2
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 3
!                    If job2:Fault_Code3 <> job:Fault_Code3
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 4
!                    If job2:Fault_Code4 <> job:Fault_Code4
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 5
!                    If job2:Fault_Code5 <> job:Fault_Code5
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 6
!                    If job2:Fault_Code6 <> job:Fault_Code6
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 7
!                    If job2:Fault_Code7 <> job:Fault_Code7
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 8
!                    If job2:Fault_Code8 <> job:Fault_Code8
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 9
!                    If job2:Fault_Code9 <> job:Fault_Code9
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 10
!                    If job2:Fault_Code10 <> job:Fault_Code10
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 11
!                    If job2:Fault_Code11 <> job:Fault_Code11
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 12
!                    If job2:Fault_Code12 <> job:Fault_Code12
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!                End !Case maf:Field_Number
!                !Is the infault excluded?
!                Access:MANFAULO.ClearKey(mfo:Field_Key)
!                mfo:Manufacturer = job:Manufacturer
!                mfo:Field_Number = maf:Field_Number
!                Case maf:Field_Number
!                Of 1
!                    mfo:Field        = job2:Fault_Code1
!                Of 2
!                    mfo:Field        = job2:Fault_Code2
!                Of 3
!                    mfo:Field        = job2:Fault_Code3
!                Of 4
!                    mfo:Field        = job2:Fault_Code4
!                Of 5
!                    mfo:Field        = job2:Fault_Code5
!                Of 6
!                    mfo:Field        = job2:Fault_Code6
!                Of 7
!                    mfo:Field        = job2:Fault_Code7
!                Of 8
!                    mfo:Field        = job2:Fault_Code8
!                Of 9
!                    mfo:Field        = job2:Fault_Code9
!                Of 10
!                    mfo:Field        = job2:Fault_Code10
!                Of 11
!                    mfo:Field        = job2:Fault_Code11
!                Of 12
!                    mfo:Field        = job2:Fault_Code12
!                End !Case maf:Field_Number
!                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Found
!                    If mfo:ExcludeFromBouncer
!                        Cycle
!                    End !If mfo:ExcludeBouncer
!                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!            Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!
!            !Are the outfaults excluded?
!            If Local.OutFaultExcluded(job2:Ref_Number,job2:Warranty_job,job2:Chargeable_Job)
!                Cycle
!            End !If Local.OutFaultExcluded()
!
!            If job2:ref_number <> f_refnumber
!                Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                Of 2 !Despatched Date
!                    If job2:Date_Despatched <> ''
!                        If job2:date_despatched + def:bouncertime > f_datebooked And job2:date_despatched <= f_datebooked
!                            tmp:count += 1
!                            GLO:Pointer20 = job2:Ref_Number
!                            ADd(glo:Queue20)
!                        End!If job2:date_booked + man:warranty_period < Today()
!
!                    Else !If job2:Date_Despatched <> ''
!
!                        !Need to get the webjob record for the bouncer job
!                        !Will save the file, and then restore it afterwards.
!                        !Therefore, no records should be lost.
!
!                        Save_wob_ID = Access:WEBJOB.SaveFile()
!
!                        Access:WEBJOB.Clearkey(wob:RefNumberKey)
!                        wob:RefNumber   = job2:Ref_Number
!                        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            !Found
!
!                        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            !Error
!                        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!
!
!                        If wob:DateJobDespatched <> ''
!                            If wob:DateJobDespatched + def:bouncertime > f_datebooked And wob:DateJobDespatched <= f_datebooked
!
!                                tmp:count += 1
!                                GLO:Pointer20 = job2:Ref_Number
!                                ADd(glo:Queue20)
!                            End!If job2:date_booked + man:warranty_period < Today()
!
!                        End !If wob:JobDateDespatched <> ''
!
!                        Access:WEBJOB.RestoreFile(Save_wob_ID)
!
!                    End !If job2:Date_Despatched <> ''
!                Of 1 !Completed Date
!                    If job2:date_completed + def:bouncertime > f_datebooked And job2:date_completed <= f_datebooked
!                        tmp:count += 1
!                        GLO:Pointer20 = job2:Ref_Number
!                        ADd(glo:Queue20)
!
!                    End!If job2:date_booked + man:warranty_period < Today()
!                Else !Booking Date
!                    If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
!                        tmp:count += 1
!                        GLO:Pointer20 = job2:Ref_Number
!                        ADd(glo:Queue20)
!                    End!If job2:date_booked + man:warranty_period < Today()
!
!                End !Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                Else !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                End !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!            End!If job2:esn <> job2:ref_number
!        end !loop
!        access:jobs2_alias.restorefile(save_job2_id)
!        setcursor()
!    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
!End !If CheckBouncer#
!Return tmp:count
Local.OutFaultExcluded       Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob)
local:FaultCode     String(30)
Code
    !Loop through outfaults
    Save_joo_ID = Access:JOBOUTFL.SaveFile()
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = local:JobNumber
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
           Break
        End !If
        If joo:JobNumber <> local:JobNumber      |
            Then Break.  ! End If
        !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Lookup the Fault Code lookup to see if it's excluded
            Save_mfo_ID = Access:MANFAULO.SaveFile()
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                   Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                Or mfo:Field_Number <> maf:Field_Number      |
                Or mfo:Field        <> joo:FaultCode      |
                    Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
                    !Make sure the descriptions match in case of duplicates
                    If mfo:ExcludeFromBouncer
                        Return Level:Fatal
                    End !If mfo:ExcludeFromBoucer
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
            Access:MANFAULO.RestoreFile(Save_mfo_ID)

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    End !Loop
    Access:JOBOUTFL.RestoreFile(Save_joo_ID)

    !Is an outfault records on parts for this manufacturer
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        !Loop through the parts as see if any of the faults codes are excluded
        If local:WarrantyJob = 'YES'

            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = local:JobNumber
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> local:JobNumber      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            local:FaultCode        = wpr:Fault_Code1
                        Of 2
                            local:FaultCode        = wpr:Fault_Code2
                        Of 3
                            local:FaultCode        = wpr:Fault_Code3
                        Of 4
                            local:FaultCode        = wpr:Fault_Code4
                        Of 5
                            local:FaultCode        = wpr:Fault_Code5
                        Of 6
                            local:FaultCode        = wpr:Fault_Code6
                        Of 7
                            local:FaultCode        = wpr:Fault_Code7
                        Of 8
                            local:FaultCode        = wpr:Fault_Code8
                        Of 9
                            local:FaultCode        = wpr:Fault_Code9
                        Of 10
                            local:FaultCode        = wpr:Fault_Code10
                        Of 11
                            local:FaultCode        = wpr:Fault_Code11
                        Of 12
                            local:FaultCode        = wpr:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = local:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> local:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ExcludeFromBouncer
                            Return Level:Fatal
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If job:Warranty_Job = 'YES'

        If local:ChargeableJob = 'YES'
            !Loop through the parts as see if any of the faults codes are excluded
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = local:JobNumber
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> local:JobNumber      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            local:FaultCode        = par:Fault_Code1
                        Of 2
                            local:FaultCode        = par:Fault_Code2
                        Of 3
                            local:FaultCode        = par:Fault_Code3
                        Of 4
                            local:FaultCode        = par:Fault_Code4
                        Of 5
                            local:FaultCode        = par:Fault_Code5
                        Of 6
                            local:FaultCode        = par:Fault_Code6
                        Of 7
                            local:FaultCode        = par:Fault_Code7
                        Of 8
                            local:FaultCode        = par:Fault_Code8
                        Of 9
                            local:FaultCode        = par:Fault_Code9
                        Of 10
                            local:FaultCode        = par:Fault_Code10
                        Of 11
                            local:FaultCode        = par:Fault_Code11
                        Of 12
                            local:FaultCode        = par:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = local:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> local:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ExcludeFromBouncer
                            Return Level:Fatal
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'
    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Return Level:Benign
LiveBouncers         PROCEDURE  (f_DateBooked,f_IMEI)      ! Declare Procedure
tmp:DateBooked       DATE                                  !
tmp:count            LONG                                  !
save_job2_id         USHORT,AUTO                           !
tmp:IMEI             STRING(30)                            !
tmp:Manufacturer     STRING(30)                            !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    !Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
    !From that see if any of the bouncer jobs are still live, i.e. not completed.

    tmp:count    = 0
    If f_IMEI <> 'N/A' And f_IMEI <> ''
        Set(defaults)
        Access:Defaults.Next()

        save_job2_id = access:jobs2_alias.savefile()
        access:jobs2_alias.clearkey(job2:esn_key)
        job2:esn = f_imei
        set(job2:esn_key,job2:esn_key)
        loop
            if access:jobs2_alias.next()
               break
            end !if
            if job2:esn <> f_imei      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if

            If job2:Cancelled = 'YES'
                Cycle
            End !If job2:Cancelled = 'YES'


            If job2:cancelled <> 'YES'
                If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
                    If job2:Date_Completed = ''
                        Return Level:Fatal
                        Break
                    End !If job2:Date_Completed = ''

                End!If job2:date_booked + man:warranty_period < Today()
            End!If job2:esn <> job2:ref_number
        end !loop
        access:jobs2_alias.restorefile(save_job2_id)
    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
    Return Level:Benign
PreviousIMEI         PROCEDURE  (f:IMEI)                   ! Declare Procedure
save_jobs_alias_id   USHORT,AUTO                           !
save_exchange_id     USHORT,AUTO                           !
save_jobthird_id     USHORT,AUTO                           !
  CODE
    If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
        Return False
    End ! If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
    Found# = False
    Save_JOBS_ALIAS_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
    job_ali:ESN = f:IMEI
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop ! Begin Loop
        If Access:JOBS_ALIAS.Next()
            Break
        End ! If Access:JOBS_ALIAS.Next()
        If job_ali:ESN <> f:IMEI
            Break
        End ! If job_ali:ESN <> f:IMEI
        Found# = True
        Break
    End ! Loop
    Access:JOBS_ALIAS.RestoreFile(Save_JOBS_ALIAS_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_JOBTHIRD_ID = Access:JOBTHIRD.SaveFile()
    Access:JOBTHIRD.Clearkey(jot:OriginalIMEIKey)
    jot:OriginalIMEI     = f:IMEI
    Set(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
    Loop ! Begin Loop
        If Access:JOBTHIRD.Next()
            Break
        End ! If Access:JOBTHIRD.Next()
        If jot:OriginalIMEI <> f:IMEI
            Break
        End ! If jot:OriginalIMEI <> f:IMEI
        Found# = True
        Break
    End ! Loop
    Access:JOBTHIRD.RestoreFile(Save_JOBTHIRD_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
    Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
    xch:ESN = f:IMEI
    Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
    Loop ! Begin Loop
        If Access:EXCHANGE.Next()
            Break
        End ! If Access:EXCHANGE.Next()
        IF xch:ESN <> f:IMEI
            Break
        End ! IF xch:ESN <> f:IMEI
        If xch:Job_Number <> ''
            Found# = True
            Break
        End ! If xch:Job_Number <> ''
    End ! Loop
    Access:EXCHANGE.RestoreFile(Save_EXCHANGE_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Return False



SetBottom            PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:SetBottom -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('SetBottom')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    Packet = '<a name="bottom" id="bottom"></a>'
    Do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
GoToBottom           PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:GoToBottom -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('GoToBottom')
    packet = '<script type="text/javascript">' & CRLF &|
            'window.location=''#bottom''' & CRLF &|
            '</script>'
    Do SendPacket
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
PickAccessory        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:TheAccessory     STRING(1000)                          !The Accessory
tmp:TheJobAccessory  STRING(1000)                          !tmp:TheJobAccessory
tmp:ShowAccessory    STRING(1000)                          !
tmp:FoundAccessory   STRING(30)                            !Found Accessory
FilesOpened     Long
ACCESSOR::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('PickAccessory')
  loc:formname = 'PickAccessory_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickAccessory','')
    p_web._DivHeader('PickAccessory',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickAccessory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickAccessory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickAccessory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickAccessory',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickAccessory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickAccessory',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCESSOR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCESSOR)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickAccessory_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ShowAccessory')
    tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  End
  if p_web.IfExistsValue('tmp:TheAccessory')
    tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickAccessory_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
 tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GetSessionValue('ReturnPath:PickAccessory')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickAccessory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickAccessory_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickAccessory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
    do SendPacket
    Do heading
    do SendPacket
    Do ShowWait
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickAccessory" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickAccessory" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickAccessory" ></input><13,10>'
  end

  do SendPacket
  If SecurityCheckFailed(p_web.GetSessionValue('BookingUserPassword'),'JOBS - AMEND ACCESSORIES')
      Do NoAccess
  End ! If SecurityCheckFailed('JOBS - AMEND ACCESSORIES')
  
  
  
  
  
  
  

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickAccessory">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickAccessory" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickAccessory')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('<!--Tab-->') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickAccessory')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickAccessory'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ShowAccessory')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickAccessory')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
    do SendPacket
    Do CloseWait
    do SendPacket
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('<!--Tab-->') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickAccessory_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('<!--Tab-->')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('<!--Tab-->')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('<!--Tab-->')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('<!--Tab-->')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::HoldButton
      do Value::HoldButton
      do Comment::HoldButton
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShowAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShowAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ShowAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TheAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TheAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:TheAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:AddAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:AddAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:AddAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:RemoveAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:RemoveAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:RemoveAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:AddAll
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:AddAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:AddAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:RemoveAll
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:RemoveAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:RemoveAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::HoldButton  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('HoldButton') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('<b>Hold "CTRL" or "SHIFT" to select more than one accessory</b>')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::HoldButton  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('HoldButton',p_web.GetValue('NewValue'))
    do Value::HoldButton
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::HoldButton  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('HoldButton') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::HoldButton  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('HoldButton') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ShowAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Available Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ShowAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('NewValue'))
    tmp:ShowAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShowAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('Value'))
    tmp:ShowAccessory = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::tmp:TheAccessory  !1

Value::tmp:ShowAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('tmp:ShowAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ShowAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ShowAccessory'',''pickaccessory_tmp:showaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ShowAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ShowAccessory',loc:fieldclass,loc:readonly,25,30,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('- All Available Accessories For Model -','',choose('' = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GetSessionValue('job:Model_Number')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      If acr:Model_Number <> p_web.GetSessionValue('job:Model_Number')
          Break
      End ! If acr:Model_Number <> p_web.GetSessionValue('tmp:ModelNumber')
      If Instring(';' & Clip(acr:Accessory),p_web.GetSessionValue('tmp:TheJobAccessory'),1,1)
          Cycle
      End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GetSessionValue('tmp:TheJobAccessory'),1,1)
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickAccessory_' & p_web._nocolon('tmp:ShowAccessory') & '_value')

Comment::tmp:ShowAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('tmp:ShowAccessory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:TheAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('tmp:TheAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessories On Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TheAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('NewValue'))
    tmp:TheAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TheAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('Value'))
    tmp:TheAccessory = p_web.GetValue('Value')
  End
  do Value::tmp:TheAccessory
  do SendAlert

Value::tmp:TheAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('tmp:TheAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:TheAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TheAccessory'',''pickaccessory_tmp:theaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TheAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:TheAccessory',loc:fieldclass,loc:readonly,25,30,0,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('- Accessories Attached To Job -','',choose('' = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GetSessionValue('tmp:TheJobAccessory') <> ''
          Loop x# = 1 To 1000
              If Sub(p_web.GetSessionValue('tmp:TheJobAccessory'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GetSessionValue('tmp:TheJobAccessory'),x#,2) = '|;'
  
              If Start# > 0
                 If Sub(p_web.GetSessionValue('tmp:TheJobAccessory'),x#,2) = ';|'
                     tmp:FoundAccessory = Sub(p_web.GetSessionValue('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                     If tmp:FoundAccessory <> ''
                         loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                         packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                         loc:even = Choose(loc:even=1,2,1)
                     End ! If tmp:FoundAccessory <> ''
                     Start# = 0
                 End ! If Sub(p_web.GetSessionValue('tmp:TheJobAccessory'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
             tmp:FoundAccessory = Clip(Sub(p_web.GetSessionValue('tmp:TheJobAccessory'),Start#,30))
             If tmp:FoundAccessory <> ''
                 loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                 packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                 loc:even = Choose(loc:even=1,2,1)
             End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GetSessionValue('tmp:TheJobAccessory') <> ''
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickAccessory_' & p_web._nocolon('tmp:TheAccessory') & '_value')

Comment::tmp:TheAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('tmp:TheAccessory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:AddAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:AddAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:AddAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:AddAccessory',p_web.GetValue('NewValue'))
    do Value::Button:AddAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SetSessionValue('tmp:TheJobAccessory',p_web.GetSessionValue('tmp:TheJobAccessory') & p_web.GetSessionValue('tmp:ShowAccessory'))
  p_web.SetSessionValue('tmp:ShowAccessory','')
  
  do Value::Button:AddAccessory
  do SendAlert
  do Value::tmp:TheAccessory  !1
  do Value::tmp:ShowAccessory  !1

Value::Button:AddAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:AddAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('AddAccessory')&'.disabled=true;sv(''Button:AddAccessory'',''pickaccessory_button:addaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddAccessory','Add Accessories','SmallButtonFixedIcon',loc:formname,,,,loc:javascript,0,'/images/pinsert.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickAccessory_' & p_web._nocolon('Button:AddAccessory') & '_value')

Comment::Button:AddAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:AddAccessory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:RemoveAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:RemoveAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:RemoveAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:RemoveAccessory',p_web.GetValue('NewValue'))
    do Value::Button:RemoveAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  tmp:TheJobAccessory = p_web.GetSessionValue('tmp:TheJobAccessory') & ';'
  tmp:TheAccessory = '|;' & Clip(p_web.GetSessionValue('tmp:TheAccessory')) & ';'
  Loop x# = 1 To 1000
      pos# = Instring(Clip(tmp:TheAccessory),tmp:TheJobAccessory,1,1)
      If pos# > 0
          tmp:TheJobAccessory = Sub(tmp:TheJobAccessory,1,pos# - 1) & Sub(tmp:TheJobAccessory,pos# + Len(Clip(tmp:TheAccessory)),1000)
          Break
      End ! If pos# > 0#
  End ! Loop x# = 1 To 1000
  p_web.SetSessionValue('tmp:TheJobAccessory',Sub(tmp:TheJobAccessory,1,Len(Clip(tmp:TheJobAccessory)) - 1))
  p_web.SetSessionValue('tmp:TheAccessory','')
  do Value::Button:RemoveAccessory
  do SendAlert
  do Value::tmp:TheAccessory  !1
  do Value::tmp:ShowAccessory  !1

Value::Button:RemoveAccessory  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:RemoveAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('RemoveAccessory')&'.disabled=true;sv(''Button:RemoveAccessory'',''pickaccessory_button:removeaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAccessory','Remove Accessories','SmallButtonFixedIcon',loc:formname,,,,loc:javascript,0,'/images/pdelete.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickAccessory_' & p_web._nocolon('Button:RemoveAccessory') & '_value')

Comment::Button:RemoveAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:RemoveAccessory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:AddAll  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:AddAll') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:AddAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:AddAll',p_web.GetValue('NewValue'))
    do Value::Button:AddAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::Button:AddAll  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:AddAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''Button:AddAll'',''pickaccessory_button:addall_value'',1,1)')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('',) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickAccessory_' & p_web._nocolon('Button:AddAll') & '_value')

Comment::Button:AddAll  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:AddAll') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:RemoveAll  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:RemoveAll') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:RemoveAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:RemoveAll',p_web.GetValue('NewValue'))
    do Value::Button:RemoveAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SetSessionValue('tmp:TheJobAccessory','')
  p_web.SetSessionValue('tmp:TheAccessory','')
  p_web.SetSessionValue('tmp:ShowAccessory','')
  do Value::Button:RemoveAll
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::Button:RemoveAll  Routine
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:RemoveAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('RemoveAll')&'.disabled=true;sv(''Button:RemoveAll'',''pickaccessory_button:removeall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAll','Remove All Accessories','SmallButtonFixedIcon',loc:formname,,,,loc:javascript,0,'/images/pdelete.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickAccessory_' & p_web._nocolon('Button:RemoveAll') & '_value')

Comment::Button:RemoveAll  Routine
    loc:comment = ''
  p_web._DivHeader('PickAccessory_' & p_web._nocolon('Button:RemoveAll') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickAccessory_tmp:ShowAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ShowAccessory
      else
        do Value::tmp:ShowAccessory
      end
  of lower('PickAccessory_tmp:TheAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TheAccessory
      else
        do Value::tmp:TheAccessory
      end
  of lower('PickAccessory_Button:AddAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:AddAccessory
      else
        do Value::Button:AddAccessory
      end
  of lower('PickAccessory_Button:RemoveAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:RemoveAccessory
      else
        do Value::Button:RemoveAccessory
      end
  of lower('PickAccessory_Button:AddAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:AddAll
      else
        do Value::Button:AddAll
      end
  of lower('PickAccessory_Button:RemoveAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:RemoveAll
      else
        do Value::Button:RemoveAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickAccessory_form:ready_',1)
  p_web.SetSessionValue('PickAccessory_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickAccessory',0)

PreCopy  Routine
  p_web.SetValue('PickAccessory_form:ready_',1)
  p_web.SetSessionValue('PickAccessory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickAccessory',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickAccessory_form:ready_',1)
  p_web.SetSessionValue('PickAccessory_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickAccessory:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickAccessory_form:ready_',1)
  p_web.SetSessionValue('PickAccessory_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickAccessory:Primed',0)
  p_web.setsessionvalue('showtab_PickAccessory',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickAccessory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PickAccessory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickAccessory:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ShowAccessory')
  p_web.StoreValue('tmp:TheAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
heading  Routine
  packet = clip(packet) & |
    '<<table class="FormSubTitle"><13,10>'&|
    '<<tr><13,10>'&|
    '    <<td><13,10>'&|
    '    <<span class="SubHeading">Add/Remove Accessories<</span><13,10>'&|
    '    <</td><13,10>'&|
    '<</tr><13,10>'&|
    '<</table><13,10>'&|
    ''
NoAccess  Routine
  packet = clip(packet) & |
    '<<table class="alertgreen"><13,10>'&|
    '<<tr><13,10>'&|
    '    <<td><13,10>'&|
    'You do not have access to amend accessories.<13,10>'&|
    '    <</td><13,10>'&|
    '<</tr><13,10>'&|
    '<</table><13,10>'&|
    ''
ShowWait  Routine
  packet = clip(packet) & |
    '<<iframe id="waitframe" src="Pleasewait.htm" style="border: 0 solid #0000CC; height: 55px; width: 950px; position: relative"><</iframe><13,10>'&|
    ''
CloseWait  Routine
  packet = clip(packet) & |
    '<<script type="text/javascript"><13,10>'&|
    'document.getElementById("waitframe").style.display="none";<13,10>'&|
    '<</script><13,10>'&|
    ''
ReloadWindow  Routine
  packet = clip(packet) & |
    '<<script type="text/javascript"><13,10>'&|
    'location.reload(1)<13,10>'&|
    '<</script><13,10>'&|
    ''
BrowseEngineersOnJob PROCEDURE  (NetWebServerWorker p_web)
tmp:EngineersName    STRING(60)                            !Engineers Name
tmp:SkillLevel       BYTE(0)                               !Skill Level
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBSENG)
                      Project(joe:RecordNumber)
                      Project(joe:DateAllocated)
                      Project(joe:AllocatedBy)
                      Project(joe:EngSkillLevel)
                      Project(joe:Status)
                      Project(joe:StatusDate)
                      Project(joe:StatusTime)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseEngineersOnJob')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseEngineersOnJob:NoForm')
      loc:NoForm = p_web.GetValue('BrowseEngineersOnJob:NoForm')
      loc:FormName = p_web.GetValue('BrowseEngineersOnJob:FormName')
    else
      loc:FormName = 'BrowseEngineersOnJob_frm'
    End
    p_web.SSV('BrowseEngineersOnJob:NoForm',loc:NoForm)
    p_web.SSV('BrowseEngineersOnJob:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseEngineersOnJob:NoForm')
    loc:FormName = p_web.GSV('BrowseEngineersOnJob:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseEngineersOnJob') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseEngineersOnJob')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSENG,joe:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TMP:ENGINEERSNAME') then p_web.SetValue('BrowseEngineersOnJob_sort','1')
    ElsIf (loc:vorder = 'JOE:DATEALLOCATED') then p_web.SetValue('BrowseEngineersOnJob_sort','2')
    ElsIf (loc:vorder = 'JOE:ALLOCATEDBY') then p_web.SetValue('BrowseEngineersOnJob_sort','3')
    ElsIf (loc:vorder = 'JOE:ENGSKILLLEVEL') then p_web.SetValue('BrowseEngineersOnJob_sort','4')
    ElsIf (loc:vorder = 'JOE:STATUS') then p_web.SetValue('BrowseEngineersOnJob_sort','6')
    ElsIf (loc:vorder = 'JOE:STATUSDATE') then p_web.SetValue('BrowseEngineersOnJob_sort','7')
    ElsIf (loc:vorder = 'JOE:STATUSTIME') then p_web.SetValue('BrowseEngineersOnJob_sort','8')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseEngineersOnJob:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseEngineersOnJob:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseEngineersOnJob:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseEngineersOnJob:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseEngineersOnJob:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseEngineersOnJob_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseEngineersOnJob_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:EngineersName','-tmp:EngineersName')
    Loc:LocateField = 'tmp:EngineersName'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'joe:DateAllocated','-joe:DateAllocated')
    Loc:LocateField = 'joe:DateAllocated'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:AllocatedBy)','-UPPER(joe:AllocatedBy)')
    Loc:LocateField = 'joe:AllocatedBy'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'joe:EngSkillLevel','-joe:EngSkillLevel')
    Loc:LocateField = 'joe:EngSkillLevel'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:Status)','-UPPER(joe:Status)')
    Loc:LocateField = 'joe:Status'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'joe:StatusDate','-joe:StatusDate')
    Loc:LocateField = 'joe:StatusDate'
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'joe:StatusTime','-joe:StatusTime')
    Loc:LocateField = 'joe:StatusTime'
  end
  if loc:vorder = ''
    loc:vorder = '+joe:JobNumber,-joe:DateAllocated,-UPPER(joe:TimeAllocated)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tmp:EngineersName')
    loc:SortHeader = p_web.Translate('Engineers Name')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@s60')
  Of upper('joe:DateAllocated')
    loc:SortHeader = p_web.Translate('Date Allocated')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@d6')
  Of upper('joe:AllocatedBy')
    loc:SortHeader = p_web.Translate('Allocated By')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@s3')
  Of upper('joe:EngSkillLevel')
    loc:SortHeader = p_web.Translate('Engineer Skill Level')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@s8')
  Of upper('joe:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@s30')
  Of upper('joe:StatusDate')
    loc:SortHeader = p_web.Translate('Status Date')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@d6')
  Of upper('joe:StatusTime')
    loc:SortHeader = p_web.Translate('Status Time')
    p_web.SetSessionValue('BrowseEngineersOnJob_LocatorPic','@t1b')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseEngineersOnJob:LookupFrom')
  End!Else
  loc:CloseAction = 'ViewJob'
  loc:formaction = 'BrowseEngineersOnJob'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
    do SendPacket
    Do Title
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineersOnJob:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineersOnJob:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseEngineersOnJob:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSENG"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joe:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineersOnJob',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseEngineersOnJob',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseEngineersOnJob.locate(''Locator2BrowseEngineersOnJob'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineersOnJob.cl(''BrowseEngineersOnJob'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseEngineersOnJob_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseEngineersOnJob_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseEngineersOnJob','Engineers Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Engineers Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseEngineersOnJob','Date Allocated','Click here to sort by Date Allocated',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Date Allocated')&'">'&p_web.Translate('Date Allocated')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseEngineersOnJob','Allocated By','Click here to sort by Allocated By',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Allocated By')&'">'&p_web.Translate('Allocated By')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseEngineersOnJob','Engineer Skill Level','Click here to sort by Engineer Skill Level',,'CenterJustify',,1)
        Else
          packet = clip(packet) & '<th class="CenterJustify" Title="'&p_web.Translate('Click here to sort by Engineer Skill Level')&'">'&p_web.Translate('Engineer Skill Level')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseEngineersOnJob','Status','Click here to sort by Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseEngineersOnJob','Status Date','Click here to sort by Status Date',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Status Date')&'">'&p_web.Translate('Status Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'8','BrowseEngineersOnJob','Status Time','Click here to sort by Status Time',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Status Time')&'">'&p_web.Translate('Status Time')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'joe:DateAllocated' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'joe:StatusDate' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('joe:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBSENG{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'joe:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joe:RecordNumber'),p_web.GetValue('joe:RecordNumber'),p_web.GetSessionValue('joe:RecordNumber'))
      loc:FilterWas = 'joe:JobNumber = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineersOnJob',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseEngineersOnJob_Filter')
    p_web.SetSessionValue('BrowseEngineersOnJob_FirstValue','')
    p_web.SetSessionValue('BrowseEngineersOnJob_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSENG,joe:RecordNumberKey,loc:PageRows,'BrowseEngineersOnJob',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSENG{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSENG,loc:firstvalue)
              Reset(ThisView,JOBSENG)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSENG{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSENG,loc:lastvalue)
            Reset(ThisView,JOBSENG)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joe:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineersOnJob.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineersOnJob.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineersOnJob.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineersOnJob.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineersOnJob',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseEngineersOnJob_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseEngineersOnJob_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseEngineersOnJob',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseEngineersOnJob.locate(''Locator1BrowseEngineersOnJob'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineersOnJob.cl(''BrowseEngineersOnJob'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseEngineersOnJob_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseEngineersOnJob_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineersOnJob.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineersOnJob.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineersOnJob.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineersOnJob.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = joe:UserCode
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        tmp:EngineersName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
    Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        tmp:EngineersName = '* Unknown User Code: ' & Clip(joe:UserCode) & ' *'
    End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    
    loc:field = joe:RecordNumber
    p_web._thisrow = p_web._nocolon('joe:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseEngineersOnJob:LookupField')) = joe:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((joe:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseEngineersOnJob.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSENG{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSENG)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSENG{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSENG)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joe:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseEngineersOnJob.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','joe:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseEngineersOnJob.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:EngineersName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:DateAllocated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:AllocatedBy
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:EngSkillLevel
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:StatusDate
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::joe:StatusTime
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseEngineersOnJob.omv(this);" onMouseOut="BrowseEngineersOnJob.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseEngineersOnJob=new browseTable(''BrowseEngineersOnJob'','''&clip(loc:formname)&''','''&p_web._jsok('joe:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('joe:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseEngineersOnJob.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseEngineersOnJob.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineersOnJob')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineersOnJob')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineersOnJob')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineersOnJob')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSENG)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSENG)
  Bind(joe:Record)
  Clear(joe:Record)
  NetWebSetSessionPics(p_web,JOBSENG)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('joe:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::tmp:EngineersName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:EngineersName_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:EngineersName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:DateAllocated   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:DateAllocated_'&joe:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:DateAllocated,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:AllocatedBy   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:AllocatedBy_'&joe:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:AllocatedBy,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:EngSkillLevel   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:EngSkillLevel_'&joe:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:EngSkillLevel,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:Status_'&joe:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:StatusDate   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:StatusDate_'&joe:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:StatusDate,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:StatusTime   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('joe:StatusTime_'&joe:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(joe:StatusTime,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = joe:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('joe:RecordNumber',joe:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('joe:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joe:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('joe:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Title  Routine
  packet = clip(packet) & |
    '<<table class="FormSubTitle"><13,10>'&|
    '<<tr><13,10>'&|
    '    <<td><13,10>'&|
    '    <<span class="SubHeading">Engineers On Job History <</span><13,10>'&|
    '    <</td><13,10>'&|
    '<</tr><13,10>'&|
    '<</table><13,10>'&|
    ''
